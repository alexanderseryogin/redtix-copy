<?php

use App\Models\Concert;
use Carbon\Carbon;
use App\Exceptions\NotEnoughTicketsException;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ConcertTest extends TestCase
{

    use DatabaseMigrations;

    /**
     * @test
     */

    function can_get_formatted_date()
    {
        // Create a concert with a known date

        $concert = factory(Concert::class)->make([
            'date' => Carbon::parse('2017-01-20, 8.00pm')
        ]);

        // retrieve the formatted date

        $date = $concert->formatted_date;

        // verify that the date is formatted as expected

        $this->assertEquals('January 20, 2017', $date);
    }

    /**
     * @test
     */

    function can_get_formatted_time()
    {

        $concert = factory(Concert::class)->make([
            'date' => Carbon::parse('2016-12-25, 20:00:00')
        ]);

        $time = $concert->formatted_time;

        $this->assertEquals('8:00pm', $time);

    }

    /**
     * @test
     */

    function can_get_formatted_price_in_ringgit()
    {

        $concert = factory(Concert::class)->make([
            'ticket_price' => 2500
        ]);

        $this->assertEquals('25.00', $concert->ticket_price_in_ringgit);

    }

    /**
     * @test
     */
    function concert_with_a_published_at_date_are_published()
    {
        $publishedConcertA = factory(Concert::class)->create(['published_at' => Carbon::parse('-1 week')]);
        $publishedConcertB = factory(Concert::class)->create(['published_at' => Carbon::parse('-1 week')]);
        $unpublishedConcert = factory(Concert::class)->create(['published_at' => null]);

        // Get all the published concerts
        $publishedConcerts = Concert::published()->get();

        //assert
        $this->assertTrue($publishedConcerts->contains($publishedConcertA));
        $this->assertTrue($publishedConcerts->contains($publishedConcertB));
        $this->assertFalse($publishedConcerts->contains($unpublishedConcert));
    }

    /** @test */
    function can_order_concert_tickets()
    {
        //Arrange
        $concert = factory(Concert::class)->states('published')->create(['ticket_price' => '2500']);

        $concert->addTickets(3);

        //Act
        $order = $concert->orderTickets('john@example.com', 3);

        //Assert
        $this->assertEquals('john@example.com', $order->email);

        $this->assertEquals(3, $order->tickets()->count());
    }

    /** @test */
    function can_add_tickets_to_concert()
    {
        $concert = factory(Concert::class)->states('published')->create();

        $concert->addTickets(50);

        $this->assertEquals(50, $concert->ticketsRemaining());
    }

    /** @test */
    function tickets_remaining_does_not_include_tickets_associated_with_an_order()
    {
        $concert = factory(Concert::class)->states('published')->create();
        $concert->addtickets(50);
        $concert->orderTickets('john@example.com', 20);

        $this->assertEquals(30, $concert->ticketsRemaining());

    }

    /** @test */
    function trying_to_purchase_more_tickets_than_remain_throws_an_exception()
    {

        $concert = factory(Concert::class)->states('published')->create();

        $concert->addTickets(10);

        try
        {

            $concert->orderTickets('john@example.com', 11);

        } catch (NotEnoughTicketsException $e)
        {
            $order = $concert->orders()->where('email', 'john@example.com')->first();

            $this->assertNull($order);

            $this->assertEquals(10, $concert->ticketsRemaining());

            return;

        }

        $this->fail('Order succeeded even though there were not enough tickets');
    }

    /** @test */
    function cannot_order_tickets_that_have_already_been_purchased()
    {

        $concert = factory(Concert::class)->states('published')->create();

        $concert->addTickets(10);

        $concert->orderTickets('jane@example.com', 8);

        try
        {

            $order = $concert->orderTickets('john@example.com', 3);

        } catch (NotEnoughTicketsException $e)
        {

            $order = $concert->orders()->where('email', 'john@example.com')->first();

            $this->assertNull($order);

            $this->assertEquals(2, $concert->ticketsRemaining());

            return;
        }

        $this->fail('Order succeeded even though there were not enough tickets');

    }

}
