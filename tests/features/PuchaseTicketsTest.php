<?php

use App\Models\Concert;
use App\Billing\FakePaymentGateway;
use App\Billing\PaymentGateway;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PuchaseTicketsTest extends TestCase
{

    use DatabaseMigrations;

    protected function setUp()
    {
        parent::setUp();

        $this->paymentGateway = new FakePaymentGateway;

        $this->app->instance(PaymentGateway::class, $this->paymentGateway);
    }

    private function orderTickets($concert, $params)
    {
        $this->json('POST', "concerts/$concert->id/orders", $params);
    }

    private function assertValidationError($field)
    {

        $this->assertResponseStatus(422);

        $this->assertArrayHasKey($field, $this->decodeResponseJson());

        //dd($this->decodeResponseJson());

    }

    /** @test */

    function customer_can_purchase_concert_tickets_to_a_published_concert()
    {

        //Arrange
        $concert = factory(Concert::class)->states('published')->create(['ticket_price' => '3250']);

        $concert->addTickets(3);

        //Act

        $this->orderTickets($concert, [
            'email' => 'john@example.com',
            'ticket_quantity' => 3,
            'payment_token' => $this->paymentGateway->getValidTestToken()
        ]);

        //Assert

        //Assert the response
        $this->assertResponseStatus(201);

        // Correct amount was charged
        $this->assertEquals(9750, $this->paymentGateway->totalCharges());

        // Order for correct email was created

        $order = $concert->orders()->where('email', 'john@example.com')->first();

        $this->assertNotNull($order);

        //Check if the ticket quantity is right

        $this->assertEquals(3, $order->tickets()->count());

    }

    /** @test */
    function cannot_purchase_tickets_to_unpublished_concert()
    {

        $concert = factory(Concert::class)->states('unpublished')->create();

        $this->orderTickets($concert, [
            'email' => 'john@example.com',
            'ticket_quantity' => 3,
            'payment_token' => $this->paymentGateway->getValidTestToken()
        ]);

        $this->assertEquals(0, $concert->orders()->count());

        $this->assertResponseStatus(404);

        $this->assertEquals(0, $this->paymentGateway->totalCharges());

    }

    /** @test */
    function cannot_puchase_more_tickets_than_remain()
    {

        $concert = factory(Concert::class)->states('published')->create();

        $concert->addTickets(50);

        $this->orderTickets($concert, [
            'email' => 'john@example.com',
            'ticket_quantity' => 51,
            'payment_token' => $this->paymentGateway->getValidTestToken()
        ]);

        $this->assertResponseStatus(422);

        $order = $concert->orders()->where('email', 'john@example.com')->first();

        $this->assertNull($order);

        $this->assertEquals(50, $concert->ticketsRemaining());

        $this->assertEquals(0, $this->paymentGateway->totalCharges());

    }

    /** @test */
    function email_is_required_to_purchase_tickets()
    {

        //$this->disableExceptionHandling();

        $concert = factory(Concert::class)->states('published')->create();

        $this->orderTickets($concert, [
            'ticket_quantity' => 3,
            'payment_token' => $this->paymentGateway->getValidTestToken()
        ]);

        $this->assertValidationError('email');



    }

    /** @test */
    function valid_email_address_is_required_to_purchase_tickets()
    {
        //Arrange
        $concert = factory(Concert::class)->states('published')->create();

        //Act
        $this->orderTickets($concert, [
            'email' => 'invalid-email-address',
            'ticket_quantity' => 3,
            'payment_token' => $this->paymentGateway->getValidTestToken()
        ]);

        $this->assertValidationError('email');

    }

    /** @test */
    function ticket_quantity_is_required_to_puchase_ticket()
    {
        $concert = factory(Concert::class)->states('published')->create([]);

        $this->orderTickets($concert, [
            'email' => 'john@example.com',
            'payment_token' => $this->paymentGateway->getValidTestToken()
        ]);

        $this->assertValidationError('ticket_quantity');

    }
    
    /** @test */
    function ticket_quantity_must_be_at_least_1_to_purchase_ticket()
    {
        //Arrange
        $concert = factory(Concert::class)->states('published')->create();
        //Act
        $this->orderTickets($concert, [
            'email' => 'john@example.com',
            'ticket_quantity' => 0,
            'payment_token' => $this->paymentGateway->getValidTestToken()
        ]);

        $this->assertValidationError('ticket_quantity');

    }

    /** @test */
    function valid_payment_token_is_required_to_puchase_ticket()
    {
        //Arrange
        $concert = factory(Concert::class)->states('published')->create();
        //Act
        $this->orderTickets($concert, [
            'email' => 'john@example.com',
            'ticket_quantity' => 3,
        ]);

        $this->assertValidationError('payment_token');

    }

    /** @test */
    function an_order_is_not_created_if_payment_fails()
    {

        $concert = factory(Concert::class)->states('published')->create([]);

        $concert->addTickets(3);

        $this->orderTickets($concert, [
            'email' => 'john@example.com',
            'ticket_quantity' => 3,
            'payment_token' => 'invalid_payment_token'
        ]);

        $this->assertResponseStatus(422);

        $order = $concert->orders()->where('email', 'john@example.com')->first();

        $this->assertNull($order);

    }


}
