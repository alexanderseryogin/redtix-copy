<?php


use App\Models\Concert;
use Carbon\Carbon;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ViewConcertListingTest extends TestCase
{

    use DatabaseMigrations;

    /**
     * @test
     */

    function user_can_view_a_published_concert_listing()
    {

        $concert = factory(Concert::class)->states('published')->create([
            'title' => 'Incubus',
            'date' => Carbon::parse('January 20, 2016 8.00pm'),
            'ticket_price' => 3250,
            'venue' => 'Stadium Melawati',
            'venue_address' => '123, Jalan Gasing',
            'city' => 'Petaling Jaya',
            'state' => 'Selangor',
            'postcode' => '68100',
            'additional_information' => 'For tickets, call +6016 9772000'
        ]);

        $this->visit('concerts/' . $concert->id);


        $this->see('Incubus');
        $this->see('January 20, 2016');
        $this->see('8:00pm');
        $this->see('32.50');
        $this->see('Stadium Melawati');
        $this->see('123, Jalan Gasing');
        $this->see('Petaling Jaya');
        $this->see('68100 Selangor');
        $this->see('For tickets, call +6016 9772000');


    }

    /**
     * @test
     */
    function user_cannot_view_unpublished_concert_listings()
    {
        $concert = factory(Concert::class)->states('unpublished')->create();

        $this->get('/concerts/' . $concert->id);

        $this->assertResponseStatus(404);
    }


}
