<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

use Carbon\Carbon;

$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Models\Concert::class, function(Faker\Generator $faker) {

    return [
        'title' => 'Example Band',
        'date' => Carbon::parse('+2 weeks'),
        'ticket_price' => 2000,
        'venue' => 'Example Stadium Melawati',
        'venue_address' => '123, Jalan Example',
        'city' => 'Example Jaya',
        'state' => 'Selangor',
        'postcode' => '52100',
        'additional_information' => 'Some sample additional information'
    ];

});

$factory->state(App\Models\Concert::class, 'published', function(Faker\Generator $faker) {

    return [
        'published_at' => Carbon::parse('-1 week')
    ];

});

$factory->state(App\Models\Concert::class, 'unpublished', function(Faker\Generator $faker) {

    return [
        'published_at' => null
    ];

});