<?php

use Illuminate\Database\Seeder;
use App\Models\Transportation;

class TransportationsTableSeeders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id'=>1,
                'city_origin' => 2,
                'city_destination' => 1,
                'city_transit' => 1,
                'cost' => 2010,
                'origin_number' => 'QZ-234',
                'destination_number' => 'KL-432',
                'date_origin_1' => '2019-09-19 13:10',
                'date_destination_1' => '2019-09-22 14:10',
                'date_origin_2' => '2019-09-19 18:15',
                'date_destination_2' => '2019-09-22 19:30'
            ],
            [
                'id'=>2,
                'city_origin' => 3,
                'city_destination' => 1,
                'city_transit' => 2,
                'cost' => 3010,
                'origin_number' => 'AK-234',
                'destination_number' => 'ZL-432',
                'date_origin_1' => '2019-09-19 15:40',
                'date_destination_1' => '2019-09-22 21:10',
                'date_origin_2' => '2019-09-19 19:15',
                'date_destination_2' => '2019-09-23 01:25'
            ],
            [
                'id'=>3,
                'city_origin' => 4,
                'city_destination' => 1,
                'city_transit' => 1,
                'cost' => 4010,
                'origin_number' => 'QZ-234',
                'destination_number' => 'KL-432',
                'date_origin_1' => '2019-09-19 13:10',
                'date_destination_1' => '2019-09-22 14:10',
                'date_origin_2' => '2019-09-19 18:15',
                'date_destination_2' => '2019-09-22 19:30'
            ],
            [
                'id'=>4,
                'city_origin' => 5,
                'city_destination' => 1,
                'city_transit' => 2,
                'cost' => 5010,
                'origin_number' => 'AK-234',
                'destination_number' => 'ZL-432',
                'date_origin_1' => '2019-09-19 15:40',
                'date_destination_1' => '2019-09-22 21:10',
                'date_origin_2' => '2019-09-19 19:15',
                'date_destination_2' => '2019-09-23 01:25'
            ],
        ];

        foreach($data as $item)
        {
            Transportation::create($item);
        }
    }
}
