<?php

use Illuminate\Database\Seeder;

class HotelsFeaturesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'hotel_id' => 1,
                'title' => 'Breakfast',
                'icon' => 'breakfast',
            ],
            [
                'hotel_id' => 1,
                'title' => 'Air conditioner',
                'icon' => 'conditioner',
            ],
            [
                'hotel_id' => 1,
                'title' => 'Free Wi-fi',
                'icon' => 'wifi',
            ]
        ];

        foreach($data as $item)
        {
            \App\Models\HotelsFeatures::create($item);
        }
    }
}
