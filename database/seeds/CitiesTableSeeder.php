<?php

use Illuminate\Database\Seeder;
use App\Models\City;

class CitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'name' => 'Hanida, Japan',
                'abbr' => 'HND',
                'airport' => 'Tokyo International Airport',
                'img' => '',
                'origin' => 2,
            ],
            [
                'id' => '2',
                'name' => 'Kuala Lumpur',
                'abbr' => 'KUL',
                'airport' => 'Kuala Lumpur International Airport 2',
                'img' => '',
                'origin' => 1,
            ],
            [
                'id' => '3',
                'name' => 'Bangkok',
                'abbr' => 'DMK',
                'airport' => 'Bangkok Airport',
                'img' => '',
                'origin' => 1,
            ],
            [
                'id' => '4',
                'name' => 'Singapore',
                'abbr' => 'SIN',
                'airport' => 'Changi International Airport',
                'img' => '',
                'origin' => 1,
            ],
            [
                'id' => '5',
                'name' => 'Jakarta',
                'abbr' => 'CGK',
                'airport' => 'Jakarta Airport',
                'img' => '',
                'origin' => 1,
            ]
        ];

        foreach($data as $item)
        {
            City::create($item);
        }
    }
}
