<?php

use Illuminate\Database\Seeder;
use App\Models\Ticket;

class TicketsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tickets = [
            [
                'id' =>1,//ok
                'participant1_name' => 'Japan',
                'participant2_name' => 'Russia',
                'participant1_flag' => 'japan.png',
                'participant2_flag' => 'russia.png',
                'group' => 'Pool A',
                'cat_B' => 2600,
                'cat_C' => 2200,
                'cat_D' => 1800,
                'venue' => 'Tokyo Stadium, Tokyo',
                'date' => '2019-09-20 19:45',
                'map' => 'tokyo.png'
            ],
            [
                'id' =>2,//ok
                'participant1_name' => 'Ireland',
                'participant2_name' => 'Scotland',
                'participant1_flag' => 'ireland.png',
                'participant2_flag' => 'scotland.png',
                'group' => 'Pool A',
                'cat_B' => 1480,
                'cat_C' => 1160,
                'cat_D' => 880,
                'venue' => 'International Stadium, Yokohama City',
                'date' => '2019-09-22 16:45',
                'map' => 'yokohama.png'
            ],
            [
                'id' =>3,//ok
                'participant1_name' => 'New Zealand',
                'participant2_name' => 'South Africa',
                'participant1_flag' => 'new-zealand.png',
                'participant2_flag' => 'south-africa.png',
                'group' => 'Pool B',
                'cat_B' => 1800,
                'cat_C' => 1400,
                'cat_D' => 1000,
                'venue' => 'International Stadium, Yokohama City',
                'date' => '2019-09-21 18:45',
                'map' => 'yokohama.png'
            ],
            [
                'id' =>4,
                'participant1_name' => 'Italy',
                'participant2_name' => 'Namibia',
                'participant1_flag' => 'italy.png',
                'participant2_flag' => 'namibia.png',
                'group' => 'Pool B',
                'cat_B' => 1800,
                'cat_C' => 1400,
                'cat_D' => 1000,
                'venue' => 'Hanazono Rugby Stadium, Higashiosaka City',
                'date' => '2019-09-22 14:15',
                'map' => 'yokohama.png'
            ],
            [
                'id' =>5,//ok
                'participant1_name' => 'England',
                'participant2_name' => 'France',
                'participant1_flag' => 'england.png',
                'participant2_flag' => 'france.png',
                'group' => 'Pool C',
                'cat_B' => 1480,
                'cat_C' => 1160,
                'cat_D' => 880,
                'venue' => 'International Stadium, Yokohama City',
                'date' => '2019-10-12 17:15',
                'map' => 'yokohama.png'
            ],
            [
                'id' =>6,
                'participant1_name' => 'USA',
                'participant2_name' => 'Tonga',
                'participant1_flag' => 'usa.png',
                'participant2_flag' => 'tonga.png',
                'group' => 'Pool C',
                'cat_B' => 1800,
                'cat_C' => 1400,
                'cat_D' => 1000,
                'venue' => 'Hanazono Rugby Stadium, Higashiosaka City',
                'date' => '2019-09-22 14:15',
                'map' => 'yokohama.png'
            ],
            [
                'id' =>7,
                'participant1_name' => 'Australia',
                'participant2_name' => 'Georgia',
                'participant1_flag' => 'australia.png',
                'participant2_flag' => 'georgia.png',
                'group' => 'Pool D',
                'cat_B' => 1800,
                'cat_C' => 1400,
                'cat_D' => 1000,
                'venue' => 'Shizuoka Stadium Ecopa, Shizuoka Prefecture',
                'date' => '2019-10-11 19:15',
                'map' => 'yokohama.png'
            ],
            [
                'id' =>8,
                'participant1_name' => 'Wales',
                'participant2_name' => 'Uruguay',
                'participant1_flag' => 'wales.png',
                'participant2_flag' => 'uruguay.png',
                'group' => 'Pool D',
                'cat_B' => 1800,
                'cat_C' => 1400,
                'cat_D' => 1000,
                'venue' => 'Kumamoto Stadium, Kumamoto City ',
                'date' => '2019-10-13 17:15',
                'map' => 'yokohama.png'
            ],
            [
                'id' =>9,//ok
                'participant1_name' => 'POOL C',
                'participant2_name' => 'POOL D',
                'participant1_flag' => 'not-known.png',
                'participant2_flag' => 'not-known.png',
                'group' => 'QF 1',
                'cat_B' => 1800,
                'cat_C' => 1400,
                'cat_D' => 1000,
                'venue' => 'Oita Stadium, Oita City',
                'date' => '2019-10-19 16:15',
                'map' => 'yokohama.png'
            ],
            [
                'id' =>10,//ok
                'participant1_name' => 'POOL B',
                'participant2_name' => 'POOL A',
                'participant1_flag' => 'not-known.png',
                'participant2_flag' => 'not-known.png',
                'group' => 'QF 2',
                'cat_B' => 1800,
                'cat_C' => 1400,
                'cat_D' => 1000,
                'venue' => 'Oita Stadium, Oita City',
                'date' => '2019-10-19 19:15',
                'map' => 'yokohama.png'
            ],
            [
                'id' =>11,//ok
                'participant1_name' => 'POOL D2',
                'participant2_name' => 'POOL C2',
                'participant1_flag' => 'not-known.png',
                'participant2_flag' => 'not-known.png',
                'group' => 'QF 3',
                'cat_B' => 1800,
                'cat_C' => 1400,
                'cat_D' => 1000,
                'venue' => 'Oita Stadium, Oita City',
                'date' => '2019-10-20 16:15',
                'map' => 'yokohama.png'
            ],
            [
                'id' =>12,//ok
                'participant1_name' => 'POOL A2',
                'participant2_name' => 'POOL B2',
                'participant1_flag' => 'not-known.png',
                'participant2_flag' => 'not-known.png',
                'group' => 'QF 4',
                'cat_B' => 1800,
                'cat_C' => 1400,
                'cat_D' => 1000,
                'venue' => 'Oita Stadium, Oita City',
                'date' => '2019-10-20 19:15',
                'map' => 'yokohama.png'
            ],
            [
                'id' =>13,//ok
                'participant1_name' => 'quarter final 1',
                'participant2_name' => 'quarter final 2',
                'participant1_flag' => 'not-known.png',
                'participant2_flag' => 'not-known.png',
                'group' => 'SF 1',
                'cat_B' => 3200,
                'cat_C' => 2200,
                'cat_D' => 1800,
                'venue' => 'International Stadium Yokohama, Yokohama City',
                'date' => '2019-10-26 17:00',
                'map' => 'yokohama.png'
            ],
            [
                'id' =>14,//ok
                'participant1_name' => 'quarter final 3',
                'participant2_name' => 'quarter final 4',
                'participant1_flag' => 'not-known.png',
                'participant2_flag' => 'not-known.png',
                'group' => 'SF 2',
                'cat_B' => 3200,
                'cat_C' => 2200,
                'cat_D' => 1800,
                'venue' => 'International Stadium Yokohama, Yokohama City',
                'date' => '2019-10-27 18:00',
                'map' => 'yokohama.png'
            ],
            [
                'id' =>15,//ok
                'participant1_name' => 'final1',
                'participant2_name' => 'final2',
                'participant1_flag' => 'not-known.png',
                'participant2_flag' => 'not-known.png',
                'group' => 'FINAL',
                'cat_B' => 4000,
                'cat_C' => 3000,
                'cat_D' => 2200,
                'venue' => 'International Stadium Yokohama, Yokohama City',
                'date' => '2019-11-02 18:00',
                'map' => 'yokohama.png'
            ]
        ];

        foreach($tickets as $ticket)
        {
            Ticket::create($ticket);
        }
    }
}
