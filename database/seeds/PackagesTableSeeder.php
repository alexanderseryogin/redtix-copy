<?php

use Illuminate\Database\Seeder;
use App\Models\Package;

class PackagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $packages = [
            [
                'id'=> 1,
                'hotel_id' => 1,
                'tickets_id' => '1,3',
                'transportations_id' => '1,2,3,4',
                'code' => 'A',
                'name' => 'Opening Ceremony',
                'total_cost' => 6325,
                'start_date' => '2019-09-19',
                'end_date' => '2019-09-22',
                'margin' => 40
            ],
            [
                'id'=> 2,
                'hotel_id' => 2,
                'tickets_id' => '1,3,2',
                'transportations_id' => '1,2,3,4',
                'code' => 'B',
                'name' => 'Extended Opening Ceremony',
                'total_cost' => 8012,
                'start_date' => '2019-09-19',
                'end_date' => '2019-09-23',
                'margin' => 40
            ],
            [
                'id'=> 3,
                'tickets_id' => '5,9,11',
                'transportations_id' => '1,2,3,4',
                'code' => 'C',
                'name' => 'England’s Route to the Final',
                'total_cost' => 14053,
                'start_date' => '2019-10-11',
                'end_date' => '2019-10-21',
                'margin' => 35
            ],
            [
                'id'=> 4,
                'tickets_id' => '5,9,11,13,14,15',
                'transportations_id' => '1,2,3,4',
                'code' => 'D',
                'name' => 'Quarter-Finals: Oita Stadium',
                'total_cost' => 4071,
                'start_date' => '2019-10-11',
                'end_date' => '2019-11-03',
                'margin' => 35
            ],
            [
                'id'=> 5,
                'hotel_id' => 1,
                'tickets_id' => '10,12,13,14,15',
                'transportations_id' => '1,2,3,4',
                'code' => 'E',
                'name' => 'England to the Quarter-Finals',
                'total_cost' => 5072,
                'start_date' => '2019-10-18',
                'end_date' => '2019-11-03',
                'margin' => 35
            ],
            [
                'id'=> 6,
                'tickets_id' => '13,14,15',
                'transportations_id' => '1,2,3,4',
                'code' => 'F',
                'name' => 'Semi-Finals to the Final',
                'total_cost' => 10764,
                'start_date' => '2019-10-25',
                'end_date' => '2019-11-03',
                'margin' => 35
            ],
            [
                'id'=> 7,
                'hotel_id' => 1,
                'tickets_id' => '13,14',
                'transportations_id' => '1,2,3,4',
                'code' => 'G',
                'name' => 'Quarter-Finals: Tokyo Stadium',
                'total_cost' => 6438,
                'start_date' => '2019-10-25',
                'end_date' => '2019-10-28',
                'margin' => 40
            ],
            [
                'id'=> 8,
                'hotel_id' => 3,
                'tickets_id' => '13,14',
                'transportations_id' => '1,2,3,4',
                'code' => 'H',
                'name' => 'Semi-Finals',
                'total_cost' => 7942,
                'start_date' => '2019-10-18',
                'end_date' => '2019-10-21',
                'margin' => 40
            ],
            [
                'id'=> 9,
                'tickets_id' => '9,11,13,14,15',
                'transportations_id' => '1,2,3,4',
                'code' => 'I',
                'name' => 'Quarter-Finals (Oita Stadium) to the Final',
                'total_cost' => 13041,
                'start_date' => '2019-10-16',
                'end_date' => '2019-11-03',
                'margin' => 35
            ],
            [
                'id'=> 10,
                'tickets_id' => '9,11',
                'transportations_id' => '1,2,3,4',
                'code' => 'J',
                'name' => 'Quarter-Finals (Tokyo Stadium) to the Final',
                'total_cost' => 13041,
                'start_date' => '2019-10-16',
                'end_date' => '2019-10-22',
                'margin' => 35
            ],
        ];

        foreach($packages as $package)
        {
            Package::create($package);
        }
    }
}
