<?php

use Illuminate\Database\Seeder;
use App\Models\Hotel;


class HotelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => 1,
                'hotel_name' => 'Hearton Hotel Higashishinagawa',
                'hotel_cost' => 1780,
                'hotel_description' => 'A 2-minute walk from Rinkai Shinagawa Seaside Station, this polished hotel with a glass-fronted lobby overlooks the Keihin Canal, and is 8 km from the landmark Tokyo Tower.',
                'roomType' => 'Single',
                'address' => '405 Funaya-cho, Nakagyo-Ku, Kyoto-shi, Kyoto, 604-0836, Japan',
                'days' => 4,
                'night' => 3,
                'hotel_img' => 'singapore-2017-club-room-marina-bay.png',
                'venue_description' => 'Tokyo Stadium - 13.8km or 22mins | International Stadium - 26.9km or 37mins',
            ],
            [
                'id' => 2,
                'hotel_name' => 'Hearton Hotel Higashishinagawa',
                'hotel_cost' => 2367,
                'hotel_description' => 'A 2-minute walk from Rinkai Shinagawa Seaside Station, this polished hotel with a glass-fronted lobby overlooks the Keihin Canal, and is 8 km from the landmark Tokyo Tower.',
                'roomType' => 'Single',
                'address' => '405 Funaya-cho, Nakagyo-Ku, Kyoto-shi, Kyoto, 604-0836, Japan',
                'days' => 5,
                'night' => 4,
                'hotel_img' => 'singapore-2017-club-room-marina-bay.png',
                'venue_description' => 'Tokyo Stadium - 13.8km or 22mins | International Stadium - 26.9km or 37mins',
            ],
            [
                'id' => 3,
                'hotel_name' => 'Candeo Hotel Ueno Park',
                'hotel_cost' => 2068,
                'hotel_description' => 'Set in the cultured Ueno district, this relaxed tower hotel is a 3-minute walk from Uguisudani train station and 5 minutes walk from Ueno Park, home to the Tokyo National Museum.',
                'roomType' => 'Single',
                'address' => '1 Chome-2- 1 3 Negishi, Taitō, Tokyo 110-0003, Japan',
                'days' => 4,
                'night' => 3,
                'hotel_img' => 'hotel_candeo.jpg',
                'venue_description' => 'Tokyo Stadium - 9.8km or 27mins',
            ],
        ];

        foreach($data as $item)
        {
            Hotel::create($item);
        }
    }
}
