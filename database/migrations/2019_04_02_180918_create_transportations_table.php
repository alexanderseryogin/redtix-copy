<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransportationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transportations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('package_id')->unsigned();
            $table->integer('city_origin')->unsigned();
            $table->integer('city_destination')->unsigned();
            $table->integer('city_transit')->unsigned();
            $table->integer('cost');
            $table->dateTime('date_origin_1');
            $table->dateTime('date_origin_2');
            $table->dateTime('date_destination_1');
            $table->dateTime('date_destination_2');
            $table->string('origin_number');
            $table->string('destination_number');
            $table->timestamps();
            $table->foreign('package_id')->references('id')->on('packages');
            $table->foreign('city_origin')->references('id')->on('cities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transportations');
    }
}
