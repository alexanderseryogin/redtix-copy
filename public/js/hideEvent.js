$(function() {
    $('div[id^=hideExpired]').each(function() {
        var date = new Date();
        var enddate = $(this).attr('datetime'); 
        if ( Date.parse(date) >= Date.parse(enddate)) {
          $(this).remove();
        } else {
            $(this).addClass('eventLaunced');
        }
    });
});