// {{-- Buy button disable --}}
$(function() {
    $('a[id^=buyButton]').each(function() {
        var date = new Date();
        var enddate = $(this).attr('datetime'); 
        if ( Date.parse(date) >= Date.parse(enddate)) {
          $(this).addClass('disabled');
        }
    });
});
