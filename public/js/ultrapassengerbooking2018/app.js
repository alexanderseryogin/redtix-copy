$(document).ready(function(){
    $('#passenger').on('submit',function (e) {
        e.preventDefault();
        var ele = $(this);
        var btn = ele.find('input[type="submit"]');
        btn.attr('disabled');
        btn.addClass('submit-disabled');
        $.ajax({
            url : ele.attr('action'),
            type : ele.attr('method'),
            data : ConvertFormToJSON(ele),
            success : function (response) {
                if(response.status == 'success'){
                    $('#passenger')[0].reset();
                    $('#thanks').modal('show');
                }
                else if(response.status == 'error'){
                    $('.error-container').html('<div class="passenger-error alert alert-danger ">'+response.message+'</div>');
                    if(response.fields){
                        $.each(response.fields,function (index,value) {
                            var name = "";
                            var field = value.split('.');
                            $.each(field, function( index, value ) {
                                name = index == 0 ? value : name +'['+value+']';
                            });
                            $('input[name="'+name+'"]').addClass('has-error');
                        });
                        $(window).scrollTop($(".error-container").offset().top);

                    }
                }
                btn.removeAttr('disabled');
                btn.removeClass('submit-disabled');
            },
            error: function () {
                btn.removeAttr('disabled');
                btn.removeClass('submit-disabled');
            }
        });

        return false;
    });
    $('#add-passenger').on('click',function(){
        var ele = $(this);
        if(!ele.hasClass('disabled')){
            ele.addClass('disabled');
            $.ajax({
                url : ele.attr('data-url'),
                type : "GET",
                data : {no : $('.p-no').length},
                success : function(response){
                    if(response.status == 'success'){
                        $('#passenger-container').append(response.html);
                        ele.removeClass('disabled');
                    }
                },
                error: function () {
                    ele.removeClass('dsabled')
                }
            });
        }
    });
    $('input.form-control').on('click foucs',function () {
        $(this).removeClass('has-error');
    });
    function ConvertFormToJSON(form){
        var array = form.serializeArray();
        var json = {};

        jQuery.each(array, function() {
            json[this.name] = this.value || '';
        });

        return json;
    }
});