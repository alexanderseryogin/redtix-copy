<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class UltrasingaporeController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public static $validation_messages = [
        'passenger.*.passport_expiry_date.date_format' => 'Please enter valid passport expiry.',
        'passenger.*.date_of_birth.date_format' => 'Please enter valid date of birth.',
        'passenger.*.date_of_birth.before' => 'Please enter valid date of birth.',
        'passenger.*.date_of_birth.after' => 'Please enter valid date of birth.',
        'passenger.*.passport_expiry_date.after' => 'Please enter a valid passport expiry date.',
        'passenger.*.email.email' => 'Please enter a valid email.',
    ];

}
