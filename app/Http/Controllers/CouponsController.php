<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Gloudemans\Shoppingcart\Facades\Cart;

use App\Models\Coupon;

use Illuminate\Http\Request;

class CouponsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    protected function couponWasApplied($cartContents, $couponCode)
    {
        foreach($cartContents as $content)
        {
            if (isset($content->options['coupon'])) {

                if ($content->options['coupon'] == $couponCode)
                {
                    return true;

                }

                return false;
            }

        }

    }

    public function apply(Request $request)
    {

        $this->validate($request, [
            'coupon' => 'required'
        ]);

        $cartItems = Cart::content();

        $coupon = Coupon::where('code', $request->coupon)->where('expires_at', '>', Carbon::now())->first();

        if (! count($coupon))
        {
            return redirect()->back()->withErrors(['coupon' => 'Invalid coupon']);
        }

        foreach ($cartItems as $item) {

            if ($item->id == $coupon->ticket_type_id) {

                if ($item->qty <= $coupon->applicable_for)
                {
                    if ($coupon->is_percentage)
                    {

                        if(!$this->couponWasApplied($cartContents = Cart::content(), $coupon->code))
                        {
                            return redirect()->back()->withErrors(['coupon' => 'Discount was already applied']);
                        }

                        Cart::update($item->rowId, ['price' => (($item->price) - (($coupon->amount / 100) * $item->price)), 'options' => ['coupon' => $coupon->code]]);

                        return redirect()->back()->with('coupon', 'Discount was applied');
                    }

                    if($this->couponWasApplied($cartContents = Cart::content(), $coupon->code))
                    {
                        return redirect()->back()->withErrors(['coupon' => 'Discount was already applied']);
                    }


                    if ($item->price <= $coupon->amount)
                    {
                        return redirect()->back()->withErrors(['coupon' => 'Coupon not applicable']);
                    }

                    Cart::update($item->rowId, ['price' => ($item->price - $coupon->amount), 'options' => ['coupon' => $coupon->code]]);

                    return redirect()->back()->with('coupon', 'Discount was applied');

                }

                return redirect()->back()->withErrors(['coupon' => 'Tickets exceed coupon availability']);

            }
        }

        return $coupon;

        //return Cart::content();

        //$coupon->first()->ticket_type_id;

    }

}
