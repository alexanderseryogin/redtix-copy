<?php

namespace App\Http\Controllers;

use App\Services\Package\PackageService;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    protected $packageService;

    public function __construct(PackageService $packageService)
    {
        $this->packageService = $packageService;
    }

    public function index()
    {
        return view('pages/homepage');
    }

    public function rwc2019()
    {
        $tickets = $this->packageService->getTicketsData();
        $packages = $this->packageService->getAllPackages();

        return view('rugby2019/master', compact('tickets', 'packages'));
    }

}
