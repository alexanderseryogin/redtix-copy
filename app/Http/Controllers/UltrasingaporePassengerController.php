<?php

namespace App\Http\Controllers;

use App\Mail\Passengers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use \App\Models\PassengerBooking;
use \App\Models\PassengerBookingDetail;

class UltrasingaporePassengerController extends UltrasingaporeController
{
    //

    public function login()
    {
        return view('ultrasg18.login');
    }
    public function get_details()
    {
        $purchase = Auth::guard('ultrasingapore')->getUser();
        $booking  = PassengerBooking::where('transaction_number', '=', $purchase->transaction_number)->first();

        $passengerCount = $purchase->total_sale_quantity_sum;

        // If no booking yet, create booking & passengers too.
        if (!$booking) {
            $booking = PassengerBooking::create(['complete_status' => false, 'transaction_number' => $purchase->transaction_number]);
        }

        $isAlreadyCompleted = $booking->complete_status;
        // Do check if booking is done!
        if ($isAlreadyCompleted) {
            // Return new view to say its completed?
        }

        return view('ultrasg18.details', compact('purchase', 'booking', 'passengerCount', 'isAlreadyCompleted'));
    }
        /*
    public function add_passenger(Request $request)
    {
        $p_no = $request->has('no') ? $request->no + 1 : '1';
        $html = view('partials.passenger', compact('p_no'))->render();
        return response()->json(['html' => $html, 'status' => 'success']);
    }*/

    public function store_passengers(Request $request)
    {
        $user = Auth::guard('ultrasingapore')->user();

        // Already completed
        if ($user->booking->complete_status) {
            Auth::guard('ultrasingapore')->logout();
            return redirect()->back();
        }

        $validator = Validator::make($request->all(), [
            'passenger.*.email'                => 'email|required|max:50',
            'passenger.*.fullname'            => 'string|required|max:50',
            'passenger.*.date_of_birth'        => 'date_format:Y-m-d|before:today|after:1900-01-01|required|max:50',
            'passenger.*.passport_no_nric'      => 'string|required|max:50',
            'passenger.*.passport_expiry_date' => 'date_format:Y-m-d|after:today|required|max:50',
            'passenger.*.country_of_issue'           => 'string|required|max:50',
            'passenger.*.nationality'         => 'string|required|max:50',
        ], static::$validation_messages);

        if ($validator->fails()) {
            
            if ($request->ajax()) {
                return response()->json(['message' => $validator->messages()->first(), 'status' => 'error', 'fields' => $validator->errors()->keys()]);
            } else {
                return redirect()->back()->withErrors($validator->messages())->withInput($request->only('passenger'));
            }
        }
        try {

            DB::transaction(function () use ($request, $user) {
                $user->booking->bookingDetails()->createMany($request->passenger);
                $user->booking->update(['complete_status' => true]);
            }, 2);

            $user->fresh('booking.bookingDetails');
            $bookingDetails = $user->booking->bookingDetails;
            Mail::to($user->email)->send(new Passengers(collect($bookingDetails)));

        } catch (\Exception $e) {
            //dd($e);
            dd('There was some error');
            if ($request->ajax()) {
                return response()->json(['message' => 'Something went wrong', 'status' => 'error']);
            } else {
                return redirect()->back()->withErrors(['There was some error.']);
            }

        }
        if ($request->ajax()) {
            return response()->json(['status' => 'success']);
        } else {
            return redirect()->back()->with('bookingStatus', 'success');
        }

    }

}
