<?php

namespace App\Http\Controllers;

use Illuminate\Session\TokenMismatchException;

use App\Events\TicketSold;

use App\Models\Customer;
use App\Models\Order;

use App\Models\Ticket2;
use App\Models\Ticketcode;
use Illuminate\Http\Request;

use Gloudemans\Shoppingcart\Facades\Cart;

use Braintree;
use Illuminate\Support\Facades\Session;

class OrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'first_name' => 'required|min:2',
            'last_name' => 'required|min:2'
        ]);

        $cartTotal = (float)str_replace(',', '', Cart::total());

        $hash = bin2hex(random_bytes(32));

        if ($cartTotal == $request->amount)
        {

            //Make Payment
            $result = Braintree\Transaction::sale([
                'amount' => $request->amount,
                'paymentMethodNonce' => $request->payment_method_nonce,
                'customer' => [
                    'firstName' => $request->first_name,
                    'lastName' => $request->last_name,
                    'email' => $request->email
                ],
                'options' => [
                    'submitForSettlement' => true
                ]
            ]);

            if ($result->success && !is_null($result->transaction)) {

                //Create a customer
                $customer = Customer::where('email','=',$request->email)->first();
                if(!$customer){
                    $customer = Customer::firstOrCreate([
                        'email' => $request->email,
                        'first_name' => $request->first_name,
                        'last_name' => $request->last_name
                    ]);
                }

                //Create an order
                $order = $customer->orders()->create([
                    'hash' => $hash,
                    'customer_id' => $customer->id,
                    'total' => $cartTotal,
                    'paid' => 1
                ]);

                //Take tickets and put in order tickets

                $tickets = $this->getTickets(Cart::content());

                foreach($tickets as $ticket)
                {
                    $order->tickets()->saveMany($ticket);
                }

                $order->tickets()->update(['is_sold' => 1]);

                event(new TicketSold($order, $result));

            } else {

                $errorString = "";

                foreach($result->errors->deepAll() as $error) {
                    $errorString .= 'Error: ' . $error->code . ": " . $error->message . "\n";
                }

                Session::flash('txn_message', $errorString);
                $_SESSION["errors"] = $errorString;
                return redirect('/cart');
            }

        }


        return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    protected function getTickets($cartContent)
    {
        $tickets_type_quantity = [];

        foreach($cartContent as $ticketType)
        {
            $tickets_type_quantity[] = ['ticket_type_id' => $ticketType->id, 'quantity' => $ticketType->qty];
        }

        $tickets = [];

        foreach($tickets_type_quantity as $ticket)
        {
            $tickets[] = Ticketcode::where('ticket_type_id', '=', $ticket['ticket_type_id'])->where('is_sold', '=', 0)->take($ticket['quantity'])->get();
        }

        return $tickets;
    }

}
