<?php

namespace App\Http\Controllers\EventInfoFixtures;

use App\Models\Ticket;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EventInfoFixturesController extends Controller
{
    protected $data;

    public function __construct()
    {
        $this->data =  [
            'match1' => [
                'pool_class' => "a",
                'pool_text' => "POOL A",
                'date' => '20 September 2019, (GMT+9) 19:45',
                'location' => 'Tokyo Stadium, Tokyo',
                'side1_country' => 'JAPAN',
                'side1_flagimg' => 'japan.png',
                'side2_country' => 'RUSSIA',
                'side2_flagimg' => 'russia.png',
            ],

            'match2' => [
                "pool_class" => "a",
                "pool_text" => "POOL A",
                "date" => "22 September 2019, (GMT+9) 16:45",
                "location" => "International Stadium Yokohama, Yokohama City",
                "side1_country" => "IRELAND",
                "side1_flagimg" => "ireland.png",
                "side2_country" => "SCOTLAND",
                "side2_flagimg" => "scotland.png"
            ],

            "match3" => [
                "pool_class" => "b",
                "pool_text" => "POOL B",
                "date" => "21 September 2019, (GMT+9) 18:45",
                "location" => "International Stadium Yokohama, Yokohama City",
                "side1_country" => "NEW ZEALAND",
                "side1_flagimg" => "new-zealand.png",
                "side2_country" => "SOUTH AFRICA",
                "side2_flagimg" => "south-africa.png"

            ],

            "match4" => [
                "pool_class" => "b",
                "pool_text" => "POOL B",
                "date" => "22 September 2019, (GMT+9) 14:15",
                "location" => "Hanazono Rugby Stadium, Higashiosaka City",
                "side1_country" => "ITALY",
                "side1_flagimg" => "italy.png",
                "side2_country" => "NAMIBIA",
                "side2_flagimg" => "namibia.png"
            ],

            "match5" => [
                "pool_class" => "c",
                "pool_text" => "POOL C",
                "date" => "2 October 2019, (GMT+9) 16:45",
                "location" => "Fukuoka Hakatanomori Stadium, Fukuoka City",
                "side1_country" => "FRANCE",
                "side1_flagimg" => "france.png",
                "side2_country" => "USA",
                "side2_flagimg" => "usa.png"

            ],

            "match6" => [
                "pool_class" => "c",
                "pool_text" => "POOL C",
                "date" => "22 September 2019, (GMT+9) 14:15",
                "location" => "Hanazono Rugby Stadium, Higashiosaka City",
                "side1_country" => "USA",
                "side1_flagimg" => "usa.png",
                "side2_country" => "TONGA",
                "side2_flagimg" => "tonga.png"
            ],

            "match7" => [
                "pool_class" => "d",
                "pool_text" => "POOL D",
                "date" => "11 October 2019, (GMT+9) 19:15",
                "location" => "Shizuoka Stadium Ecopa, Shizuoka Prefecture ",
                "side1_country" => "AUSTRALIA",
                "side1_flagimg" => "australia.png",
                "side2_country" => "GEORGIA",
                "side2_flagimg" => "georgia.png"
            ],

            "match8" => [
                "pool_class" => "d",
                "pool_text" => "POOL D",
                "date" => "13 October 2019, (GMT+9) 17:15",
                "location" => "Kumamoto Stadium, Kumamoto City",
                "side1_country" => "WALES",
                "side1_flagimg" => "wales.png",
                "side2_country" => "URUGUAY",
                "side2_flagimg" => "uruguay.png"
            ],

            "match9" => [
                "pool_class" => "not-known",
                "pool_text" => "QF 1",
                "date" => "19 October 2019, (GMT+9) 16:15",
                "location" => "Oita Stadium, Oita City",
                "side1_country" => "POOL C",
                "side1_flagimg" => "not-known.png",
                "side2_country" => "POOL D",
                "side2_flagimg" => "not-known.png"
            ],

            "match10" => [
                "pool_class" => "not-known",
                "pool_text" => "SF 1",
                "date" => "26 October 2019, (GMT+9) 17:00",
                "location" => "International Stadium Yokohama, Yokohama City",
                "side1_country" => "Quarter final",
                "side1_flagimg" => "not-known.png",
                "side2_country" => "Quarter final",
                "side2_flagimg" => "not-known.png"
            ],

            "match11" => [
                "pool_class" => "not-known",
                "pool_text" => "Final",
                "date" => "20 September 2019, (GMT+9) 19:45",
                "location" => "International Stadium Yokohama, Yokohama City",
                "side1_country" => "Final",
                "side1_flagimg" => "not-known.png",
                "side2_country" => "Final",
                "side2_flagimg" => "not-known.png",
        ]

        ];
    }

    public function getMatches()
    {
        return Ticket::all();
    }

}
