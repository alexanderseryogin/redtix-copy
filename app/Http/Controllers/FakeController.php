<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Models\Package;
use App\Services\Package\PackageService;
use Doctrine\DBAL\Schema\Index;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use App\Services\CognitixAPI;
use App\Services\Package\IPackageService;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\Input;


class FakeController extends Controller
{

    private $package;
    private $cognitixAPI;

    private $data;

    public function __construct(CognitixAPI $api, IPackageService $IPackageService)
    {
        $this->package = $IPackageService;
        $this->cognitixAPI = $api;
        $this->data = [
            'packages_description' => [
                'a' => [
                    'day1' => [
                        'description' => 'The journey begins. Time to check-in for your international flight to Japan.'
                    ],
                    'day2' => [
                        'description' => [
                            1 => 'Explore Tokyo at your own leisure. ',
                            2 => 'It’s the moment you’ve been waiting for! This evening, the opening ceremony
                                    ofRugby World Cup Japan takes place at Tokyo stadium. Followed by theopening
                                    game of Rugby World Cup - Host nation Japan take on Russia. Theatmosphere for
                                    this game will be both unique and electric. ',
                            3 => 'It’s all taking place at Tokyo Stadium (Ajinomoto Stadium).'
                        ],
                    ],
                    'day3' => [
                        'description' => [
                            1 => 'Explore Tokyo at your own leisure. Keep in mind, the hotel to InternationalStadium
                            Yokohama (Nissan Stadium) is a 55-minute train journey',
                            2 => 'Today’s big match-up will be taking place at International Stadium Yokohama
                            (Nissan Stadium)',
                            3 => 'It’s the favorites, New Zealand up against the two-time Rugby World Cupwinners, South
                            Africa'
                        ]
                    ],
                    'day4' => [
                        'description' => [
                            1 => 'All great things come to an end, it’s your final day in Tokyo. Feel free to exploreTokyo at
                            your own leisure <br> The journey begins. Time to check-in for your international flight to Thailand',
                            ''
                        ]
                    ],
                ],
                'b' => [
                    'day1' => [
                        'description' => 'B The journey begins. <br> Time to check-in for your international flight to Japan.'
                    ],
                    'day2' => [
                        'description' => 'B Explore Tokyo at your own leisure. <br> It’s the moment you’ve been waiting for! This evening, the opening ceremony
                                    ofRugby World Cup Japan takes place at Tokyo stadium. Followed by theopening
                                    game of Rugby World Cup - Host nation Japan take on Russia. Theatmosphere for
                                    this game will be both unique and electric. <br> It’s all taking place at Tokyo Stadium (Ajinomoto Stadium).'
                    ],
                    'day3' => [
                        'description' => 'B Explore Tokyo at your own leisure. Keep in mind, the hotel to InternationalStadium
                            Yokohama (Nissan Stadium) is a 55-minute train journey. <br> Today’s big match-up will be taking place at International Stadium Yokohama
                            (Nissan Stadium). <br> It’s the favorites, New Zealand up against the two-time Rugby World Cupwinners, South
                            Africa'
                    ],
                    'day4' => [
                        'description' => [
                            'B All great things come to an end, it’s your final day in Tokyo. Feel free to exploreTokyo at
                            your own leisure. <br> The journey begins. Time to check-in for your international flight to Thailand',
                        ]
                    ],
                ]
            ],
            'airports' => [
                'bangkok' => [
                    'title' => 'Bangkok Airport',
                    'description' => 'Suvarnabhumi Airport, also known unofficially as Bangkok Airport,[5][6] is one of two international airports serving Bangkok, Thailand. The other older one is Don Mueang International Airport.[7][8] Suvarnabhumi covers an area of 3,240 ha (32.4 km2; 8,000 acres), making it one of the biggest international airports in Southeast Asia and a regional hub for aviation.'
                ],
                'jakarta' => [
                    'title' => 'Soekarno–Hatta International Airport',
                    'description' => "Soekarno–Hatta International Airport is the primary airport serving the Greater Jakarta area on the island of Java in Indonesia."
                ],
                'kuala_lumpur' => [
                    'title' => 'Kuala Lumpur International Airport (KLIA)',
                    'description' => 'Kuala Lumpur International Airport is Malaysia\'s main international airport and one of the major airports in Southeast Asia and worldwide. It is located in Sepang District of Selangor, approximately 45 kilometres (28 mi) south of Kuala Lumpur city centre and serves the Greater Klang Valley conurbation.

KLIA is the largest and busiest airport in Malaysia. In 2018, it handled 59,988,409 passengers and 714,669 tonnes of cargo. It is the world\'s 23rd-busiest airport by total passenger traffic.'
                ],
                'singapore' => [
                    'title' => 'Changi Airport',
                    'description' => 'Changi Airport is the major civilian airport for Singapore, and one of the largest transportation hubs in Southeast Asia. It is currently rated the World\'s Best Airport by Skytrax,[5] for the seventh consecutive year since 2013.[6] It is also the first Airport in the world to do so for seven consecutive years and is one of the world\'s busiest airports by international passenger and cargo traffic. The airport is located in Changi, at the eastern end of Singapore, approximately 20 kilometres northeast[7] from Marina Bay (Singapore\'s Downtown Core), on a 13-square-kilometre (5.0 sq mi) site. It is operated by Changi Airport Group and it is the home base of Singapore Airlines, Singapore Airlines Cargo, SilkAir, Scoot, Jetstar Asia Airways and BOC Aviation.'
                ]
            ]

        ];
    }


    public function fakePdf($package, $selectedCityOfDeparture)
    {

        $packages = $this->package->getDetailPackageByPack($package);
//        dd($packages);


//        $cities=$this->package->getFlightsByPackage();
//        $packInfo=$this->package->getPackageGeneralInfoByPack($package);
//        dd($packInfo);


//        $packageInfo=[
//            'name'=>$package,
//            'flightNames'=>$cities,
//            'dt_start'=>$packInfo->start_date,
//            'dt_end'=>$packInfo->end_date,
//            'title'=>$packInfo->name,
//            'hotel_id'=>$packInfo->hotel_id,
//        ];

        $selectedCityOfDeparture = strtolower($selectedCityOfDeparture);

        $airport['title'] = $this->data['airports'][$selectedCityOfDeparture]['title'];
        $airport['description'] = $this->data['airports'][$selectedCityOfDeparture]['description'];
        $packageDetailByPack = $this->package->getDetailPackageByPack($package);

        $pdf = PDF::loadView('fake-example-text', [
            'packages' => $packages,
            'selectedCityOfDeparture' => $selectedCityOfDeparture,
//            'packInfo' => $packInfo,
//            'packageInfo' => $packageInfo,
            'packageDetailByPack' => $packageDetailByPack,
            'data' => $this->data,
            'airport' => $airport
        ]);


        return @$pdf->stream('fake-example-text');
    }

    public function htmlForPdf($package, $selectedCityOfDeparture)
    {

        $packages = $this->package->getDetailPackageByPack($package);
//        dd($packages);

        $airportOfSelectedCity = City::where('name', $selectedCityOfDeparture)->get();
//        dd($airportOfSelectedCity);

        $cities=$this->package->getFlightsByPackage();
        $packInfo=$this->package->getPackageGeneralInfoByPack($package);
        $packageInfo=[
            'name'=>$package,
            'flightNames'=>$cities,
            'dt_start'=>$packInfo->start_date,
            'dt_end'=>$packInfo->end_date,
            'title'=>$packInfo->name,
            'hotel_id'=>$packInfo->hotel_id,
        ];
        $packInfo = $this->package->getPackageGeneralInfoByPack($package);

        $selectedCityOfDeparture = strtolower($selectedCityOfDeparture);

        $airport['title'] = $this->data['airports'][$selectedCityOfDeparture]['title'];
        $airport['description'] = $this->data['airports'][$selectedCityOfDeparture]['description'];
        $packageDetailByPack = $this->package->getDetailPackageByPack($package);


        return view('fake-example-text', [
            'packages' => $packages,
            'selectedCityOfDeparture' => $selectedCityOfDeparture,
            'airportOfSelectedCity' => $airportOfSelectedCity,
            'packInfo' => $packInfo,
            'packageInfo' => $packageInfo,
            'packageDetailByPack' => $packageDetailByPack,
            'data' => $this->data,
            'airport' => $airport
        ]);
    }

}
