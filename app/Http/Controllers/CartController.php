<?php

namespace App\Http\Controllers;

use Gloudemans\Shoppingcart\Facades\Cart;

use Gloudemans\Shoppingcart\Exceptions\InvalidRowIDException;

use Illuminate\Http\Request;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if (count(Cart::content()))
        {
            $data['cartitems'] = Cart::content();

            $data['count'] = 0;
            $data['total'] = 0;

            foreach ($data['cartitems'] as $key => $ticket) {
                $data['count'] += $ticket->qty;
                $data['total'] += number_format((float)($ticket->qty * $ticket->price), 2, '.', '');
            }

            return view('pages/checkout', $data);
        }

        return redirect('/');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ticket = $request->all();

        $cart = Cart::add(['id' => $ticket['id'], 'name' => $ticket['title'], 'qty' => 1, 'price' => $ticket['price']])->associate('Ticket');


    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $cart = Cart::content();
        $ticket = '';

        foreach ($cart as $key => $tick) {

            if ($tick->id == $request['id']) {

                $ticket = $tick;

            }

        }

        Cart::update($ticket->rowId, $ticket->qty - 1);


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getCart()
    {
        return Cart::content();
    }

    public function getTotal()
    {
        return Cart::total();
    }

    public function remove($id)
    {

        try {

            Cart::remove($id);

        }
        catch(InvalidRowIDException $e)
        {

            return redirect()->back();

        }

        return redirect()->back();
    }

    public function getTicket($id)
    {
        $cart = Cart::content();
        $ticket = '';

        foreach($cart as $key => $tick){
            if($tick->id==$id){
                $ticket = $tick;
            }
        }

        return json_encode($ticket);
    }

    public function generatepdf(Request $request){
        return view('emails.orders.tickets');
    }

}
