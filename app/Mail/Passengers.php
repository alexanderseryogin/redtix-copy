<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Collection;

class Passengers extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $passengers;
    public $user;
    public $fullname;
    public $transactionNumber;

    public function __construct(Collection $passengers)
    {
        //
        $this->passengers = $passengers;
        $this->user = \Auth::guard('ultrasingapore')->user();
        $this->fullname = $this->user->full_name;
        $this->transactionNumber = $this->user->formattedTransactionNumber;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('AirAsia RedTix - Ultra Singapore 2018: Passenger details for flights confirmation')
            ->view('emails.passengersbooking.details');
    }
}
