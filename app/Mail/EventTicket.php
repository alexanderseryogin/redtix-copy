<?php

namespace App\Mail;

use App\Models\Concert;
use App\Models\Order;
use App\Models\Ticket2;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EventTicket extends Mailable
{
    use Queueable, SerializesModels;

    public $order;
    public $payment;
    public $user;
    public $tickets;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
        $this->user = $order->customer()->firstOrFail();
        $this->tickets = $order->tickets()->get();
        $this->payment = $order->payment()->firstOrFail();
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        $data = [
            'user'=>$this->user,
            'order'=>$this->order,
            'payment' =>$this->payment
        ];

        $mail = $this->view('emails.orders.tickets');

        foreach($this->tickets as $key => $ticketcode){
            $ticket = Ticket2::where('id','=',$ticketcode->ticket_type_id)->firstOrFail();
            $data['ticket'] = $ticket;
            $data['ticketcode'] = $ticketcode;
            $data['concert'] = Concert::where('id','=',$ticket->concert_id)->firstOrFail();

            $pdf = PDF::loadView('emails.orders.generate', $data);
            $pdf->setOptions([
                'isRemoteEnabled'=>true
            ]);
            $mail->attachData($pdf->output(), 'Tickets_'.$key.'.pdf', [
                'mime' => 'application/pdf',
            ]);
        }

        return $mail;
    }
}
