<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\TicketSold' => [
            'App\Listeners\UpdateTicketStock',
            'App\Listeners\RecordSuccessfulPayment',
            'App\Listeners\EmptyShoppingCart',
            'App\Listeners\EmailPaymentConfirmation',
            'App\Listeners\SendPdfTicket',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
