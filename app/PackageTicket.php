<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackageTicket extends Model
{
    protected $table = 'package_ticket';
}
