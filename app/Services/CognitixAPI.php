<?php

namespace App\Services;

use GuzzleHttp\Client;

class CognitixAPI
{
    const API_KEY='5e709b659900d327e65d38c7743e26a70c386362';
    const DEV_URL='http://staging.gapoora.com/api/v2/checkout/no-variants';

    protected $client;
    protected $event_code='';

    public function __construct(Client $client)
    {
        $this->client = $client;
    }
    public function sendRequestToCognitix($data)
    {
        $data=$this->getData4Send($data);
//        $data=json_encode($data);
//        dd($data);
        $data=["event_code"=>"S47GRW",
            "customer_fullname"=>"Edwin+Cheang",
            "customer_email"=>"edwinwk@bakehouse.id",
            "customer_phone"=>"12321321",
            "customer_data"=>["country"=>"Malaysia"],
            "items"=>[
                ["item_id"=>1,"qty"=>1,"total_price"=>10,"hide_price"=>false],
                ["item_id"=>2,"qty"=>2,"total_price"=>22,"hide_price"=>true]],
            "booking_id"=>"XYZ123",
            "booking_desc"=>"Description of the booking"];
        $response = $this->client->post(CognitixAPI::DEV_URL.'?xauth='.CognitixAPI::API_KEY,
            [
                \GuzzleHttp\RequestOptions::JSON => $data
            ]);
//        dd($response);
        return $response;
    }
    protected function getData4Send($data)
    {
        $data=json_decode($data,true);
        if(empty($data['packageName']))
            throw new \Exception('packageName not found!');
        if(empty($data['flight']))
            throw new \Exception('flight not found!');
        if(empty($data['tickets']))
            throw new \Exception('tickets not found!');
        if(empty($data['travelers']))
            throw new \Exception('travelers not found!');
        if(empty($data['rooms']))
            throw new \Exception('rooms not found!');

        $res=['event_code'=>'S47GRW',
            'customer_fullname'=>'Artem',
            'customer_email'=>'artem.agafonov@faceit-team.com',
            'customer_phone'=>'+380506202605',
            'booking_id'=>str_random(6),
            'booking_desc'=>'Package name is '.$data['packageName'],
            ];
        $res['items']=array();
        $itemId=1;
        //todo get flight info from DB
        $flight=['cost'=>3];
        array_push($res['items'],["item_id"=>$itemId++,"qty"=>$data['travelers'],"total_price"=>$flight['cost']*$data['travelers']]);
        //hotel
        if(!empty($data['hotel'])) {
            //todo get hotel info
            $hotel=['cost'=>1];
            array_push($res['items'],["item_id"=>$itemId++,"qty"=>$data['rooms'],"total_price"=>$hotel['cost']*$data['rooms']]);
        }
        //tickets
        /*foreach ($data['tickets'] as $ticket) {
            //todo get ticketinfo by id
            $info=['cost'=>$ticket['price']];
            array_push($res['items'],["item_id"=>$itemId++,"qty"=>1,"total_price"=>$info['cost']]);
        }*/

        return $res;
    }
}
