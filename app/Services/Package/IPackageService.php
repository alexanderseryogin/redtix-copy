<?php

namespace App\Services\Package;

interface IPackageService
{
    public function getAllPackages();

    public function getPackagesById($id);

    public function getAllTickets();

    public function getTicketsById($id);

    public function getAllCities();

    public function getCitiesById($id);

    public function getAllHotels();

    public function getHotelsById($id);

    public function getAllTransportations();

    public function getTransportationsById($id);

    public function getPackageInfoByPackageAndFlight($package, $fight);

    public function getFlightsByPackage();

    public function getDetailPackageByPack($package);

    public function getTicketsData();

    public function getPackageGeneralInfoByPack($package);

}