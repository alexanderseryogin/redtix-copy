<?php

namespace App\Services\Package;
use App\Models\{Package,Ticket, City, Hotel, HotelsFeatures, Transportation};

class PackageService implements IPackageService
{
    private $package;
    private $ticket;
    private $city;
    private $hotel;
    private $transportation;


    public function __construct(Package $package,
                                Ticket $ticket,
                                City $city,
                                Hotel $hotel,
                                Transportation $transportation)
    {
        $this->package = $package;
        $this->ticket = $ticket;
        $this->city = $city;
        $this->hotel = $hotel;;
        $this->transportation = $transportation;
    }

    public function getAllPackages()
    {
        $data = Package::all(['hotel_id', 'tickets_id', 'code', 'name', 'total_cost', 'start_date', 'end_date']);
        $res = [];
        $size = 1;
        $sizes=[1,2,3,2,1,3,1,2,3,3,3,3,3,3];
        for ($i = 0; $i < count($data); $i++) {
            $res[$i]['size'] = $sizes[$i];//($size >=3) ? 1 : $size;
            $res[$i]['type'] = $data[$i]['code'];
            $res[$i]['dt'] = $data[$i]['start_date'];
            $res[$i]['dt2'] = $data[$i]['end_date'];
            $res[$i]['features'] = $this->getSummariesFromPackage($data[$i]['hotel_id'], $data[$i]['tickets_id']);
            $res[$i]['price'] = $data[$i]['total_cost'];
            $size++;
        }

        return $res;
    }

    private function getSummariesFromPackage($hotel_id, $tickets_id){
        $i=0;
        $data[$i++] = [
            'icon' => 'airplane',
            'title' => 'Return Flight from Selected Cities'
        ];

        try {
            if ($hotel_id) {
                $hotels = Hotel::findOrFail($hotel_id)->get();
                $data[$i++] = [
                    'icon' => 'bed',
                    'title' => $hotels[0]['days'] . ' Days ' . $hotels[0]['night'] . ' Nights '// . $hotels[0]['hotel_name']
                ];
            }

            if ($tickets_id) {
                $tickets = Ticket::whereIn('id', explode(',', $tickets_id))->get();
                for ($k = 0; $k < count($tickets); $k++) {
                    $data[$i++] = [
                        'icon' => 'ticket',
                        'title' => $tickets[$k]['participant1_name'] . ' V ' . $tickets[$k]['participant2_name']
                    ];
                }
            }
        }catch (\Throwable $e){
            throw new \Exception($e->getMessage());
        }

        $data[$i] = [
            'icon' => 'support',
            'title' => 'Dedicated RedTix Online Support'
        ];

        return $data;
    }

    public function getPackagesById($id)
    {
        return $this->package->where('id', $id)->firstOrFail();
    }

    public function getAllCities()
    {
        return City::all();
    }

    public function getCitiesById($id)
    {
        return $this->city->where('id', $id)->firstOrFail();
    }

    public function getAllHotels()
    {
        return Hotel::all();
    }

    public function getHotelsById($id)
    {
        return $this->hotel->where('id', $id)->firstOrFail();
    }

    public function getAllTickets()
    {
        return Ticket::all()
            ->map(function ($data) {
                return collect($data->toArray())
                    ->only(['participant1_name',
                        'participant2_name',
                        'participant1_flag',
                        'participant2_flag',
                        'group',
                        'cat_B',
                        'cat_C',
                        'cat_D',
                        'venue',
                        'date',
                        'map',
                        'hidden']
                    )->all();
            });
    }

    public function getTicketsById($id)
    {
        return $this->ticket->where('id', $id)->firstOrFail();
    }

    public function getAllTransportations()
    {
        return Transportation::all();
    }

    public function getTransportationsById($id)
    {
        //return $this->transportation->where('id', $id)->firstOrFail();
    }

    public function getPackageInfoByPackageAndFlight($package, $flight)
    {
        try {
//            $data_package = $this->package
//                ->select(['tickets_id', 'name', 'code', 'hotel_id', 'start_date', 'end_date', 'total_cost'])
//                ->where('id', $this->getPack($package))
//                ->get();
//                ->toArray();
            $data_package = $this->package->whereCode($package)->firstOrFail();
            //flight
            $data_flight = Transportation::whereIn('id',explode(',',$data_package['transportations_id']))
                ->whereCity_origin($flight)->firstOrFail();

//            $data_flight = Package::find($this->getPack($package))
//                ->transportation
//                ->where('city_origin', $this->getFlight($flight))
//                ->toArray();

            //add to flight city detail
        $data_flight['city_destination']=$this->getCityFromQuery($data_flight['city_destination']);
        $data_flight['city_origin']=$this->getCityFromQuery($data_flight['city_origin']);
        $data_flight['city_transit']=$this->getCityFromQuery($data_flight['city_transit']);

//            for ($i = 0; $i < count($data_flight); $i++) {
//                $data_flight[$i] = $this->addCityDetail($data_flight[$i],
//                    $data_flight[$i]['city_destination'],
//                    $data_flight[$i]['city_transit'],
//                    $data_flight[$i]['city_origin']
//                );
//            }

            //hotel
//            $data_hotel = $this->hotel
//                ->select(['hotel_name', 'hotel_description', 'hotel_cost', 'roomType', 'address', 'days', 'night', 'hotel_img'])
//                ->where('id', $data_package[0]['hotel_id'])
//                ->get()
//                ->toArray();
            if($data_package['hotel_id']) {
                $data_hotel = $this->hotel->findOrFail($data_package['hotel_id']);
                $data_hotel['feature'] = $this->addHotelFeatures($data_package['hotel_id']);
            }else{
                $data_hotel = null;
            }
//        dd($data_hotel);


            $packageInfo['name'] = $data_package['code'];
            $packageInfo['title'] = $data_package['name'];
            $packageInfo['flight'] = $data_flight;
            $packageInfo['hotel'] = $data_hotel;
//            $data_hotel['features'] = $this->addHotelFeatures($data_package['hotel_id']);
            $packageInfo['tickets'] = $this->getFlightTickets($data_package['tickets_id']);
            $packageInfo['start_date'] = $data_package['start_date'];
            $packageInfo['end_date'] = $data_package['end_date'];
            $packageInfo['total_cost'] = $data_package['total_cost'];
            $packageInfo['margin'] = $data_package['margin'];

//            $packageInfo['name'] = $data_package[0]['code'];
//            $packageInfo['title'] = $data_package[0]['name'];
//            $packageInfo['flight'] = $data_flight;
//            $packageInfo['hotel'] = $data_hotel;
//            $data_hotel['features'] = $this->addHotelFeatures($data_package[0]['hotel_id']);
//            $packageInfo['tickets'] = $this->getFlightTickets($data_package[0]['tickets_id']);
//            $packageInfo['start_date'] = $data_package[0]['start_date'];
//            $packageInfo['end_date'] = $data_package[0]['end_date'];
//            $packageInfo['total_cost'] = $data_package[0]['total_cost'];

        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            throw new \Exception($e->getMessage().' not exist in database.');
        }

        return $packageInfo;
    }

    public function getFlightsByPackage()
    {
        $data = [];
//        try {
            $flights = $this->city
                ->select(['name', 'abbr', 'id'])
                ->where('origin', '=', 1)
                ->get()
                ->toArray();

            foreach ($flights as $key => $val) {
                $data[$val['id']] = $val['name'].' ('.$val['abbr'].')';
            }
//        } catch (\Throwable $e) {
//            return false;
//        }

        return $data;
    }

    public function getPackageGeneralInfoByPack($package)
    {
        $data_package = $this->package
            ->where('code', $package)
            ->first();
        return $data_package;

    }
    public function getDetailPackageByPack($package)
    {
        try {
            $data_package = $this->package
                ->select(['tickets_id', 'name', 'code', 'hotel_id', 'start_date', 'end_date', 'total_cost','transportations_id'])
                ->where('id', $this->getPack($package))
                ->get()
                ->toArray();

            //flight
//            $data_flight = Package::findOrFail($this->getPack($package))
//                ->transportation
//                ->toArray();
            $data_flight = Transportation::whereIn('id',explode(',',$data_package[0]['transportations_id']))->get()->toArray();


            //add to flight city detail
            for ($i = 0; $i < count($data_flight); $i++) {
                $data_flight[$i] = $this->addCityDetail($data_flight[$i],
                    $data_flight[$i]['city_destination'],
                    $data_flight[$i]['city_transit'],
                    $data_flight[$i]['city_origin']
                );
            }

            if($data_package[0]['hotel_id']) {
                //hotel
                $data_hotel = ($this->hotel
                    ->select(['hotel_name', 'hotel_description', 'hotel_cost', 'roomType', 'address', 'days', 'night', 'hotel_img'])
                    ->where('id', $data_package[0]['hotel_id'])
                    ->get()
                    ->toArray());

                $data_hotel[0] += ['feature' => $this->addHotelFeatures($data_package[0]['hotel_id'])];
            }else{
                $data_hotel = null;
            }

            $packageInfo['name'] = $data_package[0]['code'];
            $packageInfo['title'] = $data_package[0]['name'];
            $packageInfo['flight'] = $data_flight;
            $packageInfo['hotel'] = $data_hotel;
            $packageInfo['tickets'] = $this->getFlightTickets($data_package[0]['tickets_id']);
            $packageInfo['map'] = $packageInfo['tickets'][0]['map'];
            $packageInfo['start_date'] = $data_package[0]['start_date'];
            $packageInfo['end_date'] = $data_package[0]['end_date'];
            $packageInfo['total_cost'] = $data_package[0]['total_cost'];

        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            throw new \Exception('Package with name '.$package.' not exist in database.');
        }

        return $packageInfo;
    }

    //TODO move to service helper
    private function getPack($pack)
    {
        $packs = [
            'A' => 1,
            'B' => 2,
            'C' => 3,
            'D' => 4,
            'E' => 5,
            'F' => 6,
            'G' => 7,
            'H' => 8,
            'I' => 9,
            'J' => 10
        ];

        return $packs[$pack];
    }

    private function getFlight($fight){
        $fights = [
            'KUL' => 1,
            'DMK' => 2,
            'SIN' => 3,
            'CGK' => 4
        ];

        return $fights[$fight];
    }

    private function addCityDetail($arr, $id_destination, $id_transit, $id_origin){
        foreach ($arr as $k => $v){
            if ($k == 'city_destination')
                $arr[$k] = $this->getCityFromQuery($id_destination);
            elseif ($k == 'city_transit' )
                $arr[$k] = $this->getCityFromQuery($id_transit);
            elseif ($k == 'city_origin' )
                $arr[$k] = $this->getCityFromQuery($id_origin);
        }

        return $arr;
    }

    private function addHotelFeatures($hotel_id){
        return Hotel::findOrFail($hotel_id)
            ->features
            ->toArray();
    }

    private function getCustomCities()
    {
        $res = [];
        $data = City::all()
            ->map(function ($data) {
                return collect($data->toArray())
                    ->only(['id', 'name', 'abbr', 'airport', 'img'])
                    ->all();
            });
        foreach ($data as $k=>$v){
            $res[$v['id']] = $v;
        }

        return $res;
    }

    private function getCityFromQuery($id){
        if($id) {
            $data = $this->getCustomCities();
            return $data[$id];
        }else{
            return null;
        }
    }

    private function getFlightTickets($data_package)
    {
        return $this->ticket->select()
            ->whereIn('id', explode(',', $data_package))
            ->get()
            ->toArray();
    }

    public function getTicketsData()
    {
        $tickets = Ticket::all();
        $ticketsData = [];
        $groups = [];
        $participant1_names = [];
        $participant2_names = [];
        $cities = [];

        // countries (teams)
        foreach ($tickets as $ticket) {
            $participant1_names[] = $ticket['participant1_name'];
        }
        foreach ($tickets as $ticket) {
            $participant2_names[] = $ticket['participant2_name'];
        }
        $countries = array_unique(array_merge($participant1_names, $participant2_names));

        // info for tickets rendering
        $i = 0;
        foreach ($tickets as $ticket) {
            $countriesData[$i]['participant1_name'] = $ticket['participant1_name'];
            $countriesData[$i]['participant2_name'] = $ticket['participant2_name'];
            $countriesData[$i]['participant1_flag'] = $ticket['participant1_flag'];
            $countriesData[$i]['participant2_flag'] = $ticket['participant2_flag'];
            $countriesData[$i]['venue'] = $ticket['venue'];
            $countriesData[$i]['date'] = $ticket['date'];
            $countriesData[$i]['group'] = $ticket['group'];
            $i++;
        }

        // cities
        foreach ($tickets as $ticket) {
            $venueValue = explode(',', $ticket['venue']);
            $cities[] = trim($venueValue[1]);
        }

        // groups
        foreach ($tickets as $ticket) {
            $groups[] = $ticket['group'];
        }

        $ticketsData['countries'] = $countries;
        $ticketsData['cities'] = $cities;
        $ticketsData['groups'] = $groups;
        $ticketsData['countriesData'] = $countriesData;

        return $ticketsData;
    }

}