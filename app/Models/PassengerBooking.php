<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

class PassengerBooking extends Authenticatable
{

    protected $guarded = ['id'];

    protected $guard = 'ultrasingapore';

    public function bookingDetails(){
        return $this->hasMany(PassengerBookingDetail::class);
    }
    //
    public function getRememberTokenName()
    {
        return null; // not supported
    }

    /**
     * Overrides the method to ignore the remember token.
     */
    public function setAttribute($key, $value)
    {
        $isRememberTokenAttribute = $key == $this->getRememberTokenName();
        if (!$isRememberTokenAttribute)
        {
            parent::setAttribute($key, $value);
        }
    }
}
