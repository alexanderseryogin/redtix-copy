<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

class FlightTicketPurchase extends Authenticatable
{
    protected $table = 'flight_ticket_purchase';
    
    protected $guarded = ['id'];

    protected $guard = 'ultrasingapore';

    public $timestamps = true;

    public function booking() {
        return $this->hasOne(PassengerBooking::class, 'transaction_number', 'transaction_number');
    }

    public function getFormattedTransactionNumberAttribute() {
        return '#UMF2018-' . str_pad($this->transaction_number, 6, '0', STR_PAD_LEFT);
    }

    //
    public function getRememberTokenName()
    {
        return null; // not supported
    }

    /**
     * Overrides the method to ignore the remember token.
     */
    public function setAttribute($key, $value)
    {
        $isRememberTokenAttribute = $key == $this->getRememberTokenName();
        if (!$isRememberTokenAttribute)
        {
            parent::setAttribute($key, $value);
        }
    }
}
