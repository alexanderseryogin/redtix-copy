<?php

namespace App\Models;

use App\Exceptions\NotEnoughTicketsException;

use Illuminate\Database\Eloquent\Model;

class Concert extends Model
{
    protected $guarded = [];

    protected $dates = ['date'];

    public function getFormattedDateAttribute()
    {
        return $this->date->format('F d, Y');
    }

    public function getFormattedTimeAttribute()
    {
        return $this->date->format('g:i a');
    }

    public function getTicketPriceInRinggitAttribute()
    {
        return number_format($this->ticket_price/100, 2);
    }

    public function scopePublished($query)
    {
        return $query->whereNotNull('published_at');
    }

//    public function orders()
//    {
//        return $this->hasMany(Order::class);
//    }

    public function tickets()
    {
        return $this->hasMany(Ticket2::class);
    }

//    public function addTickets($quantity)
//    {
//        foreach(range(1, $quantity) as $i)
//        {
//            $this->tickets()->create([]);
//        }
//    }

//    public function ticketsRemaining()
//    {
//        return $this->tickets()->available()->count();
//    }

//    public function orderTickets($email, $ticketQuantity)
//    {
//
//        $tickets = $this->tickets()->available()->take($ticketQuantity)->get();
//
//        if ($tickets->count() < $ticketQuantity)
//        {
//            throw new NotEnoughTicketsException;
//        }
//
//        $order = $this->orders()->create(['email' => $email]);
//
//        foreach($tickets as $ticket)
//        {
//            $order->tickets()->save($ticket);
//        }
//
//        return $order;
//    }

}
