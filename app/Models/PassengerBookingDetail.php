<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class PassengerBookingDetail extends Model
{
    //
    protected $guarded = ['id'];
    public $dates = ['date_of_birth','passport_expiry_date'];

    public function customer(){
        return $this->belongsTo(PassengerBooking::class);
    }
}
