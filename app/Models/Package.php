<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function transportation()
    {
        return $this->hasMany(Transportation::class)->select([
            'city_origin',
            'city_destination',
            'city_transit',
            'date_origin_1',
            'date_origin_2',
            'date_destination_1',
            'date_destination_2',
            'origin_number',
            'destination_number',
            'cost',
        ]);
    }

    public function ticket()
    {
        return $this->hasMany(Ticket::class, 'id', 'id');
    }

    public function city()
    {
        return $this->hasMany(City::class, 'id', 'id');
    }




}
