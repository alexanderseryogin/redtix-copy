<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ticket2 extends Model
{

    protected $guarded = [];

    public function scopeAvailable($query)
    {
        return $query->whereNull('order_id');
    }

    public function concert()
    {
        return $this->belongsTo(Concert::class);
    }


}
