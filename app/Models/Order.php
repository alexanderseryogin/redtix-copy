<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $guarded = [];

    public function tickets()
    {
        return $this->belongsToMany(Ticketcode::class, 'orders_tickets');
    }

    public function cancel()
    {
        foreach($this->tickets as $ticket)
        {
            $ticket->update(['order_id' => null]);
        }

        $this->delete();

    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function payment()
    {
        return $this->hasOne(Payment::class);
    }


}
