<?php

namespace App\Listeners;

use App\Events\TicketSold;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Gloudemans\Shoppingcart\Facades\Cart;

class EmptyShoppingCart
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  TicketSold  $event
     * @return void
     */
    public function handle(TicketSold $event)
    {
        Cart::destroy();
    }
}
