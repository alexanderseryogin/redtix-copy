<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
use Illuminate\Http\Request;
use Illuminate\Session\Store;
use App\Mail\TicketDetail;


// Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('/', ['uses' => 'PagesController@index', 'as' => 'homepage']);

Route::get('/about', function () {
    return view('pages/about');
});

Route::get('/faq', function () {
    return view('pages/faq');
});

Route::get('/privacy', function () {
    return view('pages/privacy');
});

Route::get('/purchaseterms', function () {
    return view('pages/purchaseterms');
});

Route::get('/websiteterms', function () {
    return view('pages/webterms');
});

Route::get('/redicons', function () {
    return view('pages/redicon');
});

// Route::get('/events', function () {
//     return view('pages/events');
// });

// Route::get('/checkout', function () {
//     return view('pages/checkout');
// });

// Events Page Static
Route::prefix('rwc2019')->group(function () {
    Route::get('/', 'PagesController@rwc2019')->name('rwc2019.index');
    Route::get('/packagebuilder/{package}', 'PackageController@PackageBuilder')->name('rwc2019.packagebuilder');
    Route::match(['get', 'post'], '/packagebuilder/{package}/{flight}', 'PackageController@PackageBuilderGetData')->name('rwc2019.packagebuilder.getdata');
    Route::get('/packagedetail/{package}', 'PackageController@PackageDetail')->name('rwc2019.packagedetail');
    Route::post('/precognitix', 'PackageController@PrepareDataToCognitixAPI')->name('rwc2019.packagebuilder.cognitixdata');
});
//Route::get('rwc2019', function() {
////     return view('pages/events/rwc19');
//    return view('rugby2019/master');
// });

Route::get('rwc2019reg', function() {
    return view('pages/events/rwc2019reg');
});

//Route::get('usg2018', function() {
//    return view('pages/events/usg2018');
//});

Route::get('motogp2018', function() {
    return view('pages/events/motogp2018');
});

//Route::get('ufcfightnightsingapore', function() {
//     return view('pages/events/ufcfnsg');
//});

//Route::any('ufcfightnightsingapore', function() {
//   $tixhotelfile = public_path()."/json/ufcfnsg2018/ufcfnsg2018-tickethotel-r1.0.txt";
//    $ticket_hotel = json_decode(file_get_contents($tixhotelfile), true);
//    
//    return view('pages/events/ufcsgofficial', [ 'ticket_hotel' => $ticket_hotel ]);
//});

Route::get('ufcfightnightsingaporeofficial', function() {
    $tixhotelfile = public_path()."/json/ufcfnsg2018/ufcfnsg2018-tickethotel-r1.0.txt";
    $ticket_hotel = json_decode(file_get_contents($tixhotelfile), true);
    
    return view('pages/events/ufcsgofficial', [ 'ticket_hotel' => $ticket_hotel ]);
});

// Route::get('rugby_world_cup_2019', function() {
//     return view('pages/events/rugby2019countdown');
// });

Route::get('klims2018', function() {
   return view('pages/events/klims2018');
});

Route::get('islandmusicfestival', function() {
//    return view('pages/events/islandmusicfest2018v2');
    return view('pages/events/islandmusicfestival-activities');
});

//Route::get('islandmusicfestivalv2', function() {
//    return view('pages/events/islandmusicfest2018v2');
//});

//Route::get('islandmusicfestivalv2', function() {
//    return view('pages/events/islandmusicfestival-activities');
//});

Route::get('motogpjapan', function() {
    return view('pages/events/motogpjapan');
});

Route::get('ror2018', function() {
    return view('pages/events/borneorhythms2018');
});

Route::get('originfields', function() {
    $ticketfile = public_path()."/json/events/originfields2018-ticket-r1.txt";
    $ticket_info = json_decode(file_get_contents($ticketfile), true);
    $ticket1 = session()->get('ticket1');

    return view('pages/events/originfields2018', [ 'ticket_info' => $ticket_info , 'ticket1' => $ticket1]);
});

Route::get('gunsnroses', function() {
    return view('pages/events/gnr2018');
});

Route::get('yoona', function() {
    $ticketfile = public_path()."/json/events/yoona2018-ticket-r1.txt";
    $ticket_info = json_decode(file_get_contents($ticketfile), true);
    $ticket1 = session()->get('ticket1');

    return view('pages/events/yoona2018', [ 'ticket_info' => $ticket_info , 'ticket1' => $ticket1]);
});

//Route::get('neonkualalumpur', function() {
//    return view('pages/events/neon2018');
//});

//Route::get('neonklregisternow', function() {
//    return view('pages/events/neon2018boost');
//});

Route::get('neonbeijing', function() {
    return view('pages/events/neonbeijing2018');
});

Route::get('ufcfightnightbeijing', function() {
    return view('pages/events/ufcbeijing2018');
});

Route::get('judaspriest', function() {
    $ticketfile = public_path()."/json/events/judaspriest2018-ticket-r1.txt";
    $ticket_info = json_decode(file_get_contents($ticketfile), true);
    $ticket1 = session()->get('ticket1');

    return view('pages/events/group/lamc/judaspriest', [ 'ticket_info' => $ticket_info , 'ticket1' => $ticket1]);
});

Route::any('neon_nyc_2017', function()  {
    return redirect('neonbeijing');
});

Route::get('avrynationdannic', function()  {
    return view('pages/events/group/avry/avrynationdannic2018');
});

Route::get('ufcadelaide', function()  {
    return view('pages/events/ufcadelaide2018');
});

//Route::get('malaysiawinefiesta', function()  {
//    return view('pages/events/mywinefiesta2018');
//});

//Route::get('sensationrise', function()  {
//    return view('pages/events/sensationrise2018');
//});

Route::get('theroarsingapore', function()  {
    return view('pages/events/roarsingapore2018');
});

Route::get('thedarkparty', function()  {
    return view('pages/events/thedarkparty2018');
});

Route::get('melaniecasiatour', function()  {
    return view('pages/events/melanie2018');
});

//Route::get('seanpaulliveinkl', function()  {
//    return view('pages/events/seanpaul2018');
//});

Route::get('ufcfightnightadelaide', function() {
    $tixhotelfile = public_path()."/json/ufcadelaide2018/ufcadelaide2018-tickethotel-r1.0.txt";
    $ticket_hotel = json_decode(file_get_contents($tixhotelfile), true);    
    return view('pages/events/ufcadelaide2018v2', [ 'ticket_hotel' => $ticket_hotel ]);
});

Route::get('thekualalumpurmajor', function() {
    $ticketfile = public_path()."/json/events/dotamy2018-ticket-r1.txt";
    $ticket_info = json_decode(file_get_contents($ticketfile), true);
    $ticket1 = session()->get('ticket1');
    return view('pages/events/dotamy2018', [ 'ticket_info' => $ticket_info , 'ticket1' => $ticket1]);
});

Route::get('epizode', function()  {
    return view('pages/events/epizode2018');
});

Route::get('neonlights2018', function()  {
    return view('pages/events/neonlights2018');
});

Route::get('cre', function()  {
    return view('pages/events/crekl2018');
});

Route::get('gilles-peterson', function()  {
    return view('pages/events/group/potatohead/gilespeterson2018');
});

Route::get('gilles-peterson-tickets', function()  {
    return view('pages/events/group/potatohead/gillespetersonpay');
});

Route::get('thefiveelvesmusical', function()  {
    return view('pages/events/group/astro/fiveelves2018');
});

Route::get('thefairydoll', function()  {
    return view('pages/events/thefairydoll2018');
});

//Route::get('finnsnewyearfestival', function()  {
//    return view('pages/events/group/finns/finnsnewyearfest2018');
//});

Route::get('finnsnewyearfestival', function() {
    $ticketfile = public_path()."/json/events/finns2018-ticket-r1.txt";
    $ticket_info = json_decode(file_get_contents($ticketfile), true);
    $ticket1 = session()->get('ticket1');
    return view('pages/events/group/finns/finnsnewyearfest2018', [ 'ticket_info' => $ticket_info , 'ticket1' => $ticket1]);
});


Route::get('finnsnewyearfestival-tickets', function()  {
    return view('pages/events/group/finns/finnsnewyearfestpay');
});

Route::get('vice-honeydijon', function()  {
    return view('pages/events/group/potatohead/honeydijon2018');
});

Route::get('vice-honeydijon-tickets', function()  {
    return view('pages/events/group/potatohead/honeydijonpay');
});

Route::get('soulsisters', function()  {
    return view('pages/events/soulsisters2018');
});

Route::get('sunburnfestival', function()  {
    return view('pages/events/sunburn2018');
});

Route::get('jasonderulo', function()  {
    return view('pages/events/jasonderulo2018');
});

Route::get('hardrock-desaru-nye', function()  {
    return view('pages/events/hardrockdesaru2018');
});

Route::get('potatohead-nye', function()  {
    return view('pages/events/group/potatohead/potatoheadnye2018');
});

Route::get('femmefatale2018', function()  {
    return view('pages/events/femmefatale2018');
});

Route::get('pokok', function()  {
    return view('pages/events/pokok2018');
});

Route::get('desarucoastgourmentseries', function()  {
    return view('pages/events/desarugourmet1');
});

Route::get('neoncountdown', function() {
    $ticketfile = public_path()."/json/events/neoncount2018-ticket-r3.txt";
    $ticket_info = json_decode(file_get_contents($ticketfile), true);
    $ticket1 = session()->get('ticket1');
    return view('pages/events/neon2018tixopen', [ 'ticket_info' => $ticket_info , 'ticket1' => $ticket1]);
});

Route::get('neonkualalumpur', function() {
    $ticketfile = public_path()."/json/events/neoncount2018-ticket-r2.txt";
    $ticket_info = json_decode(file_get_contents($ticketfile), true);
    $ticket1 = session()->get('ticket1');
    return view('pages/events/neon2018tix', [ 'ticket_info' => $ticket_info , 'ticket1' => $ticket1]);
});

Route::get('formosa', function()  {
    return view('pages/events/formosa2018');
});
//-- End of General events --


//-- Start of GTF events --

//-- End of GTF events --


//-- Start of Genting events --
Route::get('shilaamzah2018', function() {
    return view('pages/events/group/genting/shilaamzah2018');
});

Route::get('hkband2018', function() {
    return view('pages/events/group/genting/hkband2018');
});

Route::get('ildivo', function() {
    return view('pages/events/group/genting/ildivo2018');
});

Route::get('richiejen', function() {
    return view('pages/events/group/genting/richiejen2018');
});

Route::get('lizawang2018', function() {
    return view('pages/events/group/genting/lizawang2018');
});

Route::get('kingofcomedy2018', function() {
    return view('pages/events/group/genting/kingofcomedy2018');
});

Route::get('dickycheung', function() {
    return view('pages/events/group/genting/dickycheung2018');
});

Route::get('ourvoices', function() {
    return view('pages/events/group/genting/ourvoices2018');
});

Route::get('spectacularvoices', function() {
    return view('pages/events/group/genting/spectacularvoices2018');
});

Route::get('naying', function() {
    return view('pages/events/group/genting/naying2019');
});

Route::get('zhangdichenjinpei', function() {
    return view('pages/events/group/genting/zhangchen2018');
});

Route::get('grasshopper', function() {
    return view('pages/events/group/genting/grasshopper2018');
});

Route::get('susannaruco', function() {
    return view('pages/events/group/genting/susanna2018');
});

Route::get('kennybee', function() {
    return view('pages/events/group/genting/kennybee2018');
});

Route::get('cxhlqy', function() {
    return view('pages/events/group/genting/caixiaohu2019');
});

Route::get('yuya', function() {
    return view('pages/events/group/genting/yuya2018');
});

Route::get('hackenlee', function() {
    return view('pages/events/group/genting/hackenlee2018');
});
//-- End of Genting events --


//-- Start of Functional Routes --
//Route::get('usg2018dbsstaff', function() {
//   $tixflightfile = public_path()."/json/ultrasg18/usg2018-ticketflight-r1.0.txt";
//     $tixonlyfile = public_path()."/json/ultrasg18/usg2018-ticket-dbs-staff.txt";
//     $ticket_flight = json_decode(file_get_contents($tixflightfile), true);
//     $ticket_only = json_decode(file_get_contents($tixonlyfile), true);
//     $ticket_dbs = session()->get('ticket_dbs');

//     return view('pages/events/usg2018dbsstaff', [ 'ticket_only' => $ticket_only , 'ticket_flight' => $ticket_flight, 'ticket_dbs' => $ticket_dbs]);
//  });


Route::any('usg2018dbsstaffx', function(Request $request) {     
    $ticket_id = $request->ticket_id;
    $tixonlyfile = public_path()."/json/ultrasg18/usg2018-ticket-dbs-staff.txt";
    $ticket_only = json_decode(file_get_contents($tixonlyfile), true);
    $tickets = collect($ticket_only);

    $ticket_dbs = $tickets->where('Sorting ID', $ticket_id)->first();
    session()->put('ticket_id', $ticket_id);

    if(strlen($request->bin_num) < 8)
    {  
        return redirect('usg2018dbsstaff')->with('error_code', 1);
    }
    $request_email = $request->bin_num;

    if (strpos($request_email,'@dbs.com') !== false) {
        $pdo = DB::connection()->getPdo();
        $now = new DateTime();
        // $results = DB::select('select * from ultra_dbsstaff where email = :email', ['email' => $request_email]);
        
        //Check If password exist
        //if(count($results) > 0) {
            //if used
        //    if($results[0]->used) {
        //        return redirect('usg2018dbsstaff')->with('error_code', 2);
        //    }
        //} else {
                //not used - mark it used and return success
                $affected = DB::insert('insert into ultra_dbsstaff (email, used, used_date) values (?, ?, ?)', [$request_email, '1', $now]);
            
                if($affected) { //successfully inserted {
                    session()->put('ticket_dbs', $ticket_dbs);
                    return redirect('usg2018dbsstaff')->with('success_dbs', [ 'ticket' =>  $ticket_dbs]);
                }
       // }
    }

    return redirect('usg2018dbsstaff')->with('error_code', 1);
})->name('ultradbsstaff');


Route::any('ultrasingapore2018x', function(Request $request) {     
    $ticket_id = $request->ticket_id;
    $tixonlyfile = public_path()."/json/ultrasg18/usg2018-ticketonly-r1.2.txt";
    $ticket_only = json_decode(file_get_contents($tixonlyfile), true);
    $tickets = collect($ticket_only);

    $ticket1 = $tickets->where('Sorting ID', $ticket_id)->first();
    session()->put('ticket_id', $ticket_id);
    if(strlen($request->bin_num) < 10)
    {  
        return redirect('ultrasingapore2018')->with('error_code', 5);
    }
    $request_bin = $request->bin_num;
    $bin_file = public_path()."/json/ultrasg18/usg2018-private-bin-r.1.1.txt";
    $bin_nums = json_decode(file_get_contents($bin_file), true);
    foreach ($bin_nums as $bin)
    {
// check 10 digit with LF range - range 1 and range 2
// if matches
//   push DBS LF Message with correct ticket desc and price, pass ticket url parameter
//   for GA or PGA
// else (not match)
//   we substring to 6 digit
//   check 6 digit with DBS ranges from JSON File
// $results = $results->where('6-digit-bin', $json-bin-array)->first();
//   If matches
//      push DBS Message with correct ticket desc and price, pass ticket url parameter
//   for GA or PGA
//else does not match all
// push Message with Register

//        if(substr($request_bin,0,6) == '411911') {
//            if( ($request_bin >= 4119110052 && $request_bin <= 4119110059) || ($request_bin >= 4119110100 && $request_bin <= 4119110199)) {
//                session()->put('ticket1', $ticket1);
//                return redirect('usg2018x')->with('success_lf', [ 'ticket' =>  $ticket1]);
//            } else {
                // Check Ticket Type GA or PGA
                // if GA
//                if($ticket1["Ticket Type"] == "GA"){
//                    $ticket1 = $tickets->where('Sorting ID', '2')->first();
//                    session()->put('ticket1', $ticket1);
//                    return redirect('usg2018x')->with('success_dbs', [ 'ticket' =>  $ticket1]);                    
//                } else {                
//                    $ticket1 = $tickets->where('Sorting ID', '5')->first();
//                    session()->put('ticket1', $ticket1);
//                    return redirect('usg2018x')->with('success_dbs', [ 'ticket' =>  $ticket1]);            
//                }
                // else 
//            }
//        } else {
//            if (substr($request_bin,0,6) == $bin["BIN"]) {
//                session()->put('ticket1', $ticket1);
//                return redirect('usg2018x')->with('success_dbs', [ 'ticket' =>  $ticket1]);
//            }
//        }
//edx
        if (($ticket_id == '1') && (substr($request_bin,0,6) == '411911')) {
            if(($request_bin >= 4119110052 && $request_bin <= 4119110059) || ($request_bin >= 4119110100 && $request_bin <= 4119110199)) {
                session()->put('ticket1', $ticket1);
                return redirect('ultrasingapore2018')->with('success_lf', [ 'ticket' =>  $ticket1]);
            } else {
                return redirect('ultrasingapore2018')->with('error_code', 6);
            }
        }

        if (($ticket_id == '4') && (substr($request_bin,0,6) == '411911')) {
            if( ($request_bin >= 4119110052 && $request_bin <= 4119110059) || ($request_bin >= 4119110100 && $request_bin <= 4119110199)) {
                session()->put('ticket1', $ticket1);
                return redirect('ultrasingapore2018')->with('success_lf', [ 'ticket' =>  $ticket1]);
            } else {
                return redirect('ultrasingapore2018')->with('error_code', 6);
            }
        }

        if (($ticket_id == '2') && (substr($request_bin,0,6) == $bin["BIN"])) {
            session()->put('ticket1', $ticket1);
            return redirect('ultrasingapore2018')->with('success_dbs', [ 'ticket' =>  $ticket1]);
        }
        
        if (($ticket_id == '5') && (substr($request_bin,0,6) == $bin["BIN"])) {
            session()->put('ticket1', $ticket1);
            return redirect('ultrasingapore2018')->with('success_dbs', [ 'ticket' =>  $ticket1]);
        }
    }
    return redirect('ultrasingapore2018')->with('error_code', 5);
})->name('ultra');


Route::any('ultrasingapore2018xxx', function(Request $request) {     
    //Manage Passwords Attempt
    $password_attemps = $request->session()->get('password_attemps');

    if($password_attemps){
        if ($password_attemps >= 5 && ($request->session()->get('password_attemps_last_time') + (5*60)) < time()) {
            $request->session()->put('password_attemps', 1);
            $request->session()->put('password_attemps_last_time', time());
            $request->session()->save();
        } else if ($password_attemps >= 5 && ($request->session()->get('password_attemps_last_time') + (5*60)) > time()){        
            return redirect('ultrasingapore2018')->with('error_code', 12);
        }        
    }

    if($password_attemps >= 0){
        $password_attemps = $password_attemps + 1;
        $request->session()->put('password_attemps', $password_attemps);
        $request->session()->put('password_attemps_last_time', time());
        $request->session()->save();
    } else {
        $request->session()->put('password_attemps', 1);
        $request->session()->put('password_attemps_last_time', time());
        $request->session()->save();
    }

    $ticket_id = $request->ticket_id;
    session()->put('ticket_id', $ticket_id);
    $tixonlyfile = public_path()."/json/ultrasg18/usg2018-ticketonly-r1.2.txt";
    $ticket_only = json_decode(file_get_contents($tixonlyfile), true);
    $tickets = collect($ticket_only);
    $ticket1 = $tickets->where('Sorting ID', $ticket_id)->first();
    $request->session()->put('ticket1', $ticket1);
    $password = $request->password;
    //Check In Database
    $pdo = DB::connection()->getPdo();
    $results = DB::select('select * from ultra_passwords where password = :pass', ['pass' => $password]);
    //Check If password exist
    if(count($results) > 0) {
        //if used
        if($results[0]->used) {
            return redirect('ultrasingapore2018')->with('error_code', 13);
        } else {
            //not used - mark it used and return success
            $affected = DB::update('update ultra_passwords set used = 1, used_date = NOW() where password = :pass', ['pass' => $password]);
            session()->put('ticket1', $ticket1);
            if($affected) { //successfully updated {
                $request->session()->put('password_attemps', 0);
                $request->session()->put('password_attemps_last_time', time());
                $request->session()->save();       
                return redirect('ultrasingapore2018')->with('success_registered', [ 'ticket1' =>  $ticket1]);
            }
        }
    } else {
        //Password not exists
        return redirect('ultrasingapore2018')->with('error_code', 11);
    }
    // return redirect('ultrasingapore2018')->with('error_code', 12);
})->name('ultraregistered');


Route::get('usg2018x', function() {

    $tixflightfile = public_path()."/json/ultrasg18/usg2018-ticketflight-r1.2.txt";
    $tixonlyfile = public_path()."/json/ultrasg18/usg2018-ticketonly-r1.2.txt";
    // dd($tixonlyfile);
    $ticket_flight = json_decode(file_get_contents($tixflightfile), true);
    $ticket_only = json_decode(file_get_contents($tixonlyfile), true);
    // dd($ticket_only);
    $ticket1 = session()->get('ticket1');

    return view('pages/events/usg2018x', [ 'ticket_only' => $ticket_only , 'ticket_flight' => $ticket_flight, 'ticket1' => $ticket1]);
});

// Route::get('ultrasingapore', function () {
//    return redirect('ultrasingapore2018');
//});

//Route::get('ultrasingapore', function() {
//    return Redirect::to('http://airasiaredtix.com/ultrasingapore2018#DBS');
//    return redirect('ultrasingapore2018')->with('error_code', 11);
//});

//Route::get('ultrasingapore2018', function() {

    // session()->flush();
    // $password_attemps = session()->get('password_attemps');
    // if($password_attemps >= 1 && (session()->get('password_attemps_last_time') + (5*60)) < time()) {
    //     dd('aa');
    // }     
    // dd(session()->all());

//    $tixflightfile = public_path()."/json/ultrasg18/usg2018-ticketflight-r1.2.txt";
//    $tixonlyfile = public_path()."/json/ultrasg18/usg2018-ticketonly-r1.2.txt";
    // dd($tixonlyfile);
//    $ticket_flight = json_decode(file_get_contents($tixflightfile), true);
//    $ticket_only = json_decode(file_get_contents($tixonlyfile), true);
    // dd($ticket_only);
//    $ticket1 = session()->get('ticket1');

//    return view('pages/events/ultrasingapore2018', [ 'ticket_only' => $ticket_only , 'ticket_flight' => $ticket_flight, 'ticket1' => $ticket1]);
//});


Route::get('eventregistration', function() {
    return view('pages/events/registration/order-retrieve');
});

Route::get('mytickets2', function() {
    return view('pages/events/registration/my-tickets-2');
});

Route::get('mytickets', function(Request $request) {
    $transaction_number = $request->session()->get('transaction_number');
    $transaction_email = $request->session()->get('transaction_email');

    if(!isset($transaction_number) && !isset($transaction_email))
        return redirect('eventregistration');

    $event_ticket_info = DB::select('select * from event_ticket_purchases as a, events as b where a.transaction_number = :transaction_number AND a.email = :email AND a.event_id = b.id', ['transaction_number' => $transaction_number, 'email' => $transaction_email]);
    $tickets = array();
    foreach ($event_ticket_info as $ticket) {
        $ticket_info = DB::select('select * from ticket_registration where ticket_number = :ticket_number', ['ticket_number' => $ticket->ticket_number]);
        if(count($ticket_info) > 0){
            $ticket->ticket_info = $ticket_info[0];
        }
        array_push($tickets, $ticket);
    }
    // print_r($tickets);die();
    return view('pages/events/registration/my-tickets', ['tickets' => $tickets]);
});


Route::get('validate-order', function(Request $request) {
    $params = request()->all();
    $pdo = DB::connection()->getPdo();
    $response = array('success' => false, 'message' => '');

    if(isset($params['transaction_number'])) {
        $results = DB::select('select * from event_ticket_purchases where transaction_number = :transaction_number', ['transaction_number' => $params['transaction_number']]);
        if (count($results) > 0) {
            $response['success'] = true;
        } else {
            $response['message'] = 'Sorry, transaction number is invalid';
        }
    }
    if(isset($params['email'])) {
        $results = DB::select('select * from event_ticket_purchases where email = :email', ['email' => $params['email']]);
        if (count($results) > 0) {
            $response['success'] = true;
        } else {
            $response['message'] = 'Sorry, email is invalid';
        }
    }
    return json_encode($response);
});


Route::post('order-retrieval', function(Request $request) {
    $params = request()->all();
    $pdo = DB::connection()->getPdo();
    $results = DB::select('select * from event_ticket_purchases where email = :email AND transaction_number = :transaction_number', ['email' => $params['email'], 'transaction_number' => $params['transactionNumber']]);
    //Check If password exist
    if(count($results) > 0) {
        $request->session()->put('transaction_number', $params['transactionNumber']);
        $request->session()->put('transaction_email', $params['email']);
        $request->session()->save();
        //TODO send to my tickets page

       return redirect('mytickets');


//        return redirect('registration?transaction_number=' .$params['transactionNumber']. '&ticket_number=1212');
    } else {
        //Ticket not exists
        return view('pages/events/registration/order-retrieve', [ 'error' => 'tickets not exists.']);
    }
    // return view('pages/events/registration/enter');
});

Route::get('assignRegistration', function() {
    return view('pages/events/registration/event-registration-page4');
});

Route::get('registration', function(Request $request) {
    $params = request()->all();
    if(!isset($params['transaction_number']) && !isset($params['ticket_number']))
        return redirect('mytickets');

    $pdo = DB::connection()->getPdo();
    $event_ticket_info = DB::select('select * from event_ticket_purchases as a, events as b where a.transaction_number = :transaction_number AND a.ticket_number = :ticket_number AND a.event_id = b.id', ['transaction_number' => $params['transaction_number'], 'ticket_number' => $params['ticket_number']]);
    $ticket_info = DB::select('select * from ticket_registration where ticket_number = :ticket_number', ['ticket_number' => $params['ticket_number']]);
    if(count($event_ticket_info) > 0 && count($ticket_info) > 0) {
        return view('pages/events/registration/registration', [ 'registered' => true, 'ticket_info' => $event_ticket_info[0], 'ticket_detail' => $ticket_info[0] ]);
    } else if (count($event_ticket_info) > 0) {
            return view('pages/events/registration/registration', [ 'ticket_info' => $event_ticket_info[0]]);
    } else {
        return redirect('eventregistration');
    }
});

Route::post('ticket-registration', function(Request $request) {
    $params = request()->all();
    $ticket_info = DB::select('select * from event_ticket_purchases as a, events as b, ticket_registration as c where a.ticket_number = :ticket_number AND a.event_id = b.id AND a.ticket_number = c.ticket_number', ['ticket_number' => $params['ticket_number']]);
    if(count($ticket_info) > 0) {
        return redirect('mytickets');
    } else {
        unset($params['_token']);
        $transaction_number = $params['transaction_number'];
        unset($params['transaction_number']);
        $result = DB::table('ticket_registration')->insert($params);
        if($result == 1) {
            //Send Email
            $complete_ticket_info = DB::select('select * from event_ticket_purchases as a, events as b, ticket_registration as c where a.ticket_number = :ticket_number AND a.event_id = b.id AND a.ticket_number = c.ticket_number', ['ticket_number' => $params['ticket_number']]);
            Mail::to($params['email'])->send(new TicketDetail($complete_ticket_info[0]));
            return redirect('registration?transaction_number='.$transaction_number.'&ticket_number='.$params['ticket_number']);
        } else {
            return redirect('eventregistration');
            // return view('pages/events/registration/enter', [ 'error' => 'tickets not exists.']);
        }    
    }
});

Route::get('successfulRegistration', function() {
    // Mail::to('ameer@novatoresols.com')->send(new TicketDetail(array()));

    return view('pages/events/registration/event-registration-page5');
});

Route::get('successful', function() {

    $ticket_number = '3815091459632';

//    $complete_ticket_info = DB::select('select * from event_ticket_purchases as a, events as b, ticket_registration as c where a.ticket_number = :ticket_number AND a.event_id = b.id AND a.ticket_number = c.ticket_number', ['ticket_number' => $ticket_number]);
        $complete_ticket_info = DB::select('select * from event_ticket_purchases as a, events as b, ticket_registration as c where a.ticket_number = :ticket_number AND a.event_id = b.id AND a.ticket_number = c.ticket_number', ['ticket_number' => $ticket_number]);


    print_r($complete_ticket_info);
    die();

    // Mail::to('ayesha.shaukat@novatoresols.com')->send(new TicketDetail(array()));
    Mail::to('ameer@novatoresols.com')->send(new TicketDetail($event_ticket_info[0]));

//    return view('emails/eventregisteration/details');
});

// Route::get('hauntfest2018', function() {
//     $current_date = new DateTime('now');
//     $launchdate = new DateTime('Feb 7 2018 10:00:00 GMT+0800');
//     if($current_date > $launchdate)
//     {
//         return view('pages/events/hauntfest2018');
//     }
//     return view('errors.404');
// });

// Route::get('gary_chaw', function () {
//     return view('pages/events/garychaw');
// });

// Route::get('malaysian_independent_live_fusion_festival', function () {
//      return view('pages/events/milff');
// });

// Route::get('concerts/{slug}', ['uses' => 'ConcertsController@show']);

// Route::get('concerts/find/{id}', 'ConcertsController@findconcert');

// Route::get('cart', 'CartController@index');

// Route::get('cart/get', 'CartController@getCart');

// Route::get('cart/total', 'CartController@getTotal');

// Route::get('cart/get/ticket/{id}', 'CartController@getTicket');

// Route::get('cart/remove/item/{id}', ['as' => 'cart.remove.item', 'uses' => 'CartController@remove']);

// Route::put('cart/{id}', ['as' => 'cart.update.item', 'uses' => 'CartController@update']);

// Route::post('cart', 'CartController@store');

// Route::post('coupon/apply', ['uses' => 'CouponsController@apply', 'as' => 'coupons.apply']);

// Route::get('braintree/token', ['uses' => 'BraintreeTokenController@token', 'as' => 'braintree.token']);

// Route::post('order', ['uses' => 'OrdersController@store', 'as' => 'order.store']);

//Route::get('/concerts/{id}', ['uses' => 'ConcertsController@show']);

//Route::post('/concerts/{id}/orders', ['uses' => 'ConcertOrdersController@store']);

// Route::get('generatepdf', 'CartController@generatepdf');

/*Route::prefix('ultrasingapore2018')->group(['middleware' => 'auth:ultrasingapore'], function(){
    Route::get('/','UltrasingaporePassengerController@get_details')->name('customer.details');
    Route::post('passengers','UltrasingaporePassengerController@store_passengers')->name('passenger.store');
    
});*/

// Ultra Singapore Flight Passenger Form
//Route::group(['prefix' => 'ultrasingapore18'], function(){
    //Route::get('/', );
    //Route::get('login', ['uses' => 'UltrasingaporeLoginController@login', 'as' => 'ultrasingapore.login']);

    
//});

// Route::get('/ultrasingapore18/login', ['uses' => 'UltrasingaporeLoginController@showLoginForm', 'as' => 'ultrasingapore.login']);
// Route::post('/ultrasingapore18/login', ['uses' => 'UltrasingaporeLoginController@login']);
// Route::match(array('GET','POST'), '/ultrasingapore18/logout', ['uses' => 'UltrasingaporeLoginController@logout', 'as' => 'ultrasingapore.logout']);
// Route::get('/ultrasingapore18/add_passenger', ['uses' => 'UltrasingaporePassengerController@add_passenger', 'as' => 'ultrasingapore.add.passenger']);

Route::middleware('auth:ultrasingapore')->group(function() {
    Route::get('/ultrasingapore18','UltrasingaporePassengerController@get_details')->name('ultrasingapore.customer.details');
    Route::post('/ultrasingapore18/passengers','UltrasingaporePassengerController@store_passengers')->name('ultrasingapore.passenger.store');
});


// Route::get('tickets', function() {
//    return view('payments/tickets');
// });

//-- EOF SITE FUNCTIONS --
Route::get('/fake/pdf/{package}/{selectedCityOfDeparture?}', 'FakeController@fakePdf')->name('fake.pdf');
Route::get('/fake/html-for-pdf/{package}/{selectedCityOfDeparture?}', 'FakeController@htmlForPdf')->name('fake.pdf');
