<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns:v="urn:schemas-microsoft-com:vml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;" />
    <meta name="viewport" content="width=600,initial-scale = 2.3,user-scalable=no">
    <!--[if !mso] --><!-- -->
    <!-- <link href='https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,600,700' rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Quicksand:300,400,700' rel="stylesheet"> -->
    <!-- <![endif]-->

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
    
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>

    <title>Successful Email</title>

    <style type="text/css">
        body {
            width: 100%;
            background-color: #ffffff;
            margin: 0;
            padding: 0;
            -webkit-font-smoothing: antialiased;
            mso-margin-top-alt: 0px;
            mso-margin-bottom-alt: 0px;
            mso-padding-alt: 0px 0px 0px 0px;
        }

        p,
        h1,
        h2,
        h3,
        h4 {
            margin-top: 0;
            margin-bottom: 0;
            padding-top: 0;
            padding-bottom: 0;
        }

        span.preheader {
            display: none;
            font-size: 1px;
        }

        html {
            width: 100%;
        }

        table {
            font-size: 14px;
            border: 0;
            max-width: 1000px;
        }
        /* ----------- responsivity ----------- */

        @media only screen and (max-width: 640px) {
            /*------ top header ------ */
            .main-header {
                font-size: 20px !important;
            }
            .main-section-header {
                font-size: 28px !important;
            }
            .show {
                display: block !important;
            }
            .hide {
                display: none !important;
            }
            .align-center {
                text-align: center !important;
            }
            .no-bg {
                background: none !important;
            }
            /*----- main image -------*/
            .main-image img {
                width: 440px !important;
                height: auto !important;
            }
            /* ====== divider ====== */
            .divider img {
                width: 440px !important;
            }
            /*-------- container --------*/
            .container590 {
                width: 440px !important;
            }
            .container580 {
                width: 400px !important;
            }
            .main-button {
                width: 220px !important;
            }
            /*-------- secions ----------*/
            .section-img img {
                width: 320px !important;
                height: auto !important;
            }
            .team-img img {
                width: 100% !important;
                height: auto !important;
            }
        }

        @media only screen and (max-width: 479px) {
            /*------ top header ------ */
            .main-header {
                font-size: 18px !important;
            }
            .main-section-header {
                font-size: 26px !important;
            }
            /* ====== divider ====== */
            .divider img {
                width: 280px !important;
            }
            /*-------- container --------*/
            .container590 {
                width: 280px !important;
            }
            .container590 {
                width: 280px !important;
            }
            .container580 {
                width: 260px !important;
            }
            /*-------- secions ----------*/
            .section-img img {
                width: 280px !important;
                height: auto !important;
            }

        }

        .text-65 {
            color: rgba(29, 29, 29, 0.65)
        }

        .text-75 {
            color: rgba(29, 29, 29, 0.75);
        }

        .text-grey {
            color: rgba(29, 29, 29, 0.35)
        }

        .text-1D {
            color: #1D1D1D;
        }

        .text-blue {
            color: #56CCF2;
        }

        .grey-email {
            color: rgba(29, 29, 29, 0.65);
            font-size: 22px;
        }

        .card-shadow { 
            box-shadow: 2px 15px 20px rgba(29, 29, 29, 0.2);
        }

        .card-1 {
            height: 300px;
            position: relative;
            border-radius: 25px;
            width: 500px;
        }

        .card-2 {
            height: 300px;
            background-size: cover;
            border-radius: 25px;
            width: 330px;
        }

        .registered-ticket-left {
            background: url({{$ticket->ticket_left_image}}) no-repeat;
            background-size: cover;
            border-right: 2px dashed rgba(29, 29, 29, 0.35); 
        }
    
        .registered-ticket-right {
            background: url({{$ticket->ticket_right_image}}) no-repeat;
            background-size: cover;
        }

        body {
            padding: 20px;
        }
    </style>
</head>

<body class="my-sm-5">
    <table class="table-borderless table">
        <tbody>
            <tr>
                <td class="text-right"><img src="{!! asset('images/redtix_logo.png') !!}" alt="no red img" height="65px" width="110px"/></td>
                <td class="text-left text-65 py-sm-4">Grab it before someone else does!</td>
            </tr>

            <tr class="text-center">
                <td colspan="4"><img src="{!! asset('images/event-registration/tint.png') !!}" alt="no tint img" width="90%"></td>
            </tr>
        </tbody>
    </table>

    <table class="table-borderless table">
        <thead class="text-center mx-sm-5 px-sm-5">
            <tr class="text-65">
                <td>EVENTS</td>
                <td>COLLECTION</td>
                <td>BLOG</td>
                <td>LOREMS</td>
            </tr>
        </thead>
    </table>

    <!-- resgitration successful -->
    <table class="table mx-sm-1">
        <tbody class="py-sm-3">
            <tr>
                <td class="text-center">
                    <img src="{!! asset('images/event-registration/form-ok.png') !!}" height="125px" width='120px' alt="no pink img">
                </td>
                <td class="text-left">
                    <h3 class="text-65">Registration successful</h3><br>
                    <h6 class='text-grey'>Email</h6>
                    <h5 class="text-1D">{{$ticket->email}}</h5>
                </td>
            </tr>
        </tbody>
    </table>
    <table class="table table-borderless mx-sm-5">
        <tbody>
            <tr>
                <td>
                    <h4 class="text-grey">Registration successful</h4>
                </td>
            </tr>
            <tr>
                <td>
                    <h5 class="text-75">{{$ticket->first_name}} {{$ticket->last_name}},</h5>
                    <p class="text-75">({{$ticket->email}})</p>
                </td>
                <td>
                    <h6 class="text-75">Transaction number</h6>
                    <h5 class="text-75">{{$ticket->transaction_number}}</h5>
                </td>
            </tr>
            <tr>
                <td>
                    <p class='text-65'>
                    The following tickets for
                        <span class="text-6 5"><strong>{{$ticket->show}}</strong></span> 
                        have been registered
                    </p>
                </td>
            </tr>
        </tbody>
    </table>

    <!-- ticket -->
    <table class="table table-borderless mx-sm-5">
        <tbody>
            <tr>
                <td>
                    <h5 class="text-75">Registered to: <span class="grey-email">{{$ticket->email}}</span></h5>
                </td>
                <td class="clearfix">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="4" class="pr-0" align="right">
                    <div class="card card-shadow card-1 registered-ticket-left" style="background: url({{$ticket->ticket_left_image}}) no-repeat; background-size: cover; border-right: 2px dashed rgba(29, 29, 29, 0.35);" >
                        <div class="card-body text-left" style="margin-left: 60%;">
                            <!-- left card details -->
                            <small class="text-75">DATE & TIME</small>
                            <h6 class="text-75">{{$ticket->date}}</h6>
                            
                            <small class="text-75">VENUE</small>
                            <h6 class="text-75">{{$ticket->venue}}</h6>

                            <small class="text-75">PRICE</small>
                            <h6 class="text-75">RM {{$ticket->price}}</h6>

                            <small class="text-75">TICKET CATEGORY</small>
                            <h6 class="text-75">{{$ticket->price_type}} <small class="text-45">{{$ticket->price_level}}</small></h6>
                        </div>
                    </div>
                </td>
                <td colspan="3" class="pl-0" align="left">
                    <div class="card card-shadow card-2 registered-ticket-right" style="background: url({{$ticket->ticket_right_image}}) no-repeat; background-size: cover; border-right: 2px dashed rgba(29, 29, 29, 0.35);">
                        <div class="card-body" style="width: 200px;">
                            <h6 class="text-75">Ticket Number:</h6>
                            <h6 class="text-75">{{$ticket->ticket_number}}</h6>
                        
                            <p class="text-45">Name: <span class="text-75">{{$ticket->first_name}} {{$ticket->last_name}}</span></p>
                            <p class="text-45">Email: <span class="text-75">{{$ticket->email}}</span></p>
                            <small class="text-75">{{$ticket->show}}</small>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td align="right">
                    <a href="#" class="text-blue"><p>View registration details</p></a>
                </td>
            </tr>
        </tbody>
    </table>

    <!-- assignment successful -->
    <table class="table mx-sm-1" style="display: none;">
        <tbody class="py-sm-3">
            <tr>
                <td class="text-center">
                    <img src="{!! asset('images/event-registration/send.png') !!}" height="125px" width='120px' alt="no pink img">
                </td>
                <td class="text-left">
                    <h3 class="text-65">Assignment successful</h3><br>
                    <h6 class='text-grey'>To:</h6>
                    <h5 class="text-1D">{{$ticket->email}}</h5>
                </td>
            </tr>
        </tbody>
    </table>
    <table class="table table-borderless mx-sm-5" style="display: none;">
        <tbody>
            <tr>
                <td>
                    <h4 class="text-grey">Assignment successful</h4>
                </td>
            </tr>
            <tr>
                <td>
                    <h5 class="text-75">{{$ticket->first_name}} {{$ticket->last_name}},</h5>
                    <p class="text-75">({{$ticket->email}})</p>
                </td>
                <td>
                    <h6 class="text-75">Transaction number</h6>
                    <h5 class="text-75">{{$ticket->transaction_number}}</h5>
                </td>
            </tr>
            <tr>
                <td>
                    <p class='text-65'>
                    The following tickets for
                        <span class="text-6 5"><strong>{{$ticket->show}}</strong></span> 
                        have been registered
                    </p>
                </td>
            </tr>
        </tbody>
    </table>

    <!-- you've been assigned a ticket -->
    <table class="table mx-sm-1" style="display: none;">
        <tbody class="py-sm-3">
            <tr>
                <td class="text-center">
                    <img src="{!! asset('images/event-registration/get-ticket.png') !!}" height="125px" width='120px' alt="no pink ticket img">
                </td>
                <td class="text-left">
                    <h3 class="text-65">You've been assigned a ticket</h3><br>
                    <h6 class='text-grey'>From:</h6>
                    <h5 class="text-1D">{{$ticket->email}}</h5>
                </td>
            </tr>
        </tbody>
    </table>
    <table class="table table-borderless mx-sm-5" style="display: none;">
        <tbody>
            <tr>
                <td>
                    <h4 class="text-grey">You've been assigned a ticket</h4>
                </td>
            </tr>
            <tr>
                <td>
                    <h5 class="text-75">Attendee,</h5>
                    <p class="text-75">({{$ticket->email}})</p>
                </td>
                <td>
                    <h6 class="text-75">Transaction number</h6>
                    <h5 class="text-75">{{$ticket->transaction_number}}</h5>
                </td>
            </tr>
            <tr>
                <td>
                    <p class='text-65'>
                    The following tickets for
                        <span class="text-6 5"><strong>{{$ticket->show}}</strong></span> 
                        have been registered
                    </p>
                </td>
            </tr>
        </tbody>
    </table> 

</body>
</html>