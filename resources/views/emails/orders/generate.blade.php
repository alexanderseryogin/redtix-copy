<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Tickets</title>
</head>

<style>
    body{
        font-family: Arial, Helvetica, sans-serif;
        font-size: 16px;
        line-height: 20px;
        background-color: #eee;
    }
    #wrapper{
        width: 670px;
        background-color: #fff;
        padding: 30px;
    }
    #tixHead{
        display: block;
        clear: both;
        overflow: hidden;
        padding-bottom: 30px;
        margin-bottom: 30px;
        border-bottom: 1px dashed #ccc;
        font-size: 14px;
    }
    #tixHead .logo img{
        float: left;
        height: 80px;
    }
    .redtixLink{
        display: block;
        text-align: right;
        margin-bottom: 10px;
    }
    a.redtixLink{
        text-decoration: none;
    }
    #tixHead .headInfo{
        float: right;
    }
    #tixHead .headInfo ul{
        margin: 0;
        padding: 0;
    }
    #tixHead .headInfo li{
        list-style: none;
        float: left;
        margin-bottom: 2px;
        margin: 0 5px 2px 5px;
        padding: 5px 10px;
        color: #fff;
    }
    #tixHead .headInfo li a{
        color: #fff;
        text-decoration: none;
    }
    #tixHead .headInfo li.fb{
        background-color: #36549d;
        width: 100px;
    }
    #tixHead .headInfo li.tw{
        background-color: #46a9e3;
        width: 100px;
    }
    #tixHead .headInfo li.yt{
        background-color: #f61c0d;
        width: 100px;
    }
    #tixHead .headInfo li.ig{
        background-color: #dd923f;
        width: 100px;
    }

    .tixBody{

    }
    .tixDetail table{
        margin: 30px 0;
    }
    .tixDetail table tr td:first-child{
        font-weight: bold;
        width: 200px;
    }
    .tixDetail table tr td:last-child{
        font-weight: bold;
        width: 420px;
    }
    .tixDetail img.barCode{
        height: 100px;
    }
    .tixNote{
        padding-top: 30px;
        margin-top: 30px;
        border-top: 1px dashed #ccc;
        font-size: 14px;
    }
    .tixNote h2{
        font-size: 16px;
    }
    .tixNote li{
        margin-bottom: 2px;
    }
    .tixFooter{
        margin-top: 50px;
        text-align: center;
    }
</style>
<body>

<div id="wrapper">
    <section id="tixHead">
        <div class="logo">
            <img src="http://redtix.dev/images/assets/redtrix_logo.png" alt="">
            <a class="redtixLink" href="http://www.airasiaredtix.com">www.airasiaredtix.com</a>
        </div>
        <div class="headInfo">
            <ul>
                <li class="fb"><a href="https://www.facebook.com/RedTix">Facebook</a></li>
                <li class="tw"><a href="https://twitter.com/redtix">Twitter</a></li>
                <li class="yt"><a href="https://www.youtube.com/user/airasiaredtix">Youtube</a></li>
                <li class="ig"><a href="https://www.instagram.com/AirAsiaRedTix/">Instagram</a></li>
            </ul>
        </div>
    </section>
    <section class="tixBody">
        <strong>Transaction Number : {{$payment->transaction_id}}</strong>
        <div class="tixDetail">
            <table>
                <tbody>
                <tr>
                    <td>Show</td>
                    <td>:</td>
                    <td>{{$concert->title}}</td>
                </tr>
                <tr>
                    <td>Venue</td>
                    <td>:</td>
                    <td>{{$concert->venue}}</td>
                </tr>
                <tr>
                    <td>Date</td>
                    <td>:</td>
                    <td>{{$concert->date}}</td>
                </tr>
                <tr>
                    <td>Name</td>
                    <td>:</td>
                    <td>{{ $user->name }}</td>
                </tr>
                <tr>
                    <td>Area</td>
                    <td>:</td>
                    <td>NO ENTRY</td>
                </tr>
                <tr>
                    <td>Ticket Price</td>
                    <td>:</td>
                    <td>RM {{ $ticket->price }}</td>
                </tr>
                <tr>
                    <td>Ticket Type</td>
                    <td>:</td>
                    <td>{{ $ticket->title }}</td>
                </tr>
                </tbody>
            </table>
            <p>For Inquiries write to us support@airasiaredtix.com</p>
            <div>
                {!! DNS1D::getBarcodeHTML($ticketcode->serial, "EAN13", 3, 55) !!}
                <p style="width: 285px; text-align: center; font-weight: 900; font-size: 12px; letter-spacing: 2px;">{{$ticketcode->serial}}</p>
            </div>

        </div>
        <div class="tixNote">
            <h2>Instructions</h2>
            <ol>
                <li>Please print this E-ticket and keep safely.</li>
                <li>RM5.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day</li>
                <li>Please present this at the redemption counter to redeem your phsical ticket for admission.</li>
                <li>You are required to produce your IC/Passport for validation purposes upon redemption.If the ticket is purchased in another name, we will require a copy of the IC/Passport and authorisation letter of the purchaser in order for you to redeem the ticket.</li>
            </ol>
            <h2>Important Notice (Terms and Conditions)</h2>
            <ol>
                <li>You have purchased this E-ticket from www.AirAsiaRedTix.com. By doing so, you agree and are bound by its terms and conditions of Puchase and Privacy Policy.</li>
                <li>All sales are final. Requests for refunds or exchanges will not be entertained.</li>
                <li>E-tickets are not transferable.</li>
                <li>Entry into the event may be denied if your E-ticket is damaged, torn, faded, cannot be scanned or verified.</li>
                <li>For multiple E-ticket purchasers in a group, ensure each of the group members is given thier own E-ticket to gain admission, into concert hall. AirAsiaRedtix.com or AirAsia Berhad is not responsible for any event cancellation or postponement.</li>
                <li>6. In the event of E-ticket fraud or duplication, AirAsiaRedTix.com reserves the right to refuse any entry to all E-ticket holders.</li>
            </ol>
        </div>
    </section>
    <section class="tixFooter">
        <div class="footerContent">
            <img src="http://redtix.dev/images/assets/ad.jpg" alt="">
        </div>
    </section>
</div>

</body>
</html>