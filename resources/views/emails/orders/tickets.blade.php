<!DOCTYPE html>
<html>
<head>
    <title>Page Title</title>
</head>

<style>
    body{
        font-family: Arial, Helvetica, sans-serif;
        font-size: 16px;
        line-height: 20px;
        background-color: #eee;
    }
    .wrapper{
        width: 800px;
        margin: 0px auto;
        background-color: #fff;
        padding: 30px;
    }
    .tixHead{
        display: block;
        clear: both;
        overflow: hidden;
        padding-bottom: 30px;
        margin-bottom: 30px;
        border-bottom: 1px dashed #ccc;
        font-size: 14px;
    }
    .tixHead .logo img{
        float: left;
        height: 80px;
    }
    .redtixLink{
        display: block;
        text-align: right;
        margin-bottom: 10px;
    }
    a.redtixLink{
        text-decoration: none;
    }
    .tixHead .headInfo{
        float: right;
    }
    .tixHead .headInfo ul{
        margin: 0;
        padding: 0;
    }
    .tixHead .headInfo li{
        list-style: none;
        float: left;
        margin-bottom: 2px;
        margin: 0 5px 2px 5px;
        padding: 5px 10px;
        color: #fff;
    }
    .tixHead .headInfo li a{
        color: #fff;
        text-decoration: none;
    }
    .tixHead .headInfo li.fb{background-color: #36549d;}
    .tixHead .headInfo li.tw{background-color: #46a9e3;}
    .tixHead .headInfo li.yt{background-color: #f61c0d;}
    .tixHead .headInfo li.ig{background-color: #dd923f;}

    .tixBody{

    }
    .tixDetail table{
        margin: 30px 0;
    }
    .tixDetail table tr td:first-child{
        font-weight: bold;
        width: 35%;
    }
    .tixDetail img.barCode{
        height: 100px;
    }
    .tixNote{
        padding-top: 30px;
        margin-top: 30px;
        border-top: 1px dashed #ccc;
        font-size: 14px;
    }
    .tixNote h2{
        font-size: 16px;
    }
    .tixNote li{
        margin-bottom: 2px;
    }
    .tixFooter{
        margin-top: 50px;
        text-align: center;
    }
</style>
<body>

<div class="wrapper">
    <section class="tixHead">
        <div class="logo">
            <img src="/images/assets/redtrix_logo.png" alt="">
            <a class="redtixLink" href="http://www.airasiaredtix.com">www.airasiaredtix.com</a>
        </div>
        <div class="headInfo">
            <ul>
                <li class="fb"><a href="https://www.facebook.com/RedTix">Facebook</a></li>
                <li class="tw"><a href="https://twitter.com/redtix">Twitter</a></li>
                <li class="yt"><a href="https://www.youtube.com/user/airasiaredtix">Youtube</a></li>
                <li class="ig"><a href="https://www.instagram.com/AirAsiaRedTix/">Instagram</a></li>
            </ul>
        </div>
    </section>
    <section class="tixBody">
        <strong>Transaction Number : 206517</strong>
        <div class="tixDetail">
            <p>This email containts your tickets in the attachments.</p>
            <p>For Inquiries write to us support@airasiaredtix.com</p>

        </div>
        <div class="tixNote">
            <h2>Instructions</h2>
            <ol>
                <li>Please print this E-ticket and keep safely.</li>
                <li>RM5.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day</li>
                <li>Please present this at the redemption counter to redeem your phsical ticket for admission.</li>
                <li>You are required to produce your IC/Passport for validation purposes upon redemption.If the ticket is purchased in another name, we will require a copy of the IC/Passport and authorisation letter of the purchaser in order for you to redeem the ticket.</li>
            </ol>
            <h2>Important Notice (Terms and Conditions)</h2>
            <ol>
                <li>You have purchased this E-ticket from www.AirAsiaRedTix.com. By doing so, you agree and are bound by its terms and conditions of Puchase and Privacy Policy.</li>
                <li>All sales are final. Requests for refunds or exchanges will not be entertained.</li>
                <li>E-tickets are not transferable.</li>
                <li>Entry into the event may be denied if your E-ticket is damaged, torn, faded, cannot be scanned or verified.</li>
                <li>For multiple E-ticket purchasers in a group, ensure each of the group members is given thier own E-ticket to gain admission, into concert hall. AirAsiaRedtix.com or AirAsia Berhad is not responsible for any event cancellation or postponement.</li>
                <li>6. In the event of E-ticket fraud or duplication, AirAsiaRedTix.com reserves the right to refuse any entry to all E-ticket holders.</li>
            </ol>
        </div>
    </section>
    <section class="tixFooter">
        <div class="footerContent">
            <img src="/images/assets/ad.jpg" alt="">
        </div>
    </section>
</div>

</body>
</html>