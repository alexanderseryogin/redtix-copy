<div class="text-center center-block hidden-lg hidden-md mobileFooter">
    <div class="socialFooter">
        <a target="_blank" href="https://www.facebook.com/RedTix"><i class="fa fa-facebook-square" aria-hidden="true"></i></a>
        <a target="_blank" href="https://twitter.com/redtix"><i class="fa fa-twitter-square" aria-hidden="true"></i></a>
        <a target="_blank" href="https://www.youtube.com/user/airasiaredtix"><i class="fa fa-youtube-square" aria-hidden="true"></i></a>
        <a target="_blank" href="https://www.instagram.com/AirAsiaRedTix/"><i class="fa fa-instagram" aria-hidden="true"></i></a>
    </div>
    <p>Copyright 2018 <a target="_blank" href="http://www.airasia.com/">RedTix Sdn Bhd</a>, All Rights Reserved.</p>
</div>

<footer class="rediconFooter">
    <div class="container">
   
        <div class="text-center center-block hidden-xs hidden-sm webFooter">
            <div class="socialFooter">
                <a target="_blank" href="https://www.facebook.com/RedTix"><i class="fa fa-facebook-square" aria-hidden="true"></i></a>
                <a target="_blank" href="https://twitter.com/redtix"><i class="fa fa-twitter-square" aria-hidden="true"></i></a>
                <a target="_blank" href="https://www.youtube.com/user/airasiaredtix"><i class="fa fa-youtube-square" aria-hidden="true"></i></a>
                <a target="_blank" href="https://www.instagram.com/AirAsiaRedTix/"><i class="fa fa-instagram" aria-hidden="true"></i></a>
            </div>
            <p>Copyright 2018 <a target="_blank" href="http://www.airasia.com/">RedTix Sdn Bhd</a>, All Rights Reserved.</p>
        </div>
    
    </div>
</footer>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
{{-- {!! Html::script('js/app.js') !!} --}}
<script type="text/javascript" src="js/app.min.js"></script>
<script type="text/javascript" src="/js/scrollToFixed.js"></script>

<!-- Featherlight lightbox -->
<script src="https://cdn.rawgit.com/noelboss/featherlight/1.7.7/release/featherlight.min.js" type="text/javascript" charset="utf-8"></script>
<script src="https://cdn.rawgit.com/noelboss/featherlight/1.7.7/release/featherlight.gallery.min.js" type="text/javascript" charset="utf-8"></script>

<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-587468fe5d4abf72"></script>

<script type="text/javascript">
    var register_newsletter = function(){
        var user_email = document.getElementById('mce-EMAIL').value;
        var user_name = user_email.split("@")[0];
        mixpanel.alias(user_email);
        mixpanel.people.set({
          "$name": user_name,
          "$email": user_email
        });
        mixpanel.identify(user_email);
        mixpanel.track('register_newsletter',
        {
            Email: user_email
        });
    }
</script>

<!-- Mixpanel last touch attributes -->
<script type="text/javascript">
    function getQueryParam(url, param) {
        // Expects a raw URL
        param = param.replace(/[[]/, "\[").replace(/[]]/, "\]");
        var regexS = "[\?&]" + param + "=([^&#]*)",
            regex = new RegExp( regexS ),
            results = regex.exec(url);
        if (results === null || (results && typeof(results[1]) !== 'string' && results[1].length)) {
            return '';
        } else {
            return decodeURIComponent(results[1]).replace(/\W/gi, ' ');
        }
    };

    function campaignParams() {
        var campaign_keywords = 'utm_source utm_medium utm_campaign utm_content utm_term'.split(' ')
            , kw = ''
            , params = {}
            , first_params = {};

        var index;
        for (index = 0; index < campaign_keywords.length; ++index) {
            kw = getQueryParam(document.URL, campaign_keywords[index]);
            if (kw.length) {
                params[campaign_keywords[index] + ' [last touch]'] = kw;
            }
        }
        for (index = 0; index < campaign_keywords.length; ++index) {
            kw = getQueryParam(document.URL, campaign_keywords[index]);
            if (kw.length) {
                first_params[campaign_keywords[index] + ' [first touch]'] = kw;
            }
        }

        mixpanel.people.set(params);
        mixpanel.people.set_once(first_params);
        mixpanel.register(params);
        mixpanel.identify()
    }

    campaignParams();
</script>