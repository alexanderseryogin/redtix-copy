
                            <div class="text-center">
                                <h1 class="subSecTitle">Ticket Price</h1>
                            </div>

                            <!-- Tickets Sale -->
                            <div class="col-sm-12" id="singleticket">
                                @foreach($ticket_info as $ticket)
                                    @if($ticket["Display Type"] == "Normal" || $ticket["Display Type"] == "Hotel")
                                        <div class="col-sm-12 col-xs-12 ticketRow hidden-xs hidden-sm">
                                            <div class="col-sm-2 text-center ticketTitle">
                                                <div class="row">
                                                    <p class="tier">{{$ticket["Ticket Tier"]}}</p>
                                                    <h1>{{$ticket["Ticket Type"]}}</h1>  
                                                </div> 
                                            </div>
                                            <div class="col-sm-3 text-center ticketDay border-left">
                                                <h6>{{ $ticket["Ticket Name"] }}</h6>
                                            </div>
                                            <div class="col-sm-4 ticketAdmission border-left">
                                                <div class="col-sm-11">
                                                    <p style="float: left;">
                                                        <span class="ticketdesc">{{ $ticket["Ticket Description"] }}</span><br><span class="smallnote">{{ $ticket["More Info"] }}</span>
                                                    </p>
                                                </div>
                                                <div class="col-sm-1" style="position: absolute;right: 10px;cursor: pointer;" data-toggle="modal" data-target="#infoModal{{ $ticket["Sorting Id"] }}">
                                                    <img src="images/assets/i.png" width="20">
                                                </div>
                                            </div>
                                            <div class="col-sm-3 ticketBuy border-left">
                                                <div class="col-sm-12 text-center">
                                                    <div class="prices">
                                                        @if(strlen($ticket["Normal Price"]) !== 0)
                                                            @if($ticket["Has Forex"] == "Yes")
                                                                <span class="forexPrice">{{$ticket["Normal Price"]}}</span>
                                                                <span class="discountPrice">{{$ticket["Discount Price"]}}</span>
                                                            @else
                                                                <span class="strikethroughPrice">{{$ticket["Normal Price"]}}</span>
                                                                <span class="discountPrice">{{$ticket["Discount Price"]}}</span>
                                                            @endif
                                                        @else
                                                        <span class="standardPrice">{{$ticket["Discount Price"]}}</span>
                                                        @endif
                                                        
                                                    </div>
                                                    <a class="btn redtixBuyBtn center-block" id="buyButton" datetime="{{ $ticket["Ticket Expiry"] }}" target="_blank" href="{{ $ticket["Ticket URL"] }}">BUY NOW</a>
                                                    <small>{{$ticket["Ticket Price Terms"]}}</small>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Mobile Tickets Sales -->
                                        <div class="col-xs-12 col-sm-6 hidden-lg hidden-md ticketCard">
                                            {{-- <small class="discountTag"> 40% OFF</small> --}}
                                            <div class="col-xs-12 card-body pd-0">
                                                <div class="col-xs-12 cardTop">
                                                    <div class="col-xs-6 pd-0 ticketTitle">
                                                        <p class="ticketFlight">{{ $ticket["Ticket Name"] }}</p>
                                                        <p class="ticketTier">{{$ticket["Ticket Tier"]}} <span class="ticketCategory">{{ $ticket["Ticket Type"] }}</span></p>
                                                        <a data-toggle="modal" data-target="#infoModal{{ $ticket["Sorting Id"] }}"><img src="images/assets/i.png" width="20"></a>
                                                    </div>
                                                    <div class="col-xs-6 ticketPrice">
                                                        @if(strlen($ticket["Normal Price"]) !== 0)
                                                            @if($ticket["Has Forex"] == "Yes")
                                                                <p class="pull-right text-right forexPrice">{{ $ticket["Normal Price"] }}</p>
                                                                <p class="pull-right text-right">{{ $ticket["Discount Price"] }}</p>
                                                            @else
                                                                <p class="pull-right text-right strikethroughPrice">{{ $ticket["Normal Price"] }}</p>
                                                                <p class="pull-right text-right">{{ $ticket["Discount Price"] }}</p>
                                                            @endif
                                                        @else
                                                            <p class="pull-right text-right">{{ $ticket["Discount Price"] }}</p>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="cardBottom col-xs-12 text-center">
                                                    <p>{{ $ticket["Ticket Description"] }}<br/><span class="smallnote">{{ $ticket["More Info"] }}</span></p>
                                                    <a class="btn redtixBuyBtn center-block" id="buyButton" datetime="{{ $ticket["Ticket Expiry"] }}" target="_blank" href="{{ $ticket["Ticket URL"] }}">BUY NOW</a>
                                                    <small>{{$ticket["Ticket Price Terms"]}}</small>
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    <!-- Ticket and Flight Sales -->
                                    @if($ticket["Display Type"] == "Flight")
                                    <div class="col-sm-12 col-xs-12 ticketRowFlight hidden-xs hidden-sm">
                                        {{-- <small class="discountTag"> 40% OFF</small> --}}
                                        <div class="col-sm-12 flightTitle"><h6>{{ $ticket["Ticket Name"] }}</h6></div>
                                        <div class="col-sm-2 text-center ticketTitle">
                                            <div class="row">
                                                {{-- <p>EARLY BIRD</p> --}}
                                                <div class="clearfix" style="height: 10px;">&nbsp;</div>
                                                <p class="tier">{{$ticket["Ticket Tier"]}}</p>
                                                <h1>{{ $ticket["Ticket Type"] }}</h1>    
                                            </div>
                                        </div>
                                        <div class="col-sm-7 flightDescription">        
                                            <div class="col-sm-6 text-center border-left">
                                                <p class="plane">Departure</p>
                                                <p class="schedule">{{ date(' D, M j, Y', strtotime($ticket["Outbound Departure Date"])).", ". date('h:i a', strtotime($ticket["Outbound Departure Time"])) }}</p>
                                                <p class="destination">{{ $ticket["Outbound Departure Airport Code"] }}</p>
                                                <p class="schedule">{{ date(' D, M j, Y', strtotime($ticket["Inbound Departure Date"])).", ". date('h:i a', strtotime($ticket["Inbound Departure Time"])) }}</p>
                                                <p class="destination">{{ $ticket["Inbound Departure Airport Code"] }}</p>
                                            </div>
                                            <div class="col-sm-5 text-center">
                                                <p class="plane">Arrival</p>
                                                <p class="schedule">{{ date(' D, M j, Y', strtotime($ticket["Outbound Arrival Date"])).", ". date('h:i a', strtotime($ticket["Outbound Arrival Time"])) }}</p>
                                                <p class="destination">{{ $ticket["Outbound Arrival Airport Code"] }}</p>
                                                <p class="schedule">{{ date(' D, M j, Y', strtotime($ticket["Inbound Arrival Date"])).", ". date('h:i a', strtotime($ticket["Inbound Arrival Time"])) }}</p>
                                                <p class="destination">{{ $ticket["Inbound Arrival Airport Code"] }}</p>
                                            </div>
                                            <div class="col-sm-1 text-center">
                                                <a data-toggle="modal" data-target="#infoModal{{ $ticket["Sorting Id"] }}"><img src="images/assets/i.png" width="20"></a>
                                            </div>
                                        </div>
                                        <div class="col-sm-3 ticketBuy border-left">
                                            <div class="col-sm-12 text-center">
                                                <div class="prices">
                                                    {{-- @if(strlen($ticket["Normal Price"]) !== 0)
                                                        <span class="strikethroughPrice">{{$ticket["Normal Price"]}}</span>
                                                    @endif --}}
                                                    <span class="discountPrice">{{$ticket["Discount Price"]}}</span>
                                                </div>
                                                <a class="btn redtixBuyBtn center-block" id="buyButton" datetime="{{ $ticket["Ticket Expiry"] }}" target="_blank" href="{{ $ticket["Ticket URL"] }}">BUY NOW</a>
                                                <small>{{$ticket["Ticket Price Terms"]}}</small>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Mobile Ticket and Flight -->
                                    <div class="col-xs-12 col-sm-6 hidden-lg hidden-md ticketCardFlight">
                                        {{-- <small class="discountTag"> 40% OFF</small> --}}
                                        <div class="col-xs-12 card-body pd-0">
                                            <div class="col-xs-12 cardTop">
                                                <p class="ticketFlightCombo text-center">{{ $ticket["Ticket Name"] }}</p>
                                                <div class="col-xs-6 pd-0 ticketTitle">
                                                    <p class="ticketTier">{{$ticket["Ticket Tier"]}} <span class="ticketCategory"> {{ $ticket["Ticket Type"] }} </span> <a data-toggle="modal" data-target="#infoModal{{ $ticket["Sorting Id"] }}"><img src="images/assets/i.png" width="20"></a></p>
                                                </div>
                                                <div class="col-xs-6 ticketPrice">
                                                    {{-- @if(strlen($ticket["Normal Price"]) !== 0)
                                                        <p class="pull-right text-right strikethroughPrice">{{ $ticket["Normal Price"] }}</p>
                                                    @endif --}}
                                                    <p class="pull-right text-right">{{ $ticket["Discount Price"] }}</p>
                                                </div>
                                            </div>
                                            <div class="cardBottom col-xs-12 text-center">
                                                <table style="margin:auto">
                                                    <tr>
                                                        <th class="text-center">Date</th>
                                                        <th class="text-left">Departure</th>
                                                        <th class="text-left">Arrival</th>
                                                    </tr>
                                                    <tr>
                                                        <td>{{ date(' D, M j, Y', strtotime($ticket["Outbound Departure Date"])) }}</td>
                                                        <td><span class="place">{{ $ticket["Outbound Departure Airport Code"] }} </span>{{ date('h:i a', strtotime($ticket["Outbound Departure Time"])) }}</td>
                                                        <td><span class="place">{{ $ticket["Outbound Arrival Airport Code"] }} </span>{{ date('h:i a', strtotime($ticket["Outbound Arrival Time"])) }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>{{ date(' D, M j, Y', strtotime($ticket["Inbound Departure Date"])) }}</td>
                                                        <td><span class="place">{{ $ticket["Inbound Departure Airport Code"] }} </span>{{ date('h:i a', strtotime($ticket["Inbound Departure Time"])) }}</td>
                                                        <td><span class="place">{{ $ticket["Inbound Arrival Airport Code"] }} </span>{{ date('h:i a', strtotime($ticket["Inbound Arrival Time"])) }}</td>
                                                    </tr>
                                                </table>
                                                <div class="col-xs-12 flightBuy">
                                                    @if(strlen($ticket["Normal Price"]) !== 0)
                                                        @if(strlen($ticket["Ticket URL"]) !== 0)
                                                            <a class="btn redtixBuyBtn center-block" id="buyButton" datetime="{{ $ticket["Ticket Expiry"] }}" target="_blank" href="{{ $ticket["Ticket URL"] }}">BUY NOW</a>
                                                        @endif
                                                        <p class="small" style="padding-top: 5px; line-height: 1.3; font-size: 80%;">{{$ticket["Ticket Price Terms"]}}</p>
                                                    @else
                                                        <div class="clearfix" style="height:30px;">&nbsp;</div>
                                                        <h6>N/A</h6>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                        @endforeach
                                    </div>
                                    <!-- End Ticket Sale -->