<!-- Modal Subscribe Newsletter-->
<div id="modalSubscribeNewsletter" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body text-center">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
                <div class="clearfix">&nbsp;</div>
                <h4>Newsletter</h4>
                <p>Be the first to find out about exciting upcoming events<br>and exclusive offers!</p>
                <form action="//AirAsiaRedtix.us14.list-manage.com/subscribe/post?u=563fc1bcdefdc4fca4efb1dbe&amp;id=719d8ddc42" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                    <div id="mc_embed_signup_scroll">
                        <div class="col-sm-8 col-xs-12">
                            <input type="email" value="" name="EMAIL" class="email form-control" id="mce-EMAIL" placeholder="email address" required>
                            <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_563fc1bcdefdc4fca4efb1dbe_719d8ddc42" tabindex="-1" value=""></div>
                        </div>
                        <div class="clearfix visible-xs">&nbsp;</div>
                        <div class="col-sm-4 col-xs-12">
                            <input type="submit" onclick="register_newsletter()" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button btn btn-danger btn-block">
                        </div>
                    </div>
                </form>
                <div class="clearfix">&nbsp;</div>
                <div class="clearfix">&nbsp;</div>
                </div>
            </div>
        </div>
    </div>
</div>


