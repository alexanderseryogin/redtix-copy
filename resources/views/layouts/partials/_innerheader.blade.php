<!-- Header & Navigation -->
<header class="innerHeader">
    <div class="container">
        <nav class="navbar navbar-empty" role="navigation">
            <div class="navbar-header" style="width: 100%;">
            <a class="navbar-brand" href="index.html"><img src="images/redtrix_logo.png" alt=""></a>
            <a class="search-btn pull-right" href="event.html">Search <i class="fa fa-search" aria-hidden="true"></i></a>
        </div>
    </div>
</header>