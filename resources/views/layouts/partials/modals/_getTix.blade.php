<!-- Modal Where to Get Tix Location-->
<div id="modalGetTixLoc" class="modal fade" role="dialog">
    <div class="modal-dialog">
    <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
                <h2>GET YOUR TICKETS FROM:</h2>
                <dl class="dl-horizontal">
                    <dt>Online:</dt>
                    <dd><a href="http://www.airasiaredtix.com">www.AirAsiaRedtix.com</a></dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>Rock Corner outlets:</dt>
                    <dd>The Gardens, Mid Valley (TEL: 03 - 2201 4893)</dd>
                    {{-- <dd>The Curve (TEL: 03 - 7733 1139)</dd> --}}
                </dl>
                {{-- <dl class="dl-horizontal">
                    <dt>Victoria Music outlets:</dt>
                    <dd>Sg Wang(TEL: 03 - 2148 7208)</dd>
                    <dd>Amcorp (TEL: 03 - 7956 0592)</dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>Penang outlets:</dt>
                    <dd>Artist Gallery, Gurney Plaza (04-637 0191)</dd>
                    <dd>Artist Gallery, Queensbay Mall (04-228 5648)</dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>Istana Budaya Jalan<br>Tun Razak:</dt>
                    <dd>03 40265558</dd>
                </dl> --}}
            </div>
        </div>
    </div>
</div>