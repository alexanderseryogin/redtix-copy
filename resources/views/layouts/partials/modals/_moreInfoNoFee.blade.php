
@foreach($ticket_info as $ticket)

{{-- ticket info modal --}}
<div id="infoModal{{ $ticket["Sorting Id"] }}" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-body text-center pd-0">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="padding: 16px;"><i class="fa fa-times" aria-hidden="true"></i></button>
            <div class="row ultraModalRow info-box">

                <div class="details">
                    <h6 class="type">{{$ticket["Ticket Type Desc"]}}</h6>
                    <h6 class="days">{{$ticket["Ticket Name"]}}</h6>
                </div>
                <div class="details">
                    <ul>
                        <li><span class="fa fa-tag"></span>{{$ticket["More Info Type"]}} : {{$ticket["Ticket Tier"]}}</li>
                        <li><span class="fa fa-calendar"></span>{{$ticket["More Info Date"]}}</li>
                        <li><span class="fa fa-map-marker"></span>{{$ticket["More Info Location"]}}</li>
                    </ul>   
                </div>

                @if($ticket["Display Type"] == "Flight")
                    <div class="details">
                    {{$ticket["More Info"]}}
                    </div>
                @endif
                @if($ticket["Display Type"] == "Hotel")
                    <div class="details">
                    {{$ticket["More Info"]}}
                    </div>
                @endif
                @if($ticket["Display Type"] == "PGA")
                    <div class="services col-md-12 hidden-xs hidden-sm text-center">
                        <div class="col-md-4">
                            <img src="images/assets/moreinfo/flag.png" style="width:50px;"><br />
                            <span>Express Lane</span>
                        </div>
                        <div class="col-md-4">
                            <img src="images/assets/moreinfo/drink.png"><br />
                            <span>Bar Access</span>
                        </div>
                            <img src="images/assets/moreinfo/beach.png"><br />
                            <span>Private Lounge</span>
                        </div>
                        </div>
                        <div class="services col-md-12 hidden-xs hidden-sm text-center">
                        <div class="col-md-4">
                            <img src="images/assets/moreinfo/rfid-chip.png"><br />
                            <span>Express RFID Top-up</span>
                        </div>
                        <div class="col-md-4">
                            <img src="images/assets/moreinfo/watch-resolution.png"><br />
                            <span>RFID Wristband</span>
                        </div> 
                        <div class="col-md-4">
                            <img src="images/assets/moreinfo/ep_mono.png"><br />
                            <span>Insurance</span>
                        </div>
                        </div>
                        <div class="services col-md-12 hidden-xs hidden-sm text-center">
                        <div class="col-md-4">
                            <img src="images/assets/moreinfo/restroom-sign.png"><br />
                            <span>Private Restroom</span>
                        </div>
                        <div class="col-md-4">
                            <img src="images/assets/moreinfo/18-plus.png"><br />
                            <span>Above 18+</span>
                        </div>

                        <div class="col-md-4">
                            <img src="images/assets/moreinfo/no-entry.png"><br />
                            <span>No re-entry</span>
                        </div> 
                    </div>
                    <div class="services col-xs-12 hidden-md hidden-lg text-center">
                        <div class="col-xs-4">
                            <img src="images/assets/moreinfo/flag.png" style="width:50px;"><br />
                            <span>Express Lane</span>
                        </div>
                        <div class="col-xs-4">
                            <img src="images/assets/moreinfo/drink.png"><br />
                            <span>Bar Access</span>
                        </div>
                            <img src="images/assets/moreinfo/beach.png"><br />
                            <span>Private Lounge</span>
                        </div>
                    </div>
                    <div class="services col-xs-12 hidden-md hidden-lg text-center">
                        <div class="col-xs-4">
                            <img src="images/assets/moreinfo/rfid-chip.png"><br />
                            <span>Express RFID Top-up</span>
                        </div>
                        <div class="col-xs-4">
                            <img src="images/assets/moreinfo/watch-resolution.png"><br />
                            <span>RFID Wristband</span>
                        </div> 
                        <div class="col-xs-4">
                            <img src="images/assets/moreinfo/ep_mono.png"><br />
                            <span>Insurance</span>
                        </div>
                    </div>
                    <div class="services col-xs-12 hidden-md hidden-lg text-center">
                        <div class="col-xs-4">
                            <img src="images/assets/moreinfo/restroom-sign.png"><br />
                            <span>Private Restroom</span>
                        </div>
                        <div class="col-xs-4">
                            <img src="images/assets/moreinfo/18-plus.png"><br />
                            <span>Above 18+</span>
                        </div>
                        <div class="col-xs-4">
                            <img src="images/assets/moreinfo/no-entry.png"><br />
                            <span>No re-entry</span>
                        </div> 
                    </div>
                @endif

                @if($ticket["Display Type"] == "GA")
                   <div class="services col-md-12 col-md-push-1 hidden-xs hidden-sm text-center">
                        <div class="col-md-2">
                            <img src="images/assets/moreinfo/drink.png"><br />
                            <span>Bar Access</span>
                        </div> 

                        <div class="col-md-2">
                            <img src="images/assets/moreinfo/ep_mono.png"><br />
                            <span>Insurance</span>
                        </div> 
                        <div class="col-md-2">
                            <img src="images/assets/moreinfo/watch-resolution.png"><br />
                            <span>RFID Wristband</span>
                        </div> 

                        <div class="col-md-2">
                            <img src="images/assets/moreinfo/18-plus.png"><br />
                            <span>Above 18+</span>
                        </div> 

                        <div class="col-md-2">
                            <img src="images/assets/moreinfo/no-entry.png"><br />
                            <span>No re-entry</span>
                        </div> 
                    </div>
                   <div class="services col-xs-12 hidden-md hidden-lg text-center">
                        <div class="col-xs-4">
                            <img src="images/assets/moreinfo/drink.png"><br />
                            <span>Bar Access</span>
                        </div> 
                        <div class="col-xs-4">
                            <img class="list-icon" src="images/assets/moreinfo/ep_mono.png"><br />
                            <span>Insurance</span>
                        </div> 

                        <div class="col-xs-4">
                            <img class="list-icon" src="images/assets/moreinfo/watch-resolution.png"><br />
                            <span>RFID Wristband</span>
                        </div> 
                    </div>
                    <div class="services col-xs-12 hidden-md hidden-lg text-center">
                        <div class="col-xs-6 col-sm-2">
                            <img class="list-icon" src="images/assets/moreinfo/18-plus.png"><br />
                            <span>Above 18+</span>
                        </div> 

                        <div class="col-xs-6 col-sm-2">
                            <img class="list-icon" src="images/assets/moreinfo/no-entry.png"><br />
                            <span>No re-entry</span>
                        </div> 
                    </div>  
                @endif
                <div class="copy_text text-center">
                    {{-- *Ticketing fee &amp; credit card <br/>
                    charges will be added at the checkout basket.<br/> --}}
                    <a href="purchaseterms">Terms &amp; Conditions</a> apply.
                </div>    

            </div> 
        </div>
      </div>
    </div>
</div>


@endforeach