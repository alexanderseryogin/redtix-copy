<!--Modal Seat Plan image-->
<div class="modal fade" id="enlargeImageModal" tabindex="-1" role="dialog" aria-labelledby="enlargeImageModal" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
                <div class="clearfix">&nbsp;</div>
                <img src="" class="enlargeImageModalSource" style="width: 100%;">
            </div>
        </div>
    </div>
</div>