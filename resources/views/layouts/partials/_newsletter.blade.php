<section class="newsletter-Home">
    <div class="container">
    <div class="col-sm-5 leftBox hidden-xs">
        <img src="images/paper-plane.png" alt="">
    </div>
    <div class="col-sm-7 rightBox">
        <span class="newsTitle">Always First.</span>
        <p>Be the first to find out on exciting upcoming events<br>and exclusive offers!</p>
        <div class="form-group row">
            <form action="//AirAsiaRedtix.us14.list-manage.com/subscribe/post?u=563fc1bcdefdc4fca4efb1dbe&amp;id=719d8ddc42" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                <div id="mc_embed_signup_scroll">
                    <div class="col-xs-8">
                        <input type="email" value="" name="EMAIL" class="email form-control" id="mce-EMAIL" placeholder="email address" required>
                        <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_563fc1bcdefdc4fca4efb1dbe_719d8ddc42" tabindex="-1" value=""></div>
                    </div>
                    <div class="col-xs-4">
                        <input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button btn btn-danger btn-block">
                    </div>
                </div>
            </form>
        </div>        
    </div>
    </div>
</section>