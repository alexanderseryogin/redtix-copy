<section id="footer">
        <div class="container-fluid">
            <div class="content">
                <a class="logo" href="index.html">
                    <img src="{{asset('images/rugby2019/logo.png')}}" alt="RedTix logo">
                </a>
                <div class="footer-content">
                    <div class="socials-wrapper">
                        <h3 class="title">Follow</h3>
                        <ul class="component-socials">

                            <li class="social social-instagram">
                                <a target="_blank" href="https://www.instagram.com/AirAsiaRedTix/"></a>
                            </li>

                            <li class="social social-youtube">
                                <a target="_blank" href="https://www.youtube.com/channel/UCGs2OxKX4WQzmDGuwwNd9-g"></a>
                            </li>

                            <li class="social social-twitter">
                                <a target="_blank" href="https://twitter.com/redtix"></a>
                            </li>

                            <li class="social social-facebook">
                                <a target="_blank" href="https://www.facebook.com/RedTix"></a>
                            </li>

                        </ul>
                    </div>
                    <div class="text-wrapper">
                        <div class="text">
                            RedTix.com is the hottest, <br class="lg-show">
                            smartest new way to discover, discuss, review <br class="lg-show">
                            and book tickets to an international line&#8209;up <br class="lg-show">
                            of concerts, sporting events, musicals, <br class="lg-show">
                            theatre performances and more.
                        </div>
                        <ul class="nav">
                            <li class="nav-item">
                                <a class="nav-link" href="/websiteterms">
                                    Website Terms & Conditions
                                </a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="/eventregistration">
                                    Register Your Details
                                </a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="/privacy">
                                    Privacy
                                </a>
                            </li>

                        </ul>
                    </div>
                </div>
                <div class="copyright">
                    <p class="rugby-cup">
                        The Rugby World Cup 2019 logo TM © Rugby World Cup Limited 2015. All rights reserved.
                    </p>
                    <p>
                        Redtix Sdn. Bhd., a subsidiary of AirAsia Berhad. ©2007-2018 All rights reserved.
                    </p>
                </div>
            </div>
        </div>
    </section>


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
{{-- {!! Html::script('js/app.js') !!} --}}
<script type="text/javascript" src="js/app.min.js"></script>
<script type="text/javascript" src="/js/scrollToFixed.js"></script>

<!-- Featherlight lightbox -->
<script src="https://cdn.rawgit.com/noelboss/featherlight/1.7.7/release/featherlight.min.js" type="text/javascript" charset="utf-8"></script>
<script src="https://cdn.rawgit.com/noelboss/featherlight/1.7.7/release/featherlight.gallery.min.js" type="text/javascript" charset="utf-8"></script>

<!-- Go to www.addthis.com/dashboard to customize your tools -->
<!-- <script type="text/javascript" src="https://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-587468fe5d4abf72"></script> -->

<script type="text/javascript">
    var register_newsletter = function(){
        var user_email = document.getElementById('mce-EMAIL').value;
        var user_name = user_email.split("@")[0];
        mixpanel.alias(user_email);
        mixpanel.people.set({
          "$name": user_name,
          "$email": user_email
        });
        mixpanel.identify(user_email);
        mixpanel.track('register_newsletter',
        {
            Email: user_email
        });
    }
</script>

@if (App::environment('production'))
<!-- Mixpanel last touch attributes -->
<script type="text/javascript">
    function getQueryParam(url, param) {
        // Expects a raw URL
        param = param.replace(/[[]/, "\[").replace(/[]]/, "\]");
        var regexS = "[\?&]" + param + "=([^&#]*)",
            regex = new RegExp( regexS ),
            results = regex.exec(url);
        if (results === null || (results && typeof(results[1]) !== 'string' && results[1].length)) {
            return '';
        } else {
            return decodeURIComponent(results[1]).replace(/\W/gi, ' ');
        }
    };

    function campaignParams() {
        var campaign_keywords = 'utm_source utm_medium utm_campaign utm_content utm_term'.split(' ')
            , kw = ''
            , params = {}
            , first_params = {};

        var index;
        for (index = 0; index < campaign_keywords.length; ++index) {
            kw = getQueryParam(document.URL, campaign_keywords[index]);
            if (kw.length) {
                params[campaign_keywords[index] + ' [last touch]'] = kw;
            }
        }
        for (index = 0; index < campaign_keywords.length; ++index) {
            kw = getQueryParam(document.URL, campaign_keywords[index]);
            if (kw.length) {
                first_params[campaign_keywords[index] + ' [first touch]'] = kw;
            }
        }

        mixpanel.people.set(params);
        mixpanel.people.set_once(first_params);
        mixpanel.register(params);
        mixpanel.identify()
    }

    campaignParams();
@endif
</script>