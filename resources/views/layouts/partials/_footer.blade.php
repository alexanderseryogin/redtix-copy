<!-- Footer -->
<style>
    #footer {
        background-image: url(/images/rugby2019/footer-bg-m.png), linear-gradient(250.26deg, #f13b54 2.06%, #c33a8c 59.34%);
        background-repeat: no-repeat;
        background-position: left bottom, center;
        background-size: 46.1%, auto;
        color: #ffffff;
        font-family: Roboto;
        font-weight: 300;
        text-align: center;
        padding: 76px 0 18px;
    }
    #footer li {
        list-style-type: none;
    }
    #footer .logo {
        display: inline-block;
    }
    #footer .logo img {
        width: 105px;
    }
    #footer .footer-content {
        display: flex;
        flex-direction: column;
    }
    #footer .text-wrapper {
        display: flex;
        order: 2;
        padding-top: 78px;
    }
    #footer .text-wrapper .text {
        margin-top: 7px;
        padding: 0 12% 0 51%;
        min-height: 190px;
        font-style: normal;
        font-weight: 300;
        font-size: 14px;
        line-height: normal;
        text-align: right;
        letter-spacing: 0.02em;
    }
    #footer .text-wrapper .nav {
        display: none;
    }
    #footer .title {
        padding-top: 44px;
        text-transform: uppercase;
        letter-spacing: 0.31em;
        font-size: 14px;
        font-weight: 700;
        margin-top: 0;
    }
    #footer .socials-wrapper {
        order: 1;
    }
    #footer .component-socials {
        margin-top: 14px;
    }
    #footer .copyright {
        padding: 93px 0 0 82px;
        font-style: normal!important;
        font-weight: 300!important;
        font-size: 12px!important;
        line-height: normal!important;
        text-align: center!important;
        letter-spacing: 0.02em!important;
    }
    #footer .copyright p {
        margin: 2px 0;
        letter-spacing: 0.02em;
        line-height: 1;
        color: #ffffff;
    }
    #footer .copyright p.rugby-cup {
        display: none;
    }
    #footer .lg-show {
        display: none;
    }

    @media (min-width: 546px) {
        #footer {
            background-size: 251px, auto;
        }
    }

    @media (min-width: 992px) {
        #footer {
            background-image: url(/images/rugby2019/footer-bg.png) , linear-gradient(250.26deg, #f13b54 2.06%, #c33a8c 59.34%);;
            background-size: auto;
            background-position: right 142px bottom, center;
            padding: 35px 0 24px;
            text-shadow: 0 1px 0 #c33a8c, 0 -1px 0 #c33a8c, 1px 0 0 #c33a8c, -1px 0 0 #c33a8c;
        }
        #footer .content {
            width: 50%;
        }
        #footer .logo {
            margin-right: 75px;
        }
        #footer .logo img {
            width: 71px;
        }
        #footer .text-wrapper {
            padding-top: 35px;
            order: 1;
        }
        #footer .text-wrapper .text {
            min-height: 0;
            text-align: right;
            font-size: 14px;
            line-height: 20px;
            width: 50%;
            padding: 0;
        }
        #footer .text-wrapper .nav {
            display: block;
            width: 50%;
            padding-left: 58px;
        }
        #footer .text-wrapper .nav .nav-item {
            text-align: left;
        }
        #footer .text-wrapper .nav .nav-link {
            color: #ffffff;
            font-style: normal!important;
            font-weight: 300!important;
            font-size: 14px!important;
            line-height: normal!important;
            letter-spacing: 0.02em;
            padding: 0;
            margin: 10px 0;
        }
        #footer .text-wrapper .nav .nav-link:hover {
            background-color: transparent !important;
            border-color: transparent !important;
        }
        #footer .title {
            padding-top: 81px;
            font-weight: 300;
        }
        #footer .socials-wrapper {
            order: 2;
        }
        #footer .copyright {
            padding: 155px 0 0;
        }
        #footer .copyright p {
            font-size: 10px;
        }
        #footer .copyright p.rugby-cup {
            display: block;
        }
    }

    @media (min-width: 1200px) {
        #footer .lg-show {
            display: block;
        }
    }

    /* socials */
    .component-socials {
        display: flex;
        justify-content: center;
    }
    .component-socials .social {
        width: 20px;
        background: no-repeat center;
        height: 41px;
        margin: 0 9px;
    }
    .component-socials .social a {
        display: block;
        width: 100%;
        height: 100%;
    }
    .component-socials .social.social-instagram  {
        background-size: 22px;
        width: 47px;
        background-image: url(/images/rugby2019/socials/instagram.svg);
    }
    .component-socials .social.social-youtube  {
        background-size: 27px;
        width: 42px;
        background-image: url(/images/rugby2019/socials/youtube.svg);
    }
    .component-socials .social.social-twitter  {
        background-size: 27px;
        width: 47px;
        background-image: url(/images/rugby2019/socials/twitter.svg);
    }
    .component-socials .social.social-facebook {
        background-size: 12px;
        width: 32px;
        background-image: url(/images/rugby2019/socials/facebook.svg);
    }

    @media (min-width: 992px) {
        .component-socials .social {
            height: 39px;
            margin: 0 20px;
        }
        .component-socials .social.social-instagram  {
            background-size: 19px;
            width: 39px;
        }
        .component-socials .social.social-youtube  {
            background-size: 25px;
            width: 45px;
        }
        .component-socials .social.social-twitter  {
            background-size: 23px;
            width: 43px;
        }
        .component-socials .social.social-facebook {
            background-size: 10px;
            width: 30px;
        }
    }

</style>
<section id="footer">
    <div class="container-fluid">
        <div class="content">
            <a class="logo" href="index.html">
                <img src="{{asset('images/rugby2019/logo.png')}}" alt="RedTix logo">
            </a>
            <div class="d-flex flex-column">
                <div class="socials-wrapper order-lg-2">
                    <h3 class="title">Follow</h3>
                    <ul class="component-socials">

                        <li class="social social-instagram">
                            <a target="_blank" href="https://www.instagram.com/AirAsiaRedTix/"></a>
                        </li>

                        <li class="social social-youtube">
                            <a target="_blank" href="https://www.youtube.com/channel/UCGs2OxKX4WQzmDGuwwNd9-g"></a>
                        </li>

                        <li class="social social-twitter">
                            <a target="_blank" href="https://twitter.com/redtix"></a>
                        </li>

                        <li class="social social-facebook">
                            <a target="_blank" href="https://www.facebook.com/RedTix"></a>
                        </li>

                    </ul>
                </div>
                <div class="text-wrapper d-flex order-lg-1">
                    <div class="text">
                        RedTix.com is the hottest, <br class="d-none d-xl-block">
                        smartest new way to discover, discuss, review <br class="d-none d-xl-block">
                        and book tickets to an international line&#8209;up <br class="d-none d-xl-block">
                        of concerts, sporting events, musicals, <br class="d-none d-xl-block">
                        theatre performances and more.
                    </div>
                    <ul class="nav flex-column d-none d-lg-block">

                        <li class="nav-item">
                            <a class="nav-link" href="/websiteterms">
                                Website Terms & Conditions
                            </a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="/eventregistration">
                                Register Your Details
                            </a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="/privacy">
                                Privacy
                            </a>
                        </li>

                    </ul>
                </div>
            </div>
            <div class="copyright">
                <p class="d-none d-lg-block">
                    The Rugby World Cup 2019 logo TM © Rugby World Cup Limited 2015. All rights reserved.
                </p>
                <p>
                    Redtix Sdn. Bhd., a subsidiary of AirAsia Berhad. ©2007-2018 All rights reserved.
                </p>
            </div>
        </div>
    </div>
</section>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
{{-- {!! Html::script('js/app.js') !!} --}}
<script type="text/javascript" src="/js/app.min.js"></script>
<script type="text/javascript" src="/js/scrollToFixed.js"></script>

<!-- Featherlight lightbox -->
<script src="https://cdn.rawgit.com/noelboss/featherlight/1.7.7/release/featherlight.min.js" type="text/javascript" charset="utf-8"></script>
<script src="https://cdn.rawgit.com/noelboss/featherlight/1.7.7/release/featherlight.gallery.min.js" type="text/javascript" charset="utf-8"></script>

<!-- Go to www.addthis.com/dashboard to customize your tools -->
<!-- <script type="text/javascript" src="https://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-587468fe5d4abf72"></script> -->

<script type="text/javascript">
    var register_newsletter = function(){
        var user_email = document.getElementById('mce-EMAIL').value;
        var user_name = user_email.split("@")[0];
        mixpanel.alias(user_email);
        mixpanel.people.set({
          "$name": user_name,
          "$email": user_email
        });
        mixpanel.identify(user_email);
        mixpanel.track('register_newsletter',
        {
            Email: user_email
        });
    }
</script>

@if (App::environment('production'))
<!-- Mixpanel last touch attributes -->
<script type="text/javascript">
    function getQueryParam(url, param) {
        // Expects a raw URL
        param = param.replace(/[[]/, "\[").replace(/[]]/, "\]");
        var regexS = "[\?&]" + param + "=([^&#]*)",
            regex = new RegExp( regexS ),
            results = regex.exec(url);
        if (results === null || (results && typeof(results[1]) !== 'string' && results[1].length)) {
            return '';
        } else {
            return decodeURIComponent(results[1]).replace(/\W/gi, ' ');
        }
    };

    function campaignParams() {
        var campaign_keywords = 'utm_source utm_medium utm_campaign utm_content utm_term'.split(' ')
            , kw = ''
            , params = {}
            , first_params = {};

        var index;
        for (index = 0; index < campaign_keywords.length; ++index) {
            kw = getQueryParam(document.URL, campaign_keywords[index]);
            if (kw.length) {
                params[campaign_keywords[index] + ' [last touch]'] = kw;
            }
        }
        for (index = 0; index < campaign_keywords.length; ++index) {
            kw = getQueryParam(document.URL, campaign_keywords[index]);
            if (kw.length) {
                first_params[campaign_keywords[index] + ' [first touch]'] = kw;
            }
        }

        mixpanel.people.set(params);
        mixpanel.people.set_once(first_params);
        mixpanel.register(params);
        mixpanel.identify()
    }

    campaignParams();
@endif
</script>