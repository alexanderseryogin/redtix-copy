<aside class="col-sm-3 mainSidebarContent hidden-xs">
    <!--Need Help-->
    <section>
        <h1 class="mainSecTitle">Need <span>Help?</span></h1>
        <p><strong>AirAsiaRedTix.com</strong></p>
        <p>We will be offering an international line-up of concerts, sporting events, musicals and theatre performances in the coming months. More updates coming your way!</p>
        <a href="mailto:support@airasiaredtix.com">Contact us <i class="fa fa-question-circle" aria-hidden="true"></i></a>
    </section>
    <!-- /Need Help-->
</aside>