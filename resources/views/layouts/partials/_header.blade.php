<!-- Header & Navigation -->
<header class="mainHeader">
    <div class="container">
    <nav class="navbar navbar-empty" role="navigation">
        <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-01">
            <span class="sr-only">Toggle navigation</span>
        </button>
        <a class="navbar-brand" href="/"><img src="{!! asset('images/redtix_logo.png') !!}" alt=""></a>
        </div>
        <div class="collapse navbar-collapse" id="navbar-collapse-01">
        <ul class="nav navbar-nav pull-right">
            <li><a href="/about">About Us</a></li>
            <li><a href="/faq">FAQ</a></li>
            <li><a class="newsletter-nav" data-toggle="modal" data-target="#modalSubscribeNewsletter">Newsletter</a></li>
            <li><a href="mailto:support@airasiaredtix.com">Contact Us</a></li>
        </ul>
        </div><!-- /.navbar-collapse -->
    </nav><!-- /navbar -->
    </div>
</header>