<!-- Subscribe Newsletter Popup at bottom screen-->
<section class="newsletterPop-footer">
    <span id="dismiss" href=""><i class="fa fa-times" aria-hidden="true"></i></span>
    <div class="container">
    <!--<div class="toSubscribe">
        <div class="col-sm-7">
        <h2>Subscribe to the newsletter</h2>
        <p>so you don't miss out on promotions & events in South East Asia.</p>
        </div>
        <div class="col-sm-5">
        <div class="row">
            <form class="form-inline" action="">
            <div class="form-group col-xs-8">
                <input type="email" class="form-control" id="inputPassword" placeholder="Email Address">
            </div>
            <button type="submit" class="btn btn-black">Subscribe</button>
            </form>
        </div>
        </div>
    </div>-->
    <div class="doneSubscribe">
        <div class="col-xs-12">
            <img class="subscribeChecked " src="images/checked.png" alt="">
            <div class="text-box">
                <h2>Thanks for subscribing.</h2>
                <p>Be on the lookout for exciting updates in your inbox!</p>
            </div>
        </div>
    </div>
    </div>
</section>