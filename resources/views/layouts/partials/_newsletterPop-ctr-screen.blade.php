<!-- Subscribe Newsletter Modal center screen-->
<div id="newsletterPop-body" class="modal fade" role="dialog">
    <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-body text-center">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
        <div class="clearfix">&nbsp;</div>
        <div class="clearfix">&nbsp;</div>
        
        <div class="toSubscribe">
            <p>Subscribe to the newsletter so you dont't miss out on promotions & events in South East Asia.</p>
            <div class="clearfix">&nbsp;</div>
            <div class="form-group row">
            <div class="col-sm-8 col-xs-12"><input type="text" placeholder="Email Address" class="form-control" /></div>
            <div class="clearfix visible-xs">&nbsp;</div>
            <div class="col-sm-4 col-xs-12"><a class="btn btn-danger btn-block" href="#" role="button">Subscribe</a></div>
        </div>

        <!--<div class="doneSubscribe">
            <img class="subscribeChecked" src="images/checked-green.png" alt="">
            <div class="text-box">
            <h2>Thanks for subscribing.</h2>
            <p>Be on the lookout for exciting updates in your inbox!</p>
            </div>
        </div>-->

        <div class="clearfix hidden-xs">&nbsp;</div>
        <div class="clearfix">&nbsp;</div>
        </div>
        </div>
    </div>
    </div>
</div>