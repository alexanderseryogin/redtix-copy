{{-- ticket only --}}
<div class="col-sm-12 col-xs-12 ticketRow hidden-xs hidden-sm">
    {{-- <small class="discountTag"> 40% OFF</small> --}}
    <div class="col-sm-2 text-center ticketTitle">
        <div class="row">
            {{-- <p>Premium</p> --}}
            <div class="clearfix" style="height: 10px;">&nbsp;</div>
            <h1>{{ $ticket["Ticket Type"] }}</h1>    
        </div> 
    </div>
    <div class="col-sm-3 text-center ticketDay border-left">
        <h6>{{ $ticket["Ticket Name"] }}</h6>
    </div>
    <div class="col-sm-4 ticketAdmission border-left">
        <p>{{ $ticket["Ticket Description"] }}</p>
    </div>
    <div class="col-sm-3 ticketBuy border-left">
        <div class="col-sm-12 text-center">
            @if(strlen($ticket["Private Sale Front End"]) !== 0)
                <h6>{{ $ticket["Private Sale Front End"] }}</h6>
                {{-- @if(strlen($ticket["Ticketserv URL"]) !== 0) --}}
                {{-- <a class="btn ultraBuyBtn center-block" data-toggle="modal" data-target="#buyModal{{ $ticket["Sorting ID"] }}">BUY NOW</a> --}}
                <a class="btn ultraBuyBtn center-block" href="{{ $ticket["Ticketserv URL"] }}" onclick="gtag_report_conversion();">BUY NOW</a>
                {{-- @endif --}}
                <p class="small" style="line-height: 1.3; font-size: 80%;">Ticketing fee, event insurance & credit card charges will be added at the checkout basket</p>
            @else
                <div class="clearfix" style="height:30px;">&nbsp;</div>
                <h6>N/A</h6>
            @endif
           {{--  <h6>{{ $ticket["Private Sale Front End"] }}</h6>
            <a class="btn ultraBuyBtn center-block" id="buyButton" datetime="Feb 10 2018 00:00:00 GMT+0800" target="_blank" href="{{ $ticket["Ticketserv URL"] }}">BUY NOW</a>
            <small>10 tickets left at this tier</small> --}}
        </div>
    </div>
</div>

<!-- mobile ticket ONLY card responsive -->
<div class="col-xs-12 col-sm-6 hidden-lg hidden-md ticketCard">
    {{-- <small class="discountTag"> 40% OFF</small> --}}
    <div class="col-xs-12 card-body pd-0">
        <div class="col-xs-12 cardTop">
            <div class="col-xs-6 pd-0 ticketTitle">
                <p class="ticketFlight">{{ $ticket["Ticket Name"] }}</p>
                <p class="ticketTier">Tier 1<span class="ticketCategory">{{ $ticket["Ticket Type"] }}</span></p>
            </div>
            <div class="col-xs-6 ticketPrice">
                @if(strlen($ticket["Private Sale Front End"]) !== 0)
                   {{-- <p class="pull-right text-right">{{ $ticket["Private Sale Front End"] }}<br><small>10 tickets left at this tier</small></p> --}}
                    <p class="pull-right text-right">{{ $ticket["Private Sale Front End"] }}</p>
                @else
                    <p class="pull-right text-right">N/A</p>
                @endif
            </div>
        </div>
        <div class="cardBottom col-xs-12 text-center">
            <p>{{ $ticket["Ticket Description"] }}</p>
            {{-- @if(strlen($ticket["Ticketserv URL"]) !== 0) --}}
                {{-- <a class="btn ultraBuyBtn center-block" data-toggle="modal" data-target="#buyModal{{ $ticket["Sorting ID"] }}">BUY NOW</a> --}}
                <a class="btn ultraBuyBtn center-block" id="buyButton" datetime="Feb 17 2018 00:00:00 GMT+0800" target="_blank" href="{{ $ticket["Ticketserv URL"] }}" onclick="gtag_report_conversion();">BUY NOW</a>
            {{-- @endif --}}
        </div>
    </div>
</div>
