{{-- ticket + flight --}}
<div class="col-sm-12 col-xs-12 ticketRowFlight hidden-xs hidden-sm">
    {{-- <small class="discountTag"> 40% OFF</small> --}}
    {{-- <a class="infobtn pull-right" data-toggle="modal" data-target="#flightModal{{ $ticket["Sorting ID"] }}"><i class="material-icons">info_outline</i></a> --}}
    <div class="col-sm-12 flightTitle"><h6>{{ $ticket["Ticket Name"] }}</h6></div>
    <div class="col-sm-2 text-center ticketTitle">
        <div class="row">
            {{-- <p>EARLY BIRD</p> --}}
            <div class="clearfix" style="height: 10px;">&nbsp;</div>
            <p class="tier">Tier 3</p>
            <h1>{{ $ticket["Ticket Type"] }}</h1>    
        </div>
    </div>
    <div class="col-sm-7 flightDescription">
        
        <div class="col-sm-6 text-center border-left">
            <p class="plane">Departure</p>
            <p class="schedule">{{ date(' D, M j, Y', strtotime($ticket["Outbound Departure Date"])).", ". date('h:i a', strtotime($ticket["Outbound Departure Time"])) }}</p>
            <p class="destination">{{ $ticket["Outbound Departure Airport Code"] }}</p>
            <p class="schedule">{{ date(' D, M j, Y', strtotime($ticket["Inbound Departure Date"])).", ". date('h:i a', strtotime($ticket["Inbound Departure Time"])) }}</p>
            <p class="destination">{{ $ticket["Inbound Departure Airport Code"] }}</p>
        </div>
        <div class="col-sm-5 text-center">
            <p class="plane">Arrival</p>
            <p class="schedule">{{ date(' D, M j, Y', strtotime($ticket["Outbound Arrival Date"])).", ". date('h:i a', strtotime($ticket["Outbound Arrival Time"])) }}</p>
            <p class="destination">{{ $ticket["Outbound Arrival Airport Code"] }}</p>
            <p class="schedule">{{ date(' D, M j, Y', strtotime($ticket["Inbound Arrival Date"])).", ". date('h:i a', strtotime($ticket["Inbound Arrival Time"])) }}</p>
            <p class="destination">{{ $ticket["Inbound Arrival Airport Code"] }}</p>
        </div>
        <div class="col-sm-1 text-center">
            <a data-toggle="modal" data-target="#flightModal{{ $ticket["Sorting ID"] }}"><img src="images/ultrasingapore2018/i.png" width="20"></a>
        </div>
    </div>
    <div class="col-sm-3 ticketBuy border-left">
        <div class="col-sm-12 text-center">
            @if(strlen($ticket["Actual Price Tier 3"]) !== 0)
                {{-- <h6>{{ $ticket["Private Sale"] }}</h6> --}}
                <div class="prices">
                    <span class="strikethrougPrice">SGD{{$ticket["Actual Price Tier 3"]}}</span>
                    <span class="discountPrice">SGD{{$ticket["Tier 3 (front end)"]}}</span>
                </div>
                @if(strlen($ticket["Ticketserv URL"]) !== 0)
                    <a class="btn ultraBuyBtn center-block" id="buyButton" datetime="Jun 1 2018 00:00:00 GMT+0800" target="_blank" href="{{ $ticket["Ticketserv URL"] }}" onclick="gtag_report_conversion();">BUY NOW</a>
                @endif
                <p class="small" style="padding-top: 5px; line-height: 1.3; font-size: 80%;">Price excludes ticketing fee, Event Protect  insurance & credit card charges</p>
            @else
                <div class="clearfix" style="height:30px;">&nbsp;</div>
                <h6>N/A</h6>
            @endif
        </div>
    </div>
</div>

<!-- mobile ticket + FLIGHT card responsive -->
<div class="col-xs-12 col-sm-6 hidden-lg hidden-md ticketCardFlight">
    {{-- <small class="discountTag"> 40% OFF</small> --}}
    <div class="col-xs-12 card-body pd-0">
        {{-- <a class="infobtn pull-right" data-toggle="modal" data-target="#flightModal{{ $ticket["Sorting ID"] }}"><i class="material-icons">info_outline</i></a> --}}
        <div class="col-xs-12 cardTop">
            <p class="ticketFlightCombo text-center">{{ $ticket["Ticket Name"] }}</p>
            <div class="col-xs-6 pd-0 ticketTitle">
                {{-- <div class="clearfix">&nbsp;</div> --}}
                <p class="ticketTier">Tier 3 <span class="ticketCategory"> {{ $ticket["Ticket Type"] }} </span> <a data-toggle="modal" data-target="#flightModal{{ $ticket["Sorting ID"] }}"><img src="images/ultrasingapore2018/i.png" width="20"></a></p>
            </div>
            <div class="col-xs-6 ticketPrice">
                {{-- <div class="clearfix">&nbsp;</div> --}}
                @if(strlen($ticket["Tier 3 (front end)"]) !== 0)
                    <p class="pull-right">{{ $ticket["Tier 3 (front end)"] }}{{-- <br><small>10 tickets left at this tier</small> --}}</p>
                @else
                    <h6>N/A</h6>
                @endif
            </div>
        </div>
        <div class="cardBottom col-xs-12 text-center">
            <table border="0" style="margin:auto">
                <tr>
                    <th class="text-center">Date</th>
                    <th class="text-left">Departure</th>
                    <th class="text-left">Arrival</th>
                </tr>
                <tr>
                    <td>{{ date(' D, M j, Y', strtotime($ticket["Outbound Departure Date"])) }}</td>
                    <td><span class="place">{{ $ticket["Outbound Departure Airport Code"] }} </span>{{ date('h:i a', strtotime($ticket["Outbound Departure Time"])) }}</td>
                    <td><span class="place">{{ $ticket["Outbound Arrival Airport Code"] }} </span>{{ date('h:i a', strtotime($ticket["Outbound Arrival Time"])) }}</td>
                </tr>
                <tr>
                    <td>{{ date(' D, M j, Y', strtotime($ticket["Inbound Departure Date"])) }}</td>
                    <td><span class="place">{{ $ticket["Inbound Departure Airport Code"] }} </span>{{ date('h:i a', strtotime($ticket["Inbound Departure Time"])) }}</td>
                    <td><span class="place">{{ $ticket["Inbound Arrival Airport Code"] }} </span>{{ date('h:i a', strtotime($ticket["Inbound Arrival Time"])) }}</td>
                </tr>
            </table>
            <div class="col-xs-12 flightBuy">
                @if(strlen($ticket["Actual Price Tier 3"]) !== 0)
                    @if(strlen($ticket["Ticketserv URL"]) !== 0)
                        <a class="btn ultraBuyBtn center-block" id="buyButton" datetime="Jun 1 2018 00:00:00 GMT+0800" target="_blank" href="{{ $ticket["Ticketserv URL"] }}" onclick="gtag_report_conversion();">BUY NOW</a>
                    @endif
                    <p class="small" style="padding-top: 5px; line-height: 1.3; font-size: 80%;">Price excludes ticketing fee, Event Protect  insurance & credit card charges</p>
                @else
                    <div class="clearfix" style="height:30px;">&nbsp;</div>
                    <h6>N/A</h6>
                @endif
            </div>
        </div>
    </div>
</div>

    {{-- ticket flight info modal --}}
    <div id="flightModal{{ $ticket["Sorting ID"] }}" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-body text-center pd-0">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="padding: 16px;"><i class="fa fa-times" aria-hidden="true"></i></button>
                <div class="row ultraModalRow info-box">

                    @if($ticket["Ticket Type"] == "PGA")
                    <div class="details">
                        <h6 class="type">Premium General Admission</h6>
                        <h6 class="days">2 DAY TICKET COMBO</h6>
                    </div>
                    <div class="details">
                        <ul>
                            <li><span class="fa fa-tag"></span>Premium General Admission, free standing.</li>
                            <li><span class="fa fa-calendar"></span>Friday &amp; Saturday, 15 - 16 June 2018</li>
                            <li><span class="fa fa-map-marker"></span>Marina Bay, Singapore</li>
                        </ul>   
                    </div>
                    @endif
                    @if($ticket["Ticket Type"] == "GA")
                    <div class="details">
                        <h6 class="type">General Admission</h6>
                        <h6 class="days">2 DAY TICKET COMBO</h6>
                    </div>
                    <div class="details">
                        <ul>
                            <li><span class="fa fa-tag"></span>General Admission, free standing.</li>
                            <li><span class="fa fa-calendar"></span>Friday &amp; Saturday, 15 - 16 June 2018</li>
                            <li><span class="fa fa-map-marker"></span>Marina Bay, Singapore</li>
                        </ul>   
                    </div>
                    @endif
                        
                    @if($ticket["Ticket Type"] == "PGA")
                        <div class="services col-sm-12 col-xs-12 col-sm-push-1 col-xs-push-1">
                            <div class="col-xs-2 col-sm-2 item">
                                <img class="list-icon" src="images/ultrasingapore2018/flag.png">
                                <span>Express Lane</span>
                            </div> 

                            <div class="col-xs-2 col-sm-2 item">
                                <img class="list-icon" src="images/ultrasingapore2018/drink.png">
                                <span>Bar Access</span>
                            </div> 

                            <div class="col-xs-2 col-sm-2 item">
                                <img class="list-icon" src="images/ultrasingapore2018/beach.png">
                                <span>Private Lounge</span>
                            </div> 

                            <div class="col-xs-2 col-sm-2 item">
                                <img class="list-icon" src="images/ultrasingapore2018/rfid-chip.png">
                                <span>Express RFID Top-up</span>
                            </div> 

                            <div class="col-xs-2 col-sm-2 item">
                                <img class="list-icon" src="images/ultrasingapore2018/watch-resolution.png" style="margin-left: 25px;">
                                <span>RFID Wristband</span>
                            </div> 
                        </div>  
                    @endif

                    @if($ticket["Ticket Type"] == "PGA")
                        <div class="services col-sm-12 col-xs-12 col-sm-push-1 col-xs-push-1">
                            <div class="col-xs-2 col-sm-2 item">
                                <img class="list-icon" src="images/ultrasingapore2018/ep_mono.png">
                                <span>Insurance</span>
                            </div> 

                            <div class="col-xs-2 col-sm-2 item">
                                <img class="list-icon" src="images/ultrasingapore2018/restroom-sign.png">
                                <span>Private Restroom</span>
                            </div> 

                            <div class="col-xs-2 col-sm-2 item">
                                <img class="list-icon" src="images/ultrasingapore2018/18-plus.png">
                                <span>Above 18+</span>
                            </div> 

                            <div class="col-xs-2 col-sm-2 item">
                                <img class="list-icon" src="images/ultrasingapore2018/no-entry.png">
                                <span>No re-entry</span>
                            </div> 
                        </div>  
                    @endif

                    @if($ticket["Ticket Type"] == "GA")
                       <div class="services col-sm-12 col-xs-12 col-sm-push-1 col-xs-push-1">

                            <div class="col-xs-2 col-sm-2 item">
                                <img class="list-icon" src="images/ultrasingapore2018/drink.png">
                                <span>Bar Access</span>
                            </div> 

                            <div class="col-xs-2 col-sm-2 item">
                                <img class="list-icon" src="images/ultrasingapore2018/ep_mono.png">
                                <span>Insurance</span>
                            </div> 

                            <div class="col-xs-2 col-sm-2 item">
                                <img class="list-icon" src="images/ultrasingapore2018/watch-resolution.png" style="margin-left: 25px;">
                                <span>RFID Wristband</span>
                            </div> 

                            <div class="col-xs-2 col-sm-2 item">
                                <img class="list-icon" src="images/ultrasingapore2018/18-plus.png">
                                <span>Above 18+</span>
                            </div> 

                            <div class="col-xs-2 col-sm-2 item">
                                <img class="list-icon" src="images/ultrasingapore2018/no-entry.png">
                                <span>No re-entry</span>
                            </div> 

                        </div>  
                    @endif

                    <div class="copy_text">
                        *Ticketing fee, Event Protect insurance &amp; credit card <br/>
                        charges will be added at the checkout basket.<br/>
                        <a href="purchaseterms">Terms &amp; Conditions</a> apply.
                    </div>    

                </div> 
            </div>
          </div>
        </div>
    </div>