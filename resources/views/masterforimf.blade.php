<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
 

  <meta property="og:site_name" content="AirAsiaRedTix" />
  <meta property="og:title" content="AirAsiaRedTix - The Passport to Entertainment" />
  <meta property="og:description" content="AirAsiaRedTix.com is the hottest, smartest new way to book tickets to an international line-up of concerts, sporting events, musicals, theatre performances and more. A subsidiary of Asia’s largest low-cost carrier, AirAsiaRedTix.com is The Passport to Entertainment." />
  <meta property="og:image" content="https://airasiaredtix.com/images/assets/fb-aart.jpg" />
  <meta property="og:type" content="website" />
  <meta property="og:url" content="https://airasiaredtix.com" />

  <title>RedTix - @yield('title')</title>
  <link rel="shortcut icon" href="images/redtix_logo.png">

  <link rel="stylesheet" href="/css/app.min.css">
  <link rel="stylesheet" href="/css/appcustom.css">
  <link rel="stylesheet" href="/css/appcustom-rd.css">
  <link rel="stylesheet" href="/css/customevent.css">
  <script src="/js/jssor.slider-27.5.0.min.js" type="text/javascript"></script>
  
  <script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
     
  
 
  <link href="https://cdn.rawgit.com/noelboss/featherlight/1.7.7/release/featherlight.min.css" type="text/css" rel="stylesheet" />
  <link href="https://cdn.rawgit.com/noelboss/featherlight/1.7.7/release/featherlight.gallery.min.css" type="text/css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans" /> 
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300" rel="stylesheet">
  
  @if (App::environment('production'))
   
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-W7WS5LM');</script>


  
    <script type="text/javascript">
      window.heap=window.heap||[],heap.load=function(e,t){window.heap.appid=e,window.heap.config=t=t||{};var r=t.forceSSL||"https:"===document.location.protocol,a=document.createElement("script");a.type="text/javascript",a.async=!0,a.src=(r?"https:":"http:")+"//cdn.heapanalytics.com/js/heap-"+e+".js";var n=document.getElementsByTagName("script")[0];n.parentNode.insertBefore(a,n);for(var o=function(e){return function(){heap.push([e].concat(Array.prototype.slice.call(arguments,0)))}},p=["addEventProperties","addUserProperties","clearEventProperties","identify","removeEventProperty","setEventProperties","track","unsetEventProperty"],c=0;c<p.length;c++)heap[p[c]]=o(p[c])};
      heap.load("2662097889");
    </script>
   
 @endif

</head>
<body class="white-bg">

@if (App::environment('production'))

  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W7WS5LM" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

@endif

@yield('header')

@yield('content')

@yield('modal')

@include('layouts.partials._newsletterModal')
    
@include('layouts.partials._footer')

@yield('customjs')

</body>
</html>