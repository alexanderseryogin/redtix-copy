<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

  <meta property="og:site_name" content="AirAsiaRedTix" />
  <meta property="og:title" content="AirAsiaRedTix - The Passport to Entertainment" />
  <meta property="og:description" content="AirAsiaRedTix.com is the hottest, smartest new way to book tickets to an international line-up of concerts, sporting events, musicals, theatre performances and more. A subsidiary of Asia’s largest low-cost carrier, AirAsiaRedTix.com is The Passport to Entertainment." />
  <meta property="og:image" content="https://airasiaredtix.com/images/assets/fb-aart.jpg" />
  <meta property="og:type" content="website" />
  <meta property="og:url" content="https://airasiaredtix.com" />

  <title>RedTix - @yield('title')</title>
  <link rel="shortcut icon" href="images/redtix_logo.png">
  <!-- Bootstrap -->
  <link rel="stylesheet" href="/css/app.min.css">
  <link rel="stylesheet" href="/css/appcustom.css">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <link href="https://cdn.rawgit.com/noelboss/featherlight/1.7.7/release/featherlight.min.css" type="text/css" rel="stylesheet" />
  <link href="https://cdn.rawgit.com/noelboss/featherlight/1.7.7/release/featherlight.gallery.min.css" type="text/css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans" />
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300" rel="stylesheet">
  
  @if (App::environment('production'))
    <!-- Global Site Tag (gtag.js) - Google Analytics PotatoHead -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-73588237-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-73588237-1');
    </script>
    <!-- End Google Tag Manager PotatoHead -->
    
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-W7WS5LM');</script>
    <!-- End Google Tag Manager -->

    <!-- Facebook Pixel Code PotatoHead-->
        <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window, document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '190085905252897');
        fbq('track', 'PageView');
        </script>
        <noscript><img height="1" width="1" style="display:none"
        src="https://www.facebook.com/tr?id=190085905252897&ev=PageView&noscript=1"
        /></noscript>
    <!-- End Facebook Pixel Code -->

    <!-- Start Heap Analytics -->
    <script type="text/javascript">
      window.heap=window.heap||[],heap.load=function(e,t){window.heap.appid=e,window.heap.config=t=t||{};var r=t.forceSSL||"https:"===document.location.protocol,a=document.createElement("script");a.type="text/javascript",a.async=!0,a.src=(r?"https:":"http:")+"//cdn.heapanalytics.com/js/heap-"+e+".js";var n=document.getElementsByTagName("script")[0];n.parentNode.insertBefore(a,n);for(var o=function(e){return function(){heap.push([e].concat(Array.prototype.slice.call(arguments,0)))}},p=["addEventProperties","addUserProperties","clearEventProperties","identify","removeEventProperty","setEventProperties","track","unsetEventProperty"],c=0;c<p.length;c++)heap[p[c]]=o(p[c])};
      heap.load("2662097889");
    </script>
    <!-- End Heap Analytics -->
 @endif

</head>
<body>

@if (App::environment('production'))
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W7WS5LM" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <!-- Google Analytics Cross Domain Tracking PotatoHead -->
    <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-73588237-1', 'auto', {allowLinker: true});


    // Loads the Linker plugin
    ga('require', 'linker');
    // Set domains for source and destination
    ga('linker:autoLink', ['airasiaredtix.com', 'redtix.cognitix.id']);

    ga(function(tracker) {
      var linkerParam = tracker.get('linkerParam');
    });

    </script>
    <!-- End Google Analytics Cross Domain Tracking PotatoHead -->

@endif

@yield('header')

@yield('content')

@yield('modal')

@include('layouts.partials._newsletterModal')

@include('layouts.partials._footer')

@yield('customjs')

</body>
</html>