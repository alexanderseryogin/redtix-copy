<style>
    @import url('https://fonts.googleapis.com/css?family=Montserrat');

    body {
        padding: 10px 100px 0 100px;
        color: #6e6e6e;
        font-family: 'Montserrat', 'sans-serif';

    }

    h4, h5, h6 {
        font-weight: lighter;
        color: #777777;
        margin: 5px;
    }

    span {
        font-size: 18px;
    }

    .title-icon.down {
        width: 18px;
        height: 12px;
        background-image: url(/images/rugby2019/icons/airplane-down.png);
    }

    hr {
        margin-top: 30px;
    }

    .line {
        margin-top: 12px;
        font-weight: lighter;
        color: #777777;
    }

    .departure-day {
        font-size: 18px;
        font-weight: 600;
        margin-top: 15px;
        margin-bottom: 15px;
    }

    .after-departure-day {
        margin-top: 10px;
        margin-bottom: 10px;
        font-size: 12px;
    }

    .description-bottom {
        margin-top: 20px;
        font-size: 12px;
        line-height: 20px;
    }

</style>


<?php $i = 1 ?>
@foreach($packages as $package)
    <?php echo '<pre>'; print_r($package); echo '</pre>' ?>


    @foreach($package[''])

    <div style="font-family: 'Montserrat', 'sans-serif'; width: 600px">

        {{--        <div style="font-size: 24px; font-weight: 600;">Opening Ceremony | Package {{ $package }}</div>--}}
        <div style="font-size: 18px; margin: 40px 0 10px 0">Day {{ $i }} | Thursday 19th September 2019</div>

            <div class="departure-day" style="font-size: 18px; margin: 20px 0 10px 0">Departure Day</div>

            <div class="departure-day" style="font-size: 18px; margin: 20px 0 10px 0">Match Day {{ $i - 1 }}}</div>

        <h4 class="after-departure-day">
            The journey begins. Time to check-in for your international flight to
            Japan.
        </h4>

        <div style="margin-top: 20px">
            <h4 class="line" style="">{{ $airportOfSelectedCity[0]->name }} ({{ $airportOfSelectedCity[0]->abbr }})
                {{--                to {{ $package['city_transit']['airport'] }}--}}
                {{--                ({{ $package['city_transit']['abbr'] }})--}}
            </h4>
        </div>

        {{--        <div class="after-departure-day" style="font-size: 18px; margin: 20px 0 10px 0">Opening ceremony</div>--}}
        {{--        <div class="after-departure-day" style="font-size: 18px; margin: 20px 0 10px 0">Start time:</div>--}}
        {{--        <div class="after-departure-day"--}}
        {{--             style="font-size: 18px; margin: 20px 0 10px 0"> {{ $package->ticket[0]->participant1_name }}--}}
        {{--            VS {{ $package->ticket[0]->participant2_name }}</div>--}}
        {{--        <div class="after-departure-day" style="font-size: 18px; margin: 20px 0 10px 0">Kick--}}
        {{--            off: {{ substr($package->ticket[0]->date, 11) }}</div>--}}


        {{--        <div style="margin-top: 20px">--}}
        {{--            <div class="line">--}}
        {{--                <img src="images/rugby2019/icons/airplane-up.png">&nbsp;&nbsp;--}}
        {{--                <span class="title-small" style="font-size:12px; font-weight: bold;">--}}
        {{--                Departs:--}}
        {{--            </span>--}}
        {{--                --}}{{--                <span style="font-size: 12px;">Thursday 19th September 2019 at 11:05 AM</span>--}}
        {{--                <span style="font-size: 12px;">{{ date('H:i, l, F jS, Y', strtotime($package['date_origin_1'])) }}</span>--}}
        {{--            </div>--}}
        {{--            <div class="line" style="font-size: 12px;">Flight--}}
        {{--                Time: {{ gmdate("H:i", strtotime($package['date_origin_2']) - strtotime($package['date_origin_1'])) }} h--}}
        {{--            </div>--}}
        {{--            <div class="line">--}}
        {{--                <img src="images/rugby2019/icons/airplane-down.png"/>&nbsp;&nbsp;--}}
        {{--                <span class="title-small" style="font-size:12px; font-weight: bold;">--}}
        {{--                Arrives:--}}
        {{--            </span>--}}
        {{--                --}}{{--                <span style="font-size: 12px;">Thursday 19th September 2019 at 19:25 PM</span>--}}
        {{--                <span style="font-size: 12px;">{{ date('H:i, l, F jS, Y', strtotime($package['date_origin_2'])) }}</span>--}}
        {{--            </div>--}}
        {{--        </div>--}}

    </div>
@endforeach


