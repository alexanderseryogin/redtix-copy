@extends('master')
@section('title')
    Maintenance
@endsection

@section('header')
    @include('layouts.partials._header')
@endsection

@section('content')

    <!-- Content Section -->
    <section class="maintenancePage">
        <div class="container text-center">
            <img class="img-responsive center-block" src="images/repair.png" alt="">
            <h1><span class="text-danger">Maintenance</span> In Progress</h1>
            <p>We are currently updating our website to serve you better. See you in a few moments!.</p>
            <div class="clearfix">&nbsp;</div>
            <a class="btn btn-lg btn-black" href="mailto:support@airasiaredtix.com"><i class="fa fa-envelope-o" aria-hidden="true"></i> support@airasiaredtix.com</a>
        </div>
    </section><!-- /Content Section -->

@endsection