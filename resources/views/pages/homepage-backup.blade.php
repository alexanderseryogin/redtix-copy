@extends('master')

@section('title')
    The Passport to Entertainment
@endsection

@section('header')
    @include('layouts.partials._header')
@endsection

@section('content')
<link type="text/css" rel="stylesheet" href="css/maincustom.css">
<link type="text/css" rel="stylesheet" href="css/bannercustom.css">
    <!-- Main Page Banner Section -->
    <section id="pageSlider2">
      <div id="pageSlider-slide" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
          <li data-target="#pageSlider-slide" data-slide-to="0" class="active"></li>
          <li data-target="#pageSlider-slide" data-slide-to="1"></li>
          <li data-target="#pageSlider-slide" data-slide-to="2"></li>
          <li data-target="#pageSlider-slide" data-slide-to="3"></li>
          <li data-target="#pageSlider-slide" data-slide-to="4"></li>
          <li data-target="#pageSlider-slide" data-slide-to="5"></li>
          <li data-target="#pageSlider-slide" data-slide-to="6"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">

            <div class="item active">
                <a href="klims2018">
                    <picture>
                        <source media="(max-width: 479px)" srcset="images/herobanner/klims2018-thumbnail.jpg">
                        <img class="bannerPoster img-responsive" src="images/herobanner/klims2018-web-banner.jpg"/>
                    </picture>
                </a>
            </div>

            <div class="item">
                <a href="pokok">
                    <picture>
                        <source media="(max-width: 479px)" srcset="images/herobanner/pokok-thumbnail.jpg">
                        <img class="bannerPoster img-responsive" src="images/herobanner/pokok-web-banner.jpg"/>
                    </picture>
                </a>
            </div>

            <div class="item">
                <a href="jasonderulo">
                    <picture>
                        <source media="(max-width: 479px)" srcset="images/herobanner/jason-thumbnail.jpg">
                        <img class="bannerPoster img-responsive" src="images/herobanner/jason-web-banner.jpg"/>
                    </picture>
                </a>
            </div>

            <div class="item">
                <a href="hardrock-desaru-nye">
                    <picture>
                        <source media="(max-width: 479px)" srcset="images/herobanner/hardrocknye-thumbnail.jpg">
                        <img class="bannerPoster img-responsive" src="images/herobanner/hardrocknye-web-banner.jpg"/>
                    </picture>
                </a>
            </div>

            <div class="item">
                <a href="#trending">
                    <picture>
                        <source media="(max-width: 479px)" srcset="images/herobanner/nov-highlight-thumbnail.jpg">
                        <img class="bannerPoster img-responsive" src="images/herobanner/nov-highlight-web-banner.jpg"/>
                    </picture>
                </a>
            </div>

            <div class="item">
                <a href="#newyeareve">
                    <picture>
                        <source media="(max-width: 479px)" srcset="images/herobanner/nye-thumbnail.jpg">
                        <img class="bannerPoster img-responsive" src="images/herobanner/nye-web-banner.jpg"/>
                    </picture>
                </a>
            </div>

            <div class="item">
                <a href="#genting-events">
                    <picture>
                        <source media="(max-width: 479px)" srcset="images/herobanner/genting-2018-hl-mobile-02.jpg">
                        <img class="bannerPoster img-responsive" src="images/herobanner/genting-2018-hl-web.jpg"/>
                    </picture>
                </a>
            </div>
        </div>
        <!-- Controls -->
        <a class="left carousel-control" data-target="#pageSlider-slide" role="button" data-slide="prev">
          <span class="fa fa-angle-left" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" data-target="#pageSlider-slide" role="button" data-slide="next">
          <span class="fa fa-angle-right" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    </section><!-- /Main Page Banner Section -->

    <!-- Event Cards -->
    <section class="pageContent-Home">
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- Main Body -->
                <div class="mainBodyContent col-xs-12">

                    <section class="homeCategory-section" id="trending">
                        {{-- <h1 class="mainSecTitle" id="trendingCount">Trending Events <span>(8)</span></h1> --}}
                        <h1 class="mainSecTitle" id="trendingCount"></h1>
                        {{-- <span class="seeAllBtn"><a href="">See All <i class="fa fa-angle-right" aria-hidden="true"></i></a></span> --}}
                        <div class="row">
                            <div class="eventList-Home">

                                <!--Ticket List-->
                                {{-- <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Sept 29 2018 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="yoona">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/yoona2018/thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert">28 Sept '18</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">Music</div>
                                            <div class="titleEvent"><a href="yoona"><h2>Yoona Fan Meeting Tour</h2></a></div>
                                            <div class="location"> ZEEP@BIGBOX, Singapore</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From RM249</div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Sept 30 2018 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="theroarsingapore">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/roarsingapore2018/thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert">29 Sept '18</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">Sports</div>
                                            <div class="titleEvent"><a href="theroarsingapore"><h2>The Roar of Singapore 5 - The Kings of Lion City</h2></a></div>
                                            <div class="location"> Marina Bay Sands Expo & Convention, Singapore</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">FREE</div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Oct 13 2018 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="islandmusicfestival">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/islandmusicfest2018v2/thumbnail1.jpg')"></div>
                                                    <div class="dateEvent concert"> 12 Oct'18</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>  
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">Festival</div>
                                            <div class="titleEvent"><a href="islandmusicfestival"><h2>Island Music Festival 2018</h2></a></div>
                                            <div class="location"> Long Beach, Redang Island</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From RM650.00</div>
                                        </div>
                                    </div>
                                </div>


                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Oct 14 2018 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="malaysiawinefiesta">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/mywinefiesta2018/thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert"> 13 Oct'18</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>  
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">Festival</div>
                                            <div class="titleEvent"><a href="malaysiawinefiesta"><h2>Malaysia Wine Fiesta 2018</h2></a></div>
                                            <div class="location"> Botanica Deli, Bangsar South, KL</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From RM80.00</div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Oct 22 2018 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="motogpjapan">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/motogpjapan/thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert"> 19 - 21 Oct '18</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>  
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">Motorsport</div>
                                            <div class="titleEvent"><a href="motogpjapan"><h2>Motul Grand Prix of Japan</h2></a></div>
                                            <div class="location"> Twin Ring Motegi, Japan</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From RM513</div>
                                        </div>
                                    </div>
                                </div>

                               <!--Ticket List-->
                               <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Oct 28 2018 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="sensationrise">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/sensationrise2018/thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert"> 27 Oct '18</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>  
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">Festival</div>
                                            <div class="titleEvent"><a href="sensationrise"><h2>Sensation Rise</h2></a></div>
                                            <div class="location"> G.M.C Stadium, Hyderabad</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From RM186</div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Nov 3 2018 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="thedarkparty">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/thedarkparty2018/thumbnail1.jpg')"></div>
                                                    <div class="dateEvent concert"> 2 Nov '18</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>  
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">Festival</div>
                                            <div class="titleEvent"><a href="thedarkparty"><h2>The Dark Party</h2></a></div>
                                            <div class="location"> KL Live at Life Centre</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From RM118</div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Nov 5 2018 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="motogp2018">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/motogp2018/thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert"> 2 - 4 Nov '18</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>  
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">Motorsport</div>
                                            <div class="titleEvent"><a href="motogp2018"><h2>Shell Malaysia Motorcycle Grand Prix 2018</h2></a></div>
                                            <div class="location"> Sepang International Circuit</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From RM52.84</div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Nov 8 2018 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="seanpaulliveinkl">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/seanpaul2018/thumbnail1.jpg')"></div>
                                                    <div class="dateEvent concert"> 7 Nov '18</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>  
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">Concert</div>
                                            <div class="titleEvent"><a href="seanpaulliveinkl"><h2>Sean Paul Live in KL - Mad Love Tour</h2></a></div>
                                            <div class="location"> KL Live at Life Centre</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From RM228</div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Nov 12 2018 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="thefiveelvesmusical">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/astro/fiveelves2018/thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert">8 - 11 Nov '18</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">Musical</div>
                                            <div class="titleEvent"><a href="thefiveelvesmusical"><h2>The Five Elves Musical: A Magical Journey</h2></a></div>
                                            <div class="location"> Plenary Hall, KLCC</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From RM54.60</div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Nov 12 2018 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="neonlights2018">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/neonlights2018/thumbnail2.jpg')"></div>
                                                    <div class="dateEvent concert">9-11 Nov '18</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">Festival</div>
                                            <div class="titleEvent"><a href="neonlights2018"><h2>Neon Lights Festival 2018</h2></a></div>
                                            <div class="location"> Fort Canning Park, Fort Gate, Singapore</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From RM67.50</div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Nov 12 2018 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="melaniecasiatour">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/melaniec2018/thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert">11 Nov '18</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">Concert</div>
                                            <div class="titleEvent"><a href="melaniecasiatour"><h2>Melanie C Asia Tour 2018 : Live in Bangkok</h2></a></div>
                                            <div class="location"> GMM Live House at Central World, Bangkok</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From RM205</div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Nov 15 2018 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="gunsnroses">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/gnr2018/thumbnail1.jpg')"></div>
                                                    <div class="dateEvent concert">14 Nov '18</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">Concert</div>
                                            <div class="titleEvent"><a href="gunsnroses"><h2>Guns N' Roses Not In This Lifetime Tour</h2></a></div>
                                            <div class="location"> Surf Beach, Sunway Lagoon</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From RM245</div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Nov 13 2018 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="gilles-peterson">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/potatohead/singles/gilespeterson/thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert">12 Nov '18</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">Music</div>
                                            <div class="titleEvent"><a href="gilles-peterson"><h2>Gilles Peterson</h2></a></div>
                                            <div class="location">Potato Head Beach Club, Bali</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From IDR250,000</div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Nov 19 2018 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="thekualalumpurmajor">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/dotamy2018/thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert">16 - 18 Nov '18</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">ESports</div>
                                            <div class="titleEvent"><a href="thekualalumpurmajor"><h2>The Kuala Lumpur Major</h2></a></div>
                                            <div class="location">Axiata Arena, Kuala Lumpur, Malaysia</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From RM716</div>
                                        </div>
                                    </div>
                                </div> --}}

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Dec 3 2018 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="klims2018">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/klims2018/thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert"> 23 Nov - 2 Dec '18</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>  
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">Exhibition</div>
                                            <div class="titleEvent"><a href="klims2018"><h2>Kuala Lumpur International Motorshow 2018</h2></a></div>
                                            <div class="location"> MITEC, Kuala Lumpur</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From RM5</div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Nov 25 2018 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="vice-honeydijon">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/potatohead/singles/honeydijon/thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert">24 Nov '18</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">Music</div>
                                            <div class="titleEvent"><a href="vice-honeydijon"><h2>VICE Indonesia 2nd Birthday Party Bali: HONEY DIJON</h2></a></div>
                                            <div class="location">Potato Head Beach Club, Bali</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From IDR250,000</div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                {{-- <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Nov 24 2018 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="ufcfightnightbeijing">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/ufcbeijing2018/thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert">24 Nov '18</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">Sports</div>
                                            <div class="titleEvent"><a href="ufcfightnightbeijing"><h2>UFC Fight Night Beijing</h2></a></div>
                                            <div class="location"> Cadillac Arena, Beijing, China</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From RM174</div>
                                        </div>
                                    </div>
                                </div> --}}

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Dec 3 2018 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="cre">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/crekl2018/thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert">30 Nov - 2 Dec '18</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">Performance</div>
                                            <div class="titleEvent"><a href="cre"><h2>CRÈ</h2></a></div>
                                            <div class="location"> KLPAC, Kuala Lumpur</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From RM70</div>
                                        </div>
                                    </div>
                                </div>


                                <!--Ticket List-->
                                {{-- <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Dec 3 2018 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="ufcfightnightadelaide">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/ufcadelaide2018v2/thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert">2 Dec '18</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">Sports</div>
                                            <div class="titleEvent"><a href="ufcfightnightadelaide"><h2>UFC Adelaide</h2></a></div>
                                            <div class="location"> Adelaide Entertainment Centre, Australia</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From AUD75 / RM238</div>
                                        </div>
                                    </div>
                                </div> --}}

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Dec 5 2018 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="judaspriest">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/lamc/singles/judaspriest/thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert">4 Dec '18</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">Concert</div>
                                            <div class="titleEvent"><a href="judaspriest"><h2>Judas Priest with Special Guests BABYMETAL - Live in Singapore</h2></a></div>
                                            <div class="location"> ZEEP@BIGBOX, Singapore</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From RM446</div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Dec 7 2018 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="jasonderulo">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/jasonderulo2018/thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert">6 Dec '18</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">Concert</div>
                                            <div class="titleEvent"><a href="jasonderulo"><h2>Jason Derulo Live in KL 2018</h2></a></div>
                                            <div class="location"> KL Live at Life Centre</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From RM228</div>
                                        </div>
                                    </div>
                                </div>


                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Dec 17 2018 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="desarucoastgourmentseries">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/desarugourmet1/thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert">7 - 9 Dec '18</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">Signature Dining</div>
                                            <div class="titleEvent"><a href="desarucoastgourmentseries"><h2>Desaru Coast Gourmet Series - A Malaysian Journey</h2></a></div>
                                            <div class="location"> The Els Club Desaru Coast Ocean Course</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From RM180</div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Dec 17 2018 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="pokok">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/pokok2018/thumbnail1.jpg')"></div>
                                                    <div class="dateEvent concert">7 - 16 Dec '18</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">Performance</div>
                                            <div class="titleEvent"><a href="pokok"><h2>Teater Perdana DBP - Pokok</h2></a></div>
                                            <div class="location"> Auditorium Dewan Bahasa dan Pustaka</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From RM20</div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Dec 16 2018 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="femmefatale2018">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/femmefatale2018/thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert">15 Dec '18</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">Music</div>
                                            <div class="titleEvent"><a href="femmefatale2018"><h2>Femme Fatale 2018</h2></a></div>
                                            <div class="location"> KL Live at Life Centre</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From RM110</div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Jan 9 2019 00:00:00 GMT+0800">
                                    <div class="eventCard" id="newyeareve">
                                        <div class="top">
                                            <a href="epizode">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/epizode2018/thumbnail1.jpg')"></div>
                                                    <div class="dateEvent concert">28 Dec'18 - 8 Jan'19</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">Festival</div>
                                            <div class="titleEvent"><a href="epizode"><h2>Epizode³</h2></a></div>
                                            <div class="location"> Sunset Sanato Beach, Phu Quoc, Vietnam</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From RM930</div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Jan 1 2019 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="sunburnfestival">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/sunburn2018/thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert">29 - 31 Dec'18</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">Festival</div>
                                            <div class="titleEvent"><a href="sunburnfestival"><h2>Sunburn Festival Pune 2018</h2></a></div>
                                            <div class="location"> Oxford Golf Resort, Pune, India</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From RM243</div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Jan 1 2019 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="originfields">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/originfields2018/thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert">30 - 31 Dec '18</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">Festival</div>
                                            <div class="titleEvent"><a href="originfields"><h2>Origin Fields</h2></a></div>
                                            <div class="location"> Langley Park, Perth, Australia</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From RM930</div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Jan 1 2019 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="hardrock-desaru-nye">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/hardrockdesaru2018/thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert">31 Dec '18</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">Music</div>
                                            <div class="titleEvent"><a href="hardrock-desaru-nye"><h2>Hard Rock Hotel Desaru Coast NYE's Dinner Party</h2></a></div>
                                            <div class="location"> Desaru Coast Conference Centre</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From RM588</div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Jan 1 2019 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="neoncountdown">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/neon2018/thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert">31 Dec '18</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">Music</div>
                                            <div class="titleEvent"><a href="neoncountdown"><h2>Neon Countdown 2018</h2></a></div>
                                            <div class="location"> Sepang International Circuit (Paddock)</div>
                                        </div>
                                        <div class="bottom">
                                            {{-- <div class="discount">Register Now</div> --}}
                                            <div class="price">From RM148</div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Jan 1 2019 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="potatohead-nye">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/potatohead/nye2018/thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert">31 Dec '18</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">Music</div>
                                            <div class="titleEvent"><a href="potatohead-nye"><h2>New Year's Eve at Potato Head Beach Club with Disclosure and Speacial Guest</h2></a></div>
                                            <div class="location">Potato Head Beach Club, Bali</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From IDR1,000,000</div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Jan 2 2019 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="finnsnewyearfestival">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/finns/finnsny2018/thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert">31 Dec '18 - 1 Jan '19</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">Festival</div>
                                            <div class="titleEvent"><a href="finnsnewyearfestival"><h2>Finns New Year's Music Festival</h2></a></div>
                                            <div class="location"> Finns Beach Club, Bali</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">IDR1,400,000</div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Jan 21 2019 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="thefairydoll">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/thefairydoll2018/thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert">18 - 19 Jan '19</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">Performance</div>
                                            <div class="titleEvent"><a href="thefairydoll"><h2>The Fairy Doll</h2></a></div>
                                            <div class="location"> Istana Budaya, Kuala Lumpur</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From RM88</div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="March 18 2019 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="formosa">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/formosa2018/thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert">16 - 17 Mac '19</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">Performance</div>
                                            <div class="titleEvent"><a href="formosa"><h2>FORMOSA in Kuala Lumpur 2019 by Cloud Gate Dance Theatre</h2></a></div>
                                            <div class="location"> Panggung Sari, Istana Budaya Kuala Lumpur</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From RM98</div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Apr 7 2019 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="soulsisters">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/soulsisters2019/thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert">5 - 6 Apr '19</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">Concert</div>
                                            <div class="titleEvent"><a href="soulsisters"><h2>Soul Sisters</h2></a></div>
                                            <div class="location"> Istana Budaya, Kuala Lumpur</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From RM155</div>
                                        </div>
                                    </div>
                                </div>

                                <!-- end -->

                            </div>
                        </div>
                    </section>

                    {{-- <section class="homeCategory-section" id="georgetownfestival">
                        <h1 class="mainSecTitle" id="georgetownfestivalCount"></h1>
                        <!-- <span class="seeAllBtn"><a href="">See All <i class="fa fa-angle-right" aria-hidden="true"></i></a></span> -->
                        <div class="row">
                            <div class="eventList-Home">

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Aug 6 2018 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="kelantan">
                                                <div class="thumb" style="background-image: url('images/gtf2018/kelantan/thumbnail.jpg')"></div>
                                                <div class="dateEvent concert">4 - 5 Aug '18</div>
                                                <div class="viewEvent"><span>View Event</span></div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">Music</div>
                                            <div class="titleEvent"><a href="kelantan"><h2>Kelantan</h2></a></div>
                                            <div class="location">Dewan Sri Pinang Auditorium, George Town, Penang</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From RM25</div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </section> --}}

                    <section class="homeCategory-section last" id="genting-events">
                        {{-- <h1 class="mainSecTitle">Genting Events <span>(5)</span></h1> --}}
                        <h1 class="mainSecTitle" id="gentingCount"></h1>
                        {{-- <span class="seeAllBtn"><a href="">See All <i class="fa fa-angle-right" aria-hidden="true"></i></a></span> --}}
                        <div class="row">
                            <div class="eventList-Home">

                                <!--Ticket List-->
                                {{-- <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Sept 30 2018 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="shilaamzah2018">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/genting/shilaamzah2018/thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert"> Sat, 29 Sept '18</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">GENM</div>
                                            <div class="titleEvent"><a href="shilaamzah2018"><h2>Shila Amzah “Loving You” World Tour"</h2></a></div>
                                            <div class="location"> Arena Of Stars, Genting Highland</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From RM51</div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Oct 7 2018 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="hkband2018">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/genting/hkband2018/thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert"> Sat, 6 Oct '18</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">GENM</div>
                                            <div class="titleEvent"><a href="hkband2018"><h2>HK Band Live in Genting</h2></a></div>
                                            <div class="location"> Arena Of Stars, Genting Highland</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From RM202</div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Oct 8 2018 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="ildivo">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/genting/ildivo2018/thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert"> Sun, 7 Oct '18</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">GENM</div>
                                            <div class="titleEvent"><a href="ildivo"><h2>IL Divo Timeless Tour Live in Genting</h2></a></div>
                                            <div class="location"> Arena Of Stars, Genting Highland</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From RM150</div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Oct 14 2018 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="dickycheung">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/genting/dickycheung2018/thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert"> Sat, 13 Oct '18</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">GENM</div>
                                            <div class="titleEvent"><a href="dickycheung"><h2>Dicky Cheung Live in Genting</h2></a></div>
                                            <div class="location"> Arena Of Stars, Genting Highland</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From RM146</div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Oct 21 2018 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="richiejen">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/genting/richiejen2018/thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert"> Fri & Sat, 19 - 20 Oct '18</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">GENM</div>
                                            <div class="titleEvent"><a href="richiejen"><h2>Richie Jen Live In Genting</h2></a></div>
                                            <div class="location"> Arena Of Stars, Genting Highland</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From RM121</div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Oct 28 2018 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="lizawang2018">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/genting/lizawang2018/thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert"> Sat, 27 Oct '18</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">GENM</div>
                                            <div class="titleEvent"><a href="lizawang2018"><h2>Liza Wang Live in Genting</h2></a></div>
                                            <div class="location"> Arena Of Stars, Genting Highland</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From RM122</div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Nov 11 2018 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="spectacularvoices">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/genting/spectacularvoices2018/thumbnail1.jpg')"></div>
                                                    <div class="dateEvent concert"> Sat, 10 Nov '18</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">GENM</div>
                                            <div class="titleEvent"><a href="spectacularvoices"><h2>Spectacular Voices Concert in Genting</h2></a></div>
                                            <div class="location"> Arena Of Stars, Genting Highland</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From RM173.20</div>
                                        </div>
                                    </div>
                                </div> --}}

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Nov 18 2018 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="yuya">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/genting/yuya2018/thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert"> Sat, 17 Nov '18</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">GENM</div>
                                            <div class="titleEvent"><a href="yuya"><h2>Yu Ya Unforgettable Memories Live in Genting</h2></a></div>
                                            <div class="location"> Arena Of Stars, Genting Highland</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From RM84</div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Nov 25 2018 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="kingofcomedy2018">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/genting/kingofcomedy2018/thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert"> Sun, 24 Nov '18</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">GENM</div>
                                            <div class="titleEvent"><a href="kingofcomedy2018"><h2>Kings of Comedy in Concert 2018</h2></a></div>
                                            <div class="location"> Arena Of Stars, Genting Highland</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From RM119.20</div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Dec 2 2018 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="susannaruco">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/genting/susanna2018/thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert"> Sat, 1 Dec '18</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">GENM</div>
                                            <div class="titleEvent"><a href="susannaruco"><h2>Susanna Kwan & Ruco Chan Live in Genting</h2></a></div>
                                            <div class="location"> Arena Of Stars, Genting Highland</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From RM110</div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Dec 16 2018 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="zhangdichenjinpei">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/genting/zhangtijinpei2018/thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert"> Sat, 15 Dec '18</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">GENM</div>
                                            <div class="titleEvent"><a href="zhangdichenjinpei"><h2>Zhang Di and Chen Jin Pei Comedy cum Hits Concert</h2></a></div>
                                            <div class="location"> Arena Of Stars, Genting Highland</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From RM119.20</div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Dec 23 2018 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="ourvoices">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/genting/ourvoices2018/thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert"> Sat, 22 Dec '18</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">GENM</div>
                                            <div class="titleEvent"><a href="ourvoices"><h2>Our Voices in Genting</h2></a></div>
                                            <div class="location"> Arena Of Stars, Genting Highland</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From RM173.20</div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Dec 26 2018 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="kennybee">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/genting/kennybee2018/thumbnail1.jpg')"></div>
                                                    <div class="dateEvent concert"> Sat, 25 Dec '18</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">GENM</div>
                                            <div class="titleEvent"><a href="kennybee"><h2>Kenny Bee the Story Continues Live 2018</h2></a></div>
                                            <div class="location"> Arena Of Stars, Genting Highland</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From RM160</div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Dec 30 2018 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="grasshopper">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/genting/grasshopper2018/thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert"> Sat, 29 Dec '18</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">GENM</div>
                                            <div class="titleEvent"><a href="ourvoices"><h2>Grasshopper 17385</h2></a></div>
                                            <div class="location"> Arena Of Stars, Genting Highland</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From RM156</div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Jan 6 2019 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="cxhlqy">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/genting/caixiaohu2019/thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert"> Sat, 5 Jan '19</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">GENM</div>
                                            <div class="titleEvent"><a href="cxhlqy"><h2>Cai Xiao Hu & Long Qian Yu Love Hokkien Songs Concert</h2></a></div>
                                            <div class="location"> Arena Of Stars, Genting Highland</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From RM172</div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Jan 20 2019 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="hackenlee">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/genting/hackenlee2019/thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert"> Sat, 19 Jan '19</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">GENM</div>
                                            <div class="titleEvent"><a href="hackenlee"><h2>Hacken Lee 30th Anniversary Concert in Malaysia</h2></a></div>
                                            <div class="location"> Arena Of Stars, Genting Highland</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From RM256</div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Feb 17 2019 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="naying">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/genting/naying2019/thumbnail1.jpg')"></div>
                                                    <div class="dateEvent concert"> Sat, 16 Feb '19</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">GENM</div>
                                            <div class="titleEvent"><a href="naying"><h2>Na Ying World Tour Concert</h2></a></div>
                                            <div class="location"> Arena Of Stars, Genting Highland</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From RM208</div>
                                        </div>
                                    </div>
                                </div>

                                <!-- end -->

                            </div>
                        </div>
                    </section>

                  {{--   <section class="homeCategory-section last" id="upcoming">
                        <h1 class="mainSecTitle">Events You Might Be Interested <span>(1)</span></h1>
                        <div class="row">
                            <div class="eventList-Home">
                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="https://goo.gl/forms/pIC136YOLW6grTo43" target="_blank">
                                                <div class="thumb" style="background-image: url('images/milff2018/thumbnail.jpg')"></div>
                                                <div class="dateEvent sports">TBA 2018</div>
                                                <div class="viewEvent"><span>Register Now</span></div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">Concert</div>
                                            <div class="titleEvent"><a href="https://goo.gl/forms/pIC136YOLW6grTo43" target="_blank"><h2> MILFF Concert Series 2018</h2></a></div>
                                            <div class="location">Malaysia</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">Price TBA</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section> --}}

                    {{-- <section class="homeCategory-section last">
                        <h1 class="mainSecTitle">Discounted / Promotions <span>(7)</span></h1>
                        <span class="seeAllBtn"><a href="">See All <i class="fa fa-angle-right" aria-hidden="true"></i></a></span>
                        
                        <!-- Swiper -->
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide" data-swiper-autoplay="2000">
                                    <div class="discountedCard">
                                    <a href="">
                                        <div class="top">
                                        <div class="thumb" style="background-image: url('images/discount.jpg')"></div>
                                        <div class="titleEvent">
                                            <h2>CIMB 20% Off</h2>
                                        </div>
                                        </div>
                                    </a>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="discountedCard">
                                    <a href="">
                                        <div class="top">
                                        <div class="thumb" style="background-image: url('images/bigmainbanner.jpg')"></div>
                                        <div class="titleEvent">
                                            <h2>Maybank Cards 90% Off only on Tuesday</h2>
                                        </div>
                                        </div>
                                    </a>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="discountedCard">
                                    <a href="">
                                        <div class="top">
                                        <div class="thumb" style="background-image: url('images/bigmainbanner.jpg')"></div>
                                        <div class="titleEvent">
                                            <h2>Discount 10% off for student</h2>
                                        </div>
                                        </div>
                                    </a>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="discountedCard">
                                    <a href="">
                                        <div class="top">
                                        <div class="thumb" style="background-image: url('images/bigmainbanner.jpg')"></div>
                                        <div class="titleEvent">
                                            <h2>Free for AirAsia Staff</h2>
                                        </div>
                                        </div>
                                    </a>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="discountedCard">
                                    <a href="">
                                        <div class="top">
                                        <div class="thumb" style="background-image: url('images/bigmainbanner.jpg')"></div>
                                        <div class="titleEvent">
                                            <h2>Maybank Cards 90% Off</h2>
                                        </div>
                                        </div>
                                    </a>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="discountedCard">
                                    <a href="">
                                        <div class="top">
                                        <div class="thumb" style="background-image: url('images/bigmainbanner.jpg')"></div>
                                        <div class="titleEvent">
                                            <h2>Maybank Cards 90% Off</h2>
                                        </div>
                                        </div>
                                    </a>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="discountedCard">
                                    <a href="">
                                        <div class="top">
                                        <div class="thumb" style="background-image: url('images/bigmainbanner.jpg')"></div>
                                        <div class="titleEvent">
                                            <h2>Maybank Cards 90% Off</h2>
                                        </div>
                                        </div>
                                    </a>
                                    </div>
                                </div>
                            </div>
                            <!-- Add Scrollbar -->
                            <div class="swiper-scrollbar"></div>
                        </div><!-- /Swiper -->
                    </section> --}}
                </div>
                <!-- /Main Body -->
            </div>
            <!-- /row -->
        </div>
    </section>
    <!-- /Event Cards -->
    
@endsection

@section('customjs')
    <script type="text/javascript">
    // Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        scrollbar: '.swiper-scrollbar',
        scrollbarHide: false,
        slidesPerView: 4,
        spaceBetween: 15,
        grabCursor: true,
        breakpoints: {
            640: {
                slidesPerView: 2,
                spaceBetween: 20
            },
            320: {
                slidesPerView: 1,
                spaceBetween: 10
            }
        }
    });

    // Main Slider Interval Time
    $('.carousel').carousel({
        interval: 1000 * 2.5
    });

     $(document).ready(function(){
          // Session based Pop up modal
        //  if(typeof(Storage) !== "undefined") {
        //     if(!sessionStorage.getItem('modal')) {                          // if the session key is not exist
        //         sessionStorage.setItem('modal', 'true');                    // create new session
        //         $("#announcementModal").modal('show')                       // show modal dialog
        //     }
        // } 

        // Add smooth scrolling to all links
        $("a").on('click', function(event) {

            // Make sure this.hash has a value before overriding default behavior
            if (this.hash !== "") {
            // Prevent default anchor click behavior
            event.preventDefault();

            // Store hash
            var hash = this.hash;

            // Using jQuery's animate() method to add smooth page scroll
            // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
            $('html, body').animate({
                scrollTop: $(hash).offset().top
            }, 1000, function(){
        
                // Add hash (#) to URL when done scrolling (default click behavior)
                window.location.hash = hash;
            });
            } // End if
        });

        if ($('#priceFixed').length > 0){

            $('#priceFixed').scrollToFixed({
                marginTop: $('.mainHeader').outerHeight(true),
                limit: $('.tixPrice').offset().top - $('.mainHeader').outerHeight()
            });
        };

    });
    </script>

    {{-- Hide expired event --}}
    <script type="text/javascript">
        $(function() {
            $('div[id^=hideExpired]').each(function() {
                var date = new Date();
                var enddate = $(this).attr('datetime'); 
                if ( Date.parse(date) >= Date.parse(enddate)) {
                  $(this).remove();
                } else {
                    $(this).addClass('eventLaunced');
                }
            });
        });
    </script>

    {{-- Show Launched Event --}}
    <script type="text/javascript">
    $(function() {
        $('div[id^=showToLaunch]').each(function() {
            var date = new Date();
            var launchdate = $(this).attr('datetime'); 
            if ( Date.parse(date) < Date.parse(launchdate)) {
              $(this).remove();
            } 
            else {
                $(this).addClass('eventLaunced');
            }
        });
    });
    </script>

    {{-- event counter --}}
    <script type="text/javascript">
        $(function() {
            var countTrending = $('#trending .row .eventList-Home .eventLaunced').length;
            var countGeorgetownFestival = $('#georgetownfestival .row .eventList-Home .eventLaunced').length;
            var countGenting = $('#genting-events .row .eventList-Home .eventLaunced').length;
            $("#trendingCount").append("Trending Events <span>" + "("+ countTrending +")" + "</span>");
            $("#georgetownfestivalCount").append("Georgetown Festival Events <span>" + "("+ countGeorgetownFestival +")" + "</span>");            
            $("#gentingCount").append("Genting Events <span>" + "("+ countGenting +")" + "</span>");
         });
    </script>

@endsection


@section('modal')

<!--Modal Announcement-->
{{-- <div id="announcementModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content" style="width: 100%">
        <div class="modal-body text-center">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
            <div class="clearfix">&nbsp;</div>
            <div class="modcustom">
                <p><center><img class="img-responsive" src="images/main/soundvalley/sv-popup.png" align="middle"></center></p>
                <a class="btn btn-danger enabled" id="buyButton" target="_blank" href="https://goo.gl/7EiKxy" role="button">GET TICKETS</a>
            </div>
        </div>
      </div>
    </div>
  </div> --}}
@endsection

