@extends('master')
@section('title')
    Events
@endsection

@section('header')
    @include('layouts.partials._innerheader')
@endsection

@section('content')

    <!-- Search Section -->
    <section class="search-nav">
      <div class="filter-navHolder">
        <div class="container">
          <div class="bannerText text-center">
            <h1>Find Events</h1>
          </div>
          <form action="">
            <div class="input-group input-group-hg input-group-rounded">
              <span class="input-group-btn">
                <button type="submit" class="btn"><span class="fui-search"></span></button>
              </span>
              <input type="text" class="form-control" placeholder="" id="">
            </div>
          </form>
        </div>
      </div>
    </section><!-- /Search Section -->

    <!-- Content Section -->
    <section class="pageContent">
      <div class="container">
        
        <div class="row">
          <!-- Filter Sidebar -->
          <aside class="col-sm-3 filterSidebar">
            <div class="filter-navHolder boxPanel">
              <form action="">
                <div class="boxPanelHead">
                  <h6><i class="fa fa-sliders" aria-hidden="true"></i> Filter your search</h6>
                </div>
                <div>
                  <p><strong>Categories</strong></p>
                  <label class="checkbox" for="checkbox1">
                    <input type="checkbox" value="" id="checkbox1" data-toggle="checkbox">Concerts
                  </label>
                  <label class="checkbox" for="checkbox2">
                    <input type="checkbox" value="" id="checkbox2" data-toggle="checkbox">Theatre
                  </label>
                  <label class="checkbox" for="checkbox3">
                    <input type="checkbox" value="" id="checkbox3" data-toggle="checkbox">Sports
                  </label>
                  <label class="checkbox" for="checkbox4">
                    <input type="checkbox" value="" id="checkbox4" data-toggle="checkbox">Family
                  </label>
                  <label class="checkbox" for="checkbox5">
                    <input type="checkbox" value="" id="checkbox5" data-toggle="checkbox">Festivals
                  </label>
                  <label class="checkbox" for="checkbox6">
                    <input type="checkbox" value="" id="checkbox6" data-toggle="checkbox">Attractions
                  </label>
                  <label class="checkbox" for="checkbox7">
                    <input type="checkbox" value="" id="checkbox7" data-toggle="checkbox">Promotional
                  </label>
                  <label class="checkbox" for="checkbox8">
                    <input type="checkbox" value="" id="checkbox8" data-toggle="checkbox">Others
                  </label>
                </div>
                <hr>
                <div>
                  <p><strong>Region</strong></p>
                  <label class="checkbox" for="kl">
                    <input type="checkbox" value="" id="kl" data-toggle="checkbox">Kuala Lumpur
                  </label>
                  <label class="checkbox" for="sel">
                    <input type="checkbox" value="" id="sel" data-toggle="checkbox">Selangor
                  </label>
                  <label class="checkbox" for="jb">
                    <input type="checkbox" value="" id="jb" data-toggle="checkbox">Johor Baharu
                  </label>
                  <label class="checkbox" for="prk">
                    <input type="checkbox" value="" id="prk" data-toggle="checkbox">Perak
                  </label>
                  <label class="checkbox" for="mlk">
                    <input type="checkbox" value="" id="mlk" data-toggle="checkbox">Melaka
                  </label>
                </div>
                <hr>
                <div class="">
                    <div class="form-group has-feedback">
                        <input placeholder="Select Date" class="form-control date form_date col-xs-12" data-date="" data-date-format="dd MM yyyy" data-link-field="date-input1" data-link-format="yyyy-mm-dd" />
                        <span class="form-control-feedback fa fa-calendar"></span>
                    </div>
                </div>
              </form>
            </div>
          </aside>
          <!-- /Filter Sidebar -->

          <!-- Main Body -->
          <div class="mainBodyContent col-sm-9 pad-L-reset15">

            <section class="eventCategory-section last">
              <h1 class="mainSecTitle">Search results for coldplay</h1>
              <p>Found 31 results</p>
              <div class="eventList boxPanel">

                <!--Ticket List-->
                <ul class="list-unstyled">
                  <li>
                    <div class="media">
                        <div class="media-left media-middle">
                            <a href="#">
                                <div class="evThumb" style="background-image: url('images/event1.jpg')"></div>
                            </a>
                        </div>
                        <div class="media-body">
                          <div class="eventText">
                            <div class="cat">Concert, Music, Festival</div>
                            <h4 class="m-heading"><a href="">Ucop Buat Hal Lagi! Live in MATIC</a></h4>
                            <p><strong>Date :</strong> 11 November 2016</p>
                            <p><strong>Location :</strong> Concert, Music, Festival</p>
                          </div>
                          <div class="priceNbtn">
                            <p>From<span>RM180 - RM450</span></p>
                            <a class="getTix-btn" href="">Get Ticket</a>
                          </div>                            
                        </div>
                    </div>
                  </li>
                  <li>
                    <div class="media">
                        <div class="media-left media-middle">
                            <a href="#">
                                <div class="evThumb" style="background-image: url('images/event2.jpg')"></div>
                            </a>
                        </div>
                        <div class="media-body">
                          <div class="eventText">
                            <div class="cat">Concert, Music, Festival</div>
                            <h4 class="m-heading"><a href="">PUTERA LA GALIGO</a></h4>
                            <p><strong>Date :</strong> 11 November 2016</p>
                            <p><strong>Location :</strong> Concert, Music, Festival</p>
                          </div>
                          <div class="priceNbtn">
                            <p><span>Free</span></p>
                            <a class="getTix-btn" href="">Get Ticket</a>
                          </div>
                        </div>
                    </div>
                  </li>
                  <li>
                    <div class="media">
                        <div class="media-left media-middle">
                            <a href="#">
                                <div class="evThumb" style="background-image: url('images/bigmainbanner.jpg')"></div>
                            </a>
                        </div>
                        <div class="media-body">
                          <div class="eventText">
                            <div class="cat">Concert, Music, Festival</div>
                            <h4 class="m-heading"><a href="">NEON #CountdownNYE 2016</a></h4>
                            <p><strong>Date :</strong> 11 November 2016</p>
                            <p><strong>Location :</strong> Concert, Music, Festival</p>
                          </div>
                          <div class="priceNbtn">
                            <p><span>RM130</span></p>
                            <a class="getTix-btn" href="">Get Ticket</a>
                          </div>
                        </div>
                    </div>
                  </li>
                  <li>
                    <div class="media">
                        <div class="media-left media-middle">
                            <a href="#">
                                <div class="evThumb" style="background-image: url('images/AFGAN.jpg')"></div>
                            </a>
                        </div>
                        <div class="media-body">
                          <div class="eventText">
                            <div class="cat">Concert, Music, Festival</div>
                            <h4 class="m-heading"><a href="">Afgan Sides Live in Miri 2016</a></h4>
                            <p><strong>Date :</strong> 11 November 2016</p>
                            <p><strong>Location :</strong> Concert, Music, Festival</p>
                          </div>
                          <div class="priceNbtn">
                            <p>From <span>RM30 - RM300</span></p>
                            <a class="getTix-btn" href="">Get Ticket</a>
                          </div>
                        </div>
                    </div>
                  </li>
                  <li>
                    <div class="media">
                        <div class="media-left media-middle">
                            <a href="#">
                                <div class="evThumb" style="background-image: url('images/event1.jpg')"></div>
                            </a>
                        </div>
                        <div class="media-body">
                          <div class="eventText">
                            <div class="cat">Concert, Music, Festival</div>
                            <h4 class="m-heading"><a href="">Ucop Buat Hal Lagi! Live in MATIC</a></h4>
                            <p><strong>Date :</strong> 11 November 2016</p>
                            <p><strong>Location :</strong> Concert, Music, Festival</p>
                          </div>
                          <div class="priceNbtn">
                            <p>From<span>RM180 - RM450</span></p>
                            <a class="getTix-btn" href="">Get Ticket</a>
                          </div>                            
                        </div>
                    </div>
                  </li>
                  <li>
                    <div class="media">
                        <div class="media-left media-middle">
                            <a href="#">
                                <div class="evThumb" style="background-image: url('images/event2.jpg')"></div>
                            </a>
                        </div>
                        <div class="media-body">
                          <div class="eventText">
                            <div class="cat">Concert, Music, Festival</div>
                            <h4 class="m-heading"><a href="">PUTERA LA GALIGO</a></h4>
                            <p><strong>Date :</strong> 11 November 2016</p>
                            <p><strong>Location :</strong> Concert, Music, Festival</p>
                          </div>
                          <div class="priceNbtn">
                            <p><span>Free</span></p>
                            <a class="getTix-btn" href="">Get Ticket</a>
                          </div>
                        </div>
                    </div>
                  </li>
                  <li>
                    <div class="media">
                        <div class="media-left media-middle">
                            <a href="#">
                                <div class="evThumb" style="background-image: url('images/bigmainbanner.jpg')"></div>
                            </a>
                        </div>
                        <div class="media-body">
                          <div class="eventText">
                            <div class="cat">Concert, Music, Festival</div>
                            <h4 class="m-heading"><a href="">NEON #CountdownNYE 2016</a></h4>
                            <p><strong>Date :</strong> 11 November 2016</p>
                            <p><strong>Location :</strong> Concert, Music, Festival</p>
                          </div>
                          <div class="priceNbtn">
                            <p><span>RM130</span></p>
                            <a class="getTix-btn" href="">Get Ticket</a>
                          </div>
                        </div>
                    </div>
                  </li>
                  <li>
                    <div class="media">
                        <div class="media-left media-middle">
                            <a href="#">
                                <div class="evThumb" style="background-image: url('images/AFGAN.jpg')"></div>
                            </a>
                        </div>
                        <div class="media-body">
                          <div class="eventText">
                            <div class="cat">Concert, Music, Festival</div>
                            <h4 class="m-heading"><a href="">Afgan Sides Live in Miri 2016</a></h4>
                            <p><strong>Date :</strong> 11 November 2016</p>
                            <p><strong>Location :</strong> Concert, Music, Festival</p>
                          </div>
                          <div class="priceNbtn">
                            <p>From <span>RM30 - RM300</span></p>
                            <a class="getTix-btn" href="">Get Ticket</a>
                          </div>
                        </div>
                    </div>
                  </li>
                </ul>
              
              </div>

              <div class="clearfix text-center">
                  <ul class="pagination">
                      <li class="previous"><a href="#fakelink" class="fui-arrow-left"></a></li>
                      <li class="active"><a href="#fakelink">1</a></li>
                      <li><a href="#fakelink">2</a></li>
                      <li><a href="#fakelink">3</a></li>
                      <li><a href="#fakelink">4</a></li>
                      <li><a href="#fakelink">5</a></li>
                      <li class="next"><a href="#fakelink" class="fui-arrow-right"></a></li>
                  </ul>
              </div>
            </section>

          </div><!-- /Main Body -->

        </div><!-- /row -->

      </div>
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <!-- Initialize Swiper -->
    <script type="text/javascript">
    // Date Picker for Filter
    $('.form_date').datetimepicker({
        fontAwesome: true,
        weekStart: 1,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        minView: 2,
        forceParse: 0
    });
    </script>
    
@endsection