@extends('master')
@section('title')
    Faq
@endsection

@section('header')
    @include('layouts.partials._header')
@endsection

@section('content')

    <!-- Banner Section -->
    <section class="pageBanner">
      <div class="jumbotron" style="background-image: url('images/bigmainbanner.jpg')">
        <div class="bigBanner-overlay"></div>
        <div class="container banner-body">
          <div class="col-sm-offset-1 col-sm-10 col-xs-12">
            <div class="bannerText text-center">
              <h1>FAQ</h1>
            </div>
          </div>
        </div>
      </div>
    </section><!-- /Banner Section -->

    <!-- Content Section -->
    <section class="pageContent">
      <div class="container">
        
        <div class="row">

          <!-- Main Body -->
          <div class="mainBodyContent col-sm-9">

            <section class="pageCategory-section last">
              <h1 class="mainSecTitle">Frequently Asked Questions</h1>
              <!-- Nav tabs -->
              <ul class="nav nav-pills nav-justified" role="tablist">
                <li class="active"><a href="#general" aria-controls="general" role="tab" data-toggle="tab">General</a></li>
                <li><a href="#ticket" aria-controls="ticket" role="tab" data-toggle="tab">Ticket</a></li>
                <li><a href="#event" aria-controls="event" role="tab" data-toggle="tab">Event Day</a></li>
                <li><a href="#merchant" aria-controls="merchant" role="tab" data-toggle="tab">Merchant</a></li>
              </ul>

              <!-- Tab panes -->
              <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="general">
                  <!--Start FAQ-->                  
                  <div class="faq">
                    <div class="panel-group" id="accordion1" role="tablist" aria-multiselectable="true">
                      <h1 class="subSecTitle">General Information</h1>
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <a role="button" data-toggle="collapse" data-parent="#accordion1" href="#cat1-a-1" aria-expanded="true" aria-controls="cat1-a-1">
                            <h4 class="panel-title">What is AirAsiaRedTix.com?</h4>
                          </a>
                        </div>
                        <div id="cat1-a-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">
                            <p>AirAsiaRedTix.com is the hottest, smartest new way to discover, discuss, review and book tickets to an international line-up of concerts, sporting events, musicals, theatre performances and more.</p>
                            <p>A subsidiary of Asia’s largest low-cost carrier, AirAsiaRedTix.com is the world’s ultimate gateway to for the latest music and entertainment news, tour dates and event calendars, artiste bios, interviews, videos, songs and other cool downloads.</p>
                            <p>By partnering with premier event organizers and promoters in Asia and across the globe, AirAsiaRedTix.com offers customers a platform from which to leap into an exciting world of top-notch entertainment.</p>
                            <p>Make bookings online and have electronic tickets issued and emailed to you or sent to your mobile phones – so waiting in queue is a thing of the past!</p>
                            <p>Connect with other fans and enthusiasts in online forums to share your views, reviews and recommendations on anything from sports to music to the arts.</p> 
                          </div>
                        </div>
                      </div>

                      <h1 class="subSecTitle">Policies: Cancellation/Postponement</h1>
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <a role="button" data-toggle="collapse" data-parent="#accordion1" href="#cat1-b-1" aria-expanded="true" aria-controls="cat1-b-1">
                            <h4 class="panel-title">If I am unable to attend the event, can I get a refund for my purchased tickets?</h4>
                          </a>
                        </div>
                        <div id="cat1-b-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">
                            <p>No. All sales are final and there are no refunds.</p>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <a role="button" data-toggle="collapse" data-parent="#accordion1" href="#cat1-b-2" aria-expanded="true" aria-controls="cat1-b-2">
                            <h4 class="panel-title">What happens if an event is cancelled or postponed?</h4>
                          </a>
                        </div>
                        <div id="cat1-b-2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">
                            <p>AirAsiaRedTix.com nor its parent company AirAsia Berhad is responsible or liable for any event cancellations or postponements. Event promoters will have to address this matter in their terms and conditions directly to you, the patron. AirAsiaRedTix.com will however require merchants or event promoters to inform patrons on details of event change or postponement or cancellation.</p>
                          </div>
                        </div>
                      </div>

                      <h1 class="subSecTitle">Customer Service</h1>
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <a role="button" data-toggle="collapse" data-parent="#accordion1" href="#cat1-c-1" aria-expanded="true" aria-controls="cat1-c-1">
                            <h4 class="panel-title">General : If I have unanswered questions, who do I contact?</h4>
                          </a>
                        </div>
                        <div id="cat1-c-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">
                            <p>Please email us at <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a></p>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <a role="button" data-toggle="collapse" data-parent="#accordion1" href="#cat1-c-2" aria-expanded="true" aria-controls="cat1-c-2">
                            <h4 class="panel-title">Merchant : How do I participate in AirAsiaRedTix.com as a merchant/promoter?</h4>
                          </a>
                        </div>
                        <div id="cat1-c-2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">
                            <p>Please email us at <a href="mailto:merchant@airasiaredtix.com">merchant@airasiaredtix.com</a> to initiate supply inventory and access for direct sales.</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!--End FAQ-->
                </div>
                <div role="tabpanel" class="tab-pane" id="ticket">
                  <!--Start FAQ-->
                  <div class="faq">
                    <div class="panel-group" id="accordion2" role="tablist" aria-multiselectable="true">
                      <h1 class="subSecTitle">Paperless Ticket</h1>
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <a role="button" data-toggle="collapse" data-parent="#accordion2" href="#cat2-a-1" aria-expanded="true" aria-controls="cat2-a-1">
                            <h4 class="panel-title">How does the paperless ticketing system work?</h4>
                          </a>
                        </div>
                        <div id="cat2-a-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">
                            <p>The system provides for delivery of a paper ticket voucher that will either be your dedicated, unique ticket or a Supplier Related Voucher (SRV) dependant on where your event is happening. Your paper ticket can be presented at the event gate where the gate staff will check your ticket and provide entry. Note that paper tickets are checked for security</p>
                            <p>Should you get an SRV, please bring it along to the event venue and exchange it at the box office service point for your event ticket.</p>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <a role="button" data-toggle="collapse" data-parent="#accordion2" href="#cat2-a-2" aria-expanded="true" aria-controls="cat2-a-2">
                            <h4 class="panel-title">How do I present my mobile phone ticket at the event venue?</h4>
                          </a>
                        </div>
                        <div id="cat2-a-2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">
                            <p>Open the paperless ticket in your email PDF attachment on your mobile phone email browser and present at the gate for the barcode to be scanned.</p>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <a role="button" data-toggle="collapse" data-parent="#accordion2" href="#cat2-a-3" aria-expanded="true" aria-controls="cat2-a-3">
                            <h4 class="panel-title">What details are on the paperless ticket?</h4>
                          </a>
                        </div>
                        <div id="cat2-a-3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">
                            <p>Name of event, seat number, date, time, venue and a secure barcode. Where an SRV is delivered, the same event details will be included as well as information on what needs to be done event venue to swap the SRV for your venue tickets.</p>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <a role="button" data-toggle="collapse" data-parent="#accordion2" href="#cat2-a-4" aria-expanded="true" aria-controls="cat2-a-4">
                            <h4 class="panel-title">Can I opt to receive my paperless ticket via email or mobile phone or both?</h4>
                          </a>
                        </div>
                        <div id="cat2-a-4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">
                            <p>Currently, you will receive your paperless ticket via email through your inbox on your computer or email browser on your mobile phone. We will be introducing paperless ticket via SMS (short messaging service) very soon.</p>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <a role="button" data-toggle="collapse" data-parent="#accordion2" href="#cat2-a-5" aria-expanded="true" aria-controls="cat2-a-5">
                            <h4 class="panel-title">How do I download paperless ticket onto my computer?</h4>
                          </a>
                        </div>
                        <div id="cat2-a-5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">
                            <p>The email attachment in your inbox on your email browser will have a PDF file. Open the PDF, print it out on a white A4 sheet of paper and present to the venue box office service point.</p>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <a role="button" data-toggle="collapse" data-parent="#accordion2" href="#cat2-a-6" aria-expanded="true" aria-controls="cat2-a-6">
                            <h4 class="panel-title">How do I upload paperless ticket onto my mobile phone?</h4>
                          </a>
                        </div>
                        <div id="cat2-a-6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">
                            <p>The email attachment in your inbox on your email browser will have a PDF file. Open the PDF and present to the venue box office service point.</p>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <a role="button" data-toggle="collapse" data-parent="#accordion2" href="#cat2-a-7" aria-expanded="true" aria-controls="cat2-a-7">
                            <h4 class="panel-title">I do not have a mobile phone or computer. How do I receive my paperless tickets?</h4>
                          </a>
                        </div>
                        <div id="cat2-a-7" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">
                            <p>The AirAsiaRedTix.com ticketing process is built on and relies on web sales and e-ticket delivery.</p>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <a role="button" data-toggle="collapse" data-parent="#accordion2" href="#cat2-a-8" aria-expanded="true" aria-controls="cat2-a-8">
                            <h4 class="panel-title">What do I do if I don't receive my tickets via email/mobile phone?</h4>
                          </a>
                        </div>
                        <div id="cat2-a-8" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">
                            <p>If you haven’t received your email and attachments through your computer or email browser on your mobile phone, check your spam or junk folder. If not, contact <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <a role="button" data-toggle="collapse" data-parent="#accordion2" href="#cat2-a-9" aria-expanded="true" aria-controls="cat2-a-9">
                            <h4 class="panel-title">Do I need to print out my paperless ticket?</h4>
                          </a>
                        </div>
                        <div id="cat2-a-9" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">
                            <p>That depends on the kind of ticket you receive. Certain SRV will allow you to store the ticket on your mobile as a PDF attachment, or require a hardcopy to be printed. Others may require you to change your SRV at the venue or box office service point.</p>
                          </div>
                        </div>
                      </div>


                      <h1 class="subSecTitle">Buying Online</h1>
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <a role="button" data-toggle="collapse" data-parent="#accordion2" href="#cat2-b-1" aria-expanded="true" aria-controls="cat2-b-1">
                            <h4 class="panel-title">How do I purchase tickets?</h4>
                          </a>
                        </div>
                        <div id="cat2-b-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">
                            <p>Just click “Buy Now” on the event you want to attend and follow the on-screen instructions. Your information and details are kept in a secure transaction, so you don’t have to worry about them.</p>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <a role="button" data-toggle="collapse" data-parent="#accordion2" href="#cat2-b-2" aria-expanded="true" aria-controls="cat2-b-2">
                            <h4 class="panel-title">What sort of payment modes does AirAsiaRedTix.com accept?</h4>
                          </a>
                        </div>
                        <div id="cat2-b-2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">
                            <p>AirAsiaRedTix.com accepts Visa and MasterCard.</p>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <a role="button" data-toggle="collapse" data-parent="#accordion2" href="#cat2-b-3" aria-expanded="true" aria-controls="cat2-b-3">
                            <h4 class="panel-title">How do I get a receipt for my ticket purchase?</h4>
                          </a>
                        </div>
                        <div id="cat2-b-3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">
                            <p>Your email confirmation will include an automatically produced receipt for your purchase.</p>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <a role="button" data-toggle="collapse" data-parent="#accordion2" href="#cat2-b-4" aria-expanded="true" aria-controls="cat2-b-4">
                            <h4 class="panel-title">How do I keep track of my transaction?</h4>
                          </a>
                        </div>
                        <div id="cat2-b-4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">
                            <p>All details of the transaction will be on your email receipt. Log on to AirAsiaRedTix.com on a regular basis to check for event updates.</p>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <a role="button" data-toggle="collapse" data-parent="#accordion2" href="#cat2-b-5" aria-expanded="true" aria-controls="cat2-b-5">
                            <h4 class="panel-title">Do I need to purchase ticket for my child?</h4>
                          </a>
                        </div>
                        <div id="cat2-b-5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">
                            <p>This depends on the admission rules for the event. Unless specified, usually infants and children without tickets will not be admitted. However this differs from event to event. Please check the admission rules on the event page for more details.</p>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <a role="button" data-toggle="collapse" data-parent="#accordion2" href="#cat2-b-6" aria-expanded="true" aria-controls="cat2-b-6">
                            <h4 class="panel-title">Is there a time limit when I purchase tickets online?</h4>
                          </a>
                        </div>
                        <div id="cat2-b-6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">
                            <p>Yes. Once you’ve begun the purchase process, your seats will be held for 10 minutes. You’ll need to complete your transaction within that time or you’ll be logged out automatically.</p>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <a role="button" data-toggle="collapse" data-parent="#accordion2" href="#cat2-b-7" aria-expanded="true" aria-controls="cat2-b-7">
                            <h4 class="panel-title">Is there a limit to the tickets I purchase online?</h4>
                          </a>
                        </div>
                        <div id="cat2-b-7" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">
                            <p>Ticket limits may vary depending on the event.</p>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <a role="button" data-toggle="collapse" data-parent="#accordion2" href="#cat2-b-8" aria-expanded="true" aria-controls="cat2-b-8">
                            <h4 class="panel-title">Can I cancel my ticket purchase?</h4>
                          </a>
                        </div>
                        <div id="cat2-b-8" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">
                            <p>Once your transaction is completed, it cannot be cancelled or exchange and any payment made are not refundable.</p>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <a role="button" data-toggle="collapse" data-parent="#accordion2" href="#cat2-b-9" aria-expanded="true" aria-controls="cat2-b-9">
                            <h4 class="panel-title">How can I seek assistance when purchasing tickets on AirAsiaRedTix.com?</h4>
                          </a>
                        </div>
                        <div id="cat2-b-9" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">
                            <p>Please email us at <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                          </div>
                        </div>
                      </div>


                      <h1 class="subSecTitle">Security</h1>
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <a role="button" data-toggle="collapse" data-parent="#accordion2" href="#cat2-c-1" aria-expanded="true" aria-controls="cat2-c-1">
                            <h4 class="panel-title">Is my online purchase secure?</h4>
                          </a>
                        </div>
                        <div id="cat2-c-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">
                            <p>Yes. Several unique security features are in place, including a complete security payment process with a customised payment “vault” system that allows pre-registered credit and debit cards, direct debit and e-wallets to be use. It takes about 48 hours for ticket buyers to receive their tickets after the payment process has been completed. This allows our Fraud Detection team to identify possible areas of irregularities and protect the ticket buyer from online fraud.</p>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <a role="button" data-toggle="collapse" data-parent="#accordion2" href="#cat2-c-2" aria-expanded="true" aria-controls="cat2-c-2">
                            <h4 class="panel-title">What does being 3D-Secure compliant mean and what does it have to do with my card?</h4>
                          </a>
                        </div>
                        <div id="cat2-c-2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">
                            <p>3D-Secure compliant is a security feature implemented to prevent fraud. To purchase tickets on AirAsiaRedTix.com you’ll need ensure this feature is activated on your card. These features are identified as either "Verified by Visa" or "MasterCard SecureCode" depending on your card. </p>
                            <p>For more details, please contact your issuing bank.</p>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <a role="button" data-toggle="collapse" data-parent="#accordion2" href="#cat2-c-3" aria-expanded="true" aria-controls="cat2-c-3">
                            <h4 class="panel-title">How do I activate my card’s Verified by Visa or MasterCard Securecode feature?</h4>
                          </a>
                        </div>
                        <div id="cat2-c-3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">
                            <p>Contact your credit card issuing bank.</p>
                          </div>
                        </div>
                      </div>


                      <h1 class="subSecTitle">Seat Reservation</h1>
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <a role="button" data-toggle="collapse" data-parent="#accordion2" href="#cat2-d-1" aria-expanded="true" aria-controls="cat2-d-1">
                            <h4 class="panel-title">Can I select my own seats?</h4>
                          </a>
                        </div>
                        <div id="cat2-d-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">
                            <p>This depends on the event venue. Where seating plans are available, you will be able to select specific seats.</p>
                          </div>
                        </div>
                      </div>


                      <h1 class="subSecTitle">Ticket Upgrade / Downgrade</h1>
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <a role="button" data-toggle="collapse" data-parent="#accordion2" href="#cat2-e-1" aria-expanded="true" aria-controls="cat2-e-1">
                            <h4 class="panel-title">Can I upgrade or downgrade purchased tickets?</h4>
                          </a>
                        </div>
                        <div id="cat2-e-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">
                            <p>No. All sales are final.</p>
                          </div>
                        </div>
                      </div>

                      <h1 class="subSecTitle">Letter of Authorization</h1>
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <a role="button" data-toggle="collapse" data-parent="#accordion2" href="#cat2-f-1" aria-expanded="true" aria-controls="cat2-f-1">
                            <h4 class="panel-title">What if I am unable to collect my tickets?</h4>
                          </a>
                        </div>
                        <div id="cat2-f-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">
                            <p>If you are the ticket holder and are unable to collect your tickets using your e-ticket confirmation, you may appoint someone else to collect on your behalf.</p>
                            <p>Letter of Authorization: <a href="{{asset('document/aart-authorization-letter-v1.pdf')}}">Download your copy here</a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!--End FAQ-->
                </div>
                <div role="tabpanel" class="tab-pane" id="event">
                  <!--Start FAQ-->
                  <div class="faq">
                    <div class="panel-group" id="accordion3" role="tablist" aria-multiselectable="true">
                      <h1 class="subSecTitle">Do's and Dont's</h1>
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <a role="button" data-toggle="collapse" data-parent="#accordion3" href="#cat3-a-1" aria-expanded="true" aria-controls="cat3-a-1">
                            <h4 class="panel-title">How do I present my email ticket at the event venue?</h4>
                          </a>
                        </div>
                        <div id="cat3-a-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">
                            <p>Print the email PDF attachment on a white A4 sheet and present it to the gate staff. They’ll scan the barcode and allow entry based on the event detail and number of persons in your ticket. The same applies when showing the PDF attachment in your email browser on your mobile phone.</p>
                            <p>Where the email delivery includes an SRV, you will have details on the SRV as to the arrangements at the venue. Note that where you have bought a ticket for multiple people to enter, they must present together as your group.</p>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <a role="button" data-toggle="collapse" data-parent="#accordion3" href="#cat7-a-2" aria-expanded="true" aria-controls="cat7-a-2">
                            <h4 class="panel-title">How do I present my mobile phone ticket at the event venue?</h4>
                          </a>
                        </div>
                        <div id="cat7-a-2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">
                            <p>Open the paperless ticket in your email PDF attachment on your mobile phone email browser and present at the gate for the barcode to be scanned.</p>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <a role="button" data-toggle="collapse" data-parent="#accordion3" href="#cat7-a-3" aria-expanded="true" aria-controls="cat7-a-3">
                            <h4 class="panel-title">If there is a group of us and we are arriving separately, can we present our paperless tickets individually?</h4>
                          </a>
                        </div>
                        <div id="cat7-a-3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">
                            <p>At present, you are required to be present together at the gate with the person who bought the group tickets. If the email attachment of your purchase is for a ticket, then that can be printed and passed to your group members and the access control will check the barcode and allow the entries at different times. Where your email attachment is an SRV and needs swapping for tickets at the venue, the purchaser will have to collect all the tickets and distribute them to the group.</p>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <a role="button" data-toggle="collapse" data-parent="#accordion3" href="#cat7-a-4" aria-expanded="true" aria-controls="cat7-a-4">
                            <h4 class="panel-title">What if I lose my mobile phone? How can I retrieve my paperless tickets?</h4>
                          </a>
                        </div>
                        <div id="cat7-a-4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">
                            <p>If you lose your mobile phone just before the event, you will be able to go to a service point at the venue where you can present valid identification for a security check to be made and replacement tickets to be arranged. Note that lost tickets may not be replaced in all cases where full security checks cannot be made and if you are unable to provide valid identification.</p>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <a role="button" data-toggle="collapse" data-parent="#accordion3" href="#cat7-a-5" aria-expanded="true" aria-controls="cat7-a-5">
                            <h4 class="panel-title">How can I find out more about the event?</h4>
                          </a>
                        </div>
                        <div id="cat7-a-5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">
                            <p>Log on to https://www.AirAsiaRedTix.com on a regular basis to check for event updates.</p>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <a role="button" data-toggle="collapse" data-parent="#accordion3" href="#cat7-a-6" aria-expanded="true" aria-controls="cat7-a-6">
                            <h4 class="panel-title">Is there any restriction that I should be aware of?</h4>
                          </a>
                        </div>
                        <div id="cat7-a-6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">
                            <p>You are not allowed to bring any audio and video recording equipment into most venues. Usually, the venue will require you to deposit your recording equipment and collect it after the event.</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!--End FAQ-->
                </div>
                <div role="tabpanel" class="tab-pane" id="merchant">
                  <!--Start FAQ-->
                  <div class="faq">
                    <div class="panel-group" id="accordion4" role="tablist" aria-multiselectable="true">

                      <h1 class="subSecTitle">General</h1>
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <a role="button" data-toggle="collapse" data-parent="#accordion4" href="#cat4-a-1" aria-expanded="true" aria-controls="cat4-a-1">
                            <h4 class="panel-title">Does AirAsiaRedTix.com offer business-to-business opportunities to event organisers and promoters?</h4>
                          </a>
                        </div>
                        <div id="cat4-a-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">
                            <p>Yes. AirAsiaRedTix.com is an e-commerce version of the ticket broker business. We offer event organizers and promoters an avenue to sell their tickets online through AirAsiaRedTix.com’s secure electronic ticketing platform.</p>
                          </div>
                        </div>
                      </div>


                      <h1 class="subSecTitle">Join Us</h1>
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <a role="button" data-toggle="collapse" data-parent="#accordion4" href="#cat4-b-1" aria-expanded="true" aria-controls="cat4-b-1">
                            <h4 class="panel-title">How do I participate in AirAsiaRedTix.com as a merchant/promoter?</h4>
                          </a>
                        </div>
                        <div id="cat4-b-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">
                            <p>Please contact us via email at <a href="mailto:merchant_redtix@airasia.com">merchant_redtix@airasia.com</a>. This will allow for supply inventory and allow direct access for direct sales.</p>
                          </div>
                        </div>
                      </div>

                      <h1 class="subSecTitle">Roles & Responsibilities</h1>
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <a role="button" data-toggle="collapse" data-parent="#accordion4" href="#cat4-c-1" aria-expanded="true" aria-controls="cat4-c-1">
                            <h4 class="panel-title">What are my responsibilities?</h4>
                          </a>
                        </div>
                        <div id="cat4-c-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">
                            <p>Your responsibility includes providing discounts, allow use of SRV or PDF ticket from AirAsiaRedTix.com and provide service point for exchange of SRV for ticket.</p>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <a role="button" data-toggle="collapse" data-parent="#accordion4" href="#cat4-c-2" aria-expanded="true" aria-controls="cat4-c-2">
                            <h4 class="panel-title">How do I sell tickets to my event on AirAsiaRedTix.com?</h4>
                          </a>
                        </div>
                        <div id="cat4-c-2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">
                            <p>We can supply a web page or link direct from the merchant or promoter’s website for tickets sold exclusively by AirAsiaRedTix.com.</p>
                          </div>
                        </div>
                      </div>

                      <h1 class="subSecTitle">Event Listing</h1>
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <a role="button" data-toggle="collapse" data-parent="#accordion4" href="#cat4-d-1" aria-expanded="true" aria-controls="cat4-d-1">
                            <h4 class="panel-title">How do I list my event on AirAsiaRedTix.com?</h4>
                          </a>
                        </div>
                        <div id="cat4-d-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">
                            <p>Contact us at <a href="mailto:merchant_redtix@airasia.com">merchant_redtix@airasia.com</a>.</p>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <a role="button" data-toggle="collapse" data-parent="#accordion4" href="#cat4-d-2" aria-expanded="true" aria-controls="cat4-d-2">
                            <h4 class="panel-title">Can I edit my listings?</h4>
                          </a>
                        </div>
                        <div id="cat4-d-2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">
                            <p>The direct build tools, available to suppliers who have completed the operational Terms & Conditions agreement and ensured that all edit/changes are approved by AirAsiaRedTix.com prior, will allow you to edit your event.</p>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <a role="button" data-toggle="collapse" data-parent="#accordion4" href="#cat4-d-3" aria-expanded="true" aria-controls="cat4-d-3">
                            <h4 class="panel-title">How do I make an event or price correction in a current listing?</h4>
                          </a>
                        </div>
                        <div id="cat4-d-3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">
                            <p>The direct build tools, available to suppliers who have completed the operational Terms & Conditions agreement and ensured that all edit/changes are approved by AirAsiaRedTix.com prior, will allow you to edit your event.</p>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <a role="button" data-toggle="collapse" data-parent="#accordion4" href="#cat4-d-4" aria-expanded="true" aria-controls="cat4-d-4">
                            <h4 class="panel-title">How do I list event profile and upload videos and images?</h4>
                          </a>
                        </div>
                        <div id="cat4-d-4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">
                            <p>There will be options to supply a build document or direct access which will be made available soon to provide you the ability to build your event(s). Depending on where sales are controlled, either exclusivity with AirAsiaRedTix.com or as agency sales, some levels of uploading will be restricted.</p>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <a role="button" data-toggle="collapse" data-parent="#accordion4" href="#cat4-d-5" aria-expanded="true" aria-controls="cat4-d-5">
                            <h4 class="panel-title">Can I list my company profile?</h4>
                          </a>
                        </div>
                        <div id="cat4-d-5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">
                            <p>No. You may only list the event you are promoting.</p>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <a role="button" data-toggle="collapse" data-parent="#accordion4" href="#cat4-d-6" aria-expanded="true" aria-controls="cat4-d-6">
                            <h4 class="panel-title">Are there specific types of events AirAsiaRedTix.com will not allow me to sell tickets?</h4>
                          </a>
                        </div>
                        <div id="cat4-d-6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">
                            <p>Please write to <a href="mailto:merchant_redtix@airasia.com">merchant_redtix@airasia.com</a> on the event you have in mind and we will revert a decision.</p>
                          </div>
                        </div>
                      </div>
                      
                      <h1 class="subSecTitle">Cancellation/Postponement</h1>
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <a role="button" data-toggle="collapse" data-parent="#accordion4" href="#cat4-e-1" aria-expanded="true" aria-controls="cat4-e-1">
                            <h4 class="panel-title">What happens if an event is cancelled/postponed?</h4>
                          </a>
                        </div>
                        <div id="cat4-e-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">
                            <p>AirAsiaRedTix.com nor its parent company AirAsia Berhad is responsible or liable for any event cancellations or postponements. Event promoters will have to address this matter in their terms and conditions directly to you, the patron. AirAsiaRedTix.com will however require merchants or event promoters to inform patrons on details of event change or postponement or cancellation.</p>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <a role="button" data-toggle="collapse" data-parent="#accordion4" href="#cat4-e-2" aria-expanded="true" aria-controls="cat4-e-2">
                            <h4 class="panel-title">Am I responsible for ticket refunds?</h4>
                          </a>
                        </div>
                        <div id="cat4-e-2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">
                            <p>Yes.</p>
                          </div>
                        </div>
                      </div>

                      <h1 class="subSecTitle">Pricing and Income</h1>
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <a role="button" data-toggle="collapse" data-parent="#accordion4" href="#cat4-f-1" aria-expanded="true" aria-controls="cat4-f-1">
                            <h4 class="panel-title">How do I set prices to my tickets?</h4>
                          </a>
                        </div>
                        <div id="cat4-f-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">
                            <p>You set prices to tickets based on current market trend. Take into consideration that AirAsiaRedTix.com will charge a service fee.</p>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <a role="button" data-toggle="collapse" data-parent="#accordion4" href="#cat4-f-2" aria-expanded="true" aria-controls="cat4-f-2">
                            <h4 class="panel-title">What is the fee structure for tickets sold on AirAsiaRedTix.com?</h4>
                          </a>
                        </div>
                        <div id="cat4-f-2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">
                            <p>There is an “Inside” and “Outside” charge applicable depending on the type of event, promotional materials needed and marketing portfolio.</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!--End FAQ-->
                </div>
              </div>
            </section>

          </div><!-- /Main Body -->

          <!-- Sidebar -->
          @include('layouts.partials._innersidebar')
          <!-- /Sidebar -->

        </div><!-- /row -->

      </div>
    </section><!-- /Content Section -->

@endsection

@section('customjs')

  <script type="text/javascript">
  // Change Collapse Head Color
  $('.faq').find('.panel-default:has(".in")').addClass('panel-danger');

  $('.faq').on('shown.bs.collapse', function (e) {
      $(e.target).closest('.panel-default').addClass(' panel-danger');
  }).on('hidden.bs.collapse', function (e) {
      $(e.target).closest('.panel-default').removeClass(' panel-danger');
  })
  </script>
      
@endsection