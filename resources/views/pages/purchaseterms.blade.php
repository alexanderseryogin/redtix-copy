@extends('master')
@section('title')
    Purchase Terms & Condition
@endsection

@section('header')
    @include('layouts.partials._header')
@endsection

@section('content')

    <!-- Banner Section -->
    <section class="pageBanner">
      <div class="jumbotron" style="background-image: url(/images/bigmainbanner.jpg);">
        <div class="bigBanner-overlay"></div>
        <div class="container banner-body">
          <div class="col-sm-offset-1 col-sm-10 col-xs-12">
            <div class="bannerText text-center">
              <h1>Purchase Terms & Condition</h1>
            </div>
          </div>
        </div>
      </div>
    </section><!-- /Banner Section -->

    <!-- Content Section -->
    <section class="pageContent">
      <div class="container">
        
        <div class="row">

          <!-- Main Body -->
          <div class="mainBodyContent col-sm-9">

            <section class="pageCategory-section last">
              <h1 class="mainSecTitle">Terms & Condition for Purchase</h1>
              <p>Before proceeding with your ticket purchase please read the AirAsiaRedTix.com Terms and Conditions of Purchase (the “Terms & Conditions”) carefully. These Terms & Conditions form the basis of the contract between you and RedTix Sdn Bhd (“RedTix”), the owner and operator of the AirAsiaRedTix.com website.</p>
              <p><strong>1. No Cancelations, Exchanges or Refunds</strong></p>
              <ol type="a">
                  <li>Once confirmed, a ticket purchased on AirAsiaRedTix.com (“Ticket”) may not be cancelled, exchanged or refunded.</li>
                  <li>Tickets are confirmed when full payment is received and when a confirmation e-mail together with an e-ticket is sent to you.</li>
              </ol>
              <p><strong>2. No Reselling</strong></p>
              <ol type="a">
                  <li>Tickets must not be resold or offered for resale at a premium (including via online auction websites) or used for advertising, promoting or other commercial purposes (including competitions or trade promotions) or to enhance the demand for other goods and services, either by the original Ticket purchaser or any subsequent bearer.</li>
                  <li>If a Ticket is sold in breach of this condition, the Ticket may be cancelled without a refund and the bearer of the Ticket may be refused admission.</li>
              </ol>
              <p><strong>3. Seat Allocation</strong></p>
              <p>Whether or not there will be specific seating arrangements to the event will depend on the particular event you are purchasing Tickets to. Details of such specific seating arrangements (if any) will be displayed during the booking process and in the confirmation e-mail.</p>
              <p><strong>4. E-Tickets</strong></p>
              <p>The e-ticket sent with your confirmation e-mail shall be your valid pass to the event and should be printed out and presented at the gate or box office from two (2) hours prior to the commencement of the event in order for you to gain admission.</p>
              <p><strong>5. Event Changes</strong></p>
              <ol type="a">
                  <li>Information, performance and event dates are subject to change. RedTix is not responsible for any cancellation or rescheduling of events or for any changes in the programme or performance.</li>
                  <li>In the case of an event itself being cancelled or postponed you will be refunded the amount paid by either the event promoters, organisers or agents, less any administration and handling fees.</li>
                  <li>RedTix reserves the right to retain any administration fees and charges incurred.</li>
                  <li>However, if an event or a portion thereof is cancelled or postponed after it has commenced, Ticket holders will not have the right to exchange or receive a refund on Tickets.</li>
              </ol>
              <p><strong>6. Price Changes</strong></p>
              <p>All prices are subject to change at any time and from time to time without prior notice until your Ticket purchase is confirmed.</p>
              <p><strong>7. Payment in Full</strong></p>
              <ol type="a">
                  <li>Upon acceptance of the booking, the Tickets must be paid for in full immediately.</li>
                  <li>Once a booking has been paid in full and has been deemed confirmed, Tickets cannot be exchanged, cancelled or refunded.</li>
              </ol>
              <p><strong>8. RedTix Disclaimer and Limitation of Liability</strong></p>
              <ol type="a">
                  <li>All Tickets, event information, performance and dates on AirAsiaRedTix.com are provided by the event organisers, promoters, suppliers or agents.</li>
                  <li>Any cancellation, postponement or alteration of any kind to an event is the responsibility of the event organisers, promoters, suppliers and/or agents and RedTix disclaims any and all liability for any loss or damage suffered as a result of such cancellation, postponement or alteration.</li>
                  <li>RedTix shall not be liable, apart from non-excludable liability, for any injury, death, damage, loss, accident, delays, generally or irregularity which may be occasioned either by reason of any defect in any venue, or vehicle or through the acts or omissions, defaults whether negligent or otherwise, of companies or persons engaged in connection with your Ticket purchase over whom we have no direct and exclusive control over.</li>
                  <li>RedTix shall not be liable whether in contract or in tort for any injury, damage, loss, delay, additional expenses or inconvenience caused directly or indirectly by force majeure or other events which are beyond our control or which are not preventable by reasonable diligence on our part including but not limited to war, civil disturbance, fire, floods, unusually severe weather, acts of God, acts of Government or of any other authorities, accidents to or failure of machinery or equipment or industrial action.</li>
              </ol>
              <p><strong>9. Responsibility of RedTix</strong></p>
              <p>RedTix, agrees to offer its services to the ticket purchaser as an event ticketing website via AirAsiaRedTix.com. RedTix is acting as an intermediary for promoters, organisers, suppliers and/or agents in selling event services or in accepting bookings for event services which are not directly supplied by RedTix.</p>
              <p><strong>10. Lost Tickets</strong></p>
              <ol type="a">
                  <li>When allocated seating Tickets are lost or stolen the Ticket purchaser or holder must be able to produce proof of the original purchase prior to a replacement Ticket being issued.</li>
                  <li>Replacement of lost tickets is subject to the event venue, organisers, promoters, suppliers and/or agents lost ticket replacement terms and conditions.</li>
                  <li>RedTix reserves the right to charge an administration fee if a replacement Ticket needs to be issued.</li>
                  <li>RedTix reserves the right not to replace Tickets where seating is not allocated (general admission).</li>
              </ol>
              <p><strong>11. Independent Contractors</strong></p>
              <ol type="a">
                  <li>Services provided in conjunction with these events are rendered by promoters, organisers, suppliers or event agents acting as independent contractors and not as agents or employees of RedTix.</li>
                  <li>RedTix therefore shall not be responsible for any breach of contract or for any intentional or negligent action or omission on the part of such promoters, organisers, suppliers, and/or event agents which result in any loss, damage, delay or injury to the ticket purchaser, ticket holders or their travel companions.</li>
              </ol>
              <p><strong>12. Third Party Information</strong></p>
              <ol type="a">
                  <li>AirAsia has utilised information provided by third parties and all attempts have been made to ensure the accuracy of this information.</li>
                  <li>Alterations and amendments to such information may occur at any time and from time to time.</li>
                  <li>AirAsia shall not be responsible for the accuracy of information provided and any alteration or amendment to such information.</li>
              </ol>
              <p><strong>13. Independence</strong></p>
              <p>RedTix is an independently owned and operated business. Any reference to specific events by RedTix is not intended to imply nor should it be understood or assumed that RedTix is endorsed by, sponsored by, approved by or otherwise affiliated with the organisers, promoters and/or sponsors of such events.</p>
              <p><strong>14. Enforceability</strong></p>
              <p>If any part of these Terms & Conditions is held to be unenforceable, the unenforceable part must be given effect to the greatest extent possible and the remainder shall remain in full force and effect.</p>
              <p><strong>15. Laws and Jurisdiction</strong></p>
              <p>These Terms & Conditions are governed by the laws of Malaysia. You irrevocably submit to the exclusive jurisdiction of the courts of Malaysia.</p>
            </section>
            
          </div><!-- /Main Body -->

          <!-- Sidebar -->
          @include('layouts.partials._innersidebar')
          <!-- /Sidebar -->

        </div><!-- /row -->

      </div>
    </section><!-- /Content Section -->

@endsection