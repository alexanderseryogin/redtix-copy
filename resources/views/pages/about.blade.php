@extends('master')
@section('title')
    About
@endsection

@section('header')
    @include('layouts.partials._header')
@endsection

@section('content')

    <!-- Banner Section -->
    <section class="pageBanner">
      <div class="jumbotron" style="background-image: url('images/bigmainbanner.jpg')">
        <div class="bigBanner-overlay"></div>
        <div class="container banner-body">
          <div class="col-sm-offset-1 col-sm-10 col-xs-12">
            <div class="bannerText text-center">
              <h1>About</h1>
            </div>
          </div>
        </div>
      </div>
    </section><!-- /Banner Section -->

    <!-- Content Section -->
    <section class="pageContent">
      <div class="container">
        
        <div class="row">

          <!-- Main Body -->
          <div class="mainBodyContent col-sm-9">

            <section class="pageCategory-section last">
                <h1 class="mainSecTitle">AirAsiaRedTix.com</h1>
                <p>AirAsiaRedTix.com is the hottest, smartest new way to discover, discuss, review and book tickets to an international line-up of concerts, sporting events, musicals, theatre performances and more.</p>
                <p>A subsidiary of Asia’s largest low-cost carrier, AirAsiaRedTix.com is the world’s ultimate gateway to for the latest music and entertainment news, tour dates and event calendars, artiste bios, interviews, videos, songs and other cool downloads.</p>
                <p>By partnering with premier event organizers and promoters in Asia and across the globe, AirAsiaRedTix.com offers customers a platform from which to leap into an exciting world of top-notch entertainment.
                <p>Make bookings online and have electronic tickets issued and emailed to you or sent to your mobile phones – so waiting in queue is a thing of the past!</p>
                <p>Connect with other fans and enthusiasts in online forums to share your views, reviews and recommendations on anything from sports to music to the arts.</p>
            </section>

          </div><!-- /Main Body -->

          <!-- Sidebar -->
          @include('layouts.partials._innersidebar')
          <!-- /Sidebar -->

        </div><!-- /row -->

      </div>
    </section><!-- /Content Section -->

@endsection