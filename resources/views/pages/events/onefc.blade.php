@extends('master')
@section('title')
    ONE: QUEST FOR GREATNESS
@endsection

@section('header')
    @include('layouts.partials._header')
    
@endsection

@section('content')
<link type="text/css" rel="stylesheet" href="css/custom2.css">

<style type="text/css">
  @media only screen and (max-device-width:1080px) {
    h1,h3 {
        font-size: 20px; 
    }
  }
</style>


    <!-- Banner Section -->
    <section class="innerPageBanner">
      <div class="bigBanner-overlay"></div>
      <div class="jumbotron eventBanner" style="height:100%; width:100%"> <!-- style="background-image: url('images/onefc/onefc-newbanner.jpg'); height:815px"> -->
        <img class="img-responsive" style="width:100%" src="images/onefc/onefc-newbanner.jpg">
        {{-- <video id="video-background" preload muted autoplay loop>
          <source src="images/neon/top-banner2000x450.mp4" type="video/mp4">
        </video> --}}
      </div>
    </section><!-- /Banner Section -->

    <!-- Title and Price -->
    <!-- <div id="priceFixed" class="section-grey">
        <div class="container">
          <div class="row priceNbtn">
            <div class="col-sm-offset-1 col-sm-10">
              <div class="row">
                <div class="col-sm-9 leftBox">
                  <h6>ONE: QUEST FOR GREATNESS</h6>
                  Tickets from <span>RM29</span>
                </div>
                <div class="col-sm-3 text-center">
                  <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                </div>
              </div>
            </div>
          </div>          
        </div>
    </div> -->

    <!-- Content Section -->
    <section class="pageContent section-black">
      <!-- Main Body -->
      <div class="mainBodyContent no-btm-mar">

        <section class="pageCategory-section last section-black">
          
          <div class="container intro">
            <div class="row">
              
                <!--<h2>Title</h2>-->
                <!-- <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i> 18 August 2017, Friday</div>
                <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i> Stadium Negara, Kuala Lumpur <a target="_blank" href="https://www.google.com/maps/place/Stadium+Negara/@3.1406192,101.7018275,17z/data=!4m5!3m4!1s0x31cc49d9c3745aa3:0x2b1b8593ee146b8f!8m2!3d3.1409406!4d101.702954">View Map</a></div>
                <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> 7.00 pm</div>
                <div class="clearfix">&nbsp;</div> -->
                <!-- Go to www.addthis.com/dashboard to customize your tools -->
                <!-- <div class="addthis_inline_share_toolbox"></div> -->
                <!-- /sharing -->
                <h5 class="title">ABOUT<span class="titleRed"> EVENT</span></h5>
                <div class="col-md-12"> 
                  <div class="col-md-6">
                    <p>HERE COMES THE PAIN! ONE Championship is back in Malaysia. Treat yourself to a constellation of the best and baddest MMA stars in the world. Come down to Stadium Negara on the 18th of August 2017, 7 P.M.</p>
                    <p>A guaranteed explosive fight is in stores for all MMA fans. In the main event, the undefeated, reigning ONE Featherweight World Champion Marat Gafurov (15-0) will risk his winning streak against the rising star from Australia, Martin Nguyen (8-1). The thrill and spills continues with Ev Ting facing former welterweight champion Nobutatsu Suzuki, Kevin Belingon faces Reece McLaren, while Gianni Subba battles Riku Shibuya.</p>
                  </div>
                  <div class="col-md-6">
                    <p>Malaysia’s favourite fighters Agilan Thani, Ann Osman and Keanu Subba are looking to make the country proud by bringing home the glory</p>
                    <p>We have a non-stop, action-packed-filled Friday night with your name on it (and maybe with a couple of your friends too!). You’ll be kicking yourself if you miss this fighting event of year. See you at the Octagon</p>
                  </div>
                </div>
            </div>
          </div>

        </section>

        <section class="pageCategory-section last">
          <div class="container">
            <h5 class="title">ABOUT<span class="titleRed"> FIGHTER</span></h5>
            <!-- fighter 1 -->
              <div class="col-md-12 fighterCard">
                <div class="col-md-6">
                  <img class="img-responsive" src="images/onefc/marat-profile-pic.jpg" > 
                </div>
                <div class="col-md-6">
                  <div class="fighterName">
                    <h1>Marat “Cobra”</h1><br>
                    <h3>Gafurov</h3>
                  </div> 
                  <div class="fighterCap">
                    <p>Marat Gafurov is from Dagestan, a Russian republic that is home to several top contenders around the world and rapidly establishing itself as an MMA stronghold. Gafurov began training in martial arts in 2002 because it was something he had always dreamed of doing, ultimately earning a black belt in Brazilian jiu jitsu (BJJ).</p>
                    <p>Gafurov would go on to become a world champion in Pankration, and win numerous regional tournaments in BJJ and grappling. He made his professional MMA debut in 2010, winning by first-round submission. He soon established himself as the number one featherweight in all of Russia, capturing the M-1 featherweight title in the process.</p>
                    <p>Still undefeated in his career, Gafurov is the reigning ONE Featherweight World Champion, and has cemented his place in history with his dominance and the longest streak of rear-naked choke victories in professional MMA.</p>
                  </div>  
                  <div class="fighterDetails">
                    <div class="col-md-12 row">
                      <div class="col-md-6 row">
                        <p>Nationality : Russia<br>
                            Age : 33<br>
                            Height : 5’ 8”<br>
                            Weight : 145 lbs<br>
                            Class : Featherweight</p>
                      </div>
                      <div class="col-md-6 row">
                        <p>Wins: 15<br>
                          TKO/KO: 1 (7%)<br>
                          Submissions: 11 (73%)<br>
                          Decisions: 3 (20%)</p>
                      </div>
                    </div>
                    <a target="_blank" href="http://www.onefc.com/world-champions/marat-gafurov">http://www.onefc.com/world-champions/marat-gafurov</a>
                  </div>
                </div>
              </div>
              <!-- ends -->

            <!-- fighter 2 -->
              <div class="col-md-12 fighterCard">
                <div class="col-sm-6 col-sm-push-6">
                  <img class="img-responsive" src="images/onefc/nguyen-profile-pic.jpg" > 
                </div>
                <div class="col-sm-6 col-sm-pull-6">
                  <div class="fighterName">
                    <h1>Martin “The Situ-Asian”</h1><br>
                    <h3>Nguyen</h3>
                  </div>  
                  <div class="fighterCap">
                    <p>Martin Nguyen is from Liverpool, New South Wales in Australia where he trains at KMA Top Team gym under head coach Fari Salievski.</p>
                    <p>Nguyen burst into the MMA scenes when he dominated BRACE with an (8-1) win-loss stats. In November 2013, he became featherweight champion of BRACE 24. Capturing the eyes of the big boys, the rising star from Australia was offered a fight on ONE: Battle of Lions which took place in Singapore in November 2014. He impressed watchful eyes with a submission onto Rocky Batolbatol, ending an impressive debut with a win</p>
                    <p>This is Martin Nguyen’s chance for redemption against the undefeated Marat Gafurov. The two faced each other in 2015 when Gafurov submitted Nguyen within 41-seconds into bout. To redeem himself, and be crowned crowned the ONE Featherweight World Champion, Nguyen will have to bring his A-game into the Octagon against Gafurov</p>
                  </div>  
                  <div class="fighterDetails">
                    <div class="col-md-12 row">
                      <div class="col-md-6 row">
                        <p>Nationality : Australia<br>
                        Age : 29<br>
                        Height : 5’ 8”<br>
                        Weight : 145 lbs<br>
                        Class : Featherweight</p>
                      </div>
                      <div class="col-md-6 row">
                        <p>Wins : 8<br>
                        TKO/KO: 5 (63%)<br>
                        Submissions: 3  (38%)<br>
                        Decisions: 0 (0%)</p>
                      </div>
                    </div>
                    <a target="_blank" href="http://www.onefc.com">http://www.onefc.com</a>
                  </div>
                </div>
              </div> 
            <!-- ends -->

            <!-- fighter 3 -->
              <div class="col-md-12 fighterCard2">
                <div class="col-md-2">
                  <img class="img-responsive" src="images/onefc/ev-profile-pic.jpg">
                </div>
                <div class="col-md-4">
                  <div class="fighterName">
                    <h1>EV “ET”</h1><br>
                    <h3>Ting</h3>
                  </div>  
                  <div class="fighterDetails2">
                    <div class="col-md-12">
                      <div class="row">
                        <div class="col-md-6">
                          <div class="row">
                            <p>Nationality : Malaysia<br>
                            Age : 29<br>
                            Height : 5’ 7”<br>
                            Weight : 145 lbs<br>
                            Class : Lightweight</p>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="row">
                            <p>Wins: 13<br>
                            TKO/KO: 4 (30%)<br>
                            Submissions: 4 (31%)<br>
                            Decisions: 5 (38%)</p>
                          </div>  
                        </div>
                      </div>
                    </div>
                  </div>  
                </div>

                <div class="col-md-2">
                  <img class="img-responsive" src="images/onefc/nobatsu-profile-pic.jpg">
                </div>
                <div class="col-md-4">
                  <div class="fighterName">
                    <h1>Nobatsu </h1><br>
                    <h3>Suzuki</h3>
                  </div>  
                  <div class="fighterDetails2">
                    <div class="col-md-12">
                      <div class="row">
                        <div class="col-md-6">
                          <div class="row">
                            <p>Nationality : Japan<br>
                            Age : 40<br>
                            Height : 5’ 8”<br>
                            Weight : 155 lbs<br>
                            Class : Lightweight</p>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="row">
                            <p>Wins: 11<br>
                            TKO/KO: 10 (91%)<br>
                            Submissions: 0 (0%)<br>
                            Decisions: 1 (9%)</p>
                          </div>  
                        </div>
                      </div>
                    </div>
                  </div>  
                </div>
              </div>

              <div class="col-md-12 fighterCard2">
                <div class="col-md-2">
                  <img class="img-responsive" src="images/onefc/agilan-profile-pic.jpg">
                </div>
                <div class="col-md-4">
                  <div class="fighterName">
                    <h1>Agilan “Alligator”</h1><br>
                    <h3>Thani</h3>
                  </div>  
                  <div class="fighterDetails2">
                    <div class="col-md-12">
                      <div class="row">
                        <div class="col-md-6">
                          <div class="row">
                            <p>Nationality : Malaysia<br>
                            Age : 22<br>
                            Height : 5’ 8”<br>
                            Weight : 185 lbs<br>
                            Class : Welterweight</p>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="row">
                            <p>Wins: 7<br>
                            TKO/KO: 3 (43%)<br>
                            Submissions: 4 (57%)<br>
                            Decisions: 0 (0%)</p>
                          </div>  
                        </div>
                      </div>
                    </div>
                  </div>  
                </div>

                <div class="col-md-2">
                  <img class="img-responsive" src="images/onefc/sherif-profile-pic.jpg">
                </div>
                <div class="col-md-4">
                  <div class="fighterName">
                    <h1>Sherif “The Shark”</h1><br>
                    <h3>Mohamed</h3>
                  </div>  
                  <div class="fighterDetails2">
                    <div class="col-md-12">
                      <div class="row">
                        <div class="col-md-6">
                          <div class="row">
                            <p>Nationality : Egypt<br>
                            Age : N/A<br>
                            Height : 5’ 10”<br>
                            Weight : 185 lbs<br>
                            Class : Welterweight</p>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="row">
                            <p>TKO/KO: 5 (50%)<br>
                            Submissions: 5 (50%)<br>
                            Decisions: 0 (0%)</p>
                          </div>  
                        </div>
                      </div>
                    </div>
                  </div>  
                </div>
              </div>
          </div>
        </section>

        <section class="pageCategory-section last">
          <div class="container text-center">
            <h5 class="title">MAIN<span class="titleRed"> CARD</span></h5>
            <img class="img-responsive" src="images/onefc/maincard.jpg" style="display: block; margin: 0 auto;">
          </div>
        </section>

        <section class="pageCategory-section last">
          <div class="container text-center">
            <h5 class="title">PRELIM</h5>
            <img class="img-responsive" src="images/onefc/prelim.jpg" style="display: block; margin: 0 auto;">
          </div>
        </section>

         <!-- <section class="pageCategory-section last section-grey">
          <div class="container">
            <div class="gallery text-center"> -->
            <!-- <h1 class="subSecTitle"><strong>GALLERY</strong></h1>  -->
              <!-- Swiper -->
              <!-- <div class="swiper-container">
                <div class="swiper-wrapper">
                  <div class="swiper-slide">
                    <a href="images/onefc/onefc-poster.jpg" data-featherlight="image"><img class="" src="images/onefc/onefc-poster.jpg" alt=""></a>
                  </div>
                  <div class="swiper-slide">
                    <a href="images/onefc/onefc-mini-card.jpg" data-featherlight="image"><img class="" src="images/onefc/onefc-mini-card.jpg" alt=""></a>
                  </div>
                  <div class="swiper-slide">
                    <a href="images/onefc/AgilanThani.jpg" data-featherlight="image"><img class="" src="images/onefc/AgilanThani.jpg" alt=""></a>
                  </div>
                  <div class="swiper-slide">
                    <a href="images/onefc/ChristianLee.jpg" data-featherlight="image"><img class="" src="images/onefc/ChristianLee.jpg" alt=""></a>
                  </div>
                  <div class="swiper-slide">
                    <a href="images/onefc/MaratGafurov.jpg" data-featherlight="image"><img class="" src="images/onefc/MaratGafurov.jpg" alt=""></a>
                  </div>
                  <div class="swiper-slide">
                    <a href="images/onefc/EvTing.jpg" data-featherlight="image"><img class="" src="images/onefc/EvTing.jpg" alt=""></a>
                  </div>
                  <div class="swiper-slide">
                    <a href="images/onefc/AnnOsman.jpg" data-featherlight="image"><img class="" src="images/onefc/AnnOsman.jpg" alt=""></a>
                  </div>
                  <div class="swiper-slide">
                    <a href="images/onefc/Dejdamrong.jpg" data-featherlight="image"><img class="" src="images/onefc/Dejdamrong.jpg" alt=""></a>
                  </div>
                  <div class="swiper-slide">
                    <a href="images/onefc/KevinBelingon.jpg" data-featherlight="image"><img class="" src="images/onefc/KevinBelingon.jpg" alt=""></a>
                  </div>
                  <div class="swiper-slide">
                    <a href="images/onefc/KeanuSubba.jpg" data-featherlight="image"><img class="" src="images/onefc/KeanuSubba.jpg" alt=""></a>
                  </div>
                  <div class="swiper-slide">
                    <a href="images/onefc/GianniSubba.jpg" data-featherlight="image"><img class="" src="images/onefc/GianniSubba.jpg" alt=""></a>
                  </div>
                </div>
                
                <div class="swiper-pagination"></div>
                
                <div class="swiper-button-next swiper-button-white"></div>
                <div class="swiper-button-prev swiper-button-white"></div>
              </div>
            </div>
          </div>
        </section> -->

        <section class="pageCategory-section last"><a id="anchorPrice"></a>
          <div class="container tixPrice">
            <div class="row">
              <div class="col-sm-offset-1 col-sm-10 ">
                <div class="text-center">
                <h5 class="title">TICKET<span class="titleRed"> PRICE</span></h5>
                </div>

                <div class="table-responsive">
                  <table class="table infoTable-D table-bordered">
                      <thead>
                        <tr>
                          <th>Price Category</th>
                          <th>Normal Price</th>
                          <th>Remarks</th>
                          <th>Seating</th>
                        </tr>
                      </thead>
                      <tbody>
                          <tr>
                              <td><span class="box orange">Cageside Tickets &plus; After Party Package</span></td>
                              <td><strike>RM 2220.00</strike><br>(5&#37; discount)<br><br>RM 2120.00<br><strong class="text-danger">(Please select 5 tickets)</strong></td>
                              <td>5 x Cageside Tickets<br><br>5 entries to After Party &plus; one drink per pax (house pour / mixer)</td>
                              <td rowspan="11"><img class="img-responsive seatPlanImg" src="images/onefc/seatplan.jpg" alt=""></td>
                          </tr>
                          <tr>
                              <td><span class="box red">Platinum Tickets &plus; After Party Package</span></td>
                              <td><strike>RM 1070.00</strike><br>(40&#37; discount)<br><br>RM 650.00<br><strong class="text-danger">(Please select 5 tickets)</strong></td>
                              <td>5 x Platinum Tickets<br><br>5 entries to After Party &plus; one drink per pax (house pour / mixer)</td>
                          </tr>
                          <tr>
                              <td><span class="box orange">Cageside</span></td>
                              <td>RM 394.00</td>
                              <td rowspan="3">Numbered Seating</td>
                          </tr>
                          <tr>
                              <td><span class="box red">Platinum</span></td>
                              <td>RM 164.00<br><strong class="text-danger">(Buy 3 Free 2: Please select 5 tickets)</strong></td>
                          </tr>
                          <tr>
                              <td><span class="box blue">Gold</span></td>
                              <td>RM 84.00</td>
                          </tr>
                          <tr>
                              <td><span class="box green">Silver</span></td>
                              <td>RM 54.00</td>
                              <td rowspan="2">Free Seating</td>
                          </tr>
                          <tr>
                              <td><span class="box pink">Bronze</span></td>
                              <td>RM 29.00</td>
                          </tr>
                      </tbody>
                  </table>
                </div>
                <div class="buyAlert-bar">
                  <a class="btn btn-danger disabled" data-toggle="modal" data-target="#announcementModal">BUY TICKETS</a>                 
                  <!-- <a class="btn btn-danger disabled" id="buyButton" target="_blank" href="http://redtix-tickets.airasia.com/en-AU/shows/one fc - one- quest for greatness/events" role="button">BUY TICKETS</a>
                  <span class="or">/</span> -->
                  <span class="popData-btn" data-toggle="modal" data-target="#modalGetTixLoc">Online sales is closed. Tickets are still available at our authorized outlets or proceed to the ticket counter at the venue. <i class="fa fa-info-circle" aria-hidden="true"></i></span>
                </div>
                <div class="note text-left">
                    <p>Partner’s latest updates will be at <a target="_blank" href="http://www.onefc.com">http://www.onefc.com</a></p>
                    <p>Prices stated are inclusive RM4.00 ticketing fee.</p>
                    <h2>Important Notes</h2>
                    <ol>
                        <li>Price shown include RM4.00 AirAsiaRedTix fee.</li>
                        <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li>
                        <li>RM10.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                        <li>Strictly no replacement for missing tickets and cancellation.</li>
                        <li>Online ticket selling will close 2 days prior to event day, subject to availability.</li>
                        <li>Infant in arms or children below the age of 10 shall not be admitted.</li>
                        <li>Flash photography is not allowed.</li>
                    </ol>
                    <h2>For enquiry only:</h2>
                    <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                </div>
              </div>
            </div>
          </div>
        </section>

        {{-- <section class="pageCategory-section last">
          <div class="buyNowSec" style="background-image: url('images/event2.jpg')">
            <div class="bannerOverlay"></div>
            <div class="container">
              <div class="col-sm-offset-1 col-sm-10 quoteArea">
                <h1>You’ve heard him on the radio, you’ve seen him on TV. It’s about time. See him live!</h1>
              </div>
              <div class="btnArea">
                <a class="btn btn-black btn-lg" target="_blank" href="http://redtix-tickets.airasia.com/en-AU/shows/one-%20throne%20of%20tigers/events" role="button">GET TICKET ONLINE NOW <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                <div class="clearfix">&nbsp;</div>
                <span data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets</span>
              </div>
            </div>
          </div>
        </section> --}}

        {{-- <section class="pageCategory-section last">
          <div class="container">
            <div class="partner text-center">
                <h1 class="subSecTitle"><strong>PARTNERS</strong></h1>
                <div class="clearfix">&nbsp;</div>
                <ul class="list-unstyled list-inline partnerList">
                    <li><img src="images/neon/aabig_logo (offical partner).png" alt=""></li>
                    <li><img src="images/neon/BAC.png" alt=""></li>
                    <li><img src="images/neon/durex (official partner).png" alt=""></li>
                    <li><img src="images/neon/fac3 (co-organiser).jpg" alt=""></li>
                    <li><img src="images/neon/seven friday (official watch).jpeg" alt=""></li>
                    <li><img src="images/neon/sunway lagoon (official venue).png" alt=""></li>
                </ul>
            </div>
          </div>
        </section> --}}

      </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 450;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });
    </script>
    
@endsection

@section('modal')

    <!--Modal Seat Plan image-->
    <div class="modal fade" id="enlargeImageModal" tabindex="-1" role="dialog" aria-labelledby="enlargeImageModal" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
            <div class="clearfix">&nbsp;</div>
            <img src="" class="enlargeImageModalSource" style="width: 100%;">
            </div>
        </div>
        </div>
    </div>

    <!-- Modal Where to Get Tix Location-->
    <div id="modalGetTixLoc" class="modal fade" role="dialog">
        <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
            <h2>GET YOUR TICKETS FROM:</h2>
            <dl class="dl-horizontal">
                <dt>Online:</dt>
                <dd><a href="http://www.airasiaredtix.com">www.AirAsiaRedtix.com</a></dd>
            </dl>
            <dl class="dl-horizontal">
                <dt>Rock Corner outlets:</dt>
                <dd>The Gardens, Mid Valley (TEL: 03 - 2201 4893)</dd>
                <dd>Subang Parade (TEL: 03 - 5613 1139)</dd>
            </dl>
            <dl class="dl-horizontal">
                <dt>Victoria Music outlets:</dt>
                <dd>Sg Wang(TEL: 03 - 2148 7208)</dd>
                <dd>Amcorp (TEL: 03 - 7956 0592)</dd>
            </dl>
            <dl class="dl-horizontal">
                <dt>Penang outlets:</dt>
                <dd>Artist Gallery, Gurney Plaza (04-637 0191)</dd>
                <dd>Artist Gallery, Queensbay Mall (04-228 5648)</dd>
            </dl>
            <!-- <dl class="dl-horizontal">
                <dt>Istana Budaya Jalan<br>Tun Razak:</dt>
                <dd>03 40265558</dd>
            </dl> -->
            </div>
        </div>
        </div>
    </div>

@endsection