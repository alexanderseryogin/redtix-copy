{{-- @extends('master') --}}
@extends('masterforfemme')
@section('title')
    Femme Fatale 2018
@endsection

@section('header')
    @include('layouts.partials._header')
    <meta property="og:url" content="{{ Request::fullUrl() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Femme Fatale 2018" />
    <meta property="og:description" content="FEMME FATALE 2018. Featuring DJ: Jade Rasif, Alyshia, Yena, The Leng Sisters, Nikki @ Saturday, December 15, 2018 8:00 PM KL Live Kuala Lumpur"/>
    <meta property="og:image" content="{{ Request::Url().'images/femmefatale2018/thumbnail.jpg' }}" />
@endsection

@section('content')
    <!-- Banner Section -->
    <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/femmefatale2018/web-banner.jpg')}}" style="width: 100%" class="img-responsive" alt="Femme Fatale 2018"></div>
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            <img src="{{asset('images/femmefatale2018/thumbnail.jpg')}}" style="width: 100%" class="img-responsive" alt="Femme Fatale 2018">
        </div>
    </section>
    <!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
            <div class="row priceNbtn">
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="row">
                        <div class="col-sm-9 leftBox">
                            <h6>Femme Fatale 2018</h6> Tickets from <span>RM110</span>
                        </div>
                        <div class="col-sm-3 text-center">
                            <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar section-white">
            <section class="pageCategory-section last">
                <div class="container intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i>  15th December 2018</div>
                            <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i>  KL Live at Life Centre <a target="_blank" href="https://goo.gl/maps/bFzWTTNJfTS2">View Map</a></div>
                            <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> Saturday, 8.00pm </div>
                            <div class="clearfix">&nbsp;</div>
                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <div class="addthis_inline_share_toolbox"></div>
                            <!-- /sharing -->                            
                            <hr>
                            <div class="col-sm-12">
                                <p><h2>FEMME FATALE 2018</h2><br/>
                                <p>MEET ASIA’S HOTTEST FEMALE DJS in da house!!! Nothing is better than the year end party, let's take it to the NEXT LEVEL!!! JOIN the party 【 FEMME FATALE 2018 】, meet the hottest DJs from MALAYSIA, SINGAPORE, TAIWAN and KOREA!! The Leng Sisters (Leng Yein & Leng Sean), DJ Yena, DJ Jade Rasif, DJ Nikki, DJ Aly$hia.</p>
                            </div>
                            <div class="col-sm-12 embed-responsive embed-responsive-16by9">
                                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/JOaf4s70mxs?ecver=2" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- <div class="clearfix"></div> --}}
            </section>

            <section class="pageCategory-section last section-grey">
                <div class="container">
                    <div class="gallery text-center">
                        <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
                        <!-- Swiper -->
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <a href="images/femmefatale2018/gallery-1x.jpg" data-featherlight="image"><img class="" src="images/femmefatale2018/gallery-1.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/femmefatale2018/gallery-1x.jpg" data-featherlight="image"><img class="" src="images/femmefatale2018/web-banner.jpg" alt=""></a>
                                </div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next swiper-button-white"></div>
                            <div class="swiper-button-prev swiper-button-white"></div>
                        </div><!-- /Swiper -->
                    </div>
                </div>
            </section>

            <section class="pageCategory-section last"><a id="anchorPrice"></a>
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                            <div class="text-center">
                                <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                                <p>Select ticket</p>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="table-responsive">
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Category</th>
                                            <th>Ticket Price</th>
                                            <th>Remarks</th>
                                            <th>Seat Plan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Phase 1 - GA Standing</td>
                                            <td>RM110</td>
                                            <td>
                                                <ul style="text-align:left">
                                                    <li>1 GA Admission</li>
                                                    <li>Free standing at Ground floor</li>
                                                </ul>
                                            </td>
                                            <td rowspan="8"><img class="img-responsive seatPlanImg" src="images/femmefatale2018/seat-plan1.jpg" alt=""><br/><br/>Refer Image for Table Category & Number</td>
                                        </tr>
                                        <tr>
                                            <td>Phase 2 - GA Standing</td>
                                            <td style="color:#999999;">RM130</td>
                                            <td>Coming Soon</td>
                                        </tr>
                                        <tr>
                                            <td>Phase 3 - GA Standing</td>
                                            <td style="color:#999999;">RM150</td>
                                            <td>Coming Soon</td>
                                        </tr>
                                        <tr>
                                            <td>VIP - Standing</td>
                                            <td>RM168</td>
                                            <td>
                                                <ul style="text-align:left">
                                                    <li>1 VIP Admission to Ground and Second floor</li>
                                                    <li>2 free drinks to redeem</li>
                                                </ul>
                                             </td>
                                        </tr>
                                        <tr>
                                            <td>General Standing Table</td>
                                            <td>RM999</td>
                                            <td>
                                                <ul style="text-align:left">
                                                    <li>3 VIP Admission</li>
                                                    <li>1 Chivas or Absolut Bottle</li>
                                                    <li>One Round Standing Table</li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>VIP Table</td>
                                            <td>RM1,899</td>
                                            <td>
                                                <ul style="text-align:left">
                                                    <li>6 VIP Admission</li>
                                                    <li>1 Champagne Bottle</li>
                                                    <li>1 Chivas or Absolut</li>
                                                    <li>One Table Around The Dance Floor</li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>VVIP Table</td>
                                            <td>RM2,899</td>
                                            <td>
                                                <ul style="text-align:left">
                                                    <li>9 VIP Admission</li>
                                                    <li>1 Champagne Bottle</li>
                                                    <li>2 Chivas or Absolut Bottles</li>
                                                    <li>One Table Next To Dance Floor With Stage View</li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Platinum Table</td>
                                            <td>RM8,888</td>
                                            <td>
                                                <ul style="text-align:left">
                                                    <li>12 VIP Admission</li>
                                                    <li>1 Martell Bottle</li>
                                                    <li>3 Champagne Bottles</li>
                                                    <li>4 Chivas or Absolut Bottles</li>
                                                    <li>One Table On Stage Next To DJ</li>
                                                </ul>
                                            </td>
                                        </td>
                                    </tbody>
                                </table>
                            </div>
                            <div class="buyAlert-bar">
                                <a class="btn btn-danger" id="buyButton" datetime="Dec 16 2018 00:00:00 GMT+0800" target="_blank" href="https://redtix-tickets.airasia.com/en-AU/shows/femme fatale 2018/events" role="button">BUY TICKETS</a>
                                {{-- <span class="or">/</span>
                                <span class="popData-btn" data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets <i class="fa fa-info-circle" aria-hidden="true"></i></span> --}}
                            </div>
                            <div class="note text-left">
                                <h2>Important Notes</h2>
                                <ol>
                                    <li>Rules: NO means NO. Please be respectful at all times. Do not touch or grab any of the DJ's and crew. Failure of complying with these rules will lead to ejection from the event.</li>
                                    <li>Prices shown excludes RM4.00 AirAsiaRedTix fee.</li>
                                    {{-- <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li> --}}
                                    <li>RM10.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                                    <li>Online ticket selling will close a day prior to event day, subject to availability.</li>
                                </ol>
                                <h2>For enquiry only:</h2>
                                <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 350;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });
    </script>

    {{-- Buy button disable --}}
    <script type="text/javascript">
        $(function() {
            $('a[id^=buyButton]').each(function() {
                var date = new Date();
                var enddate = $(this).attr('datetime'); 
                if ( Date.parse(date) >= Date.parse(enddate)) {
                  $(this).addClass('disabled');
                }
            });
        });
    </script>

@endsection

@section('modal')
    @include('layouts.partials.modals._seatplan')
    @include('layouts.partials.modals._getTix')
@endsection