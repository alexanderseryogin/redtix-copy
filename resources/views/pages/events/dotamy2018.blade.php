@extends('master')
@section('title')
    The Kuala Lumpur Major
@endsection

@section('header')
    @include('layouts.partials._header')
    <meta property="og:url" content="{{ Request::fullUrl() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="The Kuala Lumpur Major" />
    <meta property="og:description" content="The Kuala Lumpur Major is the biggest esports championship in Malaysia and the first ever Dota 2 Pro Circuit Major happening in Southeast Asia, jointly presented by PGL, eGG Network and Imba TV supported by the Ministry of Youth and Sports Malaysia, National Sports Council Malaysia and Esports Malaysia."/>
    <meta property="og:image" content="{{ Request::Url().'images/dotamy2018/thumbnail.jpg' }}" />
@endsection

@section('content')
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:600|Roboto" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
    <link type="text/css" rel="stylesheet" href="css/custom2.css">
    <link type="text/css" rel="stylesheet" href="css/clock.css">
    <link type="text/css" rel="stylesheet" href="/css/custom/redtix.css">
    <link type="text/css" rel="stylesheet" href="css/timeTo.css">

    <!-- Banner Section -->
    <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/dotamy2018/web-banner.jpg')}}" style="width: 100%" class="img-responsive" alt="The Kuala Lumpur Major"></div>
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            <img src="{{asset('images/dotamy2018/thumbnail.jpg')}}" style="width: 100%" class="img-responsive" alt="The Kuala Lumpur Major">
        </div>
    </section>
    <!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
            <div class="row priceNbtn">
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="row">
                        <div class="col-sm-9 leftBox">
                            <h6>The Kuala Lumpur Major</h6> Tickets from <span>RM716</span>
                        </div>
                        <div class="col-sm-3 text-center">
                            <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar section-white">
            <section class="pageCategory-section last">
                <div class="container intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i>  16th - 18th November 2018</div>
                            <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i>  Axiata Arena, KL Sports City, Bukit Jalil, Kuala Lumpur, Malaysia <a target="_blank" href="https://goo.gl/maps/bR4Nqz34TcA2">View Map</a></div>
                            <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> 11.00am - 9.00pm </div>
                            <div class="clearfix">&nbsp;</div>
                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <div class="addthis_inline_share_toolbox"></div>
                            <!-- /sharing -->
                            <hr>
                            <div class="col-sm-12">
                                <p><h2>The Kuala Lumpur Major</h2></p>
                                <p>The Kuala Lumpur Major is the biggest esports championship in Malaysia and the first ever Dota 2 Pro Circuit Major happening in Southeast Asia, jointly presented by PGL, eGG Network and Imba TV supported by the Ministry of Youth and Sports Malaysia, National Sports Council Malaysia and Esports Malaysia. 16 best Dota 2 teams with over 80 top e-athletes from around the world will compete in Kuala Lumpur for US$1,000,000 dollars prize money and invaluable DPC points in their quest to proof themselves as the world’s best in Dota 2 en route to The International 2019 in Shanghai.</p>
                                <p>The Kuala Lumpur Major main event will be held at the largest indoor stadium in Kuala Lumpur, the capital city of Malaysia. The Axiata Arena has hosted a myriad of international events including the Commonwealth Games, SEA Games, BWF World Championship and concerts of various international artists. The location is well connected with the city’s Light Railway Transit, public buses, taxis, ride hailing service and ample parking space is available for those who drives.</p>
                                <p>Fans can expect 3 days of exhilarating world class Dota 2 action, lots of fun activities and opportunities to meet their favorite players and personalities from 16 – 18 November at Axiata Arena. Tickets are available in 3 categories of Ancient, Divine and Immortal with prices starting from MYR180. For more information, please visit www.thekualalumpurmajor.com</p>
                                <p>See you in KL!  #kualalumpurmajor</p>
                            </div>
                        </div>
                        <div class="col-sm-offset-1 col-sm-10 img-wrap text-center"> 
                            <img src="{{asset('images/dotamy2018/layout.jpg')}}" style="width: 100%;" class="img-responsive" alt="The Kuala Lumpur Major">
                        </div>
                    </div>
                </div>
                {{-- <div class="clearfix"></div> --}}
            </section>

            {{-- <section class="pageCategory-section last section-grey">
                <div class="container">
                    <div class="gallery text-center">
                        <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
                        <!-- Swiper -->
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <a href="images/dotamy2018/gallery-1.jpg" data-featherlight="image"><img class="" src="images/dotamy2018/gallery-1.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/dotamy2018/gallery-2.jpg" data-featherlight="image"><img class="" src="images/dotamy2018/gallery-2.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/dotamy2018/gallery-3.jpg" data-featherlight="image"><img class="" src="images/dotamy2018/gallery-3.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/dotamy2018/gallery-4.jpg" data-featherlight="image"><img class="" src="images/dotamy2018/gallery-4.jpg" alt=""></a>
                                </div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next swiper-button-white"></div>
                            <div class="swiper-button-prev swiper-button-white"></div>
                        </div><!-- /Swiper -->
                    </div>
                </div>
            </section> --}}

            <section class="pageCategory-section last"><a id="anchorPrice"></a>
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                            {{-- <div class="text-center">
                                <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                                <p>Select ticket</p>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="table-responsive">
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Category</th>
                                            <th>Ticket Price</th>
                                            <th>MY KAD Price</th>
                                            <th>Remarks</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>First Release: 2 Day Pass</td>
                                            <td>AUD 319 / RM 970.00</td>
                                            <td>RM 930.00</td>
                                            <td>-</td>
                                        </tr>
                                        <tr>
                                            <td>5 Night Stay for 2 People - Bundle A<br/>Kuala Lumpur - Perth</td>
                                            <td>RM 7,400.00</td>
                                            <td>-</td>
                                            <td>Package includes: 2 Return flights on AirAsia, 5 Night stay at Duxton Hotel (5*) & (2) 2 Day Passes at Origin Fields</td>
                                        </tr>
                                        <tr>
                                            <td>5 Night Stay for 2 People - Bundle B<br/>Kuala Lumpur - Perth</td>
                                            <td>RM 6,220.00</td>
                                            <td>-</td>
                                            <td>Package includes: 2 Return flights on AirAsia, 5 Night stay at IbisHotel (3*) & (2) 2 Day Passes at Origin Fields</td>
                                        </tr>
                                        <tr>
                                            <td>5 Night Stay for 2 People - Bundle C<br/>Bali - Perth</td>
                                            <td>RM 8,032.00</td>
                                            <td>-</td>
                                            <td>Package includes: 2 Return flights on AirAsia, 5 Night stay at Duxton Hotel (5*) & (2) 2 Day Passes at Origin Fields</td>
                                        </tr>
                                        <tr>
                                            <td>5 Night Stay for 2 People - Bundle D<br/>Bali - Perth</td>
                                            <td>RM 7,161.00</td>
                                            <td>-</td>
                                            <td>Package includes: 2 Return flights on AirAsia, 5 Night stay at Ibis Hotel (3*) & (2) 2 Day Passes at Origin Fields</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="buyAlert-bar">
                                <a class="btn btn-danger" id="buyButton" datetime="Dec 29 2018 00:00:00 GMT+0800" target="_blank" href="https://redtix-tickets.airasia.com/en-AU/shows/origin fields (30th - 31st december 2018)/events" role="button">BUY TICKETS</a>
                                <span class="or">/</span>
                                <span class="popData-btn" data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets <i class="fa fa-info-circle" aria-hidden="true"></i></span>
                            </div> --}}

                            @include('layouts.partials._showtickets')

                            <div class="note text-left">
                                <h2>Important Notes</h2>
                                <ol>
                                    {{-- <li>Prices shown exclude RM4.00 AirAsiaRedTix fee and 3% Credit card fee.</li>
                                    <li>All ticket prices will be paid in Malaysian Ringgit. Australian Dollar prices are shown for reference purposes only.</li>
                                    <li>Prices quoted in AUD may differ due to currency fluctuations.</li>
                                    <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li> --}}
                                    <li>RM10.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                                    <li>Online ticket selling will close a day prior to event day, subject to availability.</li>
                                    <li>Flight times may vary due to availability</li>
                                </ol>
                                <h2>For enquiry only:</h2>
                                <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 350;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });
    </script>

    {{-- Buy button disable --}}
    <script type="text/javascript">
        $(function() {
            $('a[id^=buyButton]').each(function() {
                var date = new Date();
                var enddate = $(this).attr('datetime'); 
                if ( Date.parse(date) >= Date.parse(enddate)) {
                  $(this).addClass('disabled');
                }
            });
        });
    </script>

@endsection

@section('modal')
    @include('layouts.partials.modals._seatplan')
    @include('layouts.partials.modals._getTix')
    @include('layouts.partials.modals._moreInfoNoFee')
@endsection
