@extends('master')
@section('title')
	 UFC 221
@endsection

@section('header')
	@include('layouts.partials._header')
	
@endsection

@section('content')
<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:600|Roboto" rel="stylesheet">
<link type="text/css" rel="stylesheet" href="css/custom2.css">
<link type="text/css" rel="stylesheet" href="css/clock.css">
<style type="text/css">
  .fighterDetails2 {
	  text-align: left;
  }
  .fighterCard {
	overflow: hidden;
  }
  div.fighterDetails * {
	padding-left: 0px;
  }
  .social-media {
	padding-top: 10px;
	padding-left: 15px !important;
  }
  .social-media a{
	color: #fff;
  }
  .section-white *{
	color: #333;
  }
  .section-grey {
	color: #333;
  }
  .fighterCap {
	font-family: Lato;
	font-weight: 100;
  }
  .font-Lato {
	font-family: Lato;
	font-weight: 100;
  }
  .ptb-50 {
	padding-top: 50px;
	padding-bottom: 50px;
  }
  .font-openSans {
	font-family: 'Open Sans', sans-serif;
  }
  .font-openSansRoboto {
	font-family: 'Open Sans', sans-serif;
	font-family: 'Roboto', sans-serif;
	color: #616161;
  }
  .background-radial {
	background-image: radial-gradient(circle at 48% 40%, #0e184a, #121015);
  }
   .background-radial2 {
	background-image: radial-gradient(circle farthest-side at bottom right, #0e184a, #121015);
  }
  .background-arena1 {
	width: 90%;
	height: 100%;
	display: block;
	position: absolute;
	top: 124px;
	left: 114px;
	opacity: 0.2;
  }
  .background-arena1::after {
	content: "";
	background-image: url(images/ufc2018/arena.png);
	opacity: 0.5;
	top: 0;
	left: 0;
	bottom: 0;
	right: 0;
	position: absolute;
	
  }
  section.pageContent .intro {
	padding-bottom: 277px;
	overflow: hidden;
  }@media only screen and (max-device-width:1080px) {
	section.pageContent .intro {
	  padding-bottom: 10px;
	}
  }
  #arenaPlan {
  	padding-top: 60px;
	padding-bottom: 30px;
  }
  span.subTitle {
	font-size: 11px;
	font-weight: 600;
	color: #8f919d;
  }
  @media only screen and (max-device-width:1080px) {
	h1,h3 {
		font-size: 20px; 
	}
  }
  @media screen and (max-width: 768px){
	.fighterDetails {
	  /*text-align: center;*/
	}
	/*div.fighterDetails * {
	  padding-left: 15px;
	}*/
	div.fighterDetails a {
	  padding-left: 0px;
	}
	
  }
  .arena {
	 object-fit: contain;
	 position: absolute;
	 opacity: 0.1;
	 width: 100%;
 }
 .past-photos * {
	padding-right: 0px; 
	padding-left: 0px;
 }

 @media screen and (max-width: 768px){
	.fighterName h1{
	  font-size: 30px;
	}
  }
  .ticket-card {
	background-color: #ffffff;
	height: 100%;
	padding-right: 5px;
	padding-left: 5px;
	width: 200px;
	margin-top: 20px;
	font-family: 'Open Sans', sans-serif;
	/*margin-right: 20px;*/
  } 
  .ticket-card p { 
  	color: #333;
  }

  .ticket-card #head {
	  padding-top: 10px;
	  padding-bottom: 20px;
  } 
  .ticket-card #head .middle {
	transition: .5s ease;
	opacity: 0;
	position: absolute;
	top: 50%;
	left: 50%;
	transform: translate(-50%, -50%);
	-ms-transform: translate(-50%, -50%);
	text-align: center;
	} 
	.ticket-card #head .middle .text {
	font-family: 'Open Sans', sans-serif;
	font-size: 20px;
	font-weight: bold;
	text-align: center;
	color: #fff;
	}
	.ticket-card #head .middle .text-grey {
	font-family: 'Open Sans', sans-serif;
	font-size: 20px;
	font-weight: bold;
	text-align: center;
	color: #3b3b3b;
	}
	.ticket-card #head .middle .text-red {
	font-family: 'Open Sans', sans-serif;
	font-size: 10px;
	text-align: center;
	color: #fff;
  }
  .ticket-card #head img{
	width:100%;
	padding-top: 10px;
	padding-bottom: 30px;
	transition: .5s ease;
	backface-visibility: hidden;
	opacity: 1;
	display: block;
  }

 .ticket-card .price {
	text-align: center;
	font-size: 20.5px;
	font-weight: bold;
	color: #333333;
 }
 .image {
	  width: 100%;
	  height: 63px;
	  position:relative;
  }

  .ticket-card #head .image > .overlay {
	  width:100%;
	  height:100%;
	  position:absolute;
	  background-color: #b30000;
	  /* opacity:0.5; */
  }

	.ticket-card #head .image > .overlay-soldout {
	  width:100%;
	  height:100%;
	  position:absolute;
	  background-color: #999999;
	  /* opacity:0.5; */
	}
	
  .ticket-card #head .image .middle {
	  opacity: 1;
  }

  @media only screen and (min-device-width:1080px) {
	#getTicket {
	  padding-bottom: 150px;
	}
	.note {
	  margin-top: 90px;
	}
  }
  .datesFlight {
	font-size: 13px;
	font-weight: 600;
	text-align: center;
	color: #333333;
  }
  .scheduleFlight {
	font-size: 11px;
	padding-bottom: 20px;
  }
  .titleOpenSans {
  	font-family: 'Open Sans', sans-serif;
  	font-size: 35px;
	font-weight: bold;
	text-align: center;
	color: #ffffff;
  }

	.reminder {
		font-size: 16px;
	}
	.smallReminder {
		font-style: italic;
		font-size: 11.5px;
	}
</style>


	<!-- Banner Section -->
	 <section class="innerPageBanner" style="width: 100%">
		<div class="bigBanner-overlay"></div>
		<a href="#anchorPrice"><div class="jumbotron eventBanner hidden-xs" style="background-image: url('images/ufc2018/web-banner.jpg')"></div></a>
		<div class="widewrapper main hidden-lg hidden-md hidden-sm">
			<img src="{{asset('images/ufc2018/thumbnail.png')}}" style="width: 100%" class="img-responsive" alt="ONE: QUEST FOR GREATNESS">
		</div>
	</section>
	<!-- Content Section -->
	<section class="pageContent">
	  <!-- Main Body -->
		<div class="mainBodyContent no-btm-mar" style="padding-top: 0px;">

			<section class="pageCategory-section last section-black background-radial" style="padding-top: 50px;">
				{{-- <div class="container">
					<div id="clockdiv" class="col-sm-12 col-xs-12">
						<div><span class="days"></span><div class="smalltext">Days</div></div>
						<div><span class="hours"></span><div class="smalltext">Hours</div></div>
						<div><span class="minutes"></span><div class="smalltext">Minutes</div></div>
						<div><span class="seconds"></span><div class="smalltext">Seconds</div></div>
					</div>
				</div> --}}
				<div class="container intro">
					<div class="row"> 
						<h5 class="title">ABOUT<span class="titleRed"> EVENT</span></h5>
						<div class="col-md-12 col-xs-12 font-Lato"> 
							<img src="images/ufc2018/arena.png" class="arena hidden-xs">
							<div class="col-md-6 col-xs-12">
								<p>The interim UFC middleweight championship is on the line as former titleholder Luke Rockhold battles number one contender Yoel Romero in the main event of UFC 221.</p>
								<p>Luke Rockhold, one of the premier middleweight of this era, has held both the UFC and Strikeforce titles. At 185 pounds, he has defeated the best the division has to offer. Chris Weidman, Michael Bisping, Jacare Souza, Tim Kennedy and Lyoto Machida have all suffered defeat at his hands. And at the UFC 221, the dynamic Californian looks to start his 2018 campaign by regaining his title.</p>
							</div>
							<div class="col-md-6 col-xs-12">
								<p>But in his way will be one of the most explosive athletes to ever step into the Octagon. Cuba’s Yoel Romero, a former Olympic Silver medalist in wrestling, has dynamite in his fists, feet and knees and also an array of highlight victories to his name as he looks to strike UFC gold. </p>
								<p>Plus, in the co-main event, UFC legend Mark Hunt returns to the Octagon to take on surging heavyweight Curtis Blaydes, who is unbeaten in the last four fights.</p>
							</div>
						</div>
					</div>
				</div>
			</section>
			{{--   <section class="pageCategory-section section-black last">
			<div class="container">
			<h5 class="title">VIDEO<span class="titleRed"> HIGHLIGHT</span></h5>
			</div>
			</section> --}}
			<section class="pageCategory-section section-black last">
				<div class="container">
					<h5 class="title">PAST<span class="titleRed"> PHOTOS</span></h5>
					<div class="row center-block past-photos hidden-xs">
						<div class="col-sm-12">
							<div class="col-sm-3">
								<a href="images/ufc2018/past1.jpg" data-featherlight="image"><img class="img-responsive" src="images/ufc2018/past1.jpg" /></a>
							</div>
							<div class="col-sm-3">
								<a href="images/ufc2018/past2.jpg" data-featherlight="image"><img class="img-responsive" src="images/ufc2018/past2.jpg" /></a>
							</div>
							<div class="col-sm-3">
								<a href="images/ufc2018/past3.jpg" data-featherlight="image"><img class="img-responsive" src="images/ufc2018/past3.jpg" /></a>
							</div>
							<div class="col-sm-3">
								<a href="images/ufc2018/past4.jpg" data-featherlight="image"><img class="img-responsive" src="images/ufc2018/past4.jpg" /></a>
							</div>
						</div>
						<div class="col-sm-12">
							<div class="col-sm-3">
								<a href="images/ufc2018/past5.jpg" data-featherlight="image"><img class="img-responsive" src="images/ufc2018/past5.jpg" /></a>
							</div>
							<div class="col-sm-3">
								<a href="images/ufc2018/past6.jpg" data-featherlight="image"><img class="img-responsive" src="images/ufc2018/past6.jpg" /></a>
							</div>
							<div class="col-sm-3">
								<a href="images/ufc2018/past7.jpg" data-featherlight="image"><img class="img-responsive" src="images/ufc2018/past7.jpg" /></a>
							</div>
							<div class="col-sm-3">
								<a href="images/ufc2018/past8.jpg" data-featherlight="image"><img class="img-responsive" src="images/ufc2018/past8.jpg" /></a>
							</div>
						</div>
					</div>

					<div class="gallery text-center hidden-lg hidden-md hidden-sm">
						<div class="swiper-container">
							<div class="swiper-wrapper">
								<div class="swiper-slide">
								  	<a href="images/ufc2018/past1.jpg" data-featherlight="image"><img class="img-responsive" src="images/ufc2018/past1.jpg" /></a>
								</div>
								<div class="swiper-slide">
								  	<a href="images/ufc2018/past2.jpg" data-featherlight="image"><img class="img-responsive" src="images/ufc2018/past2.jpg" /></a>
								</div>
								<div class="swiper-slide">
								  	<a href="images/ufc2018/past3.jpg" data-featherlight="image"><img class="img-responsive" src="images/ufc2018/past3.jpg" /></a>
								</div>
								<div class="swiper-slide">
								  	<a href="images/ufc2018/past4.jpg" data-featherlight="image"><img class="img-responsive" src="images/ufc2018/past4.jpg" /></a>
								</div>
								<div class="swiper-slide">
								  <a href="images/ufc2018/past5.jpg" data-featherlight="image"><img class="img-responsive" src="images/ufc2018/past5.jpg" /></a>
								</div>
								<div class="swiper-slide">
								  <a href="images/ufc2018/past6.jpg" data-featherlight="image"><img class="img-responsive" src="images/ufc2018/past6.jpg" /></a>
								</div>
								<div class="swiper-slide">
								  <a href="images/ufc2018/past7.jpg" data-featherlight="image"><img class="img-responsive" src="images/ufc2018/past7.jpg" /></a>
								</div>
								<div class="swiper-slide">
								  <a href="images/ufc2018/past8.jpg" data-featherlight="image"><img class="img-responsive" src="images/ufc2018/past8.jpg" /></a>
								</div>
							</div>
							<div class="swiper-pagination"></div>
							<div class="swiper-button-next swiper-button-white"></div>
							<div class="swiper-button-prev swiper-button-white"></div>
						</div>
					</div>
				</div>
			</section>
			<section class="pageCategory-section section-black last">
				<div class="container">
					<h5 class="title">ABOUT THE<span class="titleRed"> ATHLETES</span></h5>
					<!-- fighter 1 -->
					<div class="col-md-12 fighterCard background-radial2">
						<div class="background-arena1 hidden-xs"></div>
						<div class="col-sm-6">
							<img class="img-responsive" src="images/ufc2018/athlete-1-2.png">
						</div>
						<div class="col-sm-6">
							<div class="fighterName">
								<h1>Yoel Romero</h1><br>
								<h6>Soldier of God</h6>
							</div> 
							<div class="fighterCap">
								<p>Yoel Romero Palacio (born April 3, 1977) is a Cuban mixed martial artist (MMA), a former amateur wrestling World Champion, and an Olympic silver medalist in freestyle wrestling at the 2000 Summer Olympics. He competes in the Middleweight division of the Ultimate Fighting Championship, and formerly competed in the now-defunct Strikeforce organization.</p>
							</div>  
							<div class="fighterDetails">
								<div class="col-md-12 col-xs-12">
									<div class="col-md-6">
										<p>{{-- Nationality : Russia<br> --}}
											Age : 40<br>
											Height: 6' 0" ( 182 cm )<br>
											Reach: 73" <br>
											Weight : 145 lbs<br>
										</p>
									</div>
									<div class="col-md-6">
										<p>
											Wins: 12<br>
										  	Losses: 2<br>
										  	Draw: 0<br>
										  	Summary: Wrestling, BJJ 
										</p>
									</div>
								</div>
								<a target="_blank" href="http://www.ufc.com/fighter/Yoel-Romero">http://www.ufc.com/fighter/Yoel-Romero</a>
								<div class="col-md-6 col-xs-12 social-media">
									<ul class="list-inline">
										<li><a href="https://www.facebook.com/yoelromeromma/" class="fa fa-facebook" target="_blank"></a></li>
										<li><a href="https://twitter.com/YoelRomeroMMA?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor" class="fa fa-twitter" target="_blank"></a></li>
										<li><a href=" https://www.instagram.com/yoelromeromma/?hl=en" class="fa fa-instagram" target="_blank"></a></li>
										<li><a href="#" class="fa fa-youtube" target="_blank"></a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<!-- ends -->
					<div class="row center-block past-photos hidden-xs">
						<div class="col-sm-12">
							<div class="col-sm-3">
								<a href="images/ufc2018/collage1-1.jpg" data-featherlight="image"><img class="img-responsive" src="images/ufc2018/collage1-1.jpg" /></a>
							</div>
							<div class="col-sm-3">
								<a href="images/ufc2018/collage1-2.jpg" data-featherlight="image"><img class="img-responsive" src="images/ufc2018/collage1-2.jpg" /></a>
							</div>
							<div class="col-sm-3">
								<a href="images/ufc2018/collage1-3.jpg" data-featherlight="image"><img class="img-responsive" src="images/ufc2018/collage1-3.jpg" /></a>
							</div>
							<div class="col-sm-3">
								<a href="images/ufc2018/collage1-4.jpg" data-featherlight="image"><img class="img-responsive" src="images/ufc2018/collage1-4.jpg" /></a>
							</div>
						</div>
					</div>
					<div class="gallery text-center hidden-lg hidden-md hidden-sm">
						<div class="swiper-container">
							<div class="swiper-wrapper">
								<div class="swiper-slide">
								  	<a href="images/ufc2018/collage1-1.jpg" data-featherlight="image"><img class="img-responsive" src="images/ufc2018/collage1-1.jpg" /></a>
								</div>
								<div class="swiper-slide">
								  	<a href="images/ufc2018/collage1-2.jpg" data-featherlight="image"><img class="img-responsive" src="images/ufc2018/collage1-2.jpg" /></a>
								</div>
								<div class="swiper-slide">
								  	<a href="images/ufc2018/collage1-3.jpg" data-featherlight="image"><img class="img-responsive" src="images/ufc2018/collage1-3.jpg" /></a>
								</div>
								<div class="swiper-slide">
								  	<a href="images/ufc2018/collage1-4.jpg" data-featherlight="image"><img class="img-responsive" src="images/ufc2018/collage1-4.jpg" /></a>
								</div>
							</div>
							<div class="swiper-pagination"></div>
							<div class="swiper-button-next swiper-button-white"></div>
							<div class="swiper-button-prev swiper-button-white"></div>
						</div>
					</div>
					<!-- fighter 2 -->
					<div class="col-md-12 fighterCard">
						<div class="col-sm-6 col-sm-push-6 col-xs-12">
							<img class="img-responsive" src="images/ufc2018/athlete-2-2.png" >
						</div>
						<div class="col-sm-6 col-sm-pull-6 col-xs-12">
							<div class="fighterName">
								<h1>Luke Rockhold</h1><br>
							</div>  
							<div class="fighterCap">
								<p>Luke Skyler Rockhold[4] (born October 17, 1984) is an American mixed martial artist competing in the UFC, where he is the former UFC Middleweight Champion.[5] He won the title on December 12, 2015 by defeating prior champion, the undefeated Chris Weidman via 4th round TKO. A two-time world champion, Rockhold also won the Strikeforce Middleweight Championship by defeating Ronaldo Souza on September 10, 2011 via unanimous decision. Rockhold defended the title twice and was the last man to hold the belt, before Strikeforce was officially taken over by the UFC. He is currently ranked the #2 middleweight and #12 official pound-for-pound fighter by the UFC and other publications like Fight Matrix and Sherdog. Luke is also an avid surfer and skateboarder.</p>
							</div>  
							<div class="fighterDetails">
								<div class="col-md-12 col-xs-12">
									<div class="col-md-6">
										<p>{{-- Nationality : Australia<br> --}}
											Age: 33<br>
											Height: 6' 3" ( 190 cm )<br>
											Reach: 77" <br>
											Weight: 185 lb ( 84 kg ) 
										</p>
									</div>
									<div class="col-md-6">
										<p>
											Wins: 16<br>
											Losses: 3<br>
											Draw: 0<br>
											Summary: Kickboxing, jiu jitsu, <br>wrestling
										</p>
									</div>
								</div>
								<a target="_blank" href="http://www.ufc.com/fighter/Luke-Rockhold">http://www.ufc.com/fighter/Luke-Rockhold</a>
								<div class="col-md-6 col-xs-12 social-media">
									<ul class="list-inline">
										<li><a href="https://www.facebook.com/rockholdmma/" class="fa fa-facebook" target="_blank"></a></li>
										<li><a href="https://twitter.com/LukeRockhold?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor" class="fa fa-twitter" target="_blank"></a></li>
										<li><a href="https://www.instagram.com/lukerockhold/?hl=en" class="fa fa-instagram" target="_blank"></a></li>
										<li><a href="#" class="fa fa-youtube" target="_blank"></a></li>
									</ul>
								</div>
							</div>
						</div>
					</div> 

					<div class="row center-block past-photos hidden-xs">
						<div class="col-sm-12">
							<div class="col-sm-3">
								<a href="images/ufc2018/collage2-1.jpg" data-featherlight="image"><img class="img-responsive" src="images/ufc2018/collage2-1.jpg" /></a>
							</div>
							<div class="col-sm-3">
								<a href="images/ufc2018/collage2-2.jpg" data-featherlight="image"><img class="img-responsive" src="images/ufc2018/collage2-2.jpg" /></a>
							</div>
							<div class="col-sm-3">
								<a href="images/ufc2018/collage2-3.jpg" data-featherlight="image"><img class="img-responsive" src="images/ufc2018/collage2-3.jpg" /></a>
							</div>
							<div class="col-sm-3">
								<a href="images/ufc2018/collage2-4.jpg" data-featherlight="image"><img class="img-responsive" src="images/ufc2018/collage2-4.jpg" /></a>
							</div>
						</div>
					</div>
					<!-- ends -->
					<div class="gallery text-center hidden-lg hidden-md hidden-sm">
						<div class="swiper-container">
							<div class="swiper-wrapper">
								<div class="swiper-slide">
								  	<a href="images/ufc2018/collage2-1.jpg" data-featherlight="image"><img class="img-responsive" src="images/ufc2018/collage2-1.jpg" /></a>
								</div>
								<div class="swiper-slide">
								  	<a href="images/ufc2018/collage2-2.jpg" data-featherlight="image"><img class="img-responsive" src="images/ufc2018/collage2-2.jpg" /></a>
								</div>
								<div class="swiper-slide">
								  	<a href="images/ufc2018/collage2-3.jpg" data-featherlight="image"><img class="img-responsive" src="images/ufc2018/collage2-3.jpg" /></a>
								</div>
								<div class="swiper-slide">
								  	<a href="images/ufc2018/collage2-4.jpg" data-featherlight="image"><img class="img-responsive" src="images/ufc2018/collage2-4.jpg" /></a>
								</div>
							</div>
							<div class="swiper-pagination"></div>
							<div class="swiper-button-next swiper-button-white"></div>
							<div class="swiper-button-prev swiper-button-white"></div>
						</div>
					</div>
					<div class="row text-center" style="padding-top: 40px;">
						<a class="btn btn-danger" href="#anchorPrice" role="button">Get Tickets</a>
					</div>
				</div>
			</section>
			<section class="pageCategory-section section-black last" id="arenaPlan">
				<div class="container text-center">
					<h5 class="titleOpenSans">SEATING<span class="titleRed"> PLAN</span></h5>
					<a href="images/ufc2018/arena-map1.png" data-featherlight="image"><img class="img-responsive" src="images/ufc2018/arena-map1.png" alt="Perth Arena"></a>
				</div>
			</section>
			<section class="pageCategory-section section-black last ptb-50" id="anchorPrice">
				<div class="container text-center">
					<h5 class="titleOpenSans">GET<span class="titleRed"> TICKETS</span></h5>
					<span class="subTitle openSansRoboto"><p>Prices below include 1 x UFC 221 ticket (Perth) and 1 x Return Flight Ticket to Perth.</p></span>
					<div class="row" style="padding-bottom: 50px;">
						<div class="col-sm-offset-1 col-sm-10 ">
							<div class="col-sm-12 col-xs-12" style="margin-top: 20px; margin-bottom: 40px;">
								<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
									<div class="ticket-card text-center center-block">
										<div id="head">
<!--											<div class="image" style="background: url('images/ufc2018/tix-1.jpg'); background-size: cover;"> -->
												<div class="image">
												<div class="overlay-soldout"></div>
												<div class="middle">
													<div class="text-red"><strike>RM3,960.00</strike></div>
													<div class="text-grey">RM3,260.00</div>
												</div>
											</div>
										</div>
<!--										<div class="price font-openSansRoboto"><strike style="color:#999999;">RM3,260.00</strike></div> -->
										<p>P1 Ticket + Return Flight<br>tickets from<br>Kuala Lumpur</p>
										<p style="color:red;font-size:14px;">SOLD OUT</p>
										<div class="datesFlight">Flight Dates Available:</div>
										<div class="scheduleFlight">Depart (Kuala Lumpur - Perth):<br>Fri, 09 Feb 00:05<br>Sat, 10 Feb 00:05<br><br>Return (Perth - Kuala Lumpur):<br>Mon, 12 Feb 06:05</div>
									</div>
								</div>
								<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
								  	<div class="ticket-card text-center center-block">
										<div id="head">
<!--											<div class="image" style="background: url('images/ufc2018/tix-2.jpg'); background-size: cover;"> -->
												<div class="image">
												<div class="overlay"></div>
												<div class="middle">
												<div class="text-red"><strike>RM3,250.00</strike></div>
													<div class="text">RM2,550.00</div>
												</div>
										  	</div>
										</div>
<!--										<div class="price font-openSansRoboto">‪RM‬2,550.00</div> -->
										<p>P2 Ticket + Return Flight<br>tickets from<br>Kuala Lumpur</p>
										<p style="color:#999999;font-size:10px;">&nbsp;</p>
										
										<div class="datesFlight">Flight Dates Available:</div>
										<div class="scheduleFlight">Depart (Kuala Lumpur - Perth):<br>Fri, 09 Feb 00:05<br>Sat, 10 Feb 00:05<br><br>Return (Perth - Kuala Lumpur):<br>Mon, 12 Feb 06:05</div>
								  	</div>
								</div>
								<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
								  	<div class="ticket-card text-center center-block">
										<div id="head">
<!--										  <div class="image" style="background: url('images/ufc2018/tix-3.jpg'); background-size: cover;"> -->
													<div class="image">
											  	<div class="overlay"></div>
											  	<div class="middle">
													<div class="text-red"><strike>RM2,890.00</strike></div>
													<div class="text">RM2,190.00</div>
											  	</div>
										  </div>
										</div>
<!--										<div class="price font-openSansRoboto">RM‬2,190.00</div> -->
										<p>P3 Ticket + Return Flight<br>tickets from<br>Kuala Lumpur</p>
										<p style="color:#999999;font-size:10px;">&nbsp;</p>
										<div class="datesFlight">Flight Dates Available:</div>
										<div class="scheduleFlight">Depart (Kuala Lumpur - Perth):<br>Fri, 09 Feb 00:05<br>Sat, 10 Feb 00:05<br><br>Return (Perth - Kuala Lumpur):<br>Mon, 12 Feb 06:05</div>
								  	</div>
								</div>
								<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
								  	<div class="ticket-card text-center center-block">
										<div id="head">
<!--											<div class="image" style="background: url('images/ufc2018/tix-4.jpg'); background-size: cover;"> -->
													<div class="image">
											  	<div class="overlay"></div>
											  	<div class="middle">
													<div class="text-red"><strike>RM2,560.00</strike></div>
													<div class="text">RM1,860.00</div>
											  	</div>
										  </div>
										</div>
<!--										<div class="price font-openSansRoboto">‪RM‬1,860.00</div> -->
										<p>P5 Ticket + Return Flight<br>tickets from<br>Kuala Lumpur</p>
										<p style="color:#999999;font-size:10px;">&nbsp;</p>
										<div class="datesFlight">Flight Dates Available:</div>
										<div class="scheduleFlight">Depart (Kuala Lumpur - Perth):<br>Fri, 09 Feb 00:05<br>Sat, 10 Feb 00:05<br><br>Return (Perth - Kuala Lumpur):<br>Mon, 12 Feb 06:05</div>
										
								  	</div>
								</div>
							</div>
							<div class="row text-center">
								<a class="btn btn-danger disabled" id="buyButton" datetime="Feb 10 2018 00:00:00 GMT+0800" target="_blank" href="http://redtix-tickets.airasia.com/en-AU/shows/ufc 221, perth/events" role="button">BUY TICKETS</a>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section class="pageCategory-section section-grey last" id="getTicket">
				<div class="container tixPrice">
					<div class="row">
						<div class="col-sm-offset-1 col-sm-10 ">
							{{-- <div class="note text-left">
								<p class="reminder">Upon purchase, our staff will contact you for further<br> details to assist in making your flight booking.<br><small class="smallReminder">Prices above are all inclusive. Only snap on price would be the RM8.00<br> transaction fee.</small></p>
							</div> --}}
							<div class="note text-left">
                                <h2>Notes</h2>
                                <ol>
                                    <li>Upon purchase, our staff will contact you for further details to assist in making your flight booking.</li>
                                    <li>Prices above are all inclusive. Only snap on price would be the RM8.00 transaction fee.</li>
                                </ol>
                            </div>
						</div>
					</div>
				</div>
			</section>

		</div><!-- /Main Body -->
  
	</section><!-- /Content Section -->

@endsection

@section('customjs')

	<script type="text/javascript">
	//Initialize Swiper
	var swiper = new Swiper('.swiper-container', {
		pagination: '.swiper-pagination',        
		paginationClickable: true,
		slidesPerView: 'auto',
		spaceBetween: 10,
		nextButton: '.swiper-button-next',
		prevButton: '.swiper-button-prev',
		freeMode: true
	});

	// Enlarge Seat Plan Image
	$(function() {
		$('.seatPlanImg').on('click', function() {
		$('.enlargeImageModalSource').attr('src', $(this).attr('src'));
		$('#enlargeImageModal').modal('show');
		});
	});

	// Hide top Banner when page scroll
	var header = $('.eventBanner');
	var range = 450;

	$(window).on('scroll', function () {
		
		var scrollTop = $(this).scrollTop();
		var offset = header.offset().top;
		var height = header.outerHeight();
		offset = offset + height;
		var calc = 1 - (scrollTop - offset + range) / range;

		header.css({ 'opacity': calc });

		if ( calc > '1' ) {
		header.css({ 'opacity': 1 });
		} else if ( calc < '0' ) {
		header.css({ 'opacity': 0 });
		}
	});

	// Smooth scroll for acnhor links
	$('a[href*="#"]:not([href="#"])').click(function() {
		if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
		var target = $(this.hash);
		target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
		if (target.length) {
			$('html, body').animate({
			scrollTop: target.offset().top
			}, 1000);
			return false;
		}
		}
	});

	</script>
	<script type="text/javascript">
	   var deadline = 'Feb 11 2018 00:00:00 GMT+0800';
	  // calculate time remaining
	  function time_remaining(endtime){
		  var t = Date.parse(endtime) - Date.parse(new Date());
		  var seconds = Math.floor( (t/1000) % 60 );
		  var minutes = Math.floor( (t/1000/60) % 60 );
		  var hours = Math.floor( (t/(1000*60*60)) % 24 );
		  var days = Math.floor( t/(1000*60*60*24) );
		  return {'total':t, 'days':days, 'hours':hours, 'minutes':minutes, 'seconds':seconds};
	  }

	  //output to html
	  function run_clock(id,endtime){
		  var clock = document.getElementById(id);
		  
		  // get spans where our clock numbers are held
		  var days_span = clock.querySelector('.days');
		  var hours_span = clock.querySelector('.hours');
		  var minutes_span = clock.querySelector('.minutes');
		  var seconds_span = clock.querySelector('.seconds');

		  function update_clock(){
			  var t = time_remaining(endtime);
			  
			  // update the numbers in each part of the clock
			  days_span.innerHTML = t.days;
			  hours_span.innerHTML = ('0' + t.hours).slice(-2);
			  minutes_span.innerHTML = ('0' + t.minutes).slice(-2);
			  seconds_span.innerHTML = ('0' + t.seconds).slice(-2);
			  
			  if(t.total<=0){ clearInterval(timeinterval); }
		  }
		  update_clock();
		  var timeinterval = setInterval(update_clock,1000);
	  }
	  run_clock('clockdiv',deadline);
	</script>

	{{-- Buy button disable --}}
	<script type="text/javascript">
		$(function() {
			$('a[id^=buyButton]').each(function() {
				var date = new Date();
				var enddate = $(this).attr('datetime'); 
				if ( Date.parse(date) >= Date.parse(enddate)) {
				  $(this).addClass('disabled');
				}
			});
		});
	</script>
	
@endsection

@section('modal')
	@include('layouts.partials.modals._seatplan')
	@include('layouts.partials.modals._getTixCustom')

@endsection