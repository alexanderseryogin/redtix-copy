{{--@extends('master')--}}
@extends('masterforpotatohead')
@section('title')
    Sunny Side Up Presents: Halsey
@endsection

@section('header')
@include('layouts.partials._header')
    <meta property="og:url" content="{{ Request::fullUrl() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Sunny Side Up" />
    <meta property="og:description" content="Sunny Side Up" />
    <meta property="og:image" content="{{ Request::Url().'images/sunnysideup2018/thumbnail1.jpg' }}" />
@endsection

@section('content')
<style type="text/css">
    @font-face {
        font-family: LeviBrush;
        src: url('/fonts/LEVIBRUSH.TTF');
    }
    #clockdiv{
        font-family: sans-serif;
        color: #fff;
        display: inline-block;
        font-weight: 100;
        text-align: center;
        font-size: 30px;
    }

    #clockdiv > div{
        margin: 2px;
        width: 60px;
        display: inline-block;
    }

    #clockdiv div > span{
        padding: 4px 10px;
        width: 100%;
        height: 60px;
        border-radius: 3px;
        background: #fcda6e;
        display: inline-block;
        font-size: 34px;
    }

    .smalltext{
        padding-top: 5px;
        font-family: Lato;
        font-size: 12px;
        color: #333333;
        background-color: #dbdbdb;
        background: linear-gradient(#fff, #dbdbdb);
        margin-top: 2px;
    }

    .text-blue {
        color: #1b2488;
    }
    .text-brush {
        font-family: 'LeviBrush';
    }
    .mt-10 {
        margin-top: 10px;
    }
    .line-up {
        background-color: #1f2144;

    }
    #path-1 {
        background-image: url('images/sunnysideupaug2018/2-bg.svg');
        background-size: 100%;
        background-repeat: no-repeat;

    }

    #event-programmes {
        background-image: url('images/sunnysideupaug2018/path-prog.svg');
        background-size: 110vw 100%;
        background-repeat: no-repeat;
    }   @media screen and (max-width: 768px){
            #event-programmes {
                background-position: 23px 801px;
            } 
        }

    .seat-map {
        width: 600px;
        position: relative;
        margin-top: -120px;
        margin-left: 280px;
    }
    .event-programmes {
        margin: 40px auto;
    }
    .social [class*="fa fa-"] {
        background-color: #333;
        border-radius: 30px;
        color: #fff;
        display: inline-block;
        height: 30px;
        line-height: 30px;
        margin: auto 3px;
        width: 30px;
        font-size: 15px;
        text-align: center;
    }

    .price-card {
        border-radius: 12px;
        background-color: #1e2144;
        width: 270px;
        height: 300px;
        padding: 30px;
        color: #fff;
        margin: 20px 2px;
        z-index: 2;
    }   @media screen and (max-width: 768px){ 
            .price-card {
                margin: 20px auto;
                
            }
        }

    .price-card2 {
        border-radius: 12px;
        background-color: #1e2144;
        width: auto;
        height: auto;
        color: #fff;
        margin: 20px 2px;
        z-index: 2;
    }   @media screen and (max-width: 768px){ 
            .price-card2 {
                margin: 20px auto;
                padding: 10px 10px;
            }
        }

    .btn-buy {
        border-radius: 6px;
        padding: 10px 50px;
        margin-top: 50px;
        margin-bottom: 50px;
        background-color: #f26844;
        font-family: Lato;
        font-size: 15px;
        font-weight: 300;
        letter-spacing: 0.8px;
        text-align: center;
        color: #ffffff;
    }
    .text-orange {
        color: #f26844;
    }
    .text-turqoise {
        color: #60bcb1;
    }
    .text-yellow {
        color: #fdd462;
    }
    .category {
        font-family: Lato;
        font-size: 12px;
        letter-spacing: 0.1px;
    }
    .price {
        font-size: 20px;
    }
    .day-column {
        margin-top: 20px;
        margin-bottom: 20px;
    } .day-column .row * {
            margin-top: 5px;
            margin-bottom: 5px;
        }

    .seating-map {
        margin-top: 200px;
        margin-bottom: 200px;
    }
    /*.ticket-price {
        padding-bottom: 200px;
    } @media screen and (max-width: 768px){
        .ticket-price {
            padding-bottom: 0px;
        } 
    }*/

    .bottompic {
        position: absolute;
        width: 50vw;
        right: 0px;
        bottom: 0px;
        z-index: 1;
    } @media screen and (max-width: 768px){
        .bottompic {
            position: absolute;
            width: 100vw;
            right: 0px;
            bottom: 0px;
            z-index: 1;
        } 
    }
    @media screen and (min-width: 1300px){
        .bottompic {
            position: absolute;
            width: 34vw;
            right: 0px;
            bottom: 0px;
            z-index: 1;
        } 
    }
    .bottompic-mobile {
        right: 0px;
        bottom: 0px;
        z-index: 1;
    }
    .toppic {
        object-fit: contain;
        position: absolute;
        width: 50%;
        left: 0px;
        top: 220px;
        z-index: 1;

        /*float: left;*/
    } @media screen and (min-width: 1300px){
        .toppic {
            object-fit: contain;
            position: absolute;
            width: 40%;
        }
    }
    .liim-desc {
        margin-top: 100px;
        z-index: 2; 
    }
    .artist-lineup {
        margin-top: 100px;
    } @media screen and (max-width: 768px){
        .artist-lineup {
            margin-top: 100px;
        }
    }  .artist-lineup  p {
            color: #fff;
        } 
        .artist-lineup img {
            width: 250px;
            padding-bottom: 20px;
        }  

    .artist-lineup-right {
        margin-top: 100px;
        text-align: right;
    } @media screen and (max-width: 768px){
        .artist-lineup-right {
            margin-top: 100px;
        }
    }  .artist-lineup-right  p {
            color: #fff;
            text-align: left;
        } 
        .artist-lineup-right img {
            width: 250px;
            padding-bottom: 20px;
        }  

    .p1 {
        margin-left: -70px;
    }
    .p2 {
        margin-left: -30px;
    }
    .p3 {
        margin-left: 10px;
    }
    .p4 {
        margin-left: 19px;
    }
    /*.line-title {
        margin-top: 30%
    }*/
    #mb-35 {
        margin-bottom: 35vh;
    } @media screen and (max-width: 768px){
        #mb-35 {
            margin-bottom: 0px;
        }
    }
    .amplitude-top {
        padding-left: 0px;
        padding-right: 0px;
        padding-bottom: 10px;
        padding-top: 2px;
    }  @media screen and (max-width: 768px){
        .amplitude-top img {
            height: auto;
        }
        
    }
    .amplitude-bottom {
        padding-left: 0px;
        padding-right: 0px;
        padding-bottom: 10px;
        padding-top: 10px;
    } .amplitude-bottom img{
            height: 30vh;
            width: 100vw;
        } @media screen and (max-width: 768px){
        .amplitude-bottom img {
            height: auto;
        }
    }
    .bottom-el {
        z-index: 2; 
        margin-bottom: 300px;
    }@media screen and (max-width: 768px){
        .bottom-el {
            z-index: 2; 
            margin-bottom: 0px;
        }
    }
</style>

    <!-- Banner Section -->
    {{-- <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        <div class="jumbotron eventBanner hidden-xs" style="background-image: url('images/sunnysideupaug2018/web-banner1.jpg')"></div>
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            <img src="{{asset('images/sunnysideupaug2018/thumbnail1.jpg')}}" style="width: 100%" class="img-responsive" alt="Sunny Side Up">
        </div>
    </section> --}}
    <!-- /Banner Section -->

    <!-- Banner Section -->
    <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/sunnysideupaug2018/web-banner1.jpg')}}" style="width: 100%" class="img-responsive" alt="Sunny Side Up Presents: Halsey"></div>
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            <img src="{{asset('images/sunnysideupaug2018/thumbnail1.jpg')}}" style="width: 100%" class="img-responsive" alt="Sunny Side Up Presents: Halsey">
        </div>
    </section>
    <!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
            <div class="row priceNbtn">
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="row">
                        <div class="col-sm-9 leftBox">
                            <h6>Sunny Side Up Presents: Halsey</h6>Tickets from <span>RM183</span>
                        </div>
                        <div class="col-sm-3 text-center">
                            <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar section-white">
            <section class="pageCategory-section last">
                {{-- <div class="toppic hidden-xs hidden-sm">
                    <img class="img-responsive" src="images/sunnysideupaug2018/about-ssu3.png">
                </div> --}}

                <div class="container-fluid intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            
                            {{-- coundown --}}
                            {{-- <div class="row text-center">
                                <h2>Early Bird Promo Ends In</h2>
                            </div> --}}
                            {{-- Styled clock --}}
                            {{-- <div id="clockdiv" class="col-sm-12 col-xs-12">
                                <div><span class="days"></span><div class="smalltext">Days</div></div>
                                <div><span class="hours"></span><div class="smalltext">Hours</div></div>
                                <div><span class="minutes"></span><div class="smalltext">Minutes</div></div>
                                <div><span class="seconds"></span><div class="smalltext">Seconds</div></div>
                            </div> --}}

                            {{-- <div class="col-xs-12 hidden-lg hidden-md">
                                <img class="img-responsive" src="images/sunnysideupaug2018/about-ssu3.png" style="position: relative;">
                            </div> --}}
                            <div class="col-md-12 liim-desc text-blue hidden-xs hidden-sm">
                                {{-- <p>Sunny Side Up returns for the 2nd night to Bali this August with a stellar line-up of music, mixology, food and culture in the tropical, open-air setting of Potato Head Beach Club. 2018’s performers include  Halsey &amp; NIKI, with more support acts to be announced.</p>
                                <p>Sunny Side Up returns for the 2nd night to Bali this August with a stellar line-up of music, mixology, food and culture in the tropical, open-air setting of Potato Head Beach Club. 2018’s performers include Halsey, RAC, Marian Hill, NIKI, Dipha Barus, Tantra &amp; Gero</p> --}}
                                <p>Sunny Side Up returns for the 2nd day to Bali this August with a stellar line-up of music, mixology, food and culture in the tropical, open-air setting of Potato Head Beach Club. 2018’s performers include Halsey, RAC, Marian Hill, NIKI, Dipha Barus, Tantra &amp; Gero.</p>
                                {{-- <p>For more information, visit: <a href="http://www.sunnysideupfest.com" target="_blank">www.sunnysideupfest.com</a> --}}
                            </div>
                            {{-- <div class="col-md-5 col-md-push-1 liim-desc text-blue hidden-xs hidden-sm text-center">
                                <p>SUNNY SIDE UP (DAY 1 | 21th July 2018)</p>
                                <p><img src="{{asset('images/sunnysideup2018v2/thumbnail.jpg')}}" style="width: 100%" class="img-responsive" alt="Sunny Side Up Presents: Nick Murphy f.k.a Chet Faker"></p>
                                <p><a class="btn btn-danger" id="buyButton" datetime="July 20 2018 00:00:00 GMT+0800" target="_blank" href="sunnysideupjul2018" role="button">Explore Day 1</a></p>
                            </div> --}}
                            <div class="col-md-12 liim-desc text-blue hidden-lg hidden-md">
                            {{-- <p>Sunny Side Up returns for the 2nd night to Bali this August with a stellar line-up of music, mixology, food and culture in the tropical, open-air setting of Potato Head Beach Club. 2018’s performers include  Halsey &amp; NIKI, with more support acts to be announced.</p>
                                <p>Sunny Side Up returns for the 2nd night to Bali this August with a stellar line-up of music, mixology, food and culture in the tropical, open-air setting of Potato Head Beach Club. 2018’s performers include Halsey, RAC, Marian Hill, NIKI, Dipha Barus, Tantra &amp; Gero</p> --}}
                                <p>Sunny Side Up returns for the 2nd day to Bali this August with a stellar line-up of music, mixology, food and culture in the tropical, open-air setting of Potato Head Beach Club. 2018’s performers include Halsey, RAC, Marian Hill, NIKI, Dipha Barus, Tantra &amp; Gero.</p>
                                {{-- <p>For more information, visit: <a href="http://www.sunnysideupfest.com" target="_blank">www.sunnysideupfest.com</a> --}}
                            </div>
                            {{-- <div class="col-md-7 liim-desc text-blue hidden-lg hidden-md text-center">
                                <p>SUNNY SIDE UP (DAY 1 | 21th July 2018)</p>
                                <p><img src="{{asset('images/sunnysideup2018v2/thumbnail.jpg')}}" style="width: 100%" class="img-responsive" alt="Sunny Side Up Presents: Nick Murphy f.k.a Chet Faker"></p>
                                <p><a class="btn btn-danger" id="buyButton" datetime="July 20 2018 00:00:00 GMT+0800" target="_blank" href="sunnysideupjul2018" role="button">Explore Day 1</a></p>
                            </div> --}}
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </section>

            <div class="container-fluid amplitude-top" style="margin-bottom: -13px">    
                <img class="img-responsive ampl" src="images/sunnysideupaug2018/amplitude-top.png">
            </div>

            <section class="pageCategory-section last line-up">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="row text-right hidden-xs hidden-sm line-title">
                                <h1 style="color: #fff">DJ</h1>
                                <h1 style="color: #fff; font-size: 100px;" class="text-brush">LINE-UP</h1>
                            </div>

                            <div class="col-xs-12 text-right hidden-lg hidden-md">
                                <h4 style="color: #60bcb1;">DJ</h4>
                                <h1 style="color: #60bcb1;" class="text-brush">LINE-UP</h1>
                            </div>

                            <div class="row artist-lineup-right">
                                <div class="col-md-6 col-xs-12 col-md-push-5">
                                    <img class="img-responsive center-block artist-img" src="images/sunnysideupaug2018/dj-lineup-1.jpg" alt="">
                                </div>
                                <div class="col-md-6 col-xs-12 col-md-pull-6">
                                    <p>Halsey is an award winning American singer/songwriter who, together with the Chainsmokers, was responsible for 2016's mega hit "Closer". She's collaborated with Justin Bieber, G-Eazy, Thirty Seconds to Mars as well as releasing numerous solo electropop and synth-pop hits on two studio albums.</p>
                                </div>
                            </div>

                            <div class="row artist-lineup">
                                <div class="col-md-6 col-xs-12 col-md-push-1">
                                    <img class="img-responsive center-block artist-img" src="images/sunnysideupaug2018/dj-lineup-2.jpg" alt="">
                                </div>
                                <div class="col-md-6 col-xs-12">
                                <p>Initially known as remixers to indie luminaries such as Washed Out, Phoenix, Yeah Yeah Yeahs, and Death Cab for Cutie, before creating their own electro-pop, RAC - short for Remix Artist Collective - is the brainchild of producer/multi-instrumentalist André Allen Anjos.</p>
                                </div>
                            </div>

                            <div class="row artist-lineup-right">
                                <div class="col-md-6 col-xs-12 col-md-push-5">
                                    <img class="img-responsive center-block artist-img" src="images/sunnysideupaug2018/dj-lineup-3.jpg" alt="">
                                </div>
                                <div class="col-md-6 col-xs-12 col-md-pull-6">
                                    <p>Marian Hill are a duo from Philadelphia consisting of singer Samantha Gongol and producer Jeremy Lloyd. Their unique sound combines sparse, minimal electronic beats with seductive vocals (which are often chopped up and manipulated) along with sultry saxophone.</p>
                                </div>
                            </div>

                            <div class="row artist-lineup">
                                <div class="col-md-6 col-xs-12 col-md-push-1">
                                    <img class="img-responsive center-block artist-img" src="images/sunnysideupaug2018/dj-lineup-4.jpg" alt="">
                                </div>
                                <div class="col-md-6 col-xs-12">
                                <p>NIKI also known as Nicole Zefanya is an Indonesian RnB singer/songwriter who gained popularity with her covers and original songs on Youtube. The soulful singer has since signed on to American label 88rising and is responsible hits like "Vintage", "Poloroid Boy" and "I lIke U".</p>
                                </div>
                            </div>

                            <div class="row artist-lineup-right">
                                <div class="col-md-6 col-xs-12 col-md-push-5">
                                    <img class="img-responsive center-block artist-img" src="images/sunnysideupaug2018/dj-lineup-5.jpg" alt="">
                                </div>
                                <div class="col-md-6 col-xs-12 col-md-pull-6">
                                    <p>Through over a decade of experience in the music industry Dipha Barus has established himself as a musical phenomenon with his brand of always relevant, upbeat music that cleverly interlocks traditional Indonesian music with current grooves.</p>
                                </div>
                            </div>

                            <div class="row artist-lineup">
                                <div class="col-md-6 col-xs-12 col-md-push-1">
                                    <img class="img-responsive center-block artist-img" src="images/sunnysideupaug2018/dj-lineup-6.jpg" alt="">
                                </div>
                                <div class="col-md-6 col-xs-12">
                                <p>From their base in Kerobokan, a multi-purpose space with recording studio and co-working space, Tantrà broadcast Paddy Grooves radio, host tabletop electronic music workshops, and throw their flagship party Senja, an intimate monthly get together featuring up-and-coming Bali selectors alongside established island residents.</p>
                                </div>
                            </div>

                            <div class="row artist-lineup-right">
                                <div class="col-md-6 col-xs-12 col-md-push-5">
                                    <img class="img-responsive center-block artist-img" src="images/sunnysideupaug2018/dj-lineup-7.jpg" alt="">
                                </div>
                                <div class="col-md-6 col-xs-12 col-md-pull-6">
                                    <p>Forsaking a career in aviation for music, Gero has become a mainstay at Potato Head’s vinyl refuge, Studio Eksotika. His influences include smoky jazz, new wave and European disco. Gero's versatile approach to playing have garnered him a reputation as one of the best selectors on the island.</p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </section>

            <div class="container-fluid amplitude-bottom" style="margin-top: -14px">
                <img class="img-responsive" src="images/sunnysideupaug2018/amplitude-bottom.png">
            </div>

            <section class="pageCategory-section last section-grey">
                <div class="container">
                    <div class="gallery text-center">
                        <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
                        <!-- Swiper -->
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <a href="images/sunnysideup2018/gallery-1.jpg" data-featherlight="image"><img class="" src="images/sunnysideup2018/gallery-1.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/sunnysideup2018/gallery-2.jpg" data-featherlight="image"><img class="" src="images/sunnysideup2018/gallery-2.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/sunnysideup2018/gallery-3.jpg" data-featherlight="image"><img class="" src="images/sunnysideup2018/gallery-3.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/sunnysideup2018/gallery-4.jpg" data-featherlight="image"><img class="" src="images/sunnysideup2018/gallery-4.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/sunnysideup2018/gallery-5.jpg" data-featherlight="image"><img class="" src="images/sunnysideup2018/gallery-5.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/sunnysideup2018/gallery-6.jpg" data-featherlight="image"><img class="" src="images/sunnysideup2018/gallery-6.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/sunnysideup2018/gallery-7.jpg" data-featherlight="image"><img class="" src="images/sunnysideup2018/gallery-7.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/sunnysideup2018/gallery-8.jpg" data-featherlight="image"><img class="" src="images/sunnysideup2018/gallery-8.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/sunnysideup2018/gallery-9.jpg" data-featherlight="image"><img class="" src="images/sunnysideup2018/gallery-9.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/sunnysideup2018/gallery-10.jpg" data-featherlight="image"><img class="" src="images/sunnysideup2018/gallery-10.jpg" alt=""></a>
                                </div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next swiper-button-white"></div>
                            <div class="swiper-button-prev swiper-button-white"></div>
                        </div><!-- /Swiper -->
                    </div>
                </div>
            </section>




            <section class="pageCategory-section last" id="anchorPrice">
                <div class="container ticket-price  text-center">
                    <div class="row">
                        <div class="col-sm-12">
                            <h1 class="hidden-xs hidden-sm text-center"><span class="text-yellow">TICKET</span> <span class="text-brush">PRICES</span></h1>
                            <h3 class="hidden-lg hidden-md text-center"><span class="text-yellow">TICKET</span> <span class="text-brush">PRICES</span></h3>

                            {{-- <div class="col-sm-12 ">
                                    <div class="col-sm-3 clearfix"></div>
                                    <div class="col-sm-6 price-card2">
                                    <p><h4 class="text-brush">VIP Packages</h4> Contact Potato Head directly to enquire about VIP packages 
                                        <a class="btn btn-danger" id="emailButton" datetime="Aug 11 2018 00:00:00 GMT+0800" target="_blank" href="mailto:sales@pttfamily.com" role="button">CONTACT US</a>
                                        </p>
                                    </div>
                                    <div class="col-sm-3 clearfix"></div>
                            </div> --}}

                            <div class="col-sm-12 ">
                                    <div class="col-sm-3 price-card text-center">
                                        <h4 class="text-center text-brush">GA Early Bird</h4>
                                        <div class="row">
                                            {{-- <div class="category">General Admission</div>
                                            <div class="price">IDR 600,000</div> --}}
                                            <div class="price"><strike>IDR 600,000</strike><p style="color:#EA374C;">SOLD OUT</p></div>
                                        </div>
                                        <div class="row">
                                            ______
                                        </div>
                                        <div class="row">
                                            {{-- <div class="category">Season Pass</div> --}}
                                            <div class="price">RM 183.00</div>
                                        </div>
                                        <span class="category">Limited Tickets Available</span>
                                    </div>
                                    <div class="col-sm-3 price-card text-center">
                                        <h4 class="text-center text-brush">GA Pre Sale</h4>
                                        <div class="row">
                                            {{-- <div class="category">General Admission</div> --}}
                                            <div class="price">IDR 700,000</div>
                                        </div>
                                        <div class="row">
                                            ______
                                        </div>
                                        <div class="row">
                                            {{-- <div class="category">Season Pass</div> --}}
                                            <div class="price">RM 214.00</div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3 price-card text-center">
                                        <h4 class="text-center text-brush" style="font-size: 24px; margin-bottom: 5px;">GA Early Arrival Before 6pm</h4>
                                        <div class="row">
                                            {{-- <div class="category">General Admission</div> --}}
                                            <div class="price">IDR 600,000</div>
                                        </div>
                                        <div class="row">
                                            ______
                                        </div>
                                        <div class="row">
                                            {{-- <div class="category">Season Pass</div> --}}
                                            <div class="price">RM 183.00</div>
                                        </div>
                                        <span class="category">Arrive before 6pm<br>limited numbers</span>
                                    </div>
                                    <div class="col-sm-3 price-card text-center">
                                        <h4 class="text-center text-brush">VIP Standing</h4>
                                        <div class="row">
                                            {{-- <div class="category">General Admission</div> --}}
                                            <div class="price">IDR 1,100,000</div>
                                        </div>
                                        <div class="row">
                                            ______
                                        </div>
                                        <div class="row">
                                            {{-- <div class="category">Season Pass</div> --}}
                                            <div class="price">RM 336.00</div>
                                        </div>
                                        <span class="category">Limited Tickets Available<br />VIP Lounge &amp; Terrace</span>
                                    </div>
                            </div>
                            <div class="col-sm-12 text-center" style="z-index: 2;">
                                {{-- <a class="btn btn-danger" id="buyButton" datetime="Aug 11 2018 00:00:00 GMT+0800" target="_blank" href="https://redtix-tickets.airasia.com/loader.aspx/?target=hall.aspx?event=5339" role="button">BUY TICKETS</a> --}}
                                <a class="btn btn-danger disabled" id="buyButton" datetime="Aug 11 2018 00:00:00 GMT+0800" target="_blank" href="https://redtix-tickets.airasia.com/loader.aspx/?target=hall.aspx?event=5348" role="button">BUY TICKETS</a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            {{-- <section class="pageCategory-section last" id="anchorPrice">
                <div class="container ticket-price  text-center">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="col-sm-12 ">
                                <p><br /><br /><img src="{{asset('images/sunnysideup2018v2/web-banner.jpg')}}" style="width: 100%" class="img-responsive" alt="Sunny Side Up Presents: Nick Murphy f.k.a Chet Faker"></p>
                            </div>
                            <div class="col-sm-12 text-center" style="z-index: 2;">
                                <a class="btn btn-danger" id="buyButton" datetime="Jul 20 2018 00:00:00 GMT+0800" target="_blank" href="sunnysideupjul2018" role="button">Explore Day 1</a>
                            </div>
                        </div>
                    </div>
                </div>
            </section> --}}

            <section class="pageCategory-section last">
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 bottom-el">
                            <div class="note text-left">
                                <h2>Important Notes</h2>
                                <ol>
                                    <li>Prices quoted in IDR may differ due to currency fluctuations.</li>
                                    <li>Prices shown exclude RM8.00 AirAsiaRedTix fee.</li>
                                    <li>All ticket prices will be paid in Malaysian Ringgit. Indonesian Rupiah prices are shown for reference purposes only.</li>
                                    <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li>
                                    <li>RM10.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                                    <li>Online ticket selling will close 1 days prior to event day, subject to availability.</li>
                                    <li>Adult: 18 years old and above.</li>
                                </ol>
                                <h2>For enquiry only:</h2>
                                <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- <div class="bottompic-mobile hidden-lg hidden-md">
                    <img class="img-responsive" src="images/sunnysideupaug2018/bottom-bg2.png">
                </div>
                <div class="bottompic hidden-xs" style="margin-top:30vh;">
                <img class="img-responsive" src="images/sunnysideupaug2018/bottom-bg2.png">
            </div> --}}
            </section>
            {{-- <div class="bottompic hidden-xs" style="margin-top:30vh;">
                <img class="img-responsive" src="images/sunnysideupaug2018/bottom-bg.png">
            </div> --}}

        </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 450;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });
    </script>

    {{-- Countdown Timer --}}
    {{-- <script>
        // endtime
        var deadline = 'Nov 30 2017 23:59:00 GMT+0800';

        // calculate time remaining
        function time_remaining(endtime){
            var t = Date.parse(endtime) - Date.parse(new Date());
            var seconds = Math.floor( (t/1000) % 60 );
            var minutes = Math.floor( (t/1000/60) % 60 );
            var hours = Math.floor( (t/(1000*60*60)) % 24 );
            var days = Math.floor( t/(1000*60*60*24) );
            return {'total':t, 'days':days, 'hours':hours, 'minutes':minutes, 'seconds':seconds};
        }

        //output to html
        function run_clock(id,endtime){
            var clock = document.getElementById(id);
            
            // get spans where our clock numbers are held
            var days_span = clock.querySelector('.days');
            var hours_span = clock.querySelector('.hours');
            var minutes_span = clock.querySelector('.minutes');
            var seconds_span = clock.querySelector('.seconds');

            function update_clock(){
                var t = time_remaining(endtime);
                
                // update the numbers in each part of the clock
                days_span.innerHTML = t.days;
                hours_span.innerHTML = ('0' + t.hours).slice(-2);
                minutes_span.innerHTML = ('0' + t.minutes).slice(-2);
                seconds_span.innerHTML = ('0' + t.seconds).slice(-2);
                
                if(t.total<=0){ clearInterval(timeinterval); }
            }
            update_clock();
            var timeinterval = setInterval(update_clock,1000);
        }
        run_clock('clockdiv',deadline);
    </script>
     --}}

    {{-- <script>
    // Modal Announcement
    //$(document).ready(function(){
    //  $("#announcementModal").modal('show');
    //});
    </script> --}}
@endsection

@section('modal')
    @include('layouts.partials.modals._seatplan')
    @include('layouts.partials.modals._getTixCustom')

    <!--Modal Announcement-->
    <div id="announcementModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body text-center">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
                <h1><i class="fa fa-bullhorn text-warning" aria-hidden="true"></i></h1>
                <!--<img style="width:100px; height:100px"src="images/airasia-logo.png">-->
                <h4>ONLINE TICKET SALES CLOSED</h4>
                <div class="clearfix">&nbsp;</div>
                <div class="well">
                    <p>Please proceed to venue to purchase your tickets</p>
                    <p>Thank you</p>
                </div>
            </div>
        </div>
        </div>
    </div>
@endsection