@extends('master')
@section('title')
    Island Music Festival 2018
@endsection

@section('header')
    @include('layouts.partials._header')
    <meta property="og:url" content="{{ Request::fullUrl() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Island Music Festival 2018" />
    <meta property="og:description" content="The Island Music Festival is back for its 6th edition from 12th – 14th October 2018 on Long Beach, Redang Island."/>
    <meta property="og:image" content="{{ Request::Url().'images/islandmusicfest2018v2/thumbnail1.jpg' }}" />
@endsection

@section('content')
<style type="text/css">
    @font-face {
        font-family: LeviBrush;
        src: url('/fonts/LEVIBRUSH.TTF');
    }
    #clockdiv{
        font-family: sans-serif;
        color: #fff;
        display: inline-block;
        font-weight: 100;
        text-align: center;
        font-size: 30px;
    }

    #clockdiv > div{
        margin: 2px;
        width: 60px;
        display: inline-block;
    }

    #clockdiv div > span{
        padding: 4px 10px;
        width: 100%;
        height: 60px;
        border-radius: 3px;
        background: #fcda6e;
        display: inline-block;
        font-size: 34px;
    }

    .smalltext{
        padding-top: 5px;
        font-family: Lato;
        font-size: 12px;
        color: #333333;
        background-color: #dbdbdb;
        background: linear-gradient(#fff, #dbdbdb);
        margin-top: 2px;
    }

    .text-blue {
        color: #1b2488;
    }
    .text-brush {
        font-family: 'LeviBrush';
    }
    .mt-10 {
        margin-top: 10px;
    }
    .line-up {
        background-color: #FFD09A;

    }
    #path-1 {
        background-image: url('images/islandmusicfest2018v2/2-bg.svg');
        background-size: 100%;
        background-repeat: no-repeat;

    }

    #event-programmes {
        background-image: url('images/islandmusicfest2018v2/path-prog.svg');
        background-size: 110vw 100%;
        background-repeat: no-repeat;
    }   @media screen and (max-width: 768px){
            #event-programmes {
                background-position: 23px 801px;
            } 
        }

    .seat-map {
        width: 600px;
        position: relative;
        margin-top: -120px;
        margin-left: 280px;
    }
    .event-programmes {
        margin: 40px auto;
    }
    .social [class*="fa fa-"] {
        background-color: #333;
        border-radius: 30px;
        color: #fff;
        display: inline-block;
        height: 30px;
        line-height: 30px;
        margin: auto 3px;
        width: 30px;
        font-size: 15px;
        text-align: center;
    }

    .price-card {
        border-radius: 12px;
        background-color: #FFD09A;
        width: 220px;
        height: 350px;
        padding: 30px;
        color: #FF9797;
        margin: 20px 2px;
        z-index: 2;
    }   @media screen and (max-width: 768px){ 
            .price-card {
                margin: 20px auto;
            }
        }

    .btn-buy {
        border-radius: 6px;
        padding: 10px 50px;
        margin-top: 50px;
        margin-bottom: 50px;
        background-color: #f26844;
        font-family: Lato;
        font-size: 15px;
        font-weight: 300;
        letter-spacing: 0.8px;
        text-align: center;
        color: #ffffff;
    }
    .text-orange {
        color: #f26844;
    }
    .text-turqoise {
        color: #60bcb1;
    }
    .text-yellow {
        color: #fdd462;
    }
    .text-pink {
        color: #FF9A9A;
    }
    .category {
        font-family: Lato;
        font-size: 12px;
        letter-spacing: 0.1px;
    }
    .price {
        font-size: 25px;
    }
    .price2 {
        font-size: 22px;
    }
    .day-column {
        margin-top: 20px;
        margin-bottom: 20px;
    } .day-column .row * {
            margin-top: 5px;
            margin-bottom: 5px;
            text-align: center;
        }

        @media screen and (max-width: 768px){ 
            .day-column {
                margin-top: 20px;
                margin-bottom: 20px;
            } .day-column .row * {
                    margin-top: 5px;
                    margin-bottom: 5px;
                    text-align: center;
                }
        }

    .seating-map {
        margin-top: 50px;
        margin-bottom: 50px;
    }
    /*.ticket-price {
        padding-bottom: 200px;
    } @media screen and (max-width: 768px){
        .ticket-price {
            padding-bottom: 0px;
        } 
    }*/

    .bottompic {
        position: absolute;
        width: 50vw;
        right: 0px;
        bottom: 0px;
        z-index: 1;
    } @media screen and (max-width: 768px){
        .bottompic {
            position: absolute;
            width: 100vw;
            right: 0px;
            bottom: 0px;
            z-index: 1;
        } 
    }
    @media screen and (min-width: 1300px){
        .bottompic {
            position: absolute;
            width: 34vw;
            right: 0px;
            bottom: 0px;
            z-index: 1;
        } 
    }
    .bottompic-mobile {
        right: 0px;
        bottom: 0px;
        z-index: 1;
    }
    .toppic {
        object-fit: contain;
        position: absolute;
        width: 50%;
        left: 0px;
        top: 220px;
        z-index: 1;

        /*float: left;*/
    } @media screen and (min-width: 1300px){
        .toppic {
            object-fit: contain;
            position: absolute;
            width: 40%;
        }
    }
    .liim-desc {
        margin-top: 25px;
        z-index: 2; 
    }
    .artist-lineup {
        margin-top: 50px;

    } @media screen and (max-width: 768px){
        .artist-lineup {
            margin-top: 20px;
        }
    }  .artist-lineup  p {
            color: #fff;
        } 
        .artist-lineup img {
            width: 350px;
            margin-bottom: 50px;
        }  

    .p1 {
        margin-left: -70px;
    }
    .p2 {
        margin-left: -30px;
    }
    .p3 {
        margin-left: 10px;
    }
    .p4 {
        margin-left: 19px;
    }
    /*.line-title {
        margin-top: 30%
    }*/
    #mb-35 {
        margin-bottom: 35vh;
    } @media screen and (max-width: 768px){
        #mb-35 {
            margin-bottom: 0px;
        }
    }
    .amplitude-top {
        padding-left: 0px;
        padding-right: 0px;
        padding-bottom: 10px;
        padding-top: 10px;
    }  @media screen and (max-width: 768px){
        .amplitude-top img {
            height: auto;
        }
        
    }
    .amplitude-bottom {
        padding-left: 0px;
        padding-right: 0px;
        padding-bottom: 10px;
        padding-top: 10px;
    } .amplitude-bottom img{
            height: 30vh;
            width: 100vw;
        } @media screen and (max-width: 768px){
        .amplitude-bottom img {
            height: auto;
        }
    }
    .bottom-el {
        z-index: 2; 
        margin-bottom: 300px;
    }@media screen and (max-width: 768px){
        .bottom-el {
            z-index: 2; 
            margin-bottom: 0px;
        }
    }
</style>

    <!-- Banner Section -->
    <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        <div class="jumbotron eventBanner hidden-xs" style="background-image: url('images/islandmusicfest2018v2/web-banner1.jpg')"></div>
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            <img src="{{asset('images/islandmusicfest2018v2/thumbnail1.jpg')}}" style="width: 100%" class="img-responsive" alt="Langkawi International Island Music 2017">
        </div>
    </section>
    <!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
          <div class="row priceNbtn">
            <div class="col-sm-offset-1 col-sm-10">
              <div class="row">
                <div class="col-sm-9 leftBox">
                  <h6>Island Music Festival 2018</h6>
                  Tickets from <span>RM650</span>
                </div>
                <div class="col-sm-3 text-center">
                  <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                </div>
              </div>
            </div>
          </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar section-white">
            <section class="pageCategory-section last">
                {{-- <div class="container intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <!--<h2>Title</h2>-->
                            <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i> 12th October 2018</div>
                            <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i> Long Beach, Redang Island<a target="_blank" href="https://goo.gl/maps/FT4p9xvR8Cs"> View Map</a></div>
                            <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> 8.00am - 12.00am</div>
                            <div class="clearfix">&nbsp;</div>
                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <div class="addthis_inline_share_toolbox"></div>
                            <!-- /sharing -->
                        </div>
                    </div>
                </div> --}}

                <div class="toppic hidden-xs hidden-sm">
                    <img class="img-responsive" src="images/islandmusicfest2018v2/about-imf.png">
                </div>

                <div class="container-fluid intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            {{-- coundown --}}
                            {{-- <div class="row text-center">
                                <h2>Early Bird Promo Ends In</h2>
                            </div> --}}
                            {{-- Styled clock --}}
                            {{-- <div id="clockdiv" class="col-sm-12 col-xs-12">
                                <div><span class="days"></span><div class="smalltext">Days</div></div>
                                <div><span class="hours"></span><div class="smalltext">Hours</div></div>
                                <div><span class="minutes"></span><div class="smalltext">Minutes</div></div>
                                <div><span class="seconds"></span><div class="smalltext">Seconds</div></div>
                            </div> --}}

                            <div class="col-xs-12 hidden-lg hidden-md">
                                <img class="img-responsive" src="images/islandmusicfest2018v2/about-imf.png" style="position: relative;">
                            </div>
                            <div class="col-md-8 col-md-offset-4 liim-desc text-blue hidden-xs hidden-sm">
                                <p>After 5 successful years, the Island Music Festival is back for its 6th edition from 12th – 14th October 2018 on Long Beach, Redang Island. This year, with more activites, workshops and performances.</p>
                                <p>The Island Music Festival guarantees a unique experience on the beautiful Redang Island, Malaysia.</p>
                                <p>Come Feel It.</p>
                                <p>#imfparadise #beachbum2018 #letsbebeachy</p>
                                <p>Event FAQ: <a href="https://theislandmusicfestival.com/faq/">https://theislandmusicfestival.com/faq/</a></p>
                            </div>
                            <div class="col-md-8 col-md-offset-4 liim-desc text-blue hidden-lg hidden-md">
                                <p>After 5 successful years, the Island Music Festival is back for its 6th edition from 12th – 14th October 2018 on Long Beach, Redang Island. This year, with more activites, workshops and performances.</p>
                                <p>The Island Music Festival guarantees a unique experience on the beautiful Redang Island, Malaysia.</p>
                                <p>Come Feel It.</p>
                                <p>#imfparadise #beachbum2018 #letsbebeachy</p>
                                <p>Event FAQ: <a href="https://theislandmusicfestival.com/faq/">https://theislandmusicfestival.com/faq/</a></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </section>

            <div class="container-fluid amplitude-top" style="margin-bottom: -13px">    
                <img class="img-responsive ampl" src="images/islandmusicfest2018v2/amplitude-top.svg">
            </div>

            <section class="pageCategory-section last line-up">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="row text-right hidden-xs hidden-sm line-title">
                                <h1 style="color: #fff">ARTIST</h1>
                                <h1 style="color: #fff; font-size: 100px;" class="text-brush">LINE-UP</h1>
                            </div>

                            <div class="col-xs-12 text-right hidden-lg hidden-md">
                                <h4 style="color: #fff;">ARTIST</h4>
                                <h1 style="color: #fff;" class="text-brush">LINE-UP</h1>
                            </div>

                            <div class="row artist-lineup">
                                <div class="col-md-4 col-xs-12">
                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/a-kid.png" alt="">
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/annatasha.png" alt=""> 
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/biggie.png" alt="">
                                </div>
                            </div>

                            <div class="row artist-lineup">
                                <div class="col-md-4 col-xs-12">
                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/bihzhu.png" alt="">
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/christian-theseira.png" alt=""> 
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/dani.png" alt="">
                                </div>
                            </div>

                            <div class="row artist-lineup">
                                <div class="col-md-4 col-xs-12">
                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/gabriel-lynch.png" alt="">
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/jumero.png" alt=""> 
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/kaya.png" alt="">
                                </div>
                            </div>

                            <div class="row artist-lineup">
                                <div class="col-md-4 col-xs-12">
                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/kidd-santhe.png" alt="">
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/laskar-sonas-ft.png" alt="">
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/lastlogic.png" alt=""> 
                                </div>
                            </div>

                            <div class="row artist-lineup">
                                <div class="col-md-4 col-xs-12">
                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/leng-sisters.png" alt="">
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/lil-j.png" alt="">
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/massmusic.png" alt=""> 
                                </div>
                            </div>

                            <div class="row artist-lineup">
                                <div class="col-md-4 col-xs-12">
                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/njwa.png" alt="">
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/oddicon.png" alt="">
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/peanut-butter-jelly-jam.png" alt=""> 
                                </div>
                            </div>

                            <div class="row artist-lineup">
                                <div class="col-md-4 col-xs-12">
                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/pj12.png" alt="">
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/pris-xavier.png" alt="">
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/rhythm-rebels.png" alt=""> 
                                </div>
                            </div>

                            <div class="row artist-lineup">
                                <div class="col-md-4 col-xs-12">
                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/seatravel.png" alt="">
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/skeletor.png" alt=""> 
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/sounds-of-jane.png" alt="">
                                </div>
                            </div>

                            <div class="row artist-lineup">
                                <div class="col-md-4 col-xs-12">
                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/vital-signals.png" alt="">
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/zupanova.png" alt=""> 
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    
                                </div>
                            </div>







                        </div>
                    </div>
                </div>
            </section>

            <div class="container-fluid amplitude-bottom" style="margin-top: -14px">
                <img class="img-responsive" src="images/islandmusicfest2018v2/amplitude-bottom.png">
            </div>


            <div class="container-fluid" id="event-programmes">
                <section class="pageCategory-section last">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-offset-1 col-sm-10 leftBar">
                                <h1 class="hidden-xs hidden-sm text-center" style="color: #FF9A9A;">EVENT <span class="text-brush text-turqoise">PROGRAMMES</span></h1>
                                <h3 class="hidden-lg hidden-md text-center" style="color: #FF9A9A;">EVENT <span class="text-brush text-turqoise">PROGRAMMES</span></h3>

                                <div class="col-md-12 col-xs-12 event-programmes">
                                    <div class="col-md-6 col-xs-12 day-column">
                                        <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/activities.png" alt="" style="width: 200px; margin-bottom: 25px;">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">ATV ON THE BEACH </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">BEACH FOOTBALL</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">BEACH VOLLEYBALL</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">FASHION SHOW</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">JUNGLE TREKKING</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">KAYAK</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">PADDLE BOARD</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">SNORKELING</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">YOGA</div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-xs-12 day-column">
                                        <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/workshops.png" alt="" style="width: 200px; margin-bottom: 25px;">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">ARTS &amp; CRAFTS</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">COOKING</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">COCKTAIL MAKING</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">FIRST AID</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">MUSIC</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">SAND SCULPTING</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-xs-12 seating-map">
                                    <div class="embed-responsive embed-responsive-16by9"> 
                                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/Zo-Q8-Hfr0A?ecver=2" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                                    </div>
                                </div>

                                <div class="col-sm-12 seating-map">
                                    <h1 class="hidden-xs hidden-sm text-center" style="color: #FF9A9A;">EVENT <span class="text-brush text-turqoise">PARTNERS</span></h1>
                                    <h3 class="hidden-lg hidden-md text-center" style="color: #FF9A9A;">EVENT <span class="text-brush text-turqoise">PARTNERS</span></h3>
                                    <div>
                                        <table>
                                            <tr>
                                                <td><img class="image-responsive" src="images/islandmusicfest2018/sponsor-2.png" style="width:90%; height:auto;" alt=""></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>

                                <div class="col-sm-12 seating-map">
                                    <h1 class="hidden-xs hidden-sm text-center" style="color: #FF9A9A;">EVENT <span class="text-brush text-turqoise">MAP</span></h1>
                                    <h3 class="hidden-lg hidden-md text-center" style="color: #FF9A9A;">EVENT <span class="text-brush text-turqoise">MAP</span></h3>
                                    <div>
                                        <table>
                                            <tr>
                                                <td><img class="image-responsive" src="images/islandmusicfest2018/layout-1.jpg" style="width:90%; height:auto;" alt=""></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>




                            </div>
                        </div>
                    </div>
                </section>
            </div>
    





            <section class="pageCategory-section last section-grey">
                <div class="container">
                    <div class="gallery text-center">
                        <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
                        <!-- Swiper -->
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <a href="images/islandmusicfest2018/activity-0.jpg" data-featherlight="image"><img class="" src="images/islandmusicfest2018/activity-0.jpg" alt=""></a>
                            </div>
                            <div class="swiper-slide">
                                <a href="images/islandmusicfest2018/activity-1.jpg" data-featherlight="image"><img class="" src="images/islandmusicfest2018/activity-1.jpg" alt=""></a>
                            </div>
                            <div class="swiper-slide">
                                <a href="images/islandmusicfest2018/activity-2.jpg" data-featherlight="image"><img class="" src="images/islandmusicfest2018/activity-2.jpg" alt=""></a>
                            </div>
                            <div class="swiper-slide">
                                <a href="images/islandmusicfest2018/activity-3.jpg" data-featherlight="image"><img class="" src="images/islandmusicfest2018/activity-3.jpg" alt=""></a>
                            </div>
                            <div class="swiper-slide">
                                <a href="images/islandmusicfest2018/gallery-1.jpg" data-featherlight="image"><img class="" src="images/islandmusicfest2018/gallery-1.jpg" alt=""></a>
                            </div>
                            <div class="swiper-slide">
                                <a href="images/islandmusicfest2018/gallery-2.jpg" data-featherlight="image"><img class="" src="images/islandmusicfest2018/gallery-2.jpg" alt=""></a>
                            </div>
                            <div class="swiper-slide">
                                <a href="images/islandmusicfest2018/gallery-3.jpg" data-featherlight="image"><img class="" src="images/islandmusicfest2018/gallery-3.jpg" alt=""></a>
                            </div>
                            <div class="swiper-slide">
                                <a href="images/islandmusicfest2018/gallery-4.jpg" data-featherlight="image"><img class="" src="images/islandmusicfest2018/gallery-4.jpg" alt=""></a>
                            </div>
                            <div class="swiper-slide">
                                <a href="images/islandmusicfest2018/gallery-5.jpg" data-featherlight="image"><img class="" src="images/islandmusicfest2018/gallery-5.jpg" alt=""></a>
                            </div>
                            <div class="swiper-slide">
                                <a href="images/islandmusicfest2018/gallery-6.jpg" data-featherlight="image"><img class="" src="images/islandmusicfest2018/gallery-6.jpg" alt=""></a>
                            </div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next swiper-button-white"></div>
                            <div class="swiper-button-prev swiper-button-white"></div>
                        </div><!-- /Swiper -->
                    </div>
                </div>
            </section>

            <section class="pageCategory-section last" id="anchorPrice">
                <div class="container ticket-price">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <h1 class="hidden-xs hidden-sm text-center"><span class="text-pink">TICKET</span> <span class="text-brush text-turqoise">PRICES</span></h1>
                            <h3 class="hidden-lg hidden-md text-center"><span class="text-pink">TICKET</span> <span class="text-brush text-turqoise">PRICES</span></h3>

                            <div class="col-sm-12 center-block">
                                    <div class="col-md-3 price-card text-center">
                                        {{-- <h4 class="text-center text-brush text-turqoise"><strike>Early Bird</strike><br><span style="font-size: 19px;">SOLD OUT</span></h4>
                                        <div class="row">
                                            <div class="category">Single Occupancy</div>
                                            <div class="price"><strike>RM 650.00</strike></div>
                                        </div>
                                        <div class="row">
                                            ______
                                        </div>
                                        <div class="row">
                                            <div class="category">Twin Sharing (2 Pax)</div>
                                            <div class="price"><strike>RM 900.00</strike></div>
                                        </div> --}}
                                        <h4 class="text-center text-brush text-turqoise">Early Bird</h4>
                                        <div class="row">
                                        <div class="category">Single Occupancy</div>
                                        <img src="{{asset('images/islandmusicfest2018v2/sold-out.png')}}" style="width: 100%" class="img-responsive" alt="Sold Out">
                                        <div class="category">Twin Sharing (2 Pax)</div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 price-card text-center">
                                        {{-- <h4 class="text-center text-brush text-turqoise"><strike>Tier 1</strike><br><span style="font-size: 19px;">SOLD OUT</h4>
                                        <div class="row">
                                            <div class="category">Single Occupancy</div>
                                            <div class="price"><strike>RM 750.00</strike></div>
                                        </div>
                                        <div class="row">
                                            ______
                                        </div>
                                        <div class="row">
                                            <div class="category">Twin Sharing (2 Pax)</div>
                                            <div class="price"><strike>RM 1,100.00</strike></div>
                                        </div> --}}
                                        <h4 class="text-center text-brush text-turqoise">Tier 1</h4>
                                        <div class="row">
                                        <div class="category">Single Occupancy</div>
                                        <img src="{{asset('images/islandmusicfest2018v2/sold-out.png')}}" style="width: 100%" class="img-responsive" alt="Sold Out">
                                        <div class="category">Twin Sharing (2 Pax)</div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 price-card text-center">
                                        {{-- <h4 class="text-center text-brush text-turqoise">Tier 2</h4>
                                        <div class="row">
                                            <div class="category">Single Occupancy</div>
                                            <div class="price">RM 800.00</div>
                                        </div>
                                        <div class="row">
                                            ______
                                        </div>
                                        <div class="row">
                                            <div class="category">Twin Sharing (2 Pax)</div>
                                            <div class="price">RM 1,200.00</div>
                                        </div>
                                        <div class="row">
                                            ______
                                        </div>
                                        <div class="row">
                                            <div class="category">Quad Sharing (4 Pax)</div>
                                            <div class="price">RM 2,000.00</div>
                                        </div> --}}
                                        <h4 class="text-center text-brush text-turqoise">Tier 2</h4>
                                        <div class="row">
                                        <div class="category">Single Occupancy</div>
                                        <img src="{{asset('images/islandmusicfest2018v2/sold-out.png')}}" style="width: 100%" class="img-responsive" alt="Sold Out">
                                        <div class="category">Twin Sharing (2 Pax)</div>
                                        </div>
                                        <div class="row">
                                        &nbsp;
                                        </div>
                                        <div class="row">
                                        <div class="category">Quad Sharing (4 Pax)</div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 price-card text-center">
                                        <h4 class="text-center text-brush text-turqoise">Tier 3</h4>
                                        <div class="row">
                                            <div class="category">Single Occupancy</div>
                                            <div class="price2">RM 850.00</div>
                                        </div>
                                        <div class="row">
                                            ______
                                        </div>
                                        <div class="row">
                                            <div class="category">Twin Sharing (2 Pax)</div>
                                            <div class="price2">RM 1,300.00</div>
                                        </div>
                                        <div class="row">
                                            ______
                                        </div>
                                        <div class="row">
                                            <div class="category">Quad Sharing (4 Pax)</div>
                                            <div class="price2">RM 2,150.00</div>
                                        </div>
                                    </div>
                            </div>
                            <div class="col-sm-12 text-center" style="z-index: 2;">
                                <a class="btn btn-buy" id="buyButton" datetime="Oct 12 2018 00:00:00 GMT+0800" target="_blank" href="https://redtix-tickets.airasia.com/en-AU/shows/island music festival 2018 (12 to 14 october 2018)/events" role="button">BUY TICKETS</a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="pageCategory-section last">
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 bottom-el">
                        <div class="clearfix">&nbsp;</div>
                            <div class="note text-left">
                            <span class="importantNote">1. Package price for Single Occupancy entitles 1 Pax only.</span>
                            <span class="importantNote">2. Package price for Twin Sharing entitles 2 Pax only.</span>
                            <span class="importantNote">3. Ticket price is inclusive of all Island Music Festival Activities and Entertainment.</span>
                            <span class="importantNote">4. Tickets price is inclusive of 3 Days 2 Nights stay at selected partner hotels.</span>
                            <span class="importantNote">5. Organizer will assign the partner hotels based on first come first serve basis.</span>
                            <span class="importantNote">6. Ticket price is inclusive of return ferry transfer (Syahbandar Jetty –> Redang Island -> Syahbandar Jetty)</span>
                                <h2>Important Notes</h2>
                                <ol style="padding-left: 15px">
                                    <li>Prices shown excludes RM4.00 AirAsiaRedTix fee.</li>
                                    <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li>
                                    <li>RM5.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                                    <li>Online ticket selling will close 2 days prior to event day, subject to availability.</li>
                                </ol>
                                <h2>For enquiry only:</h2>
                                <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bottompic-mobile hidden-lg hidden-md">
                    <img class="img-responsive" src="images/islandmusicfest2018v2/bottom-bg.png">
                </div>
                <div class="bottompic hidden-xs" style="margin-top:30vh;">
                <img class="img-responsive" src="images/islandmusicfest2018v2/bottom-bg.png">
            </div>
            </section>
            {{-- <div class="bottompic hidden-xs" style="margin-top:30vh;">
                <img class="img-responsive" src="images/islandmusicfest2018v2/bottom-bg.png">
            </div> --}}

        </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 450;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });
    </script>

    {{-- Countdown Timer --}}
    {{-- <script>
        // endtime
        var deadline = 'Nov 30 2017 23:59:00 GMT+0800';

        // calculate time remaining
        function time_remaining(endtime){
            var t = Date.parse(endtime) - Date.parse(new Date());
            var seconds = Math.floor( (t/1000) % 60 );
            var minutes = Math.floor( (t/1000/60) % 60 );
            var hours = Math.floor( (t/(1000*60*60)) % 24 );
            var days = Math.floor( t/(1000*60*60*24) );
            return {'total':t, 'days':days, 'hours':hours, 'minutes':minutes, 'seconds':seconds};
        }

        //output to html
        function run_clock(id,endtime){
            var clock = document.getElementById(id);
            
            // get spans where our clock numbers are held
            var days_span = clock.querySelector('.days');
            var hours_span = clock.querySelector('.hours');
            var minutes_span = clock.querySelector('.minutes');
            var seconds_span = clock.querySelector('.seconds');

            function update_clock(){
                var t = time_remaining(endtime);
                
                // update the numbers in each part of the clock
                days_span.innerHTML = t.days;
                hours_span.innerHTML = ('0' + t.hours).slice(-2);
                minutes_span.innerHTML = ('0' + t.minutes).slice(-2);
                seconds_span.innerHTML = ('0' + t.seconds).slice(-2);
                
                if(t.total<=0){ clearInterval(timeinterval); }
            }
            update_clock();
            var timeinterval = setInterval(update_clock,1000);
        }
        run_clock('clockdiv',deadline);
    </script>
     --}}
@endsection

@section('modal')
    @include('layouts.partials.modals._seatplan')
    @include('layouts.partials.modals._getTixCustom')
@endsection