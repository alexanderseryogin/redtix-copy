{{-- route(@extends('master') --}}
@extends('master')
@section('title')
    Ultra Singapore 2018
@endsection

@section('header')
    @include('layouts.partials._header')
@endsection

@section('content')
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:600|Roboto" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
    <link type="text/css" rel="stylesheet" href="css/custom2.css">
    <link type="text/css" rel="stylesheet" href="css/clock.css">
    <link type="text/css" rel="stylesheet" href="/css/custom/ultra2018.css">

    <!-- Banner Section -->
    <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/ultrasingapore2018/ultra-banner-mock.jpg')}}" style="width: 100%" class="img-responsive" alt="ultrasingapore2018"><a href="#usgIntroduction"><img src="images/ultrasingapore2018/arrow.png" class="center-block arrow img-responsive"></a></div>
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            <img src="{{asset('images/ultrasingapore2018/mobile-splash-3.jpg')}}" style="width: 100%" class="img-responsive" alt="ultrasingapore2018">
            <a href="#usgIntroduction"><img src="images/ultrasingapore2018/arrow.png" class="center-block arrow-mobile img-responsive"></a>
        </div>
    </section>
    <!-- /Banner Section -->

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar section-white" style="padding-top: 0px;" id="usgIntroduction">
            <section class="pageCategory-section section-black last">
                <div class="container tixPrice">
                <div class="row">
                        <div class="col-sm-offset-1 col-sm-10" style="text-align:center;">
                            <h6 style="color:#fff;">Ultra Singapore 2018</h6>
                            <p>Friday, 15 June 2018 - Saturday, 16 June 2018</p>
                            <p>Marina Bay Sands, Singapore</p>
                            <br>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10">
<!--                            <img class="whiteLogo center-block img-responsive" src="images/ultrasingapore2018/logo-3.png"> -->
                            <h6 class="text-center" style="margin-top: 40px; color: #fff;">ESCAPE THE ORDINARY. BE ULTRA.</h6>
                            <p>The music festival that has been taking the whole world by storm is landing at Singapore. This will be Ultra’s 3rd consecutive year partying at the Lion City. Hordes of ‘Ultranauts’ will travel from all over the globe to enjoy some world-class entertainment and soak up the ever stunning destination while they’re at it. Be ready for 2 days of explosive energy from the people, to the DJs on stage.</p>
                            <p>Wave your national flag and tell 45,000 revellers which part of the earth are you from. Enjoy the best of EDM from the melting pot of culture for Ultra. Leave all your worries behind when the beat drops, and enjoy the unbelievable energy from the Ultra family.</p>
                            <p>Ultra is the biggest party in the world, and you Ultranauts are the stars. Lights will guide you, music will revive you and fireworks will captivate you at beautiful Singapore with Ultra.</p>
                            <div class="embed-responsive embed-responsive-16by9" style="margin-top: 40px; margin-bottom: 40px;">
                                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/FZp6paQmCN0" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            {{-- <section class="pageCategory-section last">
                <div class="container intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                           <img class="longLogo center-block img-responsive" src="images/ultrasingapore2018/logo-1.png">
                            <div class="text-center">
                                <h1 class="subSecTitle"><strong>LATEST NEWS</strong></h1>    
                            </div>

                            <div class="col-sm-12 col-xs-12">
                                
                                <div class="col-sm-4 col-xs-12 cardWrapper">
                                    <div class="card center-block">
                                        <img class="card-img-top img-responsive" src="images/ultrasingapore2018/card1.jpg" alt="Card image cap">
                                        <div class="card-body">
                                            <h6 class="card-title">ULTRA SINGAPORE: REMIXED INTO THE WEEKEND</h6>
                                            <p class="card-text">The second edition of Ultra Singapore takes place this weekend! Prepare for the event with these official …</p>
                                            <a href="https://ultramusicfestival.com/worldwide/prepare-ultra-singapore-weekend-apple-music-playlists/" class="readMore">Read More <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </div>
                                   
                                <div class="col-sm-4 col-xs-12 cardWrapper">
                                    <div class="card center-block">
                                        <img class="card-img-top img-responsive" src="images/ultrasingapore2018/card2.jpg" alt="Card image cap">
                                        <div class="card-body">
                                            <h6 class="card-title">ULTRA MUSIC FESTIVAL’S TWENTIETH ANNIVERSARY</h6>
                                            <p class="card-text">Having welcomed over one million fans to forty-five events internationally in 2017, the world’s largest …</p>
                                            <a href="https://ultrasingapore.com/worldwide/ultra-music-festivals-twentieth-anniversary/" class="readMore">Read More <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-4 col-xs-12 cardWrapper">
                                    <div class="card center-block">
                                        <img class="card-img-top img-responsive" src="images/ultrasingapore2018/card3.jpg" alt="Card image cap">
                                        <div class="card-body">
                                            <h6 class="card-title">ULTRA SINGAPORE: OFFICIAL AFTER MOVIE 2016</h6>
                                            <p class="card-text">A 2 day event boasting the world's top EDM DJ's with unparalleled stage designs and top tier production …</p>
                                            <a href="https://umfworldwide.com/media/umf-films/" class="readMore">Read More <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>

                            <div class="col-sm-12 col-xs-12" style="padding-top: 40px;">
                                <a class="btn ultraBtn center-block" id="buyButton" datetime="Feb 10 2018 00:00:00 GMT+0800" target="_blank" href="https://ultrasingapore.com/news/">MORE NEWS</a>   
                            </div>

                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </section> --}}
           {{--  <section class="pageCategory-section last">
                <img class="img-responsive" src="images/ultrasingapore2018/sponsor.jpg" style="width: 100%; height: auto;">
            </section> --}}

            <section class="pageCategory-section last">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                           {{-- <img class="longLogo center-block img-responsive" src="images/ultrasingapore2018/logo-1.png" style="margin-top: 70px;"> --}}
                            <div class="text-center col-sm-12 col-xs-12">
                                <h1 class="subSecTitle"><strong>CONCERT GALLERY</strong></h1>
                            </div>
                            <div class="row hidden-xs">
                                <div class="col-sm-12 text-center pd-0">
                                    <div class="col-sm-8 pd-0">
                                        <div class="col-sm-6 collageImg">
                                            <a href="images/ultrasingapore2018/gallery1-1.png" data-featherlight="image"><img class="img-responsive" src="images/ultrasingapore2018/gallery1-1.png" alt=""></a>
                                        </div>
                                        <div class="col-sm-6 collageImg">
                                            <a href="images/ultrasingapore2018/gallery1-2.png" data-featherlight="image"><img class="img-responsive" src="images/ultrasingapore2018/gallery1-2.png" alt=""></a>
                                        </div>
                                        <div class="col-sm-12 collageImg">
                                            <a href="images/ultrasingapore2018/gallery4.png" data-featherlight="image"><img class="img-responsive" src="images/ultrasingapore2018/gallery4.png" alt=""></a>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 pd-0">
                                        <div class="col-sm-12 collageImg">
                                            <a href="images/ultrasingapore2018/gallery3.png" data-featherlight="image"><img class="img-responsive" src="images/ultrasingapore2018/gallery3.png" alt=""></a>
                                        </div>  
                                        <div class="col-sm-12 collageImg">
                                            <a href="images/ultrasingapore2018/gallery5.png" data-featherlight="image"><img class="img-responsive" src="images/ultrasingapore2018/gallery5.png" alt=""></a>
                                        </div>  
                                    </div>
                                </div>
                                <div class="col-sm-12 text-center pd-0">
                                    <div class="col-sm-12 pd-0">
                                        <div class="col-sm-4 collageImg">
                                            <a href="images/ultrasingapore2018/gallery6.png" data-featherlight="image"><img class="img-responsive" src="images/ultrasingapore2018/gallery6.png" alt=""></a>
                                        </div>
                                        <div class="col-sm-8 collageImg">
                                            <a href="images/ultrasingapore2018/gallery7.png" data-featherlight="image"><img class="img-responsive" src="images/ultrasingapore2018/gallery7.png" alt=""></a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="gallery text-center hidden-lg hidden-md hidden-sm">
                                <div class="swiper-container">
                                    <div class="swiper-wrapper">
                                        <div class="swiper-slide">
                                            <a href="images/ultrasingapore2018/gallery1-1.png" data-featherlight="image"><img src="images/ultrasingapore2018/gallery1-1.png" alt=""></a>
                                        </div>
                                        <div class="swiper-slide">
                                            <a href="images/ultrasingapore2018/gallery1-2.png" data-featherlight="image"><img src="images/ultrasingapore2018/gallery1-2.png" alt=""></a>
                                        </div>
                                        <div class="swiper-slide">
                                            <a href="images/ultrasingapore2018/gallery4.png" data-featherlight="image"><img src="images/ultrasingapore2018/gallery4.png" alt=""></a>
                                        </div>
                                        <div class="swiper-slide">
                                            <a href="images/ultrasingapore2018/gallery3.png" data-featherlight="image"><img src="images/ultrasingapore2018/gallery3.png" alt=""></a>
                                        </div>
                                        <div class="swiper-slide">
                                            <a href="images/ultrasingapore2018/gallery5.png" data-featherlight="image"><img src="images/ultrasingapore2018/gallery5.png" alt=""></a>
                                        </div>
                                        <div class="swiper-slide">
                                            <a href="images/ultrasingapore2018/gallery6.png" data-featherlight="image"><img src="images/ultrasingapore2018/gallery6.png" alt=""></a>
                                        </div>
                                        <div class="swiper-slide">
                                            <a href="images/ultrasingapore2018/gallery7.png" data-featherlight="image"><img src="images/ultrasingapore2018/gallery7.png" alt=""></a>
                                        </div>
                                    </div>

                                    <div class="swiper-pagination"></div>

                                    <div class="swiper-button-next swiper-button-white"></div>
                                    <div class="swiper-button-prev swiper-button-white"></div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </section>

            <section class="pageCategory-section last hidden-xs" style="margin-top: 40px; margin-bottom: 40px;">
                <img class="img-responsive" src="images/ultrasingapore2018/middle.png" style="width: 100%; height: auto;">
            </section>


            <section class="pageCategory-section last" id="ticketSection">
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10">
                            <div class="text-center col-sm-12 col-xs-12">
                                <h1 class="subSecTitle"><strong>TICKETS</strong></h1>
                            </div>
                            <div class="clearfix"></div>
                            {{-- dekstop --}}
                            <div class="col-sm-12 hidden-xs hidden-md hidden-sm">
                                <div class="col-sm-3">
                                    <button data-toggle="tab" href="#ticket_only" class="tixOptBtn btn active"><i class="fa fa-ticket"></i>TICKET ONLY</button>
                                </div>
                                <div class="col-sm-4">
                                    <button data-toggle="tab" href="" class="tixOptBtn btn disabled"><i class="fa fa-ticket"></i><i class="fa fa-plane"></i>TICKET + FLIGHT</button>
                                </div>
                                <div class="col-sm-5">
                                    <button data-toggle="tab" href="" class="tixOptBtn btn disabled"><i class="fa fa-ticket"></i><i class="fa fa-plane"></i><i class="fa fa-building-o"></i>TICKET + FLIGHT + HOTEL</button>
                                </div>
                            </div>
                            {{-- mobile --}}
                            <div class="col-xs-12 hidden-lg pd-0">
                               {{-- <div class="col-xs-12 ps-9">
                                    <button data-toggle="tab" href="#ticket_only" class="tixOptBtn btn">
                                        <i class="fa fa-ticket"></i>
                                        <p>TICKET ONLY</p>
                                    </button>
                                </div>
                                <div class="col-xs-4 ps-9">
                                    <button data-toggle="tab" href="" class="tixOptBtn btn disabled">
                                        <i class="fa fa-ticket"></i>
                                        <i class="fa fa-plane"></i>
                                        <p>TICKET + FLIGHT</p>
                                    </button>
                                </div>
                                <div class="col-xs-5 ps-9">
                                    <button data-toggle="tab" href="" class="tixOptBtn btn disabled">
                                        <i class="fa fa-ticket"></i>
                                        <i class="fa fa-plane"></i>
                                        <i class="fa fa-building-o"></i>
                                        <p>TICKET + FLIGHT + HOTEL</p>
                                    </button>
                                </div> --}}
                            </div>
                            <div class="tab-content">
                                <div id="ticket_only" class="tab-pane fade in active">
                                <div class="col-xs-12">
                                    <p style="font-size: 24px; padding-top: 80px;" class="text-center hidden-xs"><img src="images/ultrasingapore2018/livefresh.png"><strong> Cardmember</strong></p>
                                    <p style="font-size: 24px; padding-top: 80px;" class="text-center hidden-lg hidden-md hidden-sm"><img class="img-responsive center-block" src="images/ultrasingapore2018/livefresh.png"><strong> Cardmember</strong></p>
                                </div>
                                    <div class="col-sm-12">
                                        @foreach($ticket_only as $ticket)
                                            @if($ticket["Card Type"] == "LF")                                        
                                        {{--         @include('layouts.components.ultraTicketOnly') --}}

{{-- ticket only --}}
<div class="col-sm-12 col-xs-12 ticketRow hidden-xs hidden-sm">
    {{-- <small class="discountTag"> 40% OFF</small> --}}
    <div class="col-sm-2 text-center ticketTitle">
        <div class="row">
            {{-- <p>Premium</p> --}}
            <div class="clearfix" style="height: 10px;">&nbsp;</div>
            <h1>{{ $ticket["Ticket Type"] }}</h1>    
        </div> 
    </div>
    <div class="col-sm-3 text-center ticketDay border-left">
        <h6>{{ $ticket["Ticket Name"] }}</h6>
    </div>
    <div class="col-sm-4 ticketAdmission border-left">
        <p>{{ $ticket["Ticket Description"] }}</p>
    </div>
    <div class="col-sm-3 ticketBuy border-left">
        <div class="col-sm-12 text-center">
            @if(strlen($ticket["Private Sale Front End"]) !== 0)
                <h6>{{ $ticket["Private Sale Front End"] }}</h6>
                {{-- @if(strlen($ticket["Ticketserv URL"]) !== 0) --}}
                <a class="btn ultraBuyBtn center-block" data-toggle="modal" data-target="#buyModal{{ $ticket["Sorting ID"] }}">BUY NOW</a>
                {{-- <a class="btn ultraBuyBtn center-block" href="{{ $ticket["Ticketserv URL"] }}">BUY NOW</a> --}}
                {{-- @endif --}}
                <p class="small" style="line-height: 1.3; font-size: 80%;">Ticketing fee, event insurance & credit card charges will be added at the checkout basket</p>
            @else
                <div class="clearfix" style="height:30px;">&nbsp;</div>
                <h6>N/A</h6>
            @endif
           {{--  <h6>{{ $ticket["Private Sale Front End"] }}</h6>
            <a class="btn ultraBuyBtn center-block" id="buyButton" datetime="Feb 10 2018 00:00:00 GMT+0800" target="_blank" href="{{ $ticket["Ticketserv URL"] }}">BUY NOW</a>
            <small>10 tickets left at this tier</small> --}}
        </div>
    </div>
</div>


<!-- mobile ticket ONLY card responsive -->
<div class="col-xs-12 col-sm-6 hidden-lg hidden-md ticketCard">
    {{-- <small class="discountTag"> 40% OFF</small> --}}
    <div class="col-xs-12 card-body pd-0">
        <div class="col-xs-12 cardTop">
            <div class="col-xs-6 pd-0 ticketTitle">
                <p class="ticketFlight">{{ $ticket["Ticket Name"] }}</p>
                <p class="ticketTier">Tier 1<span class="ticketCategory">{{ $ticket["Ticket Type"] }}</span></p>
            </div>
            <div class="col-xs-6 ticketPrice">
                @if(strlen($ticket["Private Sale Front End"]) !== 0)
                   {{-- <p class="pull-right text-right">{{ $ticket["Private Sale Front End"] }}<br><small>10 tickets left at this tier</small></p> --}}
                   <p class="pull-right text-right">{{ $ticket["Private Sale Front End"] }}</p>
                @else
                    <p class="pull-right text-right">N/A</p>
                @endif
            </div>
        </div>
        <div class="cardBottom col-xs-12 text-center">
            <p>{{ $ticket["Ticket Description"] }}</p>
            {{-- @if(strlen($ticket["Ticketserv URL"]) !== 0) --}}
                <a class="btn ultraBuyBtn center-block" data-toggle="modal" data-target="#buyModal{{ $ticket["Sorting ID"] }}">BUY NOW</a>
                {{-- <a class="btn ultraBuyBtn center-block" id="buyButton" datetime="Feb 10 2018 00:00:00 GMT+0800" target="_blank" href="{{ $ticket["Ticketserv URL"] }}">BUY NOW</a> --}}
            {{-- @endif --}}
        </div>
    </div>
</div>
                                            @endif
                                        @endforeach



                                    </div>
                                    <div class="col-xs-12">
                                        <p style="font-size: 24px; padding-top: 80px;" class="text-center hidden-xs"><strong><img src="images/ultrasingapore2018/posbdbs.png"> Cardmember</strong></p>
                                        <p style="font-size: 24px; padding-top: 80px;" class="text-center hidden-lg hidden-md hidden-sm"><strong><img class="img-responsive center-block" src="images/ultrasingapore2018/posbdbs.png"> Cardmember</strong></p>
                                    </div>
                                    <div class="col-sm-12">
                                     @foreach($ticket_only as $ticket)
                                            @if($ticket["Card Type"] == "DBS")                                        
 {{--                                               @include('layouts.components.ultraTicketOnly') --}}
    

{{-- ticket only --}}
<div class="col-sm-12 col-xs-12 ticketRow hidden-xs hidden-sm">
    {{-- <small class="discountTag"> 40% OFF</small> --}}
    <div class="col-sm-2 text-center ticketTitle">
        <div class="row">
            {{-- <p>Premium</p> --}}
            <div class="clearfix" style="height: 10px;">&nbsp;</div>
            <h1>{{ $ticket["Ticket Type"] }}</h1>    
        </div> 
    </div>
    <div class="col-sm-3 text-center ticketDay border-left">
        <h6>{{ $ticket["Ticket Name"] }}</h6>
    </div>
    <div class="col-sm-4 ticketAdmission border-left">
        <p>{{ $ticket["Ticket Description"] }}</p>
    </div>
    <div class="col-sm-3 ticketBuy border-left">
        <div class="col-sm-12 text-center">
            @if(strlen($ticket["Private Sale Front End"]) !== 0)
                <h6>{{ $ticket["Private Sale Front End"] }}</h6>
                {{-- @if(strlen($ticket["Ticketserv URL"]) !== 0) --}}
                <a class="btn ultraBuyBtn center-block" data-toggle="modal" data-target="#buyModal{{ $ticket["Sorting ID"] }}">BUY NOW</a>
                {{-- <a class="btn ultraBuyBtn center-block" href="{{ $ticket["Ticketserv URL"] }}">BUY NOW</a> --}}
                {{-- @endif --}}
                <p class="small" style="line-height: 1.3; font-size: 80%;">Ticketing fee, event insurance & credit card charges will be added at the checkout basket</p>
            @else
                <div class="clearfix" style="height:30px;">&nbsp;</div>
                <h6>N/A</h6>
            @endif
           {{--  <h6>{{ $ticket["Private Sale Front End"] }}</h6>
            <a class="btn ultraBuyBtn center-block" id="buyButton" datetime="Feb 10 2018 00:00:00 GMT+0800" target="_blank" href="{{ $ticket["Ticketserv URL"] }}">BUY NOW</a>
            <small>10 tickets left at this tier</small> --}}
        </div>
    </div>
</div>


<!-- mobile ticket ONLY card responsive -->
<div class="col-xs-12 col-sm-6 hidden-lg hidden-md ticketCard">
    {{-- <small class="discountTag"> 40% OFF</small> --}}
    <div class="col-xs-12 card-body pd-0">
        <div class="col-xs-12 cardTop">
            <div class="col-xs-6 pd-0 ticketTitle">
                <p class="ticketFlight">{{ $ticket["Ticket Name"] }}</p>
                <p class="ticketTier">Tier 1<span class="ticketCategory">{{ $ticket["Ticket Type"] }}</span></p>
            </div>
            <div class="col-xs-6 ticketPrice">
                @if(strlen($ticket["Private Sale Front End"]) !== 0)
                   {{-- <p class="pull-right text-right">{{ $ticket["Private Sale Front End"] }}<br><small>10 tickets left at this tier</small></p> --}}
                @else
                    <p class="pull-right text-right">N/A</p>
                @endif
            </div>
        </div>
        <div class="cardBottom col-xs-12 text-center">
            <p>{{ $ticket["Ticket Description"] }}</p>
            {{-- @if(strlen($ticket["Ticketserv URL"]) !== 0) --}}
                 <a class="btn ultraBuyBtn center-block" data-toggle="modal" data-target="#buyModal{{ $ticket["Sorting ID"] }}">BUY NOW</a>
                {{-- <a class="btn ultraBuyBtn center-block" id="buyButton" datetime="Feb 10 2018 00:00:00 GMT+0800" target="_blank" href="{{ $ticket["Ticketserv URL"] }}">BUY NOW</a> --}}
            {{-- @endif --}}
        </div>
    </div>
</div>




                                            @endif

{{--                                            @if($ticket["Card Type"] == "POSB")                                        
                                                @include('layouts.components.ultraTicketOnly')
                                            @endif  --}}
                                        @endforeach


                                    </div>
                                </div>

                                <div id="ticket_flight" class="tab-pane fade">

                                    <div class="col-sm-12 col-xs-12" style="margin-top: 40px;">
                                        <p>Select the country you will be flying from*:</p>
                                        <select class="selectpicker" id="select-country">
                                            <option value="1">KUALA LUMPUR</option>
                                            <option value="2">BANGKOK</option>
                                            <option value="3">JAKARTA</option>
                                            <option value="4">PERTH</option>
                                            <option value="5">MELBOURNE</option>
                                            <option value="6">KUCHING</option>
                                            <option value="7">PENANG</option>
                                        </select>
                                    </div>
                                    
                                    <div id="country-1" class="tab-content flightTab">
                                        @foreach($ticket_flight as $ticket)
                                            @if($ticket["From City"] == "Kuala Lumpur") 
                                                @include('layouts.components.ultraFlight')
                                            @endif
                                        @endforeach
                                    </div>
                                    <div id="country-2" class="tab-content flightTab">
                                        @foreach($ticket_flight as $ticket)
                                            @if($ticket["From City"] == "Bangkok")
                                                @include('layouts.components.ultraFlight')
                                            @endif
                                        @endforeach
                                    </div>
                                    <div id="country-3" class="tab-content flightTab">
                                        @foreach($ticket_flight as $ticket)
                                            @if($ticket["From City"] == "Jakarta")
                                                @include('layouts.components.ultraFlight')
                                            @endif
                                        @endforeach
                                    </div>
                                    <div id="country-4" class="tab-content flightTab">
                                        @foreach($ticket_flight as $ticket)
                                            @if($ticket["From City"] == "Perth")
                                                @include('layouts.components.ultraFlight')
                                            @endif
                                        @endforeach
                                    </div>
                                    <div id="country-5" class="tab-content flightTab">
                                        @foreach($ticket_flight as $ticket)
                                            @if($ticket["From City"] == "Melbourne")
                                                @include('layouts.components.ultraFlight')
                                            @endif
                                        @endforeach
                                    </div>
                                    <div id="country-6" class="tab-content flightTab">
                                        @foreach($ticket_flight as $ticket)
                                            @if($ticket["From City"] == "Kuching")
                                                @include('layouts.components.ultraFlight')
                                            @endif
                                        @endforeach
                                    </div>
                                    <div id="country-7" class="tab-content flightTab">
                                        @foreach($ticket_flight as $ticket)
                                            @if($ticket["From City"] == "Penang")
                                                @include('layouts.components.ultraFlight')
                                            @endif
                                        @endforeach
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="pageCategory-section last">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                        <div class="text-center col-sm-12 col-xs-12">
                            <p>Don't have the Card? Get your DBS Live Fresh Card here: <a href="http://dbs.com.sg/livefresh">dbs.com.sg/livefresh</a></p>
                            <p>This Exclusive Sale is for DBS Live Fresh cardmembers and DBS/POSB cardmembers. We reserve the right to cancel and refund any transactions that were not purchased with a DBS card.</p>
                        </div>
                        </div>
                    </div>
                </div>
            </section>


            <section class="pageCategory-section last bottom">
                <img src="images/ultrasingapore2018/white-logo1.png" class="img-responsive logoBottom hidden-xs"> 
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                            <div class="note text-left">
                                <h4>please contact us at <a href="mailto:ultra@airasiaredtix.com">ultra@airasiaredtix.com</a> if you have any further questions for custom packages</h4>
                                
                                <h2>For enquiry only:</h2>
                                <p>Email to <a href="mailto:ultra@airasiaredtix.com">ultra@airasiaredtix.com</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });
    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 450;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });

/*    $(function() {
        $(".buy_form").submit(function(){
            alert("am here");
            return false;
        });
    })
*/
    </script>


    {{-- Buy button disable --}}
    <script type="text/javascript">
        $(function() {
            $('a[id^=buyButton]').each(function() {
                var date = new Date();
                var enddate = $(this).attr('datetime'); 
                if ( Date.parse(date) >= Date.parse(enddate)) {
                  $(this).addClass('disabled');
                }
            });
        });
    </script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>

    <script type="text/javascript">
        $('.flightTab').hide();
        //show the first tab content
        $('#country-1').show();

        $('#select-country').change(function () {
           dropdown = $('#select-country').val();
          //first hide all tabs again when a new option is selected
          $('.flightTab').hide();
          //then show the tab content of whatever option value was selected
          $('#' + "country-" + dropdown).show();                                    
        });
    </script>
    @if(!empty(Session::get('error_code')) && Session::get('error_code') == 5)
        <script>
        $(function() {
            $('#errorModal').modal('show');
        });
        </script>
    @endif
    @if(!empty(Session::get('success_lf')))
        <script>
        $(function() {
            $('#successlfModal{{ session()->get('ticket_id') }}').modal('show');
        });
        </script>
    @endif

    @if(!empty(Session::get('success_dbs')))
        <script>
        $(function() {
            $('#successdbsModal{{ session()->get('ticket_id') }}').modal('show');
        });
        </script>
    @endif
    
@endsection

@section('modal')
    @include('layouts.partials.modals._seatplan')
    @include('layouts.partials.modals._getTixCustom')
{{-- @foreach($ticket_flight as $ticket)
    <div id="flightModal{{ $ticket["Sorting ID"] }}" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-body text-center">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
                <h1><i class="fa fa-bullhorn text-warning" aria-hidden="true"></i></h1>
                <h4>Flight Dates Available</h4>
                <div class="clearfix">&nbsp;</div>
                <div class="well">
                <p>Depart ({{ $ticket["From City"] }} - Singapore):</p>
                <p>{{ date(' D, M j Y', strtotime($ticket["Outbound Departure Date"])).", ". date('h:i a', strtotime($ticket["Outbound Departure Time"])) }}</p>
                <p>Return (Singapore - {{ $ticket["From City"] }}):</p>
                <p>{{ date(' D, M j Y', strtotime($ticket["Inbound Departure Date"])).", ". date('h:i a', strtotime($ticket["Inbound Departure Time"])) }}</p>
                </div>
            </div>
          </div>
        </div>
    </div>
@endforeach --}}
@foreach($ticket_only as $ticket)

    <div id="buyModal{{ $ticket["Sorting ID"] }}" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-body text-center pd-0">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="padding: 16px;"><i class="fa fa-times" aria-hidden="true"></i></button>
                <div class="row ultraModalRow">
                    <div class="col-sm-5 hidden-xs">
                        <img class="img-responsive" src="images/ultrasingapore2018/modal-picture.jpg" style="height: auto; width: 100%;">
                    </div>
                    <div class="col-sm-7 text-center ultraform">
                        {{-- <p>Please can you enter in the <b>first 6 digits</b> of  your card number to see if you are entitled to an extra discount</p> --}}
                        {{-- <p>Please can you enter your card number to see if you are entitled to an extra discount</p> --}}
                        <p>Please can you enter in the <b>first 10 digits</b> of your card number to see if you are entitled to an extra discount</p>
                        <form action="{{route('ultra')}}" method="get" enctype="multipart/form-data" class="buy_form">
                            {{ csrf_field() }}
<input type='hidden' name='ticket_id' value='{{$ticket['Sorting ID']}}'>
                            <div class="form-group center-block">
                                <input id="bin_num" type="number" class="form-control" name="bin_num" autofocus required="required" placeholder="00000000000">
                            </div>
                            <div class="form-group center-block">
                                <button class="btn ultraBuyBtn" type="submit">SUBMIT</button>
                            </div>
                        </form>
                    </div>
                </div> 
            </div>
          </div>
        </div>
    </div>

@endforeach

<div id="successlfModal{{ $ticket1["Sorting ID"] }}" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-body text-center pd-0">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="padding: 16px;"><i class="fa fa-times" aria-hidden="true"></i></button>
            <div class="row ultraModalRow">
                <div class="col-sm-5 hidden-xs">
                    <img class="img-responsive" src="images/ultrasingapore2018/modal-picture.jpg" style="height: auto; width: 100%;">
                </div>
                <div class="col-sm-7 text-center ultraform">
                    {{-- <p>{{ $ticket["Private Sale Front End"] }}</p> --}}
                    <h6>Congratulations!</h6>
                    <p>As a LiveFresh Cardmember, you are entitled to purchase a {{ $ticket1["Ticket Name"] }} {{ $ticket1["Ticket Type"] }} ticket at {{ $ticket1["Private Sale Front End"] }}</p>
                    <a class="btn ultraBuyBtn center-block" href="{{ $ticket1["Ticketserv URL"] }}">BUY TICKET</a>
                    <small>Price excludes ticketing fee, Event Protect insurance & credit card charges</small>
                </div>
            </div> 
        </div>
      </div>
    </div>
</div>

<div id="successdbsModal{{ $ticket1["Sorting ID"] }}" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-body text-center pd-0">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="padding: 16px;"><i class="fa fa-times" aria-hidden="true"></i></button>
            <div class="row ultraModalRow">
                <div class="col-sm-5 hidden-xs">
                    <img class="img-responsive" src="images/ultrasingapore2018/modal-picture.jpg" style="height: auto; width: 100%;">
                </div>
                <div class="col-sm-7 text-center ultraform">
                    {{-- <p>{{ $ticket["Private Sale Front End"] }}</p> --}}
                    <h6>Congratulations!</h6>
                    <p>As a DBS/POSB Cardmember, you are entitled to purchase a {{ $ticket1["Ticket Name"] }} {{ $ticket1["Ticket Type"] }} ticket at {{ $ticket1["Private Sale Front End"] }}</p>
                    <a class="btn ultraBuyBtn center-block" href="{{ $ticket1["Ticketserv URL"] }}">BUY TICKET</a>
                    <small>Price excludes ticketing fee, Event Protect insurance & credit card charges</small>
                </div>
            </div> 
        </div>
      </div>
    </div>
</div>


<div id="errorModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-body text-center pd-0">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="padding: 16px;"><i class="fa fa-times" aria-hidden="true"></i></button>
            <div class="row ultraModalRow">
                <div class="col-sm-5 hidden-xs">
                    <img class="img-responsive" src="images/ultrasingapore2018/modal-picture.jpg" style="height: auto; width: 100%;">
                </div>
                <div class="col-sm-7 text-center ultraform">
                    {{-- <p>Please can you enter in the <b>first 6 digits</b> of  your card number to see if you are entitled to an extra discount</p> --}}
                    {{-- <p>Sorry! you are not a DBS cardholder and cannot purchase a ticket now and will have to wait until the tickets go on sale publicly or alternatively you can sign up to a DBS LiveFresh card now.</p> --}}
                    <p>Sorry! This exclusive card sale is for DBS cardmembers only. To make sure you secure your ULTRA SINGAPORE 2018 Tickets please register below!</p>
                    <a class="btn ultraBuyBtn center-block" href="">REGISTER</a>
                    </form>
                </div>
            </div> 
        </div>
      </div>
    </div>
</div>


@endsection
