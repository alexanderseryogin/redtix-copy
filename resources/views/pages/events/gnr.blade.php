@extends('master')
@section('title')
    GUNS N' ROSES - NOT IN THIS LIFETIME TOUR - LIVE IN SINGAPORE 2017
@endsection

@section('header')
    @include('layouts.partials._header')
@endsection

@section('content')

    <!-- Banner Section -->
    {{-- <section class="innerPageBanner">
      <div class="bigBanner-overlay"></div>
      <div class="jumbotron eventBanner" style="background-image: url('{!! asset('images/gnr/gnr-banner.jpg') !!}')">
        <video id="video-background" class="hidden-xs" preload muted autoplay loop>
          <source src="images/gnr/gnr-vidbanner.mp4" type="video/mp4">
        </video>
      </div>
    </section> --}}<!-- /Banner Section -->

    <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        <div class="jumbotron eventBanner hidden-xs" style="background-image: url('images/gnr/gnr-banner.jpg')"></div>
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            <img src="{{asset('images/gnr/gnr-thumb.jpg')}}" style="width: 100%" class="img-responsive">
        </div>
    </section>

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
          <div class="row priceNbtn">
            <div class="col-sm-offset-1 col-sm-10">
              <div class="row">
                <div class="col-sm-9 leftBox">
                  <h6>GUNS N' ROSES - NOT IN THIS LIFETIME TOUR - LIVE IN SINGAPORE 2017</h6>
                  Tickets from <span>RM610</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  
                  {{-- <span class="text text-danger animated infinite flash" style="animation-duration: 2s; -webkit-animation-duration: 2s; -moz-animation-duration: 2s; -ms-animation-duration: 2s;"><strong>Limited Tikets Available</strong></span> --}}
                </div>
                <div class="col-sm-3 text-center">
                  <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                </div>
              </div>
            </div>
          </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
      <div class="mainBodyContent no-btm-mar section-white">

        <section class="pageCategory-section last">
          
          <div class="container intro">
            <div class="row">
              <div class="col-sm-offset-1 col-sm-10 leftBar">
                <!--<h2>Title</h2>-->
                <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i> February 25, 2017</div>
                <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i> Changi Exhibition Centre, Changi Exhibition Centre 9 Aviation Park Road S (498760)  <a target="_blank" href="https://goo.gl/maps/uQ4z6Zpeni42">View Map</a></div>
                <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> 8:00 pm</div>
                <div class="clearfix">&nbsp;</div>
                <!-- Go to www.addthis.com/dashboard to customize your tools -->
                <div class="addthis_inline_share_toolbox"></div>
                <!-- /sharing -->
                <hr>
                <p>LAMC Productions is proud to announce that Guns N' Roses will play for the first time in Singapore at Changi Exhibition Centre on Saturday, 25 February, 2017 with special guests Wolfmother and Tyler Bryant & The Shakedown!</p>
                <p>Founder Axl Rose and former members, Slash and Duff McKagan have regrouped for one of the century's most anticipated world tours. The Not In This Lifetime Tour just wrapped up in North America to rave reviews and sold out stadiums. </p>
                <p>Guns N' Roses is one of the bestselling hard rock bands of all time, their massive popularity is highlighted by their unparalleled chart success since 1987. Following the group's 1985 formation, Guns N' Roses injected unbridled, unrivaled, and unstoppable attitude into the burgeoning Los Angeles rock scene. The spirit went on to captivate the entire world with the release of their 1987 debut Appetite for Destruction - the best-selling U.S. debut ever, moving 30 million copies globally. In 1991, the seven-time platinum Use Your Illusion I and Use Your Illusion II occupied the top two spots of the Billboard Top 200 upon release. </p>
                <p>Over the course of the past decade, Guns N Roses have performed sold out shows and headlined festivals worldwide following the critically acclaimed release of 2008's RIAA platinum-certified Chinese Democracy. Six studio albums later, Guns N' Roses are one of the most important and influential acts in music history and continue to set the benchmark for live performances connecting with millions of fans across the globe. </p>
                <p>Guns N' Roses' are Axl Rose, Duff McKagan (bass), Slash (lead guitar), Dizzy Reed (keyboard), Richard Fortus (rhythm guitar), Frank Ferrer (drums), and Melissa Reese (keyboard). </p>
                <p>Guns N' Roses will not be performing in nearby countries so everyone across the region should make their plans to be in Singapore on Saturday, 25 February, 2017 to witness the concert event of the century! </p>
                <h2>Rating / Age Limit</h2>
                <ul>
                    <li>No admission for infants in arms and children below 12 years old</li>
                    <li>Children 12 years and above must purchase a ticket for entry</li>
                    <li>Admission is subject to tickets produced at the entrance</li>
                </ul>
                
                <h2>Photography / Video Recording Rules</h2>
                <ul>
                    <li>No Photography, Video recording and Audio recording is permitted for this event</li>
                </ul>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>
        </section>

        <section class="pageCategory-section last section-grey">
          <div class="container">
            <div class="gallery text-center">
            <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
              <!-- Swiper -->
              <div class="swiper-container">
                <div class="swiper-wrapper">
                  <div class="swiper-slide">
                    <iframe width="560" height="100%" src="https://www.youtube.com/embed/uImFjHf4aoE" frameborder="0" allowfullscreen></iframe>
                  </div>
                  <div class="swiper-slide">
                    <iframe width="560" height="100%" src="https://www.youtube.com/embed/bjJ7KCBykVU" frameborder="0" allowfullscreen></iframe>
                  </div>
                  <div class="swiper-slide">
                    <iframe width="560" height="100%" src="https://www.youtube.com/embed/x6gjFkdrXbo" frameborder="0" allowfullscreen></iframe>
                  </div>
                </div>
                <!-- Add Pagination -->
                <div class="swiper-pagination"></div>
                <!-- Add Arrows -->
                <div class="swiper-button-next swiper-button-white"></div>
                <div class="swiper-button-prev swiper-button-white"></div>
              </div><!-- /Swiper -->
            </div>
          </div>
        </section>

        <section class="pageCategory-section last"><a id="anchorPrice"></a>
          <div class="container tixPrice">
            <div class="row">
              <div class="col-sm-offset-1 col-sm-10 ">
                <div class="text-center">
                  <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                  <p>Select ticket</p>
                  {{-- <p class="text text-danger animated infinite flash" style="animation-duration: 2s; -webkit-animation-duration: 2s; -moz-animation-duration: 2s; -ms-animation-duration: 2s;"><strong>Limited Tikets Available</strong></p> --}}
                </div>
                <div class="clearfix">&nbsp;</div>
                <div class="table-responsive">
                  <table class="table infoTable-D table-bordered">
                      <thead>
                        <tr>
                          <th>Area / Zone</th>
                          <th>Price</th>
                          <th>Remarks</th>
                          <th>Seating Plan</th>
                        </tr>
                      </thead>
                      <tbody>
                          <tr>
                              <td><span class="box midblue">Pen A</span></td>
                              <td><strike>RM 955</strike> RM 910</td>
                              <td rowspan="2">
                              <p>- General admission</p>
                              <p>- Free standing</p>
                              </td>
                              <td rowspan="6"><img class="img-responsive seatPlanImg" src="images/gnr/GNRMap.jpg" alt="">
                              <small>Click image to enlarge</small></td>
                          </tr>
                          <tr>
                              <td><span class="box green">Pen B</span></td>
                              <td><strike>RM 640</strike> RM 610</td>
                          </tr>
                          <tr>
                              <td><span class="box midblue">Pen A + 20% <br>AirAsia Promo Code</span></td>
                              <td><strike>RM 955</strike> RM 910</td>
                              <td rowspan="2" align="left">
                                  <p>- General admission</p>
                                  <p>- Free standing</p>
                                  <p class="alert alert-success"><strong>Comes with AirAsia Promo Code for discounted flights</strong></p>
                              </td>
                          </tr>
                          <tr>
                              <td><span class="box green">Pen B + 20% <br>AirAsia Promo Code</span></td>
                              <td><strike>RM 640</strike> RM 610</td>
                          </tr>
                      </tbody>
                  </table>
                </div>
                <div class="buyAlert-bar">
                  <a class="btn btn-danger disabled" onClick="rtBuyTicketsClick()" id="buyButton" target="_blank" href="https://redtix-tickets.airasia.com/en-AU/events/guns%20n'%20roses%20-%20not%20in%20this%20lifetime%20tour%20-%20live%20in%20singapore%202017/2017-2-25_20.00/changi%20exhibition%20centre,%20singapore_?back=2&area=5892141b-1bfb-47f3-9a1e-7207418173eb&type=ga" role="button">BUY TICKETS</a>
                </div>
                <div class="alert alert-danger"><i class="fa fa-info-circle" aria-hidden="true"></i> Online purchase has been closed.</div>
                <div class="note text-left">
                  <h2>Important Notes</h2>
                  <ol>
                    <li>Prices shown include RM4.00 AirAsiaRedTix fee.</li>
                    <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li>
                    <li>RM5.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                    <li>Online ticket selling will close 2 days prior to event day, subject to availability.</li>
                  </ol>
                  <h2>For enquiry only:</h2>
                  <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                </div>
              </div>
            </div>
          </div>
        </section>

      </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 450;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });
    </script>
    
@endsection

@section('modal')

    <!--Modal Seat Plan image-->
    <div class="modal fade" id="enlargeImageModal" tabindex="-1" role="dialog" aria-labelledby="enlargeImageModal" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
            <div class="clearfix">&nbsp;</div>
            <img src="" class="enlargeImageModalSource" style="width: 100%;">
            </div>
        </div>
        </div>
    </div>

    <!-- Modal Where to Get Tix Location-->
    <div id="modalGetTixLoc" class="modal fade" role="dialog">
        <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
            <h2>GET YOUR TICKETS FROM:</h2>
            <dl class="dl-horizontal">
                <dt>Online:</dt>
                <dd><a href="http://www.airasiaredtix.com">www.AirAsiaRedtix.com</a></dd>
            </dl>
            <dl class="dl-horizontal">
                <dt>Rock Corner outlets:</dt>
                <dd>The Gardens, Mid Valley (TEL: 03 - 2201 4893)</dd>
                <dd>Subang Parade (TEL: 03 - 5613 1139)</dd>
                <dd>The Curve (TEL: 03 - 7733 1139)</dd>
            </dl>
            <dl class="dl-horizontal">
                <dt>Victoria Music outlets:</dt>
                <dd>Sg Wang(TEL: 03 - 2148 7208)</dd>
                <dd>Amcorp (TEL: 03 - 7956 0592)</dd>
            </dl>
            <dl class="dl-horizontal">
                <dt>Penang outlets:</dt>
                <dd>Artist Gallery, Gurney Plaza (04-637 0191)</dd>
                <dd>Artist Gallery, Queensbay Mall (04-228 5648)</dd>
            </dl>
            </div>
        </div>
        </div>
    </div>

@endsection