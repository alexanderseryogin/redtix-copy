@extends('master')
@section('title')
    Epizode³
@endsection

@section('header')
    @include('layouts.partials._header')
    <meta property="og:url" content="{{ Request::fullUrl() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Epizode³" />
    <meta property="og:description" content="Sensational Epizode³, the most captivating music and art festival in the world, will be running from December 28th 2018 to January 8th 2019 in Vietnam’s magical Phu Quoc island."/>
    <meta property="og:image" content="{{ Request::Url().'images/epizode2018/thumbnail1.jpg' }}" />
@endsection

@section('content')
    <!-- Banner Section -->
    <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/epizode2018/web-banner1.jpg')}}" style="width: 100%" class="img-responsive" alt="Epizode³"></div>
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            <img src="{{asset('images/epizode2018/thumbnail1.jpg')}}" style="width: 100%" class="img-responsive" alt="Epizode³">
        </div>
    </section>
    <!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
            <div class="row priceNbtn">
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="row">
                        <div class="col-sm-9 leftBox">
                            <h6>Epizode³</h6> Tickets from <span>RM930</span>
                        </div>
                        <div class="col-sm-3 text-center">
                            <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar section-white">
            <section class="pageCategory-section last">
                <div class="container intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i>  28th Dec 2018 - 8th Jan 2019</div>
                            <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i>  Sunset Sanato Beach, Phu Quoc, Vietnam <a target="_blank" href="https://goo.gl/maps/zrmWC45Wha22">View Map</a></div>
                            <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> 4.00pm, 28th Dec 2018 - 7.00 pm, 8th Jan 2019</div>
                            <div class="clearfix">&nbsp;</div>
                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <div class="addthis_inline_share_toolbox"></div>
                            <!-- /sharing -->
                            <div class="col-sm-12">
                                <hr>
                                <p><h2>Epizode³</h2><br/>
                                Welcome to Epizode³ - electrifying music and art universe!</p>
                                <p>Sensational Epizode³, the most captivating music and art festival in the world, will be running from December 28th 2018 to January 8th 2019 in Vietnam’s magical Phu Quoc island. Once again, the festival will bring the world’s most forward-thinking DJ superstars to Asia, who will be playing alongside the finest regional talents.</p>
                                <p>Biggest electronic music DJ legends, such as NINA KRAVIZ, RICARDO VILLALOBOS, SETH TROXLER, DUBFIRE, PRASLESH, FERRY CORSTEN, APOLLONIA, GOLDIE, BLACK SUN EMPIRE, STANTON WARRIORS, and many more, will be gracing the decks of the spectacular festival! They will all be bringing the freshest sound of the best global dancefloors straight to the Phu Quoc beach, where the action barely stops for 11 days and nights. The venue itself is an enchanting world of its own, that showcases impressive art installation and masterpiece sculptures, all set up in the tropical paradise full of lush emerald jungles, clear seas and hypnotizing sunsets. Festival brings the ultimate music NYE getaway and welcomes thousands of people from all over the world, dancing on the sand around the clock.</p>
                                <p>Who wants to spend holidays with us, dancing to the hottest beats and sharing those hypnotic vibes?</p>
                                <p>Be ready to get electrified! Epizode³ magic awaits!</p>
                                
                                <div class="embed-responsive embed-responsive-16by9" style="margin-top: 40px; margin-bottom: 40px;">
                                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/0vdC0_fYbNY?ecver=2" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                                </div>
                                <p>DJ Lineups:</p>
                                <ul>
                                    <li>Apollonia</li>
                                    <li>Bill Patrick</li>
                                    <li>Black Sun Empire</li>
                                    <li>Blapps Posse</li>
                                    <li>Dan Andrei</li>
                                    <li>Deekline</li>
                                    <li>Denis Kaznacheev</li>
                                    <li>Dewalta</li>
                                    <li>DJ Masda</li>
                                    <li>DOTT</li>
                                    <li>Dubfire</li>
                                    <li>Ferry Corsten</li>
                                    <li>Fonarev</li>
                                    <li>Freestylers</li>
                                    <li>Fumiya Tanaka</li>
                                    <li>Goldie</li>
                                    <li>Hibiya Line</li>
                                    <li>Jake Hough</li>
                                    <li>Kerry Wallace</li>
                                    <li>Lady Waks</li>
                                    <li>Leeroy Thornhill</li>
                                    <li>Mafia Kiss</li>
                                    <li>Mahony</li>
                                    <li>Marcus L</li>
                                    <li>Mashkov</li>
                                    <li>Miiia</li>
                                    <li>Mr. G (LIVE)</li>
                                    <li>Mutantbreakz</li>
                                    <li>Nic Ford</li>
                                    <li>Nick Warren</li>
                                    <li>Nina Kraviz</li>
                                    <li>Ocean Lam</li>
                                    <li>Oliver Huntemann</li>
                                    <li>Ouissam</li>
                                    <li>Praslesh</li>
                                    <li>Rasco</li>
                                    <li>Red Axes</li>
                                    <li>Ricardo Villalobos</li>
                                    <li>Rifain</li>
                                    <li>Sammy Dee</li>
                                    <li>Serial Killaz</li>
                                    <li>Seth Troxler</li>
                                    <li>Shaded</li>
                                    <li>Sofia Rodina</li>
                                    <li>Sonja Moonear</li>
                                    <li>Stanton Warriors</li>
                                    <li>Steppa Style</li>
                                    <li>Sunju Hargun</li>
                                    <li>Sunny W</li>
                                    <li>Swoosh</li>
                                    <li>tINI</li>
                                    <li>Tyoma (LIVE)</li>
                                    <li>Weng Weng</li>
                                    <li>youANDme</li>
                                    <li>Zig Zach</li>
                                    <li>Zip</li>
                                </ul>
                                <p>more names to be announced!</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="pageCategory-section last section-grey">
                <div class="container">
                    <div class="gallery text-center">
                        <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
                        <!-- Swiper -->
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <a href="images/epizode2018/gallery-1.jpg" data-featherlight="image"><img class="" src="images/epizode2018/gallery-1.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/epizode2018/gallery-2.jpg" data-featherlight="image"><img class="" src="images/epizode2018/gallery-2.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/epizode2018/gallery-3.jpg" data-featherlight="image"><img class="" src="images/epizode2018/gallery-3.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/epizode2018/gallery-4.jpg" data-featherlight="image"><img class="" src="images/epizode2018/gallery-4.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/epizode2018/gallery-5.jpg" data-featherlight="image"><img class="" src="images/epizode2018/gallery-5.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/epizode2018/gallery-6.jpg" data-featherlight="image"><img class="" src="images/epizode2018/gallery-6.jpg" alt=""></a>
                                </div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next swiper-button-white"></div>
                            <div class="swiper-button-prev swiper-button-white"></div>
                        </div><!-- /Swiper -->
                    </div>
                </div>
            </section>

            <section class="pageCategory-section last"><a id="anchorPrice"></a>
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                            <div class="text-center">
                                <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                                <p>Select ticket</p>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="table-responsive">
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Category</th>
                                            <th>Ticket Price</th>
                                            <th>Remarks</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1-Day Multi Pass</td>
                                            <td>RM284</td>
                                            <td rowspan="5">Inclusive of Ticket Fee</td>
                                        </tr>
                                        <tr>
                                            <td>3-Day Multi Pass</td>
                                            <td>RM724</td>
                                        </tr>
                                        <tr>
                                            <td>11-Day Multi Pass</td>
                                            <td>RM1008</td>
                                        </tr>
                                        <tr>
                                            <td>Sisters multi pass (for 2 girls)</td>
                                            <td>RM1008</td>
                                        </tr>
                                        <tr>
                                            <td>Group multi pass (for 3 pax)</td>
                                            <td>RM2408</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="buyAlert-bar">
                                <a class="btn btn-danger" id="buyButton" datetime="Dec 26 2018 00:00:00 GMT+0800" target="_blank" href="https://redtix-tickets.airasia.com/en-AU/shows/epizode³ (28th dec 2018 - 08th jan 2019)/events" role="button">BUY TICKETS</a>
                                {{-- <span class="or">/</span>
                                <span class="popData-btn" data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets <i class="fa fa-info-circle" aria-hidden="true"></i></span> --}}
                            </div>
                            <div class="note text-left">
                                <h2>Important Notes</h2>
                                <ol>
                                    {{-- <li>Prices shown exclude ticket fee.</li>
                                    <li>All ticket prices will be paid in Malaysian Ringgit. Indian Rupee prices are shown for reference purposes only.</li>
                                    <li>Prices quoted in INR may differ due to currency fluctuations.</li>
                                    <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li> --}}
                                    <li>RM10.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                                    <li>Online ticket selling will close 2 days prior to event day, subject to availability.</li>
                                    <li>Adult: Strictly 18 years old and above.</li>
                                </ol>
                                <h2>For enquiry only:</h2>
                                <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                            </div>

                        </div>
                    </div>
                </div>
            </section>
        </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 350;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });
    </script>

    {{-- Buy button disable --}}
    <script type="text/javascript">
        $(function() {
            $('a[id^=buyButton]').each(function() {
                var date = new Date();
                var enddate = $(this).attr('datetime'); 
                if ( Date.parse(date) >= Date.parse(enddate)) {
                  $(this).addClass('disabled');
                }
            });
        });
    </script>

@endsection

@section('modal')
    @include('layouts.partials.modals._seatplan')
    @include('layouts.partials.modals._getTix')
@endsection