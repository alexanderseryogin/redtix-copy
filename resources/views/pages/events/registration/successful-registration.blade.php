{{-- route(@extends('master') --}}
@extends('event-master')
@section('title')
   Successful Registration
@endsection

@section('header')
<header class="mt-3">
    <div class="d-flex justify-content-center">
        <div class="p-2 float-right">
            <a href="/"><img src="{!! asset('images/redtix_logo.png') !!}" alt="no red img" width="105px" height="63px"></a>
        </div>
        <div class="p-2 mt-4 float-right">
            <span class="text-grey">Grab it before someone else does!</span>
        </div>
    </div>
    
    <div class="d-flex justify-content-center">
        <img src="{!! asset('images/event-registration/tint.png') !!}" alt="no tint img" width="90%" height="6%" class="mt-4">
    </div>

    <div class="d-flex justify-content-around text-grey mx-sm-5 mt-4 px-0">
        <div>EVENTS</div>
        <div>COLLECTION</div>
        <div>BLOG</div>
        <div>LOREMS</div>
    </div>

    <hr class="text-grey" style="width:90%;">
</header>
@endsection

@section('content')
<div class="conatiner px-5">
    <!-- registration successful -->
    <div class="row my-5" id="registration-successful">
        <div class="col-sm-12 col-md-3 text-center">
            <img src="{!! asset('images/event-registration/form-ok.png') !!}" height="125px" width='100px' alt="no pink img">
        </div>
        <div class="col-sm-12 col-md-9">
            <h3 class="text-65">Registration successful</h3>
            <h6 class='text-grey'>Email</h6>
            <h5 class="text-1D">johndoe@gmail.com</h5>
        </div>
        <div class="col-sm-12 mt-5">
            <h4 class="text-grey">Registration successful</h4>
        </div>
    </div>

    <!-- hide (#registration-successful) & display (#assignment-successful) -->
    <!-- assignment successful -->
    <div class="row my-5" id="assignment-successful" style="display: none;">
        <div class="col-sm-12 col-md-3 text-center">
            <img src="{!! asset('images/event-registration/send.png') !!}" height="125px" width='120px' alt="no pink img">
        </div>
        <div class="col-sm-12 col-md-9">
            <h3 class="text-65">Assignment successful</h3>
            <h6 class='text-grey'>To:</h6>
            <h5 class="text-1D">johndoe@gmail.com</h5>
        </div>
        <div class="col-sm-12 mt-5">
            <h4 class="text-grey">Assignment successful</h4>
        </div>
    </div>

    <!-- hide (#registration-successful), (#assignment-successful) & display (#assignment-successful) -->
    <!-- get ticket successfully -->
    <div class="row my-5" id="get-ticket" style="display: none;">
        <div class="col-sm-12 col-md-3 text-center">
            <img src="{!! asset('images/event-registration/get-ticket.png') !!}" height="125px" width='100px' alt="no pink ticket img">
        </div>
        <div class="col-sm-12 col-md-9">
            <h3 class="text-65">You've been assigned a ticket</h3>
            <h6 class='text-grey'>From:</h6>
            <h5 class="text-1D">johndoe@gmail.com</h5>
        </div>
        <div class="col-sm-12 mt-5">
            <h4 class="text-grey">Have been assigned a ticket</h4>
        </div>
    </div>

    <div class="row">
        <!-- name & email -->
        <div class="col-sm-12 col-md-6">
            <h5 class="dark-grey">John Doe,</h5>
            <p class="dark-grey">(johndoe@gmail.com)</p>
        </div>
        <!-- transaction number -->
        <div class="col-sm-12 col-md-6">
            <h6 class="dark-grey">Transaction number</h6>
            <h5 class="dark-grey"><strong>6392 3720 2802</strong></h5>
        </div>
        <!-- registered for -->
        <div class="col-sm-12">
            <p class='text-grey'>
                The following tickets for
                <span class="dark-grey"><strong>IJM Allianz Duo Highway Challenge 2018 - Malaysia</strong></span> 
                have been registered
            </p>
        </div>
    </div>

    <div class="container my-5">
        <!-- registered to -->
        <div class="row">
            <p class="text-grey">Registered to: <span class="grey-email">buyer@gmail.com</span></p>
        </div>
        <!-- ticket -->
        <div class="row">
            <div class="col-md-6 pr-0">
                <div class="card card-shadow card-1 registered-ticket-left">
                    <div class="card-body text-left" style="margin-left: 60%;">
                        <!-- left card details -->
                        <small class="dark-grey">DATE & TIME</small>
                        <h6 class="dark-grey">24 July 2018, 8:00 p.m.</h6>
                        
                        <small class="dark-grey">VENUE</small>
                        <h6 class="dark-grey">Stadium Negara</h6>

                        <small class="dark-grey">PRICE</small>
                        <h6 class="dark-grey">RM 90</h6>

                        <small class="dark-grey">TICKET CATEGORY</small>
                        <h6 class="dark-grey">VVIP <small class="text-grey">Tier 1</small></h6>
                    </div>
                </div>
            </div>
            <di class="col-md-3 pl-0">
                <div class="card card-shadow card-2 registered-ticket-right">
                    <div class="card-body">
                        <h6 class="dark-grey">Ticket Number:</h6>
                        <h6 class="dark-grey">BKH78L</h6>
                    
                        <p class="text-grey">Name: <span class="dark-grey">John Doe</span></p>
                        <p class="text-grey">Email: <span class="dark-grey">johndoe@gmail.com</span></p>
                        <small class="text-grey">NPE Highway 12KM Men's (Age: 16-48)</small>
                    </div>
                </div>
            </di>
            <!-- <div class="col-md-3">
                <h6 class="text-65">this ticket is registered to:</h6>
                <h5 class="text-65">johndoe@gmail.com</h5>
                <div class="card d-inline-flex flex-row">
                    <div class="p-2 px-4">
                        <a href="#"><img src="{!! asset('images/event-registration/messenger.png') !!}" alt="no messenger img" width="23px" height="23px"></a>
                    </div>
                    <div class="p-2 px-4">
                        <a href="#"><img src="{!! asset('images/event-registration/facebook.png') !!}" alt="no fb img" width="20px" height="20px"></a>
                    </div>
                    <div class="p-2 px-4">
                        <a href="#"><img src="{!! asset('images/event-registration/gmail.png') !!}" alt="no gmail img" width="24px" height="20px"></a>
                    </div>
                </div>
            </div> -->
        </div>
        <!-- view registeration details -->
        <div class="row pt-3">
            <div class="col-sm-7"></div>
            <div class="col-sm-3">
                <a href="#" class="text-blue text-right">View registration details</a> 
            </div>          
        </div>
    </div>
</div>

<style>
    .registered-ticket-left {
        background: url(/images/event-registration/mytickets-left-active-bg.png) no-repeat;
        background-size: cover;
        border-right: 2px dashed rgba(29, 29, 29, 0.35); 
    }
    .registered-ticket-right {
        background: url(/images/event-registration/mytickets-right-active-bg.png) no-repeat;
        background-size: cover;
    }
    .un-registered-ticket-left {
        background: linear-gradient(
            rgba(0, 0, 0, 0.7),
            rgba(0, 0, 0, 0.7)
            ),url(/images/event-registration/mytickets-left-active-bg.png) no-repeat;
        background-size: cover;
        border-right: 2px dashed rgba(29, 29, 29, 0.35); 
    }
    .un-registered-ticket-right {
        background: linear-gradient(
            rgba(0, 0, 0, 0.7),
            rgba(0, 0, 0, 0.7)
            ),url(/images/event-registration/mytickets-right-active-bg.png) no-repeat;
        background-size: cover;
    }
    @import url('https://fonts.googleapis.com/css?family=Roboto');

    html, body{
        font-family: 'Roboto', sans-serif;
        height: 100%;
    }

    body {
        background-color: #FCFCFC;
    }

    header.mainHeader {
        background-color: #1D1D1D;
        position: absolute;
        left: 0;
        right: 0;
        top: 0px;
        z-index: 1001!important;
        height: 60px;
    }

    @media screen and (min-width: 200px) and (max-width: 769px){
        search {
            font-size: 24px;
            color: white;
            padding-top: 15px;
            display: block;
        }
    }

    @media screen and (min-width: 768px) and (max-width: 1024px){
        header.mainHeader .navbar-nav {
            float: right !important; 
            padding: 0px 0px; 
        }

        header.mainHeader .navbar-collapse {
            text-align: center !important;
            padding-bottom: 10px;
            background-color: transparent;
        }

        .navbar-collapse.collapse {
            display: block !important;
            height: auto !important;
            padding-bottom: 0;
            overflow: visible !important;
        }

        header.mainHeader .navbar-brand img {
            width: 100px;
            position: relative;
            z-index: 1001;
        }

        header.mainHeader .navbar-nav li a {
            font-family: open_sansregular;
            color: #fff;
            font-size: 12px;
            text-transform: uppercase;
            padding: 5px 21px;
            margin-top: 10px;
        }

        header.mainHeader {
            right: 0;
            z-index: 1001!important;
            height: 44px;
        }
    }

    .container {
        display: table;
        vertical-align: middle;
    }

    .vertical-center-row {
        display: table-cell;
        padding-top: 18%; 
        /* vertical-align: middle; */
    }

    h4 {
        color: rgba(29, 29, 29, 0.75);
    }

    .text-grey {
        color: rgba(29, 29, 29, 0.35);
    }

    .p-text-font {
        font-size: 18px;
    }

    .form-control {
        border: 2px solid #eee;
        border-radius: 2px;
        width: 420px;
        height: 50px;
    }

    .btn-submit-order {
        background-color: rgba(29, 29, 29, 0.2);
        border-radius: 2px; 
        color: white;
        margin-top: 2%; 
        padding: 15px 60px 15px 60px; 
        font-size: 18px;
    }

    .btn-submit {
        border-radius: 2px; 
        color: white;
        margin-top: 2%; 
        padding: 15px 60px 15px 60px; 
        font-size: 18px;

        background: #C33A8C;  /* fallback for old browsers */
        background: -webkit-linear-gradient(to bottom, #F13B54, #C33A8C);  /* Chrome 10-25, Safari 5.1-6 */
        background: linear-gradient(to bottom, #F13B54, #C33A8C); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
    }
    .form-control:focus {
        border-left: 5px solid #F13B54; 
        border-top: 1px solid rgba(29, 29, 29, 0.2); 
        border-right: 1px solid rgba(29, 29, 29, 0.2); 
        border-bottom: 1px solid rgba(29, 29, 29, 0.2);   
    }

    .has-error {
        border-left: 5px solid #F13B54 !important; 
        border-top: 1px solid #F13B54 !important; 
        border-right: 1px solid  #F13B54 !important; 
        border-bottom: 1px solid  #F13B54 !important;  
    }

    .error-message {
        color: #F13B54;
        margin-left: -160px;
        font-size: 12px;
        width: 250px;
        display: inline-block;
        text-align: left;
    }

    .form-control::-webkit-input-placeholder { color: rgba(29, 29, 29, 0.35); }  /* WebKit, Blink, Edge */
    .form-control:-moz-placeholder { color: rgba(29, 29, 29, 0.35) }  /* Mozilla Firefox 4 to 18 */
    .form-control::-moz-placeholder { color: rgba(29, 29, 29, 0.35) }  /* Mozilla Firefox 19+ */
    .form-control:-ms-input-placeholder { color: rgba(29, 29, 29, 0.35) }  /* Internet Explorer 10-11 */
    .form-control::-ms-input-placeholder { color: rgba(29, 29, 29, 0.35) }  /* Microsoft Edge */

    .form-input {
        border: 2px solid #eee;
        border-radius: 4px;
        height: 60px;
    }    
</style>
@endsection