{{-- route(@extends('master') --}}
@extends('event-master')
@section('title')
   Registration Form (assign to others)
@endsection

@section('header')
<nav class="navbar">
  <div class="container-fluid px-sm-0">
    <div class="navbar-header">
        <a class="navbar-brand" href="/"><img src="{!! asset('images/redtix_logo.png') !!}" alt="" width="105px" height="63px" class="mx-sm-0 px-sm-0"></a>
    </div>
  </div>
</nav>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-sm-12 pr-sm-0">
            <h3 class='dark-grey mb-sm-5'><strong>Runner Registration Form</strong></h3>
            <h5 class='text-grey mb-sm-4'>How to register an attendee for an event:</h5>
            
            <div class="row px-sm-0 mx-sm-0 instructions-text">
                <div class="col-xs-1 px-sm-0">1. </div>
                <div class="col-sm-11 px-sm-0">If you are attending the event, please fill in the form with your detials to assign a ticket to yourself.</div>
            </div>
            <div class="row px-sm-0 mx-sm-0 instructions-text">
                <div class="col-xs-1 px-sm-0">2.</div>
                <div class="col-sm-11 px-sm-0">If this ticket is for another attendee, you can simply assign a ticket to them by submitting their email.<br>
                    They will receive a link to fill in the form with their own details. Ensure that you assign the correct ticket by refering to the ticket name and/or serial number.
                </div>
            </div>

            <!-- assign ticket -->
            <div class="card my-sm-5 px-sm-0" id="assign-ticket-card">
                <div class="card-header bg-dark-grey text-justify">
                    <div class="row">
                        <div class="col-sm-8">
                            <h5 class="text-white">Tickets</h5>
                        </div>
                        <div class="col-sm-4">
                            <span class="text-white float-right">Ticket Number: <strong>BKH87L</strong></span>
                        </div>
                    </div>
                </div>
                <div class="card-body mb-sm-5">
                    <div class="row">
                        <div class="col-lg-8 col-md-8 col-sm-12">
                            <span class="card-title dark-grey" style="font-size: 32px;">VVIP</span>
                            <small class='text-grey'>Tier 1</small>
                            <h6 class="card-text text-grey mt-sm-3">NPE Highway (E10) - 12KM Men's (Age 16-39)</h6>
                            <h4 class="text-red">RM 90.00</h4>
                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-12 pl-sm-0">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" id="gridCheck1">
                                <label class="form-check-label text-grey" for="gridCheck1">
                                Assign this to another attendee so that I don't have to fill in the form for them
                                Example checkbox
                                </label>
                            </div>
                        </div>
                    </div>

                    <!-- form -->
                    <form action="" method="post">
                        <!-- email  -->
                        <div class="row my-sm-5">
                            <div class="col-sm-12 form-group">
                                <input type="email" class="form-control form-control-email " placeholder="Email" required>
                            </div>
                        </div>

                        <!-- submit button -->
                        <div class="row d-flex justify-content-center">
                            <button class="btn btn-submit btn-pink-shadow btn-md" type="submit">Send</button>
                            <!-- add (.btn-submit .btn-pink-shadow) when you get all valid form inputs & remove (.form-btn) -->
                        </div>
                    </form>
                </div> 
            </div> 

            <!-- hide above (assign-ticket-card) & display (ticket-success-card) on successful registration -->
            <!-- successful registration -->
            <div class="card my-sm-5 px-sm-0" id="ticket-success-card">
                <div class="card-header bg-dark-grey text-justify">
                    <div class="row">
                        <div class="col-sm-8">
                            <h5 class="text-white">Tickets</h5>
                        </div>
                        <div class="col-sm-4">
                            <span class="text-white float-right">Ticket number: <strong>BKH78l</strong></span>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-8 col-md-8 col-sm-12">
                            <span class="card-title dark-grey" style="font-size: 32px;">VVIP</span>
                            <small class='text-grey'>Tier 1</small>
                            <h6 class="card-text text-grey mt-sm-3">NPE Highway (E10) - 12KM Men's (Age 16-39)</h6>
                            <h4 class="text-red">RM 90.00</h4>
                        </div>
                    </div>

                    <div class="row">
                        <div class="card mx-sm-3 my-sm-5" style="width: 100%;">
                            <div class="card-body px-sm-5 py-sm-5">
                                <div class="row">
                                    <div class="col-md-4 col-sm-12">
                                        <img class="card-img-top px-sm-0 py-sm-2" src="{!! asset('images/event-registration/send.png') !!}" alt="no send img">
                                    </div>
                                    <div class="col-md-8 col-sm-12 d-flex align-items-center">
                                        <p class="text-grey" style="font-weight: lighter; font-size: 14px;">
                                            This ticket has been succesfully registered to<br> 
                                            <span class="grey-email">otherattendee@gmail.com</span>
                                        </p>
                                    </div>
                                </div>

                                <!-- view registration details -->
                                <div class="row d-flex justify-content-center">
                                    <a href="#" class="text-blue text-center">View registration details</a>
                                </div>

                                <!-- return to my tickets button -->
                                <div class="row mt-sm-3 d-flex justify-content-center">
                                    <button class="btn btn-submit btn-pink-shadow" style="font-size: 16px;">Return to MyTickets</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-4 col-sm-12">
            <div class="card card-shadow" style="width: 18rem;">
                <img class="card-img-top px-sm-2 py-sm-2" id="lit-img" src="{!! asset('images/event-registration/lit_run.png') !!}" alt="no lit img">
                <div class="card-body">
                    <h4 class='card-title dark-grey'><strong>Lit Run</strong></h4>
                    <h6 class='text-grey'>
                        <img src="{!! asset('images/event-registration/calendar.png') !!}" alt="no calendar img" height="25px" width="25px">
                        24 July 2018 & 8:00 a.m.
                    </h6>
                    <h6 class='text-grey'>
                        <img src="{!! asset('images/event-registration/location-marker.png') !!}" alt="no location img" height="25px" width="25px">
                        Stadium Negara
                    </h6>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
