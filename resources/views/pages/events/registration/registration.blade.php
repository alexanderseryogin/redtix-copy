{{-- route(@extends('master') --}}
@extends('event-master')
@section('title')
    Registration Form (self registering)
@endsection

@section('header')
<nav class="navbar">
  <div class="container-fluid px-sm-0">
    <div class="navbar-header">
        <a class="navbar-brand" href="/"><img src="{!! asset('images/redtix_logo.png') !!}" alt="" width="105px" height="63px" class="mx-sm-0 px-sm-0"></a>
    </div>
  </div>
</nav>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-sm-12 pr-sm-0">
            <h3 class='dark-grey mb-sm-5'><strong>Runner Registration Form</strong></h3>
            <h5 class='text-grey mb-sm-4'>How to register an attendee for an event:</h5>
            
            <div class="row px-sm-0 mx-sm-0 instructions-text">
                <div class="col-xs-1 px-sm-0">1. </div>
                <div class="col-sm-11 px-sm-0">If you are attending the event, please fill in the form with your detials to assign a ticket to yourself.</div>
            </div>
            <div class="row px-sm-0 mx-sm-0 instructions-text">
                <div class="col-xs-1 px-sm-0">2.</div>
                <div class="col-sm-11 px-sm-0">If this ticket is for another attendee, you can simply assign a ticket to them by submitting their email.<br>
                    They will receive a link to fill in the form with their own details. Ensure that you assign the correct ticket by refering to the ticket name and/or serial number.
                </div>
            </div>
            @if (!isset($registered))
            <!-- registration form -->
            <div class="card my-sm-5 px-sm-0" id="registration-card">
                <div class="card-header bg-dark-grey text-justify">
                    <div class="row">
                        <div class="col-sm-4">
                            <h6 class="text-white">Ticket</h6>
                        </div>
                        <div class="col-sm-8">
                            <span class="text-white float-right">Ticket Number: <strong>{{$ticket_info->ticket_number}}</strong></span>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-8 col-md-8 col-sm-12">
                            <span class="card-title dark-grey" style="font-size: 32px;"><strong>{{$ticket_info->area}}</strong></span>
                            <small class='text-grey'>{{$ticket_info->price_type}}</small>
                            <h6 class="card-text text-65 mt-sm-3">{{$ticket_info->show}}</h6>
                            <h4 class="text-red">RM {{$ticket_info->price}}</h4>
                        </div>
                        <!-- Assign Ticket Checkbox -->
                        <div class="col-lg-4 col-md-4 col-sm-12 pl-sm-0" style="display:none;">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" id="gridCheck1">
                                <label class="form-check-label text-grey" for="gridCheck1">
                                Assign this to another attendee so that I don't have to fill in the form for them
                                Example checkbox
                                </label>
                            </div>
                        </div>
                        <!-- Assign Ticket Checkbox -->
                    </div>

                    <!-- registartion form -->
                    <form action="ticket-registration" method="POST" id="registration_form">
                        {{ csrf_field() }}
                        <!-- personal information -->
                        <!-- hidden on small only -->
                        <div class="row d-none d-sm-block">
                            <h5 class="col-sm-12 my-sm-3 text-grey">
                                <img src="{!! asset('images/event-registration/person1.png') !!}" alt="no location img" height="30px" width="30px">
                                Personal Information
                            </h5>
                        </div>
                        <!-- visible on small only -->
                        <div class="row sticky-top d-block d-sm-none sm-row py-sm-3 mb-sm-3 mx-sm-0">
                            <h5 class="col-sm-12 my-sm-3 text-white">
                                <img src="{!! asset('images/event-registration/person.png') !!}" alt="no location img" height="30px" width="30px">
                                Personal Information
                            </h5>
                        </div>

                        <!-- first name & last name -->
                        <div class="row">
                            <div class="col-sm-6 form-group">
                                <input type="text" class="form-control form-input" id="firstName" name="first_name" placeholder="First Name" required>
                            </div>
                            <div class="col-sm-6 form-group">
                                <input type="text" class="form-control form-input" id="lastName" name="last_name" placeholder="Last Name" required>
                            </div>
                        </div>

                        <!-- email  -->
                        <div class="row">
                            <div class="col-sm-12 form-group">
                                <input type="email" class="form-control form-input" name="email" placeholder="Email" required>
                            </div>
                        </div>
                        
                        <!-- country &  mobile number -->
                        <div class="row">
                            <div class="col-sm-12 col-md-6 form-group">
                                <input type="text" class="form-control form-input" id="ph_country_code" name="ph_country_code" placeholder="Country Code" required>
                                <!-- COMMENTED FOR TESTING PURPOSES -->
                                
                                <!-- <select class="form-control form-select" id="country" name="ph_country_code" style="height: 60px;">
                                    <option value="null">Country Code</option>
                                    <option>Malaysia (+60)</option>
                                    <option>Pakistan (+92)</option>
                                    <option>India (+91)</option>
                                </select> -->
                            </div>
                            <div class="col-sm-12 col-md-6 form-group">
                                <input type="text" class="form-control form-input" id="mobileNumber" name="mobile_number" placeholder="Mobile Number" required>
                            </div>
                        </div>

                        <!-- nationality & passport -->
                        <div class="row">
                            <div class="col-md-6 col-sm-12 form-group">
                                <input type="text" class="form-control form-input" id="nationality" name="nationality" placeholder="Nationality" required>
                                <!-- COMMENTED FOR TESTING PURPOSES -->
                                
                                <!-- <select class="form-control form-select" id="nationality" name="nationality" style="height: 60px;">
                                    <option value="null">Nationality</option>
                                    <option>Malaysia</option>
                                    <option>Pakistan</option>
                                </select> -->
                            </div>
                            <div class="col-md-6 col-sm-12 form-group">
                                <input type="text" class="form-control form-input" name="passport" placeholder="Malaysian IC number/Passport" required>
                            </div>
                        </div>

                        <!-- gender & dob -->
                        <div class="row">
                            <div class="col-md-6 col-sm-12 form-group">
                                <select class="form-control form-select" id="gender" name="gender" style="height: 60px;">
                                    <option value="null">Gender</option>
                                    <option>Male</option>
                                    <option>Female</option>
                                    <option>Other</option>
                                    <option>Rather not say</option>
                                </select>
                            </div>
                            <div class="col-md-2 col-sm-6 form-group text-justify mt-sm-3">
                                <span class="text-grey">Date of birth</span>
                            </div>
                            <div class="col-md-4 col-sm-6 form-group">
                                <input type="date" class="form-control form-input" id="dob" name="dob" placeholder="DD/MM/YY" required>
                            </div>
                        </div>

                        <!-- blood type -->
                        <div class="row">
                            <div class="col-md-6 col-sm-12 form-group">
                                <select class="form-control form-select" id="blood-type" name="blood_type" style="height: 60px;">
                                    <option value="null">Blood Type</option>
                                    <option>+A</option>
                                    <option>-A</option>
                                    <option>+B</option>
                                    <option>-B</option>
                                    <option>+AB</option>
                                    <option>-AB</option>
                                    <option>+O</option>
                                    <option>-O</option>
                                </select>
                            </div>
                        </div>

                        <!-- T-shirt size & Finisher t-shirt size -->
                        <div class="row">
                            <div class="col-md-6 col-sm-12 form-group">
                                <select class="form-control form-select" id="t-shirt-size" name="t_size" style="height: 60px;">
                                    <option value="null">T shirt size</option>
                                    <option>S</option>
                                    <option>M</option>
                                    <option>L</option>
                                    <option>XL</option>
                                </select>
                            </div>
                            <div class="col-md-6 col-sm-12 form-group">
                                <select class="form-control form-select" id="finisher-t-shirt-size" name="finisher_t_size" style="height: 60px;">
                                    <option value="null">Finisher t-shirt size</option>
                                    <option>S</option>
                                    <option>M</option>
                                    <option>L</option>
                                    <option>XL</option>
                                </select>
                            </div>
                        </div>

                        <!-- race bib name -->
                        <div class="row">
                            <div class="col-md-6 col-sm-12 form-group">
                                <input type="text" class="form-control form-input" name="race_bib_name" placeholder="Race bib name" required>
                            </div>
                        </div>

                        <!-- address information -->
                        <!-- hidden on small only -->
                        <div class="row d-none d-sm-block">
                            <h5 class="col-sm-12 my-sm-3 text-grey">
                                <img src="{!! asset('images/event-registration/home1.png') !!}" alt="no address info img" height="28px" width="28px">
                                Address Information
                            </h5>
                        </div>
                        <!-- visible on small only -->
                        <div class="row sticky-top d-block d-sm-none sm-row py-sm-3 mb-sm-3 mx-sm-0">
                            <h5 class="col-sm-12 my-sm-3 text-white">
                                <img src="{!! asset('images/event-registration/home.png') !!}" alt="no address info img" height="28px" width="28px">
                                Address Information
                            </h5>
                        </div>

                        <!-- street address -->
                        <div class="row">
                            <div class="col-sm-12 form-group">
                                <input type="text" class="form-control form-input" name="street" placeholder="Street Address" required>
                            </div>
                        </div>

                        <!-- city & state -->
                        <div class="row">
                            <div class="col-md-6 col-sm-12 form-group">
                                <input type="text" class="form-control form-input" id="state" name="state" placeholder="State" required>
                                <!-- COMMENTED FOR TESTING PURPOSES -->
                                <!-- <select class="form-control form-select" id="state" name="state" style="height: 60px;">
                                    <option value="null">Choose your state</option>
                                    <option>Malaysia</option>
                                    <option>Pakistan</option>
                                </select> -->
                            </div>
                            <div class="col-md-6 col-sm-12 form-group">
                                <input type="text" class="form-control form-input" id="city" name="city" placeholder="City" required>

                                <!-- COMMENTED FOR TESTING PURPOSES -->
                                <!--  <select class="form-control form-select" id="city" name="city" style="height: 60px;">
                                    <option value="null">Choose your city</option>
                                    <option>Kuala Lumpur</option>
                                    <option>Johor Bahru</option> -->
                                </select>
                            </div>
                        </div>

                        <!-- postcode & country -->
                        <div class="row">
                            <div class="col-md-6 col-sm-12 form-group">
                                <input type="text" class="form-control form-input"  id="postcode" name="postcode" placeholder="Postcode" required>

                                <!-- FOR TESTING -->
                                <!-- <select class="form-control form-select" id="postcode" name="postcode" style="height: 60px;">
                                    <option value="null">Postcode</option>
                                    <option>+9876</option>
                                    <option>+9878</option>
                                </select> -->
                            </div>
                            <div class="col-md-6 col-sm-12 form-group">
                                <input type="text" class="form-control form-input"  id="country_code" name="country_code" placeholder="Country" required>

                                <!-- <select class="form-control form-select" name="country_code" style="height: 60px;">
                                    <option value="null">Country</option>
                                    <option>Malyasia</option>
                                    <option>Pakistan</option>
                                </select> -->
                            </div>
                        </div>

                        <!-- emergency contact -->
                        <!-- hidden on small only -->
                        <div class="row d-none d-sm-block">
                            <h5 class="col-sm-12 my-sm-3 text-grey">
                                <img src="{!! asset('images/event-registration/phone1.png') !!}" alt="no address info img" height="28px" width="20px">
                                <span class="ml-sm-2">Emergency Contact</span>
                            </h5>
                        </div>
                        <!-- visible on small only -->
                        <div class="row sticky-top d-block d-sm-none sm-row py-sm-3 mb-sm-3 mx-sm-0">
                            <h5 class="col-sm-12 my-sm-3 text-white">
                                <img src="{!! asset('images/event-registration/phone.png') !!}" alt="no address info img" height="28px" width="20px">
                                <span class="ml-sm-2">Emergency Contact</span>
                            </h5>
                        </div>

                        <!-- emergency contact person -->
                        <div class="row">
                            <div class="col-sm-12 form-group">
                                <input type="text" class="form-control form-input" name="em_contact" placeholder="Emergency contact person" required>
                            </div>
                        </div>

                        <!-- relationship -->
                        <div class="row">
                            <div class="col-sm-12 form-group">
                                <input type="text" class="form-control form-input" name="em_relation" placeholder="Relationship" required>
                            </div>
                        </div>

                        <!-- country &  mobile number -->
                        <div class="row">
                            <div class="col-md-6 col-sm-12 form-group">
                                <input type="text" class="form-control form-input" id="em_country_code" name="em_country_code" placeholder="Country Code" required>
                                <!-- COMMENTED FOR TESTING PURPOSES -->
                                <!-- <select class="form-control form-select" name="em_country_code" style="height: 60px;">
                                    <option value="null">Country Code</option>
                                    <option>Malyasia (+60)</option>
                                    <option>Pakistan (+92)</option>
                                </select> -->
                            </div>
                            <div class="col-md-6 col-sm-12 form-group">
                                <input type="text" class="form-control form-input" id="mobileNumber" name="em_mobile_number" placeholder="Mobile Number" required>
                            </div>
                        </div>
                        <input type="hidden" name="ticket_number" value="{{$ticket_info->ticket_number}}">
                        <input type="hidden" name="transaction_number" value="{{$ticket_info->transaction_number}}">

                        <!-- checkbox -->
                        <div class="row d-flex justify-content-center">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" id="agree-check">
                                <label class="form-check-label text-grey" for="agree-check">
                                I have read & agree on the <a href="#"><u>Waiver / Indemnity</u></a> stated
                                </label>
                            </div>
                        </div>
                        
                        <!-- submit button -->
                        <div class="row mt-sm-3 d-flex justify-content-center">
                            <button class="btn form-btn btn-md" type="submit">Done</button>
                            <!-- add (.btn-submit .btn-pink-shadow) when you get all valid form inputs & remove (.form-btn) -->
                        </div>
                    </form> 
                </div> 
            </div> <!-- ./ registration form -->
            @endif

            <!-- hide form and display this card -->
            <!-- successful registration -->
            @if (isset($registered) && $registered)
            <div class="card my-sm-5 px-sm-0">
                <div class="card-header bg-dark-grey text-justify">
                    <div class="row">
                        <div class="col-sm-4">
                            <h5 class="text-white">Tickets</h5>
                        </div>
                        <div class="col-sm-8">
                            <span class="text-white float-right">Serial number: <strong>{{$ticket_info->ticket_number}}</strong></span>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-8 col-md-8 col-sm-12">
                            <span class="card-title dark-grey" style="font-size: 32px;">{{$ticket_info->area}}</span>
                            <small class='text-grey'>{{$ticket_info->price_type}}</small>
                            <h6 class="card-text text-grey mt-sm-3">{{$ticket_info->show}}</h6>
                            <h4 class="text-red">RM {{$ticket_info->price}}</h4>
                        </div>
                    </div>

                    <div class="row">
                        <div class="card mx-sm-3 my-sm-5" style="width: 100%;">
                            <div class="card-body px-sm-5 py-sm-5">
                                <div class="row">
                                    <div class="col-md-4">
                                        <img class="card-img-top px-sm-2 py-sm-2" src="{!! asset('images/event-registration/form-ok.png') !!}" alt="no pink img">
                                    </div>
                                    <div class="col-md-8 d-flex align-items-center">
                                        <h6 class="text-45">
                                            This ticket has been succesfully registered to<br> 
                                            <span class="grey-email"><strong>{{$ticket_detail->first_name}} {{$ticket_detail->last_name}} ({{$ticket_detail->email}})<strong></span>
                                        </h6>
                                    </div>
                                </div>

                                <!-- view registration details -->
                                <div class="row d-flex justify-content-center">
                                    <a href="#" class="text-blue text-center" style="display:none;">View registration details</a>
                                </div>

                                <!-- return to my tickets button -->
                                <div class="row mt-sm-3 d-flex justify-content-center">
                                    <a href="/mytickets"><button class="btn btn-submit btn-pink-shadow" style="font-size: 16px;">Return to MyTickets</button></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
        </div>

        <div class="col-md-4 col-sm-12">
            <div class="card card-shadow" style="width: 18rem;">
                <img class="card-img-top px-sm-2 py-sm-2" id="lit-img" src="{{$ticket_info->reg_page_image}}" alt="no lit img">
                <div class="card-body">
                    <h4 class='card-title dark-grey'><strong>{{$ticket_info->name}}</strong></h4>
                    <h6 class='text-65'>
                        <img src="{!! asset('images/event-registration/calendar.png') !!}" alt="no calendar img" height="25px" width="25px">
                        {{$ticket_info->date}}
                    </h6>
                    <h6 class='text-65'>
                        <img src="{!! asset('images/event-registration/location-marker.png') !!}" alt="no location img" height="25px" width="25px">
                        {{$ticket_info->venue}}
                    </h6>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
