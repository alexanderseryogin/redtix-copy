{{-- route(@extends('master') --}}
@extends('master-tickets')
@section('title')
   My Tickets
@endsection

@section('header')
<!-- TODO  -->
<!-- <nav class="navbar navbar-dark">
  <div class="container-fluid">
    <div class="navbar-header">
        <a class="navbar-brand nav-img" href="/"><img src="{!! asset('images/redtix_logo.png') !!}" alt="no nav img" width="105px" height="56px"></a>
    </div>
    <ul class="nav navbar-right">
        <li class="px-sm-4"><a class="text-white" href="/about">ABOUT US</a></li>
        <li class="px-sm-4"><a class="text-white" href="/faq">FAQ</a></li>
        <li class="px-sm-4"><a class="text-white" href="#">NEWSLETTER</a></li>
        <li class="px-sm-4"><a class="text-white" href="mailto:support@airasiaredtix.com">CONTACT US</a></li>
    </ul>
  </div>
</nav> -->
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="p-2 pt-sm-5"><h4 class="dark-grey">My Tickets</h4></div>
    </div>

    <div class="justify-content-end">
        <div class="form-check" style="display: none;">
            <input class="form-check-input" type="checkbox" id="gridCheck">
            <label class="form-check-label text-grey" for="gridCheck">
                Show tickets I've assigned to others
            </label>
        </div>
    </div>  
</div>

<!-- tickets -->
<div class="container">
    @foreach ($tickets as $ticket)
    <!-- if registered -->
    <div class="row pb-sm-5">
        <div class="col-sm-12 col-md-6 mx-sm-0 px-sm-0">
            <div class="card card-shadow card-1 @if (isset($ticket->ticket_info)) {{'registered-left'}} @else {{'un-registered-left'}} @endif">
                <div class="card-body text-left">
                    <!-- card 1 details -->
                    <div class="details">
                        <small class="dark-grey">DATE & TIME</small>
                        <h6 class="dark-grey">{{$ticket->date}}</h6>
                        
                        <small class="dark-grey">VENUE</small>
                        <h6 class="dark-grey">{{$ticket->venue}}</h6>

                        <small class="dark-grey">PRICE</small>
                        <h6 class="dark-grey">RM {{$ticket->price}}</h6>

                        <small class="dark-grey">TICKET CATEGORY</small>
                        <h6 class="dark-grey">{{$ticket->area}}<small class="text-grey">{{$ticket->price_type}}</small></h6>
                    </div>
               </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-4 mx-sm-0 px-sm-0">
            <div class="card card-shadow card-2 @if (isset($ticket->ticket_info)) {{'registered-right'}} @else {{'un-registered-right'}} @endif">
                <div class="card-body pl-5 text-left d-flex flex-column">
                    <h6 class="dark-grey">Ticket Number:</h6>
                    <h6 class="dark-grey">{{$ticket->ticket_number}}</h6>
                @if (isset($ticket->ticket_info))

                    <p class="text-grey">Name: <span class="dark-grey">{{$ticket->ticket_info->first_name}} {{$ticket->ticket_info->last_name}}</span></p>
                    <p class="text-grey">Email: <span class="dark-grey">{{$ticket->ticket_info->email}}</span></p>
                    <small class="text-grey">{{$ticket->show}}</small>
                    <!-- TODO QR IMAGE -->
                    <div id="{{$ticket->ticket_number}}-qr-code"></div>
                @endif
                </div>
            </div>
        </div>

        @if (isset($ticket->ticket_info))
        <!-- registered person details -->
        <div class="col-md-2 py-sm-5">
            <h6 class="text-65">this ticket is registered to:</h6>
            <h5 class="text-65">{{$ticket->ticket_info->email}}</h5>
            <div class="card d-inline-flex flex-row">
                <div class="p-2">
                    <a href="#"><img src="{!! asset('images/event-registration/messenger.png') !!}" alt="no messenger img" width="23px" height="23px"></a>
                </div>
                <div class="p-2">
                    <a href="#"><img src="{!! asset('images/event-registration/facebook.png') !!}" alt="no fb img" width="20px" height="20px"></a>
                </div>
                <div class="p-2">
                    <a href="#"><img src="{!! asset('images/event-registration/gmail.png') !!}" alt="no gmail img" width="24px" height="20px"></a>
                </div>
            </div>
        </div>
        @else
            <a href="/registration?transaction_number={{$ticket->transaction_number}}&ticket_number={{$ticket->ticket_number}}">
                <button type="submit" class="btn register-btn">Register</button>
            </a>
            <!-- <a href="/registration?transaction_number={{$ticket->transaction_number}}&ticket_number={{$ticket->ticket_number}}"><button>Register</button></a> -->
        @endif
    </div>
    @endforeach
</div>

@foreach ($tickets as $ticket)
<script>
    $(function(){
        $('#{{$ticket->ticket_number}}-qr-code').qrcode({width: 64,height: 64,text: "size doesn't matter"});
    });
</script>
@endforeach

<style>
    .registered-left {
        background: url({{$ticket->ticket_left_image}}) no-repeat;
        background-size: cover;
    }
    .registered-right {
        background: url({{$ticket->ticket_right_image}}) no-repeat;
        background-size: cover;
    }
    .un-registered-left {
        background: linear-gradient(
            rgba(0, 0, 0, 0.7),
            rgba(0, 0, 0, 0.7)
            ),url({{$ticket->ticket_left_image}}) no-repeat;
            background-size: cover;
    }
    .un-registered-right {
        background: linear-gradient(
            rgba(0, 0, 0, 0.7),
            rgba(0, 0, 0, 0.7)
            ),url({{$ticket->ticket_right_image}}) no-repeat;
            background-size: cover;
    }
</style>

@endsection
