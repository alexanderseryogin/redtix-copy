<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns:v="urn:schemas-microsoft-com:vml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;" />
    <meta name="viewport" content="width=600,initial-scale = 2.3,user-scalable=no">
    <!--[if !mso]><!-- -->
    <link href='https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,600,700' rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Quicksand:300,400,700' rel="stylesheet">
    <!-- <![endif]-->

    <title>AART</title>

    <style type="text/css">
        body {
            width: 100%;
            background-color: #ffffff;
            margin: 0;
            padding: 0;
            -webkit-font-smoothing: antialiased;
            mso-margin-top-alt: 0px;
            mso-margin-bottom-alt: 0px;
            mso-padding-alt: 0px 0px 0px 0px;
        }

        p,
        h1,
        h2,
        h3,
        h4 {
            margin-top: 0;
            margin-bottom: 0;
            padding-top: 0;
            padding-bottom: 0;
        }

        span.preheader {
            display: none;
            font-size: 1px;
        }

        html {
            width: 100%;
        }

        table {
            font-size: 14px;
            border: 0;
        }
        /* ----------- responsivity ----------- */

        @media only screen and (max-width: 640px) {
            /*------ top header ------ */
            .main-header {
                font-size: 20px !important;
            }
            .main-section-header {
                font-size: 28px !important;
            }
            .show {
                display: block !important;
            }
            .hide {
                display: none !important;
            }
            .align-center {
                text-align: center !important;
            }
            .no-bg {
                background: none !important;
            }
            /*----- main image -------*/
            .main-image img {
                width: 440px !important;
                height: auto !important;
            }
            /* ====== divider ====== */
            .divider img {
                width: 440px !important;
            }
            /*-------- container --------*/
            .container590 {
                width: 440px !important;
            }
            .container580 {
                width: 400px !important;
            }
            .main-button {
                width: 220px !important;
            }
            /*-------- secions ----------*/
            .section-img img {
                width: 320px !important;
                height: auto !important;
            }
            .team-img img {
                width: 100% !important;
                height: auto !important;
            }
        }

        @media only screen and (max-width: 479px) {
            /*------ top header ------ */
            .main-header {
                font-size: 18px !important;
            }
            .main-section-header {
                font-size: 26px !important;
            }
            /* ====== divider ====== */
            .divider img {
                width: 280px !important;
            }
            /*-------- container --------*/
            .container590 {
                width: 280px !important;
            }
            .container590 {
                width: 280px !important;
            }
            .container580 {
                width: 260px !important;
            }
            /*-------- secions ----------*/
            .section-img img {
                width: 280px !important;
                height: auto !important;
            }

        }
    </style>
    <!-- [if gte mso 9]><style type=”text/css”>
        body {
            font-family: arial, sans-serif!important;
        }
        .passenger-head{
            color: #5caad2;
            font-family: Quicksand, Calibri, sans-serif;
            font-size: 19px;
            font-weight: 600;
        }
    </style>
    <![endif]-->
</head>


<body class="respond" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<table width="1000">
    <tbody>
        <tr>
            <td style="text-align: right;"><img src="{!! asset('images/redtix_logo.png') !!}" alt="no red img" height="90px" /></td>
            <td style="text-align: left;">Grab it before someone else does!</td>
        </tr>
        <tr>
            <td colspan="2"><img src="{!! asset('images/event-registration/tint.png') !!}" alt="no tint img"></td>
        </tr>
    </tbody>
</table>

<header class="mt-3">

    <div class="d-flex justify-content-around text-grey mx-sm-5 mt-4 px-0">
        <div>EVENTS</div>
        <div>COLLECTION</div>
        <div>BLOG</div>
        <div>LOREMS</div>
    </div>

    <hr class="text-grey" style="width:90%;">
</header>

@section('content')
<!-- registration successful -->
<div class="conatiner px-5">
    <div class="row my-5">
        <div class="col-sm-12 col-md-3 text-center">
            <img src="{!! asset('images/event-registration/form-ok.png') !!}" height="125px" width='100px' alt="no pink img">
        </div>
        <div class="col-sm-12 col-md-9">
            <h3 class="text-65">Registration successful</h3>
            <h6 class='text-grey'>Email</h6>
            <h5 class="text-1D">johndoe@gmail.com</h5>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <h5 class="text-grey">Registration successful</h5>
        </div>

        <!-- name & email -->
        <div class="col-sm-12 col-md-6">
            <h5 class="dark-grey">John Doe,</h5>
            <p class="dark-grey">(johndoe@gmail.com)</p>
        </div>
        <!-- transaction number -->
        <div class="col-sm-12 col-md-6">
            <h6 class="dark-grey">Transaction number</h6>
            <h5 class="dark-grey"><strong>6392 3720 2802</strong></h5>
        </div>
    </div>
</div>
@endsection


</body>

</html>