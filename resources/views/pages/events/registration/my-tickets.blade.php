{{-- route(@extends('master') --}}
@extends('master-tickets')
@section('title')
   My Tickets
@endsection

@section('header')
<!-- TODO  -->
<!-- Header & Navigation -->
<!-- <header class="mainHeader">
    <div class="container-fluid">
    <nav class="navbar navbar-expand-lg" role="navigation">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-01">
                <span class="sr-only">Toggle navigation</span>
            </button>
            <a class="navbar-brand" href="/"><img src="{!! asset('images/redtix_logo.png') !!}" alt="no nav img"></a>
        </div>
        <div class="collapse navbar-collapse" id="navbar-collapse-01">
            <ul class="nav navbar-nav pull-right">
                <li><a href="/about">About Us</a></li>
                <li><a href="/faq">FAQ</a></li>
                <li><a class="newsletter-nav" data-toggle="modal" data-target="#modalSubscribeNewsletter">Newsletter</a></li>
                <li><a href="mailto:support@airasiaredtix.com">Contact Us</a></li>
            </ul>
        </div>
    </nav>
    </div>
</header> -->

<nav class="navbar navbar-expand-lg navbar-dark" role="navigation">
  <a class="navbar-brand nav-img" href="/"><img src="{!! asset('images/redtix_logo.png') !!}" alt="no logo img" width="110px" height="66px"></a>

  <button class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span style="color: white; font-size: 24px;"><i class="fa fa-bars"></i></span>
  </button>

  <div class="collapse navbar-collapse navbar-dark" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto text-center">
      <li class="nav-item">
        <a class="nav-link text-white p-4" href="/about">ABOUT US</a>
      </li>
      <li class="nav-item">
        <a class="nav-link text-white p-4" href="/faq">FAQ</a>
      </li>
      <li class="nav-item">
        <a class="nav-link newsletter-nav text-white p-4" data-toggle="modal" data-target="#modalSubscribeNewsletter">NEWSLETTER</a>
      </li>
      <li class="nav-item">
        <a class="nav-link text-white p-4" href="mailto:support@airasiaredtix.com">CONTACT US</a>
      </li>
    </ul>
  </div>
</nav>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="p-2 pt-sm-5"><h4 class="dark-grey">My Tickets</h4></div>
    </div>

    <div class="d-flex justify-content-end pb-5">
        <div class="form-check" style="display: none;">
            <input class="form-check-input" type="checkbox" id="gridCheck">
            <label class="form-check-label text-grey" for="gridCheck">
                Show tickets I've assigned to others
            </label>
        </div>
    </div>  
</div>

<!-- tickets for large screens -->
<div class="d-none d-lg-block">
    <div class="container">
        @foreach ($tickets as $ticket)
        <!-- if registered -->
        <div class="row pb-sm-5">
            <div class="col-sm-12 col-md-6 mx-sm-0 px-sm-0">
                <div class="card card-shadow card-1 @if (isset($ticket->ticket_info)) {{'registered-left'}} @else {{'un-registered-left'}} @endif">
                    <div class="card-body text-left">
                        <!-- card 1 details -->
                        <div class="details">
                            <small class="dark-grey">DATE & TIME</small>
                            <h6 class="dark-grey">{{$ticket->date}}</h6>
                            
                            <small class="dark-grey">VENUE</small>
                            <h6 class="dark-grey">{{$ticket->venue}}</h6>

                            <small class="dark-grey">PRICE</small>
                            <h6 class="dark-grey">RM {{$ticket->price}}</h6>

                            <small class="dark-grey">TICKET CATEGORY</small>
                            <h6 class="dark-grey">{{$ticket->area}}<small class="text-grey">{{$ticket->price_type}}</small></h6>
                        </div>
                </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-4 mx-sm-0 px-sm-0">
                <div class="card card-shadow card-2 @if (isset($ticket->ticket_info)) {{'registered-right'}} @else {{'un-registered-right'}} @endif">
                    <div class="card-body pl-5 text-left d-flex flex-column">
                        <h6 class="dark-grey">Ticket Number:</h6>
                        <h6 class="dark-grey">{{$ticket->ticket_number}}</h6>
                    @if (isset($ticket->ticket_info))

                        <p class="text-45">Name: <span class="dark-grey">{{$ticket->ticket_info->first_name}} {{$ticket->ticket_info->last_name}}</span></p>
                        <p class="text-45">Email: <span class="dark-grey">{{$ticket->ticket_info->email}}</span></p>
                        <small class="text-65">{{$ticket->show}}</small>
                        <!-- TODO QR IMAGE -->
                        <div class="{{$ticket->ticket_number}}-qr-code qr-code d-flex justify-content-end"></div>
                    @endif
                    </div>
                </div>
            </div>

            @if (isset($ticket->ticket_info))
            <!-- registered person details -->
            <div class="col-md-2 py-sm-5">
                <h6 class="text-65">This ticket is registered to:</h6>
                <h5 class="text-65"><b>{{$ticket->ticket_info->email}}<b></h5>
                <div class="card d-inline-flex flex-row">
                    <div class="p-2 px-3">
                        <a href="#"><img src="{!! asset('images/event-registration/messenger.png') !!}" alt="no messenger img" width="23px" height="23px"></a>
                    </div>
                    <div class="p-2 px-3">
                        <a href="#"><img src="{!! asset('images/event-registration/facebook.png') !!}" alt="no fb img" width="20px" height="20px"></a>
                    </div>
                    <div class="p-2 px-3">
                        <a href="#"><img src="{!! asset('images/event-registration/gmail.png') !!}" alt="no gmail img" width="24px" height="20px"></a>
                    </div>
                </div>
            </div>
            @else
                <a href="/registration?transaction_number={{$ticket->transaction_number}}&ticket_number={{$ticket->ticket_number}}">
                    <button type="submit" class="btn register-btn">Register</button>
                </a>
                <!-- <a href="/registration?transaction_number={{$ticket->transaction_number}}&ticket_number={{$ticket->ticket_number}}"><button>Register</button></a> -->
            @endif
        </div>
        @endforeach
    </div>
</div>

<!-- tickets for smaller/medium screens -->
<div class="d-lg-none">
    <div class="container">
        <div class="container">
            @foreach ($tickets as $ticket)
            <div class="row pb-sm-5">
                <div class="col-sm-12 col-md-6 mx-sm-0 px-sm-0">
                    <!-- <div class="card card-shadow card-1 @if (isset($ticket->ticket_info)) {{'resp-registered-top'}} @else {{'resp-un-registered-top'}} @endif"> -->
                    <div class="card card-shadow card-1 @if (isset($ticket->ticket_info)) {{'resp-registered-top'}} @else {{'resp-un-registered-top'}} @endif">                    
                        <div class="card-body text-left" style="padding-left: 20%">
                            <!-- card 1 details -->
                            <div class="details">
                                <small class="dark-grey">DATE & TIME</small>
                                <h5 class="dark-grey">{{$ticket->date}}</h5>
                                
                                <small class="dark-grey">VENUE</small>
                                <h5 class="dark-grey">{{$ticket->venue}}</h5>

                                <small class="dark-grey">PRICE</small>
                                <h5 class="dark-grey">RM {{$ticket->price}}</h5>

                                <small class="dark-grey">TICKET CATEGORY</small>
                                <h5 class="dark-grey">{{$ticket->area}}<small class="text-grey">{{$ticket->price_type}}</small></h5>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-4 mx-sm-0 px-sm-0">
                    <!-- <div class="card card-shadow card-2 @if (isset($ticket->ticket_info)) {{'resp-registered-bottom'}} @else {{'resp-un-registered-bottom'}} @endif"> -->
                    <div class="card card-shadow card-2 @if (isset($ticket->ticket_info)) {{'resp-registered-bottom'}} @else {{'resp-un-registered-bottom'}} @endif">
                        <div class="card-body pl-5 text-left d-flex flex-column">
                            <h6 class="dark-grey">Ticket Number:</h6>
                            <h6 class="dark-grey">{{$ticket->ticket_number}}</h6>
                        @if (isset($ticket->ticket_info))
                            <p class="text-45">Name: <span class="dark-grey">{{$ticket->ticket_info->first_name}} {{$ticket->ticket_info->last_name}}</span></p>
                            <p class="text-45">Email: <span class="dark-grey">{{$ticket->ticket_info->email}}</span></p>
                            <small class="text-65">{{$ticket->show}}</small>
                            <!-- TODO QR IMAGE -->
                            <div class="{{$ticket->ticket_number}}-qr-code qr-code"></div>
                        @endif
                        </div>
                    </div>
                </div>

                @if (isset($ticket->ticket_info))
                <!-- registered person details -->
                <div class="col-md-2 py-sm-5">
                    <h6 class="text-65">this ticket is registered to:</h6>
                    <h5 class="text-65">{{$ticket->ticket_info->email}}</h5>
                    <div class="card d-inline-flex flex-row">
                        <div class="p-2">
                            <a href="#"><img src="{!! asset('images/event-registration/messenger.png') !!}" alt="no messenger img" width="23px" height="23px"></a>
                        </div>
                        <div class="p-2">
                            <a href="#"><img src="{!! asset('images/event-registration/facebook.png') !!}" alt="no fb img" width="20px" height="20px"></a>
                        </div>
                        <div class="p-2">
                            <a href="#"><img src="{!! asset('images/event-registration/gmail.png') !!}" alt="no gmail img" width="24px" height="20px"></a>
                        </div>
                    </div>
                </div>
                @else
                    <a href="/registration?transaction_number={{$ticket->transaction_number}}&ticket_number={{$ticket->ticket_number}}">
                        <button type="submit" class="btn resp-register-btn">Register</button>
                    </a>
                    <!-- <a href="/registration?transaction_number={{$ticket->transaction_number}}&ticket_number={{$ticket->ticket_number}}"><button>Register</button></a> -->
                @endif
            </div>
            @endforeach
        </div>
    </div>
</div>
@foreach ($tickets as $ticket)
<script>
    $(function(){
        $('.{{$ticket->ticket_number}}-qr-code').qrcode({width: 64,height: 64,text: "{{$ticket->ticket_number}}"});
    });
</script>
@endforeach

<style>
    .registered-left {
        background: url({{$ticket->ticket_left_image}}) no-repeat;
        background-size: cover;
        border-right: 2px dashed rgba(29, 29, 29, 0.35); 
    }
    .registered-right {
        background: url({{$ticket->ticket_right_image}}) no-repeat;
        background-size: cover;
    }
    .un-registered-left {
        background: linear-gradient(
            rgba(0, 0, 0, 0.7),
            rgba(0, 0, 0, 0.7)
            ),url({{$ticket->ticket_left_image}}) no-repeat;
        background-size: cover;
        border-right: 2px dashed rgba(29, 29, 29, 0.35); 
    }
    .un-registered-right {
        background: linear-gradient(
            rgba(0, 0, 0, 0.7),
            rgba(0, 0, 0, 0.7)
            ),url({{$ticket->ticket_right_image}}) no-repeat;
        background-size: cover;
    }

    /* for responsive view */
    .resp-registered-top {
        background: url({{$ticket->ticket_left_image_resp}}) no-repeat;
        background-size: cover;
        border-bottom: 2px dashed rgba(29, 29, 29, 0.35); 
        height: 450px;
        margin-top: 10px;
    }
    .resp-registered-top .card-body {
        margin-top: 190px;
        padding-left: 30px;
    }
    .resp-registered-bottom {
        background: url({{$ticket->ticket_right_image_resp}}) no-repeat;
        background-size: cover;
        margin-bottom: 20px;
    }
    .resp-un-registered-top {
        background: linear-gradient(
            rgba(0, 0, 0, 0.7),
            rgba(0, 0, 0, 0.7)
            ),url({{$ticket->ticket_left_image_resp}}) no-repeat;
        background-size: cover;
        border-bottom: 2px dashed rgba(29, 29, 29, 0.35); 
        height: 450px;
        margin-top: 10px;
    }
    .resp-un-registered-top .card-body {
        margin-top: 190px;
        padding-left: 30px;
    }
    .resp-un-registered-bottom {
        background: linear-gradient(
            rgba(0, 0, 0, 0.7),
            rgba(0, 0, 0, 0.7)
            ),url({{$ticket->ticket_right_image_resp}}) no-repeat;
        background-size: cover;
    }
    .qr-code {
        margin-top:15px;
    }
</style>

@endsection
