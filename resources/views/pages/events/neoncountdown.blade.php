@extends('master')
@section('title')
    NEON #CountdownNYE 2016
@endsection

@section('header')
    @include('layouts.partials._header')
@endsection

@section('content')

    <!-- Banner Section -->
    <section class="innerPageBanner">
      <div class="bigBanner-overlay"></div>
      <div class="jumbotron eventBanner" style="background-image: url('')">
        <video id="video-background" class="hidden-xs" preload muted autoplay loop>
          <source src="images/neon/top-banner2000x450.mp4" type="video/mp4">
        </video>
        <video id="video-background" class="visible-xs-block" preload muted autoplay loop>
          <source src="images/neon/top-banner768x450.mp4" type="video/mp4">
        </video>
      </div>
    </section><!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
          <div class="row priceNbtn">
            <div class="col-sm-offset-1 col-sm-10">
              <div class="row">
                <div class="col-sm-9 leftBox">
                  <h6>NEON #CountdownNYE 2016</h6>
                  Tickets from <span>RM103</span>
                </div>
                <div class="col-sm-3 text-center">
                  <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                </div>
              </div>
            </div>
          </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
      <div class="mainBodyContent no-btm-mar section-white">

        <section class="pageCategory-section last">
          
          <div class="container intro">
            <div class="row">
              <div class="col-sm-offset-1 col-sm-10 leftBar">
                <!--<h2>Title</h2>-->
                <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i> 31 December 2016</div>
                <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i> Sunway Lagoon @ Surf Beach <a target="_blank" href="https://www.google.com/maps/place/Sunway+Lagoon/@3.0692108,101.604838,17z/data=!3m1!4b1!4m5!3m4!1s0x31cc4c88dded3125:0xdb654cc77af0cdbc!8m2!3d3.0691643!4d101.6069192">View Map</a></div>
                <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> 6 pm</div>
                <hr>
                <p>Welcome to the world of #CountdownNYE and join us into an endless ocean of colors. We’re just over the halfway mark for 2016, and already we’re getting ready to celebrate one heck of a year with the return of Neon #CountdownNYE, our annual New Year’s Eve blowout.</p>
                <p>Last year’s extravaganza helped kick 2016 into immediate high gear, and this year’s event will prove to be bigger and louder than ever before. Featuring grand stage all New Year’s Eve day long and music from all your favourite artists, international and homegrown.</p>
                <p>Ring in the New Year with your fellow Headliners, families, and friends as Countdown 2016 takes over the Pyramid zone at Sunway Lagoon this 31st Dec. Trust us: You’ll add Neon #CountdownNYE 2016 to the list of nights you’ll remember for the rest of next year.</p>
                <p>The clock is ticking. Will you be there to Countdown with us? You are advised to purchase your tickets as soon as you decided to join us, usually our shows are sold out before the event.</p>
                <p>Plus, we have a special treat for AirAsia BIG Members to kick start their travel plans in 2017! Be among the first 1,000 AirAsia BIG members to purchase Neon Countdown tickets on Redtix and you will receive 1,000 AirAsia BIG Points for every ticket purchased. Not an AirAsia BIG member yet? Sign up now at AIRASIABIG.com!</p>
                <p><strong>Terms & Conditions</strong></p>
                <ul>
                  <li>1,000 AirAsia BIG Points giveaway is limited to the first 1,000 Neon Countdown tickets purchased on Redtix.</li>
                  <li>Redtix will contact all eligible members who purchased Neon Countdown tickets for points fulfillment.</li>
                </ul>
                <iframe width="100%" height="400" src="https://www.youtube.com/embed/WYtHQV3MHkQ?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>
        </section>

        <section class="pageCategory-section last section-grey">
          <div class="container">
            <div class="lineup text-center">
                <h1 class="subSecTitle"><strong>THE LINEUP</strong></h1>
                <p>6 performers that will be performed</p>
                <ul class="list-unstyled list-inline lineupList">
                    <li><img src="images/neon/headhunterz-thumb.jpg" alt="">HeadHunterz</li>
                    <li><img src="images/neon/bate-thumb.jpg" alt="">Bate</li>
                    <li><img src="images/neon/chukiess_whackboi-thumb.jpg" alt="">Chukiess Whackboi</li>
                    <li><img src="images/neon/mastermind-thumb.jpg" alt="">Mastermind</li>
                    <li><img src="images/neon/shaqir-thumb.jpg" alt="">Shaqir</li>
                    <li><img src="images/neon/simonleeandalvin-thumb.jpg" alt="">Simon Lee & Alvin</li>
                </ul>
            </div>
            <div class="gallery">
              <!-- Swiper -->
              <div class="swiper-container">
                <div class="swiper-wrapper">
                  <div class="swiper-slide">
                    <img class="" src="images/neon/NEON-MusicFest-2016.jpg" alt="">
                  </div>
                  <div class="swiper-slide">
                    <img class="" src="images/neon/img1.jpg" alt="">
                  </div>
                  <div class="swiper-slide">
                    <img class="" src="images/neon/img2.jpg" alt="">
                  </div>
                  <div class="swiper-slide">
                    <img class="" src="images/neon/img3.jpg" alt="">
                  </div>
                  <div class="swiper-slide">
                    <img class="" src="images/neon/img4.jpg" alt="">
                  </div>
                  <div class="swiper-slide">
                    <img class="" src="images/neon/img5.jpg" alt="">
                  </div>
                  <div class="swiper-slide">
                    <img class="" src="images/neon/img6.jpg" alt="">
                  </div>
                </div>
                <!-- Add Pagination -->
                <div class="swiper-pagination"></div>
                <!-- Add Arrows -->
                <div class="swiper-button-next swiper-button-white"></div>
                <div class="swiper-button-prev swiper-button-white"></div>
              </div><!-- /Swiper -->
            </div>
          </div>
        </section>

        <section class="pageCategory-section last"><a id="anchorPrice"></a>
          <div class="container tixPrice">
            <div class="row">
              <div class="col-sm-offset-1 col-sm-10 ">
                <div class="text-center">
                  <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                  <p>Select ticket</p>
                </div>
                <div class="clearfix">&nbsp;</div>
                <div class="table-responsive">
                  <table class="table infoTable-D table-bordered">
                      <thead>
                        <tr>
                          <th>Category</th>
                          <th>Price</th>
                          <th>Quantity</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                          <tr>
                            <td>Early Glow</td>
                            <td>RM 103.00</td>                          
                            <td>
                              <select class="form-control">
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                              </select>
                            </td>
                            <td>
                              <a class="btn btn-danger disabled" target="_blank" href="https://redtix-tickets.airasia.com/en-AU/events/neon%20countdownnye%202016/2016-12-31_18.00/sunway%20lagoon%20surf%20beach?back=2&area=86c4c5c3-1fa9-4b37-bc25-b772c4785a80&type=ga" role="button">BUY TICKETS</a>
                            </td>
                          </tr>
                          <tr>
                            <td>Normal Glow</td>
                            <td>RM 163.00</td>
                            <td>
                              <select class="form-control">
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                              </select>
                            </td>
                            <td>
                              <a class="btn btn-danger disabled" id="buyButton" target="_blank" href="https://redtix-tickets.airasia.com/en-AU/events/neon%20countdownnye%202016/2016-12-31_18.00/sunway%20lagoon%20surf%20beach?back=2&area=86c4c5c3-1fa9-4b37-bc25-b772c4785a80&type=ga" role="button">BUY TICKETS</a>
                            </td>
                          </tr>
                      </tbody>
                  </table>
                </div>
                {{-- <div class="buyAlert-bar">
                  <a class="btn btn-danger disabled" target="_blank" href="https://redtix-tickets.airasia.com/en-AU/events/neon%20countdownnye%202016/2016-12-31_18.00/sunway%20lagoon%20surf%20beach?back=2&area=86c4c5c3-1fa9-4b37-bc25-b772c4785a80&type=ga" role="button">BUY TICKETS</a>
                  <span class="or">/</span>
                  <span class="popData-btn" data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets <i class="fa fa-info-circle" aria-hidden="true"></i></span>
                </div> --}}

                <div class="note text-left">
                  <h2>Important Terms & Conditions</h2>
                  <ol>
                    <li>Your possession of this Ticket constitutes Your acceptance of these Terms and Conditions.</li>
                    <li>Tickets are personal revocable licences and shall at all times remain Our property and subject to these Terms and Conditions.</li>
                    <li>Upon purchase, please check Tickets carefully as mistakes cannot always be rectified after purchase.</li>
                    <li>You must retain this Ticket on Your person at all times during the Event.</li>
                    <li>Duplicate Tickets will not be issued in any circumstances.</li>
                    <li>Your Ticket may be invalidated if any part of it is removed, altered or defaced.</li>
                    <li>We reserve the right to conduct security searches from time to time, and to refuse admission to You, or eject You, from the Event and/or Venue should You refuse to comply with such security searches. No refunds will be given to You if You are refused entry or ejected due to Your own behaviour.</li>
                    <li>We reserve the right to confiscate any item, which in Our reasonable opinion may cause danger or disruption to any other persons at the Event or is one of the items not permitted in the Venue as listed on the Event website. Without limitation, the following may not be brought into the Venue: animals (except for assistance dogs); Any recording or transmitting equipment (including professional cameras); pyrotechnics or fireworks; Distress flares & flares; smoke bombs; laser pens; flags on poles; glass containers; items which may be regarded as weapons; illegal substances; legal highs (Inc. nitrous oxide); tables; chairs of any sort, including stools, floor seating and shooting sticks; barbeques and any kind of cooking apparatus; gazebos and parasols. Food hampers/cool bags/boxes; alcohol; large (golf) umbrellas bicycles, scooters, roller skates; drones.</li>
                  </ol>
                  <h2>Important Notes</h2>
                  <ol>
                    <li>Prices shown include RM3.00 AirAsiaRedTix fee & 6% GST.</li>
                    <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li>
                    <li>RM5.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                    <li>Online ticket selling will close 2 days prior to event day, subject to availability.</li>
                  </ol>
                  <h2>For enquiry only:</h2>
                  <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                </div>
              </div>
            </div>
          </div>
        </section>

        <section class="pageCategory-section last">
          <div class="buyNowSec" style="background-image: url('images/event2.jpg')">
            <div class="bannerOverlay"></div>
            <div class="container">
              <div class="col-sm-offset-1 col-sm-10 quoteArea">
                <h1>You’ve heard him on the radio, you’ve seen him on TV. It’s about time. See him live!</h1>
              </div>
              <div class="btnArea">
                <a class="btn btn-black btn-lg" target="_blank" href="https://redtix-tickets.airasia.com/en-AU/events/neon%20countdownnye%202016/2016-12-31_18.00/sunway%20lagoon%20surf%20beach?back=2&area=86c4c5c3-1fa9-4b37-bc25-b772c4785a80&type=ga" role="button">GET TICKET ONLINE NOW <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                <div class="clearfix">&nbsp;</div>
                <span data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets</span>
              </div>
            </div>
          </div>
        </section>

        <section class="pageCategory-section last">
          <div class="container">
            <div class="partner text-center">
                <h1 class="subSecTitle"><strong>PARTNERS</strong></h1>
                <div class="clearfix">&nbsp;</div>
                <ul class="list-unstyled list-inline partnerList">
                    <li><img src="images/neon/aabig_logo (offical partner).png" alt=""></li>
                    <li><img src="images/neon/BAC.png" alt=""></li>
                    <li><img src="images/neon/durex (official partner).png" alt=""></li>
                    <li><img src="images/neon/fac3 (co-organiser).jpg" alt=""></li>
                    <li><img src="images/neon/seven friday (official watch).jpeg" alt=""></li>
                    <li><img src="images/neon/sunway lagoon (official venue).png" alt=""></li>
                </ul>
            </div>
          </div>
        </section>

      </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 450;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });
    </script>
    
@endsection

@section('modal')

    <!--Modal Seat Plan image-->
    <div class="modal fade" id="enlargeImageModal" tabindex="-1" role="dialog" aria-labelledby="enlargeImageModal" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
            <div class="clearfix">&nbsp;</div>
            <img src="" class="enlargeImageModalSource" style="width: 100%;">
            </div>
        </div>
        </div>
    </div>

    <!-- Modal Where to Get Tix Location-->
    <div id="modalGetTixLoc" class="modal fade" role="dialog">
        <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
            <h2>GET YOUR TICKETS FROM:</h2>
            <dl class="dl-horizontal">
                <dt>Online:</dt>
                <dd><a href="http://www.airasiaredtix.com">www.AirAsiaRedtix.com</a></dd>
            </dl>
            <dl class="dl-horizontal">
                <dt>Rock Corner outlets:</dt>
                <dd>The Gardens, Mid Valley (TEL: 03 - 2201 4893)</dd>
                <dd>Subang Parade (TEL: 03 - 5613 1139)</dd>
                <dd>The Curve (TEL: 03 - 7733 1139)</dd>
            </dl>
            <dl class="dl-horizontal">
                <dt>Victoria Music outlets:</dt>
                <dd>Sg Wang(TEL: 03 - 2148 7208)</dd>
                <dd>Amcorp (TEL: 03 - 7956 0592)</dd>
            </dl>
            <dl class="dl-horizontal">
                <dt>Penang outlets:</dt>
                <dd>Artist Gallery, Gurney Plaza (04-637 0191)</dd>
                <dd>Artist Gallery, Queensbay Mall (04-228 5648)</dd>
            </dl>
            <dl class="dl-horizontal">
                <dt>Istana Budaya Jalan<br>Tun Razak:</dt>
                <dd>03 40265558</dd>
            </dl>
            </div>
        </div>
        </div>
    </div>

@endsection