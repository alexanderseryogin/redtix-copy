@extends('master')
@section('title')
    Motul Grand Prix of Japan
@endsection

@section('header')
    @include('layouts.partials._header')
    <meta property="og:url" content="{{ Request::fullUrl() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Motul Grand Prix of Japan" />
    <meta property="og:description" content="Motul Grand Prix of Japan8" />
    <meta property="og:image" content="{{ Request::Url().'images/motogpjapan/thumbnail.jpg' }}" />
@endsection

@section('content')
    <!-- Banner Section -->
    <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/motogpjapan/web-banner.jpg')}}" style="width: 100%" class="img-responsive" alt="Motul Grand Prix of Japan"></div>
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            <img src="{{asset('images/motogpjapan/thumbnail-test.jpg')}}" style="width: 100%" class="img-responsive" alt="Motul Grand Prix of Japan">
        </div>
    </section>
    <!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
            <div class="row priceNbtn">
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="row">
                        <div class="col-sm-9 leftBox">
                            <h6>Motul Grand Prix of Japan</h6>Tickets from <span>JPY14,000 / RM513</span>
                        </div>
                        <div class="col-sm-3 text-center">
                            <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar section-white">
            <section class="pageCategory-section last">
                <div class="container intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i> 19th - 21th October 2018</div>
                            <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i> Twin Ring Motegi, Japan <a target="_blank" href="https://goo.gl/maps/5XB5ev6VdLE2"> View Map</a></div>
                            <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> 8.00 a.m </div>
                            <div class="clearfix">&nbsp;</div>
                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <div class="addthis_inline_share_toolbox"></div>
                            <!-- /sharing -->
                            <hr>
                            <p>Last years lap of Japan's Grand Prix had the whole world watching, it was a battle that no one expected. For this season, the battle among the rival teams have already heated up. The drama unique to MotoGP is drawing attention from everyone around the world. Marquez is the king of HONDA and Rossi leading the legendary team YAMAHA, SUZUKI is aiming for their second victory after recovering well, Nakagami who wants to conquer the race at his homeland, and DUCATI who wants to win, even when racing away from their home. At the Twin Ring Motegi, which is the homeland of Japanese motorcycle makers all riders and teams will fight for their honour and pride.</p>
                            <p>We have 3 whole days full of activities (at the circuit) and events that has been powered up for the Japan Moto GP. This year we have powered up the “Lodging Area” and other “Events”. On the eve of finale, many riders are scheduled to appear on stage like last year. Among others, Marquez and Pedrosa will also be on stage. This is also the second year we will have an event at the lodging area “Overnight Square”, which will comprise of gourmet and stage events. There will also be many other guests that will surprise you!</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="col-sm-1 clearfix"></div>
                            <div class="col-sm-10">
                                <div class="embed-responsive embed-responsive-16by9">
                                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/lXI89MS5AdE?ecver=2" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </section>
            
            <section class="pageCategory-section last section-grey">
                <div class="container">
                    <div class="gallery text-center">
                        <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
                        <!-- Swiper -->
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <a href="images/motogpjapan/gallery-1.jpg" data-featherlight="image"><img class="" src="images/motogpjapan/gallery-1.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/motogpjapan/gallery-2.jpg" data-featherlight="image"><img class="" src="images/motogpjapan/gallery-2.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/motogpjapan/gallery-3.jpg" data-featherlight="image"><img class="" src="images/motogpjapan/gallery-3.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/motogpjapan/gallery-4.jpg" data-featherlight="image"><img class="" src="images/motogpjapan/gallery-4.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/motogpjapan/gallery-5.jpg" data-featherlight="image"><img class="" src="images/motogpjapan/gallery-5.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/motogpjapan/gallery-6.jpg" data-featherlight="image"><img class="" src="images/motogpjapan/gallery-6.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/motogpjapan/gallery-7.jpg" data-featherlight="image"><img class="" src="images/motogpjapan/gallery-7.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/motogpjapan/gallery-8.jpg" data-featherlight="image"><img class="" src="images/motogpjapan/gallery-8.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/motogpjapan/gallery-9.jpg" data-featherlight="image"><img class="" src="images/motogpjapan/gallery-9.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/motogpjapan/gallery-10.jpg" data-featherlight="image"><img class="" src="images/motogpjapan/gallery-10.jpg" alt=""></a>
                                </div>

                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next swiper-button-white"></div>
                            <div class="swiper-button-prev swiper-button-white"></div>
                        </div><!-- /Swiper -->
                    </div>
                </div>
            </section>


            <!-- Sponsors -->
            <section class="pageCategory-section last">
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                            <div class="table-responsive">
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th colspan="2">FRIDAY</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Moto3™ Free Practice 1</td>
                                            <td>09:00-09:40</td>
                                        </tr>
                                        <tr>
                                            <td>MotoGP™ Free Practice 1</td>
                                            <td>09:55-10:40</td>
                                        </tr>
                                        <tr>
                                            <td>Moto2™ Free Practice 1</td>
                                            <td>10:55-11:40</td>
                                        </tr>
                                        <tr>
                                            <td>Moto3™ Free Practice 2</td>
                                            <td>13:10-13:50</td>
                                        </tr>
                                        <tr>
                                            <td>MotoGP™ Free Practice 2</td>
                                            <td>14:05-14:50</td>
                                        </tr>
                                        <tr>
                                            <td>Moto2™ Free Practice 2</td>
                                            <td>15:05-15:50</td>
                                        </tr>
                                    </tbody>
                                </table>

                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th colspan="2">SATURDAY</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                            Moto3™ Free Practice 3</td>
                                            <td>09:00-09:40</td>
                                            </tr>
                                            <tr>
                                            <td>MotoGP™ Free Practice 3</td>
                                            <td>09:55-10:40</td>
                                            </tr>
                                            <tr>
                                            <td>Moto2™ Free Practice 3</td>
                                            <td>10:55-11:40</td>
                                            </tr>
                                            <tr>
                                            <td>Moto3™ Qualifying</td>
                                            <td>12:35-13:15</td>
                                            </tr>
                                            <tr>
                                            <td>MotoGP™ Free Practice 4</td>
                                            <td>13:30-14:00</td>
                                            </tr>
                                            <tr>
                                            <td>MotoGP™ Qualifying 1</td>
                                            <td>14:10-14:25</td>
                                            </tr>
                                            <tr>
                                            <td>MotoGP™ Qualifying 2</td>
                                            <td>14:35-14:50</td>
                                            </tr>
                                            <tr>
                                            <td>Moto2™ Qualifying</td>
                                            <td>15:05-15:50</td>

                                        </tr>
                                    </tbody>
                                </table>

                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th colspan="2">SUNDAY</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                            Moto3™ Warm Up</td>
                                            <td>08:40-09:00</td>
                                        </tr>
                                        <tr>
                                            <td>Moto2™ Warm Up</td>
                                            <td>09:10-09:30</td>
                                        </tr>
                                        <tr>
                                            <td>MotoGP™ Warm Up</td>
                                            <td>09:40-10:00</td>
                                        </tr>
                                        <tr>
                                            <td>Moto3™ Race</td>
                                            <td>11:00</td>
                                        </tr>
                                        <tr>
                                            <td>Moto2™ Race</td>
                                            <td>12:20</td>
                                        </tr>
                                        <tr>
                                            <td><b>MotoGP™ Race</td>
                                            <td>14:00</b></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2"><i>*Please be aware that the programme is subject to change. All times listed are local (time zone of the event location</i></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-sm-1 clearfix">
                        </div>
                    </div>
                </div>
            </section>
            <!-- /Sponsors -->


            <section class="pageCategory-section last"><a id="anchorPrice"></a>
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                            <div class="text-center">
                                <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                                <p>Select ticket</p>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="table-responsive">
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Category</th>
                                            <th>Normal</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td style="background-color: #318c53; color: #fff;">Victory Stand V6 (Fri-Sun)</td>
                                            <td>JPY19,000 / RM696.00</td>
                                        </tr>
                                        <tr>
                                            <td style="background-color: #ffd542;">Grandstand Z6 (Fri-Sun)</td>
                                            <td>JPY14,000 / RM513.00</td>
                                        </tr>
                                        <tr>
                                            <td style="background-color: #03a7a0; color: #fff;">Grandstand A (Fri-Sun)</td>
                                            <td>JPY14,000 / RM513.00</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="buyAlert-bar">
                                <a class="btn btn-danger" id="buyButton" datetime="Oct 18 2018 00:00:00 GMT+0800" target="_blank" href="https://redtix-tickets.airasia.com/en-AU/shows/motul grand prix of japan 2018/events" role="button">BUY TICKETS</a>
                                {{-- <span class="or">/</span>
                                <span class="popData-btn" data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets <i class="fa fa-info-circle" aria-hidden="true"></i></span> --}}
                            </div>
                            {{-- <span class="importantNote">* Additional charges will be applied for credit/debit card transaction, optionally customer may pay cash for outlet purchase to avoid the charges.</span> --}}
                            <div class="note text-left">
                                <h2>Important Notes</h2>
                                <ol>
                                    <li>Prices shown excludes RM14 AirAsiaRedTix fee and 3% Credit card fee.</li>
                                    <li>All ticket prices will be paid in Malaysian Ringgit. Japanese Yen prices are shown for reference purposes only.</li>
                                    <li>Prices quoted in YEN may differ due to currency fluctuations.</li>
                                    <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li>
                                    <li>RM10.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                                    <li>Online ticket selling will close 1 days prior to event day, subject to availability.</li>
                                    {{-- <li>Adult: 18 years old and above.</li> --}}
                                </ol>
                                <h2>For enquiry only:</h2>
                                <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 350;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });
    </script>

    {{-- Buy button disable --}}
    <script type="text/javascript">
        $(function() {
            $('a[id^=buyButton]').each(function() {
                var date = new Date();
                var enddate = $(this).attr('datetime'); 
                if ( Date.parse(date) >= Date.parse(enddate)) {
                  $(this).addClass('disabled');
                }
            });
        });
    </script>

@endsection

@section('modal')
    @include('layouts.partials.modals._seatplan')
    @include('layouts.partials.modals._getTix')
@endsection