@extends('master')
@section('title')
	 UFC Adelaide
@endsection

@section('header')
	@include('layouts.partials._header')
	
@endsection

@section('content')
<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
<link type="text/css" rel="stylesheet" href="css/clock.css">
<link type="text/css" rel="stylesheet" href="css/timeTo.css">
<link type="text/css" rel="stylesheet" href="css/custom2.css">
<link type="text/css" rel="stylesheet" href="css/ufc221.css">
<style type="text/css">
	.background-full {
		background-image: url('/images/ufcadelaide2018/background.jpg');
	    background-size: 100% auto;
		background-repeat: no-repeat;
		height: 950px;
	}
    
    .sect-count p img{
        padding-top: 100px;
		width: 40%;
    }
	.open-sans {
		font-family: 'Open Sans', sans-serif;
        font-weight: 100;
	}
	.h2-transform {
        font-size: 18px;
        text-transform: uppercase;
	}
	.btn-danger {
		background: linear-gradient(#b82941, #40151c);
	}
	.btn-wrapper {
		margin-top: 20px;
	}
	
	@media screen and (max-width: 760px){ 
		.background-full {
			background-image: url('/images/ufcadelaide2018/mobile-background.jpg');	
		}
		.btn-wrapper {
			margin-top: 20px;
		}

	}
	@media screen and (min-width: 768px){ 
		.background-full { 
			height: 950px;
		}
	}
	@media screen and (min-width: 1024px){ 
		.background-full { 
   			height: 950px;
		}
	}
	@media screen and (max-width: 1024px){ 
		.background-full { 
			height: 800x;
		}
	}
	@media screen and (min-width: 768px) and (max-width: 1024px){ 
		.background-full {
			height: 950px;
		}
	}
	@media screen and (min-width: 1024px) and (max-width: 1366px){ 
		.background-full{
			height: 950px;
		}
	}
	@media screen and (min-width: 1400px){ 
		.background-full{
			height: 950px;
		}
	}
	@media screen and (max-width: 414px){ 
		.background-full { 
			height: 600px;
		}
        .sect-count p img{
        	padding-top: 10px;
			width: 60%;
    	}
	}
    
	.embed-responsive-item {
		padding-top: 20px;
	}
</style>

<section class="pageContent">
      <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar" style="padding-top: 0px;">
            <section class="pageCategory-section last section-black background-full">
                <div class="container intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 btn-wrapper text-center"> 
						    {{-- <a class="btn btn-danger" href="https://docs.google.com/forms/d/1ZmS66gXYIW8tawDoe9F3Y7ALPck-yNPL1fKp16Gjfws/viewform" role="button">Register</a> --}}
                        </div>
                    </div>
            </section>

            <section class="pageCategory-section last section-black">
                <div class="container intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 text-center">
                        
                        <h2 class="open-sans h2-transform" style="color: #fff; padding-top:1px;">Be the first to get notified when tickets go on sale!<br />
                       	</div>
                    </div>
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 btn-wrapper text-center"> 
                            <a class="btn btn-danger" href="https://docs.google.com/forms/d/1ZmS66gXYIW8tawDoe9F3Y7ALPck-yNPL1fKp16Gjfws/viewform" role="button">Register</a>
                        </div>
                    </div>
            </section>


        </div><!-- /Main Body -->
    </section><!-- /Content Section -->
@endsection

@section('customjs')
	{{-- countdown --}}
	<script src="js/jquery.time-to.js"></script>
	<script type="text/javascript">
		$('#countdown').timeTo({
		    timeTo: new Date(new Date('Saturday June 23 2018 00:00:00 GMT+0800 (+08)')),
		    displayDays: 2,
		    theme: "black",
		    displayCaptions: true,
		    fontSize: 48,
		    captionSize: 14
		}); 
	</script>
	{{-- /countdown --}}

	<script type="text/javascript">
	//Initialize Swiper
	var swiper = new Swiper('.swiper-container', {
		pagination: '.swiper-pagination',        
		paginationClickable: true,
		slidesPerView: 'auto',
		spaceBetween: 10,
		nextButton: '.swiper-button-next',
		prevButton: '.swiper-button-prev',
		freeMode: true
	});

	// Enlarge Seat Plan Image
	$(function() {
		$('.seatPlanImg').on('click', function() {
		$('.enlargeImageModalSource').attr('src', $(this).attr('src'));
		$('#enlargeImageModal').modal('show');
		});
	});

	// Hide top Banner when page scroll
	var header = $('.eventBanner');
	var range = 450;

	$(window).on('scroll', function () {
		
		var scrollTop = $(this).scrollTop();
		var offset = header.offset().top;
		var height = header.outerHeight();
		offset = offset + height;
		var calc = 1 - (scrollTop - offset + range) / range;

		header.css({ 'opacity': calc });

		if ( calc > '1' ) {
		header.css({ 'opacity': 1 });
		} else if ( calc < '0' ) {
		header.css({ 'opacity': 0 });
		}
	});

	// Smooth scroll for acnhor links
	$('a[href*="#"]:not([href="#"])').click(function() {
		if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
		var target = $(this.hash);
		target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
		if (target.length) {
			$('html, body').animate({
			scrollTop: target.offset().top
			}, 1000);
			return false;
		}
		}
	});

	</script>

	{{-- Buy button disable --}}
	<script type="text/javascript">
		$(function() {
			$('a[id^=buyButton]').each(function() {
				var date = new Date();
				var enddate = $(this).attr('datetime'); 
				if ( Date.parse(date) >= Date.parse(enddate)) {
				  $(this).addClass('disabled');
				}
			});
		});
	</script>
	
@endsection

@section('modal')
	@include('layouts.partials.modals._seatplan')
	@include('layouts.partials.modals._getTixCustom')

@endsection