@extends('master')
@section('title')
    Sunburn Festival Pune 2018
@endsection

@section('header')
    @include('layouts.partials._header')
    <meta property="og:url" content="{{ Request::fullUrl() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Sunburn Festival Pune 2018" />
    <meta property="og:description" content="Originating in Amsterdam, the Netherlands, Sensations RISE is said to symbolize breaking free from the worries and boundaries of daily life, letting go, and rising above yourself to ultimate euphoria."/>
    <meta property="og:image" content="{{ Request::Url().'images/sunburn2018/thumbnail.jpg' }}" />
@endsection

@section('content')
    <!-- Banner Section -->
    <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/sunburn2018/web-banner.jpg')}}" style="width: 100%" class="img-responsive" alt="Sunburn Festival Pune 2018"></div>
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            <img src="{{asset('images/sunburn2018/thumbnail.jpg')}}" style="width: 100%" class="img-responsive" alt="Sunburn Festival Pune 2018">
        </div>
    </section>
    <!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
            <div class="row priceNbtn">
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="row">
                        <div class="col-sm-9 leftBox">
                            <h6>Sunburn Festival Pune 2018</h6> Tickets from <span>RM243</span>
                        </div>
                        <div class="col-sm-3 text-center">
                            <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar section-white">
            <section class="pageCategory-section last">
                <div class="container intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i>  29th - 31st December 2018</div>
                            <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i>  Oxford Golf Resort, Pune, India <a target="_blank" href="https://goo.gl/maps/Bk4MymwM7on">View Map</a></div>
                            {{-- <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> To be announced</div> --}}
                            <div class="clearfix">&nbsp;</div>
                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <div class="addthis_inline_share_toolbox"></div>
                            <!-- /sharing -->
                            <hr>
                            <p><h2>Sunburn Festival Pune 2018</h2><br/>
                            The Sunburn festival is back with its 12th edition! Experience the best of dance music from across the globe at the Sunburn Festival 2018!</p>
                            <p>Sunburn is amongst the world's biggest music festivals. In the past eleven years of its existence, Sunburn has brought together renowned Indian and international artists to entertain hundreds of thousands of dance music lovers through its various formats that reach out to fans across India & Asia. Sunburn caters to a wide Indian and International audience and has positioned India as a prime dance festival destination to the world.</p>
                            <div class="clearfix"></div>
                            <div class="embed-responsive embed-responsive-16by9" style="margin-top: 40px; margin-bottom: 40px;">
                                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/CZChE7G90G0?ecver=2" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- <div class="clearfix"></div> --}}
            </section>

            {{-- <section class="pageCategory-section last section-grey">
                <div class="container">
                    <div class="gallery text-center">
                        <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
                        <!-- Swiper -->
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <a href="images/sunburn2018/gallery-1.jpg" data-featherlight="image"><img class="" src="images/sunburn2018/gallery-1.jpg" alt=""></a>
                                </div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next swiper-button-white"></div>
                            <div class="swiper-button-prev swiper-button-white"></div>
                        </div><!-- /Swiper -->
                    </div>
                </div>
            </section> --}}

            <section class="pageCategory-section last"><a id="anchorPrice"></a>
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                            <div class="text-center">
                                <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                                <p>Select ticket</p>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="table-responsive">
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Category</th>
                                            <th>Ticket Price</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>GA Festival (3 day)</td>
                                            <td>RM378.00</td>
                                        </tr>
                                        <tr>
                                            <td>VIP Festival (3 day)</td>
                                            <td>RM629.00</td>
                                        </tr>
                                        <tr>
                                            <td>GA Festival - 2 tickets (3 day)</td>
                                            <td>RM629.00</td>
                                        </tr>
                                        <tr>
                                            <td>VIP Festival - 2 tickets (3 day)</td>
                                            <td>RM1,002.00</td>
                                        </tr>
                                        <tr style="background-color: #FCEFDC;">
                                            <td>GA Day 1 & 2</td>
                                            <td>RM251.00</td>
                                        </tr>
                                        <tr style="background-color: #FCEFDC;">
                                            <td>VIP Day 1 & 2</td>
                                            <td>RM439.00</td>
                                        </tr>
                                        <tr style="background-color: #FCEFDC;">
                                            <td>GA Day 1 & 2 - 2 tickets</td>
                                            <td>RM439.00</td>
                                        </tr>
                                        <tr style="background-color: #FCEFDC;">
                                            <td>VIP Day 1 & 2 - 2 tickets</td>
                                            <td>RM751.00</td>
                                        </tr>
                                        <tr>
                                            <td>GA Day 2 & 3</td>
                                            <td>RM279.00</td>
                                        </tr>
                                        <tr>
                                            <td>VIP Day 2 & 3</td>
                                            <td>RM501.00</td>
                                        </tr>
                                        <tr>
                                            <td>GA Day 2 & 3 - 2 tickets</td>
                                            <td>RM471.00</td>
                                        </tr>
                                        <tr>
                                            <td>VIP Day 2 & 3 - 2 tickets</td>
                                            <td>RM814.00</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="buyAlert-bar">
                                <a class="btn btn-danger" id="buyButton" datetime="Dec 31 2018 00:00:00 GMT+0800" target="_blank" href="https://redtix-tickets.airasia.com/en-AU/shows/sunburn festival pune 2018 (29th-31st dec 2018)/events" role="button">BUY TICKETS</a>
                                {{-- <span class="or">/</span>
                                <span class="popData-btn" data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets <i class="fa fa-info-circle" aria-hidden="true"></i></span> --}}
                            </div>
                            
                            <div class="note text-left">
                                <h2>Important Notes</h2>
                                <ol>
                                    {{-- <li>Prices shown exclude ticket fee.</li> --}}
                                    <li>All ticket prices will be paid in Malaysian Ringgit.</li>
                                    <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li>
                                    <li>RM10.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                                    <li>Online ticket selling will close 2 days prior to event day, subject to availability.</li>
                                    <li>Adult: Strictly 15 years old and above.</li>
                                </ol>
                                <h2>For enquiry only:</h2>
                                <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                            </div>

                        </div>
                    </div>
                </div>
            </section>
        </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 350;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });
    </script>

    {{-- Buy button disable --}}
    <script type="text/javascript">
        $(function() {
            $('a[id^=buyButton]').each(function() {
                var date = new Date();
                var enddate = $(this).attr('datetime'); 
                if ( Date.parse(date) >= Date.parse(enddate)) {
                  $(this).addClass('disabled');
                }
            });
        });
    </script>

@endsection

@section('modal')
    @include('layouts.partials.modals._seatplan')
    @include('layouts.partials.modals._getTix')
@endsection