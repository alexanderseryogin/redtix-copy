@extends('master')
@section('title')
    KLIMS 2018
@endsection

@section('header')
    @include('layouts.partials._header')
    <meta property="og:url" content="{{ Request::fullUrl() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Kuala Lumpur International Motorshow 2018" />
    <meta property="og:description" content="Kuala Lumpur International Motorshow 2018" />
    <meta property="og:image" content="{{ Request::Url().'images/klims2018/thumbnail.jpg' }}" />
@endsection

@section('content')
    <!-- Banner Section -->
    <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/klims2018/web-banner.jpg')}}" style="width: 100%" class="img-responsive" alt="Kuala Lumpur International Motorshow 2018"></div>
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            <img src="{{asset('images/klims2018/thumbnail.jpg')}}" style="width: 100%" class="img-responsive" alt="Kuala Lumpur International Motorshow 2018">
        </div>
    </section>
    <!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
            <div class="row priceNbtn">
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="row">
                        <div class="col-sm-9 leftBox">
                            <h6>Kuala Lumpur International Motorshow 2018</h6>Tickets from <span>RM5</span>
                        </div>
                        <div class="col-sm-3 text-center">
                            <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar section-white">
            <section class="pageCategory-section last">
                <div class="container intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i>  23rd Nov - 2nd Dec 2018</div>
                            <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i> Malaysia International Trade and Exhibition Centre, Kuala Lumpur <a target="_blank" href="https://goo.gl/maps/iU1WmWXZpHD2">View Map</a></div>
                            <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> Mon - Fri : 11.00am - 10.00pm</div>
                            <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> Sat - Sun : 10.00am - 10.00pm</div>
                            <div class="clearfix">&nbsp;</div>
                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <div class="addthis_inline_share_toolbox"></div>
                            <!-- /sharing -->
                            <hr>
                            <p>Kuala Lumpur International Motor Show 2018 (KLIMS18) is billed as the largest and most significant motor show in Malaysia.<br />
                            The 10-day show is set to showcase a range of motoring machines from passenger vehicles, commercial vehicles, motorcycles, accessories, etc. There will be concept cars, performance cars, new model launches, and many exciting activities.</p>
                            <p>This 9th edition of KLIMS enters a new era as for the first time it will be held at the country’s newest and largest exhibition centre i.e. Malaysia International Trade and Exhibition Centre (MITEC).</p>
                            <p><b>Beyond Mobility</b><br />
                            As KLIMS reinvents itself to include more than automobiles, visitors can expect a showcase that offers innovative technologies amidst exhibits that demonstrate a connected lifestyle.<br />
                            <ul>
                            <li>model launches</li>
                            <li>provocative concept cars</li>
                            <li>thrilling performance cars</li>
                            <li>bespoke cars</li>
                            <li>the spectrum of wheels from motor bikes, commercial vehicles ,etc.</li>
                            <li>automotive products and services</li>
                            </ul>
                            </p>
                        </div>
                    </div>
                </div>
                {{-- <div class="clearfix"></div> --}}
            </section>


            <section class="pageCategory-section last"><a id="anchorPrice"></a>
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-2 col-sm-8 ">
                            <img src="{{asset('images/klims2018/klims2018-lucky-draw5.jpg')}}" class="img-responsive">
                        </div>
                        <div class="col-sm-offset-1 col-sm-10 ">
                            <p>Each online purchase is entitled to one (1) entry into our fortnightly lucky draw contest to stand a chance to win a return flight to the following destinations:</p>
                            <div class="table-responsive">
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th colspan="3">Category</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><b>Destinations</b></td>
                                            <td><b>Contest Period</b></td>
                                            <td><b>Winners</b></td>
                                        </tr>
                                        <tr>
                                            <td>Bangkok</td>
                                            <td>3rd Sept 2018 - 16th Sept 2018</td>
                                            <td>Chew Jin Aun</a>
                                        </tr>
                                        <tr>
                                            <td>Hong Kong</td>
                                            <td>17th Sept 2018 - 30th Sept 2018</td>
                                            <td>Chung Chiang Tang</td>
                                        </tr>
                                        <tr>
                                            <td>Bali</td>
                                            <td>1st Oct 2018 - 14th Oct 2018</td>
                                            <td>Khalil Izzat</td>
                                        </tr>
                                        <tr>
                                            <td>Taipei</td>
                                            <td>15th Oct 2018 - 28th Oct 2018</td>
                                            <td>Thor Kok Chun</td>
                                        </tr>
                                        <tr>
                                            <td>Jakarta</td>
                                            <td>29th Oct 2018 - 11th Nov 2018</td>
                                            <td>-</td>
                                        </tr>
                                        <tr>
                                            <td>Shanghai</td>
                                            <td>12th Nov 2018 - 2nd Dec 2018</td>
                                            <td>-</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        
                        <div class="col-sm-offset-1 col-sm-10 ">
                            <p><br/>In addition, every online purchasers shall also stand a chance to win a return flight to <font style="color:red;">TOKYO</font>.</p>
                            <div class="table-responsive">
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th colspan="2">Grand Prize</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Tokyo</td>
                                            <td>3rd Sept 2018 - 2nd Dec 2018</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <p>Terms and Conditions applies. You may view it here: <a href="{{asset('document/klims2018event-terms-conditions.pdf')}}">Lucky Draw Terms and Conditions </a>
                        </div>
                        
                    </div>
                </div>
            </section>



            <section class="pageCategory-section last section-grey">
                <div class="container">
                    <div class="gallery text-center">
                        <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
                        <!-- Swiper -->
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <a href="images/klims2018/gallery-1.jpg" data-featherlight="image"><img class="" src="images/klims2018/gallery-1.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/klims2018/gallery-2.jpg" data-featherlight="image"><img class="" src="images/klims2018/gallery-2.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/klims2018/gallery-3.jpg" data-featherlight="image"><img class="" src="images/klims2018/gallery-3.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/klims2018/gallery-4.jpg" data-featherlight="image"><img class="" src="images/klims2018/gallery-4.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/klims2018/gallery-5.jpg" data-featherlight="image"><img class="" src="images/klims2018/gallery-5.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/klims2018/gallery-6.jpg" data-featherlight="image"><img class="" src="images/klims2018/gallery-6.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/klims2018/gallery-7.jpg" data-featherlight="image"><img class="" src="images/klims2018/gallery-7.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/klims2018/gallery-8.jpg" data-featherlight="image"><img class="" src="images/klims2018/gallery-8.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/klims2018/gallery-9.jpg" data-featherlight="image"><img class="" src="images/klims2018/gallery-9.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/klims2018/gallery-10.jpg" data-featherlight="image"><img class="" src="images/klims2018/gallery-10.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/klims2018/gallery-11.jpg" data-featherlight="image"><img class="" src="images/klims2018/gallery-11.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/klims2018/gallery-12.jpg" data-featherlight="image"><img class="" src="images/klims2018/gallery-12.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/klims2018/gallery-13.jpg" data-featherlight="image"><img class="" src="images/klims2018/gallery-13.jpg" alt=""></a>
                                </div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next swiper-button-white"></div>
                            <div class="swiper-button-prev swiper-button-white"></div>
                        </div><!-- /Swiper -->
                    </div>
                </div>
            </section>

            <section class="pageCategory-section last"><a id="anchorPrice"></a>
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                            <div class="text-center">
                                <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                                <p>Select ticket</p>
                            </div>

                            <div class="clearfix">&nbsp;</div>
                            <div class="table-responsive">
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Category</th>
                                            <th>Weekdays (per entry)</th>
                                            <th>Weekends (per entry)</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Adults</td>
                                            <td>RM 20</td>
                                            <td>RM 25</td>
                                        </tr>
                                        <tr>
                                            <td>Child<br/>(12 years old &amp; below)</td>
                                            <td>RM 5</td>
                                            <td>RM 5</td>
                                        </tr>
                                        <tr>
                                            <td>Family Package<br/>(2 adults with max. 5 children of age 12 years old and below)</td>
                                            <td>RM 40</td>
                                            <td>RM 50</td>
                                        </tr>
                                        <tr>
                                            <td>Student</td>
                                            <td>RM 5<br/>(with Student ID card / school uniform)</td>
                                            <td>N/A</td>
                                        </tr>
                                        <tr>
                                            <td>Handicapped</td>
                                            <td colspan="2">Complimentary Entry<br /> (with handicapped card / physically handicapped)<br />FREE for accompanying person, 1 ticket only</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                             <div class="buyAlert-bar">
                                <a class="btn btn-danger" id="buyButton" datetime="Dec 2 2018 00:00:00 GMT+0800" target="_blank" href="https://redtix-tickets.airasia.com/en-AU/events/kuala%20lumpur%20international%20motor%20show%20'18/2018-11-23_10.00/malaysia%20international%20trade%20-%20exhibition%20centre%20(mitec),%20kl?back=2&area=2c28c4d5-e88f-e811-80da-9c8e991e54fc&type=ga" role="button">BUY TICKETS</a>
                                {{-- <span class="or">/</span>
                                <span class="popData-btn" data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets <i class="fa fa-info-circle" aria-hidden="true"></i></span> --}}
                            </div>

                            {{-- <span class="importantNote">*Additional charges will be applied for credit/debit card transaction, optionally customer may pay cash for outlet purchase to avoid the charges.</span> --}}
                            {{-- <div class="note text-left">
                                <h2>Reminder</h2>
                                <ol>
                                    <li><b>Dress code</b> : Neat and decent. Worn out / faded / shabby jeans, t-shirt without collar, shorts, slippers, and sandals are STRICTLY NOT ALLOWED.</li>
                                    <li><b>Age limit to show</b> : 4 years old and above. Every person/kid needs ticket to own seat.</li>
                                    <li>Food and drinks are not allowed in the auditorium.</li>
                                </ol>
                            </div> --}}

                            <div class="note text-left">
                                <h2>Why Purchase Online?</h2>
                                <ul>
                                    <li>Skip the queues - Dedicated redemption counter for online purchases</li>
                                    <li>1 x entry to Fortnightly Lucky Draw to win a return flight to various destinations.</li>
                                    <li>1 x entry to the Lucky Draw Contest to win a return flight to Japan.</li>
                                    <li>1 x entry in KLIMS18 lucky draw to win a brand new Toyota C-HR, Honda City 1.5S and Perodua MyVi 1.3G.</li>
                                </ul>
                            </div>
                            <div class="note text-left">
                                <img src="{{asset('images/klims2018/klims2018-car-prizes.jpg')}}" class="img-responsive">
                            </div>
                            <div class="note text-left">
                                <h2>Important Notes</h2>
                                <ol>
                                    {{-- <li>Prices shown include RM4.00 AirAsiaRedTix fee & other taxes.</li>
                                    <li>Prices shown exclude RM4.00 AirAsiaRedTix fee.</li> --}}
                                    <li>Transaction fee of RM4.00 per event applicable for Internet purchase.</li>
                                    <li>RM5.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                                    <li>Weekday Pass is eligible to any weekdays entry and weekend pass is eligible to any weekends entry within event period (23rd November - 2nd December 2018). Each ticket is eligible for one (1) entry only.</li>
                                    <!-- <li>Strictly no replacement for missing tickets, torn tickets and cancellation.</li> -->
                                    <li>Online ticket selling will close 1 days prior to event day, subject to availability.</li>
                                </ol>
                                <h2>For enquiry only:</h2>
                                <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 350;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });
    </script>

    {{-- Buy button disable --}}
    <script type="text/javascript">
        $(function() {
            $('a[id^=buyButton]').each(function() {
                var date = new Date();
                var enddate = $(this).attr('datetime'); 
                if ( Date.parse(date) >= Date.parse(enddate)) {
                  $(this).addClass('disabled');
                }
            });
        });
    </script>

@endsection

@section('modal')
    @include('layouts.partials.modals._seatplan')
    @include('layouts.partials.modals._getTix')
@endsection