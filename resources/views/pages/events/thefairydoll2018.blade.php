@extends('master')
@section('title')
    The Fairy Doll
@endsection

@section('header')
    @include('layouts.partials._header')
    <meta property="og:url" content="{{ Request::fullUrl() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="The Fairy Doll" />
    <meta property="og:description" content="The Fairy Doll, or Die Puppenfee, was premiered at the Vienna Court Opera on the 4th October 1888, a ballet which owes its inspiration to E.T.A Hoffman’s 1815 story the Sandman in which a mechanical doll comes to life."/>
    <meta property="og:image" content="{{ Request::Url().'images/thefairydoll2018/thumbnail.jpg' }}" />
@endsection

@section('content')
    <!-- Banner Section -->
    <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/thefairydoll2018/web-banner.jpg')}}" style="width: 100%" class="img-responsive" alt="The Fairy Doll"></div>
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            <img src="{{asset('images/thefairydoll2018/thumbnail.jpg')}}" style="width: 100%" class="img-responsive" alt="The Fairy Doll">
        </div>
    </section>
    <!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
            <div class="row priceNbtn">
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="row">
                        <div class="col-sm-9 leftBox">
                            <h6>The Fairy Doll</h6> Tickets <span>RM88</span>
                        </div>
                        <div class="col-sm-3 text-center">
                            <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar section-white">
            <section class="pageCategory-section last">
                <div class="container intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i>  18th - 19th January 2019</div>
                            <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i> Istana Budaya, Kuala Lumpur <a target="_blank" href="https://goo.gl/maps/xDVBXk3rECr">View Map</a></div>
                            <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> 8.30 pm - 10.00 pm</div>
                            <div class="clearfix">&nbsp;</div>
                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <div class="addthis_inline_share_toolbox"></div>
                            <!-- /sharing -->
                            <div class="col-sm-12">
                                <hr>
                                <p><h2>The Fairy Doll</h2><br/><br/>
                                First Act<br/>
                                The owner of a toy store shows his customers all the beautiful dolls in his shop. He has distinctive dolls from different countries, including a doll that blows kisses, a doll from Austria, dolls from India, a doll that talks and many Harlequins. The young assistant helps the owner taking care of the dolls and enjoys dancing with them.</p>
                                <p>The customers seem tired and uninterested. Finally, the store owner brings the most beautiful doll of all - The Fairy Doll. The stunning and charming doll dances in front of all customers. With her magic powers, she puts adults, including the store owners and parents, to sleep.</p>
                                <p>Second Act<br/>
                                The Fairy Doll takes the young assistance and children around the world. First she takes her guests to visit the French doll which celebrates with joy. Then the tin soldier dances with dozens of beautiful dolls; the Bunny plays her drum, and Harlequins accompany her with their cymbals. In Spain, the doll dances and jumps while the Chinese dolls shows their beautiful and exotic laps. As two happy and colourful dolls amuse the other dolls dancing to a vibrant rhythm, the characters of a Music Box come to life. The Japanese doll elegantly dances with her enigmatic fan, and Russian dolls twirl around while Korean dolls plays with their drums. Two Pierrots dances with Fairy Doll as the exciting journey ends.</p>
                                <p>As children go back to the store, the adults awake from a dream of the very same journey. They found the purity of joy within the adventure and enjoys playing the dolls in harmony. The Fairy Doll has taught everyone to find their innocence and to think young and be positive each and every single day. They realize that age is insignificant. To be young at heart is to love life, wake up every day and enjoy that day as a gift.
                                </p><br/><br/>
                            </div>
                            <div class="clear-fix"></div>
                            <div class="col-sm-12">
                                <h2>Biography</h2>
                                <p>Guest are principle dancer of  Arts Ballet Theatre of Florida.</p>
                                <hr>
                            </div>
                            <div class="col-sm-12">
                                <div class="col-sm-4">
                                    <p><img class="img-responsive text-center" src="images/thefairydoll2018/janis-liu.png" alt=""></p>
                                </div>
                                <div class="col-sm-8">
                                    <p><b>Janis Liu</b><br/>
                                    Born in Hong Kong and began her ballet training under Ivy Chung School Of Ballet in Hong Kong.  After being accepted into the pre-professional program at Pittsburgh Ballet Theatre School in 2012, she joined Coastal City Ballet of Vancouver, Canada in 2013 and until 2016 she was a soloist at Columbia Classical Ballet in U.S.A.</p>
                                    <p>Janis was a Guest Principal Dancers with Charlestion Ballet, WV, U.S.A. in 2015, Gulf Coast Ballet, MS, U.S.A. in 2016 and Star Of Canaan Dance International Ballet Competition, H.K. in 2016 & 2018. Ms. Liu has won numerous international and important awards competing in many significant competitions including the prestigious Varna IBC held in Varna, Bulgaria, USA IBC in Jackson, World Ballet Competition.</p>
                                    <p>Recently she won the Gold Medal at Miami International Ballet Competition in Florida. Janis joined the Arts Ballet Theatre Of Florida in 2016 since then she has performed many soloist and leading roles, such as Sugar Plum Fairy and Snow Queen in Nutcracker, The Snake in Stone Flower, Columbine in Harlequinade, Le Corsaire Pas De Deux among others. She has traveled to Mexico and Poland as guest Artist with Mr. Vladimir Issaev. Besides that, Ms Liu is a certified teacher, who has successfully completed the Bolshoi Ballet Academy Vaganova Teacher Training Program in 2017-2018.  She was a guest ballet teacher for Taipei Youth Ballet Company, Taiwan in 2017.  Currently faculty member of Vladimir Issaev School Of Classical Ballet, U.S.A. and Asia Ballet Academy, Malaysia.</p>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <hr>
                                <div class="col-sm-4">
                                    <p><img class="img-responsive text-center" src="images/thefairydoll2018/taiyu-he.png" alt=""></p>
                                </div>
                                <div class="col-sm-8">
                                    <p><b>Taiyu He</b><br/>
                                    Taiyu He was born in Hunan, China. His early ballet training started at the Liaoning Ballet School where he also graduated.  Mr. He has won a gold medal at Osaka International Ballet Competition in 2010, gold medal at Korea International Ballet Competition in 2011, gold medal at Helsinki International Ballet Competition, gold medal at Seoul International Dance competition (Ballet Category) in 2012, gold medal at USA International Ballet Competition in 2014 and a silver medal at the Moscow International Ballet Competition in 2013.</p>
                                    <p>He was also granted a Special Distinction at the Varna International Ballet Competition in 2012. Prior to joining Arts Ballet Theatre of Florida Mr. He danced with Joffrey Ballet Studio Company and Tulsa Ballet Second Company.</p>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <hr>
                                <div class="col-sm-4">
                                    <p><img class="img-responsive text-center" src="images/thefairydoll2018/kevin-zong.png" alt=""></p>
                                </div>
                                <div class="col-sm-8">
                                    <p><b>Kevin Zong</b><br/>
                                    Is a graduate from the Ellison Ballet of New York where he studied for the last three years trained by Edward Ellison himself and under full scholarship. He started his ballet training at the First State Ballet Theater in Wilmington, Delaware under the tutelage of Pasha and Kristina Kambalov. He also attended the Shanghai Dance School, and summer programs at the American Ballet Theater, Next Generation Ballet in Florida, the School of American Ballet and s short –term apprenticeship at the Royal Danish Ballet in Copenhagen, Denmark in 2015.</p>
                                    <p>His repertoire includes Swan Lake Pas de Trois, Napoli, Villanelle, Carmen, Flams of Paris, Le Corsair among many others. He has been awarded Silver Medal at the Indianapolis International Ballet Competition in 2015, and First and Second prize awards at the Youth American Grand Prix Semi-finals in 2014 and 2015 along with several full merit scholarships.</p>
                                    <p>Kevin began his professional career last season at Arts Ballet Theatre of Florida. Since then, he has performed soloist roles and performed in Mexico along with Principal dancers Mary Carmen Catoya and Janis Liu.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- <div class="clearfix"></div> --}}
            </section>

            {{-- <section class="pageCategory-section last section-grey">
                <div class="container">
                    <div class="gallery text-center">
                        <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
                        <!-- Swiper -->
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <a href="images/thefairydoll2018/gallery-1.jpg" data-featherlight="image"><img class="" src="images/thefairydoll2018/gallery-1.jpg" alt=""></a>
                                </div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next swiper-button-white"></div>
                            <div class="swiper-button-prev swiper-button-white"></div>
                        </div><!-- /Swiper -->
                    </div>
                </div>
            </section>--}}

            <section class="pageCategory-section last"><a id="anchorPrice"></a>
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                            <div class="text-center">
                                <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                                <p>Select ticket</p>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="table-responsive">
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Category</th>
                                            <th>Normal Price</th>
                                            <th>Senior Citizen / Student / OKU </th>
                                            <th>Seat Map</p>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td style="background-color: #FF30C7; color: #ffffff;">Cat 1</td>
                                            <td>RM198.00</td>
                                            <td>RM159.25</td>
                                            <td rowspan="5"><img class="img-responsive seatPlanImg" src="images/thefairydoll2018/seat-plan.jpg" alt=""></td>
                                        </tr>
                                        <tr>
                                            <td style="background-color: #FF9700; color: #ffffff;">Cat 2</td>
                                            <td>RM158.00</td>
                                            <td>RM134.95</td>
                                        </tr>
                                        <tr>
                                            <td style="background-color: #9802C6; color: #ffffff;">Cat 3</td>
                                            <td>RM128.00</td>
                                            <td>RM115.60</td>
                                        </tr>
                                        <tr>
                                            <td style="background-color: #94D14C; color: #ffffff;">Cat 4</td>
                                            <td>RM88.00</td>
                                            <td>N/A</td>
                                        </tr>
                                        <tr>
                                            <td style="background-color: #FF7791; color: #ffffff;">Gold</td>
                                            <td>TBA</td>
                                            <td>TBA</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="buyAlert-bar">
                                <a class="btn btn-danger" id="buyButton" datetime="Jan 20 2019 00:00:00 GMT+0800" target="_blank" href="https://redtix-tickets.airasia.com/en-AU/shows/The Fairy Doll/events" role="button">BUY TICKETS</a>
                                {{-- <span class="or">/</span>
                                <span class="popData-btn" data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets <i class="fa fa-info-circle" aria-hidden="true"></i></span> --}}
                            </div>

                            <div class="note text-left">
                                <h2>Terms & Conditions for Discount</h2>
                                <ol>
                                    <li>Senior Citizens / Student / OKU discounts can be purchased online. You must present the relevant documents during the redemption of the ticket. Failure to do so will result in your ticket being canceled and will have to pay a full price for entry on the day of the show. Please read the NOTES before purchase.</li>
                                </ol>
                            </div>

                            <div class="note text-left">
                                <h2>Important Notes</h2>
                                <ol>
                                    <li>Prices shown includes ticket fee.</li>
                                    <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li>
                                    <li>RM10.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                                    <li>Online ticket selling will close 2 days prior to event day, subject to availability.</li>
                                    <li>The discount applicable for ticket bearer only.</li>
                                </ol>
                                <h2>For enquiry only:</h2>
                                <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                            </div>

                        </div>
                    </div>
                </div>
            </section>
        </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 350;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });

    // Modal Announcement
	$(document).ready(function(){
        $("#announcementModal").modal('show');
  	});	
    </script>

    {{-- Buy button disable --}}
    <script type="text/javascript">
        $(function() {
            $('a[id^=buyButton]').each(function() {
                var date = new Date();
                var enddate = $(this).attr('datetime'); 
                if ( Date.parse(date) >= Date.parse(enddate)) {
                  $(this).addClass('disabled');
                }
            });
        });
    </script>

@endsection

@section('modal')
    @include('layouts.partials.modals._seatplan')
    @include('layouts.partials.modals._getTix')

   <div id="announcementModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
                    <h1><i class="fa fa-bullhorn text-warning" aria-hidden="true"></i></h1>
                    <h4>Announcement</h4>
                    <div class="clearfix">&nbsp;</div>
                    <div class="well">
                        <p>Senior Citizens / Student / OKU discounts can be purchased online. You must present the relevant documents during the redemption of the ticket. Failure to do so will result in your ticket being canceled and will have to pay a full price for entry on the day of the show. Please read the NOTES before purchase.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection