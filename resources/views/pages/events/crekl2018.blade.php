@extends('master')
@section('title')
     CRÈ
@endsection

@section('header')
    @include('layouts.partials._header')
    <meta property="og:url" content="{{ Request::fullUrl() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content=" CRÈ" />
    <meta property="og:description" content="CRE is a modern circus theatre extravaganza that takes you to another world where otherworldly creatures of different elements will enchant and excite the entire family."/>
    <meta property="og:image" content="{{ Request::Url().'images/crekl2018/thumbnail.jpg' }}" />
@endsection

@section('content')
    <!-- Banner Section -->
    <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/crekl2018/web-banner.jpg')}}" style="width: 100%" class="img-responsive" alt=" CRÈ"></div>
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            <img src="{{asset('images/crekl2018/thumbnail.jpg')}}" style="width: 100%" class="img-responsive" alt=" CRÈ">
        </div>
    </section>
    <!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
            <div class="row priceNbtn">
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="row">
                        <div class="col-sm-9 leftBox">
                            <h6> CRÈ</h6> Tickets from <span>RM70.00</span>
                        </div>
                        <div class="col-sm-3 text-center">
                            <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar section-white">
            <section class="pageCategory-section last">
                <div class="container intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i>  1st - 2nd Dec 2018</div>
                            <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i>  KLPac, Kuala Lumpur <a target="_blank" href="https://goo.gl/maps/tpKnH8Tp5eq">View Map</a></div>
                            <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> 01 Dec 2018 - 1st show: 3.00pm, 2nd show: 8.30pm</div>
                            <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> 02 Dec 2018 - 3rd show: 11.00am, 4th show: 3.00pm</div>
                            <div class="clearfix">&nbsp;</div>
                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <div class="addthis_inline_share_toolbox"></div>
                            <!-- /sharing -->
                            <div class="col-sm-12">
                                <hr>
                                <p><h2> CRÈ</h2><br/>
                                CRE is a modern circus theatre extravaganza that takes you to another world where otherworldly creatures of different elements will enchant and excite the entire family. The production will consist of spellbinding circus acts; aerial acrobatics, object manipulation, artistic dance and a fire/pyrotechnic show.</p>
                                <p>CRE is presented to you by CRE Arts Asia, a coalition of professional Malaysian modern circus artists whom are leaders in the local/South East Asian performing arts and fitness scene.</p>
                            </div>
                            <div class="col-sm-12">
                                <p><img class="img-responsive seatPlanImg" src="images/crekl2018/viva-circus.png" alt=""></p>
                                <p><b>About Viva Circus</b><br/>
                                Founded by Ms. Vivian Lea in 2007. The pioneers in the dance fitness industry, Viva Vertical is the first pole dance, aerial dance, flyoga and burlesque school to be founded in Malaysia. Viva Vertical has since taught thousands of people across our 5 branches and toured the world conducting courses.</p>
                                <p>A member of the Aerial Arts Associaton of Asia (AAA Asia) and the International Pole Dance Fitness Association (IPDFA), Viva Vertical's pole dance syllabus is award winning, safe and internationally acclaimed. Our mission is to provide a friendly atmosphere, professional instruction and safe practices for our members.</p>
                                <p>Viva Vertical's success is attributed to the belief, commitment and loyalty of our leaders. Together, our vision is to strive for excellence and expansion so that our classes can be accesssible to more people. Join us, and together we can help you achiece your goals.</p>
                            </div>
                            <div class="col-sm-12">
                                <p><img class="img-responsive seatPlanImg" src="images/crekl2018/psycusix.png" alt=""></p>
                                <p><b>About Psycusix</b><br/>
                                Talented and passionate flow and circus artists have come together to push the artistic and technical limits of movement performance in Malaysia.</p>
                                <p>Since its birth six years ago, Pcycusix has been invited to Singapore, Philippines, Korea, Indonesia, Hong Kong, Maldives, Taiwan, Vietnam, Thailand, UAE, US, and the UK, allowing them to dance and learn with many inspring flow and circus artists. This, in turn, further honed their performance skills never before seen in Malaysia.</p>
                                <p>Today, Psycusix is Malaysia's leading modern and contemporary circus performing group. They are an exceptionally flexible, experimental, and unique group that delivers performances of high aesthetic values. When they perform, you'll see how passion meets flow.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="pageCategory-section last section-grey">
                <div class="container">
                    <div class="gallery text-center">
                        <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
                        <!-- Swiper -->
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <a href="images/crekl2018/gallery-1.jpg" data-featherlight="image"><img class="" src="images/crekl2018/gallery-1.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/crekl2018/gallery-2.jpg" data-featherlight="image"><img class="" src="images/crekl2018/gallery-2.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/crekl2018/gallery-3.jpg" data-featherlight="image"><img class="" src="images/crekl2018/gallery-3.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/crekl2018/gallery-4.jpg" data-featherlight="image"><img class="" src="images/crekl2018/gallery-4.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/crekl2018/gallery-5.jpg" data-featherlight="image"><img class="" src="images/crekl2018/gallery-5.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/crekl2018/gallery-6.jpg" data-featherlight="image"><img class="" src="images/crekl2018/gallery-6.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/crekl2018/gallery-7.jpg" data-featherlight="image"><img class="" src="images/crekl2018/gallery-7.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/crekl2018/gallery-8.jpg" data-featherlight="image"><img class="" src="images/crekl2018/gallery-8.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/crekl2018/gallery-9.jpg" data-featherlight="image"><img class="" src="images/crekl2018/gallery-9.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/crekl2018/gallery-10.jpg" data-featherlight="image"><img class="" src="images/crekl2018/gallery-10.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/crekl2018/gallery-11.jpg" data-featherlight="image"><img class="" src="images/crekl2018/gallery-11.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/crekl2018/gallery-12.jpg" data-featherlight="image"><img class="" src="images/crekl2018/gallery-12.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/crekl2018/gallery-13.jpg" data-featherlight="image"><img class="" src="images/crekl2018/gallery-13.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/crekl2018/gallery-14.jpg" data-featherlight="image"><img class="" src="images/crekl2018/gallery-14.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/crekl2018/gallery-15.jpg" data-featherlight="image"><img class="" src="images/crekl2018/gallery-15.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/crekl2018/gallery-16.jpg" data-featherlight="image"><img class="" src="images/crekl2018/gallery-16.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/crekl2018/gallery-17.jpg" data-featherlight="image"><img class="" src="images/crekl2018/gallery-17.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/crekl2018/gallery-18.jpg" data-featherlight="image"><img class="" src="images/crekl2018/gallery-18.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/crekl2018/gallery-19.jpg" data-featherlight="image"><img class="" src="images/crekl2018/gallery-19.jpg" alt=""></a>
                                </div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next swiper-button-white"></div>
                            <div class="swiper-button-prev swiper-button-white"></div>
                        </div><!-- /Swiper -->
                    </div>
                </div>
            </section>

            <section class="pageCategory-section last"><a id="anchorPrice"></a>
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                            <div class="text-center">
                                <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                                <p>Select ticket</p>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="table-responsive">
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Seating</th>
                                            <th>Regular</th>
                                            <th>Senior Citizen / OKU / Kids</th>
                                            <th>Package of 4</th>
                                            <th>Seat Plan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr style="background-color: #40A737; color: #ffffff;">
                                            <td>Earth Zone</td>
                                            <td>RM 198.00</td>
                                            <td>RM 119.00</td>
                                            <td>RM 710.00</td>
                                            <td rowspan="3" style="background-color: #ffffff;"><img class="img-responsive seatPlanImg" src="images/crekl2018/seat-plan.jpg" alt=""></td>
                                        </tr>
                                        <tr style="background-color: #049CE3; color: #ffffff;">
                                            <td>Water Zone</td>
                                            <td>RM 158.00</td>
                                            <td>RM 95.00</td>
                                            <td>RM 570.00</td>
                                        </tr>
                                        <tr style="background-color: #F39F01; color: #ffffff;">
                                            <td>Fire Zone</td>
                                            <td>RM 118.00</td>
                                            <td>RM 70.00</td>
                                            <td>RM 420.00</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="buyAlert-bar">
                                <a class="btn btn-danger" id="buyButton" datetime="Nov 29 2018 00:00:00 GMT+0800" target="_blank" href="https://redtix-tickets.airasia.com/en-AU/shows/crè/events" role="button">BUY TICKETS</a>
                                {{-- <span class="or">/</span>
                                <span class="popData-btn" data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets <i class="fa fa-info-circle" aria-hidden="true"></i></span> --}}
                            </div>
                            {{-- <span class="importantNote">* Package validity - until 2nd Nov 2018</span> --}}
                            <div class="note text-left">
                                <h2>Discounts &amp; Promotions</h2>
                                <ol>
                                    <li>To enjoy the “Package Of 4" promotion please select 4 seat and proceed to checkout. Kindly select the promotion "Package Of 4 Promotion" and key in "CREARTSASIAP4" promo code before proceed to make payment.</li>
                                    <li>HLB Debit & Credit card holders enjoy 10% off with promo code CREHLB10 ; HLB GSC Credit card holders enjoy 15% off with promo code CREHLB15G</li>
                                </ol>
                            </div>

                            <div class="note text-left">
                                <h2>Important Notes</h2>
                                <ol>
                                    {{-- <li>Prices shown exclude ticket fee.</li>
                                    <li>All ticket prices will be paid in Malaysian Ringgit. Indian Rupee prices are shown for reference purposes only.</li>
                                    <li>Prices quoted in INR may differ due to currency fluctuations.</li>
                                    <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li>
                                    <li>RM10.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                                    <li>Online ticket selling will close 2 days prior to event day, subject to availability.</li>
                                    <li>Adult: Strictly 18 years old and above.</li> --}}
                                    <li>Color indicates price category.</li>
                                    <li>Prices shown exclude AirAsiaRedTix RM4.00 ticket fee.</li>
                                    <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li>
                                    <li>Limited to 4 tickets only per transaction per client.</li>
                                    <li>1 ticket admits 1 person ONLY, no sharing seat is allowed.</li>
                                    <li>Once tickets are sold, no exchanges or cancellations will be entertained</li>
                                    <li>RM10.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                                </ol>
                                <h2>For enquiry only:</h2>
                                <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 350;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });

    // Modal Announcement
	$(document).ready(function(){
        $("#announcementModal").modal('show');
  	});	
    </script>

    {{-- Buy button disable --}}
    <script type="text/javascript">
        $(function() {
            $('a[id^=buyButton]').each(function() {
                var date = new Date();
                var enddate = $(this).attr('datetime'); 
                if ( Date.parse(date) >= Date.parse(enddate)) {
                  $(this).addClass('disabled');
                }
            });
        });
    </script>

@endsection

@section('modal')
    @include('layouts.partials.modals._seatplan')
    @include('layouts.partials.modals._getTix')

   <div id="announcementModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
                    <h1><i class="fa fa-bullhorn text-warning" aria-hidden="true"></i></h1>
                    <h4>Announcement</h4>
                    <div class="clearfix">&nbsp;</div>
                    <div class="well">
                        <p>To enjoy the “Package Of 4" promotion please select 4 seat and proceed to checkout. Kindly select the promotion "Package Of 4 Promotion" and key in "CREARTSASIAP4" promo code before proceed to make payment.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection