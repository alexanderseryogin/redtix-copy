@extends('master')
@section('title')
    Guns N' Roses Not In This Lifetime Tour
@endsection

@section('header')
    @include('layouts.partials._header')
    <meta property="og:url" content="{{ Request::fullUrl() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Guns N' Roses Not In This Lifetime Tour" />
    <meta property="og:description" content="Unstoppable rock music icons Guns N' Roses will electrify their Malaysian fans in support for the band’s Not In This Lifetime Tour."/>
    <meta property="og:image" content="{{ Request::Url().'images/gnr2018/thumbnail1.jpg' }}" />
@endsection

@section('content')
    <!-- Banner Section -->
    <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/gnr2018/web-banner1.jpg')}}" style="width: 100%" class="img-responsive" alt="Guns N' Roses Not In This Lifetime Tour"></div>
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            <img src="{{asset('images/gnr2018/thumbnail1.jpg')}}" style="width: 100%" class="img-responsive" alt="Guns N' Roses Not In This Lifetime Tour">
        </div>
    </section>
    <!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
            <div class="row priceNbtn">
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="row">
                        <div class="col-sm-9 leftBox">
                            <h6>Guns N' Roses Not In This Lifetime Tour</h6> Tickets from <span>RM245</span>
                        </div>
                        <div class="col-sm-3 text-center">
                            <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar section-white">
            <section class="pageCategory-section last">
                <div class="container intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i>  14th November 2018</div>
                            <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i>  Surf Beach, Sunway Lagoon <a target="_blank" href="https://goo.gl/maps/hsewiWbZk112">View Map</a></div>
                            <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> Wed, 14th November 8.00pm </div>
                            <div class="clearfix">&nbsp;</div>
                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <div class="addthis_inline_share_toolbox"></div>
                            <!-- /sharing -->                            
                            <hr>
                            <div class="col-sm-8">
                                <p><h2>GUNS N’ ROSES TO BRING “NOT IN THIS LIFETIME” TOUR TO KUALA, LUMPUR, MALAYSIA THIS NOVEMBER!</h2><br/>
                                <p>Unstoppable rock music icons Guns N' Roses will electrify their Malaysian fans in support for the band’s Not In This Lifetime Tour.  Produced by Galaxy Group in association with LAMC Productions & Rockstar Touring, the exhilarating global phenomenon is letting GNR fans get closer on Wednesday, November 14, 2018 at Surf Beach, Sunway Lagoon.</p>
                                <p>The Not in This Lifetime Tour has played to sold out stadiums around the world with Rolling Stone dubbing it “a triumphant return of one of the most important bands in rock music history." LA WEEKLY notes it's "everything fans could hope for" while The Washington Times says the show delivers a set of "inspired surprises."</p>
                                <p>Following the group's 1985 formation, Guns N' Roses injected unbridled, unrivalled, and unstoppable attitude into the rock scene. The spirit went on to captivate the entire world with the release of their 1987 debut Appetite for Destruction–the best-selling U.S. debut ever, moving 30 million copies globally.  In 1991, the seven-time platinum Use Your Illusion I and Use Your Illusion II occupied the top two spots of the Billboard Top 200 upon release.</p>
                                <p>Over the course of the past decade, Guns N' Roses have performed sold out shows and headlined festivals worldwide following the critically acclaimed release of 2008's RIAA platinum-certified Chinese Democracy. Six studio albums later, Guns N' Roses is one of the most important and influential acts in music history and continue to set the benchmark for live performances connecting with millions of fans across the globe. Guns N' Roses' are Axl Rose (vocals, piano), Duff McKagan (bass), Slash (lead guitar), Dizzy Reed (keyboard), Richard Fortus (rhythm guitar), Frank Ferrer  (drums), and Melissa Reese (keyboard).</p>
                                {{-- <p>For more information, please follow Galaxy Group <a href="https://www.facebook.com/GalaxyConcerts/">www.facebook.com/GalaxyConcerts/</a> and @galaxygroup (on Instagram, and Twitter), or visit <a href="http://galaxy.com.my/main">galaxy.com.my</a> for details.</p> --}}
                                <p>#GNRKL2018</p>
                            </div>
                            <div class="col-sm-4">
                                <iframe src="https://open.spotify.com/embed/user/spotify/playlist/37i9dQZF1DX74DnfGTwugU" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- <div class="clearfix"></div> --}}
            </section>

            <section class="pageCategory-section last section-grey">
                <div class="container">
                    <div class="gallery text-center">
                        <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
                        <!-- Swiper -->
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <a href="images/gnr2018/gallery-1x.jpg" data-featherlight="image"><img class="" src="images/gnr2018/gallery-1x.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/gnr2018/gallery-2.jpg" data-featherlight="image"><img class="" src="images/gnr2018/gallery-2.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/gnr2018/gallery-3.jpg" data-featherlight="image"><img class="" src="images/gnr2018/gallery-3.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/gnr2018/gallery-2.jpg" data-featherlight="image"><img class="" src="images/gnr2018/web-banner.jpg" alt=""></a>
                                </div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next swiper-button-white"></div>
                            <div class="swiper-button-prev swiper-button-white"></div>
                        </div><!-- /Swiper -->
                    </div>
                </div>
            </section>

            <section class="pageCategory-section last"><a id="anchorPrice"></a>
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                            <div class="text-center">
                                <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                                <p>Select ticket</p>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="table-responsive">
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Category</th>
                                            <th>Ticket Price</th>
                                            {{--<th>Remarks</th> --}}
                                            <th>Venue</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td style="background-color:#666666; color:#fff;">VVIP<br/>(Rock Zone)<br/>Free Standing</td>
                                            <td><strike>RM 1,288</strike><br /><font style="color:red">Sold Out</font></td>
                                            {{-- <td>-</td> --}}
                                            <td rowspan="5"><img class="img-responsive seatPlanImg" src="images/gnr2018/seat-plan1.jpg" alt=""></td>
                                        </tr>
                                        <tr>
                                            <td style="background-color:#BC1E2D; color:#fff;">CAT 1<br/>Free Standing</td>
                                            {{-- <td><strike>RM 988</strike><br/>RM 840</td>
                                            <td>15% Discount for RedTix</td> --}}
                                            <td>RM 988</td>
                                        </tr>
                                        <tr>
                                            <td style="background-color:#FFEF01;">CAT 2<br/>Free Standing</td>
                                            {{-- <td><strike>RM 688</strike><br/>RM 585</td>
                                            <td>15% Discount for RedTix</td> --}}
                                            <td>RM 688</td>
                                        </tr>
                                        <tr>
                                            <td style="background-color:#666666;">CAT 3<br/>Free Standing</td>
                                            <td><strike>RM 488</strike><br /><font style="color:red">Sold Out</font></td>
                                            {{-- <td>-</td> --}}
                                        </tr>
                                        <tr>
                                            <td style="background-color:#016938; color:#fff;">CAT 4<br/>Free Standing</td>
                                            {{-- <td><strike>RM 288</strike><br/>RM 245</td>
                                            <td>15% Discount for RedTix</td> --}}
                                            <td>RM 288</td>
                                        </tr>
                                        {{-- <tr>
                                            <td style="background-color:#016938; color:#fff;">CAT 4 (QUAD PACKAGE)<br/>Free Standing</td>
                                            <td>RM 924</td>
                                            <td>4 x Tickets<br/>20% Discount for RedTix</td>
                                        </tr> --}}
                                    </tbody>
                                </table>
                            </div>
                            <div class="buyAlert-bar">
                                <a class="btn btn-danger" id="buyButton" datetime="Nov 13 2018 00:00:00 GMT+0800" target="_blank" href="https://redtix-tickets.airasia.com/en-AU/shows/not in this lifetime tour guns n’ roses/events" role="button">BUY TICKETS</a>
                                {{-- <span class="or">/</span>
                                <span class="popData-btn" data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets <i class="fa fa-info-circle" aria-hidden="true"></i></span> --}}
                            </div>
                            <div class="note text-left">
                                <h2>Important Notes</h2>
                                <ol>
                                    <li>Prices shown includes RM4.00 AirAsiaRedTix fee.</li>
                                    {{-- <li>All ticket prices will be paid in Malaysian Ringgit. Australian Dollar prices are shown for reference purposes only.</li>
                                    <li>Prices quoted in AUD may differ due to currency fluctuations.</li> --}}
                                    <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li>
                                    <li>RM10.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                                    <li>Online ticket selling will close a day prior to event day, subject to availability.</li>
                                </ol>
                                <h2>For enquiry only:</h2>
                                <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 350;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });
    </script>

    {{-- Buy button disable --}}
    <script type="text/javascript">
        $(function() {
            $('a[id^=buyButton]').each(function() {
                var date = new Date();
                var enddate = $(this).attr('datetime'); 
                if ( Date.parse(date) >= Date.parse(enddate)) {
                  $(this).addClass('disabled');
                }
            });
        });
    </script>

@endsection

@section('modal')
    @include('layouts.partials.modals._seatplan')
    @include('layouts.partials.modals._getTix')
@endsection