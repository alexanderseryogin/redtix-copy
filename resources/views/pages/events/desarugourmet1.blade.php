@extends('master')
@section('title')
    Desaru Coast Gourmet Series - A Malaysian Journey
@endsection

@section('header')
    @include('layouts.partials._header')
    <meta property="og:url" content="{{ Request::fullUrl() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Desaru Coast Gourmet Series - A Malaysian Journey" />
    <meta property="og:description" content="For the first time in Malaysia, Desaru Coast if proud to present it's very own signature dining programme"/>
    <meta property="og:image" content="{{ Request::Url().'images/desarugourmet1/thumbnail.jpg' }}" />
@endsection

@section('content')
    <!-- Banner Section -->
    <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/desarugourmet1/web-banner.jpg')}}" style="width: 100%" class="img-responsive" alt="Desaru Coast Gourmet Series - A Malaysian Journey"></div>
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            <img src="{{asset('images/desarugourmet1/thumbnail.jpg')}}" style="width: 100%" class="img-responsive" alt="Desaru Coast Gourmet Series - A Malaysian Journey">
        </div>
    </section>
    <!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
            <div class="row priceNbtn">
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="row">
                        <div class="col-sm-9 leftBox">
                            <h6>Desaru Coast Gourmet Series - A Malaysian Journey</h6> Tickets from <span>RM260</span>
                        </div>
                        <div class="col-sm-3 text-center">
                            <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar section-white">
            <section class="pageCategory-section last">
                <div class="container intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i>  7th - 9th December 2018</div>
                            <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i>  The Els Club Desaru Coast Ocean Course <a target="_blank" href="https://goo.gl/maps/VcpNT5gJ6ty">View Map</a></div>
                            <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i>Lunch 12.00pm, Dinner 7.00pm</div>
                            <div class="clearfix">&nbsp;</div>
                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <div class="addthis_inline_share_toolbox"></div>
                            <!-- /sharing -->
                            <div class="col-sm-12">
                                <hr>
                                <p><h2>Desaru Coast Gourmet Series First Edition – A Malaysian Journey</h2><br/>
                                For the first time in Malaysia, Desaru Coast is proud to present its very own signature dining programme, <b>Desaru Coast Gourmet Series First Edition</b>. The new destination resort introduces an exclusively curated 6-series dining programme that offers a unique and holistic culinary experience for its prestige guests. Rediscover iconic flavours of Malaysian cuisines through new interpretations by award-winning chefs from around the world. This year, Desaru Coast Gourmet Series First Edition proudly presents Relais & Chateaux’s award-winning Michelin Grand Chef, <b>Emmanuel Stroobant</b>.</p> 
                                <p><b>BE THE FIRST</b> to savour a one-of-a-kind mouth-watering fusion flavour at Desaru Coast!</p>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                {{-- <div class="clearfix"></div> --}}
            </section>

            <section class="pageCategory-section last section-grey">
                <div class="container">
                    <div class="gallery text-center">
                        <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
                        <!-- Swiper -->
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <a href="images/desarugourmet1/gallery-1.jpg" data-featherlight="image"><img class="" src="images/desarugourmet1/gallery-1.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/desarugourmet1/web-banner.jpg" data-featherlight="image"><img class="" src="images/desarugourmet1/web-banner.jpg" alt=""></a>
                                </div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next swiper-button-white"></div>
                            <div class="swiper-button-prev swiper-button-white"></div>
                        </div><!-- /Swiper -->
                    </div>
                </div>
            </section>

            <section class="pageCategory-section last"><a id="anchorPrice"></a>
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                            <div class="text-center">
                                <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                                <p>Select ticket</p>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="table-responsive">
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Category</th>
                                            <th>Ticket Type</th>
                                            <th>Normal Price</th>
                                            <Th>Remarks</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr style="font-size:10px; background:#F1F2E8;">
                                            <td>Day 1 - 7th Dec 2018</td>
                                            <td>By Invitation Only</td>
                                            <td>-</td>
                                            <td>-</td>
                                        </tr>
                                        <tr style="font-size:10px;">
                                            <td rowspan="6">Day 2 - 8th Dec 2018</td>
                                            <td>3-Course Lunch Session</td>
                                            <td>RM260</td>
                                            <td rowspan="2">
                                                <ul style="text-align:left">
                                                    <li>Choice of Beef or Fish.</li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr style="font-size:10px;">
                                            <td>6-Course Dinner Session</td>
                                            <td>RM390</td>
                                        </tr>
                                        <tr style="font-size:10px;">
                                            <td>Stay & Indulge - Lunch</td>
                                            <td>RM1,350</td>
                                            <td rowspan="2">
                                                <ul style="text-align:left">
                                                    <li>Choice of Beef or Fish.</li>
                                                    <li>Reserved dining seats for 2 guest & One night stay - Deluxe Room at Hard Rock Hotel Desaru Coast</li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr style="font-size:10px;">
                                            <td>Stay & Indulge - Dinner</td>
                                            <td>RM1,500</td>
                                        </tr>
                                        <tr style="font-size:10px;">
                                            <td>Stay, Play & Indulge - Lunch</td>
                                            <td>RM1,950</td>
                                            <td rowspan="2">
                                                <ul style="text-align:left">
                                                    <li>Choice of Beef or Fish.</li>
                                                    <li>Reserved dining seats for 2 guest, One night stay - Deluxe Room at Hard Rock Hotel Desaru Coast & One golf session for 2 guest at The Els Club Ocean Course</li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr style="font-size:10px;">
                                            <td>Stay, Play & Indulge - Dinner</td>
                                            <td>RM2,100</td>
                                        </tr>
                                        <tr style="font-size:10px; background:#F1F2E8;">
                                            <td rowspan="6">Day 3 - 9th Dec 2018</td>
                                            <td>3-Course Lunch Session</td>
                                            <td>RM260</td>
                                            <td rowspan="2">
                                                <ul style="text-align:left">
                                                    <li>Choice of Beef or Fish.</li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr style="font-size:10px; background:#F1F2E8;">
                                            <td>6-Course Dinner Session</td>
                                            <td>RM390</td>
                                        </tr>
                                        <tr style="font-size:10px; background:#F1F2E8;">
                                            <td>Stay & Indulge - Lunch</td>
                                            <td>RM1,350</td>
                                            <td rowspan="2">
                                                <ul style="text-align:left">
                                                    <li>Choice of Beef or Fish.</li>
                                                    <li>Reserved dining seats for 2 guest & One night stay - Deluxe Room at Hard Rock Hotel Desaru Coast</li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr style="font-size:10px; background:#F1F2E8;">
                                            <td>Stay & Indulge - Dinner</td>
                                            <td>RM1,500</td>
                                        </tr>
                                        <tr style="font-size:10px; background:#F1F2E8;">
                                            <td>Stay, Play & Indulge - Lunch</td>
                                            <td>RM1,950</td>
                                            <td rowspan="2">
                                                <ul style="text-align:left">
                                                    <li>Choice of Beef or Fish.</li>
                                                    <li>Reserved dining seats for 2 guest, One night stay - Deluxe Room at Hard Rock Hotel Desaru Coast & One golf session for 2 guest at The Els Club Ocean Course</li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr style="font-size:10px; background:#F1F2E8;">
                                            <td>Stay, Play & Indulge - Dinner</td>
                                            <td>RM2,100</td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                            <div class="buyAlert-bar">
                                <a class="btn btn-danger" id="buyButton" datetime="Dec 7 2018 00:00:00 GMT+0800" target="_blank" href="https://redtix-tickets.airasia.com/en-AU/shows/desaru coast gourmet series - a malaysian journey (8th - 9th december 2018)/events" role="button">BUY TICKETS</a>
                                {{-- <span class="or">/</span>
                                <span class="popData-btn" data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets <i class="fa fa-info-circle" aria-hidden="true"></i></span> --}}
                            </div>
                            {{-- <div class="note text-left">
                                <h2>Admission Age Limit</h2>
                                <ol>
                                    <li>Below 3 years - FOC (No Seating)</li>
                                    <li>Above 3 years - Full Normal Price (With Seating)</li>
                                </ol>
                            </div> --}}
                            <div class="note text-left">
                                <h2>Important Notes</h2>
                                <ol>
                                    <li>Prices shown exclude RM4 ticket fee.</li>
                                    {{-- <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li> --}}
                                    <li>RM10.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                                    <li>Online ticket selling will close 1 day prior to event day, subject to availability.</li>
                                </ol>
                                <h2>For enquiry only:</h2>
                                <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                            </div>

                        </div>
                    </div>
                </div>
            </section>
        </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 350;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });
    </script>

    {{-- Buy button disable --}}
    <script type="text/javascript">
        $(function() {
            $('a[id^=buyButton]').each(function() {
                var date = new Date();
                var enddate = $(this).attr('datetime'); 
                if ( Date.parse(date) >= Date.parse(enddate)) {
                  $(this).addClass('disabled');
                }
            });
        });
    </script>

@endsection

@section('modal')
    @include('layouts.partials.modals._seatplan')
    @include('layouts.partials.modals._getTix')
@endsection