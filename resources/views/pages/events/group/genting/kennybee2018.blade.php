@extends('master')
@section('title')
  Kenny Bee the Story Continues Live 2018
@endsection

@section('header')
    @include('layouts.partials._header')
    <meta property="og:url" content="{{ Request::fullUrl() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Kenny Bee the Story Continues Live 2018" />
    <meta property="og:description" content="Kenny Bee the Story Continues Live 2018" />
    <meta property="og:image" content="{{ Request::Url().'images/genting/kennybee2018/thumbnail1.jpg' }}" />
@endsection

@section('content')
    <!-- Banner Section -->
    <section class="innerPageBanner" style="width: 100%">
      <div class="bigBanner-overlay"></div>
      <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/genting/kennybee2018/web-banner1.jpg')}}" style="width: 100%" class="img-responsive" alt="Kenny Bee the Story Continues Live 2018"></div>
      <div class="widewrapper main hidden-lg hidden-md hidden-sm">
      <img src="{{asset('images/genting/kennybee2018/thumbnail1.jpg')}}" width="100%" class="img-responsive" alt="Kenny Bee the Story Continues Live 2018">
      </div>
    </section>
    <!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
          <div class="row priceNbtn">
            <div class="col-sm-offset-1 col-sm-10">
              <div class="row">
                <div class="col-sm-9 leftBox">
                  <h6>Kenny Bee the Story Continues Live 2018</h6>
                  Tickets from <span>RM160.00</span>
                </div>
                <div class="col-sm-3 text-center">
                  <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                </div>
              </div>
            </div>
          </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
      <div class="mainBodyContent no-btm-mar section-white">

        <section class="pageCategory-section last">
          
          <div class="container intro">
            <div class="row">
              <div class="col-sm-offset-1 col-sm-10 leftBar">
                <!--<h2>Title</h2>-->
                <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i> 24th Dec 2018 (Monday)</div>
                <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> 10.00pm</div>
                <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i> Arena Of Stars, Genting Highland <a target="_blank" href="https://www.google.com/maps/place/Genting+Arena+of+Stars/@3.423828,101.7910333,17z/data=!3m1!4b1!4m5!3m4!1s0x31cc14072aa30697:0xb4fadf09a74b1b30!8m2!3d3.423828!4d101.793222">View Map</a></div>
                <div class="clearfix">&nbsp;</div>
                <!-- Go to www.addthis.com/dashboard to customize your tools -->
                <div class="addthis_inline_share_toolbox"></div>
                <!-- /sharing -->
                <hr>
                <h2>Kenny Bee the Story Continues Live 2018 / 鍾鎮濤故事繼續演唱會2018</h2>
                <p>Kenny Bee, former member of renowned 70s band, The Wynners, has transformed into a jack of all trades. Talents spanning from acting and directing alongside Chow Yun Fat and Stephen Chow to fronting several of his own bands producing popular hits has certainly made him the one to watch.</p>
                <p>Kenny Bee, whose real name is Chung Chun-to, is best known for his time as one of the lead singers of the hugely popular 70s band, The Wynners—the other lead singer being Alan Tam. He also made a name for himself in the world of acting, and has remained active as a solo artist in the Hong Kong entertainment scene for the past three decades.</p>
                <p>Bee has always been a vocalist, having started out in his career as a singer and saxophonist in the Hong Kong nightclub circuit, fronting a band called The Sergeant Majors.</p>
                <p>The 80s marked the peak of his popularity; four of his studio albums went gold and two went platinum. During this era, his biggest hits were Let Everything Be Gone with the Wind, If We Were Meant To Be, and A Romance. He also made an impact in December 2006 when he released his Cantonese solo album, Escape—backed by his new band, Black Tea. Then in 2008, he performed a series of solo concerts at the Hong Kong Coliseum, as well as in Singapore and Macau. Aside from working solo, the Wynners also continued their world tour then.</p>
                <p>钟镇涛, 风靡70年代香港乐坛的钟镇涛，或者大家熟悉的“阿B”，于1973年与谭咏麟、彭健新、叶智强和陈友一起组成温拿乐队。钟镇涛在团体内担任主唱兼吉他手，富有磁性又沧桑的嗓音，多首歌曲皆成为香港乐坛的经典热门歌曲。出道多年的他，无论是以团体还是个人身份，均创造了亮眼的成绩。他的代表作包括了《只要你过得比我好》、《让一切随风》、《Sha La La La La》和《聚散两依依》。</p>
                <p>Now as a seasoned soloist, Kenny Bee returns to the Arena of Stars with new music as part of his tour - ‘The Story Continues’.</p>
                </div>
            </div>
          </div>
          <div class="clearfix"></div>
        </section>

        <section class="pageCategory-section last section-grey">
          <div class="container">
            <div class="gallery text-center">
              <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
              <!-- Swiper -->
              <div class="swiper-container">
                <div class="swiper-wrapper">
                  <div class="swiper-slide">
                    <a href="images/genting/kennybee2018/gallery-1.jpg" data-featherlight="image"><img class="" src="images/genting/kennybee2018/gallery-1.jpg" alt=""></a>
                  </div>
                  <div class="swiper-slide">
                    <a href="images/genting/kennybee2018/web-banner.jpg" data-featherlight="image"><img class="" src="images/genting/kennybee2018/web-banner1.jpg" alt=""></a>
                  </div>
                </div>
                <!-- Add Pagination -->
                <div class="swiper-pagination"></div>
                <!-- Add Arrows -->
                <div class="swiper-button-next swiper-button-white"></div>
                <div class="swiper-button-prev swiper-button-white"></div>
              </div><!-- /Swiper -->
            </div>
          </div>
        </section>

        <section class="pageCategory-section last"><a id="anchorPrice"></a>
          <div class="container tixPrice">
            <div class="row">
              <div class="col-sm-offset-1 col-sm-10 ">
                <div class="text-center">
                  <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                  <p>Select ticket</p>
                </div>
                <div class="clearfix">&nbsp;</div>
                <div class="table-responsive">
                  <table class="table infoTable-D table-bordered">
                      <thead>
                        <tr>
                          <th>Price Category</th>
                          <th>Normal</th>
                          <th>Seating Plan</th>
                        </tr>
                      </thead>
                      <tbody>
                          <tr>
                            <td><span class="box purple">VIP</span></td>
                            <td>RM 870</td>
                            <td rowspan="5"><img class="img-responsive seatPlanImg" src="images/genting/kennybee2018/seat-plan.jpg" alt=""></td>
                          </tr>
                          <tr>
                            <td><span class="box red">PS1</span></td>
                            <td>RM 560</td>
                          </tr>
                          <tr>
                            <td><span class="box green">PS2</span></td>
                            <td>RM 410</td>
                          </tr>
                          <tr>
                            <td><span class="box grey">PS3</span></td>
                            <td>RM 270</td>
                          </tr>
                          <tr>
                            <td><span class="box midblue">PS4</span></td>
                            <td>RM 160</td>
                          </tr>
                      </tbody>
                  </table>
                </div>

                <div class="buyAlert-bar">
                  <a class="btn btn-danger" id="buyButton" datetime="Dec 22 2018 00:00:00 GMT+0800" target="_blank" href="https://redtix-tickets.airasia.com/en-AU/shows/kenny bee the story continues live 2018/events" role="button">BUY TICKETS</a>
                  {{-- <span class="or">/</span>
                  <span class="popData-btn" data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets <i class="fa fa-info-circle" aria-hidden="true"></i></span> --}}
                </div>
                {{-- <div class="alert alert-info"><i class="fa fa-info-circle" aria-hidden="true"></i> Online purchase has closed. Please proceed to venue.</div> --}}
                <span class="importantNote">** Ticket pricing include RM 4 processing fee.</span>
                <span class="importantNote">** GWC/GRC discount is applicable via outlets only.</span><br>
                <span class="importantNote">** The GRC’s details (i.e. member’s name &amp; number) are compulsory to fill up for the purchase of GRC discounted rates. Failure which, the normal ticket rates will be charged.</span><br>
                <div class="note text-left alert alert-warning">
                  <div class="media">
                    <div class="media-left media-middle">
                      <a href="#">
                        <img class="media-object" src="images/oku.png" alt="">
                      </a>
                    </div>
                    <div class="media-body">
                      <h2>OKU Info</h2>
                      <p>Seats for those with special needs are located in the easily accessible zone (marked with the wheelchair sign on the floor plan). For ticket reservations, please call: 03-2718 1118.</p>
                    </div>
                  </div>
                </div>
                <p>3rd party collection will require authorisation letter and photocopy of the NRIC of ticket holder.</p>
                <p>Please be informed that all the tickets which issued by Ticketing Agent no longer to redeem at Genting Box Office. Guest may directly enter to the concert with using their original purchased ticket. However, guest who holding the Confirmation Letter/E-Ticket for any concert/show shall remain to exchange their ticket at Arena of Star counter, Genting.</p>
                <div class="note text-left">
                <h2>CONCERT TERMS & CONDITIONS:</h2>
                  <ul>
                    <li>Each ticket is valid for one-time admission only.</li>
                    <li>Child must be of 95cm height and below to be eligible for child ticket priced at RM50 without seat, while child at height above 95cm must purchase tickets according to price scale. Terms &amp; Conditions applies.</li>
                    <li>Strictly no video recording and photography during the show.</li>
                    <li>Video cameras, cameras and tablet computers such as iPad are not allowed in the venue.</li>
                    <li>Lost or damaged ticket(s) will not be entertained.</li>
                    <li>The concert starts at 10.00pm sharp, so please be seated by or before then.</li>
                    <li>Late arrival may result in non-admittance until a suitable break in the performance.</li>
                    <li>每张门票只限入场一次。</li>
                    <li>身高95公分及以下的儿童需购买价值50令吉只限入场不附席位的儿童票；而身高超过95公分以上的儿童则需依据票价购票。须符合条规。</li>
                    <li>演出进行期间，一律不允许录影及摄影。</li>
                    <li>录影机、相机及智能性平板电脑，如：iPad等，一律不允许带进表演现场。</li>
                    <li>门票若遗失或损坏，恕不受理。</li>
                    </li>演唱会将于晚上10点00分开始，请准时或提前入场。</li>
                    <li>迟到可能导致禁止入场，直至演出小休时间为止。</li>
                  </ul>

                  <h2>IMPORTANT NOTE:</h2>
                  <ol>
                    {{-- <li>Prices shown inclusive RM 4.00 AirAsiaRedTix fee and 6% GST.</li> --}}
                    <li>Prices shown inclusive RM 4.00 AirAsiaRedTix fee.</li>
                    <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li>
                    <li>RM5.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                    <li>Online ticket selling will close 2 days prior to event day, subject to availability.</li>
                  </ol>
                  <h2>Enquiries Hotline:</h2>
                  <p>Genting: 03-2718 1118 or visit website www.rwgenting.com</p>
                  <h2>For enquiry only:</h2>
                  <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                </div>
              </div>
            </div>
          </div>

        </section>
      </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')
<script type="text/javascript">
//Initialize Swiper
var swiper = new Swiper('.swiper-container', {
    pagination: '.swiper-pagination',        
    paginationClickable: true,
    slidesPerView: 'auto',
    spaceBetween: 10,
    nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev',
    freeMode: true
});

// Enlarge Seat Plan Image
$(function() {
    $('.seatPlanImg').on('click', function() {
    $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
    $('#enlargeImageModal').modal('show');
    });
});

// Hide top Banner when page scroll
var header = $('.eventBanner');
var range = 450;

$(window).on('scroll', function () {
    
    var scrollTop = $(this).scrollTop();
    var offset = header.offset().top;
    var height = header.outerHeight();
    offset = offset + height;
    var calc = 1 - (scrollTop - offset + range) / range;

    header.css({ 'opacity': calc });

    if ( calc > '1' ) {
    header.css({ 'opacity': 1 });
    } else if ( calc < '0' ) {
    header.css({ 'opacity': 0 });
    }
});

// Smooth scroll for acnhor links
$('a[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
    var target = $(this.hash);
    target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
    if (target.length) {
        $('html, body').animate({
        scrollTop: target.offset().top
        }, 1000);
        return false;
    }
    }
});
</script>

{{-- Buy button disable --}}
<script type="text/javascript">
    $(function() {
        $('a[id^=buyButton]').each(function() {
            var date = new Date();
            var enddate = $(this).attr('datetime'); 
            if ( Date.parse(date) >= Date.parse(enddate)) {
              $(this).addClass('disabled');
            }
        });
    });
</script>
@endsection

@section('modal')
@include('layouts.partials.modals._seatplan')
@include('layouts.partials.modals._getTix')
@endsection