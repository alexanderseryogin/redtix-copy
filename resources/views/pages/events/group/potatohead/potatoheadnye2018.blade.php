{{-- @extends('master') --}}
@extends('masterforpotatoheadnye')
@section('title')
    New Year's Eve at Potato Head Beach Club with Disclosure and Special Guest
@endsection

@section('header')
    @include('layouts.partials._header')
    <meta property="og:url" content="{{ Request::fullUrl() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="New Year's Eve at Potato Head Beach Club with Disclosure and Speacial Guest" />
    <meta property="og:description" content="The biggest night of the year is fast approaching and, as always, all roads are leading to Potato Head Beach Club."/>
    <meta property="og:image" content="{{ Request::Url().'images/potatohead/nye2018/thumbnail.jpg' }}" />
@endsection

@section('content')
    <!-- Banner Section -->
    <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/potatohead/nye2018/web-banner.jpg')}}" style="width: 100%" class="img-responsive" alt="New Year's Eve at Potato Head Beach Club with Disclosure and Special Guest"></div>
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            <img src="{{asset('images/potatohead/nye2018/thumbnail.jpg')}}" style="width: 100%" class="img-responsive" alt="New Year's Eve at Potato Head Beach Club with Disclosure and Special Guest">
        </div>
    </section>
    <!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
            <div class="row priceNbtn">
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="row">
                        <div class="col-sm-9 leftBox">
                            <h6>New Year's Eve at Potato Head Beach Club with Disclosure and Special Guest</h6> Tickets from <span>IDR1,000,000</span>
                        </div>
                        <div class="col-sm-3 text-center">
                            <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar section-white">
            <section class="pageCategory-section last">
                <div class="container intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i>  31st Dec 2018</div>
                            <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i>  Potato Head Beach Club, Bali  <a target="_blank" href="https://goo.gl/maps/ViaZE8gHKT62">View Map</a></div>
                            <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> Monday, 31 December (4.00 pm - 2.00am)</div>
                            <div class="clearfix">&nbsp;</div>
                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <div class="addthis_inline_share_toolbox"></div>
                            <!-- /sharing -->
                            <hr>
                            <p><h2>New Year's Eve at Potato Head Beach Club with Disclosure and Special Guest</h2><br/>
                            The biggest night of the year is fast approaching and, as always, all roads are leading to Potato Head Beach Club. We want to celebrate the night, and our island home, in true Potato Head style, so we are teaming up with some very special members of our global tribe to give you an unforgettable New Year’s bash under our favourite tropical skies.</p>
                            <p>Having risen to superstardom at the speed of light, Disclosure has made the transition into one of the most sought-after collaborators across the music industry with a repertoire that lists Sam Smith, The Weeknd, London Grammar, Miguel, Lorde, Q-tip, Usher, Jamie Woon and Mary J Blige. Despite their rapid ascent the meticulous craftsmanship behind their many modern day dance-pop anthems is undeniable, a fact echoed by the massive critical acclaim they continue to receive. 
                            Disclosure is considered the masterminds behind a new wave in U.K. house music, garage and R&B, and they have sent audiences into rapturous ecstasy across sonic territories around the world hundreds of times over with their now-iconic Disclosure sound— an effortless combination of the 2-step garage rhythms and dubstep basslines of their locale and their own rich musical heritage of soul, jazz and ‘90s hip-hop.</p>
                            <p>We cannot wait to dance into 2019 to the hypnotic sounds of one the biggest names in music, so make sure you join in before it’s too late!</p>
                        </div>
                    </div>
                </div>
                {{-- <div class="clearfix"></div> --}}
            </section>

            {{-- <section class="pageCategory-section last section-grey">
                <div class="container">
                    <div class="gallery text-center">
                        <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
                        <!-- Swiper -->
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <a href="images/potatohead/nye2018/gallery-dj-1.jpg" data-featherlight="image"><img class="" src="images/potatohead/nye2018/gallery-dj-1.jpg" alt=""></a>
                                </div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next swiper-button-white"></div>
                            <div class="swiper-button-prev swiper-button-white"></div>
                        </div><!-- /Swiper -->
                    </div>
                </div>
            </section> --}}

            <section class="pageCategory-section last"><a id="anchorPrice"></a>
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                            <div class="text-center">
                                <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                                <p>Select ticket</p>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="table-responsive">
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Category</th>
                                            <th>Ticket Price</th>
                                            <th>Remarks</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Early Bird</td>
                                            <td><strike>IDR 1,000,000</strike><br/>SOLD OUT</td>
                                            <td>-</td>
                                        </tr>
                                        <tr>
                                            <td>Pre - Sale 1</td>
                                            <td>IDR 1,200,000</td>
                                            <td>-</td>
                                        </tr>
                                        <tr>
                                            <td>VIP Lounge + GA<br/>9.30pm until 1.00am free-flow</td>
                                            <td>IDR 4,200,000</td>
                                            <td style="font-size: 9px;">Includes a selection of authentic Indonesian canapés as well as free-flow drinks in sheltered area from 9.30pm until 1am.<br/><br/>(Free flow includes house spirits, wine and beer)</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="buyAlert-bar">
                                <a class="btn btn-danger" id="buyButton" datetime="Dec 31 2018 12:00:00 GMT+0800" target="_blank" href="https://redtix.cognitix.id/aart/potatohead-nye/booking" role="button">BUY TICKETS</a>
                                {{-- <span class="or">/</span>
                                <span class="popData-btn" data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets <i class="fa fa-info-circle" aria-hidden="true"></i></span> --}}
                            </div>
                            <div class="note text-left">
                                <h2>Important Notes</h2>
                                <ol>
                                    <li>Prices shown exclude IDR30,000 AirAsiaRedTix fee.</li>
                                    {{-- <li>Price shown exclude 3.5% Card Charges and IDR5,500 Internet Processing Fee</li> --}}
                                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                                    <li>Online ticket selling will close mid day prior to the event, subject to availability.</li>
                                    <li>Adult: 18 years old and above.</li>
                                    <li>Corporate credit cards are not accepted</li>
                                </ol>
                                <h2>For enquiry only:</h2>
                                <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 350;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });
    </script>

    {{-- Buy button disable --}}
    <script type="text/javascript">
        $(function() {
            $('a[id^=buyButton]').each(function() {
                var date = new Date();
                var enddate = $(this).attr('datetime'); 
                if ( Date.parse(date) >= Date.parse(enddate)) {
                  $(this).addClass('disabled');
                }
            });
        });
    </script>

@endsection

@section('modal')
    @include('layouts.partials.modals._seatplan')
    @include('layouts.partials.modals._getTix')
@endsection