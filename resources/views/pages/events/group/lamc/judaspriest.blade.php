@extends('master')
@section('title')
    Judas Priest with Special Guests BABYMETAL - Live in Singapore
@endsection

@section('header')
    @include('layouts.partials._header')
    <meta property="og:url" content="{{ Request::fullUrl() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Judas Priest with Special Guests BABYMETAL - Live in Singapore" />
    <meta property="og:description" content="LAMC Productions is excited to announce that one of heavy metal’s most legendary acts, JUDAS PRIEST, will be bringing their ‘Firepower’ tour to Singapore on  Tuesday, 4 December, 2018 at ZEPP@BIGBOX Singapore!"/>
    <meta property="og:image" content="{{ Request::Url().'images/lamc/singles/judaspriest/thumbnail.jpg' }}" />
@endsection

@section('content')
<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:600|Roboto" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
    <link type="text/css" rel="stylesheet" href="css/custom2.css">
    <link type="text/css" rel="stylesheet" href="css/clock.css">
    <link type="text/css" rel="stylesheet" href="/css/custom/redtix.css">
    <link type="text/css" rel="stylesheet" href="css/timeTo.css">

    <!-- Banner Section -->
    <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/lamc/singles/judaspriest/web-banner.jpg')}}" style="width: 100%" class="img-responsive" alt="Judas Priest with Special Guests BABYMETAL - Live in Singapore"></div>
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            <img src="{{asset('images/lamc/singles/judaspriest/thumbnail.jpg')}}" style="width: 100%" class="img-responsive" alt="Judas Priest with Special Guests BABYMETAL - Live in Singapore">
        </div>
    </section>
    <!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
            <div class="row priceNbtn">
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="row">
                        <div class="col-sm-9 leftBox">
                            <h6>Judas Priest with Special Guests BABYMETAL - Live in Singapore</h6> Tickets from <span>RM446</span>
                        </div>
                        <div class="col-sm-3 text-center">
                            <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar section-white">
            <section class="pageCategory-section last">
                <div class="container intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i>  4th December 2018</div>
                            <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i>  Zepp@Bigbox, Level 3, Big Box 1 Venture Avenue, 608521 <a target="_blank" href="https://goo.gl/maps/NcUVPEJKmm32">View Map</a></div>
                            <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i>  8.00pm - 10.00pm</div>
                            <div class="clearfix">&nbsp;</div>
                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <div class="addthis_inline_share_toolbox"></div>
                            <!-- /sharing -->
                            <hr>
                            <div class="col-sm-8">
                                <p><h2><b>LAMC Productions</b> is excited to announce that one of heavy metal’s most legendary acts, JUDAS PRIEST, will be bringing their ‘Firepower’ tour to Singapore with special guests, Japanese Metal band, <b>BABYMETAL</b>, on <b>Tuesday, 4 December, 2018 at ZEPP@BIGBOX Singapore!</b></h2></p>
                                <p>Come December 2018, headbangers in Singapore will get the opportunity to experience the mighty Priest take the stage with the very loved <b>BABYMETAL!</b> Double the fun!</p>
                                <p><font style="text-align:center;"><i>“Forty-eight years and eighteen albums into Judas Priest’s career, and -- against all odds -- the Metal Gods are still in powerful form.”</i> - <b>Loudwire</b></font></p>
                                <p>There are few heavy metal bands that have managed to scale the heights that Judas Priest have during their 40 year career - originally formed during the early '70s in Birmingham, England, Judas Priest is responsible for some of the genre's most influential and landmark albums (1980's 'British Steel,' 1982's 'Screaming for Vengeance,' 1990's 'Painkiller,' etc.) and for decades have been one of the greatest live bands in the entire heavy metal genre including an iconic performance at Live Aid in 1985 - they also brought metal to the masses by their appearance on American Idol in 2011 - plus Judas Priest were one of the first metal bands to exclusively wear leather and studs -  a look that began during this era and was eventually embraced by metal fans throughout the world! And in 2017, the band was nominated for induction into the Rock and Roll Hall of Fame.</p>
                                <p>In March 2018, Judas Priest also released their latest album, ‘Firepower’ (produced by Andy Sneap and Tom Allom), to rave reviews. The album sold around 49,000 copies in the United States within its first week of release, debuting at No. 5 on the Billboard 200 chart, making it the band's highest-charting album in the US.</p>
                                <p><b>ABOUT BABYMETAL:</b><br />
                                BABYMETAL was formed in 2010. In March 2014, they took the stage at Nippon Budokan for a 2-day gig, making them the youngest female act to perform there. Their 1st studio album BABYMETAL instantly made it to the US Billboard charts. In summer of the same year, they embarked on their first world tour, performing on the main stage at UK’s prodigious festival Sonisphere Festival 2014 in front of 65,000 festival goers and supporting Lady Gaga on her ArtRave: The Artpop Ball 2014 Tour.</p>
                                <p>In summer of 2015, BABYMETAL received various awards from prominent UK music magazines KERRANG! and METAL HAMMER and became the first Japanese artist to ever receive these awards. BABYMETAL also performed on the main stage at the massive Reading & Leeds. In April 2016, BABYMETAL had a simultaneous global release for their 2nd album METAL RESISTANCE. Not only did they make the charts in Japan, they also became the highest-charting Japanese band ever on UK’s Official Albums Chart, landing at no. 15th.</p>
                                <p>Don’t miss this concert!</p>
                            </div>
                            <div class="col-sm-4">
                                <iframe src="https://open.spotify.com/embed/artist/2tRsMl4eGxwoNabM08Dm4I" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="embed-responsive embed-responsive-16by9" style="margin-top: 40px; margin-bottom: 40px;">
                                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/fT5aVTJSmyw?ecver=2" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- <div class="clearfix"></div> --}}
            </section>

            <section class="pageCategory-section last section-grey">
                <div class="container">
                    <div class="gallery text-center">
                        <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
                        <!-- Swiper -->
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <a href="images/lamc/singles/judaspriest/gallery-1.jpg" data-featherlight="image"><img class="" src="images/lamc/singles/judaspriest/gallery-1.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/lamc/singles/judaspriest/gallery-2.jpg" data-featherlight="image"><img class="" src="images/lamc/singles/judaspriest/gallery-2.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/lamc/singles/judaspriest/gallery-3.jpg" data-featherlight="image"><img class="" src="images/lamc/singles/judaspriest/gallery-3.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/lamc/singles/judaspriest/gallery-4.jpg" data-featherlight="image"><img class="" src="images/lamc/singles/judaspriest/gallery-4.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/lamc/singles/judaspriest/web-banner.jpg" data-featherlight="image"><img class="" src="images/lamc/singles/judaspriest/web-banner.jpg" alt=""></a>
                                </div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next swiper-button-white"></div>
                            <div class="swiper-button-prev swiper-button-white"></div>
                        </div><!-- /Swiper -->
                    </div>
                </div>
            </section>

            <section class="pageCategory-section last"><a id="anchorPrice"></a>
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">

                            @include('layouts.partials._showtickets')

                            {{-- <span class="importantNote">* First 100 ticket purchasers will get 1 x NETS Card with credit of SGD10. First come first serve basis. NETS Card to be collected at ticket redemption counter on event day.</span> --}}
                            <div class="note text-left">
                                <h2>Important Notes</h2>
                                <ol>
                                    <li>Special Limited offer at AirAsia RedTix only</li>
                                    <li>Malaysian MyKad required at ticket redemption and entry into event for MyKad package</li>
                                    <li>Prices shown exclude RM4.00 AirAsiaRedTix fee and 3% Credit card fee.</li>
                                    {{-- <li>All ticket prices will be paid in Malaysian Ringgit. Australian Dollar prices are shown for reference purposes only.</li>
                                    <li>Prices quoted in AUD may differ due to currency fluctuations.</li> --}}
                                    <li>All ticket prices will be paid in Malaysian Ringgit.</li>
                                    <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li>
                                    <li>RM10.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                                    <li>Online ticket selling will close 3 days prior to event day, subject to availability.</li>
                                </ol>
                                <h2>For enquiry only:</h2>
                                <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 350;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });

    // Modal Announcement
	$(document).ready(function(){
      $("#announcementModal").modal('show');
  	});	

    </script>


    {{-- Buy button disable --}}
    <script type="text/javascript">
        $(function() {
            $('a[id^=buyButton]').each(function() {
                var date = new Date();
                var enddate = $(this).attr('datetime'); 
                if ( Date.parse(date) >= Date.parse(enddate)) {
                  $(this).addClass('disabled');
                }
            });
        });
    </script>

@endsection

@section('modal')
    @include('layouts.partials.modals._seatplan')
    @include('layouts.partials.modals._getTix')
    @include('layouts.partials.modals._moreInfo')

   <div id="announcementModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
                    <h1><i class="fa fa-bullhorn text-warning" aria-hidden="true"></i></h1>
                    <h4>Announcement</h4>
                    <div class="clearfix">&nbsp;</div>
                    <div class="well">
                        <p>1. Special Limited offer for Malaysian MyKad holder at AirAsia RedTix only</p>
                        <p>2. MyKad strictly required at ticket redemption and entry into event</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection