@extends('master')
@section('title')
    Mike Shinoda of Linkin Park's Post Traumatic Tour Singapore
@endsection

@section('header')
    @include('layouts.partials._header')
    <meta property="og:url" content="{{ Request::fullUrl() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Mike Shinoda of Linkin Park's Post Traumatic Tour Singapore" />
    <meta property="og:description" content="LAMC Productions is excited to announce that the multi talented singer, rapper & producer, MIKE SHINODA, of Linkin Park, will  will perform in Singapore, as part of his ‘Post Traumatic’ tour on Wednesday, 22 August, 2018 at ZEPP@BIGBOX Singapore!"/>
    <meta property="og:image" content="{{ Request::Url().'images/lamc/singles/mikeshinoda/thumbnail.jpg' }}" />
@endsection

@section('content')
    <!-- Banner Section -->
    <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/lamc/singles/mikeshinoda/web-banner.jpg')}}" style="width: 100%" class="img-responsive" alt="Mike Shinoda of Linkin Park's Post Traumatic Tour Singapore"></div>
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            <img src="{{asset('images/lamc/singles/mikeshinoda/thumbnail.jpg')}}" style="width: 100%" class="img-responsive" alt="Mike Shinoda of Linkin Park's Post Traumatic Tour Singapore">
        </div>
    </section>
    <!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
            <div class="row priceNbtn">
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="row">
                        <div class="col-sm-9 leftBox">
                            <h6>Mike Shinoda of Linkin Park's Post Traumatic Tour Singapore</h6> Tickets from <span>RM250</span>
                        </div>
                        <div class="col-sm-3 text-center">
                            <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar section-white">
            <section class="pageCategory-section last">
                <div class="container intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i>  22nd August 2018</div>
                            <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i>  Zepp@Bigbox, Level 3, Big Box 1 Venture Avenue, 608521 <a target="_blank" href="https://goo.gl/maps/NcUVPEJKmm32">View Map</a></div>
                            <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> Wednesday, 22 August (8.00 pm - 10.00pm)</div>
                            <div class="clearfix">&nbsp;</div>
                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <div class="addthis_inline_share_toolbox"></div>
                            <!-- /sharing -->
                            <hr>
                            <p><h2>Mike Shinoda of Linkin Park's Post Traumatic Tour Singapore</h2><br/>
                            <b>LAMC Productions</b> is excited to announce that the multi talented singer, rapper &amp; producer, <b>MIKE SHINODA</b>, of Linkin Park, will  will perform in Singapore, as part of his ‘Post Traumatic’ tour on <b>Wednesday, 22 August, 2018 at ZEPP@BIGBOX Singapore!</b>
                            <br /><br /><i>“I’m looking forward to getting back to releasing music and touring, those things are ‘normal’ to me -- it’s what my professional life has revolved around for years. And I know this will be different, so I’m excited to figure out what this new normal is.”</i> - <b>Mike Shinoda</b>
                            <br /><br />Mike Shinoda is a songwriter, performer, record producer, film composer and visual artist with a B.A. in illustration and Doctorate of Humane Letters from Art Center College of Design. He is best known as co-lead vocalist for multi-platinum Grammy-Award winning rock band, Linkin Park, which has sold over 55 million albums worldwide and commanded a massive fan following, holding the title as the most-liked band on Facebook and amassing over 5.5 billion YouTube views. Shinoda is the founding member of the group, which achieved the best-selling debut of this century with their Diamond-certified album, Hybrid Theory, selling over 10 million copies in the US alone. Collectively, they sold out stadiums around the globe and earned a multitude of accolades including 2 Grammy Awards, 5 American Music Awards, 4 MTV VMA Awards, 10 MTV Europe Music Awards, 3 World Music Awards, and most recently, “Rock Album of the Year” at the 2018 iHeartRadio Music Awards for their seventh studio album, <i>One More Light</i>. 
                            <br /><br />In the months since the passing of Linkin Park vocalist Chester Bennington, Shinoda has immersed himself in art as a way of processing his grief. With no agenda, Shinoda hunkered down alone in his Los Angeles home and began writing, recording, and painting.  In January, he released the <i>Post Traumatic EP</i> consisting of three deeply personal songs  – each one a powerful, stream-of-consciousness expression of unvarnished grief – accompanied by homemade visuals that Shinoda filmed, painted and edited himself. The response was overwhelmingly positive, with <i>New York Times</i> stating “The tracks are reverberant electronic dirges; the rhymes, heading into sung choruses, testify to bewilderment, mourning, resentment, self-pity and questions about what to do.”
                            <br /><br />Following the EP release, Shinoda continued to create, and the result is the upcoming Post Traumatic, a transparent and intensely personal album that, despite its title, isn’t entirely about grief, though it does start there. 
                            <br /><br />Get ready for Mike Shinoda’s first tour as a solo artist!
                            </p>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="embed-responsive embed-responsive-16by9" style="margin-top: 40px; margin-bottom: 40px;">
                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/mzzyrpvLY3I?ecver=2" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                    </div>
                </div>
            </section>

            {{-- <section class="pageCategory-section last section-grey">
                <div class="container">
                    <div class="gallery text-center">
                        <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
                        <!-- Swiper -->
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <a href="images/lamc/singles/mikeshinoda/gallery-dj-1.jpg" data-featherlight="image"><img class="" src="images/lamc/singles/mikeshinoda/gallery-dj-1.jpg" alt=""></a>
                                </div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next swiper-button-white"></div>
                            <div class="swiper-button-prev swiper-button-white"></div>
                        </div><!-- /Swiper -->
                    </div>
                </div>
            </section> --}}

            <section class="pageCategory-section last"><a id="anchorPrice"></a>
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                            <div class="text-center">
                                <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                                <p>Select ticket</p>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="table-responsive">
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Category</th>
                                            <th>Ticket Price</th>
                                            <th>Remarks</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>GA Standing</td>
                                            <td>RM 300.00</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>GA Standing MY</td>
                                            <td>RM 250.00</td>
                                            <td>For Malaysian MyKad Holder</td>
                                        </tr>
                                        <tr>
                                            <td>GA Standing MY2<br/>
                                            (includes 2 Tickets)</td>
                                            <td>RM 450.00</td>
                                            <td>For Malaysian MyKad Holder (2 Tickets)</td>
                                        </tr>
                                        {{-- <tr>
                                            <td>RedTix Package A<br />(2 Tickets + 1 Night Stay)</td>
                                            <td>RM 950.00</td>
                                            <td>YOTEL Singapore (4 Star)</td>
                                        </tr>
                                        <tr>
                                            <td>RedTix Package B<br />(2 Tickets + 1 Night Stay)</td>
                                            <td>RM 1050.00</td>
                                            <td>Orchard Hotel Singapore (5 Star)</td>
                                        </tr>
                                        <tr>
                                            <td>RedTix Package C<br />(2 Tickets + 1 Night Stay)</td>
                                            <td>RM 550.00</td>
                                            <td>Tune Hotel Danga Bay, Johor (3 Star)</td>
                                        </tr> --}}
                                    </tbody>
                                </table>
                            </div>
                            <div class="buyAlert-bar">
                                <a class="btn btn-danger" id="buyButton" datetime="Aug 20 2018 00:00:00 GMT+0800" target="_blank" href="http://redtix-tickets.airasia.com/en-AU/shows/mike shinoda of linkin park's post traumatic tour singapore/events" role="button">BUY TICKETS</a>
                                {{-- <span class="or">/</span>
                                <span class="popData-btn" data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets <i class="fa fa-info-circle" aria-hidden="true"></i></span> --}}
                            </div>
                            <span class="importantNote">1. Special Limited offer at AirAsia RedTix only</span>
                            <span class="importantNote">2. Malaysian MyKad required at ticket redemption and entry into event for GA Standing MY &amp; GA Standing MY2 tickets</span>
                            <div class="note text-left">
                                <h2>Important Notes</h2>
                                <ol>
                                    {{--<li>Prices shown exclude RM8.00 AirAsiaRedTix fee and 3% Credit card fee.</li>
                                    <li>All ticket prices will be paid in Malaysian Ringgit. Indonesian Rupiah prices are shown for reference purposes only.</li>
                                    <li>Prices quoted in IDR may differ due to currency fluctuations.</li> --}}
                                    <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li>
                                    <li>RM10.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                                    {{-- <li>Online ticket selling will close 1 days prior to event day, subject to availability.</li>
                                    <li>Adult: 18 years old and above.</li> --}}
                                </ol>
                                <h2>For enquiry only:</h2>
                                <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 350;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });

    // Modal Announcement
	$(document).ready(function(){
      $("#announcementModal").modal('show');
  	});	
    </script>

    {{-- Buy button disable --}}
    <script type="text/javascript">
        $(function() {
            $('a[id^=buyButton]').each(function() {
                var date = new Date();
                var enddate = $(this).attr('datetime'); 
                if ( Date.parse(date) >= Date.parse(enddate)) {
                  $(this).addClass('disabled');
                }
            });
        });
    </script>

@endsection

@section('modal')
    @include('layouts.partials.modals._seatplan')
    @include('layouts.partials.modals._getTix')

   <div id="announcementModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
                    <h1><i class="fa fa-bullhorn text-warning" aria-hidden="true"></i></h1>
                    <h4>Announcement</h4>
                    <div class="clearfix">&nbsp;</div>
                    <div class="well">
                        <p>1. Special Limited offer at AirAsia RedTix only</p>
                        <p>2. Malaysian MyKad required at ticket redemption and entry into event for GA Standing MY &amp; GA Standing MY2 tickets</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection