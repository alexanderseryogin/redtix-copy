@extends('master')
@section('title')
    Avrynation Dannic
@endsection

@section('header')
    @include('layouts.partials._header')
    <meta property="og:url" content="{{ Request::fullUrl() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Avrynation Dannic" />
    <meta property="og:description" content="Dannic, the highly prodigious DJ and producer from Breda in the Netherlands"/>
    <meta property="og:image" content="{{ Request::Url().'images/avry/dannic/thumbnail1.jpg' }}" />
@endsection

@section('content')
    <!-- Banner Section -->
    <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/avry/dannic2018/web-banner.jpg')}}" style="width: 100%" class="img-responsive" alt="Avrynation Dannic"></div>
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            <img src="{{asset('images/avry/dannic2018/thumbnail1.jpg')}}" style="width: 100%" class="img-responsive" alt="Avrynation Dannic">
        </div>
    </section>
    <!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
            <div class="row priceNbtn">
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="row">
                        <div class="col-sm-9 leftBox">
                            <h6>Avrynation Dannic</h6> Tickets from <span>RM108</span>
                        </div>
                        <div class="col-sm-3 text-center">
                            <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar section-white">
            <section class="pageCategory-section last">
                <div class="container intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i>  21st September 2018</div>
                            <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i>  Avry Nightclub, 9 Raffles Boulevard, Millenia Walk, Singapore <a target="_blank" href="https://goo.gl/maps/BWc37Jvuxc42">View Map</a></div>
                            <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> Saturday (11.00 pm - 6.00am)</div>
                            <div class="clearfix">&nbsp;</div>
                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <div class="addthis_inline_share_toolbox"></div>
                            <!-- /sharing -->
                            <hr>
                            <p><h2>AVRYNATION with Dannic (NL)</h2><br/>
                            Dannic, the highly prodigious DJ and producer from Breda in the Netherlands - the hometown of Tiesto and Hardwell - is one of the hottest acts to have emerged from the new generation of Dutch artists breaking through in recent years. His phenomenal rise of 44 places to take the #30 spot in the 2014 DJ Mag Top 100 DJs Poll, followed by a further jump of four more places to take the #26 spot in the 2015 DJ Mag Top 100 DJs Poll confirms to tastemakers that he is set to remain a driving force within the scene</p>
                        </div>
                    </div>
                </div>
                {{-- <div class="clearfix"></div> --}}
            </section>

            {{-- <section class="pageCategory-section last section-grey">
                <div class="container">
                    <div class="gallery text-center">
                        <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
                        <!-- Swiper -->
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <a href="images/avry/dannic2018/gallery-1.jpg" data-featherlight="image"><img class="" src="images/avry/dannic2018/gallery-1.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/avry/dannic2018/gallery-2.jpg" data-featherlight="image"><img class="" src="images/avry/dannic2018/gallery-2.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/avry/dannic2018/gallery-3.jpg" data-featherlight="image"><img class="" src="images/avry/dannic2018/gallery-3.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/avry/dannic2018/gallery-4.jpg" data-featherlight="image"><img class="" src="images/avry/dannic2018/gallery-4.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/avry/dannic2018/gallery-5.jpg" data-featherlight="image"><img class="" src="images/avry/dannic2018/gallery-5.jpg" alt=""></a>
                                </div>

                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next swiper-button-white"></div>
                            <div class="swiper-button-prev swiper-button-white"></div>
                        </div><!-- /Swiper -->
                    </div>
                </div>
            </section> --}}

            <section class="pageCategory-section last"><a id="anchorPrice"></a>
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                            <div class="text-center">
                                <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                                <p>Select ticket</p>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="table-responsive">
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Category</th>
                                            <th>Ticket Price</th>
                                            <th>Remarks</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Limited Pre Sale</td>
                                            <td><strike>SGD35 / RM108</strike><br /><font style="color:red;">SOLD OUT</font></td>
                                            <td rowspan="2">Free Housepour Drink With Every Ticket</td>
                                        </tr>
                                        <tr>
                                            <td>Early Bird</td>
                                            <td>SGD40 / RM123</td>
                                        </tr>
                                        <tr>
                                            <td>General Entry</td>
                                            <td>SGD50 / RM154</td>
                                            <td>Available at the door from 11.00pm</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="buyAlert-bar">
                                <a class="btn btn-danger" id="buyButton" datetime="Sep 21 2018 20:00:00 GMT+0800" target="_blank" href="https://redtix-tickets.airasia.com/en-AU/shows/avrynation dannic/events" role="button">BUY TICKETS</a>
                                {{-- <span class="or">/</span>
                                <span class="popData-btn" data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets <i class="fa fa-info-circle" aria-hidden="true"></i></span> --}}
                            </div>
                            
                            <div class="note text-left">
                                <h2>Important Notes</h2>
                                {{-- <ol>
                                    <li>Prices shown exclude RM4.00 AirAsiaRedTix fee and 3% Credit card fee.</li>
                                    <li>All ticket prices will be paid in Malaysian Ringgit. Chinese Renminbi prices are shown for reference purposes only.</li>
                                    <li>Prices quoted in RMB may differ due to currency fluctuations.</li>
                                    <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li>
                                    <li>RM10.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                                    <li>Online ticket selling will close 14 days prior to event day, subject to availability.</li>
                                </ol> --}}
                                <ol>
                                    <li>Prices shown exclude RM4.00 AirAsiaRedTix fee and 3% Credit card fee.</li>
                                    <li>All ticket prices will be paid in Malaysian Ringgit. Singapore Dollar prices are shown for reference purposes only.</li>
                                    <li>Prices quoted in SGD may differ due to currency fluctuations.</li>
                                    <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li>
                                    <li>RM10.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                                    <li>Online ticket selling will close 2 days prior to event day, subject to availability.</li>
                                </ol>
                                <h2>For enquiry only:</h2>
                                <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                            </div>

                            {{-- <div class="buyAlert-bar" style="border:thin">
                            <span>REGISTER NOW TO GET ACCESS TO NEON COUNTDOWN KUALA LUMPUR 2018 EARLY BIRD PRICING</span>
                                <a class="btn btn-danger" datetime="Sep 22 2018 00:00:00 GMT+0800" target="_blank" href="neonkualalumpur" role="button">Register Now</a>
                            </div> --}}


                        </div>
                    </div>
                </div>
            </section>
        </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 350;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });
    </script>

    {{-- Buy button disable --}}
    <script type="text/javascript">
        $(function() {
            $('a[id^=buyButton]').each(function() {
                var date = new Date();
                var enddate = $(this).attr('datetime'); 
                if ( Date.parse(date) >= Date.parse(enddate)) {
                  $(this).addClass('disabled');
                }
            });
        });
    </script>

@endsection

@section('modal')
    @include('layouts.partials.modals._seatplan')
    @include('layouts.partials.modals._getTix')
@endsection