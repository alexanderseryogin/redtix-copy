@extends('master')
@section('title')
    The Five Elves Musical: A Magical Journey
@endsection

@section('header')
    @include('layouts.partials._header')
    <meta property="og:url" content="{{ Request::fullUrl() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="The Five Elves Musical: A Magical Journey" />
    <meta property="og:description" content="Astro小太阳年度巨献，大马最大型中文儿童歌舞剧《五力魔法小精灵之奇幻旅程》将于11月首登吉隆坡KLCC Plenary Hall开演！"/>
    <meta property="og:image" content="{{ Request::Url().'images/astro/fiveelves2018/thumbnail.jpg' }}" />
@endsection

@section('content')
    <!-- Banner Section -->
    <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/astro/fiveelves2018/web-banner.jpg')}}" style="width: 100%" class="img-responsive" alt="The Five Elves Musical: A Magical Journey"></div>
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            <img src="{{asset('images/astro/fiveelves2018/thumbnail.jpg')}}" style="width: 100%" class="img-responsive" alt="The Five Elves Musical: A Magical Journey">
        </div>
    </section>
    <!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
            <div class="row priceNbtn">
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="row">
                        <div class="col-sm-9 leftBox">
                            <h6>The Five Elves Musical: A Magical Journey</h6> Tickets from <span>RM54.60</span>
                        </div>
                        <div class="col-sm-3 text-center">
                            <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar section-white">
            <section class="pageCategory-section last">
                <div class="container intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i>  8th - 11th Nov 2018</div>
                            <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i>  Plenary Hall, KLCC, Kuala Lumpur <a target="_blank" href="https://goo.gl/maps/5uY4thiJ4uA2">View Map</a></div>
                            <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> 08 Nov 2018, Thursday - 8.00pm</div>
                            <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> 09 Nov 2018, Friday - 8.00pm</div>
                            <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> 10 Nov 2018, Saturday - 3.00pm / 8.00pm</div>
                            <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> 11 Nov 2018, Sunday - 2.00pm / 7.00pm</div>
                            <div class="clearfix">&nbsp;</div>
                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <div class="addthis_inline_share_toolbox"></div>
                            <!-- /sharing -->
                            <div class="col-sm-12">
                                <hr>
                                <p><h2> The Five Elves Musical: A Magical Journey</h2><br/>
                                Astro小太阳年度巨献，大马最大型中文儿童歌舞剧《五力魔法小精灵之奇幻旅程》将于11月首登吉隆坡KLCC Plenary Hall开演！
                                <ul>
                                    <li>由多媒体创作导演叶准准、著名音乐创作人罗忆诗担任音乐总监及著名舞编凌秀眉鼎力打造，斥资百万把100%本地原创同名3D动画片搬上舞台。</li>
                                    <li>结合童童欢乐园等原创歌曲，带领小太阳的哥哥姐姐等超过20位演员阵容，从千人大合唱、到欢乐舞动、到震撼视效、到神兽人物魔法变身超级英雄，势必让大人与小孩从寓教于乐，揉合欢乐和魔幻元素的故事中，让孩子学习自我认知，让大人感受陪伴的力量！</li>
                                    <li>爸爸妈妈们，有多久没有和孩子们度过快乐又有意义的亲子时光了呢？现在就与孩子一起经历奇幻的成长旅程吧</li>
                                    <li>大手牵小手，学习很快乐！We Are WONDER！陪伴孩子赴一场寓教于乐的故事飨宴！</li>
                                    <li>此歌舞剧将带领孩子以正能量迎向成长，鼓励每一个学习路上努力的孩子!</li>
                                </ul>
                                <p><b>【故事简介】</b><br/>
                                讲述性格内向自卑的小V，获得老龙王赠予童童作为礼物后，神兽化身的五力魔法小精灵来到地球，帮助小V掌握五种能力（学习、思考、创造、竞争、品格）以及重拾学习的乐趣，并与同学们展开了一段奇幻的成长旅程。</p>
                                <p>Astro Xiao Tai Yang (XTY) has been the No.1 Chinese-language kids learning channel in Malaysia for the past decade. Its signature show, Tong Tong’s Wonderland, is Astro’s highest-rating Chinese-language kids content. Moreover, its numerous characters seen in Jollyfish and Doo Doo Rhythm &amp; Beat resonate well with the kids. Over the years, XTY characters have made cameos at kids-related events and festivals while attracting huge crowds along the way.</p>
                                <p>This is the first and the largest original Chinese musical created by Malaysians for Malaysian kids, “The Five Elves: A Magical Journey (The Musical)”, as part of XTY’s efforts to advocate the five pillars of learning - learning, thinking, competing, creating &amp; developing character – the musical provides children with a head start for a lifelong active learning through performing arts. Originated from 3D Animation, a journey of self-discovery</p>
                                <p>This musical originated from a 3D animation, “The Five Elves: A Magical Journey”, which is produced by Astro Xiao Tai Yang and Malaysia-based animation studio, Happy Yeye. It tells the story of five superheroes who transformed from animal prototypes. They are given the mission to help an introverted girl, Xiao V, to regain her self-confidence. Together, they stand fighting against the bad with the companionship and motivation from Tong Tong. She won herself friendship &amp; love and, in the end, truly experience the joy of learning.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="pageCategory-section last section-grey">
                <div class="container">
                    <div class="gallery text-center">
                        <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
                        <!-- Swiper -->
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <a href="images/astro/fiveelves2018/gallery-1.jpg" data-featherlight="image"><img class="" src="images/astro/fiveelves2018/gallery-1.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/astro/fiveelves2018/gallery-2.jpg" data-featherlight="image"><img class="" src="images/astro/fiveelves2018/gallery-2.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/astro/fiveelves2018/gallery-3.jpg" data-featherlight="image"><img class="" src="images/astro/fiveelves2018/gallery-3.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/astro/fiveelves2018/gallery-4.jpg" data-featherlight="image"><img class="" src="images/astro/fiveelves2018/gallery-4.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/astro/fiveelves2018/gallery-5.jpg" data-featherlight="image"><img class="" src="images/astro/fiveelves2018/gallery-5.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/astro/fiveelves2018/gallery-6.jpg" data-featherlight="image"><img class="" src="images/astro/fiveelves2018/gallery-6.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/astro/fiveelves2018/gallery-7.jpg" data-featherlight="image"><img class="" src="images/astro/fiveelves2018/gallery-7.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/astro/fiveelves2018/gallery-8.jpg" data-featherlight="image"><img class="" src="images/astro/fiveelves2018/gallery-8.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/astro/fiveelves2018/gallery-9.jpg" data-featherlight="image"><img class="" src="images/astro/fiveelves2018/gallery-9.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/astro/fiveelves2018/gallery-10.jpg" data-featherlight="image"><img class="" src="images/astro/fiveelves2018/gallery-10.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/astro/fiveelves2018/gallery-11.jpg" data-featherlight="image"><img class="" src="images/astro/fiveelves2018/gallery-11.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/astro/fiveelves2018/gallery-12.jpg" data-featherlight="image"><img class="" src="images/astro/fiveelves2018/gallery-12.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/astro/fiveelves2018/gallery-13.jpg" data-featherlight="image"><img class="" src="images/astro/fiveelves2018/gallery-13.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/astro/fiveelves2018/gallery-14.jpg" data-featherlight="image"><img class="" src="images/astro/fiveelves2018/gallery-14.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/astro/fiveelves2018/gallery-15.jpg" data-featherlight="image"><img class="" src="images/astro/fiveelves2018/gallery-15.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/astro/fiveelves2018/gallery-16.jpg" data-featherlight="image"><img class="" src="images/astro/fiveelves2018/gallery-16.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/astro/fiveelves2018/gallery-17.jpg" data-featherlight="image"><img class="" src="images/astro/fiveelves2018/gallery-17.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/astro/fiveelves2018/gallery-18.jpg" data-featherlight="image"><img class="" src="images/astro/fiveelves2018/gallery-18.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/astro/fiveelves2018/gallery-19.jpg" data-featherlight="image"><img class="" src="images/astro/fiveelves2018/gallery-19.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/astro/fiveelves2018/gallery-20.jpg" data-featherlight="image"><img class="" src="images/astro/fiveelves2018/gallery-20.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/astro/fiveelves2018/gallery-21.jpg" data-featherlight="image"><img class="" src="images/astro/fiveelves2018/gallery-21.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/astro/fiveelves2018/gallery-22.jpg" data-featherlight="image"><img class="" src="images/astro/fiveelves2018/gallery-22.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/astro/fiveelves2018/gallery-23.jpg" data-featherlight="image"><img class="" src="images/astro/fiveelves2018/gallery-23.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/astro/fiveelves2018/gallery-24.jpg" data-featherlight="image"><img class="" src="images/astro/fiveelves2018/gallery-24.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/astro/fiveelves2018/gallery-25.jpg" data-featherlight="image"><img class="" src="images/astro/fiveelves2018/gallery-25.jpg" alt=""></a>
                                </div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next swiper-button-white"></div>
                            <div class="swiper-button-prev swiper-button-white"></div>
                        </div><!-- /Swiper -->
                    </div>
                </div>
            </section>

            <section class="pageCategory-section last"><a id="anchorPrice"></a>
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                            <div class="text-center">
                                <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                                <p>Select ticket</p>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="table-responsive">
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Ticket Category</th>
                                            <th>Original Price</th>
                                            <th>Ticket Price</th>
                                            <th>Seat Plan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td style="background-color: #EA1B88; color: #ffffff;">VVIP</td>
                                            <td><strike>RM 228.00</strike><br/><font style="font-size: 9px;">(Save 30%)</font></td>
                                            <td>RM 159.60</td>
                                            <td rowspan="5" style="background-color: #ffffff;"><img class="img-responsive seatPlanImg" src="images/astro/fiveelves2018/seat-plan.jpg" alt=""></td>
                                        </tr>
                                        <tr>
                                            <td style="background-color: #52BA5E; color: #ffffff;">Platinum</td>
                                            <td><strike>RM 198.00</strike><br/><font style="font-size: 9px;">(Save 30%)</font></td>
                                            <td>RM 138.60</td>
                                        </tr>
                                        <tr>
                                            <td style="background-color: #FAA74B; color: #ffffff;">Gold</td>
                                            <td><strike>RM 148.00</strike><br/><font style="font-size: 9px;">(Save 30%)</font></td>
                                            <td>RM 103.60</td>
                                        </tr>
                                        <tr>
                                            <td style="background-color: #828284; color: #ffffff;">Silver</td>
                                            <td><strike>RM 98.00</strike><br/><font style="font-size: 9px;">(Save 30%)</font></td>
                                            <td>RM 68.60</td>
                                        </tr>
                                        <tr>
                                            <td style="background-color: #26B2B9; color: #ffffff;">Normal</td>
                                            <td><strike>RM 78.00</strike><br/><font style="font-size: 9px;">(Save 30%)</font></td>
                                            <td>RM 54.60</td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                            <div class="buyAlert-bar">
                                <a class="btn btn-danger" id="buyButton" datetime="Nov 11 2018 12:00:00 GMT+0800" target="_blank" href="https://redtix-tickets.airasia.com/en-AU/shows/astro xty the five elves musical/events" role="button">BUY TICKETS</a>
                                {{-- <span class="or">/</span>
                                <span class="popData-btn" data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets <i class="fa fa-info-circle" aria-hidden="true"></i></span> --}}
                            </div>
                            {{-- <span class="importantNote">* Package validity - until 2nd Nov 2018</span> --}}
                            <div class="note text-left">
                                <h2>Terms &amp; Conditions for Children Tickets</h2>
                                <ol>
                                    <li>2岁或以上的儿童须持票入场， 并且需要大人陪同。未满2岁儿童不得入场。</li>
                                    <li>Children 2-year-old & above require a ticket for admission and must be accompanied by an adult.</li>
                                    <li>Children under 2 years old are not allowed to enter the hall;</li>
                                </ol>
                            </div>

                            <div class="note text-left">
                                <h2>Important Notes</h2>
                                <ol>
                                    {{-- <li>Prices shown exclude ticket fee.</li>
                                    <li>All ticket prices will be paid in Malaysian Ringgit. Indian Rupee prices are shown for reference purposes only.</li>
                                    <li>Prices quoted in INR may differ due to currency fluctuations.</li>
                                    <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li>
                                    <li>RM10.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                                    <li>Online ticket selling will close 2 days prior to event day, subject to availability.</li>
                                    <li>Adult: Strictly 18 years old and above.</li> --}}
                                    <li>Special Limited offer at AirAsia RedTix only</li>
                                    <li>Malaysian MyKad required at ticket redemption and entry into event for MyKad package</li>
                                    <li>Prices shown exclude RM4.00 AirAsiaRedTix fee and 3% Credit card fee.</li>
                                    <li>All ticket prices will be paid in Malaysian Ringgit.</li>
                                    <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li>
                                    <li>RM10.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                                    <li>Online ticket selling will close on 11 Nov (Sun) 12pm, subject to availability.</li>
                                </ol>
                                <h2>For enquiry only:</h2>
                                <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 350;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });

    // Modal Announcement
	//$(document).ready(function(){
    //    $("#announcementModal").modal('show');
  	//});	
    </script>

    {{-- Buy button disable --}}
    <script type="text/javascript">
        $(function() {
            $('a[id^=buyButton]').each(function() {
                var date = new Date();
                var enddate = $(this).attr('datetime'); 
                if ( Date.parse(date) >= Date.parse(enddate)) {
                  $(this).addClass('disabled');
                }
            });
        });
    </script>

@endsection

@section('modal')
    @include('layouts.partials.modals._seatplan')
    @include('layouts.partials.modals._getTix')

   <div id="announcementModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
                    <h1><i class="fa fa-bullhorn text-warning" aria-hidden="true"></i></h1>
                    <h4>Announcement</h4>
                    <div class="clearfix">&nbsp;</div>
                    <div class="well">
                        <p>To enjoy the “Package Of 4" promotion please select 4 seat and proceed to checkout. Kindly select the promotion "Package Of 4 Promotion" and key in "CREARTSASIA(4)" promo code before proceed to make payment.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection