@extends('master')
@section('title')
    Hard Rock Hotel Desaru Coast NYE's Dinner Party
@endsection

@section('header')
    @include('layouts.partials._header')
    <meta property="og:url" content="{{ Request::fullUrl() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Hard Rock Hotel Desaru Coast NYE Dinner Party" />
    <meta property="og:description" content="Lucky for you, Hard Rock Hotel Desaru Coast has just the party to tick this resolution off your list. On 31st December 2018, the brand-new hotel is hosting a New Year’s Eve dinner party for you to celebrate your way into 2019."/>
    <meta property="og:image" content="{{ Request::Url().'images/hardrockdesaru2018/thumbnail.jpg' }}" />
@endsection

@section('content')
    <!-- Banner Section -->
    <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/hardrockdesaru2018/web-banner.jpg')}}" style="width: 100%" class="img-responsive" alt="Hard Rock Hotel Desaru Coast NYE Dinner Party"></div>
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            <img src="{{asset('images/hardrockdesaru2018/thumbnail.jpg')}}" style="width: 100%" class="img-responsive" alt="Hard Rock Hotel Desaru Coast NYE Dinner Party">
        </div>
    </section>
    <!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
            <div class="row priceNbtn">
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="row">
                        <div class="col-sm-9 leftBox">
                            <h6>Hard Rock Hotel Desaru Coast NYE's Dinner Party</h6> Tickets from <span>RM588</span>
                        </div>
                        <div class="col-sm-3 text-center">
                            <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar section-white">
            <section class="pageCategory-section last">
                <div class="container intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i>  31st December 2018</div>
                            <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i>  Desaru Coast Conference Centre <a target="_blank" href="https://goo.gl/maps/F1DnMjzgwNC2">View Map</a></div>
                            <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> Monday (7.00pm - 1.00am)</div>
                            <div class="clearfix">&nbsp;</div>
                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <div class="addthis_inline_share_toolbox"></div>
                            <!-- /sharing -->
                            <div class="col-sm-12">
                                <hr>
                                <p><h2>HARD ROCK HOTEL DESARU COAST NYE’S DINNER PARTY</h2>
                                <ol>
                                    <li>Set Dinner : 5-course Western</li>
                                    <li>Venue : Desaru Coast Conference Centre</li>
                                    <li>Date  : 31st December 2018</li>
                                    <li>Time  : 7pm onwards</li>
                                </ol>
                                </p>
                                <p><h2>ENTERTAINMENT</h2>
                                <ol>
                                    <li>Songstress YUNA</li>
                                    <li>Local Band Hydra</li>
                                    <li>DJ Chinois</li>
                                </ol>
                                </p>
                                <hr>
                                <p><h2>New Year’s Resolution: Treat Yourself.</h2><br/>
                                Lucky for you, Hard Rock Hotel Desaru Coast has just the party to tick this resolution off your list. On 31st December 2018, the brand-new hotel is hosting a New Year’s Eve dinner party for you to celebrate your way into 2019.</p>
                                <p>Think tropical glitz and glamour. The gala occasion, which will be hosted at the Desaru Coast Conference Centre, will entail a 5-course western set dinner bound to tantalise your taste buds.</p>
                                <p>On the entertainment side, the Hard Rock team will be readying the dance floor for you with DJ Chinois and local rock band Hydra, with a promise to keep your feet moving all night. To usher in the new year with a bang, guests will be brought along a musical journey with none other than Malaysia’s sweetheart superstar, Yuna Zarai. With tunes to serenade and captivate, the Billboard artiste will be sending guests off into the new year with nothing but good music and good vibes.</p>
                                <p>The event starts at 7pm, but of course, the party won’t start ‘til you walk in. Tickets are RM588nett per person. Want a sweet promo? Book your tickets before Nov 15, 2018 for tickets at RM488nett per person!</p><br/>
                                <hr>
                                <p><h2>Event Run Down</h2><br/>
                                07.00pm : Arrival of Guest & Welcome Cocktails<br/>
                                08.00pm : Serving of Dinner<br/>
                                08.15pm : 1st Set Band Performance by Hydra<br/>
                                09.00pm : 1st DJ Session by DJ Chinois<br/>
                                09.30pm : 2nd Band Performance by Hydra<br/>
                                10.15pm	: 2nd DJ Session by DJ Chinois<br/>
                                10.45pm	: Special Performance by Yuna<br/>
                                11.30pm	: DJ session<br/>
                                11.40pm	: Countdown & Display of Fireworks<br/>
                                01.00am	: Ends</p>
                            </div>
                            <div class="col-sm-12 text-center">
                                <hr>
                                <p><b>Menu<br/><br/>
                                Smoked Salmon</b><br/>
                                Served with Lime Crème Fraiche | Flying Fish Caviar | Orange Segment | Marinated Garden Salad<br/>
                                <b>*****<br/><br/>
                                Cream of Pumpkin & Orange Soup</b><br/>
                                Chicken & Herb Dumplings<br/>
                                <b>*****<br/><br/>
                                Sorbet</b><br/>
                                Raspberry Sorbet | Mint Leaf<br/>
                                <b>*****<br/><br/>
                                Chicken Surf & Turf</b><br/>
                                Grilled Boneless Chicken Breast | Tiger Prawns | Potato Dauphine | Buttered Garden Vegetables | Wild Mushrooms Jus<br/>
                                <b>*****<br/><br/>
                                Belgian Chocolate Hazelnut Mousse Slice</b><br/>
                                Raspberry Coulis | Almond Brittle<br/>
                                <b>*****<br/><br/>
                                Coffee | Tea</b><br/>
                                <i>*Subject to changes without prior notice.</i></p>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                {{-- <div class="clearfix"></div> --}}
            </section>

            <section class="pageCategory-section last section-grey">
                <div class="container">
                    <div class="gallery text-center">
                        <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
                        <!-- Swiper -->
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <a href="images/hardrockdesaru2018/gallery-1.jpg" data-featherlight="image"><img class="" src="images/hardrockdesaru2018/gallery-1.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/hardrockdesaru2018/gallery-2.jpg" data-featherlight="image"><img class="" src="images/hardrockdesaru2018/gallery-2.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/hardrockdesaru2018/gallery-3.jpg" data-featherlight="image"><img class="" src="images/hardrockdesaru2018/gallery-3.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/hardrockdesaru2018/gallery-4.jpg" data-featherlight="image"><img class="" src="images/hardrockdesaru2018/gallery-4.jpg" alt=""></a>
                                </div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next swiper-button-white"></div>
                            <div class="swiper-button-prev swiper-button-white"></div>
                        </div><!-- /Swiper -->
                    </div>
                </div>
            </section>

            <section class="pageCategory-section last"><a id="anchorPrice"></a>
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                            <div class="text-center">
                                <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                                <p>Select ticket</p>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="table-responsive">
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Category</th>
                                            <th>Ticket Price</th>
                                            <th>Seating Layout</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {{-- <tr>
                                            <td>Early Bird</td>
                                            <td>RM488.00</td>
                                            <td rowspan="2"><img class="img-responsive seatPlanImg" src="images/hardrockdesaru2018/seat-plan.jpg" alt=""></td>
                                        </tr>
                                        <tr>
                                            <td>Normal Price</td>
                                            <td>RM588.00</td>
                                        </tr>--}}
                                        <tr>
                                            <td>Normal Price</td>
                                            <td>RM588.00</td>
                                            <td><img class="img-responsive seatPlanImg" src="images/hardrockdesaru2018/seat-plan.jpg" alt=""></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="buyAlert-bar">
                                <a class="btn btn-danger" id="buyButton" datetime="Dec 31 2018 00:00:00 GMT+0800" target="_blank" href="https://redtix-tickets.airasia.com/en-AU/shows/hard rock hotel desaru coast nye dinner party/events" role="button">BUY TICKETS</a>
                                {{-- <span class="or">/</span>
                                <span class="popData-btn" data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets <i class="fa fa-info-circle" aria-hidden="true"></i></span> --}}
                            </div>
                            <div class="note text-left">
                                <h2>Admission Age Limit</h2>
                                <ol>
                                    <li>Below 3 years - FOC (No Seating)</li>
                                    <li>Above 3 years - Full Normal Price (With Seating)</li>
                                </ol>
                            </div>
                            <div class="note text-left">
                                <h2>Important Notes</h2>
                                <ol>
                                    <li>Prices shown exclude RM4 ticket fee.</li>
                                    <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li>
                                    <li>RM10.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                                    <li>Online ticket selling will close 2 days prior to event day, subject to availability.</li>
                                </ol>
                                <h2>For enquiry only:</h2>
                                <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                            </div>

                        </div>
                    </div>
                </div>
            </section>
        </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 350;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });
    </script>

    {{-- Buy button disable --}}
    <script type="text/javascript">
        $(function() {
            $('a[id^=buyButton]').each(function() {
                var date = new Date();
                var enddate = $(this).attr('datetime'); 
                if ( Date.parse(date) >= Date.parse(enddate)) {
                  $(this).addClass('disabled');
                }
            });
        });
    </script>

@endsection

@section('modal')
    @include('layouts.partials.modals._seatplan')
    @include('layouts.partials.modals._getTix')
@endsection