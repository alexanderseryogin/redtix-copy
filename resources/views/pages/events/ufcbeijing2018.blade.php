@extends('master')
@section('title')
    UFC Fight Night Beijing
@endsection

@section('header')
    @include('layouts.partials._header')
    <meta property="og:url" content="{{ Request::fullUrl() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="UFC Fight Night Beijing" />
    <meta property="og:description" content="UFC FIGHT NIGHT® BEIJING, Presented by General Tire, will take place on Nov 24 at the Cadillac Arena."/>
    <meta property="og:image" content="{{ Request::Url().'images/ufcbeijing2018/thumbnail.jpg' }}" />
@endsection

@section('content')
    <!-- Banner Section -->
    <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/ufcbeijing2018/web-banner.jpg')}}" style="width: 100%" class="img-responsive" alt="UFC Fight Night Beijing"></div>
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            <img src="{{asset('images/ufcbeijing2018/thumbnail.jpg')}}" style="width: 100%" class="img-responsive" alt="UFC Fight Night Beijing">
        </div>
    </section>
    <!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
            <div class="row priceNbtn">
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="row">
                        <div class="col-sm-9 leftBox">
                            <h6>UFC Fight Night Beijing</h6> Tickets from <span>RM174</span>
                        </div>
                        <div class="col-sm-3 text-center">
                            <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar section-white">
            <section class="pageCategory-section last">
                <div class="container intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i>  24th November 2018</div>
                            <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i>  Cadillac Arena, Beijing, China <a target="_blank" href="https://goo.gl/maps/9dUf82i86wj">View Map</a></div>
                            <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> Saturday (4.00 pm - 10.00pm)</div>
                            <div class="clearfix">&nbsp;</div>
                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <div class="addthis_inline_share_toolbox"></div>
                            <!-- /sharing -->
                            <hr>
                            <p><h2>UFC FIGHT NIGHT® BEIJING, Presented by General Tire, will take place on Nov 24 at the Cadillac Arena.</h2><br />
                            Following last year’s highly successful  sell-out debut in mainland China, UFC, the world’s premier mixed martial arts organization,  is primed to deliver another thrilling night of action to fans in Beijing. Over the past few years, UFC has worked to identify and nurture the talents of top tier Chinese athletes who have shown great strength, skill and determination to succeed inside the Octagon®.  UFC fans around the world have witnessed the sharp rise of local athletes such as Li Jingliang, Song Yadong, Yan Xiaonan, Song Kenan, and Pingyuan Liu, who have put their respective divisions on notice.</p>
                            <p>About UFC<br />
                            UFC® is a premium global sports brand and the largest Pay-Per-View event provider in the world. Celebrating its 25th Anniversary in 2018, UFC boasts more than 284 million fans worldwide and has produced over 440 events in 22 countries since its inception in 1993. Acquired in 2016 by global sports, entertainment and fashion leader Endeavor (formerly WME | IMG), together with strategic partners Silver Lake Partners and KKR, UFC is headquartered in Las Vegas with a network of employees around the world.<br /> 
                            UFC produces more than 40 live events annually that consistently sell out some of the most prestigious arenas around the globe, while programming is broadcast in over 160 countries and territories to 1.1 billion TV households worldwide in 40 different languages. UFC FIGHT PASS®, a digital subscription service, delivers exclusive live events, thousands of fights on-demand and original content to fans around the world. For more information, visit UFC.com and follow UFC at Facebook.com/UFC, Twitter, Snapchat and Instagram: @UFC.</p>
                        </div>
                    </div>
                </div>
                {{-- <div class="clearfix"></div> --}}
            </section>

            {{-- <section class="pageCategory-section last section-grey">
                <div class="container">
                    <div class="gallery text-center">
                        <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
                        <!-- Swiper -->
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <a href="images/ufcbeijing2018/above-beyond.jpg" data-featherlight="image"><img class="" src="images/ufcbeijing2018/above-beyond.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/ufcbeijing2018/gallery-1.jpg" data-featherlight="image"><img class="" src="images/ufcbeijing2018/gallery-1.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/ufcbeijing2018/gallery-2.jpg" data-featherlight="image"><img class="" src="images/ufcbeijing2018/gallery-2.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/ufcbeijing2018/gallery-3.jpg" data-featherlight="image"><img class="" src="images/ufcbeijing2018/gallery-3.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/ufcbeijing2018/gallery-4.jpg" data-featherlight="image"><img class="" src="images/ufcbeijing2018/gallery-4.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/ufcbeijing2018/gallery-5.jpg" data-featherlight="image"><img class="" src="images/ufcbeijing2018/gallery-5.jpg" alt=""></a>
                                </div>

                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next swiper-button-white"></div>
                            <div class="swiper-button-prev swiper-button-white"></div>
                        </div><!-- /Swiper -->
                    </div>
                </div>
            </section> --}}

            <section class="pageCategory-section last"><a id="anchorPrice"></a>
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                            <div class="text-center">
                                <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                                <p>Select ticket</p>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="table-responsive">
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Category</th>
                                            <th>Ticket Price</th>
                                            <th>Seat Plan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td style="background-color: #410080; color: #fff;">P7</td>
                                            <td>¥280 / RM174</td>
                                            <td rowspan="7"><img class="img-responsive seatPlanImg" src="images/ufcbeijing2018/seat-plan.jpg" alt=""></td>
                                        </tr>
                                        <tr>
                                            <td style="background-color: #007F40; color: #fff;">P6</td>
                                            <td>¥380 / RM236</td>
                                        </tr>
                                        <tr>
                                            <td style="background-color: #1D92FC; color: #fff;">P5</td>
                                            <td>¥480 / RM298</td>
                                        </tr>
                                            <td style="background-color: #26999A; color: #fff;">P4</td>
                                            <td>¥680 / RM422</td>
                                        </tr>
                                        <tr>
                                            <td style="background-color: #435EAD; color: #fff;">P3</td>
                                            <td>¥880 / RM547</td>
                                        </tr>
                                        <tr>
                                            <td style="background-color: #70C9E5; color: #fff;">P2</td>
                                            <td>¥1,280 / RM795</td>
                                        </tr>
                                        <tr>
                                            <td style="background-color: #FC08EB; color: #fff;">P1</td>
                                            <td>¥1,480 / RM919</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="buyAlert-bar">
                                <a class="btn btn-danger" id="buyButton" datetime="Nov 19 2018 00:00:00 GMT+0800" target="_blank" href="https://redtix-tickets.airasia.com/en-AU/shows/ufc fight night beijing/events" role="button">BUY TICKETS</a>
                                {{-- <span class="or">/</span>
                                <span class="popData-btn" data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets <i class="fa fa-info-circle" aria-hidden="true"></i></span> --}}
                            </div>
                            
                            <div class="note text-left">
                                <h2>Important Notes</h2>
                                <ol>
                                    <li>Prices shown exclude RM4.00 AirAsiaRedTix fee and 3% Credit card fee.</li>
                                    <li>All ticket prices will be paid in Malaysian Ringgit. Chinese Renminbi prices are shown for reference purposes only.</li>
                                    <li>Prices quoted in RMB may differ due to currency fluctuations.</li>
                                    <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li>
                                    <li>RM10.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                                    <li>Online ticket selling will close 5 days prior to event day, subject to availability.</li>
                                </ol>
                                <h2>For enquiry only:</h2>
                                <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 350;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });
    </script>

    {{-- Buy button disable --}}
    <script type="text/javascript">
        $(function() {
            $('a[id^=buyButton]').each(function() {
                var date = new Date();
                var enddate = $(this).attr('datetime'); 
                if ( Date.parse(date) >= Date.parse(enddate)) {
                  $(this).addClass('disabled');
                }
            });
        });
    </script>

@endsection

@section('modal')
    @include('layouts.partials.modals._seatplan')
    @include('layouts.partials.modals._getTix')
@endsection