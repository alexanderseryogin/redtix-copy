@extends('master')
@section('title')
    Sean Paul Live in KL - Mad Love Tour
@endsection

@section('header')
    @include('layouts.partials._header')
    <meta property="og:url" content="{{ Request::fullUrl() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Sean Paul Live in KL - Mad Love Tour" />
    <meta property="og:description" content=""/>
    <meta property="og:image" content="{{ Request::Url().'images/seanpaul2018/thumbnail1.jpg' }}" />
@endsection

@section('content')
    <!-- Banner Section -->
    <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/seanpaul2018/web-banner1.jpg')}}" style="width: 100%" class="img-responsive" alt="Sean Paul Live in KL"></div>
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            <img src="{{asset('images/seanpaul2018/thumbnail1.jpg')}}" style="width: 100%" class="img-responsive" alt="Sean Paul Live in KL">
        </div>
    </section>
    <!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
            <div class="row priceNbtn">
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="row">
                        <div class="col-sm-9 leftBox">
                            <h6>Sean Paul Live in KL - Mad Love Tour</h6> Tickets from <span>RM228</span>
                        </div>
                        <div class="col-sm-3 text-center">
                            <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar section-white">
            <section class="pageCategory-section last">
                <div class="container intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i>  7th November 2018</div>
                            <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i>  KL Live at Life Centre <a target="_blank" href="https://goo.gl/maps/bFzWTTNJfTS2">View Map</a></div>
                            <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> Thursday (8.00 pm - 12.30am)</div>
                            <div class="clearfix">&nbsp;</div>
                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <div class="addthis_inline_share_toolbox"></div>
                            <!-- /sharing -->
                            <div class="col-sm-8">
                                <hr>
                                <p><h2>Sean Paul Live in KL - Mad Love Tour </h2><br/>
                                Sean Paul (born 9 Jan 1973) aka Sean Paul Ryan Francis Henrique is an R&B rapper and producer from Kingston, Jamaica who helped popularise Dancehall as it entered the mainstream market.</p>
                                <p>Eagerly asked to check out a new local talent reminiscent of dancehall pioneer Super Cat, producer Jeremy Harding went down to an open mic event in Kingston, Jamaica and unbeknownst to him he would discover the future global star, Sean Paul. Connecting with Paul and offering his advice, Harding began collaborating with him on tracks such as "Baby Girl", it was just the beginning of their strong and long standing relationship as Harding took him under his wing as his manager.</p>
                                <p>Soon a buzz erupted locally as people began to talk of the future superstar. In 2000 Paul released his debut album "Stage One" through VP Records. The album would be the start of his international career as his music was well received by neighbours in the U.S.</p>
                                <p>His monumental second release "Dutty Rock" released in 2002 through Atlantic Records would be the catalyst that turned a local star into a global superstar. The album featured the hit tracks "Gimme the Light" and "Get Busy" which navigated thorough freshly fused native Jamaican dancehall with contemporary R&B.</p>
                                <p>Keeping himself busy, in 2003 featured on several tracks with big artists such as Busta Rhymes, Blu Cantrell, Beenie Man and mostly famously appeared on superstar Beyonce’s number one single “Baby Boy” which further catapulted Paul into the spotlight. His electric and energetic performances provide the perfect party atmosphere enthusing audiences across the world to dance.</p>
                                <p>In 2005 Paul released his third album eloquently titled “Trinity” which featured “We Be Burnin’”, “Give It Up To Me” (featured in the film Step Up 2) and the smash hit “Temperature” released in 2006 which became a club classic. Later that year Paul was awarded with an American Music Award for “(When You Gonna) When It Up To Me”.</p>
                                <p>Sean Paul has proved himself to be the master of crossover as throughout his career he has continued to collaborate with artists from a range of different genres including: Joss Stone, Rihanna, Enrique Iglesias, The Saturdays, Major Lazer and Simple Plan.</p>
                            </div>
                            <div class="col-sm-4">
                                <iframe src="https://open.spotify.com/embed/user/spotify/playlist/37i9dQZF1DZ06evO2abUti" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="pageCategory-section last section-grey">
                <div class="container">
                    <div class="gallery text-center">
                        <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
                        <!-- Swiper -->
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <a href="images/seanpaul2018/gallery-1x.jpg" data-featherlight="image"><img class="" src="images/seanpaul2018/gallery-1x.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/seanpaul2018/web-banner1.jpg" data-featherlight="image"><img class="" src="images/seanpaul2018/web-banner1.jpg" alt=""></a>
                                </div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next swiper-button-white"></div>
                            <div class="swiper-button-prev swiper-button-white"></div>
                        </div><!-- /Swiper -->
                    </div>
                </div>
            </section>

            <section class="pageCategory-section last"><a id="anchorPrice"></a>
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                            <div class="text-center">
                                <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                                <p>Select ticket</p>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="table-responsive">
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Category</th>
                                            <th>Ticket Price</th>
                                            <th>Seat Plan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td style="background-color: #84C99B; color: #fff;">Early Bird Silver<br/>(Limited to 100 pax)</td>
                                            <td><strike>RM268</strike><br/>RM228</td>
                                            <td rowspan="11"><img class="img-responsive seatPlanImg" src="images/seanpaul2018/seat-plan1.jpg" alt=""></td>
                                        </tr>
                                        <tr>
                                            <td style="background-color: #F26623; color: #fff;">Early Bird Gold<br/>(Limited to 100 pax)</td>
                                            <td><strike>RM468</strike><br/>RM428</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="buyAlert-bar">
                                <a class="btn btn-danger disabled" id="buyButton" datetime="Nov 6 2018 00:00:00 GMT+0800" target="_blank" href="https://redtix-tickets.airasia.com/en-AU/shows/sean paul live in kl (mad love tour)/events" role="button">BUY TICKETS</a>
                                {{-- <span class="or">/</span>
                                <span class="popData-btn" data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets <i class="fa fa-info-circle" aria-hidden="true"></i></span> --}}
                            </div>
                            <div class="note text-left">
                                <h2>Important Notes</h2>
                                <ol>
                                    <li>Prices shown exclude ticket fee.</li>
                                    {{-- <li>All ticket prices will be paid in Malaysian Ringgit. Indian Rupee prices are shown for reference purposes only.</li>
                                    <li>Prices quoted in INR may differ due to currency fluctuations.</li> --}}
                                    <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li>
                                    <li>RM10.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                                    <li>Online ticket selling will close 2 days prior to event day, subject to availability.</li>
                                    <li>Adult: Strictly 18 years old and above.</li>
                                </ol>
                                <h2>For enquiry only:</h2>
                                <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                            </div>

                        </div>
                    </div>
                </div>
            </section>
        </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 350;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });
    </script>

    {{-- Buy button disable --}}
    <script type="text/javascript">
        $(function() {
            $('a[id^=buyButton]').each(function() {
                var date = new Date();
                var enddate = $(this).attr('datetime'); 
                if ( Date.parse(date) >= Date.parse(enddate)) {
                  $(this).addClass('disabled');
                }
            });
        });
    </script>

@endsection

@section('modal')
    @include('layouts.partials.modals._seatplan')
    @include('layouts.partials.modals._getTix')
@endsection