@extends('master')
@section('title')
    Teater Perdana DBP - Pokok
@endsection

@section('header')
    @include('layouts.partials._header')
    <meta property="og:url" content="{{ Request::fullUrl() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Teater Perdana DBP - Pokok" />
    <meta property="og:description" content="Teater Perdana DBP - Pokok. POKOK karya JOHAN JAAFFAR - Tersiar dalam Dewan Sastera, September 2017"/>
    <meta property="og:image" content="{{ Request::Url().'images/pokok2018/thumbnail1.jpg' }}" />
@endsection

@section('content')
    <!-- Banner Section -->
    <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/pokok2018/web-banner1.jpg')}}" style="width: 100%" class="img-responsive" alt="Teater Perdana DBP - Pokok"></div>
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            <img src="{{asset('images/pokok2018/thumbnail1.jpg')}}" style="width: 100%" class="img-responsive" alt="Teater Perdana DBP - Pokok">
        </div>
    </section>
    <!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
            <div class="row priceNbtn">
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="row">
                        <div class="col-sm-9 leftBox">
                            <h6>Teater Perdana DBP - Pokok</h6> Tickets from <span>RM20</span>
                        </div>
                        <div class="col-sm-3 text-center">
                            <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar section-white">
            <section class="pageCategory-section last">
                <div class="container intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i>  7th - 16th December 2018</div>
                            <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i>  Auditorium Dewan Bahasa dan Pustaka, Kuala Lumpur <a target="_blank" href="https://goo.gl/maps/VBbuAczQbJD2">View Map</a></div>
                            <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> Daily:  8.30pm</div>
                            <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> Matinee:  3.00pm  - 8th, 9th, 15th &amp; 16th Dec 2018</div>
                            <div class="clearfix">&nbsp;</div>
                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <div class="addthis_inline_share_toolbox"></div>
                            <!-- /sharing -->                            
                            <hr>
                            <div class="col-sm-12">
                                <p><h2>POKOK karya JOHAN JAAFFAR</h2><br/>(Tersiar dalam Dewan Sastera, September 2017)</p>
                                <p>SINOPSIS:<br/>
                                DARWIS tinggal bersama isterinya, MARDIAH yang sedang sarat mengandung di sebuah rumah kecil di pinggir hutan. Rumah itu dipayungi oleh pokok raksaksa yang bukan sahaja melindungi tetapi juga mengancam keselamatan mereka. Ada kalanya dahan pokok besar itu akan patah. Pokok itu pasti akan memangsai mereka berdua sekiranya ia tumbang. Pokok besar itu menjadi metafora kehidupan mereka berdua. Pokok itu sekaligus melindungi dan mengugut, seperti juga kehidupan di sekeliling mereka. Manusia seperti TUK KAYA, tuan tanah di mana mereka tinggal, juga menjadi pelindung tetapi juga punca kegelisahan Mardiah.</p>
                                <p>Darwis yang kudung sebelah kakinya bekerja untuk Tuk Kaya, orang yua biang keladi. Darwis pernah membuat pelbagai kerja termasuk di Singapura dan dia terlibat dalam satu kemalangan yang menyebabkan kakinya tercedera.</p>
                                <p>Mardiah seorang wanita cantik tetapi hidup malang. Dia bekas penagih dadah malah banyak kali dipenjarakan oleh sebab pelbagai kesalahan termasuk merompak. Terlalu lama dia bergelumang dalam dosa dan jenayah. Entah mengapa pertemuannya dengan Darwis mengubah hidupnya. Dia mengahwini Darwis dengan kerelaan hatinya.</p>
                                <p>Dia mengakui bahawa sekian lama hidupnya tanpa arah dan tujuan. Hanya dengan Darwis dia menemui dirinya yang baru dan mengenal makna kasih dan sayang.</p>
                                <p>Darwis terhutang budi dengan Tuk Kaya yang memberikan ruang untuknya bekerja dan tinggal di tanahnya. Tetapi Tuk Kaya sentiasa melihat kecantikan Mardiah dan cuba menggodanya. Mardiah tahu apa yang difikirkan oleh Tuk Kaya. Tetapi Mardinah tahu juga dia tidak mahu lagi kembali ke jalan yang pernah dilaluinya. Kecantikan dirinya menjadi bebanan pada dirinya. Penjara mengajarnya tentang kehidupan yang berbeza. Di bawah pokok besar itu dia menemui kehidupan yang berbeza.</p>
                                <p>Kepada AMUD, kawan Darwis dia mengakui:<br/>
                                “…Dulu saya hanya rasa selamat di penjara. Di luar saya rasa resah, gelisah, tak tentu arah,. Saya rasa tak selamat. Saya rasa saya kekurangan. Tak diperlukan. Dipandang rendah. Dihina. Tak ada harga diri. Di penjara, saya jumpa diri saya. Saya rasa tenang. Selamat.”
                                <p>Hanya apabila mengahwini Darwis, dia menemui laluan kehidupan yang berbeza. Kepada Tuk Kaya dia berkata:<br/>
                                “Saya menggunakan kecantikan saya untuk menggoda, menyusahkan orang lain, melakukan maksiat dan akhirnya saya masuk ke penjara. Di penjara saya menggunakan kecantikan saya untuk mencapai matlamat saya. Saya tidak ragu-ragu menggunakannya. Saya memilih untuk jadi hodoh di sini….Sebab saya mahu jadi baik…”</p>
                                <p>Amud menyedari dilema Mardiah dan lebih faham lagi penanggungan Darwis. Mengenai Darwis dia memberitahu Mardiah:<br/>
                                “Orang macam Darwis selalu fikir pasal kekurangannya, kecacatannya. ..Orang yang serba kekurangan akan berasa orang lain semuanya ada kelebihan.</p>
                                <p>Tuk Kaya cuba mengambil kesempatan daripada pertolongannya tetapi dia sedar bahawa Mardiah adalah wanita yang cuba menjadi manusia baik. Darwis mengubahnya. Mencorakkan hidup barunya. Darwis sedar bahawa tempat tinggal di bawah pokok besar itu bukan tempat yang sesuai untuk berkeluarga apa lagi untuk membesarkan anak yang bakal lahir. Tetapi dia juga sedar bahawa sekali ini dia perlu berkorban.</p>
                                <p>Drama ini adalah cerminan kehidupan manusia dengan segala keanekaragaman dan keunikannya. Apakah manusia kecil seperti Darwis akan bertahan dengan penderitaan? Apakah Mardiah akan mempertahankan “kebaikannya”? Apakah Tuk Kaya mengerti bahawa wang dan kekayaan tidak boleh membeli segala-galanya?</p>
                            </div>
                            <div class="col-sm-12 embed-responsive embed-responsive-16by9">
                                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/Tk4dEW1dYiE?ecver=2" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- <div class="clearfix"></div> --}}
            </section>

            <section class="pageCategory-section last section-grey">
                <div class="container">
                    <div class="gallery text-center">
                        <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
                        <!-- Swiper -->
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <a href="images/pokok2018/gallery-1.jpg" data-featherlight="image"><img class="" src="images/pokok2018/gallery-1.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/pokok2018/gallery-2.jpg" data-featherlight="image"><img class="" src="images/pokok2018/gallery-2.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/pokok2018/gallery-3.jpg" data-featherlight="image"><img class="" src="images/pokok2018/gallery-3.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/pokok2018/gallery-4.jpg" data-featherlight="image"><img class="" src="images/pokok2018/gallery-4.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/pokok2018/gallery-5.jpg" data-featherlight="image"><img class="" src="images/pokok2018/gallery-5.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/pokok2018/gallery-6.jpg" data-featherlight="image"><img class="" src="images/pokok2018/gallery-6.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/pokok2018/gallery-7.jpg" data-featherlight="image"><img class="" src="images/pokok2018/gallery-7.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/pokok2018/gallery-8.jpg" data-featherlight="image"><img class="" src="images/pokok2018/gallery-8.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/pokok2018/gallery-9.jpg" data-featherlight="image"><img class="" src="images/pokok2018/gallery-9.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/pokok2018/gallery-10.jpg" data-featherlight="image"><img class="" src="images/pokok2018/gallery-10.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/pokok2018/gallery-11.jpg" data-featherlight="image"><img class="" src="images/pokok2018/gallery-11.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/pokok2018/gallery-12.jpg" data-featherlight="image"><img class="" src="images/pokok2018/gallery-12.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/pokok2018/gallery-13.jpg" data-featherlight="image"><img class="" src="images/pokok2018/gallery-13.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/pokok2018/gallery-14.jpg" data-featherlight="image"><img class="" src="images/pokok2018/gallery-14.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/pokok2018/gallery-15.jpg" data-featherlight="image"><img class="" src="images/pokok2018/gallery-15.jpg" alt=""></a>
                                </div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next swiper-button-white"></div>
                            <div class="swiper-button-prev swiper-button-white"></div>
                        </div><!-- /Swiper -->
                    </div>
                </div>
            </section>

            <section class="pageCategory-section last"><a id="anchorPrice"></a>
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                            <div class="text-center">
                                <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                                <p>Select ticket</p>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="table-responsive">
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Category</th>
                                            <th>Normal</th>
                                            <th>Senior Citizen / Child<br/>OKU (Discount)</th>
                                            <th>Seat Plan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Stall</td>
                                            <td>RM50.00</td>
                                            <td>N/A</td>
                                            <td rowspan="6"><img class="img-responsive seatPlanImg" src="images/pokok2018/seat-plan.jpg" alt=""></td>
                                        </tr>
                                        <tr>
                                            <td>Upper Stall</td>
                                            <td>RM30.00</td>
                                            <td>RM20.00</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="buyAlert-bar">
                                <a class="btn btn-danger" id="buyButton" datetime="Dec 17 2018 00:00:00 GMT+0800" target="_blank" href="https://redtix-tickets.airasia.com/en-AU/shows/teater perdana dbp 'pokok'/events" role="button">BUY TICKETS</a>
                                {{-- <span class="or">/</span>
                                <span class="popData-btn" data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets <i class="fa fa-info-circle" aria-hidden="true"></i></span> --}}
                            </div>
                            <div class="note text-left">
                                <h2>Important Notes</h2>
                                <ol>
                                    <li>Prices shown exclude RM4 ticket fee.</li>
                                    <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li>
                                    <li>RM10.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                                    <li>Online ticket selling will close 2 days prior to event day, subject to availability.</li>
                                    <li>Admission Age Limit: 6 Years & Above</li>
                                </ol>
                                <h2>For enquiry only:</h2>
                                <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 350;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });

    // Modal Announcement
	$(document).ready(function(){
        $("#announcementModal").modal('show');
  	});	
    </script>

    {{-- Buy button disable --}}
    <script type="text/javascript">
        $(function() {
            $('a[id^=buyButton]').each(function() {
                var date = new Date();
                var enddate = $(this).attr('datetime'); 
                if ( Date.parse(date) >= Date.parse(enddate)) {
                  $(this).addClass('disabled');
                }
            });
        });
    </script>

@endsection

@section('modal')
    @include('layouts.partials.modals._seatplan')
    @include('layouts.partials.modals._getTix')

    <div id="announcementModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
                    <h1><i class="fa fa-bullhorn text-warning" aria-hidden="true"></i></h1>
                    <h4>Announcement</h4>
                    <div class="clearfix">&nbsp;</div>
                    <div class="well">
                        <p>Senior Citizens / Student / OKU discounts can be purchased online. You must present the relevant documents during the redemption of the ticket. Failure to do so will result in your ticket being canceled and will have to pay a full price for entry on the day of the show. Please read the NOTES before purchase.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection