@extends('master')
@section('title')
    The Roar of Singapore 5 - The Kings of Lion City
@endsection

@section('header')
    @include('layouts.partials._header')
    <meta property="og:url" content="{{ Request::fullUrl() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="The Roar of Singapore 5 - The Kings of Lion City" />
    <meta property="og:description" content="Ringstar Management and Marina Bay Sands present the ultimate spectacle of combat sports: Roar of Singapore - The Kings of Lion City."/>
    <meta property="og:image" content="{{ Request::Url().'images/roarsingapore2018/thumbnail.jpg' }}" />
@endsection

@section('content')
    <!-- Banner Section -->
    <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/roarsingapore2018/web-banner.jpg')}}" style="width: 100%" class="img-responsive" alt="The Roar of Singapore 5 - The Kings of Lion City"></div>
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            <img src="{{asset('images/roarsingapore2018/thumbnail.jpg')}}" style="width: 100%" class="img-responsive" alt="The Roar of Singapore 5 - The Kings of Lion City">
        </div>
    </section>
    <!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
            <div class="row priceNbtn">
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="row">
                        <div class="col-sm-9 leftBox">
                            <h6>The Roar of Singapore 5 - The Kings of Lion City</h6> Tickets <span>FREE</span>
                        </div>
                        <div class="col-sm-3 text-center">
                            <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar section-white">
            <section class="pageCategory-section last">
                <div class="container intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i>  29th September 2018</div>
                            <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i>  Marina Bay Sands Expo & Convention - Hall E & F <a target="_blank" href="https://goo.gl/maps/ySahEwDd7Qx">View Map</a></div>
                            <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> Saturday (5.00 pm - 12.00am)</div>
                            <div class="clearfix">&nbsp;</div>
                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <div class="addthis_inline_share_toolbox"></div>
                            <!-- /sharing -->
                            <hr>
                            <p><h2>The Roar of Singapore 5 - The Kings of Lion City </h2><br/>
                            Ringstar Management and Marina Bay Sands present the ultimate spectacle of combat sports: Roar of Singapore - The Kings of Lion City. In the main event, Singapore’s undefeated herald of the sweet science meets his most decorated opponent to date - the two time world champion Paulus Ambunda, from Namibia. This clash of titans will crown a new IBO Super Bantamweight World Champion.</p>
                            <p>Even the precursors to this epic showdown promise some fistic fury! ‘The Mean Machine’ Keng Fai (Malaysia) will defend his IBO Oceania Welterweight Title against France’s own Abdelelah Karroum, while his fellow Malaysian fighter ‘Miracle’ Mirage Khan takes on New Zealand’s Jonathan Taylor for the right to the WBC Asia Continental Light Heavyweight belt.</p>
                            <p>Ranked IBO #1, WBC #5, and IBF #6, the Philippines’ hero Michael ‘Gloves On Fire’ Dasmarinas looks to capitalise on his recent stunning KO victory as he steps into the squared circle with Jesse ‘The Black Flash’ Many Plange.</p>
                            <p>Don’t miss this epic, action-packed evening as we bring to you the best of boxing in Asian and beyond! Broadcast live on Fox Sports and Fight Sports, the commentary team of Mike Angove, Mike Ochoa, and Andrew Whitelaw will accompany you as we determine just who will earn the right to be called the Kings of Lion City.</p>
                        </div>
                    </div>
                </div>
                {{-- <div class="clearfix"></div> --}}
            </section>

            {{-- <section class="pageCategory-section last section-grey">
                <div class="container">
                    <div class="gallery text-center">
                        <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
                        <!-- Swiper -->
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <a href="images/roarsingapore2018/gallery-1.jpg" data-featherlight="image"><img class="" src="images/roarsingapore2018/gallery-1.jpg" alt=""></a>
                                </div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next swiper-button-white"></div>
                            <div class="swiper-button-prev swiper-button-white"></div>
                        </div><!-- /Swiper -->
                    </div>
                </div>
            </section> --}}

            <section class="pageCategory-section last"><a id="anchorPrice"></a>
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                            <div class="text-center">
                                <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                                <p>Select ticket</p>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="table-responsive">
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Category</th>
                                            <th>Ticket Price</th>
                                            <th>Remark</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>GA Category B</td>
                                            <td>FREE</td>
                                            <td>We are giving away 20 free tickets, limited to 2 tickets per transaction</td>
                                        </tr>
                                        <tr>
                                            <td>GA Category A</td>
                                            <td>RM310</td>
                                            <td>Buy One Free One</td>
                                        </tr>
                                        <tr>
                                            <td>GA Category B</td>
                                            <td>RM250</td>
                                            <td>Buy One Free One</td>
                                        </tr>
                                        <tr>
                                            <td>Ringside Seats</td>
                                            <td>RM636</td>
                                            <td>Buy One Free One</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="buyAlert-bar">
                                <a class="btn btn-danger" id="buyButton" datetime="Sep 29 2018 00:00:00 GMT+0800" target="_blank" href="https://redtix-tickets.airasia.com/en-AU/shows/the roar of singapore 5 - the kings of lion city/events" role="button">BUY TICKETS</a>
                                {{-- <span class="or">/</span>
                                <span class="popData-btn" data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets <i class="fa fa-info-circle" aria-hidden="true"></i></span> --}}
                            </div>
                            
                            <div class="note text-left">
                                <h2>Important Notes</h2>
                                <ol>
                                    <li>Prices shown exclude RM4.00 AirAsiaRedTix fee and 3% Credit card fee.</li>
                                    {{-- <li>All ticket prices will be paid in Malaysian Ringgit. Singapore Dollar prices are shown for reference purposes only.</li>
                                    <li>Prices quoted in SGD may differ due to currency fluctuations.</li> --}}
                                    <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li>
                                    <li>RM10.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                                    <li>Online ticket selling will close 2 days prior to event day, subject to availability.</li>
                                </ol>
                                <h2>For enquiry only:</h2>
                                <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                            </div>

                        </div>
                    </div>
                </div>
            </section>
        </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 350;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });
    </script>

    {{-- Buy button disable --}}
    <script type="text/javascript">
        $(function() {
            $('a[id^=buyButton]').each(function() {
                var date = new Date();
                var enddate = $(this).attr('datetime'); 
                if ( Date.parse(date) >= Date.parse(enddate)) {
                  $(this).addClass('disabled');
                }
            });
        });
    </script>

@endsection

@section('modal')
    @include('layouts.partials.modals._seatplan')
    @include('layouts.partials.modals._getTix')
@endsection