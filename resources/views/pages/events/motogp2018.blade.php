@extends('master')
@section('title')
    Shell Malaysia Motorcycle Grand Prix 2018
@endsection

@section('header')
    @include('layouts.partials._header')
    <meta property="og:url" content="{{ Request::fullUrl() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Shell Malaysia Motorcycle Grand Prix 2018" />
    <meta property="og:description" content="Shell Malaysia Motorcycle Grand Prix 2018" />
    <meta property="og:image" content="{{ Request::Url().'images/motogp2018/thumbnail.jpg' }}" />
@endsection

@section('content')
    <!-- Banner Section -->
    <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/motogp2018/web-banner.jpg')}}" style="width: 100%" class="img-responsive" alt="Shell Malaysia Motorcycle Grand Prix 2018"></div>
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            <img src="{{asset('images/motogp2018/thumbnail-test.jpg')}}" style="width: 100%" class="img-responsive" alt="Shell Malaysia Motorcycle Grand Prix 2018">
        </div>
    </section>
    <!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
            <div class="row priceNbtn">
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="row">
                        <div class="col-sm-9 leftBox">
                            <h6>Shell Malaysia Motorcycle Grand Prix 2018</h6>Tickets from <span>RM52.84</span>
                        </div>
                        <div class="col-sm-3 text-center">
                            <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar section-white">
            <section class="pageCategory-section last">
                <div class="container intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i> 2nd - 4th November 2018</div>
                            <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i> Sepang International Circuit <a target="_blank" href="https://goo.gl/maps/esHcuCcubmA2"> View Map</a></div>
                            {{-- <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> 7.30 p.m </div> --}}
                            <div class="clearfix">&nbsp;</div>
                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <div class="addthis_inline_share_toolbox"></div>
                            <!-- /sharing -->
                            <hr>
                            <p>The Shell Malaysia Motorcycle Grand Prix 2018 returns on 2 to 4 November 2018, again as the penultimate race in the MotoGP calendar. With the event selling out in recent years and crowd numbers breaking records year after year, the Malaysia MotoGP looks set to be another hit this 2018.</p>
                            <p>Race weekend will be bigger and better with more exciting action and activities to attract fans this year. Nevertheless, the key attraction remains the riders. Fans will once again be able to cheer on our Malaysian riders. Our first ever Malaysian MotoGP rider, Hafizh Syahrin Abdullah will be racing with Yamaha Tech 3, while Zulfahmi Khairuddin is back with SIC Racing Team in Moto2 and Khairul Idham Pawi in his second season with Idemitsu Honda Team Asia. In Moto3, Adam Norrodin and his teammate Ayumu Sasaki will be racing with Petronas Sprinta Racing fighting for the championship.</p>
                            <p>The Malaysia MotoGP will also see action from some of Asia’s top young riders in the Idemitsu Asia Talent Cup. This year, Malaysia is represented by 2 riders – Ibrahim Pawi and Danial Shahril.</p>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </section>

            <section class="pageCategory-section last section-grey">
                <div class="container">
                    <div class="gallery text-center">
                        <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
                        <!-- Swiper -->
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <a href="images/motogp2018/gallery.jpg" data-featherlight="image"><img class="" src="images/motogp2018/gallery.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/motogp2018/gallery1.jpg" data-featherlight="image"><img class="" src="images/motogp2018/gallery1.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/motogp2018/gallery2.jpg" data-featherlight="image"><img class="" src="images/motogp2018/gallery2.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/motogp2018/gallery3.jpg" data-featherlight="image"><img class="" src="images/motogp2018/gallery3.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/motogp2018/gallery4.jpg" data-featherlight="image"><img class="" src="images/motogp2018/gallery4.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/motogp2018/gallery5.jpg" data-featherlight="image"><img class="" src="images/motogp2018/gallery5.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/motogp2018/gallery6.jpg" data-featherlight="image"><img class="" src="images/motogp2018/gallery6.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/motogp2018/gallery7.jpg" data-featherlight="image"><img class="" src="images/motogp2018/gallery7.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/motogp2018/gallery8.jpg" data-featherlight="image"><img class="" src="images/motogp2018/gallery8.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/motogp2018/gallery9.jpg" data-featherlight="image"><img class="" src="images/motogp2018/gallery9.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/motogp2018/gallery10.jpg" data-featherlight="image"><img class="" src="images/motogp2018/gallery10.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/motogp2018/gallery11.jpg" data-featherlight="image"><img class="" src="images/motogp2018/gallery11.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/motogp2018/gallery12.jpg" data-featherlight="image"><img class="" src="images/motogp2018/gallery12.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/motogp2018/gallery13.jpg" data-featherlight="image"><img class="" src="images/motogp2018/gallery13.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/motogp2018/gallery14.jpg" data-featherlight="image"><img class="" src="images/motogp2018/gallery14.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/motogp2018/gallery15.jpg" data-featherlight="image"><img class="" src="images/motogp2018/gallery15.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/motogp2018/gallery16.jpg" data-featherlight="image"><img class="" src="images/motogp2018/gallery16.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/motogp2018/gallery17.jpg" data-featherlight="image"><img class="" src="images/motogp2018/gallery17.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/motogp2018/gallery18.jpg" data-featherlight="image"><img class="" src="images/motogp2018/gallery18.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/motogp2018/gallery19.jpg" data-featherlight="image"><img class="" src="images/motogp2018/gallery19.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/motogp2018/gallery20.jpg" data-featherlight="image"><img class="" src="images/motogp2018/gallery20.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/motogp2018/poster.jpg" data-featherlight="image"><img class="" src="images/motogp2018/poster.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/motogp2018/web-banner.jpg" data-featherlight="image"><img class="" src="images/motogp2018/web-banner.jpg" alt=""></a>
                                </div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next swiper-button-white"></div>
                            <div class="swiper-button-prev swiper-button-white"></div>
                        </div><!-- /Swiper -->
                    </div>
                </div>
            </section>

            <section class="pageCategory-section last"><a id="anchorPrice"></a>
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                            <div class="text-center">
                                <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                                <p>Select ticket</p>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="table-responsive">
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Category</th>
                                            <th>Normal</th>
                                            {{-- <th>Remarks</th> --}}
                                            <th>General Map</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td style="background-color: #e9d900;">Premier Roving</td>
                                            <td>RM334.00</td>
                                            <td rowspan="6"><img class="img-responsive seatPlanImg" src="images/motogp2018/general-map1.png"></td>
                                        </tr>
                                        <tr>
                                            <td style="background-color: #318c53; color: #fff;">Main Grandstand</td>
                                            <td>RM224.00</td>
                                        </tr>
                                        <tr>
                                            <td style="background-color: #ffd542;">VR46 Tribune</td>
                                            <td>RM180.00</td>
                                        </tr>
                                        <tr>
                                            <td style="background-color: #03a7a0; color: #fff;">SIC Racing Team (K2 Hillstand)</td>
                                            <td>RM81.00</td>
                                        </tr>
                                        <tr>
                                            <td style="background-color: #ef691e; color: #fff;">C1 Hafizh Syahrin Tribune</td>
                                            <td>RM81.00</td>
                                        </tr>
                                        <tr>
                                            <td style="background-color: #135ca1; color: #fff;">C2 Hillstand (Covered)</td>
                                            <td>RM48.00</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="buyAlert-bar">
                                <a class="btn btn-danger disabled" id="buyButton" datetime="Oct 4 2018 00:00:00 GMT+0800" target="_blank" href="http://redtix-tickets.airasia.com/en-AU/shows/2018%20shell%20malaysia%20motorcycle%20grand%20prix%20(2-4%20november%202018)/events" role="button">BUY TICKETS</a>
                                <span class="or">/</span>
                                <span class="popData-btn" data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets <i class="fa fa-info-circle" aria-hidden="true"></i></span> 
                            </div>
                            {{-- <span class="importantNote">* Additional charges will be applied for credit/debit card transaction, optionally customer may pay cash for outlet purchase to avoid the charges.</span> --}}
                            <div class="note text-left">
                                <h2>Important Notes</h2>
                                <ol>
                                    {{-- <li>Prices shown include RM4.00 AirAsiaRedTix fee & other taxes.</li> --}}
                                    <li>Prices shown include RM4.00 AirAsiaRedTix fee.</li>
                                    <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li>
                                    <li>RM10.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                                    <li>Online ticket selling will close 1 days prior to event day, subject to availability.</li>
                                    <li>Ticket is required for any child aged 2 &amp; above</li>
                                </ol>
                                <h2>For enquiry only:</h2>
                                <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 350;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });
    </script>

    {{-- Buy button disable --}}
    <script type="text/javascript">
        $(function() {
            $('a[id^=buyButton]').each(function() {
                var date = new Date();
                var enddate = $(this).attr('datetime'); 
                if ( Date.parse(date) >= Date.parse(enddate)) {
                  $(this).addClass('disabled');
                }
            });
        });
    </script>

@endsection

@section('modal')
    @include('layouts.partials.modals._seatplan')
    @include('layouts.partials.modals._getTix')
@endsection