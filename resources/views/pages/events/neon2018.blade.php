@extends('master')
@section('title')
	 Neon Countdown 2018
@endsection

@section('header')
	@include('layouts.partials._header')
	
@endsection

@section('content')
<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
<link type="text/css" rel="stylesheet" href="css/clock.css">
<link type="text/css" rel="stylesheet" href="css/timeTo.css">
<link type="text/css" rel="stylesheet" href="css/custom2.css">
<link type="text/css" rel="stylesheet" href="css/ufc221.css">
<style type="text/css">
	.background-full {
		background-image: url('/images/neon2018/background.jpg');
	    background-size: 100% auto;
		background-repeat: no-repeat;
		height: 950px;
	}
    
    .sect-count p img{
        padding-top: 100px;
		width: 40%;
    }
	.open-sans {
		font-family: 'Open Sans', sans-serif;
        font-weight: 100;
	}
	.h2-transform {
        font-size: 18px;
        text-transform: uppercase;
	}
	.btn-danger {
		background: linear-gradient(#b82941, #40151c);
	}
	.btn-wrapper {
		margin-top: 20px;
	}
	
	@media screen and (max-width: 760px){ 
		.background-full {
			background-image: url('/images/neon2018/mobile-background.jpg');	
		}
		.btn-wrapper {
			margin-top: 20px;
		}

	}
	@media screen and (min-width: 768px){ 
		.background-full { 
			height: 950px;
		}
	}
	@media screen and (min-width: 1024px){ 
		.background-full { 
   			height: 950px;
		}
	}
	@media screen and (max-width: 1024px){ 
		.background-full { 
			height: 800x;
		}
	}
	@media screen and (min-width: 768px) and (max-width: 1024px){ 
		.background-full {
			height: 950px;
		}
	}
	@media screen and (min-width: 1024px) and (max-width: 1366px){ 
		.background-full{
			height: 950px;
		}
	}
	@media screen and (min-width: 1400px){ 
		.background-full{
			height: 950px;
		}
	}
	@media screen and (max-width: 414px){ 
		.background-full { 
			height: 600px;
		}
        .sect-count p img{
        	padding-top: 10px;
			width: 60%;
    	}
	}
    
	.embed-responsive-item {
		padding-top: 20px;
	}
</style>

<section class="pageContent">
      <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar" style="padding-top: 0px;">
            <section class="pageCategory-section last section-black background-full">
                <div class="container intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 sect-count text-center">
                        <p><img src="{{asset('images/neon2018/logo.png')}}" alt="Neon Countdown 2018"></p>
						
                        {{-- <h2 class="open-sans h2-transform" style="color: #fff; padding-top:40px;">Register now to get access to early bird pricing</h2> --}}
                        </div>
                    </div>
					<div class="row">
						<div class="col-sm-offset-1 col-sm-10 sect-count text-center">
							<div class="text-center" id="countdown" style="padding-top: 40px; padding-bottom: 100px;">
							</div>
						</div>
						<div class="col-sm-offset-1 col-sm-10 sect-count text-center">
	                        <p style="color: #fff; padding-top:60px; text-transform:uppercase; font-size:18px;">to NEON Countdown 2018 ticket sales!</p>
							<p>(Ticket sales begins on 22 Nov at 2.00pm)</p>
							
                        </div>

                    {{-- <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 btn-wrapper text-center"> 
						    <a class="btn btn-danger" href="https://docs.google.com/forms/d/e/1FAIpQLSf13UV7-VejvodSVnAqpjVHyXLa20rURwwOwRfM9JSKVKJ8rg/viewform" role="button">Register for NEON Kuala Lumpur</a>
                        </div>
                    </div> --}}
					</div>
            </section>

            <section class="pageCategory-section last section-black">
                <div class="container intro">
                    <div class="row">
                        <div class="clear-fix">
                        
                       	</div>
						<div class="col-sm-offset-1 col-sm-10 sect-count text-center">
							<p>Available for Private Early Bird Tickets for both GA & VIP (NEONSkydeck)<br/>
							(check your email for unique promo code if you've registered successfully).<br/><br/>
							Available for Normal Glow Tickets.<br/>
							T&C apply.</p>
						</div>
                    </div>
                    <div class="row">
                        <div class="clear-fix"> 
                            
                        </div>
						<div class="col-sm-offset-2 col-sm-8 text-center">
                            <div class="embed-responsive embed-responsive-16by9">
                                <iframe class="embed-responsive-item" style="padding-top:40px;" src="https://www.youtube.com/embed/cjII5SKf7-s?rel=0&amp;showinfo=0" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
				</div>
            </section>

            {{-- <section class="pageCategory-section last section-black">
                <div class="container intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 text-center">
                        <hr>
                        <h2 class="open-sans h2-transform" style="color: #fff; padding-top:1px;">Interested to attend NEON Music Festival Beijing (22 & 23 September 2018)?<br/>Ticket is on sale now!</h2><br />
                       	</div>
                    </div>
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 btn-wrapper text-center"> 
                            <a class="btn btn-danger" href="neonbeijing" role="button">Buy NEON Beijing Tickets</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 img-wrap text-center"> 
                        <img src="{{asset('images/neon2018/neon-beijing-edm.jpg')}}" style="width: 100%;" class="img-responsive" alt="Neon Beijing">
                        </div>
                    </div>
				</div>
            </section> --}}

        </div><!-- /Main Body -->
    </section><!-- /Content Section -->
@endsection

@section('customjs')
	{{-- countdown --}}
	<script src="js/jquery.time-to.js"></script>
	<script type="text/javascript">
		$('#countdown').timeTo({
		    timeTo: new Date(new Date('Thursday November 22 2018 14:00:00 GMT+0800 (+08)')),
		    displayDays: 2,
		    theme: "black",
		    displayCaptions: true,
		    fontSize: 48,
		    captionSize: 14
		}); 
	</script>
	{{-- /countdown --}}

	<script type="text/javascript">
	//Initialize Swiper
	var swiper = new Swiper('.swiper-container', {
		pagination: '.swiper-pagination',        
		paginationClickable: true,
		slidesPerView: 'auto',
		spaceBetween: 10,
		nextButton: '.swiper-button-next',
		prevButton: '.swiper-button-prev',
		freeMode: true
	});

	// Enlarge Seat Plan Image
	$(function() {
		$('.seatPlanImg').on('click', function() {
		$('.enlargeImageModalSource').attr('src', $(this).attr('src'));
		$('#enlargeImageModal').modal('show');
		});
	});

	// Hide top Banner when page scroll
	var header = $('.eventBanner');
	var range = 450;

	$(window).on('scroll', function () {
		
		var scrollTop = $(this).scrollTop();
		var offset = header.offset().top;
		var height = header.outerHeight();
		offset = offset + height;
		var calc = 1 - (scrollTop - offset + range) / range;

		header.css({ 'opacity': calc });

		if ( calc > '1' ) {
		header.css({ 'opacity': 1 });
		} else if ( calc < '0' ) {
		header.css({ 'opacity': 0 });
		}
	});

	// Smooth scroll for acnhor links
	$('a[href*="#"]:not([href="#"])').click(function() {
		if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
		var target = $(this.hash);
		target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
		if (target.length) {
			$('html, body').animate({
			scrollTop: target.offset().top
			}, 1000);
			return false;
		}
		}
	});

	</script>

	{{-- Buy button disable --}}
	<script type="text/javascript">
		$(function() {
			$('a[id^=buyButton]').each(function() {
				var date = new Date();
				var enddate = $(this).attr('datetime'); 
				if ( Date.parse(date) >= Date.parse(enddate)) {
				  $(this).addClass('disabled');
				}
			});
		});
	</script>
	
@endsection

@section('modal')
	@include('layouts.partials.modals._seatplan')
	@include('layouts.partials.modals._getTixCustom')

@endsection