@extends('master')
@section('title')
    Sensation Rise
@endsection

@section('header')
    @include('layouts.partials._header')
    <meta property="og:url" content="{{ Request::fullUrl() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Sensation Rise" />
    <meta property="og:description" content="Originating in Amsterdam, the Netherlands, Sensations RISE is said to symbolize breaking free from the worries and boundaries of daily life, letting go, and rising above yourself to ultimate euphoria."/>
    <meta property="og:image" content="{{ Request::Url().'images/sensationrise2018/thumbnail.jpg' }}" />
@endsection

@section('content')
    <!-- Banner Section -->
    <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/sensationrise2018/web-banner.jpg')}}" style="width: 100%" class="img-responsive" alt="Sensation Rise"></div>
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            <img src="{{asset('images/sensationrise2018/thumbnail.jpg')}}" style="width: 100%" class="img-responsive" alt="Sensation Rise">
        </div>
    </section>
    <!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
            <div class="row priceNbtn">
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="row">
                        <div class="col-sm-9 leftBox">
                            <h6>Sensation Rise</h6> Tickets from <span>RM186</span>
                        </div>
                        <div class="col-sm-3 text-center">
                            <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar section-white">
            <section class="pageCategory-section last">
                <div class="container intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i>  27th October 2018</div>
                            <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i>  G.M.C Stadium, Hyderabad <a target="_blank" href="https://goo.gl/maps/EdBTANcnJi72">View Map</a></div>
                            <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> Saturday (6.30 pm - 12.30am)</div>
                            <div class="clearfix">&nbsp;</div>
                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <div class="addthis_inline_share_toolbox"></div>
                            <!-- /sharing -->
                            <hr>
                            <p><h2>Sensation Rise </h2><br/>
                            Originating in Amsterdam, the Netherlands, Sensations RISE is said to symbolize breaking free from the worries and boundaries of daily life, letting go, and rising above yourself to ultimate euphoria. Deviating from the notable centre stage, RISE will mark the beginning of a new era with an end-stage set-up, bringing a new Sensation experience to thousands of people around the world.</p>
                            <p>This show embodies the message of freedom and rebirth. It is delicate, transparent, open-minded, and will bring you to a higher state of mind. World-renowned electronic music event, Sensation, will transform Hyderabad into the world’s biggest nightclub with a spine-tingling show full of fantasy.</p>
                            <div class="clearfix"></div>
                            <div class="embed-responsive embed-responsive-16by9" style="margin-top: 40px; margin-bottom: 40px;">
                                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/JHf2U3aLmwU?ecver=2" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                            </div>
                        </div>
                        
                    </div>
                </div>
                {{-- <div class="clearfix"></div> --}}
            </section>

            {{-- <section class="pageCategory-section last section-grey">
                <div class="container">
                    <div class="gallery text-center">
                        <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
                        <!-- Swiper -->
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <a href="images/sensationrise2018/gallery-1.jpg" data-featherlight="image"><img class="" src="images/sensationrise2018/gallery-1.jpg" alt=""></a>
                                </div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next swiper-button-white"></div>
                            <div class="swiper-button-prev swiper-button-white"></div>
                        </div><!-- /Swiper -->
                    </div>
                </div>
            </section> --}}

            <section class="pageCategory-section last"><a id="anchorPrice"></a>
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                            <div class="text-center">
                                <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                                <p>Select ticket</p>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="table-responsive">
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Category</th>
                                            <th>Ticket Price</th>
                                            <th>Remarks</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>General Admission (GA)</td>
                                            <td>RM186</td>
                                            <td rowspan="2">DRESS CODE - WHITE Mandatory</td>
                                        </tr>
                                        <tr>
                                            <td>Deluxe Ticket (VIP)</td>
                                            <td>RM278</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="buyAlert-bar">
                                <a class="btn btn-danger" id="buyButton" datetime="Oct 27 2018 00:00:00 GMT+0800" target="_blank" href="https://redtix-tickets.airasia.com/en-AU/shows/sensation rise - hyderabad 2018/events" role="button">BUY TICKETS</a>
                                {{-- <span class="or">/</span>
                                <span class="popData-btn" data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets <i class="fa fa-info-circle" aria-hidden="true"></i></span> --}}
                            </div>
                            
                            <div class="note text-left">
                                <h2>Important Notes</h2>
                                <ol>
                                    <li>DRESS CODE - WHITE Mandatory </li>
                                    <li>GA Tickets - Access to the GA Zone/main dance floor at Sensation Rise. Dress Code - ALL WHITE MANDATORY.</li>
                                    <li>Deluxe tickets give access to the deluxe zone. Deluxe experience includes a seperate entry, exclusive elevated platform with the best view of the main stage, dedicated bar and toilets. Dress code - ALL WHITE MANDATORY.</li>
                                    <li>Prices shown exclude RM4.00 AirAsiaRedTix fee.</li>
                                    {{-- <li>All ticket prices will be paid in Malaysian Ringgit. Indian Rupee prices are shown for reference purposes only.</li>
                                    <li>Prices quoted in INR may differ due to currency fluctuations.</li> --}}
                                    <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li>
                                    <li>RM10.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                                    <li>Online ticket selling will close 2 days prior to event day, subject to availability.</li>
                                </ol>
                                <h2>Terms &amp; Conditions</h2>
                                <ol>
                                    <li>Entry permitted for age 15 years and above only</li>
                                    <li>The mandatory dress code is all white. No entry will be permitted without all white clothing.</li>
                                    <li>The event is subject to government permissions. In case the permissions are not granted and event is cancelled, a full refund shall be immediately issued to all patrons.</li>
                                    <li>A band will be issued at the venue box office on the day of the event in exchange for your e-ticket purchase confirmation from bookmyshow.com. Please carry valid photo id for verification of the ticket holder name along with your e-ticket print out.</li>
                                    <li>No refund on a purchased ticket is possible, even in case of any rescheduling.</li>
                                    <li>Unlawful resale (or attempted unlawful resale) of a band would lead to seizure or cancellation of that ticket without refund or other compensation.</li>
                                    <li>Duplicate bands will not be issued for lost or stolen bands.</li>
                                    <li>Each band admits one person only.</li>
                                    <li>This is a wristband access festival. In case wrist-bands are taken off, a replacement wrist-band will not be issued.</li>
                                    <li>Artist lineup and billed attractions may be subject to change.</li>
                                    <li>Wristbands removed or tampered with will be rendered invalid and WILL NOT be replaced.</li>
                                    <li>No re-entry is permitted for the entire period of the event. Wrist bands will be cut by the security team in case a customer wishes to exit the venue during the period of the event.</li>
                                    <li>Alcohol will be served to guests above the legal drinking age (LDA) and on display of valid age proof. LDA bands will be provided at the venue on proof of age.</li>
                                    <li>Artist line-up and billed attractions may be subject to change.</li>
                                    <li>Organizers reserve the right to perform security checks on invitees/members of the audience at the entry point for security reasons.</li>
                                    <li>Organizers or any of its agents, officers, employees shall not be responsible for any injury, damage, theft, losses or cost suffered at or as a result of the event of any part of it.</li>
                                    <li>Parking near or at the festival premises is at the risk of the vehicle owner. Organizer will not hold responsibility for any damage or theft of any vehicles within the stipulated parking premises of the festival.</li>
                                    <li>Consumption and sale of illegal substances is strictly prohibited.</li>
                                    <li>Professional cameras, any form of recording instruments, arms and ammunition, eatables, bottled water, beverages, alcohol are not allowed from outside the festival. Food and beverages will be available inside the festival.</li>
                                    <li>This band permits entry to the festival grounds only. Any Pre- or After parties may have other ticketing requirements and may be sold separately.</li>
                                    <li>Rights of admission reserved, even to valid ticket holders.</li>
                                </ol>
                                <h2>For enquiry only:</h2>
                                <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                            </div>

                        </div>
                    </div>
                </div>
            </section>
        </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 350;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });
    </script>

    {{-- Buy button disable --}}
    <script type="text/javascript">
        $(function() {
            $('a[id^=buyButton]').each(function() {
                var date = new Date();
                var enddate = $(this).attr('datetime'); 
                if ( Date.parse(date) >= Date.parse(enddate)) {
                  $(this).addClass('disabled');
                }
            });
        });
    </script>

@endsection

@section('modal')
    @include('layouts.partials.modals._seatplan')
    @include('layouts.partials.modals._getTix')
@endsection