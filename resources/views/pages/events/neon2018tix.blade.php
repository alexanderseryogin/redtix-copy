@extends('master')
@section('title')
	 Neon Countdown 2018
@endsection

@section('header')
	@include('layouts.partials._header')
	
@endsection

@section('content')
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:600|Roboto" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
    <link type="text/css" rel="stylesheet" href="css/ufc221.css">
    <link type="text/css" rel="stylesheet" href="css/custom2.css">
    <link type="text/css" rel="stylesheet" href="css/clock.css">
    <link type="text/css" rel="stylesheet" href="/css/custom/redtix.css">
    <link type="text/css" rel="stylesheet" href="css/timeTo.css">

<style type="text/css">
	.background-full {
		background-image: url('/images/neon2018/background.jpg');
	    background-size: 100% auto;
		background-repeat: no-repeat;
		height: 950px;
	}
    
    .sect-count p img{
        padding-top: 100px;
		width: 40%;
    }
	.open-sans {
		font-family: 'Open Sans', sans-serif;
        font-weight: 100;
	}
	.h2-transform {
        font-size: 18px;
        text-transform: uppercase;
	}
	.btn-danger {
		background: linear-gradient(#b82941, #40151c);
	}
	.btn-wrapper {
		margin-top: 20px;
	}
	
	@media screen and (max-width: 760px){ 
		.background-full {
			background-image: url('/images/neon2018/mobile-background.jpg');	
		}
		.btn-wrapper {
			margin-top: 20px;
		}

	}
	@media screen and (min-width: 768px){ 
		.background-full { 
			height: 950px;
		}
	}
	@media screen and (min-width: 1024px){ 
		.background-full { 
   			height: 950px;
		}
	}
	@media screen and (max-width: 1024px){ 
		.background-full { 
			height: 800x;
		}
	}
	@media screen and (min-width: 768px) and (max-width: 1024px){ 
		.background-full {
			height: 950px;
		}
	}
	@media screen and (min-width: 1024px) and (max-width: 1366px){ 
		.background-full{
			height: 950px;
		}
	}
	@media screen and (min-width: 1400px){ 
		.background-full{
			height: 950px;
		}
	}
	@media screen and (max-width: 414px){ 
		.background-full { 
			height: 600px;
		}
        .sect-count p img{
        	padding-top: 10px;
			width: 60%;
    	}
	}
    
	.embed-responsive-item {
		padding-top: 20px;
	}
</style>

<section class="pageContent">
      <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar" style="padding-top: 0px;">
            <section class="pageCategory-section last section-black background-full">
                <div class="container intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 sect-count text-center">
                        <p><img src="{{asset('images/neon2018/logo.png')}}" alt="Neon Countdown 2018"></p>
						
                        {{-- <h2 class="open-sans h2-transform" style="color: #fff; padding-top:40px;">Register now to get access to early bird pricing</h2> --}}
                        </div>
                    </div>
					<div class="row">
						<div class="col-sm-offset-1 col-sm-10 sect-count text-center">
							<div class="text-center" id="countdown" style="padding-top: 40px; padding-bottom: 100px;">
							</div>
						</div>
						<div class="col-sm-offset-1 col-sm-10 sect-count text-center">
	                        <p style="color: #fff; padding-top:60px; text-transform:uppercase; font-size:18px;">to NEON Countdown 2018 at<br/>Sepang International Circuit (Paddock)</p>
                        </div>

                    {{-- <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 btn-wrapper text-center"> 
						    <a class="btn btn-danger" href="https://docs.google.com/forms/d/e/1FAIpQLSf13UV7-VejvodSVnAqpjVHyXLa20rURwwOwRfM9JSKVKJ8rg/viewform" role="button">Register for NEON Kuala Lumpur</a>
                        </div>
                    </div> --}}
					</div>
            </section>

            <section class="pageCategory-section last section-black">
                <div class="container intro">
                    <div class="row">
                        <div class="clear-fix">
                        
                       	</div>
						<div class="col-sm-offset-1 col-sm-10 sect-count text-center">
							<p>Malaysia’s biggest Countdown is back!</p>
                            <p>Thousands of music lovers gathered once again under the NEONfield this past New Year’s Eve for the annual Neon Countdown event. The festival—hosted by Neon Events at Sepang International Circuit—combines electronic music, performers, art and engagement for hours of whimsical fun for attendees—also known as Headliners. Explore, Engage, Energize and Enjoy! Take your NYE countdown celebration to the next level and treat yourself to some unique perks, from the fast entry and VIP-exclusive entry, to gourmet food and drinks options, to a dazzling array of special activities and entertainment. Take in the view from one of our dedicated VIP NEON Skydeck, get your body painted.</p>
                            <p>All this and much more await you with the NEON Countdown SKY DECK VIP Experience!</p>
                            <p>Started with our Pre-registration for VIP; GA where attendees can secure their Early Glow tickets before it ran out and secure the experience during the event day. Neon Countdown’s upgraded this time with VIP called Premium Neon Sky Deck, carrying a cool price tag in 2018. For that small fortune, you get lounges with prime views of the main stages, private bars, catered dinner, premium excess, and a whole lot more to make you very, very smug. Guess, time to consider the VIP package now!</p>
                            <p>The line-up for the 2018 edition of the Neon Countdown is W&W, while other special names dropping on November. As DJs, currently #14 in the DJ Mag Top 100, they are also dominating dance floors and festivals around the world.This year, we welcome you into Midnight, as they illuminate the darkness and unlock the gates to our lost wonderland of the Neon Garden. Be entwined in fluorescent vines, seduced by the glow of carnivorous flowers, decoration, activities, our unique UV paints and cosmetics and tempted by mysterious forest-dwelling beauties.</p>
                            <p>Come to the hothouse of phosphorescent delectation and dare to neon-dream your way into a nubile new year. The memories you make, the people you meet, the music you listen to, and the activities you do will stick with you forever.</p>
                            <p>Tick-tock goes the clock – we’ll all be together when the beat drops! Put on your New Year’s Eve best, and turn up with us as we countdown to midnight and start 2018 with a bang. Get your tickets at www.AirAsiaRedtix.com.</p>
						</div>
                    </div>
                    <div class="row">
                        <div class="clear-fix"> 
                            
                        </div>
						<div class="col-sm-offset-2 col-sm-8 text-center">
                            <div class="embed-responsive embed-responsive-16by9">
                                <iframe class="embed-responsive-item" style="padding-top:40px;" src="https://www.youtube.com/embed/cjII5SKf7-s?rel=0&amp;showinfo=0" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
				</div>
            </section>

            {{-- <section class="pageCategory-section last section-grey">
                <div class="container">
                    <div class="gallery text-center">
                        <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
                        <!-- Swiper -->
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <a href="images/lamc/singles/judaspriest/gallery-1.jpg" data-featherlight="image"><img class="" src="images/lamc/singles/judaspriest/gallery-1.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/lamc/singles/judaspriest/gallery-2.jpg" data-featherlight="image"><img class="" src="images/lamc/singles/judaspriest/gallery-2.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/lamc/singles/judaspriest/gallery-3.jpg" data-featherlight="image"><img class="" src="images/lamc/singles/judaspriest/gallery-3.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/lamc/singles/judaspriest/gallery-4.jpg" data-featherlight="image"><img class="" src="images/lamc/singles/judaspriest/gallery-4.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/lamc/singles/judaspriest/web-banner.jpg" data-featherlight="image"><img class="" src="images/lamc/singles/judaspriest/web-banner.jpg" alt=""></a>
                                </div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next swiper-button-white"></div>
                            <div class="swiper-button-prev swiper-button-white"></div>
                        </div><!-- /Swiper -->
                    </div>
                </div>
            </section> --}}

            <section class="pageCategory-section last"><a id="anchorPrice"></a>
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">

                            @include('layouts.partials._showticketsneon')

                            <div class="note text-left">
                                <h2>Important Notes</h2>
                                <ol>
                                    <li>Prices shown exclude RM4.00 AirAsiaRedTix fee.</li>
                                    <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li>
                                    <li>RM10.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                                    <li>Online ticket selling will close 1 day prior to event day, subject to availability.</li>
                                </ol>
                                <h2>For enquiry only:</h2>
                                <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        </div><!-- /Main Body -->
    </section><!-- /Content Section -->
@endsection

@section('customjs')
	{{-- countdown --}}
	<script src="js/jquery.time-to.js"></script>
	<script type="text/javascript">
		$('#countdown').timeTo({
		    timeTo: new Date(new Date('Monday December 31 2018 16:00:00 GMT+0800 (+08)')),
		    displayDays: 2,
		    theme: "black",
		    displayCaptions: true,
		    fontSize: 48,
		    captionSize: 14
		}); 
	</script>
	{{-- /countdown --}}

	<script type="text/javascript">
	//Initialize Swiper
	var swiper = new Swiper('.swiper-container', {
		pagination: '.swiper-pagination',        
		paginationClickable: true,
		slidesPerView: 'auto',
		spaceBetween: 10,
		nextButton: '.swiper-button-next',
		prevButton: '.swiper-button-prev',
		freeMode: true
	});

	// Enlarge Seat Plan Image
	$(function() {
		$('.seatPlanImg').on('click', function() {
		$('.enlargeImageModalSource').attr('src', $(this).attr('src'));
		$('#enlargeImageModal').modal('show');
		});
	});

	// Hide top Banner when page scroll
	var header = $('.eventBanner');
	var range = 450;

	$(window).on('scroll', function () {
		
		var scrollTop = $(this).scrollTop();
		var offset = header.offset().top;
		var height = header.outerHeight();
		offset = offset + height;
		var calc = 1 - (scrollTop - offset + range) / range;

		header.css({ 'opacity': calc });

		if ( calc > '1' ) {
		header.css({ 'opacity': 1 });
		} else if ( calc < '0' ) {
		header.css({ 'opacity': 0 });
		}
	});

	// Smooth scroll for acnhor links
	$('a[href*="#"]:not([href="#"])').click(function() {
		if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
		var target = $(this.hash);
		target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
		if (target.length) {
			$('html, body').animate({
			scrollTop: target.offset().top
			}, 1000);
			return false;
		}
		}
	});

    // Modal Announcement
	//$(document).ready(function(){
    //    $("#announcementModal").modal('show');
  	//});	

	</script>

	{{-- Buy button disable --}}
	<script type="text/javascript">
		$(function() {
			$('a[id^=buyButton]').each(function() {
				var date = new Date();
				var enddate = $(this).attr('datetime'); 
				if ( Date.parse(date) >= Date.parse(enddate)) {
				  $(this).addClass('disabled');
				}
			});
		});
	</script>
	
@endsection

@section('modal')
	@include('layouts.partials.modals._seatplan')
	@include('layouts.partials.modals._getTixCustom')
    @include('layouts.partials.modals._moreInfoNeon')

   <div id="announcementModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
                    <h1><i class="fa fa-bullhorn text-warning" aria-hidden="true"></i></h1>
                    <h4>Announcement</h4>
                    <div class="clearfix">&nbsp;</div>
                    <div class="well">
                        {{-- <p>Dear Ticket Purchasers, it will take up to 24hrs to be processed for purchases made through FPX Payment. Once processed, your tickets will be sent to you thereafter. Thank you.</p> --}}
						<p>Dear Ticket Purchasers, we have to temporarily suspend our tickets sales due to high demands. We shall get back to you guys soon!</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


