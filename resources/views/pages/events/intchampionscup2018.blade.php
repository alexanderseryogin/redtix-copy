@extends('master')
@section('title')
    International Champions Cup Singapore
@endsection

@section('header')
    @include('layouts.partials._header')
    <meta property="og:url" content="{{ Request::fullUrl() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="International Champions Cup Singapore" />
    <meta property="og:description" content="International Champions Cup Singapore" />
    <meta property="og:image" content="{{ Request::Url().'images/intchampionscup2018/thumbnail.jpg' }}" />
@endsection

@section('content')
    <!-- Banner Section -->
    <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/intchampionscup2018/web-banner.jpg')}}" style="width: 100%" class="img-responsive" alt="International Champions Cup Singapore"></div>
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            <img src="{{asset('images/intchampionscup2018/thumbnail.jpg')}}" style="width: 100%" class="img-responsive" alt="International Champions Cup Singapore">
        </div>
    </section>
    <!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
            <div class="row priceNbtn">
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="row">
                        <div class="col-sm-9 leftBox">
                            <h6>International Champions Cup Singapore</h6>Tickets from <span>RM88</span>
                        </div>
                        <div class="col-sm-3 text-center">
                            <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar section-white">
            <section class="pageCategory-section last">
                <div class="container intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i>  26th - 30th July 2018</div>
                            <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i> National Stadium, Singapore <a target="_blank" href="https://goo.gl/maps/FUXXiGGjWZo">View Map</a></div>
                            <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> 26th - 30th July 2018 (7.30 pm)</div>
                            <div class="clearfix">&nbsp;</div>
                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <div class="addthis_inline_share_toolbox"></div>
                            <!-- /sharing -->
                            <hr>
                            <p>The 2018 International Champions Cup in Singapore will see Arsenal, Atlético de Madrid and Paris Saint-Germain take on each other at the National Stadium on 26 July, 28 July and 30 July.</p>
                            <p>Kicking off on Thursday, 26 July, Atlético de Madrid will play Arsenal in a highly anticipated meeting after the teams’ hard-fought campaign in Europe this season. Arsenal will tackle Paris Saint-Germain on Saturday, 28 July in a rematch of their 2016/ 2017 UEFA Champions League group stage. While the final match on Monday, 30 July, will pit champion of Ligue 1, Paris Saint-Germain, against Atlético de Madrid. Fans will, no doubt, be looking forward to these match-ups.</p>
                            <p>The 2018 International Champions Cup takes on a new global format, featuring the largest number of clubs, matches and venues to date. This year’s chase for the Cup features 18 of the best clubs in the world playing 27 matches across the United States, Europe and Singapore.</p>
                            <p><b>Fixtures of the 2018 International Champions Cup in Singapore:</b><br />
                            Thursday, July 26, 2018<br />
                            Atletico de Madrid vs Arsenal<br /><br />
                            Saturday, July 28, 2018<br />
                            Arsenal vs Paris Saint-Germain<br /><br />
                            Monday, July 30, 2018<br />
                            Paris Saint-Germain vs Atletico de Madrid</p>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </section>

            <section class="pageCategory-section last">
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                            <div class="text-center">
                                <h1 class="subSecTitle"><strong>International Champions Cup Singapore</strong></h1>
                                {{-- <p>Select ticket</p> --}}
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="table-responsive">
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Sponsors</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><img class="image-responsive" src="images/intchampionscup2018/sponsors.jpg" style="width:100%; height:auto;" alt=""></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            {{-- <section class="pageCategory-section last section-grey">
                <div class="container">
                    <div class="gallery text-center">
                        <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
                        <!-- Swiper -->
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <a href="images/intchampionscup2018/poster.jpg" data-featherlight="image"><img class="" src="images/intchampionscup2018/poster.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/intchampionscup2018/gallery1.jpg" data-featherlight="image"><img class="" src="images/intchampionscup2018/gallery1.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/intchampionscup2018/gallery2.jpg" data-featherlight="image"><img class="" src="images/intchampionscup2018/gallery2.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/intchampionscup2018/gallery3.jpg" data-featherlight="image"><img class="" src="images/intchampionscup2018/gallery3.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/intchampionscup2018/gallery4.jpg" data-featherlight="image"><img class="" src="images/intchampionscup2018/gallery4.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/intchampionscup2018/gallery5.jpg" data-featherlight="image"><img class="" src="images/intchampionscup2018/gallery5.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/intchampionscup2018/gallery6.jpg" data-featherlight="image"><img class="" src="images/intchampionscup2018/gallery6.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/intchampionscup2018/gallery7.jpg" data-featherlight="image"><img class="" src="images/intchampionscup2018/gallery7.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/intchampionscup2018/gallery8.jpg" data-featherlight="image"><img class="" src="images/intchampionscup2018/gallery8.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/intchampionscup2018/gallery9.jpg" data-featherlight="image"><img class="" src="images/intchampionscup2018/gallery9.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/intchampionscup2018/web-banner1.jpg" data-featherlight="image"><img class="" src="images/intchampionscup2018/web-banner1.jpg" alt=""></a>
                                </div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next swiper-button-white"></div>
                            <div class="swiper-button-prev swiper-button-white"></div>
                        </div><!-- /Swiper -->
                    </div>
                </div>
            </section> --}}

            <section class="pageCategory-section last"><a id="anchorPrice"></a>
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                            <div class="text-center">
                                <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                                <p>Select ticket</p>
                            </div>

                            <div class="clearfix">&nbsp;</div>
                            <div class="text-center">
                                <h1 class="subSecTitle"><strong>Buy 2 tickets for a special discounted price. Choices available for match 1,2 or 3 in CAT 2, 3, 4 and 5. Limited time only.</strong></h1>
                            </div>
                            <div class="table-responsive">
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th>MATCH</th>
                                            <th>CAT 1</th>
                                            <th>CAT 2</th>
                                            <th>CAT 3</th>
                                            <th>CAT 4</th>
                                            <th>CAT 5</th>
                                            <th>CAT 6</th>
                                            <th>CAT 7</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Atletico v Arsenal (Match 1)</td>
                                            <td>n/a</td>
                                            <td>RM592.00</td>
                                            <td>RM440.00</td>
                                            <td>RM320.00</td>
                                            <td>RM228.0</td>
                                            <td>n/a</td>
                                            <td>n/a</td>
                                        </tr>
                                        <tr>
                                            <td>Arsenal vs PSG (Match 2)</td>
                                            <td>n/a</td>
                                            <td>RM592.00</td>
                                            <td>RM440.00</td>
                                            <td>RM320.00</td>
                                            <td>RM228.0</td>
                                            <td>n/a</td>
                                            <td>n/a</td>
                                        </tr>
                                        <tr>
                                            <td>PSG v Atletico (Match 3)</td>
                                            <td>n/a</td>
                                            <td>RM530.00</td>
                                            <td>RM380.00</td>
                                            <td>RM260.00</td>
                                            <td>RM228.0</td>
                                            <td>n/a</td>
                                            <td>n/a</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                             <div class="buyAlert-bar">
                                <a class="btn btn-danger" id="buyButton" datetime="July 25 2018 00:00:00 GMT+0800" target="_blank" href="http://redtix-tickets.airasia.com/en-AU/shows/international champions cup singapore - match 1/events" role="button">BUY MATCH 1</a>
                                <span class="or">/</span>
                                <a class="btn btn-danger" id="buyButton" datetime="July 26 2018 00:00:00 GMT+0800" target="_blank" href="http://redtix-tickets.airasia.com/en-AU/shows/international champions cup singapore - match 2/events" role="button">BUY MATCH 2</a>
                                <span class="or">/</span>
                                <a class="btn btn-danger" id="buyButton" datetime="July 26 2018 00:00:00 GMT+0800" target="_blank" href="http://redtix-tickets.airasia.com/en-AU/shows/international champions cup singapore - match 3/events" role="button">BUY MATCH 3</a>
                                {{-- <span class="or">/</span>
                                <span class="popData-btn" data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets <i class="fa fa-info-circle" aria-hidden="true"></i></span> --}}
                            </div>

                            <div class="clearfix">&nbsp;</div>
                            <div class="table-responsive">
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Match 1 (Atletico v Arsenal)</th>
                                            <th>Ticket Price</th>
                                            <th>Seating Map</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Cat 1</td>
                                            <td>RM580</td>
                                            <td rowspan="7"><img class="img-responsive seatPlanImg" src="images/intchampionscup2018/seat-plan3.jpg" alt=""></td>
                                        </tr>
                                        <tr>
                                            <td>Cat 2</td>
                                            <td>RM519</td>
                                        </tr>
                                        <tr>
                                            <td>Cat 3</td>
                                            <td>RM368</td>
                                        </tr>
                                        <tr>
                                            <td>Cat 4</td>
                                            <td>RM248</td>
                                        </tr>
                                        <tr>
                                            <td>Cat 5</td>
                                            <td>RM217</td>
                                        </tr>
                                        <tr>
                                            <td>Cat 6</td>
                                            <td>RM157</td>
                                        </tr>
                                        <tr>
                                            <td>Cat 7</td>
                                            <td>RM88</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                             <div class="buyAlert-bar">
                                <a class="btn btn-danger" id="buyButton" datetime="July 27 2018 00:00:00 GMT+0800" target="_blank" href="http://redtix-tickets.airasia.com/en-AU/shows/international champions cup singapore - match 1/events" role="button">BUY TICKETS</a>
                                {{-- <span class="or">/</span>
                                <span class="popData-btn" data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets <i class="fa fa-info-circle" aria-hidden="true"></i></span>  --}}
                            </div>

                            <div class="clearfix">&nbsp;</div>
                            <div class="table-responsive">
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Match 2 (Arsenal v PSG)</th>
                                            <th>Ticket Price</th>
                                            <th>Seating Map</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Cat 1</td>
                                            <td><strike>RM640</strike> <p style="color:red;">SOLD OUT</p></td>
                                            <td rowspan="7"><img class="img-responsive seatPlanImg" src="images/intchampionscup2018/seat-plan2.jpg" alt=""></td>
                                        </tr>
                                        <tr>
                                            <td>Cat 2</td>
                                            <td>RM580</td>
                                        </tr>
                                        <tr>
                                            <td>Cat 3</td>
                                            <td>RM429</td>
                                        </tr>
                                        <tr>
                                            <td>Cat 4</td>
                                            <td>RM308</td>
                                        </tr>
                                        <tr>
                                            <td>Cat 5</td>
                                            <td>RM217</td>
                                        </tr>
                                        <tr>
                                            <td>Cat 6</td>
                                            <td>RM157</td>
                                        </tr>
                                        <tr>
                                            <td>Cat 7</td>
                                            <td>RM88</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                             <div class="buyAlert-bar">
                                <a class="btn btn-danger" id="buyButton" datetime="July 26 2018 00:00:00 GMT+0800" target="_blank" href="http://redtix-tickets.airasia.com/en-AU/shows/international champions cup singapore - match 2/events" role="button">BUY TICKETS</a>
                                {{-- <span class="or">/</span>
                                <span class="popData-btn" data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets <i class="fa fa-info-circle" aria-hidden="true"></i></span>  --}}
                            </div>

                            <div class="clearfix">&nbsp;</div>
                            <div class="table-responsive">
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Match 3 (PSG v Atletico)</th>
                                            <th>Ticket Price</th>
                                            <th>Seating Map</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Cat 1</td>
                                            <td>RM580</td>
                                            <td rowspan="7"><img class="img-responsive seatPlanImg" src="images/intchampionscup2018/seat-plan3.jpg" alt=""></td>
                                        </tr>
                                        <tr>
                                            <td>Cat 2</td>
                                            <td>RM519</td>
                                        </tr>
                                        <tr>
                                            <td>Cat 3</td>
                                            <td>RM368</td>
                                        </tr>
                                        <tr>
                                            <td>Cat 4</td>
                                            <td>RM248</td>
                                        </tr>
                                        <tr>
                                            <td>Cat 5</td>
                                            <td>RM217</td>
                                        </tr>
                                        <tr>
                                            <td>Cat 6</td>
                                            <td>RM157</td>
                                        </tr>
                                        <tr>
                                            <td>Cat 7</td>
                                            <td>RM88</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                             <div class="buyAlert-bar">
                                <a class="btn btn-danger" id="buyButton" datetime="July 27 2018 00:00:00 GMT+0800" target="_blank" href="http://redtix-tickets.airasia.com/en-AU/shows/international champions cup singapore - match 3/events" role="button">BUY TICKETS</a>
                                {{-- <span class="or">/</span>
                                <span class="popData-btn" data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets <i class="fa fa-info-circle" aria-hidden="true"></i></span>  --}}
                            </div>

                            <div class="clearfix">&nbsp;</div>
                            <div class="table-responsive">
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Packages</th>
                                            <th>Cat 1</th>
                                            <th>Cat 2</th>
                                            <th>Cat 3</th>
                                            <th>Cat 5</th>
                                            <th>Seating Map</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Match 1 &amp; 2 (Arsenal)</td>
                                            <td>RM1,153</td>
                                            <td>RM1,045</td>
                                            <td>RM773</td>
                                            <td>RM392</td>
                                            <td rowspan="3"><img class="img-responsive seatPlanImg" src="images/intchampionscup2018/seat-plan3.jpg" alt=""></td>
                                        </tr>
                                        <tr>
                                            <td>Match 1 &amp; 3 (Atletico)</td>
                                            <td>RM1,099</td>
                                            <td>RM990</td>
                                            <td>RM718</td>
                                            <td>RM392</td>
                                        </tr>
                                        <tr>
                                            <td>Match 2 &amp; 3 (PSG)</td>
                                            <td>RM1,099</td>
                                            <td>RM990</td>
                                            <td>RM718</td>
                                            <td>RM392</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                             <div class="buyAlert-bar">
                                <a class="btn btn-danger disabled" id="buyButton" datetime="July 25 2018 00:00:00 GMT+0800" target="_blank" href="http://redtix-tickets.airasia.com/en-AU/shows/international champions cup singapore - 2-day club package/events" role="button">BUY TICKETS</a>
                                {{-- <span class="or">/</span>
                                <span class="popData-btn" data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets <i class="fa fa-info-circle" aria-hidden="true"></i></span>  --}}
                            </div>


                            {{-- <span class="importantNote">*Additional charges will be applied for credit/debit card transaction, optionally customer may pay cash for outlet purchase to avoid the charges.</span> --}}
                            {{-- <div class="note text-left">
                                <h2>Reminder</h2>
                                <ol>
                                    <li><b>Dress code</b> : Neat and decent. Worn out / faded / shabby jeans, t-shirt without collar, shorts, slippers, and sandals are STRICTLY NOT ALLOWED.</li>
                                    <li><b>Age limit to show</b> : 4 years old and above. Every person/kid needs ticket to own seat.</li>
                                    <li>Food and drinks are not allowed in the auditorium.</li>
                                </ol>
                            </div> --}}
                            <div class="note text-left">
                                <h2>Important Notes</h2>
                                <ol>
                                    {{-- <li>Prices shown include RM4.00 AirAsiaRedTix fee & other taxes.</li> --}}
                                    <li>Prices shown exclude RM12.00 AirAsiaRedTix fee.</li>
                                    <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li>
                                    <li>RM30.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                                    <!-- <li>Strictly no replacement for missing tickets, torn tickets and cancellation.</li> -->
                                    <li>Online ticket selling will close 1 days prior to event day, subject to availability.</li>
                                </ol>
                                <h2>For enquiry only:</h2>
                                <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 350;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });
    </script>

    {{-- Buy button disable --}}
    <script type="text/javascript">
        $(function() {
            $('a[id^=buyButton]').each(function() {
                var date = new Date();
                var enddate = $(this).attr('datetime'); 
                if ( Date.parse(date) >= Date.parse(enddate)) {
                  $(this).addClass('disabled');
                }
            });
        });
    </script>

@endsection

@section('modal')
    @include('layouts.partials.modals._seatplan')
    @include('layouts.partials.modals._getTix')
@endsection