@extends('master')
@section('title')
    Neon Lights Festival 2018
@endsection

@section('header')
    @include('layouts.partials._header')
    <meta property="og:url" content="{{ Request::fullUrl() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Neon Lights Festival 2018" />
    <meta property="og:description" content=""/>
    <meta property="og:image" content="{{ Request::Url().'images/neonlights2018/thumbnail2.jpg' }}" />
@endsection

@section('content')
    <!-- Banner Section -->
    <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/neonlights2018/web-banner2.jpg')}}" style="width: 100%" class="img-responsive" alt="Neon Lights Festival 2018"></div>
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            <img src="{{asset('images/neonlights2018/thumbnail2.jpg')}}" style="width: 100%" class="img-responsive" alt="Neon Lights Festival 2018">
        </div>
    </section>
    <!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
            <div class="row priceNbtn">
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="row">
                        <div class="col-sm-9 leftBox">
                            <h6>Neon Lights Festival 2018</h6> Tickets from <span>RM67.50</span>
                        </div>
                        <div class="col-sm-3 text-center">
                            <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar section-white">
            <section class="pageCategory-section last">
                <div class="container intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i>  9-11th November 2018</div>
                            <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i>  Fort Canning Park, Fort Gate, Singapore <a target="_blank" href="https://goo.gl/maps/98zvtwN4j932">View Map</a></div>
                            <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> 09 November 2018: 18.00</div>
                            <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> 10 - 11 November 2018: 14.00-22.30</div>
                            <div class="clearfix">&nbsp;</div>
                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <div class="addthis_inline_share_toolbox"></div>
                            <!-- /sharing -->
                            <div class="col-sm-12">
                                <hr>
                                <p><h2>Neon Lights Festival 2018</h2><br/>
                                Neon Lights, Singapore’s premier outdoor music and arts festival, is back for its 3rd edition at Fort Gate, Fort Canning Park from 9 to 11 November 2018! Be prepared for three exhilarating days of discovery and fun!</p>
                                <p>The family-friendly festival features a stellar line-up of international and local music acts, alongside an eclectic array of activities for all ages. Fans can catch their favourite bands live, indulge in creative pursuits with workshops, explore avant-garde performances and roving acts, see art installations, sample delicious food and many other surprises.</p>
                                <p>Attended by music and arts lovers in 2015 and in 2016, the Festival’s return is set to be one of the most eagerly anticipated and dynamic lifestyle events of the year.</p>
                                <p><i>“An interactive roller coaster of music-driven energy and emotion”</i> Billboard</p>
                                <p><i>“Two days of boundary-burning music and arts performances”</i> Timeout Singapore</p>
                                <p><br/>Enquiry Email: <a href="mailto:info@neonlights.sg">info@neonlights.sg</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="pageCategory-section last section-grey">
                <div class="container">
                    <div class="gallery text-center">
                        <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
                        <!-- Swiper -->
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <a href="images/neonlights2018/gallery-1.jpg" data-featherlight="image"><img class="" src="images/neonlights2018/gallery-1.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/neonlights2018/gallery-2.jpg" data-featherlight="image"><img class="" src="images/neonlights2018/gallery-2.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/neonlights2018/gallery-3.jpg" data-featherlight="image"><img class="" src="images/neonlights2018/gallery-3.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/neonlights2018/gallery-4.jpg" data-featherlight="image"><img class="" src="images/neonlights2018/gallery-4.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/neonlights2018/gallery-5.jpg" data-featherlight="image"><img class="" src="images/neonlights2018/gallery-5.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/neonlights2018/gallery-6.jpg" data-featherlight="image"><img class="" src="images/neonlights2018/gallery-6.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/neonlights2018/gallery-7.jpg" data-featherlight="image"><img class="" src="images/neonlights2018/gallery-7.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/neonlights2018/gallery-8.jpg" data-featherlight="image"><img class="" src="images/neonlights2018/gallery-8.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/neonlights2018/gallery-9.jpg" data-featherlight="image"><img class="" src="images/neonlights2018/gallery-9.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/neonlights2018/gallery-10.jpg" data-featherlight="image"><img class="" src="images/neonlights2018/gallery-10.jpg" alt=""></a>
                                </div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next swiper-button-white"></div>
                            <div class="swiper-button-prev swiper-button-white"></div>
                        </div><!-- /Swiper -->
                    </div>
                </div>
            </section>

            <section class="pageCategory-section last"><a id="anchorPrice"></a>
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                        <span class="importantNote"><br />* First 50 ticket purchasers will get 1 x NETS Card with credit of SGD10. First come first serve basis. NETS Card to be collected at ticket redemption counter on event day.</span>
                            <div class="text-center">
                                <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                                <p>Select ticket</p>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="table-responsive">
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th colspan="2">Ticket Category</th>
                                            <th>Ticket Price (RM)</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td rowspan="3">Early Bird</td>
                                            <td>Friday</td>
                                            <td>267.30</td>
                                        </tr>
                                        <tr>
                                            <td>Friday x 2 + 1 night stay</td>
                                            <td>1,034.60</td>
                                        </tr>
                                        <tr>
                                            <td>Sunday</td>
                                            <td>324.00</td>
                                        </tr>
                                        <tr style="background-color: #FCEFDC;">
                                            <td rowspan="5">Tier 2</td>
                                            <td>3 Day (Fri, Sat &amp; Sun)</td>
                                            <td>756.00</td>
                                        </tr>
                                        <tr style="background-color: #FCEFDC;">
                                            <td>3 Day (Fri, Sat &amp; Sun) x 2 + 3 nights stay</td>
                                            <td>3,012.00</td>
                                        </tr>
                                        <tr style="background-color: #FCEFDC;">
                                            <td>2 Day (Sat &amp; Sun)</td>
                                            <td>607.00</td>
                                        </tr>
                                        <tr style="background-color: #FCEFDC;">
                                            <td>2 Day (Sat &amp; Sun) x 2 + 2 nights stay</td>
                                            <td>2,215.00</td>
                                        </tr>
                                        <tr style="background-color: #FCEFDC;">
                                            <td>Saturday</td>
                                            <td>378.00</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="4">Child</td>
                                            <td>2 Day (Sat &amp; Sun)</td>
                                            <td>121.50</td>
                                        </tr>
                                        <tr>
                                            <td>Friday</td>
                                            <td>67.50</td>
                                        </tr>
                                        <tr>
                                            <td>Saturday</td>
                                            <td>67.50</td>
                                        </tr>
                                        <tr>
                                            <td>Sunday</td>
                                            <td>67.50</td>
                                        </tr>
                                        <tr style="background-color: #FCEFDC;">
                                            <td rowspan="5">Door Price (Adult)</td>
                                            <td>3 Day (Fri, Sat &amp; Sun)</td>
                                            <td>1,050.00</td>
                                        </tr>
                                        <tr style="background-color: #FCEFDC;">
                                            <td>2 Day (Sat &amp; Sun)</td>
                                            <td>840.00</td>
                                        </tr>
                                        <tr style="background-color: #FCEFDC;">
                                            <td>Friday</td>
                                            <td>375.00</td>
                                        </tr>
                                        <tr style="background-color: #FCEFDC;">
                                            <td>Saturday</td>
                                            <td>525.00</td>
                                        </tr>
                                        <tr style="background-color: #FCEFDC;">
                                            <td>Sunday</td>
                                            <td>420.00</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="4">Door Price (Child)</td>
                                            <td>2 Day (Sat &amp; Sun)</td>
                                            <td>135.50</td>
                                        </tr>
                                        <tr>
                                            <td>Friday</td>
                                            <td>75.00</td>
                                        </tr>
                                        <tr>
                                            <td>Saturday</td>
                                            <td>75.00</td>
                                        </tr>
                                        <tr>
                                            <td>Sunday</td>
                                            <td>75.00</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="buyAlert-bar">
                                <a class="btn btn-danger disabled" id="buyButton" datetime="Nov 8 2018 00:00:00 GMT+0800" target="_blank" href="https://redtix-tickets.airasia.com/en-AU/shows/neon lights festival 2018/events" role="button">BUY TICKETS</a>
                                {{-- <span class="or">/</span>
                                <span class="popData-btn" data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets <i class="fa fa-info-circle" aria-hidden="true"></i></span> --}}
                            </div>
                            {{--<span class="importantNote">Terms & Conditions for Children Tickets</span>
                            <span class="importantNote">* Only persons aged 2 years and younger do not require a ticket.</span>
                            <span class="importantNote">* Any persons under the age of 18 years must be accompanied and supervised at all times at the Festival by a parent and/or guardian who must also be a ticket holder.</span>
                            <span class="importantNote">* Under 18s must present identification at the Festival entrance.</span>
                            <br/>
                            <span class="importantNote"><b>Terms & Conditions for Hotel Package</b></span>
                            <span class="importantNote">* The stay will be at Yotel Singapore</span>
                            <span class="importantNote">* Room Type - Premium Queen (room only)</span>
                            <span class="importantNote">* Package validity - until 2nd Nov 2018</span> --}}
                            <div class="note text-left">
                                <h2>Terms & Conditions for Children Tickets</h2>
                                <ol>
                                    <li>Only persons aged 2 years and younger do not require a ticket.</li>
                                    <li>Any persons under the age of 18 years must be accompanied and supervised at all times at the Festival by a parent and/or guardian who must also be a ticket holder.</li>
                                    <li>Under 18s must present identification at the Festival entrance.</li>
                                </ol>
                            </div>

                            <div class="note text-left">
                                <h2>Terms & Conditions for Hotel Package</h2>
                                <ol>
                                    <li>The stay will be at Yotel Singapore</li>
                                    <li>Room Type - Premium Queen (room only)</li>
                                    <li>Package validity - until 2nd Nov 2018</li>
                                </ol>
                            </div>

                            <div class="note text-left">
                                <h2>Important Notes</h2>
                                <ol>
                                    {{-- <li>Prices shown exclude ticket fee.</li>
                                    <li>All ticket prices will be paid in Malaysian Ringgit. Indian Rupee prices are shown for reference purposes only.</li>
                                    <li>Prices quoted in INR may differ due to currency fluctuations.</li>
                                    <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li>
                                    <li>RM10.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                                    <li>Online ticket selling will close 2 days prior to event day, subject to availability.</li>
                                    <li>Adult: Strictly 18 years old and above.</li> --}}
                                    <li>Special Limited offer at AirAsia RedTix only</li>
                                    <li>Malaysian MyKad required at ticket redemption and entry into event for MyKad package</li>
                                    <li>Prices shown exclude RM4.00 AirAsiaRedTix fee and 3% Credit card fee.</li>
                                    <li>All ticket prices will be paid in Malaysian Ringgit.</li>
                                    <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li>
                                    <li>RM10.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                                    <li>Online ticket selling will close 2 days prior to event day, subject to availability.</li>
                                </ol>
                                <h2>For enquiry only:</h2>
                                <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 350;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });

    // Modal Announcement
	$(document).ready(function(){
        $("#announcementModal").modal('show');
  	});	
    </script>

    {{-- Buy button disable --}}
    <script type="text/javascript">
        $(function() {
            $('a[id^=buyButton]').each(function() {
                var date = new Date();
                var enddate = $(this).attr('datetime'); 
                if ( Date.parse(date) >= Date.parse(enddate)) {
                  $(this).addClass('disabled');
                }
            });
        });
    </script>

@endsection

@section('modal')
    @include('layouts.partials.modals._seatplan')
    @include('layouts.partials.modals._getTix')

   <div id="announcementModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
                    <h1><i class="fa fa-bullhorn text-warning" aria-hidden="true"></i></h1>
                    <h4>Announcement</h4>
                    <div class="clearfix">&nbsp;</div>
                    <div class="well">
                        <p>1. 10% off special limited offer for Malaysian MyKad holder at AirAsia RedTix only</p>
                        <p>2. 10% discounted price is reflected on the ticket price table.</p>
                        <p>3. MyKad strictly required at ticket redemption and entry into event</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection