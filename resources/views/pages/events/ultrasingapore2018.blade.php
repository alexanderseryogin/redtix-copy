{{-- route(@extends('master') --}}
@extends('masterforultra')
@section('title')
    Ultra Singapore 2018
@endsection

@section('header')
    @include('layouts.partials._header')
@endsection

@section('content')
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:600|Roboto" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
    <link type="text/css" rel="stylesheet" href="css/custom2.css">
    <link type="text/css" rel="stylesheet" href="css/clock.css">
    <link type="text/css" rel="stylesheet" href="/css/custom/ultra2018.css">
    <link type="text/css" rel="stylesheet" href="css/timeTo.css">

    <!-- Banner Section -->
    {{-- <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/ultrasingapore2018/ultra-banner-mock.jpg')}}" style="width: 100%" class="img-responsive" alt="ultrasingapore2018"><a href="#usgIntroduction"><img src="images/ultrasingapore2018/arrow.png" class="center-block arrow img-responsive"></a></div>
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            <img src="{{asset('images/ultrasingapore2018/mobile-splash-3.jpg')}}" style="width: 100%" class="img-responsive" alt="ultrasingapore2018">
            <a href="#usgIntroduction"><img src="images/ultrasingapore2018/arrow.png" class="center-block arrow-mobile img-responsive"></a>
        </div>
    </section> --}}

	<section class="pageCategory-section section-black background-ultra">
        <div class="col-md-12 col-lg-12 sect-logo hidden-xs hidden-sm">
            <div class=logo-block>
                <div class="col-md-12 col-lg-12 ultra-logo">
                    <img src="/images/ultrasingapore2018/ultra-logo-white.png" class="img-responsive">
                </div>
                <div class="col-md-12 col-lg-12 ultra-date">
                    <img src="/images/ultrasingapore2018/ultra-event-date-2.png" class="img-responsive">
                </div>
            </div>
        </div>
        <div class="col-md-12 col-lg-12 sect-count hidden-xs hidden-sm">
			<div class="col-md-6 col-lg-6"> 
                <div class="countdown-block">
                    {{-- <h2 class="text-center text-white open-sans h2-count">Event Starts in :</h2>
                    <div class="text-center" id="countdown"></div> --}}
                </div>
			</div>
			<div class="col-md-6 col-lg-6 text-center btn-wrapper"> 
                {{-- <h2 class="text-center text-white open-sans h2-count">TIER 3 TICKETS : <font style="color:red;">SELLING FAST</font></h2>
				<a class="btn ultraBuyBtnGreyYellow center-block disabled" href="#publicticket" role="button">BUY NOW</a> --}}
			</div>
        </div>
        
        <div class="col-xs-12 col-sm-12 sect-count hidden-md hidden-lg">
			<div class="col-xs-12 col-sm-12"> 
                <div class="countdown-block">
                    {{-- <h2 class="text-center text-white open-sans h2-count">Event Starts in :</h2>
				    <div class="text-center" id="countdown2"></div> --}}
                </div>
            </div>
			<div class="col-xs-12 col-sm-12 text-center btn-wrapper">
                {{-- <h2 class="text-center text-white open-sans h2-count-1">TIER 3 TICKETS : <font style="color:red;">SELLING FAST</font></h2>
				<a class="btn ultraBuyBtnGreyYellow center-block disabled" href="#publicticket" role="button">BUY NOW</a> --}}
			</div>
		</div>

	</section>
    <!-- /Banner Section -->


    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar section-white" style="padding-top: 0px;">

            {{-- <section class="pageCategory-section last">
                <div class="container tixPrice" id="publicticket">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10">
                            <div class="clearfix"></div>
                            <div class="tab-content"> --}}

                                {{-- ticket only tab --}}
                                {{-- <div id="publicticket_only" class="tab-pane fade in active">

                                    <!-- Public Sale -->
                                    <div class="col-xs-12">
                                        <p style="font-size: 24px; padding-top: 80px;" class="text-center"><strong>Public Sale</strong></p>
                                    </div>
                                    <div class="col-sm-12" id="singleticket">
                                        @foreach($ticket_only as $ticket)
                                            @if($ticket["Card Type"] == "Public") 
                                                <div class="col-sm-12 col-xs-12 ticketRow hidden-xs hidden-sm">
                                                    <div class="col-sm-2 text-center ticketTitle">
                                                        <div class="row">
                                                            <p class="tier">Tier 3</p>
                                                            <h1>{{$ticket["Ticket Type"]}}</h1>  
                                                        </div> 
                                                    </div>
                                                    <div class="col-sm-3 text-center ticketDay border-left">
                                                        <h6>{{ $ticket["Ticket Name"] }}</h6>
                                                    </div>
                                                    <div class="col-sm-4 ticketAdmission border-left">
                                                        <p style="float: left;">
                                                            {{ $ticket["Ticket Description"] }}<br><span class="smallnote">Very limited tickets. First come, first served.</span>
                                                        </p>
                                                        <div style="position: absolute;right: 10px;cursor: pointer;" data-toggle="modal" data-target="#infoModal{{ $ticket["Sorting ID"] }}">
                                                            <img src="images/ultrasingapore2018/i.png" width="20">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3 ticketBuy border-left">
                                                        <div class="col-sm-12 text-center">
                                                            <div class="prices">
                                                                <span class="strikethrougPrice">SGD{{$ticket["Normal Price Front End"]}}</span>
                                                                <span class="discountPrice">SGD{{$ticket["Private Sale Front End"]}}</span>
                                                            </div>
                                                            <a class="btn ultraBuyBtnGrey center-block disabled" href="{{ $ticket["Ticketserv URL"] }}" onclick="location.href=this.href&linkerParam;return false;">BUY NOW</a>
                                                            <small>Price excludes ticketing fee, Event Protect &amp; card charges</small>
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- mobile ticket ONLY card responsive -->
                                                <div class="col-xs-12 col-sm-6 hidden-lg hidden-md ticketCard"> --}}
                                                    {{-- <small class="discountTag"> 40% OFF</small> --}}
                                                    {{-- <div class="col-xs-12 card-body pd-0">
                                                        <div class="col-xs-12 cardTop">
                                                            <div class="col-xs-6 pd-0 ticketTitle">
                                                                <p class="ticketFlight">{{ $ticket["Ticket Name"] }}</p>
                                                                <p class="ticketTier">Tier 3 <span class="ticketCategory">{{ $ticket["Ticket Type"] }}</span></p>
                                                                <a data-toggle="modal" data-target="#infoModal{{ $ticket["Sorting ID"] }}"><img src="images/ultrasingapore2018/i.png" width="20"></a>
                                                            </div>
                                                            <div class="col-xs-6 ticketPrice">
                                                                @if(strlen($ticket["Private Sale Front End"]) !== 0)
                                                                   <p class="pull-right text-right">{{ $ticket["Private Sale Front End"] }}</p>
                                                                @else
                                                                    <p class="pull-right text-right">N/A</p>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="cardBottom col-xs-12 text-center">
                                                            <p>{{ $ticket["Ticket Description"] }}</p>
                                                            <a class="btn ultraBuyBtnGrey center-block disabled" href="{{ $ticket["Ticketserv URL"] }}">BUY NOW</a>
                                                            <small>Price excludes ticketing fee, Event Protect &amp; card charges</small>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        @endforeach
                                    </div>
                                    <!-- End Public Sale -->
                            </div>
                        </div>
                    </div>
                </div>
            </section> --}}


            <section class="pageCategory-section last">
                <div class="container tixPrice" id="myticket">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10">
                            <div class="text-center col-sm-12 col-xs-12">
                                <h1 class="subSecTitle"><strong>TICKETS</strong></h1>
                                {{-- <p>This Exclusive Sale is for DBS Live Fresh cardmembers and DBS/POSB cardmembers. We reserve the right to cancel and refund any transactions that were not purchased with a DBS card.</p> --}}
                                 {{-- <p>DBS/POSB Cardholders Promo: 2 Free drinks per 2-day GA purchased (limited to the first 1,000 tickets)<br/>
                                 Live Fresh Promo: Free upgrade to 2-day PGA per 2-day GA purchased (limited to the first 500 tickets)<br/>
                                 <a href="https://intercom.help/airasia-redtix/your-guide-to-ultra-singapore-2018">Click here to find out more</a></p> --}}
                            </div>
                            <div class="clearfix"></div>

                            {{-- dekstop tab--}}
                            <div class="col-sm-12 hidden-xs hidden-md hidden-sm">
                                <div class="col-sm-5">
                                    <button data-toggle="tab" href="#ticket_only" class="tixOptBtn btn active"><i class="fa fa-ticket"></i>TICKET ONLY</button>
                                </div>
                                <div class="col-sm-7">
                                    <button data-toggle="tab" href="#ticket_flight" class="tixOptBtn btn"><i class="fa fa-ticket"></i><i class="fa fa-plane"></i>TICKET + FLIGHT</button>
                                </div>
                                {{-- <div class="col-sm-5">
                                    <button data-toggle="tab" href="" class="tixOptBtn btn disabled"><i class="fa fa-ticket"></i><i class="fa fa-plane"></i><i class="fa fa-building-o"></i>TICKET + FLIGHT + HOTEL</button>
                                </div> --}}
                            </div>

                            {{-- mobile tab --}}
                            <div class="col-xs-12 hidden-lg pd-0">
                               <div class="col-xs-12 ps-9">
                                    <button data-toggle="tab" href="#ticket_only" class="tixOptBtn btn">
                                        <i class="fa fa-ticket"></i>
                                        <p>TICKET ONLY</p>
                                    </button>
                                </div>
                                <div class="col-xs-12 ps-9">
                                    <button data-toggle="tab" href="#ticket_flight" class="tixOptBtn btn">
                                        <i class="fa fa-ticket"></i>
                                        <i class="fa fa-plane"></i>
                                        <p>TICKET + FLIGHT</p>
                                    </button>
                                </div>
                                {{-- <div class="col-xs-5 ps-9">
                                    <button data-toggle="tab" href="" class="tixOptBtn btn disabled">
                                        <i class="fa fa-ticket"></i>
                                        <i class="fa fa-plane"></i>
                                        <i class="fa fa-building-o"></i>
                                        <p>TICKET + FLIGHT + HOTEL</p>
                                    </button>
                                </div> --}}
                            </div>

                            <div class="tab-content">

                                {{-- ticket only tab --}}
                                <div id="ticket_only" class="tab-pane fade in active">
                                    
                                    {{-- <div class="col-xs-12">
                                        <p style="font-size: 24px; padding-top: 80px;" class="text-center"><strong>Registered Users Only</strong></p>
                                    </div>
                                    <div class="col-sm-12">
                                        @foreach($ticket_only as $ticket)
                                            @if($ticket["Card Type"] == "Registered User") 
                                                <div class="col-sm-12 col-xs-12 ticketRow hidden-xs hidden-sm">
                                                <small class="discountTag"> SOLD OUT</small>
                                                    <div class="col-sm-2 text-center ticketTitle">
                                                        <div class="row">
                                                            <p class="tier">Tier 1</p>
                                                            <h1>{{$ticket["Ticket Type"]}}</h1>  
                                                        </div> 
                                                    </div>
                                                    <div class="col-sm-3 text-center ticketDay border-left">
                                                        <h6>2 DAY COMBO TICKET</h6>
                                                    </div>
                                                    <div class="col-sm-4 ticketAdmission border-left">
                                                        <p style="float: left;">
                                                            {{ $ticket["Ticket Description"] }}<br><span class="smallnote">Very limited tickets. First come, first served.</span>
                                                        </p>
                                                        <div style="position: absolute;right: 10px;cursor: pointer;" data-toggle="modal" data-target="#infoModal{{ $ticket["Sorting ID"] }}">
                                                            <img src="images/ultrasingapore2018/i.png" width="20">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3 ticketBuy border-left">
                                                        <div class="col-sm-12 text-center">
                                                            <div class="prices">
                                                                <span class="strikethrougPrice">SGD{{$ticket["Normal Price Front End"]}}</span>
                                                                <span class="discountPrice">SGD{{$ticket["Private Sale Front End"]}}</span>
                                                            </div>    
                                                            <a class="btn ultraBuyBtnGreyGrey center-block disabled" data-toggle="modal" data-target="#buyModalRegistered{{ $ticket["Sorting ID"] }}">BUY NOW</a>
                                                            <small>Price excludes ticketing fee, Event Protect &amp; card charges</small>
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- mobile ticket ONLY card responsive -->
                                                <div class="col-xs-12 col-sm-6 hidden-lg hidden-md ticketCard">
                                                    <small class="discountTag"> SOLD OUT</small>
                                                    <div class="col-xs-12 card-body pd-0">
                                                        <div class="col-xs-12 cardTop">
                                                            <div class="col-xs-6 pd-0 ticketTitle">
                                                                <p class="ticketFlight">{{ $ticket["Ticket Name"] }}</p>
                                                                <p class="ticketTier">Tier 1<span class="ticketCategory">{{ $ticket["Ticket Type"] }}</span></p>
                                                                <a data-toggle="modal" data-target="#infoModal{{ $ticket["Sorting ID"] }}"><img src="images/ultrasingapore2018/i.png" width="20"></a>
                                                            </div>
                                                            <div class="col-xs-6 ticketPrice">
                                                                @if(strlen($ticket["Private Sale Front End"]) !== 0)
                                                                   <p class="pull-right text-right">{{ $ticket["Private Sale Front End"] }}</p>
                                                                @else
                                                                    <p class="pull-right text-right">N/A</p>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="cardBottom col-xs-12 text-center">
                                                            <p>{{ $ticket["Ticket Description"] }}</p>
                                                            <a class="btn ultraBuyBtnGreyGrey center-block disabled" data-toggle="modal" data-target="#buyModalRegistered{{ $ticket["Sorting ID"] }}">BUY NOW</a>
                                                            <small>Price excludes ticketing fee, Event Protect &amp; card charges</small>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        @endforeach
                                    </div> --}}


                                    <!-- Public Sale -->
                                    <div class="col-xs-12">  
                                        <p style="font-size: 24px; padding-top: 80px;" class="text-center"><strong>Public Sale</strong></p>
                                    </div>
                                    <div class="col-sm-12" id="singleticket">
                                        @foreach($ticket_only as $ticket)
                                            @if($ticket["Card Type"] == "Public") 
                                                <div class="col-sm-12 col-xs-12 ticketRow hidden-xs hidden-sm">
                                                    <div class="col-sm-2 text-center ticketTitle">
                                                        <div class="row">
                                                            <p class="tier">Tier 3</p>
                                                            <h1>{{$ticket["Ticket Type"]}}</h1>  
                                                        </div> 
                                                    </div>
                                                    <div class="col-sm-3 text-center ticketDay border-left">
                                                        <h6>{{ $ticket["Ticket Name"] }}</h6>
                                                    </div>
                                                    <div class="col-sm-4 ticketAdmission border-left">
                                                        <p style="float: left;">
                                                            {{ $ticket["Ticket Description"] }}<br><span class="smallnote">Very limited tickets. First come, first served.</span>
                                                        </p>
                                                        <div style="position: absolute;right: 10px;cursor: pointer;" data-toggle="modal" data-target="#infoModal{{ $ticket["Sorting ID"] }}">
                                                            <img src="images/ultrasingapore2018/i.png" width="20">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3 ticketBuy border-left">
                                                        <div class="col-sm-12 text-center">
                                                            <div class="prices">
                                                                <span class="strikethrougPrice">SGD{{$ticket["Normal Price Front End"]}}</span>
                                                                <span class="discountPrice">SGD{{$ticket["Private Sale Front End"]}}</span>
                                                            </div>
                                                            <a class="btn ultraBuyBtnGrey center-block disabled" href="{{ $ticket["Ticketserv URL"] }}" onclick="gtag_report_conversion(); location.href=this.href&linkerParam;return false;">BUY NOW</a>
                                                            <small>Price excludes ticketing fee, Event Protect &amp; card charges</small>
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- mobile ticket ONLY card responsive -->
                                                <div class="col-xs-12 col-sm-6 hidden-lg hidden-md ticketCard">
                                                    {{-- <small class="discountTag"> 40% OFF</small> --}}
                                                    <div class="col-xs-12 card-body pd-0">
                                                        <div class="col-xs-12 cardTop">
                                                            <div class="col-xs-6 pd-0 ticketTitle">
                                                                <p class="ticketFlight">{{ $ticket["Ticket Name"] }}</p>
                                                                <p class="ticketTier">Tier 3 <span class="ticketCategory">{{ $ticket["Ticket Type"] }}</span></p>
                                                                <a data-toggle="modal" data-target="#infoModal{{ $ticket["Sorting ID"] }}"><img src="images/ultrasingapore2018/i.png" width="20"></a>
                                                            </div>
                                                            <div class="col-xs-6 ticketPrice">
                                                                @if(strlen($ticket["Private Sale Front End"]) !== 0)
                                                                   <p class="pull-right text-right">{{ $ticket["Private Sale Front End"] }}</p>
                                                                @else
                                                                    <p class="pull-right text-right">N/A</p>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="cardBottom col-xs-12 text-center">
                                                            <p>{{ $ticket["Ticket Description"] }}</p>
                                                            <a class="btn ultraBuyBtnGrey center-block disabled" href="{{ $ticket["Ticketserv URL"] }}" onclick="gtag_report_conversion();">BUY NOW</a>
                                                            <small>Price excludes ticketing fee, Event Protect &amp; card charges</small>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        @endforeach
                                    </div>
                                    <!-- End Public Sale -->


                                    <!-- Public Sale -->
                                    {{-- <div class="col-xs-12">
                                        <p style="font-size: 24px; padding-top: 80px;" class="text-center"><strong>Public Sale</strong></p>
                                    </div>
                                    <div class="col-sm-12" id="singleticket">
                                        @foreach($ticket_only as $ticket)
                                            @if($ticket["Card Type"] == "Public") 
                                                <div class="col-sm-12 col-xs-12 ticketRow hidden-xs hidden-sm">
                                                    <div class="col-sm-2 text-center ticketTitle">
                                                        <div class="row">
                                                            <p class="tier">Tier 3</p>
                                                            <h1>{{$ticket["Ticket Type"]}}</h1>  
                                                        </div> 
                                                    </div>
                                                    <div class="col-sm-3 text-center ticketDay border-left">
                                                        <h6>{{ $ticket["Ticket Name"] }}</h6>
                                                    </div>
                                                    <div class="col-sm-4 ticketAdmission border-left">
                                                        <p style="float: left;">
                                                            {{ $ticket["Ticket Description"] }}<br><span class="smallnote">Very limited tickets. First come, first served.</span>
                                                        </p>
                                                        <div style="position: absolute;right: 10px;cursor: pointer;" data-toggle="modal" data-target="#infoModal{{ $ticket["Sorting ID"] }}">
                                                            <img src="images/ultrasingapore2018/i.png" width="20">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3 ticketBuy border-left">
                                                        <div class="col-sm-12 text-center">
                                                            <div class="prices">
                                                                <span class="strikethrougPrice">SGD{{$ticket["Normal Price Front End"]}}</span>
                                                                <span class="discountPrice">SGD{{$ticket["Private Sale Front End"]}}</span>
                                                            </div>
                                                            <a class="btn ultraBuyBtnGrey center-block disabled" href="{{ $ticket["Ticketserv URL"] }}" onclick="location.href=this.href&linkerParam;return false;">BUY NOW</a>
                                                            <small>Price excludes ticketing fee, Event Protect &amp; card charges</small>
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- mobile ticket ONLY card responsive -->
                                                <div class="col-xs-12 col-sm-6 hidden-lg hidden-md ticketCard">
                                                    <div class="col-xs-12 card-body pd-0">
                                                        <div class="col-xs-12 cardTop">
                                                            <div class="col-xs-6 pd-0 ticketTitle">
                                                                <p class="ticketFlight">{{ $ticket["Ticket Name"] }}</p>
                                                                <p class="ticketTier">Tier 3 <span class="ticketCategory">{{ $ticket["Ticket Type"] }}</span></p>
                                                                <a data-toggle="modal" data-target="#infoModal{{ $ticket["Sorting ID"] }}"><img src="images/ultrasingapore2018/i.png" width="20"></a>
                                                            </div>
                                                            <div class="col-xs-6 ticketPrice">
                                                                @if(strlen($ticket["Private Sale Front End"]) !== 0)
                                                                   <p class="pull-right text-right">{{ $ticket["Private Sale Front End"] }}</p>
                                                                @else
                                                                    <p class="pull-right text-right">N/A</p>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="cardBottom col-xs-12 text-center">
                                                            <p>{{ $ticket["Ticket Description"] }}</p>
                                                            <a class="btn ultraBuyBtnGrey center-block disabled" href="{{ $ticket["Ticketserv URL"] }}">BUY NOW</a>
                                                            <small>Price excludes ticketing fee, Event Protect &amp; card charges</small>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        @endforeach
                                    </div> --}}
                                    <!-- End Public Sale -->

                                    <div class="col-xs-12" id="DBS">
                                        <p style="font-size: 24px; padding-top: 80px;" class="text-center"><img src="images/ultrasingapore2018/livefresh2.png" height="25"> & <img src="images/ultrasingapore2018/dbs_logo.png" height="25"> <strong> Cardmember</strong></p>
                                        {{-- <p class="text-center">DBS/POSB Cardholders Promo: 2 Free drinks per 2-day GA purchased  (limited to the first 1,000 tickets)<br />
                                        Live Fresh Promo: Free upgrade to 2-day PGA per 2-day GA purchased (limited to the first 500 tickets)<br /></p> --}}
                                        <p style="font-size:12px;" class="text-center"><font color="#999999"><strike>Live Fresh Promo: 2-day GA tickets at S$166.50 + 1 Free Vodka Red bull + upgrade to PGA! (Complimentary Upgrade up to first 500)</strike></font><font color="red"> Fully Redeemed!</font><br/   >
                                        <font color="#999999"><strike>DBS/POSB Cardholders Promo: 1 Free Vodka Red bull per 2-day GA purchased (limited to the first 2,000 tickets)</strike></font><font color="red"> Fully Redeemed!</font></p>
                                        {{-- <a href="https://intercom.help/airasia-redtix/your-guide-to-ultra-singapore-2018">Click here to find out more</a> --}}
                                        <p class="text-center">Don't have the Card? Get your DBS Live Fresh Card here: <a href="http://dbs.com.sg/livefresh">dbs.com.sg/livefresh</a></p>
                                    </div>

                                    <div class="col-sm-12">
                                        @foreach($ticket_only as $ticket)
                                            @if($ticket["Card Type"] == "LF & DBS") 
                                                <div class="col-sm-12 col-xs-12 ticketRow hidden-xs hidden-sm">
                                                    <div class="col-sm-2 text-center ticketTitle">
                                                        <div class="row">
                                                            <p class="tier">Tier 3</p>
                                                            <h1>{{$ticket["Ticket Type"]}}</h1>  
                                                        </div> 
                                                    </div>
                                                    <div class="col-sm-3 text-center ticketDay border-left">
                                                        <h6>2 DAY COMBO TICKET</h6>
                                                    </div>
                                                    <div class="col-sm-4 ticketAdmission border-left">
                                                        <p style="float: left;">
                                                            {{ $ticket["Ticket Description"] }}<br><span class="smallnote">Very limited tickets. First come, first served.</span>
                                                        </p>
                                                        <div style="position: absolute;right: 10px;cursor: pointer;" data-toggle="modal" data-target="#infoModal{{ $ticket["Sorting ID"] }}">
                                                            <img src="images/ultrasingapore2018/i.png" width="20">
                                                        </div>

                                                    </div>
                                                    <div class="col-sm-3 ticketBuy border-left">
                                                        <div class="col-sm-12 text-center">
                                                            <div class="prices">
                                                                <span class="strikethrougPrice">SGD{{$ticket["Normal Price Front End"]}}</span>
                                                                <span class="discountPrice">SGD{{$ticket["Private Sale Front End"]}}</span>
                                                            </div>    
                                                            <a class="btn ultraBuyBtnGrey center-block disabled" data-toggle="modal" data-target="#buyModal{{ $ticket["Sorting ID"] }}">BUY NOW</a>
                                                            {{-- <small>Price excludes ticketing fee, Event Protect insurance &amp; credit card charges</small> --}}
                                                            <small>Price excludes ticketing fee, Event Protect &amp; card charges</small>
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- mobile ticket ONLY card responsive -->
                                                <div class="col-xs-12 col-sm-6 hidden-lg hidden-md ticketCard">
                                                    {{-- <small class="discountTag"> 40% OFF</small> --}}
                                                    <div class="col-xs-12 card-body pd-0">
                                                        <div class="col-xs-12 cardTop">
                                                            <div class="col-xs-6 pd-0 ticketTitle">
                                                                <p class="ticketFlight">{{ $ticket["Ticket Name"] }}</p>
                                                                <p class="ticketTier">Tier 3 <span class="ticketCategory">{{ $ticket["Ticket Type"] }}</span></p>
                                                                <a data-toggle="modal" data-target="#infoModal{{ $ticket["Sorting ID"] }}"><img src="images/ultrasingapore2018/i.png" width="20"></a>
                                                                {{-- <div style="position: absolute;right: -80px;top: 0;cursor: pointer;" data-toggle="modal" data-target="#infoModal{{ $ticket["Sorting ID"] }}">
                                                                    <img src="images/ultrasingapore2018/i.png" width="20">
                                                                </div> --}}
                                                            </div>
                                                            <div class="col-xs-6 ticketPrice">
                                                                @if(strlen($ticket["Private Sale Front End"]) !== 0)
                                                                   {{-- <p class="pull-right text-right">{{ $ticket["Private Sale Front End"] }}<br><small>10 tickets left at this tier</small></p> --}}
                                                                   <p class="pull-right text-right">{{ $ticket["Private Sale Front End"] }}</p>
                                                                @else
                                                                    <p class="pull-right text-right">N/A</p>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="cardBottom col-xs-12 text-center">
                                                            <p>{{ $ticket["Ticket Description"] }}</p>
                                                            {{-- @if(strlen($ticket["Ticketserv URL"]) !== 0) --}}
                                                                <a class="btn ultraBuyBtnGrey center-block disabled" data-toggle="modal" data-target="#buyModal{{ $ticket["Sorting ID"] }}">BUY NOW</a>
                                                                {{-- <small>Price excludes ticketing fee, Event Protect insurance &amp; credit card charges</small> --}}
                                                                <small>Price excludes ticketing fee, Event Protect &amp; card charges</small>
                                                                {{-- <a class="btn ultraBuyBtnGrey center-block disabled" id="buyButton" datetime="Feb 10 2018 00:00:00 GMT+0800" target="_blank" href="{{ $ticket["Ticketserv URL"] }}">BUY NOW</a> --}}
                                                            {{-- @endif --}}
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        @endforeach
                                    </div>



                                 {{--    <div class="col-xs-12">
                                        <p style="font-size: 24px; padding-top: 80px;" class="text-center hidden-xs"><img class="livefresh" src="images/ultrasingapore2018/livefresh2.png"><br/><strong> Cardmember</strong></p>
                                        <p style="font-size: 24px; padding-top: 80px;" class="text-center hidden-lg hidden-md hidden-sm"><img class="livefresh img-responsive center-block" src="images/ultrasingapore2018/livefresh2.png"><br/><strong> Cardmember</strong></p>
                                    </div>
                                    <div class="col-sm-12">
                                        @foreach($ticket_only as $ticket)
                                            @if($ticket["Card Type"] == "LF")        --}}                                 
                                        {{--         @include('layouts.components.ultraTicketOnly') --}}
                                                {{-- ticket only --}}
                                                {{-- <div class="col-sm-12 col-xs-12 ticketRow hidden-xs hidden-sm"> --}}
                                                    {{-- <small class="discountTag"> 40% OFF</small> --}}
                                                    {{-- <div class="col-sm-2 text-center ticketTitle">
                                                        <div class="row"> --}}
                                                            {{-- <p>Premium</p> --}}
                                                            {{-- <div class="clearfix" style="height: 10px;">&nbsp;</div>
                                                            <h1>{{ $ticket["Ticket Type"] }}</h1>    
                                                        </div> 
                                                    </div>
                                                    <div class="col-sm-3 text-center ticketDay border-left">
                                                        <h6>{{ $ticket["Ticket Name"] }}</h6>
                                                    </div>
                                                    <div class="col-sm-4 ticketAdmission border-left">
                                                        <p>{{ $ticket["Ticket Description"] }}</p>
                                                    </div>
                                                    <div class="col-sm-3 ticketBuy border-left">
                                                        <div class="col-sm-12 text-center">
                                                            @if(strlen($ticket["Private Sale Front End"]) !== 0)
                                                                <h6>{{ $ticket["Private Sale Front End"] }}</h6> --}}
                                                                {{-- @if(strlen($ticket["Ticketserv URL"]) !== 0) --}}
                                                                {{-- <a class="btn ultraBuyBtnGrey center-block disabled" data-toggle="modal" data-target="#buyModal{{ $ticket["Sorting ID"] }}">BUY NOW</a> --}}
                                                                {{-- <a class="btn ultraBuyBtnGrey center-block disabled" href="{{ $ticket["Ticketserv URL"] }}">BUY NOW</a> --}}
                                                                {{-- @endif --}}
                                                             {{--    <p class="small" style="line-height: 1.3; font-size: 80%;">Ticketing fee, event insurance & credit card charges will be added at the checkout basket</p>
                                                            @else
                                                                <div class="clearfix" style="height:30px;">&nbsp;</div>
                                                                <h6>N/A</h6>
                                                            @endif --}}
                                                           {{--  <h6>{{ $ticket["Private Sale Front End"] }}</h6>
                                                            <a class="btn ultraBuyBtnGrey center-block disabled" id="buyButton" datetime="Feb 10 2018 00:00:00 GMT+0800" target="_blank" href="{{ $ticket["Ticketserv URL"] }}">BUY NOW</a>
                                                            <small>10 tickets left at this tier</small> --}}
                                             {{--            </div>
                                                    </div>
                                                </div> --}}
                                                <!-- mobile ticket ONLY card responsive -->
                                                {{-- <div class="col-xs-12 col-sm-6 hidden-lg hidden-md ticketCard"> --}}
                                                    {{-- <small class="discountTag"> 40% OFF</small> --}}
                                                   {{--  <div class="col-xs-12 card-body pd-0">
                                                        <div class="col-xs-12 cardTop">
                                                            <div class="col-xs-6 pd-0 ticketTitle">
                                                                <p class="ticketFlight">{{ $ticket["Ticket Name"] }}</p>
                                                                <p class="ticketTier">Tier 1<span class="ticketCategory">{{ $ticket["Ticket Type"] }}</span></p>
                                                            </div>
                                                            <div class="col-xs-6 ticketPrice">
                                                                @if(strlen($ticket["Private Sale Front End"]) !== 0) --}}
                                                                   {{-- <p class="pull-right text-right">{{ $ticket["Private Sale Front End"] }}<br><small>10 tickets left at this tier</small></p> --}}
                                                                 {{--   <p class="pull-right text-right">{{ $ticket["Private Sale Front End"] }}</p>
                                                                @else
                                                                    <p class="pull-right text-right">N/A</p>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="cardBottom col-xs-12 text-center">
                                                            <p>{{ $ticket["Ticket Description"] }}</p> --}}
                                                            {{-- @if(strlen($ticket["Ticketserv URL"]) !== 0) --}}
                                                              {{--   <a class="btn ultraBuyBtnGrey center-block disabled" data-toggle="modal" data-target="#buyModal{{ $ticket["Sorting ID"] }}">BUY NOW</a> --}}
                                                                {{-- <a class="btn ultraBuyBtnGrey center-block disabled" id="buyButton" datetime="Feb 10 2018 00:00:00 GMT+0800" target="_blank" href="{{ $ticket["Ticketserv URL"] }}">BUY NOW</a> --}}
                                                            {{-- @endif --}}
                                                {{--         </div>
                                                    </div>
                                                </div>
                                            @endif
                                        @endforeach
                                    </div>
                                    <div class="col-xs-12">
                                        <p style="font-size: 24px; padding-top: 80px;" class="text-center hidden-xs"><img src="images/ultrasingapore2018/dbs_logo.png"><br/><strong>POSB/DBS Cardmember</strong></p>
                                        <p style="font-size: 24px; padding-top: 80px;" class="text-center hidden-lg hidden-md hidden-sm"><img class="img-responsive center-block" src="images/ultrasingapore2018/dbs_logo.png"><br /><strong>POSB/DBS Cardmember</strong></p>
                                    </div>
                                    <div class="col-sm-12">
                                     @foreach($ticket_only as $ticket)
                                            @if($ticket["Card Type"] == "DBS")                         --}}                
                                            {{--  @include('layouts.components.ultraTicketOnly') --}}
                                            {{-- ticket only --}}
                                            {{-- <div class="col-sm-12 col-xs-12 ticketRow hidden-xs hidden-sm"> --}}
                                                {{-- <small class="discountTag"> 40% OFF</small> --}}
                                                {{-- <div class="col-sm-2 text-center ticketTitle"> --}}
                                                    {{-- <div class="row"> --}}
                                                        {{-- <p>Premium</p> --}}
                                                        {{-- <div class="clearfix" style="height: 10px;">&nbsp;</div>
                                                        <h1>{{ $ticket["Ticket Type"] }}</h1>    
                                                    </div> 
                                                </div>
                                                <div class="col-sm-3 text-center ticketDay border-left">
                                                    <h6>{{ $ticket["Ticket Name"] }}</h6>
                                                </div>
                                                <div class="col-sm-4 ticketAdmission border-left">
                                                    <p>{{ $ticket["Ticket Description"] }}</p>
                                                </div>
                                                <div class="col-sm-3 ticketBuy border-left">
                                                    <div class="col-sm-12 text-center">
                                                        @if(strlen($ticket["Private Sale Front End"]) !== 0)
                                                            <h6>{{ $ticket["Private Sale Front End"] }}</h6> --}}
                                                            {{-- @if(strlen($ticket["Ticketserv URL"]) !== 0) --}}
                                                          {{--   <a class="btn ultraBuyBtnGrey center-block disabled" data-toggle="modal" data-target="#buyModal{{ $ticket["Sorting ID"] }}">BUY NOW</a> --}}
                                                            {{-- <a class="btn ultraBuyBtnGrey center-block disabled" href="{{ $ticket["Ticketserv URL"] }}">BUY NOW</a> --}}
                                                            {{-- @endif --}}
                                                          {{--   <p class="small" style="line-height: 1.3; font-size: 80%;">Ticketing fee, event insurance & credit card charges will be added at the checkout basket</p>
                                                        @else
                                                            <div class="clearfix" style="height:30px;">&nbsp;</div>
                                                            <h6>N/A</h6>
                                                        @endif --}}
                                                       {{--  <h6>{{ $ticket["Private Sale Front End"] }}</h6>
                                                        <a class="btn ultraBuyBtnGrey center-block disabled" id="buyButton" datetime="Feb 10 2018 00:00:00 GMT+0800" target="_blank" href="{{ $ticket["Ticketserv URL"] }}">BUY NOW</a>
                                                        <small>10 tickets left at this tier</small> --}}
                                       {{--              </div>
                                                </div>
                                            </div> --}}
                                            <!-- mobile ticket ONLY card responsive -->
                                            {{-- <div class="col-xs-12 col-sm-6 hidden-lg hidden-md ticketCard"> --}}
                                                {{-- <small class="discountTag"> 40% OFF</small> --}}
                                              {{--   <div class="col-xs-12 card-body pd-0">
                                                    <div class="col-xs-12 cardTop">
                                                        <div class="col-xs-6 pd-0 ticketTitle">
                                                            <p class="ticketFlight">{{ $ticket["Ticket Name"] }}</p>
                                                            <p class="ticketTier">Tier 1<span class="ticketCategory">{{ $ticket["Ticket Type"] }}</span></p>
                                                        </div>
                                                        <div class="col-xs-6 ticketPrice">
                                                            @if(strlen($ticket["Private Sale Front End"]) !== 0)
                                                            <h6>{{ $ticket["Private Sale Front End"] }}</h6>    --}}
                                                            {{-- <p class="pull-right text-right">{{ $ticket["Private Sale Front End"] }}<br><small>10 tickets left at this tier</small></p> --}}
                                                            {{-- @else --}}
                                                     {{--            <p class="pull-right text-right">N/A</p>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="cardBottom col-xs-12 text-center">
                                                        <p>{{ $ticket["Ticket Description"] }}</p> --}}
                                                        {{-- @if(strlen($ticket["Ticketserv URL"]) !== 0) --}}
                                                          {{--    <a class="btn ultraBuyBtnGrey center-block disabled" data-toggle="modal" data-target="#buyModal{{ $ticket["Sorting ID"] }}">BUY NOW</a> --}}
                                                            {{-- <a class="btn ultraBuyBtnGrey center-block disabled" id="buyButton" datetime="Feb 10 2018 00:00:00 GMT+0800" target="_blank" href="{{ $ticket["Ticketserv URL"] }}">BUY NOW</a> --}}
                                                        {{-- @endif --}}
                                             {{--        </div>
                                                </div>
                                            </div> --}}
                                           {{--  @endif --}}
                                            {{-- @if($ticket["Card Type"] == "POSB")                                        
                                                @include('layouts.components.ultraTicketOnly')
                                            @endif  --}}
                                       {{--  @endforeach
                                    </div> --}}
                                </div>

                                {{-- ticket + flight tab --}}
                               <div id="ticket_flight" class="tab-pane fade">
                                    <div class="col-sm-12 col-xs-12" style="margin-top: 40px;">
                                        <p>Select the country you will be flying from*:</p>
                                        <select class="selectpicker" id="select-country">
                                            <option value="1">KUALA LUMPUR</option>
                                            <option value="2">KUCHING</option>
                                            <option value="3">PENANG</option>
                                            <option value="4">BANGKOK</option>
                                            <option value="5">JAKARTA</option>
                                        {{--    <option value="6">PERTH</option>
                                            <option value="7">MELBOURNE</option> --}}
                                        </select>
                                    </div>
                                    
                                    <div id="country-1" class="tab-content flightTab">
                                        @foreach($ticket_flight as $ticket)
                                            @if($ticket["From City"] == "Kuala Lumpur") 
                                                @include('layouts.components.ultraFlight')
                                            @endif
                                        @endforeach
                                    </div>
                                    <div id="country-2" class="tab-content flightTab">
                                        @foreach($ticket_flight as $ticket)
                                            @if($ticket["From City"] == "Kuching")
                                                @include('layouts.components.ultraFlight')
                                            @endif
                                        @endforeach
                                    </div>
                                    <div id="country-3" class="tab-content flightTab">
                                        @foreach($ticket_flight as $ticket)
                                            @if($ticket["From City"] == "Penang")
                                                @include('layouts.components.ultraFlight')
                                            @endif
                                        @endforeach
                                    </div>
                                    <div id="country-4" class="tab-content flightTab">
                                        @foreach($ticket_flight as $ticket)
                                            @if($ticket["From City"] == "Bangkok")
                                                @include('layouts.components.ultraFlight')
                                            @endif
                                        @endforeach
                                    </div>
                                    <div id="country-5" class="tab-content flightTab">
                                        @foreach($ticket_flight as $ticket)
                                            @if($ticket["From City"] == "Jakarta")
                                                @include('layouts.components.ultraFlight')
                                            @endif
                                        @endforeach
                                    </div>
                                    <div id="country-6" class="tab-content flightTab">
                                        @foreach($ticket_flight as $ticket)
                                            @if($ticket["From City"] == "Perth")
                                                @include('layouts.components.ultraFlight')
                                            @endif
                                        @endforeach
                                    </div>
                                    <div id="country-7" class="tab-content flightTab">
                                        @foreach($ticket_flight as $ticket)
                                            @if($ticket["From City"] == "Melbourne")
                                                @include('layouts.components.ultraFlight')
                                            @endif
                                        @endforeach
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>



            <section class="pageCategory-section section-black last">
                <div class="container tixPrice" id="usgIntroduction">
                <div class="row">
                        <div class="col-sm-offset-1 col-sm-10" style="text-align:center;">
                            <h6 style="color:#fff;">Ultra Singapore 2018</h6>
                            <p>Friday, 15 June 2018 - Saturday, 16 June 2018</p>
                            <p>Ultra Park, 1 Bayfront Avenue, Singapore</p>
                            <br>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10">
                            <!-- <img class="whiteLogo center-block img-responsive" src="images/ultrasingapore2018/logo-3.png"> -->
                            <h6 class="text-center" style="margin-top: 40px; color: #fff;">ESCAPE THE ORDINARY. BE ULTRA.</h6>
                            <p>The music festival that has been taking the whole world by storm is landing at Singapore. This will be Ultra’s 3rd consecutive year partying at the Lion City. Hordes of ‘Ultranauts’ will travel from all over the globe to enjoy some world-class entertainment and soak up the ever stunning destination while they’re at it. Be ready for 2 days of explosive energy from the people, to the DJs on stage.</p>
                            <p>Wave your national flag and tell 45,000 revellers which part of the earth are you from. Enjoy the best of EDM from the melting pot of culture for Ultra. Leave all your worries behind when the beat drops, and enjoy the unbelievable energy from the Ultra family.</p>
                            <p>Ultra is the biggest party in the world, and you Ultranauts are the stars. Lights will guide you, music will revive you and fireworks will captivate you at beautiful Singapore with Ultra.</p>
                            <div class="embed-responsive embed-responsive-16by9" style="margin-top: 40px; margin-bottom: 40px;">
                                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/oN9uCVPBB3Q" frameborder="0" allow="encrypted-media" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="pageCategory-section-alt text-center section-black-alt last">
                <div class="col-sm-12 text-center section-black-alt">
                <div class="row text-center section-black-alt">
                        {{-- desktop --}}
                    <div class="col-xs-12 col-md-12 col-sm-12 hidden-xs hidden-md hidden-sm">
                        <h1><strong>Ultra Singapore 2018 Lineup</strong></h1>
                    </div>
                    <div class="col-xs-2 col-md-2 col-sm-2 hidden-xs hidden-md hidden-sm">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-above-beyond.jpg" style="width:auto; height: auto;">
                        <p>Above &amp; Beyond</p>
                    </div>
                    <div class="col-xs-2 col-md-2 col-sm-2 hidden-xs hidden-md hidden-sm">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-afrojack.jpg" style="width:auto; height: auto;">
                        <p>Afrojack</p>
                    </div>
                    <div class="col-xs-2 col-md-2 col-sm-2 hidden-xs hidden-md hidden-sm">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-aly-fila.jpg" style="width:auto; height: auto;">
                        <p>Aly &amp; Fila</p>
                    </div>
                    <div class="col-xs-2 col-md-2 col-sm-2 hidden-xs hidden-md hidden-sm">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-andrewrayel.jpg" style="width:auto; height: auto;">
                        <p>Andrew Rayel</p>
                    </div>
                    <div class="col-xs-2 col-md-2 col-sm-2 hidden-xs hidden-md hidden-sm">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-cesqeaux.jpg" style="width:auto; height: auto;">
                        <p>Cesqeaux</p>
                    </div>
                    <div class="col-xs-2 col-md-2 col-sm-2 hidden-xs hidden-md hidden-sm">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-djsnake.jpg" style="width:auto; height: auto;">
                        <p>DJ Snake</p>
                    </div>
                </div>
                <div class="row text-center section-black-alt">
                    <div class="col-xs-2 col-md-2 col-sm-2 hidden-xs hidden-md hidden-sm">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-feddelegrand.jpg" style="width:auto; height: auto;">
                        <p>Fedde Le Grand</p>
                    </div>
                    <div class="col-xs-2 col-md-2 col-sm-2 hidden-xs hidden-md hidden-sm">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-ggmagree.jpg" style="width:auto; height: auto;">
                        <p>GG Magree</p>
                    </div>
                    <div class="col-xs-2 col-md-2 col-sm-2 hidden-xs hidden-md hidden-sm">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-illenium.jpg" style="width:auto; height: auto;">
                        <p>Illenium</p>
                    </div>
                    <div class="col-xs-2 col-md-2 col-sm-2 hidden-xs hidden-md hidden-sm">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-joyride.jpg" style="width:auto; height: auto;">
                        <p>JOYRYDE</p>
                    </div>
                    <div class="col-xs-2 col-md-2 col-sm-2 hidden-xs hidden-md hidden-sm">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-kriskrosamsterdam.jpg" style="width:auto; height: auto;">
                        <p>Kris Kross Amsterdam</p>
                    </div>
                    <div class="col-xs-2 col-md-2 col-sm-2 hidden-xs hidden-md hidden-sm">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-mykris.jpg" style="width:auto; height: auto;">
                        <p>Mykris</p>
                    </div>
                </div>
                <div class="row text-center section-black-alt">
                    <div class="col-xs-2 col-md-2 col-sm-2 hidden-xs hidden-md hidden-sm">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-myrne.jpg" style="width:auto; height: auto;">
                        <p>MYRNE</p>
                    </div>
                    <div class="col-xs-2 col-md-2 col-sm-2 hidden-xs hidden-md hidden-sm">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-nervo.jpg" style="width:auto; height: auto;">
                        <p>NERVO</p>
                    </div>
                    <div class="col-xs-2 col-md-2 col-sm-2 hidden-xs hidden-md hidden-sm">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-gudvibrations.jpg" style="width:auto; height: auto;">
                        <p>NGHTMRE + SLANDER present Gud Vibrations</p>
                    </div>
                    <div class="col-xs-2 col-md-2 col-sm-2 hidden-xs hidden-md hidden-sm">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-nickyromero.jpg" style="width:auto; height: auto;">
                        <p>Nicky Romero</p>
                    </div>
                    <div class="col-xs-2 col-md-2 col-sm-2 hidden-xs hidden-md hidden-sm">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-raverepublic.jpg" style="width:auto; height: auto;">
                        <p>Rave Republic</p>
                    </div>
                    <div class="col-xs-2 col-md-2 col-sm-2 hidden-xs hidden-md hidden-sm">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-rlgrime.jpg" style="width:auto; height: auto;">
                        <p>RL Grime</p>
                    </div>
                </div>
                <div class="row text-center section-black-alt">
                    <div class="col-xs-2 col-md-2 col-sm-2 hidden-xs hidden-md hidden-sm">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-steveangello.jpg" style="width:auto; height: auto;">
                        <p>Steve Angello</p>
                    </div>
                    <div class="col-xs-2 col-md-2 col-sm-2 hidden-xs hidden-md hidden-sm">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-sunneryjames-ryanmarciano.jpg" style="width:auto; height: auto;">
                        <p>Sunnery James &amp; Ryan Marciano</p>
                    </div>
                    <div class="col-xs-2 col-md-2 col-sm-2 hidden-xs hidden-md hidden-sm">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-ummetozcan.jpg" style="width:auto; height: auto;">
                        <p>Ummet Ozcan</p>
                    </div>
                </div>
                    {{-- Mobile --}}
                <div class="row text-center section-black-alt">
                    <div class="col-xs-12 col-sm-12 hidden-lg hidden-md">
                        <h1><strong>DJ Line Ups</strong></h1>
                    </div>
                    <div class="col-xs-4 col-sm-4 hidden-lg hidden-md">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-above-beyond.jpg" style="width:auto; height: auto;">
                        <p>Above &amp; Beyond</p>
                    </div>
                    <div class="col-xs-4 col-sm-4 hidden-lg hidden-md">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-afrojack.jpg" style="width:auto; height: auto;">
                        <p>Afrojack</p>
                    </div>
                    <div class="col-xs-4 col-sm-4 hidden-lg hidden-md">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-aly-fila.jpg" style="width:auto; height: auto;">
                        <p>Aly &amp; Fila</p>
                    </div>
                </div>
                <div class="row text-center section-black-alt">
                    <div class="col-xs-4 col-sm-4 hidden-lg hidden-md">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-andrewrayel.jpg" style="width:auto; height: auto;">
                        <p>Andrew Rayel</p>
                    </div>
                    <div class="col-xs-4 col-sm-4 hidden-lg hidden-md">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-cesqeaux.jpg" style="width:auto; height: auto;">
                        <p>Cesqeaux</p>
                    </div>
                    <div class="col-xs-4 col-sm-4 hidden-lg hidden-md">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-djsnake.jpg" style="width:auto; height: auto;">
                        <p>DJ Snake</p>
                    </div>
                </div>
                <div class="row text-center section-black-alt">
                    <div class="col-xs-4 col-sm-4 hidden-lg hidden-md">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-feddelegrand.jpg" style="width:auto; height: auto;">
                        <p>Fedde Le Grand</p>
                    </div>
                    <div class="col-xs-4 col-sm-4 hidden-lg hidden-md">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-ggmagree.jpg" style="width:auto; height: auto;">
                        <p>GG Magree</p>
                    </div>
                    <div class="col-xs-4 col-sm-4 hidden-lg hidden-md">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-illenium.jpg" style="width:auto; height: auto;">
                        <p>Illenium</p>
                    </div>
                </div>
                <div class="row text-center section-black-alt">
                    <div class="col-xs-4 col-sm-4 hidden-lg hidden-md">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-joyride.jpg" style="width:auto; height: auto;">
                        <p>JOYRYDE</p>
                    </div>
                    <div class="col-xs-4 col-sm-4 hidden-lg hidden-md">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-kriskrosamsterdam.jpg" style="width:auto; height: auto;">
                        <p>Kris Kross Amsterdam</p>
                    </div>
                    <div class="col-xs-4 col-sm-4 hidden-lg hidden-md">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-mykris.jpg" style="width:auto; height: auto;">
                        <p>Mykris</p>
                    </div>
                </div>
                <div class="row text-center section-black-alt">
                    <div class="col-xs-4 col-sm-4 hidden-lg hidden-md">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-myrne.jpg" style="width:auto; height: auto;">
                        <p>MYRNE</p>
                    </div>
                    <div class="col-xs-4 col-sm-4 hidden-lg hidden-md">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-nervo.jpg" style="width:auto; height: auto;">
                        <p>NERVO</p>
                    </div>
                    <div class="col-xs-4 col-sm-4 hidden-lg hidden-md">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-gudvibrations.jpg" style="width:auto; height: auto;">
                        <p>NGHTMRE + SLANDER present Gud Vibrations</p>
                    </div>
                </div>
                <div class="row text-center section-black-alt">
                    <div class="col-xs-4 col-sm-4 hidden-lg hidden-md">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-nickyromero.jpg" style="width:auto; height: auto;">
                        <p>Nicky Romero</p>
                    </div>
                    <div class="col-xs-4 col-sm-4 hidden-lg hidden-md">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-raverepublic.jpg" style="width:auto; height: auto;">
                        <p>Rave Republic</p>
                    </div>
                    <div class="col-xs-4 col-sm-4 hidden-lg hidden-md">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-rlgrime.jpg" style="width:auto; height: auto;">
                        <p>RL Grime</p>
                    </div>
                </div>
                <div class="row text-center section-black-alt">
                    <div class="col-xs-4 col-sm-4 hidden-lg hidden-md">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-steveangello.jpg" style="width:auto; height: auto;">
                        <p>Steve Angello</p>
                    </div>
                    <div class="col-xs-4 col-sm-4 hidden-lg hidden-md">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-sunneryjames-ryanmarciano.jpg" style="width:auto; height: auto;">
                        <p>Sunnery James &amp; Ryan Marciano</p>
                    </div>
                    <div class="col-xs-4 col-sm-4 hidden-lg hidden-md">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-ummetozcan.jpg" style="width:auto; height: auto;">
                        <p>Ummet Ozcan</p>
                    </div>
                </div>
                </div>
            </section>



            {{-- <section class="pageCategory-section last">
                <div class="container intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                           <img class="longLogo center-block img-responsive" src="images/ultrasingapore2018/logo-1.png">
                            <div class="text-center">
                                <h1 class="subSecTitle"><strong>LATEST NEWS</strong></h1>    
                            </div>

                            <div class="col-sm-12 col-xs-12">
                                
                                <div class="col-sm-4 col-xs-12 cardWrapper">
                                    <div class="card center-block">
                                        <img class="card-img-top img-responsive" src="images/ultrasingapore2018/card1.jpg" alt="Card image cap">
                                        <div class="card-body">
                                            <h6 class="card-title">ULTRA SINGAPORE: REMIXED INTO THE WEEKEND</h6>
                                            <p class="card-text">The second edition of Ultra Singapore takes place this weekend! Prepare for the event with these official …</p>
                                            <a href="https://ultramusicfestival.com/worldwide/prepare-ultra-singapore-weekend-apple-music-playlists/" class="readMore">Read More <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </div>
                                   
                                <div class="col-sm-4 col-xs-12 cardWrapper">
                                    <div class="card center-block">
                                        <img class="card-img-top img-responsive" src="images/ultrasingapore2018/card2.jpg" alt="Card image cap">
                                        <div class="card-body">
                                            <h6 class="card-title">ULTRA MUSIC FESTIVAL’S TWENTIETH ANNIVERSARY</h6>
                                            <p class="card-text">Having welcomed over one million fans to forty-five events internationally in 2017, the world’s largest …</p>
                                            <a href="https://ultrasingapore.com/worldwide/ultra-music-festivals-twentieth-anniversary/" class="readMore">Read More <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-4 col-xs-12 cardWrapper">
                                    <div class="card center-block">
                                        <img class="card-img-top img-responsive" src="images/ultrasingapore2018/card3.jpg" alt="Card image cap">
                                        <div class="card-body">
                                            <h6 class="card-title">ULTRA SINGAPORE: OFFICIAL AFTER MOVIE 2016</h6>
                                            <p class="card-text">A 2 day event boasting the world's top EDM DJ's with unparalleled stage designs and top tier production …</p>
                                            <a href="https://umfworldwide.com/media/umf-films/" class="readMore">Read More <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>

                            <div class="col-sm-12 col-xs-12" style="padding-top: 40px;">
                                <a class="btn ultraBtn center-block" id="buyButton" datetime="Feb 10 2018 00:00:00 GMT+0800" target="_blank" href="https://ultrasingapore.com/news/">MORE NEWS</a>   
                            </div>

                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </section> --}}
           {{--  <section class="pageCategory-section last">
                <img class="img-responsive" src="images/ultrasingapore2018/sponsor.jpg" style="width: 100%; height: auto;">
            </section> --}}

            <section class="pageCategory-section last">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                           {{-- <img class="longLogo center-block img-responsive" src="images/ultrasingapore2018/logo-1.png" style="margin-top: 70px;"> --}}
                            <div class="text-center col-sm-12 col-xs-12">
                                <h1 class="subSecTitle"><strong>CONCERT GALLERY</strong></h1>
                            </div>
                            <div class="row hidden-xs">
                                <div class="col-sm-12 text-center pd-0">
                                    <div class="col-sm-8 pd-0">
                                        <div class="col-sm-6 collageImg">
                                            <a href="images/ultrasingapore2018/gallery1-1.png" data-featherlight="image"><img class="img-responsive" src="images/ultrasingapore2018/gallery1-1.png" alt=""></a>
                                        </div>
                                        <div class="col-sm-6 collageImg">
                                            <a href="images/ultrasingapore2018/gallery1-2.png" data-featherlight="image"><img class="img-responsive" src="images/ultrasingapore2018/gallery1-2.png" alt=""></a>
                                        </div>
                                        <div class="col-sm-12 collageImg">
                                            <a href="images/ultrasingapore2018/gallery4.png" data-featherlight="image"><img class="img-responsive" src="images/ultrasingapore2018/gallery4.png" alt=""></a>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 pd-0">
                                        <div class="col-sm-12 collageImg">
                                            <a href="images/ultrasingapore2018/gallery3.png" data-featherlight="image"><img class="img-responsive" src="images/ultrasingapore2018/gallery3.png" alt=""></a>
                                        </div>  
                                        <div class="col-sm-12 collageImg">
                                            <a href="images/ultrasingapore2018/gallery5.png" data-featherlight="image"><img class="img-responsive" src="images/ultrasingapore2018/gallery5.png" alt=""></a>
                                        </div>  
                                    </div>
                                </div>
                                <div class="col-sm-12 text-center pd-0">
                                    <div class="col-sm-12 pd-0">
                                        <div class="col-sm-4 collageImg">
                                            <a href="images/ultrasingapore2018/gallery6.png" data-featherlight="image"><img class="img-responsive" src="images/ultrasingapore2018/gallery6.png" alt=""></a>
                                        </div>
                                        <div class="col-sm-8 collageImg">
                                            <a href="images/ultrasingapore2018/gallery7.png" data-featherlight="image"><img class="img-responsive" src="images/ultrasingapore2018/gallery7.png" alt=""></a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="gallery text-center hidden-lg hidden-md hidden-sm">
                                <div class="swiper-container">
                                    <div class="swiper-wrapper">
                                        <div class="swiper-slide">
                                            <a href="images/ultrasingapore2018/gallery1-1.png" data-featherlight="image"><img src="images/ultrasingapore2018/gallery1-1.png" alt=""></a>
                                        </div>
                                        <div class="swiper-slide">
                                            <a href="images/ultrasingapore2018/gallery1-2.png" data-featherlight="image"><img src="images/ultrasingapore2018/gallery1-2.png" alt=""></a>
                                        </div>
                                        <div class="swiper-slide">
                                            <a href="images/ultrasingapore2018/gallery4.png" data-featherlight="image"><img src="images/ultrasingapore2018/gallery4.png" alt=""></a>
                                        </div>
                                        <div class="swiper-slide">
                                            <a href="images/ultrasingapore2018/gallery3.png" data-featherlight="image"><img src="images/ultrasingapore2018/gallery3.png" alt=""></a>
                                        </div>
                                        <div class="swiper-slide">
                                            <a href="images/ultrasingapore2018/gallery5.png" data-featherlight="image"><img src="images/ultrasingapore2018/gallery5.png" alt=""></a>
                                        </div>
                                        <div class="swiper-slide">
                                            <a href="images/ultrasingapore2018/gallery6.png" data-featherlight="image"><img src="images/ultrasingapore2018/gallery6.png" alt=""></a>
                                        </div>
                                        <div class="swiper-slide">
                                            <a href="images/ultrasingapore2018/gallery7.png" data-featherlight="image"><img src="images/ultrasingapore2018/gallery7.png" alt=""></a>
                                        </div>
                                    </div>

                                    <div class="swiper-pagination"></div>

                                    <div class="swiper-button-next swiper-button-white"></div>
                                    <div class="swiper-button-prev swiper-button-white"></div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </section>

            {{-- <section class="pageCategory-section last hidden-xs" style="margin-top: 40px; margin-bottom: 0px;">
                <img class="img-responsive" src="images/ultrasingapore2018/middle.png" style="width: 100%; height: auto;">
            </section> --}}



            {{-- <section class="pageCategory-section last">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                            <div class="text-center col-sm-12 col-xs-12">
                                 <p>DBS/POSB Cardholders Promo: 2 Free drinks per 2-day GA purchased (limited to the first 1,000 tickets)<br/>
                                 Live Fresh Promo: Free upgrade to 2-day PGA per 2-day GA purchased (limited to the first 500 tickets)<br/>
                                 <a href="https://intercom.help/airasia-redtix/your-guide-to-ultra-singapore-2018">Click here to find out more</a></p>
                            </div>
                            <div class="text-center col-sm-12 col-xs-12">
                                <p>Don't have the Card? Get your DBS Live Fresh Card here: <a href="http://dbs.com.sg/livefresh">dbs.com.sg/livefresh</a></p>
                                <p>This Exclusive Sale is for DBS Live Fresh cardmembers and DBS/POSB cardmembers. We reserve the right to cancel and refund any transactions that were not purchased with a DBS card.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section> --}}

            
             <section class="pageCategory-section last bottom">
                <img src="images/ultrasingapore2018/white-logo1.png" class="img-responsive logoBottom hidden-xs"> 
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                            <div class="note text-left">
                                {{-- <h4>please contact us at <a href="mailto:ultra@airasiaredtix.com">ultra@airasiaredtix.com</a> if you have any further questions for custom packages</h4> --}}
                                {{-- <h4>Don't have the Card? Get your DBS Live Fresh Card here: <a href="http://dbs.com.sg/livefresh">dbs.com.sg/livefresh</h4>
                                <h4>Tickets go on sale on Tues 20th Feb. Don't miss out, <a href="https://ultrasingapore.com/">register</a> now to get access to tier 1 tickets!</h4> --}}
                                {{-- <h4>Frequently Asked Questions:</h4>
                                <p><a href="https://intercom.help/airasia-redtix/your-guide-to-ultra-singapore-2018">Click here for more information</a></p> --}}
                                +                                <h4>please contact us at <a href="mailto:ultra@airasiaredtix.com">ultra@airasiaredtix.com</a><br> if you have any further questions for custom packages</h4>
                            </div>
                            <div class="note text-left">
                                <h4>For enquiry only:</h4>
                                <p>Email to <a href="mailto:ultra@airasiaredtix.com">ultra@airasiaredtix.com</a></p>
                            </div>
                        </div>
                    </div>
                    <div class="center-block text-center">
                        <div class="faq center-block">
                            {{-- FAQ button --}}
                            <img src="images/ultrasingapore2018/question.svg" style="width: 30px;"> Need more info? <a class="tixOptBtn" href="https://intercom.help/airasia-redtix/your-guide-to-ultra-singapore-2018">FAQ</a>
                        </div>
                    </div>
                </div>
            </section>
 
        </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')
    <script type="text/javascript">
        $(document).ready(function() {

        //Get hash from URL. Ex. index.html#Chapter1
        var hash = location.hash;

        if (hash == '#getflight') {
            //Do stuff here
            //alert("Hello");
            location.href = "#myticket";
            $(function() {
                $('[href="#ticket_flight"]').tab('show');
            });
        }
    });
    </script>

	{{-- countdown --}}
	<script src="js/jquery.time-to.js"></script>
	<script type="text/javascript">
		$('#countdown').timeTo({
		    timeTo: new Date(new Date('Friday June 15 2018 12:00:00 GMT+0800 (+08)')),
		    displayDays: 2,
		    theme: "white",
		    displayCaptions: true,
		    fontSize: 36,
		    captionSize: 12
		}); 
		$('#countdown2').timeTo({
		    timeTo: new Date(new Date('Friday June 15 2018 12:00:00 GMT+0800 (+08)')),
		    displayDays: 2,
		    theme: "white",
		    displayCaptions: true,
		    fontSize: 36,
		    captionSize: 12
		}); 
	</script>
	{{-- /countdown --}}

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });
    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 450;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for anchor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });

/*    $(function() {
        $(".buy_form").submit(function(){
            alert("am here");
            return false;
        });
    })
*/
    </script>


    {{-- Buy button disable --}}
    <script type="text/javascript">
        $(function() {
            $('a[id^=buyButton]').each(function() {
                var date = new Date();
                var enddate = $(this).attr('datetime'); 
                if ( Date.parse(date) >= Date.parse(enddate)) {
                  $(this).addClass('disabled');
                }
            });
        });
    </script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>

    <script type="text/javascript">
        $('.flightTab').hide();
        //show the first tab content
        $('#country-1').show();

        $('#select-country').change(function () {
           dropdown = $('#select-country').val();
          //first hide all tabs again when a new option is selected
          $('.flightTab').hide();
          //then show the tab content of whatever option value was selected
          $('#' + "country-" + dropdown).show();                                    
        });
    </script>
    @if(!empty(Session::get('error_code')) && Session::get('error_code') == 11)
        <script>
        $(function() {
            $('#errorModalRegistered').modal('show');
        });
        </script>
    @endif

    @if(!empty(Session::get('error_code')) && Session::get('error_code') == 12)
        <script>
        $(function() {
            $('#failModalRegistered').modal('show');
        });
        </script>
    @endif

    @if(!empty(Session::get('error_code')) && Session::get('error_code') == 13)
        <script>
        $(function() {
            $('#errorModalUsedPassword').modal('show');
        });
        </script>
    @endif

    @if(!empty(Session::get('error_code')) && Session::get('error_code') == 5)
        <script>
        $(function() {
            $('#errorModal').modal('show');
        });
        </script>
    @endif
    @if(!empty(Session::get('error_code')) && Session::get('error_code') == 6)
        <script>
        $(function() {
            $('#errorModal2').modal('show');
        });
        </script>
    @endif
    @if(!empty(Session::get('success_lf')))
        <script>
        $(function() {
            $('#successlfModal{{ session()->get('ticket_id') }}').modal('show');
        });
        </script>
    @endif

    @if(!empty(Session::get('success_dbs')))
        <script>
        $(function() {
            $('#successdbsModal{{ session()->get('ticket_id') }}').modal('show');
        });
        </script>
    @endif
    
    @if(!empty(Session::get('success_registered')))
        <script>
        $(function() {
            $('#successModalRegistered{{ session()->get('ticket_id') }}').modal('show');
        });
        </script>
    @endif

@endsection

@section('modal')
    @include('layouts.partials.modals._seatplan')
    @include('layouts.partials.modals._getTixCustom')
{{-- @foreach($ticket_flight as $ticket)
    <div id="flightModal{{ $ticket["Sorting ID"] }}" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-body text-center">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
                <h1><i class="fa fa-bullhorn text-warning" aria-hidden="true"></i></h1>
                <h4>Flight Dates Available</h4>
                <div class="clearfix">&nbsp;</div>
                <div class="well">
                <p>Depart ({{ $ticket["From City"] }} - Singapore):</p>
                <p>{{ date(' D, M j Y', strtotime($ticket["Outbound Departure Date"])).", ". date('h:i a', strtotime($ticket["Outbound Departure Time"])) }}</p>
                <p>Return (Singapore - {{ $ticket["From City"] }}):</p>
                <p>{{ date(' D, M j Y', strtotime($ticket["Inbound Departure Date"])).", ". date('h:i a', strtotime($ticket["Inbound Departure Time"])) }}</p>
                </div>
            </div>
          </div>
        </div>
    </div>
@endforeach --}}
@foreach($ticket_only as $ticket)

    <div id="buyModal{{ $ticket["Sorting ID"] }}" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-body text-center pd-0">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="padding: 16px;"><i class="fa fa-times" aria-hidden="true"></i></button>
                <div class="row ultraModalRow">
                    <div class="col-sm-7 col-xs-12 colUltraImg">
                        <img class="img-responsive imgUltraModal hidden-xs" src="images/ultrasingapore2018/modal-picture2.jpg">
                        <img class="img-responsive imgUltraModal hidden-lg hidden-md hidden-sm" src="images/ultrasingapore2018/modal-picture-mobile.jpg">
                    </div>
                    <div class="col-sm-5 text-center ultraform">
                        {{-- <p>Please can you enter in the <b>first 6 digits</b> of  your card number to see if you are entitled to an extra discount</p> --}}
                        {{-- <p>Please can you enter your card number to see if you are entitled to an extra discount</p> --}}
                        <p>Please can you enter in the <b>first 10 digits</b> of your card number to see if you are entitled to extra savings</p>
                        <form action="{{route('ultra')}}" method="get" enctype="multipart/form-data" class="buy_form">
                            {{ csrf_field() }}
                            <input type='hidden' name='ticket_id' value='{{$ticket['Sorting ID']}}'>
                            <div class="form-group center-block">
                                <input id="bin_num{{ $ticket["Sorting ID"] }}" type="number" class="form-control" name="bin_num" autofocus required="required" placeholder="00000000000" maxlength="10">
                            </div>
                            <div class="form-group center-block">
                                <button class="btn ultraBuyBtnGrey" type="submit">SUBMIT</button>
                            </div>
                        </form>
                    </div>
                </div> 
            </div>
          </div>
        </div>
    </div>

    {{-- password form for registered user --}}
    <div id="buyModalRegistered{{ $ticket["Sorting ID"] }}" class="modal fade registeredModal" role="dialog">
        <div class="modal-dialog">
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-body text-center pd-0">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="padding: 16px;"><i class="fa fa-times" aria-hidden="true"></i></button>
                <div class="row ultraModalRow">
                    <div class="col-sm-12 text-center ultraform">
                        <div class="col-sm-12 tixClass">
                            <h6>{{$ticket["Ticket Type"]}}</h6>
                            <div class="col-sm-12 col-xs-12 tixTitle">
                                <div class="col-xs-6 col-sm-6 day">2 DAY</div>
                                <div class="col-xs-6 col-sm-6 combo">COMBO<br>TICKET</div>
                            </div>
                        </div>
                        {{-- <p>Please enter your one time use password. <br>Tier 1 tickets are only available for registered users.</p> --}}
                        <p>***Please enter your Registration password for access to Tier 1 tickets.<br />
                        Please note each unique code can only be SUBMITTED ONCE.<br />
                        If you submit your code and attempt to purchase at a later time, they will no longer be valid.</p>
                        <form action="{{route('ultraregistered')}}" method="get" enctype="multipart/form-data" class="buy_form">
                            {{ csrf_field() }}
                            <input type='hidden' name='ticket_id' value='{{$ticket['Sorting ID']}}'>
                            <div class="form-group center-block">
                                <input id="password{{ $ticket["Sorting ID"] }}" type="text" class="form-control" name="password" autofocus required="required" placeholder="*****">
                            </div>
                            <div class="form-group center-block">
                                <button class="btn ultraBuyBtnGrey" type="submit">SUBMIT</button>
                            </div>
                        </form>
                    </div>
                </div> 
            </div>
          </div>
        </div>
    </div>

    {{-- ticket info modal --}}
    <div id="infoModal{{ $ticket["Sorting ID"] }}" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-body text-center pd-0">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="padding: 16px;"><i class="fa fa-times" aria-hidden="true"></i></button>
                <div class="row ultraModalRow info-box">

                    @if($ticket["Ticket Type"] == "PGA")
                    <div class="details">
                        <h6 class="type">Premium General Admission</h6>
                        <h6 class="days">2 DAY TICKET COMBO</h6>
                    </div>
                    <div class="details">
                        <ul>
                            <li><span class="fa fa-tag"></span>Premium General Admission, free standing.</li>
                            <li><span class="fa fa-calendar"></span>Friday &amp; Saturday, 15 - 16 June 2018</li>
                            <li><span class="fa fa-map-marker"></span>Ultra Park, 1 Bayfront Avenue, Singapore</li>
                        </ul>   
                    </div>
                    @endif
                    @if($ticket["Ticket Type"] == "GA")
                        @if($ticket["Ticket Name"] == "1 DAY TICKET FRIDAY")
                        <div class="details">
                            <h6 class="type">General Admission</h6>
                            <h6 class="days">1 DAY TICKET FRIDAY</h6>
                        </div>
                        <div class="details">
                            <ul>
                                <li><span class="fa fa-tag"></span>General Admission, free standing.</li>
                                <li><span class="fa fa-calendar"></span>Friday, 15 June 2018</li>
                                <li><span class="fa fa-map-marker"></span>Ultra Park, 1 Bayfront Avenue, Singapore</li>
                            </ul>   
                        </div>
                        @elseif($ticket["Ticket Name"] == "1 DAY TICKET SATURDAY")
                        <div class="details">
                            <h6 class="type">General Admission</h6>
                            <h6 class="days">1 DAY TICKET SATURDAY</h6>
                        </div>
                        <div class="details">
                            <ul>
                                <li><span class="fa fa-tag"></span>General Admission, free standing.</li>
                                <li><span class="fa fa-calendar"></span>Saturday, 16 June 2018</li>
                                <li><span class="fa fa-map-marker"></span>Ultra Park, 1 Bayfront Avenue, Singapore</li>
                            </ul>   
                        </div>
                        @else
                        <div class="details">
                            <h6 class="type">General Admission</h6>
                            <h6 class="days">2 DAY TICKET COMBO</h6>
                        </div>
                        <div class="details">
                            <ul>
                                <li><span class="fa fa-tag"></span>General Admission, free standing.</li>
                                <li><span class="fa fa-calendar"></span>Friday &amp; Saturday, 15 - 16 June 2018</li>
                                <li><span class="fa fa-map-marker"></span>Ultra Park, 1 Bayfront Avenue, Singapore</li>
                            </ul>   
                        </div>
                        @endif
                    @endif

                    @if($ticket["Ticket Type"] == "PGA")
                        <div class="services col-md-12 hidden-xs hidden-sm text-center">
                            <div class="col-md-4">
                                <img src="images/ultrasingapore2018/flag.png" style="width:50px;"><br />
                                <span>Express Lane</span>
                            </div>
                            <div class="col-md-4">
                                <img src="images/ultrasingapore2018/drink.png"><br />
                                <span>Bar Access</span>
                            </div>
                                <img src="images/ultrasingapore2018/beach.png"><br />
                                <span>Private Lounge</span>
                            </div>
                            </div>
                            <div class="services col-md-12 hidden-xs hidden-sm text-center">
                            <div class="col-md-4">
                                <img src="images/ultrasingapore2018/rfid-chip.png"><br />
                                <span>Express RFID Top-up</span>
                            </div>
                            <div class="col-md-4">
                                <img src="images/ultrasingapore2018/watch-resolution.png"><br />
                                <span>RFID Wristband</span>
                            </div> 
                            <div class="col-md-4">
                                <img src="images/ultrasingapore2018/ep_mono.png"><br />
                                <span>Insurance</span>
                            </div>
                            </div>
                            <div class="services col-md-12 hidden-xs hidden-sm text-center">
                            <div class="col-md-4">
                                <img src="images/ultrasingapore2018/restroom-sign.png"><br />
                                <span>Private Restroom</span>
                            </div>
                            <div class="col-md-4">
                                <img src="images/ultrasingapore2018/18-plus.png"><br />
                                <span>Above 18+</span>
                            </div>

                            <div class="col-md-4">
                                <img src="images/ultrasingapore2018/no-entry.png"><br />
                                <span>No re-entry</span>
                            </div> 
                        </div>
                        <div class="services col-xs-12 hidden-md hidden-lg text-center">
                            <div class="col-xs-4">
                                <img src="images/ultrasingapore2018/flag.png" style="width:50px;"><br />
                                <span>Express Lane</span>
                            </div>
                            <div class="col-xs-4">
                                <img src="images/ultrasingapore2018/drink.png"><br />
                                <span>Bar Access</span>
                            </div>
                                <img src="images/ultrasingapore2018/beach.png"><br />
                                <span>Private Lounge</span>
                            </div>
                        </div>
                        <div class="services col-xs-12 hidden-md hidden-lg text-center">
                            <div class="col-xs-4">
                                <img src="images/ultrasingapore2018/rfid-chip.png"><br />
                                <span>Express RFID Top-up</span>
                            </div>
                            <div class="col-xs-4">
                                <img src="images/ultrasingapore2018/watch-resolution.png"><br />
                                <span>RFID Wristband</span>
                            </div> 
                            <div class="col-xs-4">
                                <img src="images/ultrasingapore2018/ep_mono.png"><br />
                                <span>Insurance</span>
                            </div>
                        </div>
                        <div class="services col-xs-12 hidden-md hidden-lg text-center">
                            <div class="col-xs-4">
                                <img src="images/ultrasingapore2018/restroom-sign.png"><br />
                                <span>Private Restroom</span>
                            </div>
                            <div class="col-xs-4">
                                <img src="images/ultrasingapore2018/18-plus.png"><br />
                                <span>Above 18+</span>
                            </div>
                            <div class="col-xs-4">
                                <img src="images/ultrasingapore2018/no-entry.png"><br />
                                <span>No re-entry</span>
                            </div> 
                        </div>
                    @endif

                    @if($ticket["Ticket Type"] == "GA")
                       <div class="services col-md-12 col-md-push-1 hidden-xs hidden-sm text-center">
                            <div class="col-md-2">
                                <img src="images/ultrasingapore2018/drink.png"><br />
                                <span>Bar Access</span>
                            </div> 

                            <div class="col-md-2">
                                <img src="images/ultrasingapore2018/ep_mono.png"><br />
                                <span>Insurance</span>
                            </div> 
                            <div class="col-md-2">
                                <img src="images/ultrasingapore2018/watch-resolution.png"><br />
                                <span>RFID Wristband</span>
                            </div> 

                            <div class="col-md-2">
                                <img src="images/ultrasingapore2018/18-plus.png"><br />
                                <span>Above 18+</span>
                            </div> 

                            <div class="col-md-2">
                                <img src="images/ultrasingapore2018/no-entry.png"><br />
                                <span>No re-entry</span>
                            </div> 
                        </div>
                       <div class="services col-xs-12 hidden-md hidden-lg text-center">
                            <div class="col-xs-4">
                                <img src="images/ultrasingapore2018/drink.png"><br />
                                <span>Bar Access</span>
                            </div> 
                            <div class="col-xs-4">
                                <img class="list-icon" src="images/ultrasingapore2018/ep_mono.png"><br />
                                <span>Insurance</span>
                            </div> 

                            <div class="col-xs-4">
                                <img class="list-icon" src="images/ultrasingapore2018/watch-resolution.png"><br />
                                <span>RFID Wristband</span>
                            </div> 
                        </div>
                        <div class="services col-xs-12 hidden-md hidden-lg text-center">
                            <div class="col-xs-6 col-sm-2">
                                <img class="list-icon" src="images/ultrasingapore2018/18-plus.png"><br />
                                <span>Above 18+</span>
                            </div> 

                            <div class="col-xs-6 col-sm-2">
                                <img class="list-icon" src="images/ultrasingapore2018/no-entry.png"><br />
                                <span>No re-entry</span>
                            </div> 
                        </div>  
                    @endif
                    <div class="copy_text text-center">
                        *Ticketing fee, Event Protect insurance &amp; credit card <br/>
                        charges will be added at the checkout basket.<br/>
                        <a href="purchaseterms">Terms &amp; Conditions</a> apply.
                    </div>    

                </div> 
            </div>
          </div>
        </div>
    </div>


@endforeach

<div id="successlfModal{{ $ticket1["Sorting ID"] }}" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-body text-center pd-0">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="padding: 16px;"><i class="fa fa-times" aria-hidden="true"></i></button>
            <div class="row ultraModalRow">
                <div class="col-sm-7 col-xs-12 colUltraImg">
                    <img class="img-responsive imgUltraModal hidden-xs" src="images/ultrasingapore2018/modal-picture2.jpg">
                    <img class="img-responsive imgUltraModal hidden-lg hidden-md hidden-sm" src="images/ultrasingapore2018/modal-picture-mobile.jpg">
                </div>
                <div class="col-sm-5 text-center ultraform">
                    {{-- <p>{{ $ticket["Private Sale Front End"] }}</p> --}}
                    <h6>Congratulations!</h6>
                    <p>As a LiveFresh Cardmember, you are entitled to purchase a {{ $ticket1["Ticket Name"] }} {{ $ticket1["Ticket Type"] }} ticket at {{ $ticket1["Private Sale Front End"] }}</p>
                    <a class="btn ultraBuyBtnGrey center-block disabled" href="{{ $ticket1["Ticketserv URL"] }}" onclick="gtag_report_conversion(); location.href=this.href&linkerParam;return false;">BUY TICKET</a>
                    <small>Price excludes ticketing fee, Event Protect insurance &amp; credit card charges</small>
                </div>
            </div> 
        </div>
      </div>
    </div>
</div>

<div id="successdbsModal{{ $ticket1["Sorting ID"] }}" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-body text-center pd-0">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="padding: 16px;"><i class="fa fa-times" aria-hidden="true"></i></button>
            <div class="row ultraModalRow">
                <div class="col-sm-7 col-xs-12 colUltraImg">
                    <img class="img-responsive imgUltraModal hidden-xs" src="images/ultrasingapore2018/modal-picture2.jpg">
                    <img class="img-responsive imgUltraModal hidden-lg hidden-md hidden-sm" src="images/ultrasingapore2018/modal-picture-mobile.jpg">
                </div>
                <div class="col-sm-5 text-center ultraform">
                    {{-- <p>{{ $ticket["Private Sale Front End"] }}</p> --}}
                    <h6>Congratulations!</h6>
                    <p>As a DBS/POSB Cardmember, you are entitled to purchase a {{ $ticket1["Ticket Name"] }} {{ $ticket1["Ticket Type"] }} ticket at {{ $ticket1["Private Sale Front End"] }}</p>
                    <a class="btn ultraBuyBtnGrey center-block disabled" href="{{ $ticket1["Ticketserv URL"] }}" onclick="gtag_report_conversion(); location.href=this.href&linkerParam;return false;">BUY TICKET</a>
                    <small>Price excludes ticketing fee, Event Protect insurance &amp; credit card charges</small>
                </div>
            </div> 
        </div>
      </div>
    </div>
</div>


<div id="errorModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-body text-center pd-0">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="padding: 16px;"><i class="fa fa-times" aria-hidden="true"></i></button>
            <div class="row ultraModalRow">
                <div class="col-sm-7 col-xs-12 colUltraImg">
                    <img class="img-responsive imgUltraModal hidden-xs" src="images/ultrasingapore2018/modal-picture2.jpg">
                    <img class="img-responsive imgUltraModal hidden-lg hidden-md hidden-sm" src="images/ultrasingapore2018/modal-picture-mobile.jpg">
                </div>
                <div class="col-sm-5 text-center ultraform">
                    {{-- <p>Please can you enter in the <b>first 6 digits</b> of  your card number to see if you are entitled to an extra discount</p> --}}
                    {{-- <p>Sorry! you are not a DBS cardholder and cannot purchase a ticket now and will have to wait until the tickets go on sale publicly or alternatively you can sign up to a DBS LiveFresh card now.</p> --}}
                    <p>Sorry! This exclusive card sale is for DBS cardmembers only.</p>
                    {{-- <p>To make sure you secure your ULTRA SINGAPORE 2018 Tickets please register below!</p>
                    <a class="btn ultraBuyBtnGrey center-block disabled" href="https://ultrasingapore.com/">REGISTER</a> --}}
                    </form>
                </div>
            </div> 
        </div>
      </div>
    </div>
</div>


<div id="errorModal2" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-body text-center pd-0">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="padding: 16px;"><i class="fa fa-times" aria-hidden="true"></i></button>
            <div class="row ultraModalRow">
                <div class="col-sm-7 col-xs-12 colUltraImg">
                    <img class="img-responsive imgUltraModal hidden-xs" src="images/ultrasingapore2018/modal-picture2.jpg">
                    <img class="img-responsive imgUltraModal hidden-lg hidden-md hidden-sm" src="images/ultrasingapore2018/modal-picture-mobile.jpg">
                </div>
                <div class="col-sm-5 text-center ultraform">
                    {{-- <p>Please can you enter in the <b>first 6 digits</b> of  your card number to see if you are entitled to an extra discount</p> --}}
                    {{-- <p>Sorry! you are not a DBS Live Fresh cardholder and cannot purchase a ticket now and will have to wait until the tickets go on sale publicly or alternatively you can sign up to a DBS LiveFresh card now.</p> --}}
                    <p>Sorry! This exclusive card sale is for DBS cardmembers only. To make sure you secure your ULTRA SINGAPORE 2018 Tickets please register below!</p>
                    <a class="btn ultraBuyBtnGrey center-block disabled" href="https://ultrasingapore.com/">REGISTER</a>
                    </form>
                </div>
            </div> 
        </div>
      </div>
    </div>
</div>

{{-- error modal for registered user --}}
<div id="errorModalRegistered" class="modal fade registeredModal" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-body text-center pd-0">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="padding: 16px;"><i class="fa fa-times" aria-hidden="true"></i></button>
            <div class="row ultraModalRow">
                <div class="col-sm-12 text-center ultraform">
                    <div class="col-sm-12 tixClass">
                        <h6>{{$ticket["Ticket Type"]}}</h6>
                        <div class="col-sm-12 col-xs-12 tixTitle">
                            <div class="col-xs-6 col-sm-6 day">2 DAY</div>
                            <div class="col-xs-6 col-sm-6 combo">COMBO<br>TICKET</div>
                        </div>
                    </div>
                    <p class="text-danger">Sorry, your password is incorrect. Please try again. <br>You have {{5 - (session()->get('password_attemps'))}} attempts left.</p>
                    <form action="{{route('ultraregistered')}}" method="get" enctype="multipart/form-data" class="buy_form">
                        {{ csrf_field() }}
                        <div class="form-group center-block">
                            <input type='hidden' name='ticket_id' value='{{$ticket1['Sorting ID']}}'>
                            <input id="password" type="text" class="form-control input-warning" name="password" autofocus required="required" placeholder="*****">
                        </div>
                        <div class="form-group center-block">
                            <button class="btn ultraBuyBtnGrey" type="submit">TRY AGAIN</button>
                        </div>
                    </form>
                </div>
            </div> 
        </div>
      </div>
    </div>
</div>

{{-- password used modal for registered user --}}
<div id="errorModalUsedPassword" class="modal fade registeredModal" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-body text-center pd-0">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="padding: 16px;"><i class="fa fa-times" aria-hidden="true"></i></button>
            <div class="row ultraModalRow">
                <div class="col-sm-12 text-center ultraform">
                    <div class="col-sm-12 tixClass">
                        <h6>{{$ticket["Ticket Type"]}}</h6>
                        <div class="col-sm-12 col-xs-12 tixTitle">
                            <div class="col-xs-6 col-sm-6 day">2 DAY</div>
                            <div class="col-xs-6 col-sm-6 combo">COMBO<br>TICKET</div>
                        </div>
                    </div>
                    <p class="text-danger" style="margin: 0 100px; margin-bottom: 10px;">Sorry, your password has been used. If you need help, contact us via <a href="#">chat</a></p>
                    <form action="{{route('ultraregistered')}}" method="get" enctype="multipart/form-data" class="buy_form">
                        {{ csrf_field() }}
                        <div class="form-group center-block">
                            <input type='hidden' name='ticket_id' value='{{$ticket1['Sorting ID']}}'>
                            <input id="password" type="text" class="form-control input-warning" name="password" autofocus required="required" placeholder="*****">
                        </div>
                        <div class="form-group center-block">
                            <button class="btn ultraBuyBtnGrey" type="submit">TRY AGAIN</button>
                        </div>
                    </form>
                </div>
            </div> 
        </div>
      </div>
    </div>
</div>


{{-- fail attempted modal for registered user --}}
<div id="failModalRegistered" class="modal fade registeredModal" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-body text-center pd-0">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="padding: 16px;"><i class="fa fa-times" aria-hidden="true"></i></button>
            <div class="row ultraModalRow">
                <div class="col-sm-12 text-center ultraform">
                    <div class="col-sm-12 tixClass">
                        <h6>{{$ticket["Ticket Type"]}}</h6>
                        <div class="col-sm-12 col-xs-12 tixTitle">
                            <div class="col-xs-6 col-sm-6 day">2 DAY</div>
                            <div class="col-xs-6 col-sm-6 combo">COMBO<br>TICKET</div>
                        </div>
                    </div>
                    <img class="img-responsive center-block lock-img" src="images/ultrasingapore2018/lock.png" style="width: 90px; height: 90px;">
                    <p class="failText">Sorry, you've made 5 incorrect attempts. <br>Please try again in 5 minutes.</p>
                </div>
            </div> 
        </div>
      </div>
    </div>
</div>

{{-- success modal for registered user --}}
<div id="successModalRegistered{{ $ticket1["Sorting ID"] }}" class="modal fade registeredModal" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-body text-center pd-0">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="padding: 16px;"><i class="fa fa-times" aria-hidden="true"></i></button>
            <div class="row ultraModalRow">
                <div class="col-sm-12 text-center ultraform">
                    <div class="col-sm-12 tixClass">
                        <h6>{{$ticket["Ticket Type"]}}</h6>
                        <div class="col-sm-12 col-xs-12 tixTitle">
                            <div class="col-xs-6 col-sm-6 day">2 DAY</div>
                            <div class="col-xs-6 col-sm-6 combo">COMBO<br>TICKET</div>
                        </div>
                    </div>
                    <img class="img-responsive center-block check-img" src="images/ultrasingapore2018/checked.png" style="width: 90px; height: 90px;">
                    <p class="textSuccess">Password is correct. We hope you enjoy the festival!</p>
                    <a class="btn ultraBuyBtnGrey center-block disabled" href="{{ $ticket1["Ticketserv URL"] }}" onclick="gtag_report_conversion(); location.href=this.href&linkerParam;return false;" style="width: 200px; margin-bottom: 20px;">BUY TICKET</a>
                </div>
            </div> 
        </div>
      </div>
    </div>
</div>






@endsection
