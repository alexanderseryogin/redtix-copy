@extends('master')
@section('title')
    Rugby World Cup 2019
@endsection

@section('header')
    @include('layouts.partials._header')
@endsection

@section('content')
    <style type="text/css">
        .pageCart, .pageContent, section.pageContent-Home {
            overflow: hidden;
        }
        @media screen and (max-width: 768px){
            header.mainHeader {
                top: 0px;
                background-color: black;
                height: 65px;
                display: block;
            }
        }
        .white {
            background-color: #ffffff;
        }
        .black {
            background-color: #181818;
            color: #ffffff;
        }
        #section-black{
            background-color: #181818;
            color: #ffffff;
        }
        .cnyred {
            background-color: #ec0b44;
        }

        #section-black h4 {
            font-family: Lato;
            font-weight: 400;
            line-height: 1.19;
            text-align: center;
            margin-bottom: 40px;
            color: #ffffff;
        }

        #section-black h6 {
            font-family: 'Open Sans', sans-serif;
            font-size: 20px;
            line-height: 1.74;
            letter-spacing: 0.2px;
            text-align: center;
            color: #ffffff;
            font-weight: 100;
        }

        #rwc19 .introRWC {
            margin-top: 301px;
            height: 460px;
            display: flex;
            width: 100vw;
            position: relative;
            /* align-items: center; */
            justify-content: center;
        }
        #rwc19 .introRWC img {
            position: absolute;
            width: 1100px;
            bottom: -46px;
            height: auto;
        }
        #rwc19 .wrapper {
            position: relative;
            overflow: hidden;
            height: 760px;
        }
        .center {
           width: 300px;
           height: 300px;
           position: absolute;
           left: 50%;
           top: 50%; 
           margin-left: -150px;
           margin-top: -150px;
        }
        #fuji {
            width: 100vw;
            height: 950px;
            background-color: #ffffff;
            background-image: url('images/rwc19/fuji.png');
            background-size: contain;
            background-position: center 108%;
            background-repeat: no-repeat;
            color: #ffffff;
        } 

        @media  screen and (min-width: 1920px) {
            #fuji {
                    width: 100vw;
                    height: 1200px;
                    background-color: #ffffff;
                    background-image: url(images/rwc19/fuji.png);
                    background-size: contain;
                    background-position: center 46vh;
                    background-repeat: no-repeat;
                    color: #ffffff;
                } 
        }

        #fuji .title {
            border-top: 4px solid #ec0b44;
            margin-top: 50px;
            padding: 10px 0px;
            color: #ec0b44;
        }
        #fuji .title h4 {
            text-transform: uppercase;
            margin: 10px 0px;
            color: #ec0b44;
            font-family: Lato;
            font-size: 42px;
            font-weight: bold;
            text-align: left;
        }
        #fuji .title h1 {
            margin-top: 0px;
            font-size: 70px;
            font-weight: bold;
            line-height: 0.92;
            text-align: left;
            color: #333333;
        }
        #fuji .fujiImg {
            width: 100vw;
        }
        @media screen and (max-width: 768px){
            #fuji {
                width: 100%;
                height: 400px;
                background-color: #ffffff;
                background-image: url('images/rwc19/fuji.png');
                background-size: contain;
                background-position: center 105%;
                background-repeat: no-repeat;
                color: #ffffff;
            }

            #fuji .title {
                border-top: 4px solid #ec0b44;
                margin: auto 20px;
                margin-top: 50px;
                padding: 10px 0px;
                color: #ec0b44;
            }
            #fuji .title h4 {
                text-transform: uppercase;
                margin: 0px 0px;
                color: #ec0b44;
                font-family: Lato;
                font-size: 14px;
                font-weight: bold;
                text-align: left;
            }
            #fuji .title h1 {
                margin-top: 0px;
                font-size: 33px;
                font-weight: bold;
                line-height: 0.92;
                text-align: left;
                color: #333333;
            }
        }

        #rwc19-center .monument1 {
            background-image: url('images/rwc19/monument1.png');
            background-size: 90%;
            background-position: bottom; 
            background-repeat: no-repeat;
            height: 200px;
            padding: 20px 20px;
            margin-top: 10px;
        }
        #rwc19-center .monument2 {
            background-image: url('images/rwc19/monument2.png');
            background-size: 90%;
            background-position: bottom; 
            background-repeat: no-repeat;
            height: 300px;
            padding: 20px 20px;
            margin-top: 10px;
        }

        @media screen and (max-width: 768px){
            #rwc19-center .monument2 {
                background-image: url('images/rwc19/monument2.png');
                background-size: 90%;
                background-position: bottom; 
                background-repeat: no-repeat;
                padding: 20px 20px;
                margin-top: 10px;
                min-height: 350px;
            }
        }
        .btn-rwc {
            font-family: Lato;
            font-size: 16px;
            font-weight: 100;
            text-align: center;
            color: #ffffff;
            border-radius: 100px;
            background-color: #ec0b43;
            padding: 10px 30px;
            margin: 20px 20px;
        }

        #schedule {
            margin: 70px auto;
        }

        #schedule .schedule-mobile {
            padding: 30px 20px;
            margin-top: 30px;
            margin-left: 40px;
            margin-right: 40px;  
        }

        #schedule .schedule-row {
            margin-top: 30px; 
            margin-bottom: 60px; 
        }
        #schedule .schedule-col {
            margin: 15px 0px;
        }

        #schedule img {
            width: 250px;
        }

        #schedule .day {
            font-family: 'Open Sans', sans-serif;
            font-size: 20px;
            font-weight: 300;
            letter-spacing: 0.3px;
            color: #ffffff;
        }

        #schedule .day-mobile 
        {
            font-family: 'Open Sans', sans-serif;
            font-size: 15px;
            font-size: 15px;
            letter-spacing: 0.1px;
            font-weight: 300;
            text-align: center;
            color: #ffffff;
        }

        #schedule .day-mobile p {
            margin-top: 60px;
        }

        .border-bottom {
            border-bottom: solid 1.5px #ec0b43;
        }

        .mt-md {
            margin-top: 50px; 
        }

    #subscribe {
        padding-left: 0px;
        padding-top: 15%;
        padding-bottom: 13%;
    }

    #subscribe .subscribe-form {
        z-index: 2;
    }
    @media screen and (max-width: 768px){
            #subscribe {
                padding-left: 15px;
                padding-right: 15px;
                padding-top: 15%;
                padding-bottom: 112%;
            }
        }
        #subscribe h4 {
            text-align: left;
        }
        #subscribe input {
            background-color: #181818;
            border: none;
            border-bottom: solid 0.5px #cfcfcf;
            width: 100%;
            margin-top: 35px;
            text-align: left;
            padding-left: 0px;
        }
        @media screen and (max-width: 768px){
            #subscribe h4, p {
                text-align: center;
            }
        }
        #subscribe .subs-form {
            padding-left: 0px;   
        }
        #subscribe .bottompic {
            position: absolute;
            width: 50%;
            right: -179px;
            top: -165px;
            z-index: 1;
        }
        @media screen and (max-width: 768px){
            #subscribe .bottompic {
                position: absolute;
                width: 88%;
                right: -81px;
                top: 125%;
            }

            #subscribe .subs-form {
                padding-left: 15px;   
            }
        }


    #rwc-pools .border-bottom {
        border-bottom: solid 0.5px #cfcfcf;
        padding-bottom: 80px;
    }
    #rwc-pools .pools-mobile {
        margin: 40px 0px;
    } 
    #rwc-pools .poolImg {
        width: 120px;
    }
    #rwc-pools .table>tbody>tr>td {
        padding: 20px;
        line-height: 1.42857;
        vertical-align: top;
        border-top: solid 0.5px #cfcfcf;
    }

    #rwc-pools .pools-table {
        padding: 0px 70px;
    }
    #rwc-pools .pools-text {
        padding: 0px 70px;
        text-align: center;
    }
    @media screen and (max-width: 768px){
         #rwc-pools .pools-text {
            padding: 0px 10px;
            text-align: left;
        }
    }
    </style>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar section-white" id="rwc19">
            <section class="pageCategory-section last" >
                <div class="wrapper">
                    <div class="introRWC cnyred">
                        <img src="images/rwc19/haka.png">
                    </div>   
                </div>
                <div id="fuji">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-offset-1 col-sm-10 text-center">
                                <div class="col-md-12 title">
                                    <h4><strong>SCRUM TICKETS, FLIGHT & HOTEL <br>COMING SOON IN 2019</strong></h4>
                                    <h1>LET US KNOW <br>YOUR PREFERENCES</h1>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- <div class="col-md-12 fujiImg">
                        <img class="img-responsive" src="images/rwc19/fuji.png">
                    </div> --}}
                </div>
            </section>

            <section class="pageCategory-section last" id="section-black">
                
                <div class="container" id="rwc19-center">
                     <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 text-center">
                            <div class="text-center">
                                <h4>WE ARE MAKING IT EASIER FOR YOU<br>TO GET TO THE RWC</h4>
                                <h6>Book match tickets</h6>
                                <h6>Enjoy flights to get you there</h6>
                                <h6>Reserve hotels of your choice</h6>
                            </div>
                            <div class="row col-xs-12 text-center monument1">
                                <span class="importantNote">*Place a deposit with us to secure your RWC match tickets.</span>
                                <div class="col-sm-12 col-xs-12">
                                    <a href="https://docs.google.com/forms/d/e/1FAIpQLSdSCn9H3Ws99VoNScEesOhuOzl5HSy6ueYKCuyKz2iHjOzm4g/viewform?c=0&w=1&includes_info_params=true" class="btn btn-rwc" target="_blank">Book tickets</a>
                                </div>    
                            </div>

                            <div class="row col-xs-12 text-center monument2">
                                <h4>NEED TO TACKLE SOME QUESTIONS?</h4>
                                <p>We provide you the answer in our help centre page</p>
                                <div class="col-sm-12 col-xs-12">
                                    <a href="/help" class="btn btn-rwc" target="_blank">Go to the page</a>
                                </div>    
                            </div>
                        </div>
                    </div>
                </div>

                <div class="container" id="schedule">
                     <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 text-center">
                            <h4>FINALS SCHEDULE</h4>
                            <div class="col-xs-12 col-md-offset-2 schedule-row">
                                <div class="col-md-12 col-xs-12 schedule-col">
                                    <div class="col-md-4 col-xs-12 scheduleImg"><img class="img-responsive center-block" src="images/rwc19/quarter-finals.png"></div>
                                    <div class="col-md-6 text-left hidden-xs hidden-sm">
                                        <div class="day">Saturday</div>
                                        <p>16th October 2019 & Sunday 17th October 2019</p>
                                    </div>
                                    <div class="col-md-6 text-center hidden-lg hidden-md day-mobile">
                                        <p>Saturday<br>
                                        16th October 2019 &<br>Sunday 17th October 2019</p>
                                    </div>
                                </div>
                                <div class="col-md-12 col-xs-12 schedule-col">
                                    <div class="col-md-4 col-xs-12 scheduleImg"><img class="img-responsive center-block" src="images/rwc19/semi-finals.png"></div>
                                    <div class="col-md-6 text-left hidden-xs hidden-sm">
                                        <div class="day">Saturday</div>
                                        <p>16th October 2019 & Sunday 17th October 2019</p>
                                    </div>
                                    <div class="col-md-6 text-center hidden-lg hidden-md day-mobile">
                                        <p>Saturday<br>
                                        16th October 2019 & Sunday 17th October 2019</p>
                                    </div>
                                </div>
                                <div class="col-md-12 col-xs-12 schedule-col">
                                    <div class="col-md-4 col-xs-12 scheduleImg"><img class="img-responsive center-block" src="images/rwc19/3rd-placing.png"></div>
                                    <div class="col-md-6 text-left hidden-xs hidden-sm">
                                        <div class="day">Saturday</div>
                                        <p>1st November 2019</p>
                                    </div>
                                    <div class="col-md-6 text-center hidden-lg hidden-md day-mobile">
                                        <p>Saturday<br>
                                        2nd November 2019</p>
                                    </div>
                                </div>  
                                <div class="col-md-12 col-xs-12 schedule-col">
                                    <div class="col-md-4 col-xs-12 scheduleImg"><img class="img-responsive center-block" src="images/rwc19/final.png"></div>
                                    <div class="col-md-6 text-left hidden-xs hidden-sm">
                                        <div class="day">Saturday</div>
                                        <p>2nd November 2019</p>
                                    </div>
                                    <div class="col-md-6 text-center hidden-lg hidden-md day-mobile">
                                        <p>Saturday<br>
                                        2nd November 2019</p>
                                    </div>
                                </div>    
                            </div>
                            <div class="col-xs-12 border-bottom"></div>
                        </div>
                    </div>   
                </div>
                <div class="container" id="rwc-pools">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10" style="text-align: center;">
                            <div class="text-center">
                                <h4>RWC 2019 POOLS<h4>
                            </div>
                            <div class="col-xs-12 pools-text">
                                The pools have been drawn for the Rugby World Cup 2019.  Host Japan are in Pool A, while the defending champion New Zealand are with South Africa in Pool B, England, France and Argentina in Pool C, and Australia, Wales and Georgia are in Pool D.
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="table-responsive hidden-xs hidden-sm pools-table">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th class="text-center"><img class="poolImg" src="images/rwc19/pool-a.png"></th>
                                            <th class="text-center"><img class="poolImg" src="images/rwc19/pool-b.png"></th>
                                            <th class="text-center"><img class="poolImg" src="images/rwc19/pool-c.png"></th>
                                            <th class="text-center"><img class="poolImg" src="images/rwc19/pool-d.png"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr style="border-top : hidden!important;">
                                            <td>Ireland</td>
                                            <td>New Zealand</td>
                                            <td>England</td>
                                            <td>Australia</td>
                                        </tr>
                                        <tr>
                                            <td>Scotland</td>
                                            <td>South Africa</td>
                                            <td>France</td>
                                            <td>Wales</td>
                                        </tr>
                                        <tr>
                                            <td>Japan</td>
                                            <td>Italy</td>
                                            <td>Argentina</td>
                                            <td>Georgia</td>
                                        </tr>
                                        <tr>
                                            <td>Europe 1</td>
                                            <td>Africa 1</td>
                                            <td>USA</td>
                                            <td>Fiji</td>
                                        </tr>
                                        <tr>
                                            <td>Play-off winner</td>
                                            <td>Repecharge winner</td>
                                            <td>Oceania 2</td>
                                            <td>Americas 2</td>
                                        </tr>
                                        
                                    </tbody>
                                </table>
                            </div>
                            <div class="row hidden-lg hidden-md">
                                <div class="col-xs-12 pools-mobile">
                                    <img class="poolImg" src="images/rwc19/pool-a.png">
                                    <div class="col-xs-12 text-center mt-md">
                                        <p>Ireland</p>
                                        <p>New Zealand</p>
                                        <p>England</p>
                                        <p>Australia</p>
                                        <p>Play-off winner</p>
                                    </div>
                                    <div class="col-xs-8 col-xs-offset-2 border-bottom"></div>
                                </div>
                                <div class="col-xs-12 pools-mobile">
                                    <img class="poolImg" src="images/rwc19/pool-b.png">
                                    <div class="col-xs-12 text-center mt-md">
                                        <p>Scotland</p>
                                        <p>South Africa</p>
                                        <p>France</p>
                                        <p>Wales</p>
                                        <p>Repecharge winner</p>
                                    </div>
                                    <div class="col-xs-8 col-xs-offset-2 border-bottom"></div>
                                </div>
                                <div class="col-xs-12 pools-mobile">
                                    <img class="poolImg" src="images/rwc19/pool-c.png">
                                    <div class="col-xs-12 text-center mt-md">
                                        <p>Japan</p>
                                        <p>Italy</p>
                                        <p>Argentina</p>
                                        <p>Georgia</p>
                                        <p>Oceania 2</p>
                                    </div>
                                    <div class="col-xs-8 col-xs-offset-2 border-bottom"></div>
                                </div>
                                <div class="col-xs-12 pools-mobile">
                                    <img class="poolImg" src="images/rwc19/pool-d.png">
                                    <div class="col-xs-12 text-center mt-md">
                                        <p>Europe 1</p>
                                        <p>Africa 1</p>
                                        <p>USA</p>
                                        <p>Fiji</p>
                                        <p>Americas 2</p>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                        </div>
                    </div>
                </div>
                <div class="container" id="subscribe">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 col-xs-12">
                            <div class="col-md-10 col-xs-12 subscribe-form">
                                <div class="row">
                                    <div class="col-md-12 col-xs-12">
                                        <h4>Psyched to watch the Japan Rugby World Cup?</h4>
                                        <p>Fill in your email and get the latest updates.</p>
                                    </div>
                                    <div class="col-md-12 col-xs-12">
                                        <form action="//AirAsiaRedtix.us14.list-manage.com/subscribe/post?u=563fc1bcdefdc4fca4efb1dbe&amp;id=719d8ddc42" method="POST" target="_blank">
                                            {{ csrf_field() }}
                                            <div class="form-row">
                                                <div class="col-sm-8 col-xs-12 subs-form">
                                                    <input type="email" name="email" id="email" placeholder="Enter your email"></input>
                                                </div>
                                                <div class="col-sm-4 col-xs-12 text-center" id="submit-form">
                                                    <button type="submit" class="btn btn-rwc" id="button-submit">Keep me updated</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="bottompic">
                                <img class="img-responsive" src="images/rwc19/bottompic.png">
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 450;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });
    </script>
    
@endsection

@section('modal')
    @include('layouts.partials.modals._seatplan')
    @include('layouts.partials.modals._getTix')
@endsection