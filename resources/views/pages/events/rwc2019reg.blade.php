@extends('masterforrwc2019reg')
@section('title')
    Rugby World Cup 2019
@endsection

@section('header')
    @include('layouts.partials._rwc2019header')
@endsection

@section('content')

<style>
.White-color{
    color:white !important;
}
.Back-color{
    background-color: white;
}
.Red-color {
    background-color:  #b82941;
}
.Black-color{
    color: black;
}
/* Banner RWC Start */
.Banner-BackGround{
    background-image: url(images/rwc2019reg/Banner.png);
    background-repeat: no-repeat, repeat;
    background-size: cover;
}
.Banner-BackGround-padding{
    padding-top: 112px;
    padding-bottom: 195px;
    border-radius: 0px !important;
    margin-bottom: 0px !important;

}
.Banner-Heading-Border{
    border-bottom: 2px solid #f9f6f3;
    width: 30%;
}
.banner-head-h4{
    font-family: "Lato", "Webb Ellis Cup";
}
.dispay-hand{
    display: none;
}
.banner-head-h1{
    font-family: "Lato", "Webb Ellis Cup";
}
/* Banner RWC END */

/*   Register Now  Start*/
.Register-Now-Banner{
    background-image: url(images/rwc2019reg/Register.jpeg);
    background-repeat: no-repeat, repeat;
    background-size: cover;
    background-position: 1px -523px;
}
.reg-small-button{
   margin-top:25px;
}
.banner-head-h4 {
    margin-bottom : 0px;
    font-size: 36px !important;

}
.banner-head-h1 {
margin-top : 0px;
font-size: 81px !important;
font-family: "Lato", "Webb Ellis Cup";
}
.Register-Now-Banner-padding{
    padding-right: 101px !important;
    padding-top: 56px;
    padding-bottom: 108px;
    border-radius: 0px !important;
    margin-bottom: 0px !important;

}
.Reg-small-H4{
    font-family: "Lato", "Webb Ellis Cup";
    font-weight: 700 !important;
    font-size: 36px;
}
.reg-p-small-float{
    font-family: "Lato", "Webb Ellis Cup";
    font-weight: 500 !important;
    font-size: 18px !important; 
}
.Register-Now-button-float{
    float: right;
    font-weight: 700 !important;
}
.reg-p-small-hight{
    margin-top: 41px;
}
.Register-Now-button {
    border: none;
    color: white;
    padding: 20px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    cursor: pointer;
    border-radius: 61px;
    padding: 11px 32px 12px 37px;
}
.padd-logo{
    padding-bottom: 790px;
}
/*   Register Now  End*/

/* RWC POOL   Start */
.RWC-POOL-Banner{
    background-image: url(images/rwc2019reg/Pool-Background.png);
    background-repeat: no-repeat, repeat;
    background-size: cover;
}
.RWC-POOL-padding{
    padding-top: 112px;
}
.pool-heading-marg{
    margin-bottom: 9px;
}
.pool-padding{
    padding: 0px 0px 1px 0px;
}
.setting-pool-margin{
    margin-bottom: 20px;
}
.RWC-POOL-Banner-h3-small{
    font-family: "Lato", "Webb Ellis Cup";
    font-size: 34px !important;
}
.RWC-POOL-Banner-p{
    font-family: "Lato", "Webb Ellis Cup";
    font-weight: 700 !important;
    font-size: 17px !important;
}
.dotted{
    display: none;
    font-size: 10px;
}
.pool-ul{
    background: red;
    padding-bottom: 12px;
    border-radius: 5px;
}
.pool-li-size {
   font-size: 20px;
}
.pool-li{
    margin: 5px;
    border-radius: 5px;
}
.pool-match{
    padding: 0px 8% 0px 8%;
    margin-top: 74px ;
}
.Register-watch-Now-Banner{
    background-image: url(images/rwc2019reg/Register-Pool.jpg);
    background-repeat: no-repeat, repeat;
    background-size: cover;
    background-position: 1px -143px;
    padding-top: 90px !important;
}
.Register-watch-h1{
font-size: 29px !important;
}
.Register-watch-p{
    font-family: "Lato", "Webb Ellis Cup";
    font-size: 18px !important;
}
.RWC-POOL-CONTAINER{
margin-top: 89px;
}
.Match-Pool-img{
    margin-top:48px;
}
.RWC-POOL-Banner{
    margin-bottom: 0px;
}

.Reg-email::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
    color: white;
   
}
.Reg-email{

    text-align: center;
    color: white;
    background-color: transparent;
    border: none;
    border-bottom: 1px solid white;
    border-radius: 0;
    outline: none;
    height: 3rem;
    width: 76%;
    font-size: 16px;
    margin: 0 0 8px 0;
    padding: 0;
    -webkit-box-shadow: none;
    box-shadow: none;
    -webkit-box-sizing: content-box;
    box-sizing: content-box;
    -webkit-transition: border .3s, -webkit-box-shadow .3s;
    transition: border .3s, -webkit-box-shadow .3s;
    transition: box-shadow .3s, border .3s;
    transition: box-shadow .3s, border .3s, -webkit-box-shadow .3s;
}
.Reg-email-label{
    color: white;
    position: absolute;
    top: -15px;
    left: 0;
    font-size: 1rem;
    cursor: text;
    -webkit-transition: color .2s ease-out, -webkit-transform .2s ease-out;
    transition: color .2s ease-out, -webkit-transform .2s ease-out;
    transition: transform .2s ease-out, color .2s ease-out;
    transition: transform .2s ease-out, color .2s ease-out, -webkit-transform .2s ease-out;
    -webkit-transform-origin: 0% 100%;
    transform-origin: 0% 100%;
    text-align: initial;
    -webkit-transform: translateY(12px);
    transform: translateY(12px);
    margin-left: 45%;
    font-size: 18px;
    font-weight: 700;
}
.Reg-div-email{
    position: relative;
    width: 60%;
    margin: auto;
}
.Register-watch-Now-Banner-h3{
    font-family: "Lato", "Webb Ellis Cup";
    font-weight: 700 !important;

}
.Register-watch-Now-Banner-p{
    font-family: "Lato", "Webb Ellis Cup";
    font-weight: 700 !important;

}
/* RWC POOL   End */

/* footer */
.footerbanner {
    background-image: url(images/rwc2019reg/footer.png);
    background-repeat: no-repeat, repeat;
    background-size: cover;
}
.Add-footer-size{
    padding: 5% 5% 5% 5%;
}


.footer-col1 {
    text-align: right;
    letter-spacing: 0.02em;
    color: #FFFFFF;
    font-family: "Lato", "Webb Ellis Cup";
}
.footer-col2-ul li{
    font-family: "Lato", "Webb Ellis Cup";
    list-style: none;
    margin-bottom: 6px;
}
.footer-col2-ul li a{
    /* COLLECTIONS */
    font-family: "Lato", "Webb Ellis Cup";
    color: #FFFFFF ;
    font-style: normal;
    font-weight: 500;
    line-height: normal;
    font-size: 12px;
    letter-spacing: 0.02em;
}

.footer-social-navlist li{
    display: inline;
    list-style-type: none;
    padding-left: 0px;
    margin-right:30px; 
}
.footerbannerooter-col3 p{
    font-weight: 300;
    line-height: normal;
    font-size: 12px;
    color: #FFFFFF ;
    font-family: "Lato", "Webb Ellis Cup";
    text-align: center;
}
.footer-col3 p{
    font-family: "Lato", "Webb Ellis Cup";
    color: white;
    text-align: center;
}
.footer-col4 li a{
    color: #FFFFFF !important;
    font-family: "Lato", "Webb Ellis Cup";
    font-style: normal;
    font-weight: 300;
    font-size: 13px;
    letter-spacing: 0.02em;
    list-style-type: none;
}
.footer-col4 li{
    list-style-type: none;
}
.footer-col1-margin{
    margin-top: 36px;
}
.footer-col2-margin{
    margin-top: 42px;
}
.footer-col3-margin{
    margin-top: 42px;
    font-family: "Lato", "Webb Ellis Cup";
    font-style: normal;
    font-size: 13px;
    letter-spacing: 0.02em;
    list-style-type: none;
}
.footer-col4-margin{
    margin-top: 42px;
}
.footer-col5{
    text-align: center;
    margin-top:80px;
    color: white;
    font-size: 10px;
/* footer */
}
.Add-footer-size{
    padding: 5% 5% 5% 5%;
}
.footer-col1 {
    text-align: right;
    letter-spacing: 0.02em;
    color: #FFFFFF;
}
.footer-col2-ul li{
    font-family: "Lato", "Webb Ellis Cup";
    list-style: none;
    margin-bottom: 6px;
}
.footer-col2-ul li a{
    /* COLLECTIONS */
    font-family: "Lato", "Webb Ellis Cup";
    color: #FFFFFF ;
    
    font-style: normal;
    font-weight: 500;
    line-height: normal;
    font-size: 12px;
    letter-spacing: 0.02em;
}
.footer-social-navlist li{
    font-family: "Lato", "Webb Ellis Cup";
    display: inline;
    list-style-type: none;
    padding-left: 0px;
    margin-right:30px; 
}
.footer-col3 p{
    color: white;
    text-align: center;
}
.footerbannerooter-col3 p{
    font-weight: 300;
    line-height: normal;
    font-size: 12px;
    color: #FFFFFF ;
    font-family: "Lato", "Webb Ellis Cup";
    text-align: center;
}
.footer-col4 li a{
    font-family: "Lato", "Webb Ellis Cup";
    color: #FFFFFF !important;
    font-style: normal;
    font-weight: 300;
    font-size: 13px;
    letter-spacing: 0.02em;
    list-style-type: none;
}
.footer-col4 li{
    list-style-type: none;
}
.footer-col1-margin{
    margin-top: 36px;
}
.footer-col3-margin{
    font-family: "Lato", "Webb Ellis Cup";
    margin-top: 42px;
}
.footer-col4-margin{
    font-family: "Lato", "Webb Ellis Cup";
    margin-top: 42px;
}
.footer-col5{
    font-family: "Lato", "Webb Ellis Cup";
    text-align: center;
    margin-top:80px;
    letter-spacing: 0.02em;
    color: #FFFFFF;
    font-size: 10px;
}
.footer-col2-ul li{
    font-family: "Lato", "Webb Ellis Cup";
    list-style: none;
    margin-bottom: 6px;
}
.footer-col2-ul li a{
    /* COLLECTIONS */
    color: #FFFFFF ;
    font-family: "Lato", "Webb Ellis Cup";
    font-style: normal;
    font-weight: 300;
    line-height: normal;
    font-size: 12px;
    letter-spacing: 0.02em;
}
.footer-social-navlist li{
    display: inline;
    list-style-type: none;
    padding-left: 0px;
    margin-right:30px; 
}
.footer-col3 p{
    color: white;
    text-align: center;
}
.footerbannerooter-col3 p{
    font-weight: 300;
    line-height: normal;
    font-size: 12px;
    color: #FFFFFF ;
    font-family: "Lato", "Webb Ellis Cup";
    text-align: center;
}
.footer-col4 li a{
    font-family: "Lato", "Webb Ellis Cup";
    color: #FFFFFF !important;
    font-style: normal;
    font-weight: 300;
    font-size: 13px;
    letter-spacing: 0.02em;
    list-style-type: none;
}
.footer-col4 li{
    list-style-type: none;
}
.footer-col1-margin{
    margin-top: 36px;
}
.footer-col3-margin{
    margin-top: 42px;
}
.footer-col4-margin{
    margin-top: 42px;
}
.footer-col5{
    text-align: center;
    margin-top:80px;
    font-size: 10px;
}
.fa {
    color: white;
    font-size: 20px;
}
@media (min-width: 1281px) {
    .dispay-hand{
        display: none;
    }
}
@media (min-width: 768px) and (max-width: 1024px) {
    .Register-Now-Banner {
        background-image: url(images/rwc2019reg/Register.jpeg);
        background-repeat: no-repeat, repeat;
        background-size: cover;
        background-position: 1px 1px;
    }
    .footerbanner {
        background-image: url(images/rwc2019reg/footer.png);
        background-repeat: no-repeat, repeat;
        background-size: cover;
        background-position: -64px -6px;
    }
    .dispay-hand{
        display: none;
    }
    .padd-logo {
    padding-bottom: 372px;
    }
    .Register-watch-Now-Banner {
        background-image: url(images/rwc2019reg/Register-Pool.jpg);
        background-repeat: no-repeat, repeat;
        background-size: cover;
        background-position: 1px -55px;
        padding-top: 90px !important;
    }
    .footer-col1-margin {
        margin-top: 36px;
        width: 50%;
        float: left;
        overflow: hidden;
    }
    .footer-col2-margin {
        float: left;
        width: 50%;
    }
    .footer-col3-margin {
        margin-top: 42px;
        float: left;
        overflow: hidden;
    }
}
/* 
  ##Device = Tablets, Ipads (landscape)
  ##Screen = B/w 768px to 1024px
*/
@media (min-width: 768px) and (max-width: 1024px) and (orientation: landscape) {
    .Register-Now-Banner {
        background-image: url(images/rwc2019reg/Register.jpeg);
        background-repeat: no-repeat, repeat;
        background-size: cover;
        background-position: 1px -523px;
    }
    .footerbanner {
        background-image: url(images/rwc2019reg/footer.png);
        background-repeat: no-repeat, repeat;
        background-size: cover;
    }
    .dispay-hand{
        display: none;
    }
}
/* 
  ##Device = Low Resolution Tablets, Mobiles (Landscape)
  ##Screen = B/w 481px to 767px
*/
@media (min-width: 481px) and (max-width: 767px) {
    .Banner-BackGround-padding{
        padding-top: 324px;
        padding-bottom: 36px;
    }
    .Banner-row{
        margin-left: 2px;
    }
    .Banner-Heading-Border {
        margin-bottom: 9px;
    }
     .banner-head-h4{
        font-size: 29px !important;
        margin: 0px;
     }
     .banner-head-h1{
        font-size: 31px;
        margin: 0px;
     }
     .Register-Now-Banner {
        background-image: url(images/rwc2019reg/Register.jpeg);
        background-repeat: no-repeat, repeat;
        background-size: cover;
        background-position: 0px 0px;
    }
    .Reg-small-H4{
        text-align: center !important;
        font-size: 23px;
    }
    .Register-Now-Banner-padding {
        text-align: center !important;
    }
    .reg-p-small-float{
        float: left;
        font-size: 18px;
    }
    .reg-p-small-fl1{
       margin: 1%;
    }
    .Register-Now-button {
        border: none;
        color: white;
        padding: 20px;
        text-align: center;
        text-decoration: none;
        display: table-cell;
        font-size: 16px;
        margin: 4px 2px;
        cursor: pointer;
        border-radius: 61px;
        padding: 11px 32px 12px 37px;
    }
    .Register-Now-Banner-padding {
        padding-top: 89px;
        padding-bottom: 185px;
    }
    .reg-small-path{
        text-align: center;
        margin: auto;
        display: block;
        width: 100%;
        overflow: hidden;
        display: flex;
        justify-content: center;
        overflow: hidden;
    }    
    .reg-small-button{
        display: flex;
        justify-content: center;
        overflow: hidden;
    }
    .RWC-POOL-Banner-p{
        font-size: 11px !important;
        width: 80%;
        text-align: center;
        margin: auto;
    }
    .RWC-POOL-Banner-h3-small{
        font-size: 15px;
    }
    .Register-Now-Banner-padding {
        padding-right: 15px !important;}
    .dotted {
        display: flex;
        justify-content: center;
        font-size: 21px !important;
        color: #efedea;
        line-height: 6px !important;
    }
    .footer-col1-margin {
        margin-top: 6px;
    }
    .footer-col3-margin {
        margin-top: 10px;
    }
  .reg-p-small-fl1{
       margin: 1%;
    }
    .Register-Now-button {
        border: none;
        color: white;
        padding: 20px;
        text-align: center;
        text-decoration: none;
        display: table-cell;
        font-size: 16px;
        margin: 4px 2px;
        cursor: pointer;
        border-radius: 61px;
        padding: 11px 32px 12px 37px;
    }
    .Register-Now-Banner-padding {
        padding-top: 89px;
        padding-bottom: 185px;
    }
    .reg-small-path{
        text-align: center;
        margin: auto;
        display: block;
        width: 100%;
        overflow: hidden;
        display: flex;
        justify-content: center;
        overflow: hidden;
    }    
    .reg-small-button{
        display: flex;
        justify-content: center;
        overflow: hidden;
    }
    .RWC-POOL-Banner-p{
        font-size: 11px !important;
        width: 80%;
        text-align: center;
        margin: auto;
        line-height: 18px;
    }
    .RWC-POOL-Banner-h3-small{
        font-size: 15px;
    }
    .footer-col2-margin {
        display: none;
    }
    .footer-col4-margin {
        display: none;
    }
    .footer-col1 {
        text-align: center;
        font-size: 10px;
    }
    .footer-social-navlist{
        text-align: center;
    }
    .footerbannerooter-col3 p{
        font-weight: 300;
        line-height: normal;
        font-size: 12px;
        color: #FFFFFF ;
        font-family: "Lato", "Webb Ellis Cup";
        text-align: center;
    }
    .follow-p{
        font-family: "Lato", "Webb Ellis Cup";
        text-align: center;
        color: white;
    }
    .displayhide{
        display: none;
    }
    .Register-Now-Banner-padding {
        padding-right: 15px !important;}
    .reg-p-small-fl1 {
        margin: 0px;
    }
    .dotted {
        display: flex;
        justify-content: center;
        font-size: 21px !important;
        color: #efedea;
        line-height: 6px !important;
    }
    .footer-col1-margin {
        margin-top: 6px;
    }
    .footer-text{
        font-size: 10px !important;
        float: right !important;
        width: 65% !important;
    }
    .footer-col5 {
        font-family: "Lato", "Webb Ellis Cup";
        text-align: center;
        font-size: 10px;
    }
    .footerbanner {
        background-image: url(images/rwc2019reg/small-footer.png);
        background-repeat: no-repeat, repeat;
        background-size: cover;
        height: 100vh;
    }
    .dispay-hand{
        display: block;
    }
    .banner-head-h1{
        font-size: 17px !important;
    }
    .Reg-small-H4 {
        text-align: center !important;
    }
    .banner-head-h1{
        font-size: 29px !important;
    }
    .Register-watch-h1 {
        font-size: 24px !important;
    }
    .Register-watch-p {
        font-family: "Lato", "Webb Ellis Cup";
        font-size: 14px !important;
    }
    .Register-watch-Now-Banner {
        background-image: url(images/rwc2019reg/Register-Pool.jpg);
        background-repeat: no-repeat, repeat;
        background-size: cover;
        background-position: 1px 1px;
        padding-top: 90px !important;
    }
    .Reg-small-H4 {
        font-family: "Lato", "Webb Ellis Cup";
        font-weight: 700 !important;
        font-size: 18px;
    }
    .Banner-BackGround-padding {
        padding-top: 0px; 
        padding-bottom: 25px;
    }
    .div-logo{
        padding-top: 72px;
        padding-left: 25px;
        width: 80px;
        height: 80px;
    }
    .Banner-Heading-Border {
        margin-bottom: 9px;
        padding-top: 223px;
    }

    .reg-p-small-hight {
        margin-top: 0px;
    }
    .padd-logo {
        padding-bottom: 0px;
    }
    .banner-head-h1 {
        font-size: 31px !important;
    }
    .RWC-POOL-Banner-p {
        font-size: 10px !important;
        width: 80%;
        text-align: center;
        margin: auto;
        line-height: 18px;
    }
    .Register-watch-h1 {
        font-size: 29px !important;
    }
    .Register-Now-Banner {
        background-image: url(images/rwc2019reg/Register.jpeg);
        background-repeat: no-repeat, repeat;
        background-size: cover;
        background-position: 1px 0px;
    }
    .reg-p-small-float {
        font-weight: 500 !important;
        font-size: 18px !important;
        margin-right: 9px;
    }
    .Reg-email {
        text-align: center;
        color: white;
        background-color: transparent;
        border: none;
        border-bottom: 1px solid white;
        border-radius: 0;
        outline: none;
        height: 3rem;
        width: 100%;
        font-size: 16px;
        margin: 0 0 8px 0;
        padding: 0;
        -webkit-box-shadow: none;
        box-shadow: none;
        -webkit-box-sizing: content-box;
        box-sizing: content-box;
        -webkit-transition: border .3s, -webkit-box-shadow .3s;
        transition: border .3s, -webkit-box-shadow .3s;
        transition: box-shadow .3s, border .3s;
        transition: box-shadow .3s, border .3s, -webkit-box-shadow .3s;
    }
    .follow-p {
        text-align: center;
        color: white;
        font-size: 700;
        font-weight: 700;
    }
    .Reg-div-email {
        position: relative;
        width: 94%;
        margin: auto;
    }
    .RWC-POOL-padding {
        padding-top: 7px;
    }
}
/* 
  ##Device = Most of the Smartphones Mobiles (Portrait)
  ##Screen = B/w 320px to 479px
*/
@media (min-width: 320px) and (max-width: 480px) {
    /* Banner RWC Start */
    .Banner-BackGround{
        background-image: url(images/rwc2019reg/banner-mobile.png);
        background-repeat: no-repeat, repeat;
        background-size: cover;
        height: 100vh;
    }
    .Banner-BackGround-padding {
        padding-top : 324px;
        padding-bottom : 36px;
    }
    .Banner-row {
        margin-left : 2px;
    }
    .Banner-Heading-Border {
        margin-top: 225px;
        margin-bottom: 9px;
    }
    .banner-head-h4{
        font-size : 15px !important;
        margin : 0px;
    }
    .footer-col3-margin {
        margin-top : 10px;
    }
    .Register-Now-Banner {
        background-image : url(images/rwc2019reg/Register.jpeg);
        background-repeat : no-repeat, repeat;
        background-size : cover;
        background-position : 0px 0px;
    }
    .Reg-small-H4{
        margin-top : 10px; 
        text-align : center !important;
        font-size : 23px;
    }
    .Register-Now-Banner-padding {
        text-align : center !important;
    }
    .reg-p-small-float{
        float : left;
        font-size : 18px;
    }
    .reg-p-small-fl1{
       margin : 1%;
    }
    .Register-Now-button {
        border : none;
        color : white;
        padding : 20px;
        text-align : center;
        text-decoration : none;
        display : table-cell;
        font-size : 16px;
        margin : 4px 2px;
        cursor : pointer;
        border-radius : 61px;
        padding : 11px 32px 12px 37px;
    }
    .Register-Now-Banner-padding {
        padding-top : 89px;
        padding-bottom : 185px;
    }
    .reg-small-path{
        text-align : center;
        margin : auto;
        display : block;
        width : 100%;
        overflow : hidden;
        display : flex;
        justify-content : center;
        overflow : hidden;
    }    
    .reg-small-button{
        display : flex;
        justify-content : center;
        overflow : hidden;
    }
    .RWC-POOL-Banner-p {
        font-size : 11px !important;
        width : 80%;
        text-align : center;
        margin : auto;
    }
    .RWC-POOL-Banner-h3-small {
        font-size : 15px;

    }
    .footer-col2-margin {
        display : none;
    }
    .footer-col4-margin {
        display : none;
    }
    .footer-col1 {
        text-align : center;
        font-size: 10px;
    }
    .footer-social-navlist {
        text-align : center;
    }
    .footerbannerooter-col3 p{
        font-weight: 300;
        line-height: normal;
        font-size: 12px;
        color: #FFFFFF ;
        font-family: "Lato", "Webb Ellis Cup";
        text-align: center;
    }
    .follow-p {
        font-family : "Lato", "Webb Ellis Cup";
        text-align : center;
        color : white;
    }
    .displayhide {
        display : none;
    }
    .Register-Now-Banner-padding {
        padding-right : 15px !important;
    }
    .reg-p-small-fl1 {
        margin : 0px;
    }
    .dotted {
        display : flex;
        justify-content : center;
        font-size: 21px !important;
        color: #efedea;
        line-height: 6px !important;
    }
    .footer-col1-margin {
        margin-top : 6px;
    }
    .footer-col5 {
        text-align: center;
        font-size: 10px;
        
    }
    .footerbanner {
        background-image: url(images/rwc2019reg/small-footer.png);
        background-repeat: no-repeat, repeat;
        background-size: cover;
        height: 100vh;
    }
    .dispay-hand {
        display : block;
    }
    .banner-head-h1 {
        font-size : 17px !important;
    }
    .Reg-small-H4 {
         text-align : center !important;
    }
    .footer-text {
        font-size : 10px !important;
        float : right !important;
        width : 65% !important;
    }
    .Register-watch-h1 {
        font-size : 24px !important ;
    }
    .Register-watch-p {
        font-family : "Lato", "Webb Ellis Cup";
        font-size : 14px !important ;
    }
    .Register-watch-Now-Banner {
        background-image : url(images/rwc2019reg/Register-Pool.jpg);
        background-repeat : no-repeat, repeat;
        background-size : cover;
        background-position : 1px 1px;
        padding-top : 90px !important ;
    }
    .Reg-small-H4 {
        font-family : "Lato", "Webb Ellis Cup";
        font-weight : 700 !important ;
        font-size : 18px;
    }
    .Banner-BackGround-padding {
        padding-top : 0;
        padding-bottom : 25px;
    }
    .div-logo {
        padding-top : 72px;
        padding-left : 25px;
        width : 80px;
        height : 80px;
    }
    .Banner-Heading-Border {
        margin-bottom : 9px;
        padding-top : 223px;
    }
    .reg-p-small-hight {
        margin-top : 0;
    }
    .padd-logo {
        padding-bottom : 0;
    }
    .banner-head-h1 {
        font-size : 31px !important ;
    }
    .RWC-POOL-Banner-p {
        font-size : 10px !important ;
        width : 80%;
        text-align : center;
        margin : auto;
        line-height: 18px;
    }
    .Register-watch-h1 {
        font-size: 29px !important;
    }
    .Register-Now-Banner {
        background-image: url(images/rwc2019reg/Register.jpeg);
        background-repeat: no-repeat, repeat;
        background-size: cover;
        background-position: 1px 0px;
    }
    .reg-p-small-float {
        font-weight: 500 !important;
        font-size: 18px !important;
        margin-right: 9px;
    }
    .Reg-email {
        text-align: center;
        color: white;
        background-color: transparent;
        border: none;
        border-bottom: 1px solid white;
        border-radius: 0;
        outline: none;
        height: 3rem;
        width: 100%;
        font-size: 16px;
        margin: 0 0 8px 0;
        padding: 0;
        -webkit-box-shadow: none;
        box-shadow: none;
        -webkit-box-sizing: content-box;
        box-sizing: content-box;
        -webkit-transition: border .3s, -webkit-box-shadow .3s;
        transition: border .3s, -webkit-box-shadow .3s;
        transition: box-shadow .3s, border .3s;
        transition: box-shadow .3s, border .3s, -webkit-box-shadow .3s;
    }
    .follow-p {
        text-align: center;
        color: white;
        font-size: 700;
        font-weight: 700;
    }
    .Reg-div-email {
        position: relative;
        width: 94%;
        margin: auto;
    }
    .RWC-POOL-padding {
        padding-top: 7px;
    }
}
</style>

 <div class="container-fluid">
    <div class="row">
        <!-- Banner RWC Start-->
        <div class="Banner-BackGround  Banner-BackGround-padding jumbotron ">
            <div class="row Banner-row">
            <div class="div-logo"><img class="padd-logo img-responsive" src="images/rwc2019reg/logo.png" alt="Chania"></div>
                <div class="Banner-Heading-Border"></div>
                <h4 class="White-color banner-head-h4">RUGBY WORLD CUP 2019
                    <sup>TM</sup> JAPAN </h4>
                <h1 class="White-color banner-head-h1" >TICKETS COMING SOON</h1>
            </div>
        </div>
        <!-- Banner RWC End-->

        <!-- Register Now Start-->
        <div class="Register-Now-Banner  Register-Now-Banner-padding Register-Now-padding jumbotron ">
            <h4 class="White-color text-right Reg-small-H4">WE ARE MAKING IT EASIER FOR YOU
                <br> TO GET TO RUGBY WORLD CUP 2019
            </h4>
            <p class="dotted">.<br>.<br>.<br>.<br>.<br>.<br>.<br></p>
            <div class="reg-small-path">
            <p class="White-color text-right reg-p-small-float reg-p-small-hight">Match tickets</p>
            <p class="White-color text-right reg-p-small-float">+</p>
            <p class="White-color text-right reg-p-small-float">Flights </p>
            <p class="White-color text-right reg-p-small-float">+</p>
            <p class="White-color text-right reg-p-small-float">Hotels</p>
        </div>
        <p class="dotted">.<br>.<br>.<br>.<br>.<br>.<br>.<br></p>
        {{-- <div class="reg-small-button">
            <a href="" class="Register-Now-button Back-color Register-Now-button-float">Register now</a>
        </div>--}}
    </div>
    <!-- Register Now End-->

    <!-- RWC POOL Start-->
    <div class="jumbotron RWC-POOL-Banner">
        <div class="row">
            <div class="container RWC-POOL-padding">
                <div class="row">
                        <h3 class="White-color text-center RWC-POOL-Banner-h3-small"> RUGBY WORLD CUP 2019 POOLS </h3>
                        <p class="White-color text-center RWC-POOL-Banner-p"> The pools have been drawn for Rugby World Cup 2019. Hosts Japan are in pool A, while the
                        <br> defending champions New Zealand are with South Africa in pool B, England, France and Argentina in
                        <br> pool C, and Australia, Wales and Georgia are in pool D.
                        </p>
                    </div>
                    <div class="row">
                        <div class="container RWC-POOL-CONTAINER">
                            <div class="col-md-3 setting-pool-margin">
                                <img class="img-responsive" src="images/rwc2019reg/poolA.png"  alt="Chania" > 
                            </div>
                            <div class="col-md-3 text-center setting-pool-margin">
                                <img class="img-responsive" src="images/rwc2019reg/PoolB.png"  alt="Chania" > 
                            </div>
                            <div class="col-md-3 setting-pool-margin" >
                                <img class="img-responsive" src="images/rwc2019reg/PoolC.png"  alt="Chania" > 
                            </div>
                            <div class="col-md-3 setting-pool-margin">
                                <img class="img-responsive" src="images/rwc2019reg/PoolD.png"  alt="Chania" > 
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="container pool-match">
                            <div> <img class="img-responsive" src="images/rwc2019reg/MatchSchedule-ext.jpg"  alt="Chania" > </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- RWC POOL End-->

        <!-- RWC REG Now -->
        <div class="Register-watch-Now-Banner  Register-Now-Banner-padding Register-Now-padding jumbotron text-center">
            <h3 class="White-color text-center  Register-watch-h1">Psyched to watch Rugby World Cup 2019?</h3> 
            <p class="White-color text-center  Register-watch-p">Register now and get access to the best prices</p>
            {{-- <div class="Reg-div-email">
                    <input id="email" type="email" class="Reg-email" placeholder="Email" >
            </div> --}}
            <div class="reg-small-button">
                <a href="https://goo.gl/forms/QQRlXSBKuu1UJxB23" class="Register-Now-button Red-color ">Register now</a>
            </div>
        </div>
        <!-- RWC END Now -->
    </div>
</div>
@endsection
