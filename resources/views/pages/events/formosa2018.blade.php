@extends('master')
@section('title')
    FORMOSA in Kuala Lumpur 2019 by Cloud Gate Dance Theatre
@endsection

@section('header')
    @include('layouts.partials._header')
    <meta property="og:url" content="{{ Request::fullUrl() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="FORMOSA in Kuala Lumpur 2019 by Cloud Gate Dance Theatre" />
    <meta property="og:description" content="In the 16th century, gazing out from the decks of ships off the coast of Southern China, Portuguese sailors saw an island thick with mountains and trees, rising from the sea. “Formosa!” they exclaimed—“beautiful!”—anointing the verdant place that would come to be known as Taiwan."/>
    <meta property="og:image" content="{{ Request::Url().'images/formosa2018/thumbnail.jpg' }}" />
@endsection

@section('content')
    <!-- Banner Section -->
    <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/formosa2018/web-banner.jpg')}}" style="width: 100%" class="img-responsive" alt="FORMOSA in Kuala Lumpur 2019 by Cloud Gate Dance Theatre"></div>
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            <img src="{{asset('images/formosa2018/thumbnail.jpg')}}" style="width: 100%" class="img-responsive" alt="FORMOSA in Kuala Lumpur 2019 by Cloud Gate Dance Theatre">
        </div>
    </section>
    <!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
            <div class="row priceNbtn">
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="row">
                        <div class="col-sm-9 leftBox">
                            <h6>FORMOSA in Kuala Lumpur 2019 by Cloud Gate Dance Theatre</h6> Tickets from <span>RM98</span>
                        </div>
                        <div class="col-sm-3 text-center">
                            <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar section-white">
            <section class="pageCategory-section last">
                <div class="container intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i>  16th - 17th March 2019</div>
                            <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i>  Panggung Sari, Istana Budaya Kuala Lumpur <a target="_blank" href="https://goo.gl/maps/xDVBXk3rECr">View Map</a></div>
                            <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i>16th March 2019, 8.30pm</div>
                            <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i>17th March 2019, 3.00pm</div>
                            <div class="clearfix">&nbsp;</div>
                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <div class="addthis_inline_share_toolbox"></div>
                            <!-- /sharing -->
                            <div class="col-sm-12">
                                <hr>
                                <p><h2>FORMOSA in Kuala Lumpur 2019 by Cloud Gate Dance Theatre</h2><br/>
                                In the 16th century, gazing out from the decks of ships off the coast of Southern China, Portuguese sailors saw an island thick with mountains and trees, rising from the sea. “Formosa!” they exclaimed—“beautiful!”—anointing the verdant place that would come to be known as Taiwan.</p>
                                <p>In this full-length work choreographer Lin Hwai-min uses Formosa, a beautiful island beset by earthquakes, typhoons and societal rifts, as a metaphor of the world we live in and to contemplate an epigram from the Buddhist Diamond Sutra:<br/><br/>
                                    All things contrived are like dream, illusion, bubble, shadow,<br/>
                                    and as dewdrop or lightning. They should be regarded as such.</p>
                                    <p>While recorded indigenous songs, contemporary music, reading of poems about the land, and lore of the island serve as the soundscape, luminous projected images of Chinese character typefaces provide visual backdrop. The characters are mainly names of mountains and rivers, cities and villages of Taiwan; they interlock, overlap and merge in teeming thickets to evoke a host of stunning imagery.</p>
                                <p>Against the lustrous, transfigured sphere, dancers mingle in intimations of community, making tribal ritual and urban bustle seem as one and transforming the stage as a playground of love and life, mediated by tragedy, hope, and rebirth.</p>  
                                <p>Towards the end, blocks of characters drop onto stage, like landslide during earthquake, then characters gradually fall apart into dispersed strokes and lines. They seem to imply writing as a precarious vehicle for memories, which blur and recombine at the whim of history’s wind.</p>
                                <p>A blue sea appears, only to wash the broken characters away in raging waves. The dancers exit, projection vanishes, leaving a white, empty stage just as the dance opens.</p>
                            </div>
                            <div class="clearfix"></div>
                            <div class="embed-responsive embed-responsive-16by9" style="margin-top: 40px; margin-bottom: 40px;">
                                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/2FNcP-fiI5M?ecver=2" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- <div class="clearfix"></div> --}}
            </section>

            <section class="pageCategory-section last section-grey">
                <div class="container">
                    <div class="gallery text-center">
                        <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
                        <!-- Swiper -->
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <a href="images/formosa2018/gallery-1.jpg" data-featherlight="image"><img class="" src="images/formosa2018/gallery-1.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/formosa2018/web-banner.jpg" data-featherlight="image"><img class="" src="images/formosa2018/web-banner.jpg" alt=""></a>
                                </div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next swiper-button-white"></div>
                            <div class="swiper-button-prev swiper-button-white"></div>
                        </div><!-- /Swiper -->
                    </div>
                </div>
            </section>

            <section class="pageCategory-section last"><a id="anchorPrice"></a>
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                            <div class="text-center">
                                <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                                <p>Select ticket</p>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="table-responsive">
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Category</th>
                                            <th>Normal Price</th>
                                            <th>Early Bird Promotion</th>
                                            <th>Student Promotion</th>
                                            <th>Seat Plan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td style="background-color:#FFFF06; color:#000;">Cat 1</td>
                                            <td>RM468</td>
                                            <td>-</td>
                                            <td>-</td>
                                            <td rowspan="5"><img class="img-responsive seatPlanImg" src="images/formosa2018/seat-plan.jpg" alt=""></td>
                                        </tr>
                                        <tr>
                                            <td style="background-color:#FF6698; color:#000;">Cat 2</td>
                                            <td>RM368</td>
                                            <td>-</td>
                                            <td>-</td>
                                        </tr>
                                        <tr>
                                            <td style="background-color:#90D050; color:#fff;">Cat 3</td>
                                            <td>RM268</td>
                                            <td>RM228</td>
                                            <td>-</td>
                                        </tr>
                                        <tr>
                                            <td style="background-color:#08AFF0; color:#fff;">Cat 4</td>
                                            <td>RM168</td>
                                            <td>RM150</td>
                                            <td>-</td>
                                        </tr>
                                        <tr>
                                            <td style="background-color:#E26B0A; color:#fff;">Cat 5</td>
                                            <td>RM128</td>
                                            <td>RM115</td>
                                            <td>RM98</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="buyAlert-bar">
                                <a class="btn btn-danger" id="buyButton" datetime="March 15 2019 00:00:00 GMT+0800" target="_blank" href="https://redtix-tickets.airasia.com/en-AU/shows/formosa 2019/events" role="button">BUY TICKETS</a>
                                {{-- <span class="or">/</span>
                                <span class="popData-btn" data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets <i class="fa fa-info-circle" aria-hidden="true"></i></span> --}}
                            </div>
                            {{-- <div class="note text-left">
                                <h2>Admission Age Limit</h2>
                                <ol>
                                    <li>Below 3 years - FOC (No Seating)</li>
                                    <li>Above 3 years - Full Normal Price (With Seating)</li>
                                </ol>
                            </div> --}}
                            <div class="note text-left">
                                <h2>Important Notes</h2>
                                <ol>
                                    <li>Prices shown inclusive of RM4.00 ticket fee and credit card fee.</li>
                                    <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li>
                                    <li>RM10.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                                    <li>Online ticket selling will close 2 days prior to event day, subject to availability.</li>
                                </ol>
                                <h2>For enquiry only:</h2>
                                <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                            </div>

                        </div>
                    </div>
                </div>
            </section>
        </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 350;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });
    </script>

    {{-- Buy button disable --}}
    <script type="text/javascript">
        $(function() {
            $('a[id^=buyButton]').each(function() {
                var date = new Date();
                var enddate = $(this).attr('datetime'); 
                if ( Date.parse(date) >= Date.parse(enddate)) {
                  $(this).addClass('disabled');
                }
            });
        });
    </script>

@endsection

@section('modal')
    @include('layouts.partials.modals._seatplan')
    @include('layouts.partials.modals._getTix')
@endsection