@extends('master')
@section('title')
    NEON BEIJING 2018
@endsection

@section('header')
    @include('layouts.partials._header')
    <meta property="og:url" content="{{ Request::fullUrl() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="NEON BEIJING 2018" />
    <meta property="og:description" content="The NEON Music Festival, the Asian UV Paint Electronic Dance Music Festival, will be in Beijing, China on 22nd & 23rd September at the Beijing National Stadium (Bird's Nest)"/>
    <meta property="og:image" content="{{ Request::Url().'images/neonbeijing2018/thumbnail.jpg' }}" />
@endsection

@section('content')
    <!-- Banner Section -->
    <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/neonbeijing2018/web-banner.jpg')}}" style="width: 100%" class="img-responsive" alt="NEON BEIJING 2018"></div>
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            <img src="{{asset('images/neonbeijing2018/thumbnail.jpg')}}" style="width: 100%" class="img-responsive" alt="NEON BEIJING 2018">
        </div>
    </section>
    <!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
            <div class="row priceNbtn">
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="row">
                        <div class="col-sm-9 leftBox">
                            <h6>NEON Beijing</h6> Tickets from <span>RM371</span>
                        </div>
                        <div class="col-sm-3 text-center">
                            <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar section-white">
            <section class="pageCategory-section last">
                <div class="container intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i>  22nd - 23rd September 2018</div>
                            <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i>  Beijing National Stadium, China <a target="_blank" href="https://goo.gl/maps/oADs58WffdS2">View Map</a></div>
                            <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> Saturday - Sunday (5.00 pm - 12.00am)</div>
                            <div class="clearfix">&nbsp;</div>
                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <div class="addthis_inline_share_toolbox"></div>
                            <!-- /sharing -->
                            <hr>
                            {{-- <p><h2>The NEON Music Festival, the Asian UV Paint Electronic Dance Music Festival, will be in Beijing, China on 22nd & 23rd September at the Beijing National Stadium (Bird's Nest) to bring a burst of large-scale carnival painting feast for the friends who love electronic dance music - "Rui Dance 2018".
                            <br/>亚洲彩绘电音节NEON Music Festival来华，将在9月22日-23日于北京国家体育场(鸟巢)为热爱电音的朋友带来一场嗨爆的大型狂欢彩绘盛宴——锐舞·2018。</h2></p>
                            <p>"NEON Music Festival" is the fastest growing music festival in Malaysia and even Southeast Asia. Compared to the European and American electronic music brands such as Ultra and Tomorrowland, NEON Music Festival is a true Asian audio brand. And with the increasing number of international events, live music, DJ performances, sound and light effects and its unique painted fluorescent experience are becoming more and more mature, attracting tens of thousands of visitors every year, gradually becoming a global electricity company. Music lovers must go to the pilgrimage carnival every year. It is not only a hearing feast, but also includes music, performance, entertainment, art and other elements to stimulate the audience's hearing and vision.
                            <p>「NEON Music Festival」是在马来西亚甚至东南亚发展最快的音乐节，相对于Ultra、Tomorrowland这些欧美电音品牌，NEON Music Festival是真正的亚洲电音品牌。而且随着在国际上举办的次数越来越多，现场音乐、DJ表演、声光效果和其独有的彩绘荧光体验越来越成熟，每年都吸引上万位游客前往参加，逐渐成为全球电音爱好者每年必去朝圣的狂欢节。它不仅是一场听觉盛宴，更包含了音乐、表演、娱乐、艺术等多方面元素，全方位刺激观众的听觉和视觉。</p>
                            <p>NEON has hosted many influential music festivals around the world. Through advanced lighting, sound design and large-scale art installations, it stimulates the senses and stimulates the enthusiasm of the fans to create a harmonious atmosphere. The scene is like a crowd earthquake, so that each soul in the body is separated from the body, and experience the unprecedented large-scale scenes! At the same time, the festival official will provide painted fluorescent paint to the audience for face painting, everyone on the scene will become the focus, the brightest star in the night sky is you! 
                            NEON公司在国际上举办过多次极具影响力的音乐节，通过先进的灯光、音响设计和大型艺术装置，刺激感官，激发粉丝互动积极性，营造出万众齐嗨的氛围。现场如人群地震，让置身其中的每一个人灵魂与肉体分离，体验前所未有的大型现场蹦迪！同时，音乐节官方将会提供彩绘荧光颜料给观众来作面部彩绘，现场的所有人都将成为焦点，夜空中最亮的星就是你！ </p>

                            <p>This "Rui Dance 2018" invited a number of the world's top 100 DJs, the lineup is very strong - 
                            本次「锐舞·2018」邀请了多位全球百大DJ，演出阵容十分强大——</p>

                            <p>September 22:<br />
                            9月22日： </p>
                            <p>Dimitri Vegas & Like Mike (DVLM) successfully listed in the top 100 list "DJ Mag" in 2015 The top spot of the Top 100 became the champion of the original defending team. In 2016 and 2017, it ranked second for two consecutive years.
                            于2015年成功登上百大排行榜《DJ Mag》Top100第一位的宝座，成为最 初以组合卫冕的冠军。在2016、2017年连续两年稳居第二。</p>

                            <p>KSHMR top 100 DJ ranked 12th. Highest Live Act of the Year Award. World DJ God! K's extraordinary strength and unique personal charm have attracted the attention of global electronic music lovers.
                            百大DJ排名第12。Highest Live Act年度最 佳现场大奖。世界DJ大神！ K神不凡的实力与独特的个人魅力吸引了全球电子音乐爱好者的追随。</p>
                            
                            <p>TIMMY TRUMPET's remarkable performance features, the origin of jazz music, sweeping the world's electronic trumpet! Currently one of the top 100 DJs in DJ MAG.
                            显着的表演特色，爵士音乐的出身，横扫全球的电音小喇叭！目前在DJ MAG前100名中独特的DJ之一。</p>
                            
                            <p>Quintino's main style: Electro House, Big Room House. Signed at the music label Spinnin Records, one of the most important figures in the global electronic music scene.
                            主要风格：Electro House、Big Room House。签约于音乐厂牌Spinnin Records，当今全球电子乐坛的重要人物之一。</p>
                            
                            <p>Many WIWEK heavyweight DJs have mixed Wiwek songs in the set of performances. With the release of some works on major brands, Wiwek has received support from all major brands.
                            许多电音界重量级DJ都曾经在演出的set里混入Wiwek的歌。随着在各大厂牌发行了一些作品之后Wiwek得到了所有大厂牌的支持。</p>

                            <p>DIMATIK is known as the new generation of dance floor bombers with its superb cutting skills and highly recognizable sounds!
                            凭借其高超的切歌技术及作品极具辨识度的音色，被誉为新一代舞池轰炸机！</p>


                            <p>September 23:<br />
                            9月23日： </p>
                            <p>Yellow Claw is a 
                            global Trap group! A horrible combination of hormones! Know what is Thug Life? No one is more accurate than they interpret!
                            红遍全球的Trap天团 ! 让人荷尔蒙狂飙的组合 ! 知道什么是Thug Life吗？没有人比他们诠释得更加准确 !</p>
                            
                            <p>Lost Frequencies is the first Belgian artist to have a record in the UK. Supported by artists such as Smash The House, Dimitri Vegas & Like Mike, Tiesto and Alesso. The phenomenon of Lost Frequencies is spreading throughout the planet.
                            第一位在英国拥有唱片的的比利时艺人。受到了Smash The House厂牌大佬Dimitri Vegas & Like Mike, Tiesto 以及Alesso等艺人的支持。Lost Frequencies的现象正在整座星球蔓延。</p>

                            <p>Salvatore Ganacci is a super invincible play in Sweden. Because of the live performance of the flea dance and the funny interaction overnight, the typhoon on the field reaches the world level, and the expression pack is the most popular!
                            瑞典超级无敌戏精骚男，因为现场演出跳骚舞搞笑互动一夜成名，场上台风达世界水平，无时无刻的表情包、最骚跳舞第一人！</p>
                            
                            <p>Headhunterz ranked 11th in the top 100 DJs in 2012. The impact on Hardstyle is very large. With more and more Hardstyle electronic parties expanding in the world, Headhunterz is destined to be at the forefront.
                            2012年跻身百大DJ第11名，对Hardstyle的影响非常大，随着越来越多的Hardstyle电音派对在全世界的扩大，Headhunterz注定是走在最前沿的人。</p>
                            
                            <p>A representative DJ duo of the MOKSI Barong Family. As the leader of the bass house, their genre rebellion and incitement, the scene is very powerful and explosive.
                            Barong Family旗下极具代表性的DJ双人组。作为 bass house 界领军人物的，他们的的曲风叛乱、躁动，现场极具震撼力和爆发力。</p>
                            
                            <p>Chukiess & Whackboi electronic dance music overlord, Malaysia DJ combination.
                            电子舞曲霸主，马来西亚DJ组合。</p>

                            <p>The 2 best combination of China most significant venue and the top international electronic dance music festival, we are looking forward to your arrival on 22nd & 23rd September 2018 in Bird's Nest! 
                            国家地标性场馆与国际电音节的激情碰撞，将会掀起怎样的音浪，期待值MAX！9月22-23日，我们在鸟巢期待你的到来。</p>
                            </p> --}}

                            <p>The NEON Music Festival, the Asian UV Paint Electronic Dance Music Festival will be in Beijing, China on 22nd & 23rd September at the Beijing National Stadium (Bird's Nest) to bring a burst of large-scale carnival painting feast for the friends who love electronic dance music - "Rui Dance 2018".
                            <br /><br /><br />DJ Lineup<br /><br />
                            September 22 (Saturday) :<br />
                            Dimitri Vegas & Like Mike (DVLM)<br />
                            KSHMR<br />
                            TIMMY TRUMPET's<br />
                            Quintino's<br />
                            WIWEK<br />
                            DIMATIK<br /><br />
                            September 23 (Sunday) :<br />
                            Yellow Claw<br />
                            Lost Frequencies<br />
                            Salvatore Ganacci<br />
                            Headhunterz<br />
                            MOKSI<br />
                            Chukiess & Whackboi <br /><br />
                            see you on 22nd & 23rd September 2018 in Bird's Nest with UV paint for NEON Music Festival!</p>
                            
                        </div>
                    </div>
                </div>
                {{-- <div class="clearfix"></div> --}}
            </section>

            {{-- <section class="pageCategory-section last section-grey">
                <div class="container">
                    <div class="gallery text-center">
                        <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
                        <!-- Swiper -->
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <a href="images/neonbeijing2018/above-beyond.jpg" data-featherlight="image"><img class="" src="images/neonbeijing2018/above-beyond.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/neonbeijing2018/gallery-1.jpg" data-featherlight="image"><img class="" src="images/neonbeijing2018/gallery-1.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/neonbeijing2018/gallery-2.jpg" data-featherlight="image"><img class="" src="images/neonbeijing2018/gallery-2.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/neonbeijing2018/gallery-3.jpg" data-featherlight="image"><img class="" src="images/neonbeijing2018/gallery-3.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/neonbeijing2018/gallery-4.jpg" data-featherlight="image"><img class="" src="images/neonbeijing2018/gallery-4.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/neonbeijing2018/gallery-5.jpg" data-featherlight="image"><img class="" src="images/neonbeijing2018/gallery-5.jpg" alt=""></a>
                                </div>

                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next swiper-button-white"></div>
                            <div class="swiper-button-prev swiper-button-white"></div>
                        </div><!-- /Swiper -->
                    </div>
                </div>
            </section> --}}

            <section class="pageCategory-section last"><a id="anchorPrice"></a>
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                            <div class="text-center">
                                <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                                <p>Select ticket</p>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="table-responsive">
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Category</th>
                                            <th>Ticket Price</th>
                                            <th>30% Discount (MyKad)</th>
                                            <th>Seat Plan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td style="color: #999999;">VIP 2 DAY (SAT & SUN)</td>
                                            <td>¥1980 / RM1252</td>
                                            <td></td>
                                            <td rowspan="9"><img class="img-responsive seatPlanImg" src="images/neonbeijing2018/seat-plan.jpg" alt=""></td>
                                        </tr>
                                        <tr>
                                            <td style="color: #999999;">VIP 1 DAY (SAT)</td>
                                            <td>¥1180 / RM746</td>
                                            <td>-</td>
                                        </tr>
                                        <tr>
                                            <td style="color: #999999;">VIP 1 DAY (SUN)</td>
                                            <td>¥1180 / RM746</td>
                                            <td>-</td>
                                        </tr>
                                        <tr>
                                            <td style="background-color: #C38DBF; color: #fff;">PGA 2 DAY (SAT & SUN)</td>
                                            <td>¥1280 / RM809</td>
                                            <td>RM566</td>
                                        </tr>
                                        <tr>
                                            <td style="background-color: #C38DBF; color: #fff;">PGA 1 DAY (SAT)</td>
                                            <td>¥780 / RM493</td>
                                            <td>-</td>
                                        </tr>
                                        <tr>
                                            <td style="background-color: #C38DBF; color: #fff;">PGA 1 DAY (SUN)</td>
                                            <td>¥780 / RM493</td>
                                            <td>-</td>
                                        </tr>
                                            <td style="background-color: #A2CFD2; color: #fff;">GA 2 DAY (SAT & SUN)</td>
                                            <td>¥980 / RM620</td>
                                            <td>RM434</td>
                                        </tr>
                                        <tr>
                                            <td style="background-color: #A2CFD2; color: #fff;">GA 1 DAY (SAT)</td>
                                            <td>¥580 / RM367</td>
                                            <td>-</td>
                                        </tr>
                                        <tr>
                                            <td style="background-color: #A2CFD2; color: #fff;">GA 1 DAY (SUN)</td>
                                            <td>¥580 / RM367</td>
                                            <td>-</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="buyAlert-bar">
                                <a class="btn btn-danger" id="buyButton" datetime="Sep 21 2018 00:00:00 GMT+0800" target="_blank" href="http://redtix-tickets.airasia.com/en-AU/shows/neon%20music%20festival/events" role="button">BUY TICKETS</a>
                                {{-- <span class="or">/</span>
                                <span class="popData-btn" data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets <i class="fa fa-info-circle" aria-hidden="true"></i></span> --}}
                            </div>
                            
                            <div class="note text-left">
                                <h2>Important Notes</h2>
                                {{-- <ol>
                                    <li>Prices shown exclude RM4.00 AirAsiaRedTix fee and 3% Credit card fee.</li>
                                    <li>All ticket prices will be paid in Malaysian Ringgit. Chinese Renminbi prices are shown for reference purposes only.</li>
                                    <li>Prices quoted in RMB may differ due to currency fluctuations.</li>
                                    <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li>
                                    <li>RM10.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                                    <li>Online ticket selling will close 14 days prior to event day, subject to availability.</li>
                                </ol> --}}
                                <ol>
                                    <li>Prices shown exclude RM4.00 AirAsiaRedTix fee and 3% Credit card fee.</li>
                                    <li>Malaysian MyKad required at ticket redemption and entry into event for MyKad promotions.</li>
                                    <li>All ticket prices will be paid in Malaysian Ringgit. Chinese Renminbi prices are shown for reference purposes only.</li>
                                    <li>Prices quoted in RMB may differ due to currency fluctuations.</li>
                                    <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li>
                                    <li>RM10.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                                    <li>Online ticket selling will close 14 days prior to event day, subject to availability.</li>
                                </ol>
                                <h2>For enquiry only:</h2>
                                <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                            </div>

                            <div class="buyAlert-bar" style="border:thin">
                            <span>REGISTER NOW TO GET ACCESS TO NEON COUNTDOWN KUALA LUMPUR 2018 EARLY BIRD PRICING</span>
                                <a class="btn btn-danger" datetime="Sep 22 2018 00:00:00 GMT+0800" target="_blank" href="neonkualalumpur" role="button">Register Now</a>
                            </div>


                        </div>
                    </div>
                </div>
            </section>
        </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 350;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });
    </script>

    {{-- Buy button disable --}}
    <script type="text/javascript">
        $(function() {
            $('a[id^=buyButton]').each(function() {
                var date = new Date();
                var enddate = $(this).attr('datetime'); 
                if ( Date.parse(date) >= Date.parse(enddate)) {
                  $(this).addClass('disabled');
                }
            });
        });
    </script>

@endsection

@section('modal')
    @include('layouts.partials.modals._seatplan')
    @include('layouts.partials.modals._getTix')
@endsection