@extends('masterforimf')
@section('title')
    Island Music Festival 2018
@endsection
@section('header')
    {{-- @include('layouts.partials._header') --}}
    <meta property="og:url" content="{{ Request::fullUrl() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Island Music Festival 2018" />
    <meta property="og:description" content="The Island Music Festival is back for its 6th edition from 12th – 14th October 2018 on Long Beach, Redang Island."/>
    <meta property="og:image" content="{{ Request::Url().'images/islandmusicfest2018v2/thumbnail1.jpg' }}" />
@endsection
@section('content')
<style type="text/css">
    .glyphicon-chevron-left {
        background: url("data:image/svg+xml;charset=utf8,%3Csvg xmlns='http://www.w3.org/2000/svg' fill='%23ccc' viewBox='0 0 8 8'%3E%3Cpath d='M5.25 0l-4 4 4 4 1.5-1.5-2.5-2.5 2.5-2.5-1.5-1.5z'/%3E%3C/svg%3E") no-repeat !important;
    }

    .glyphicon-chevron-left::before,
    .glyphicon-chevron-right::before {
        content: '' !important;
    }

    .importantNote {
        font-style: italic;
        color: #ee3123;
        display: block;
    }

    .glyphicon-chevron-right {
        background: url("data:image/svg+xml;charset=utf8,%3Csvg xmlns='http://www.w3.org/2000/svg' fill='%23ccc' viewBox='0 0 8 8'%3E%3Cpath d='M2.75 0l-1.5 1.5 2.5 2.5-2.5 2.5 1.5 1.5 4-4-4-4z'/%3E%3C/svg%3E") no-repeat !important;

    }

    .stepwizard-step p {
        margin-top: 10px;
    }

    .stepwizard-row {
        display: table-row;
    }

    .stepwizard {
        display: table;
        width: 100%;
        position: relative;
    }

    .stepwizard-step button[disabled] {
        opacity: 1 !important;
        filter: alpha(opacity=100) !important;
    }

    .stepwizard-row:before {
        top: 14px;
        bottom: 0;
        position: absolute;
        content: " ";
        width: 100%;
        height: 1px;
        background-color: #ccc;
        z-order: 0;
    }

    .stepwizard-step {
        display: table-cell;
        text-align: center;
        position: relative;
    }

    .btn-circle {
        width: 30px;
        height: 30px;
        text-align: center;
        padding: 6px 0;
        font-size: 12px;
        line-height: 1.428571429;
        border-radius: 15px;
    }

    .faq-Model-Style-Header {
        background: #FFD09A;
        color: white;
        border-radius: 22px;
    }

    .PStyle {
        padding: 15px;
        padding-top: 35px;
    }

    .faq-questions {
        height: calc(100% - 60px);
        overflow: auto;
        position: relative;
    }

    .faq-carousel-body {
        height: auto;
    }

    .faq-navigation {
        height: 50px;
        clear: both;
    }

    .faq-modal {
        height: calc(100% - 25px);
    }

    .faq-modal [role='document'],
    .faq-modal .container,
    .faq-modal .modal-content,
    .faq-modal .faq-modal-row,
    .faq-modal .modal-body .container-fluid,
    .faq-navigation-col {
        height: 100%;
    }

    .faq-modal .modal-body {
        height: calc(100% - 113px);
    }

    .faq-modal .carousel-control {
        width: 48%;
        top: unset;
        bottom: unset;
        height: 100%;
    }

    .faq-modal .carousel-control .glyphicon-chevron-right,
    .faq-modal .carousel-control .glyphicon-chevron-left {
        height: 70%;
        top: 40%;
    }

    .faq-modal .carousel-control .glyphicon-chevron-right {
        right: 0;
        margin-right: unset;
    }

    .faq-modal .carousel-control .glyphicon-chevron-left {
        left: 0;
        margin-leftrgba(29, 29, 29, 0.35): unset;
    }

    .faq-modal .carousel-control.right {
        text-align: right;
    }

    .faq-modal .sr-prev,
    .faq-modal .sr-next {
        overflow: unset;
        clip: unset;
        border: 0;
        color: rgba(29, 29, 29, 0.35);
        width: auto;
        top: 12px;
        font-size: 16px;
    }

    .faq-modal .sr-prev {
        left: 30px;
    }

    .faq-modal .sr-next {
        right: 30px;
    }

    .faq-modal .carousel-control.left,
    .faq-modal .carousel-control.right {
        background-image: none;
    }
    .child img{
      width: 90%;
      
    }
    .ArtistisCarousel .item .row {
      padding: 10px;
    }
</style>

<div class="container-fluid">
    <div class="row">
        <div id="myCarousel" class="carousel slide" data-ride="carousel">

            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <div class="item active">
                    <div style="width:100%">
                        <div class="container-fluid bgimage size-handler">
                            <div class="row">
                                <div class="col-md-1  equal-size1">
                                    <img src="{{asset('images/header/header_log.png')}}" style="width: 100%" class="img-responsive" alt="Island International Island Music 2017">
                                </div>
                                <div class="col-md-6 equal-size2">
                                    <div>
                                        <h4>Day 1
                                            <span class="small-word">(12 October 2018 | 8AM - 12PM)
                                                <span>
                                        </h4>
                                        <span class="small-hide"> Upon arrival to the island, you will check into the resort and wait for the briefing</span>
                                    </div>
                                    <div>
                                        <ul  style="color:#FD9394;" class="ul-dot">
                                            {{-- <li>
                                                <span class="color-list"> 7:30 AM </span>
                                                <span class="time-span color-list"> Breakfast</span>
                                            </li>
                                            <li>
                                                <span class="color-list">10:00 AM </span>
                                                <span class="time-span color-list">Guest departure </span>
                                            </li> --}}
                                            <li>
                                                <span class="color-list">11:00 AM </span>
                                                <span class="time-span color-list">Guest Check In</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-5 small-hide">
                                    <div class="col-md-offset-2  equal-size3">
                                        <div class="thumbnail nail-padding" style="  background-color: rgba(223, 220, 201, 1.3);  height: 333px;  width: 333px;">
                                            <img src="images/header/slider-side.png" alt="Nature" style="width:100%">
                                            <div class="caption">
                                                <h6 style="text-align: center;">DAY 1 </h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                               
                                <div class="col-md-11 col-md-offset-1 argest">
                                    <div class="type-set-pop" style="padding-top: 0px;">
                                        <p>
                                            {{-- <a href="" data-toggle="modal" class="v1" data-target="#video">VIDEO</a>
                                            <span> |</span> --}}
                                            <a onclick="loadSlider()" href="" data-toggle="modal" class="Im" data-target="#gallery">IMAGES</a>
                                            <span> |</span>
                                            <a href="" data-toggle="modal" class="Avt" data-target="#Activities">ACTIVITIES</a>
                                            <span> |</span>
                                            <a href="" data-toggle="modal" class="Art" data-target="#Artist">ARTISTS</a>
                                            <span> |</span>
                                            </br>
                                            <a href="" data-toggle="modal" class=" Ar-sa" data-target="#Artistschedule">ARTIST SCHEDULE </a>
                                            <span> |</span>
                                            <a href="" data-toggle="modal" class=" Ar-sa" data-target="#faq"> FAQ </a>
                                            <span> |</span>
                                        </p>
                                    </div>
                                    <div class="GetTicket" style="padding-top: 0px;">
                                            <a href="islandmusicfestival#gettickets">
                                                <input type="submit" value="Get Ticket" name="subscribe" id="mc-embedded-subscribe" class="button btn  btn-style-ticket btn-block">
                                            </a>
                                    </div>
                                </div>

                                <div class="col-md-12 top-slider-opacity">
                                    <div class="stepwizard">
                                        <div class="stepwizard-row setup-panel">
                                            <div class="stepwizard-step">
                                                <span class="radioButton-env">
                                                    <a href="#step-1" type="button" class="btn btn-primary btn-circle"></a>
                                                </span>
                                                <p>8 AM</p>
                                            </div>
                                            <div class="stepwizard-step">
                                                <p class="p-primary" style="margin-top: -40px;">Day 1</p>
                                                <span class="radioButton-env"><a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled"></a></span>
                                                <p>12 PM</p>
                                            </div>
                                            <div class="stepwizard-step">
                                                <span class="radioButton-env"><a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled"></a></span>
                                                <p>8 PM</p>
                                            </div>
                                            <div class="stepwizard-step">
                                                <span class="radioButton-env"><a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled"></a></span>
                                                <p>8 AM</p>
                                            </div>
                                            <div class="stepwizard-step">
                                                <p style="margin-top: -40px;">Day 2</p>
                                                <span class="radioButton-env"><a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled"></a></span>
                                                <p>12 PM</p>
                                            </div>
                                            <div class="stepwizard-step">
                                                <span class="radioButton-env"><a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled"></a></span>
                                                <p>8 PM</p>
                                            </div>
                                            <div class="stepwizard-step">
                                                <p style="margin-top: -40px;">Day 3</p>
                                                <span class="radioButton-env"><a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled"></a></span>
                                                <p>8 AM</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div style="width:100%">
                        <div class="container-fluid bgimage size-handler">
                            <div class="row">
                                <div class="col-md-1  equal-size1">
                                    <img src="{{asset('images/header/header_log.png')}}" style="width: 100%" class="img-responsive" alt="Island International Island Music 2017">
                                </div>
                                <div class="col-md-6 equal-size2">
                                    <div>
                                        <h4>Day 1
                                            <span class="small-word">(12 October 2018 | 12PM - 8PM)</span>
                                        </h4>
                                        <span class="small-hide"> The excitement begins with activities and performances, Meet exciting people on the Island!</span>
                                        <ul  style="color:#FD9394;" class="ul-dot">
                                            <li>
                                                <span class="color-list">12:30 PM</span>
                                                <span class="time-span color-list">Briefing</span>
                                            </li>
                                            <li>
                                                <span class="color-list">2:00 PM</span>
                                                {{-- <span class="time-span color-list">Limbo, Bowling & Beer Pong</span> --}}
                                                <span class="time-span color-list">Limbo & Bowling</span>
                                            </li>
                                            <li>
                                                <span class="color-list">3:00 PM</span>
                                               <span class="time-span color-list">Snorkeling, Sand Sculpture & Tic Tac Toe</span>
                                               \ <span class="color-list">LSP</span>
                                            </li>
                                            <li>
                                                <span class="color-list">4:00 PM</span>
                                                <span class="time-span color-list">Wood Carving & Remote Car Race</span>
                                                \ <span class="color-list">Peanut Butter Jelly Jam</span>
                                            </li>
                                            <li>
                                                <span class="color-list">4:30 PM</span>
                                                <span class="time-span color-list">Football & Volleyball</span>
                                                \ <span class="color-list">Jumero</span>
                                            </li>
                                            <li>
                                                <span class="color-list">5:00 PM</span>
                                                <span class="time-span color-list"> </span>
                                                \ <span class="color-list">Bihzhu</span>
                                            </li>
                                            <li>
                                                <span class="color-list">5:30 PM</span>
                                                <span class="time-span color-list">Beach Yoga & Tug Of War</span>
                                                \ <span class="color-list">Gabriel Lynch</span>
                                            </li>
                                            <li>
                                                <span class="color-list">6:00 PM</span>
                                                {{-- <span class="time-span color-list">Fashion show</span> --}}
                                                \ <span class="color-list">Sea Travel</span>
                                            </li>
                                            <li>
                                                <span class="color-list">6:30 PM</span>
                                                <span class="time-span color-list"> </span>
                                                \ <span class="color-list">NJWA</span>
                                            </li>
                                            <li>
                                                <span class="color-list">7:00 PM</span>
                                                <span class="time-span color-list"> </span>
                                                \ <span class="color-list">Christian Theseira</span>
                                            </li>
                                            <li>
                                                <span class="color-list">7:30 PM</span>
                                                <span class="time-span color-list"> </span>
                                                \ <span class="color-list">Dani</span>
                                            </li>
                                        </ul>
                                            
                                    </div>
                                </div>
                                <div class="col-md-5 small-hide">
                                    <div class="col-md-offset-2  equal-size3">
                                        <div class="thumbnail nail-padding" style="  background-color: rgba(223, 220, 201, 1.3);  height: 333px;  width: 333px;">
                                            <img src="images/header/slider-side.png" alt="Nature" style="width:100%">
                                            <div class="caption">
                                                <h6 style="text-align: center;">DAY 1 </h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-11 col-md-offset-1 argest">
                                    <div class="type-set-pop" style="padding-top: 0px;">
                                        <p>
                                            {{-- <a href="" data-toggle="modal" class="v1" data-target="#video">VIDEO</a>
                                            <span> |</span> --}}
                                            <a onclick="loadSlider()" href="" data-toggle="modal" class="Im" data-target="#gallery">IMAGES</a>
                                            <span> |</span>
                                            <a href="" data-toggle="modal" class="Avt" data-target="#Activities">ACTIVITIES</a>
                                            <span> |</span>
                                            <a href="" data-toggle="modal" class="Art" data-target="#Artist">ARTISTS</a>
                                            <span> |</span>
                                            </br>
                                            <a href="" data-toggle="modal" class=" Ar-sa" data-target="#Artistschedule">ARTIST SCHEDULE </a>
                                            <span> |</span>
                                            <a href=""> FAQ </a>
                                        </p>
                                    </div>
                                    <div class="GetTicket" style="padding-top: 0px;">
                                            <a href="islandmusicfestival#gettickets">
                                                <input type="submit" value="Get Ticket" name="subscribe" id="mc-embedded-subscribe" class="button btn  btn-style-ticket btn-block">
                                            </a>
                                    </div>
                                </div>
                                <div class="col-md-12 top-slider-opacity">
                                    <div class="stepwizard">
                                        <div class="stepwizard-row setup-panel">
                                            <div class="stepwizard-step">
                                                <span class="radioButton-env"><a href="#step-1" type="button" class="btn btn-default btn-circle" disabled="disabled"></a></span>
                                                <p>8 AM</p>
                                            </div>
                                            <div class="stepwizard-step">
                                                <p style="margin-top: -40px;">Day 1</p>
                                                <span class="radioButton-env"><a href="#step-2" type="button" class="btn btn-primary btn-circle"></a></span>
                                                <p>12 PM</p>
                                            </div>
                                            <div class="stepwizard-step">
                                                <span class="radioButton-env"><a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled"></a></span>
                                                <p>8 PM</p>
                                            </div>
                                            <div class="stepwizard-step">
                                                <span class="radioButton-env"><a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled"></a></span>
                                                <p>8 AM</p>
                                            </div>
                                            <div class="stepwizard-step">
                                                <p style="margin-top: -40px;">Day 2</p>
                                                <span class="radioButton-env"><a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled"></a></span>
                                                <p>12 PM</p>
                                            </div>
                                            <div class="stepwizard-step">
                                                <span class="radioButton-env"><a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled"></a></span>
                                                <p>8 PM</p>
                                            </div>
                                            <div class="stepwizard-step">
                                                <p style="margin-top: -40px;">Day 3</p>
                                                <span class="radioButton-env"><a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled"></a></span>
                                                <p>8 AM</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div style="width:100%">
                        <div class="container-fluid bgimage size-handler">
                            <div class="row">
                                <div class="col-md-1  equal-size1">
                                    <img src="{{asset('images/header/header_log.png')}}" style="width: 100%" class="img-responsive" alt="Island International Island Music 2017">
                                </div>
                                <div class="col-md-6 equal-size2">
                                    <div>
                                        <h4>Day 1
                                            <span class="small-word">(12 October 2018 | 8PM - 12AM)
                                                <span>
                                        </h4>
                                        <span class="small-hide">After Sunset, It's Party Time! Performance by:</span>
                                    </div>
                                    <div>
                                        <ul  style="color:#FD9394;" class="ul-dot">
                                            <li>
                                                <span class="color-list">8:05 PM </span>
                                                \ <span class="color-list">Fazz</span>
                                            </li>
                                            <li>
                                                <span class="color-list">8:30 PM </span>
                                                \ <span class="color-list">Kaya</span>
                                            </li>
                                            <li>
                                                <span class="color-list">9:00 PM </span>
                                                \ <span class="color-list">Mass Music</span>
                                            </li>
                                            <li>
                                                <span class="color-list">9:30 PM </span>
                                                \ <span class="color-list">Skeletor, Lil J, A-Kid</span>
                                            </li>
                                            <li>
                                                <span class="color-list">10:00 PM </span>
                                                \ <span class="color-list">Leng Sisters</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-5 small-hide">
                                    <div class="col-md-offset-2  equal-size3">
                                        <div class="thumbnail nail-padding" style="  background-color: rgba(223, 220, 201, 1.3);  height: 333px;  width: 333px;">
                                            <img src="images/header/slider-side1.png" alt="Nature" style="width:100%">
                                            <div class="caption">
                                                <h6 style="text-align: center;">DAY 1 </h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-11 col-md-offset-1 argest">
                                    <div class="type-set-pop" style="padding-top: 0px;">
                                        <p>
                                            {{-- <a href="" data-toggle="modal" class="v1" data-target="#video">VIDEO</a>
                                            <span> |</span> --}}
                                            <a onclick="loadSlider()" href="" data-toggle="modal" class="Im" data-target="#gallery">IMAGES</a>
                                            <span> |</span>
                                            <a href="" data-toggle="modal" class="Avt" data-target="#Activities">ACTIVITIES</a>
                                            <span> |</span>
                                            <a href="" data-toggle="modal" class="Art" data-target="#Artist">ARTISTS</a>
                                            <span> |</span>
                                            </br>
                                            <a href="" data-toggle="modal" class=" Ar-sa" data-target="#Artistschedule">ARTIST SCHEDULE </a>
                                            <span> |</span>
                                            <a href="" data-toggle="modal" class=" Ar-sa" data-target="#faq"> FAQ </a>
                                            <span> |</span>
                                        </p>
                                    </div>
                                    <div class="GetTicket" style="padding-top: 0px;">

                                            <a href="islandmusicfestival#gettickets">
                                                <input type="submit" value="Get Ticket" name="subscribe" id="mc-embedded-subscribe" class="button btn  btn-style-ticket btn-block">
                                            </a>
                                    </div>
                                </div>

                                <div class="col-md-12 top-slider-opacity">
                                    <div class="stepwizard">
                                        <div class="stepwizard-row setup-panel">
                                            <div class="stepwizard-step">
                                                <span class="radioButton-env"><a href="#step-1" type="button" class="btn btn-default btn-circle"></a></span>
                                                <p>8 AM</p>
                                            </div>
                                            <div class="stepwizard-step">
                                                <p class="p-primary" style="margin-top: -40px;">Day 1</p>
                                                <span class="radioButton-env"><a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled"></a></span>
                                                <p>12 PM</p>
                                            </div>
                                            <div class="stepwizard-step">
                                            <span class="radioButton-env">
                                                <a href="#step-3" type="button" class="btn btn-primary btn-circle change-bg" disabled="disabled">

                                                </a>
                                                </span>
                                                <p>8 PM</p>
                                            </div>
                                            <div class="stepwizard-step">
                                                <span class="radioButton-env"><a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled"></a></span>
                                                <p>8 AM</p>
                                            </div>
                                            <div class="stepwizard-step">
                                                <p style="margin-top: -40px;">Day 2</p>
                                                <span class="radioButton-env"><a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled"></a></span>
                                                <p>12 PM</p>
                                            </div>
                                            <div class="stepwizard-step">
                                                <span class="radioButton-env"><a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled"></a></span>
                                                <p>8 PM</p>
                                            </div>
                                            <div class="stepwizard-step">
                                                <p style="margin-top: -40px;">Day 3</p>
                                                <span class="radioButton-env"><a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled"></a></span>
                                                <p>8 AM</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div style="width:100%">
                        <div class="container-fluid bgimage size-handler">
                            <div class="row">
                                <div class="col-md-1  equal-size1">
                                    <img src="{{asset('images/header/header_log.png')}}" style="width: 100%" class="img-responsive" alt="Island International Island Music 2017">
                                </div>
                                <div class="col-md-6 equal-size2">
                                    <div>
                                        <h4>Day 2
                                            <span class="small-word">(13 October 2018 | 8AM - 12PM)</span>
                                        </h4>
                                        <span class="small-hide"> Rise & Shine!! Fun filled activities and performances awaits you!</span>
                                        <ul  style="color:#FD9394;" class="ul-dot">
                                            <li>
                                                <span class="color-list">8:00 AM</span>
                                                <span class="time-span color-list">Sunrise Yoga</span>
                                            </li>
                                            <li>
                                                <span class="color-list">10:00 AM</span>
                                                <span class="time-span color-list">Jungle Trekking</span>
                                            </li>
                                        </ul>
                                            
                                    </div>
                                </div>
                                <div class="col-md-5 small-hide">
                                    <div class="col-md-offset-2  equal-size3">
                                        <div class="thumbnail nail-padding" style="  background-color: rgba(223, 220, 201, 1.3);  height: 333px;  width: 333px;">
                                            <img src="images/header/slider-side.png" alt="Nature" style="width:100%">
                                            <div class="caption">
                                                <h6 style="text-align: center;">DAY 2 </h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-11 col-md-offset-1 argest">
                                <div class="type-set-pop" style="padding-top: 0px;">
                                    <p>
                                        {{-- <a href="" data-toggle="modal" class="v1" data-target="#video">VIDEO</a>
                                        <span> |</span> --}}
                                        <a onclick="loadSlider()" href="" data-toggle="modal" class="Im " class="btn btn-info btn-lg" data-target="#gallery">IMAGES</a>
                                        <span> |</span>
                                        <a href="" data-toggle="modal" class="Avt" data-target="#Activities">ACTIVITIES</a>
                                        <span> |</span>
                                        <a href="" data-toggle="modal" class="Art" data-target="#Artist">ARTISTS</a>
                                        <span> |</span>
                                        </br>
                                        <a href="" data-toggle="modal" class=" Ar-sa" data-target="#Artistschedule">ARTIST SCHEDULE </a>
                                        <span> |</span>
                                        <a href=""> FAQ </a>
                                    </p>
                                </div>
                                <div class="GetTicket " style="padding-top: 0px;">

                                            <a href="islandmusicfestival#gettickets">
                                                <input type="submit" value="Get Ticket" name="subscribe" id="mc-embedded-subscribe" class="button btn  btn-style-ticket btn-block">
                                            </a>
                                </div>
                            </div>
                            <div class="col-md-12 top-slider-opacity">
                                <div class="stepwizard">
                                    <div class="stepwizard-row setup-panel">
                                        <div class="stepwizard-step">
                                            <span class="radioButton-env"><a href="#step-1" type="button" class="btn btn-default btn-circle" disabled="disabled"></a></span>
                                            <p>8 AM</p>
                                        </div>
                                        <div class="stepwizard-step">
                                            <p style="margin-top: -40px;">Day 1</p>
                                            <span class="radioButton-env"><a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled"></a></span>
                                            <p>12 PM</p>
                                        </div>
                                        <div class="stepwizard-step">
                                            <span class="radioButton-env"><a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled"></a></span>
                                            <p>8 PM</p>
                                        </div>
                                        <div class="stepwizard-step">
                                        <span class="radioButton-env">
                                            <a href="#step-3" type="button" class="btn btn-primary btn-circle"></a>
                                        </span>
                                            <p>8 AM</p>
                                        </div>
                                        <div class="stepwizard-step">
                                            <p style="margin-top: -40px;">Day 2</p>
                                            <span class="radioButton-env"><a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled"></a></span>
                                            <p>12 PM</p>
                                        </div>
                                        <div class="stepwizard-step">
                                            <span class="radioButton-env"><a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled"></a></span>
                                            <p>8 PM</p>
                                        </div>
                                        <div class="stepwizard-step">
                                            <p style="margin-top: -40px;">Day 3</p>
                                            <span class="radioButton-env"><a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled"></a></span>
                                            <p>8 AM</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div style="width:100%">
                        <div class="container-fluid bgimage size-handler">
                            <div class="row">
                                <div class="col-md-1  equal-size1">
                                    <img src="{{asset('images/header/header_log.png')}}" style="width: 100%" class="img-responsive" alt="Island International Island Music 2017">
                                </div>
                                <div class="col-md-6 equal-size2">
                                    <div>
                                        <h4>Day 2
                                            <span class="small-word">(13 October 2018 | 12PM - 8PM)</span>
                                        </h4>
                                        <span class="small-hide"> The excitement begins with activities and performances, Meet exciting people on the Island!</span>
                                        <ul  style="color:#FD9394;" class="ul-dot">
                                            <li>
                                                <span class="color-list">1:00 PM</span>
                                                <span class="time-span color-list">Tic Tac Toe</span>
                                            </li>
                                            <li>
                                                <span class="color-list">2:00 PM</span>
                                               {{-- <span class="time-span color-list">Sand Sculpture, Beer Pong, Wood Carving & Snorkeling</span> --}}
                                               <span class="time-span color-list">Sand Sculpture, Wood Carving & Snorkeling</span>
                                            </li>
                                            <li>
                                                <span class="color-list">2:30 PM</span>
                                                <span class="time-span color-list"> </span>
                                                \ <span class="color-list">Skeletor</span>
                                            </li>
                                            <li>
                                                <span class="color-list">3:00 PM</span>
                                                <span class="time-span color-list">Limbo & Remote Car Race</span>
                                            </li>
                                            <li>
                                                <span class="color-list">4:00 PM</span>
                                                <span class="time-span color-list"> </span>
                                                \ <span class="color-list">Peanut Butter Jelly Jam</span>
                                            </li>
                                            <li>
                                                <span class="color-list">4:30 PM</span>
                                                <span class="time-span color-list">Football, Volleyball & Bowling</span>
                                                \ <span class="color-list">Pris Xavier</span>
                                            </li>
                                            <li>
                                                <span class="color-list">5:00 PM</span>
                                                <span class="time-span color-list"> </span>
                                                \ <span class="color-list">Annatasha</span>
                                            </li>
                                            <li>
                                                <span class="color-list">5:30 PM</span>
                                                <span class="time-span color-list"> </span>
                                                \ <span class="color-list">Sound of Jane</span>
                                            </li>
                                            <li>
                                                <span class="color-list">6:00 PM</span>
                                                <span class="time-span color-list">Tug Of War & Beach Yoga</span>
                                                \ <span class="color-list">Last Logic</span>
                                            </li>
                                            <li>
                                                <span class="color-list">6:30 PM</span>
                                                {{-- <span class="time-span color-list">Fashion show</span> --}}
                                                \ <span class="color-list">Rhythm Rebels</span>
                                            </li>
                                            <li>
                                                <span class="color-list">7:00 PM</span>
                                                <span class="time-span color-list"> </span>
                                                \ <span class="color-list">Vital Signs</span>
                                            </li>
                                            <li>
                                                <span class="color-list">7:30 PM</span>
                                                <span class="time-span color-list"> </span>
                                                \ <span class="color-list">PJ12</span>
                                            </li>
                                        </ul>
                                            
                                    </div>
                                </div>
                                <div class="col-md-5 small-hide">
                                    <div class="col-md-offset-2  equal-size3">
                                        <div class="thumbnail nail-padding" style="  background-color: rgba(223, 220, 201, 1.3);  height: 333px;  width: 333px;">
                                            <img src="images/header/slider-side.png" alt="Nature" style="width:100%">
                                            <div class="caption">
                                                <h6 style="text-align: center;">DAY 2 </h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-1">
                            </div>
                            <div class="col-md-1">
                            </div>
                            <div class="col-md-11 col-md-offset-1 argest">
                                <div class="type-set-pop" style="padding-top: 0px;">
                                    <p>
                                        {{-- <a href="" data-toggle="modal" class="v1" data-target="#video">VIDEO</a>
                                        <span> |</span> --}}
                                        <a onclick="loadSlider()" href="" data-toggle="modal" class="Im " class="btn btn-info btn-lg" data-target="#gallery">IMAGES</a>
                                        <span> |</span>
                                        <a href="" data-toggle="modal" class="Avt" data-target="#Activities">ACTIVITIES</a>
                                        <span> |</span>
                                        <a href="" data-toggle="modal" class="Art" data-target="#Artist">ARTISTS</a>
                                        <span> |</span>
                                        </br>
                                        <a href="" data-toggle="modal" class=" Ar-sa" data-target="#Artistschedule">ARTIST SCHEDULE </a>
                                        <span> |</span>
                                        <a href=""> FAQ </a>
                                    </p>
                                </div>
                                <div class="GetTicket " style="padding-top: 0px;">

                                            <a href="islandmusicfestival#gettickets">
                                                <input type="submit" value="Get Ticket" name="subscribe" id="mc-embedded-subscribe" class="button btn  btn-style-ticket btn-block">
                                            </a>
                                </div>
                            </div>
                            <div class="col-md-12 top-slider-opacity">
                                <div class="stepwizard">
                                    <div class="stepwizard-row setup-panel">
                                        <div class="stepwizard-step">
                                            <span class="radioButton-env"><a href="#step-1" type="button" class="btn btn-default btn-circle" disabled="disabled"></a></span>
                                            <p>8 AM</p>
                                        </div>
                                        <div class="stepwizard-step">
                                            <p style="margin-top: -40px;">Day 1</p>
                                            <span class="radioButton-env"><a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled"></a></span>
                                            <p>12 PM</p>
                                        </div>
                                        <div class="stepwizard-step">
                                            <span class="radioButton-env"><a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled"></a></span>
                                            <p>8 PM</p>
                                        </div>
                                        <div class="stepwizard-step">
                                        <span class="radioButton-env">
                                            <a href="#step-3" type="button" class="btn btn-default btn-circle"></a></span>
                                            <p>8 AM</p>
                                        </div>
                                        <div class="stepwizard-step">
                                            <p style="margin-top: -40px;">Day 2</p>
                                            <span class="radioButton-env">
                                            <a href="#step-3" type="button" class="btn btn-primary btn-circle" disabled="disabled"></a>
                                            </span>
                                            <p>12 PM</p>
                                        </div>
                                        <div class="stepwizard-step">
                                            <span class="radioButton-env"><a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled"></a></span>
                                            <p>8 PM</p>
                                        </div>
                                        <div class="stepwizard-step">
                                            <p style="margin-top: -40px;">Day 3</p>
                                            <span class="radioButton-env"><a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled"></a></span>
                                            <p>8 AM</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div style="width:100%">
                        <div class="container-fluid bgimage size-handler">
                            <div class="row">
                                <div class="col-md-1  equal-size1">
                                    <img src="{{asset('images/header/header_log.png')}}" style="width: 100%" class="img-responsive" alt="Island International Island Music 2017">
                                </div>
                                <div class="col-md-6 equal-size2">
                                    <div>
                                        <h4>Day 2
                                            <span class="small-word">(13 October 2018 | 8PM - 12AM)
                                                <span>
                                        </h4>
                                        <span class="small-hide"> Let loose and groove to the awesome performances. Performance by:</span>
                                    </div>
                                    <div>
                                        <ul  style="color:#FD9394;"  class="ul-dot">
                                            <li>
                                                <span class="color-list"> 8:05 PM </span>
                                                \ <span class="color-list">Zupanova</span>
                                            </li>
                                            <li>
                                                <span class="color-list"> 8:30 PM </span>
                                                \ <span class="color-list">LSP, UCOP & Wymann</span>
                                            </li>
                                            <li>
                                                <span class="color-list">9:30 PM </span>
                                                \ <span class="color-list">Jenni F</span>
                                            </li>
                                            <li>
                                                <span class="color-list">10:30 PM </span>
                                                \ <span class="color-list">Oddicon</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-5 small-hide">
                                    <div class="col-md-offset-2  equal-size3">
                                        <div class="thumbnail nail-padding" style="  background-color: rgba(223, 220, 201, 1.3);  height: 333px;  width: 333px;">
                                            <img src="images/header/slider-side2.png" alt="Nature" style="width:100%">
                                            <div class="caption">
                                                <h6 style="text-align: center;">DAY 2 </h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-md-11 col-md-offset-1 argest">
                                    <div class="type-set-pop" style="padding-top: 0px;">
                                        <p>
                                            {{-- <a href="" data-toggle="modal" class="v1" data-target="#video">VIDEO</a>
                                            <span> |</span> --}}
                                            <a onclick="loadSlider()" href="" data-toggle="modal" class="Im" data-target="#gallery">IMAGES</a>
                                            <span> |</span>
                                            <a href="" data-toggle="modal" class="Avt" data-target="#Activities">ACTIVITIES</a>
                                            <span> |</span>
                                            <a href="" data-toggle="modal" class="Art" data-target="#Artist">ARTISTS</a>
                                            <span> |</span>
                                            </br>
                                            <a href="" data-toggle="modal" class=" Ar-sa" data-target="#Artistschedule">ARTIST SCHEDULE </a>
                                            <span> |</span>
                                            <a href="" data-toggle="modal" class=" Ar-sa" data-target="#faq"> FAQ </a>
                                            <span> |</span>
                                        </p>
                                    </div>
                                    <div class="GetTicket" style="padding-top: 0px;">

                                            <a href="islandmusicfestival#gettickets">
                                                <input type="submit" value="Get Ticket" name="subscribe" id="mc-embedded-subscribe" class="button btn  btn-style-ticket btn-block">
                                            </a>
                                    </div>
                                </div>

                                <div class="col-md-12 top-slider-opacity">
                                    <div class="stepwizard">
                                        <div class="stepwizard-row setup-panel">
                                            <div class="stepwizard-step">
                                                <span class="radioButton-env"><a href="#step-1" type="button" class="btn btn-default btn-circle"></a></span>
                                                <p>8 AM</p>
                                            </div>
                                            <div class="stepwizard-step">
                                                <p class="p-primary" style="margin-top: -40px;">Day 1</p>
                                                <span class="radioButton-env"><a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled"></a></span>
                                                <p>12 PM</p>
                                            </div>
                                            <div class="stepwizard-step">
                                            <span class="radioButton-env">
                                                <a href="#step-3" type="button" class="btn btn-default btn-circle change-bg" disabled="disabled"></a>
                                            </span>
                                                <p>8 PM</p>
                                            </div>
                                            <div class="stepwizard-step">
                                                <span class="radioButton-env"><a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled"></a></span>
                                                <p>8 AM</p>
                                            </div>
                                            <div class="stepwizard-step">
                                                <p style="margin-top: -40px;">Day 2</p>
                                                <span class="radioButton-env"><a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled"></a></span>
                                                <p>12 PM</p>
                                            </div>
                                            <div class="stepwizard-step">
                                            <span class="radioButton-env">
                                                <a href="#step-3" type="button" class="btn btn-primary btn-circle change-bg" disabled="disabled"></a>
                                            </span>
                                                <p>8 PM</p>
                                            </div>
                                            <div class="stepwizard-step">
                                                <p style="margin-top: -40px;">Day 3</p>
                                                <span class="radioButton-env"><a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled"></a></span>
                                                <p>8 AM</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div style="width:100%">
                        <div class="container-fluid bgimage size-handler">
                            <div class="row">
                                <div class="col-md-1  equal-size1">
                                    <img src="{{asset('images/header/header_log.png')}}" style="width: 100%" class="img-responsive" alt="Island International Island Music 2017">
                                </div>
                                <div class="col-md-6 equal-size2">
                                    <div>
                                        <h4>Day 3
                                            <span class="small-word">(14 October 2018 | 8AM - 12 PM)
                                                <span>
                                        </h4>
                                        <span class="small-hide"> After an amazing weekend, It's time to say Goodbye</span>
                                    </div>
                                    <div>
                                        <ul  style="color:#FD9394;"  class="ul-dot">
                                            <li>
                                                <span class="color-list"> 11:00 AM </span>
                                                <span class="time-span color-list"> Ferry Depart from Island</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-5 small-hide">
                                    <div class="col-md-offset-2  equal-size3">
                                        <div class="thumbnail nail-padding" style="  background-color: rgba(223, 220, 201, 1.3);  height: 333px;  width: 333px;">
                                            <img src="images/header/slider-side.png" alt="Nature" style="width:100%">
                                            <div class="caption">
                                                <h6 style="text-align: center;">DAY 3 </h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-md-11 col-md-offset-1 argest">
                                    <div class="type-set-pop" style="padding-top: 0px;">
                                        <p>
                                            {{-- <a href="" data-toggle="modal" class="v1" data-target="#video">VIDEO</a>
                                            <span> |</span> --}}
                                            <a onclick="loadSlider()" href="" data-toggle="modal" class="Im" data-target="#gallery">IMAGES</a>
                                            <span> |</span>
                                            <a href="" data-toggle="modal" class="Avt" data-target="#Activities">ACTIVITIES</a>
                                            <span> |</span>
                                            <a href="" data-toggle="modal" class="Art" data-target="#Artist">ARTISTS</a>
                                            <span> |</span>
                                            <br>
                                            <a href="" data-toggle="modal" class=" Ar-sa" data-target="#Artistschedule">ARTIST SCHEDULE </a>
                                            <span> |</span>
                                            <a href="" data-toggle="modal" class=" Ar-sa" data-target="#faq"> FAQ </a>
                                            <span> |</span>
                                        </p>
                                    </div>
                                    <div class="GetTicket" style="padding-top: 0px;">

                                            <a href="islandmusicfestival#gettickets">
                                                <input type="submit" value="Get Ticket" name="subscribe" id="mc-embedded-subscribe" class="button btn  btn-style-ticket btn-block">
                                            </a>
                                    </div>
                                </div>

                                <div class="col-md-12 top-slider-opacity">
                                    <div class="stepwizard">
                                        <div class="stepwizard-row setup-panel">
                                            <div class="stepwizard-step">
                                                <span class="radioButton-env"><a href="#step-1" type="button" class="btn btn-default btn-circle"></a></span>
                                                <p>8 AM</p>
                                            </div>
                                            <div class="stepwizard-step">
                                                <p class="p-primary" style="margin-top: -40px;">Day 1</p>
                                                <span class="radioButton-env"><a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled"></a></span>
                                                <p>12 PM</p>
                                            </div>
                                            <div class="stepwizard-step">
                                                <span class="radioButton-env"><a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled"></a></span>
                                                <p>8 PM</p>
                                            </div>
                                            <div class="stepwizard-step">
                                                <span class="radioButton-env"><a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled"></a></span>
                                                <p>8 AM</p>
                                            </div>
                                            <div class="stepwizard-step">
                                                <p style="margin-top: -40px;">Day 2</p>
                                                <span class="radioButton-env"><a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled"></a></span>
                                                <p>12 PM</p>
                                            </div>
                                            <div class="stepwizard-step">
                                                <span class="radioButton-env"><a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled"></a></span>
                                                <p>8 PM</p>
                                            </div>
                                            <div class="stepwizard-step">
                                                <p style="margin-top: -40px;">Day 3</p>
                                                <span class="radioButton-env">
                                                    <a href="#step-3" type="button" class="btn btn-primary btn-circle" disabled="disabled"></a>
                                                </span>
                                                <p>8 AM</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Left and right controls -->
        <a class="left carousel-control-left carousel-control  " href="#myCarousel" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left left">  </span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control carousel-control-right" href="#myCarousel" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"> <span class="circle-but"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>
</div>
</div>
<!-- /Main Body -->
</section>
<!-- /Content Section -->
<!-- All pop-up -->
<div>
    <!-- Button trigger modal -->
    <!-- Modal -->
    <div class="modal fade faq-modal" id="faq" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div role="document" style="margin-top:23px; ">
            <div class="container">
                <div class="row faq-modal-row">
                    <div class="modal-content">
                        <div class="modal-header" style="border-bottom:0px;">
                            <div>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                            </div>
                            </button>
                            <div>
                                <h4 style="text-align: center;font-weight: normal;"> FAQ </h4>
                            </div>
                        </div>
                        <div class="modal-body faq-Model-Style">
                            <div class="container-fluid">
                                <div class="row faq-questions">
                                    <div class="col-md-12 faq-carousel-body">
                                        <div id="faqCarousel" class="carousel slide" data-wrap="false" data-ride="false" data-interval="false">

                                            <!-- Wrapper for slides -->
                                            <div class="carousel-inner">
                                                <div class="item active">
                                                    <div style="width:100%">
                                                        <div class="container-fluid">
                                                            <div class="col-md-12">
                                                                <div class="row">
                                                                    <div class="col-md-10 col-md-offset-1 faq-Model-Style-Header">
                                                                        <h4 style="margin-left: 14px;">WHAT IS IMF?</h4>
                                                                    </div>
                                                                    <div class="col-md-10 col-md-offset-1">
                                                                        <p class="PStyle">Island Music Festival is a 3 days & 2 nights celebration
                                                                            of Rhythm & Nature that has been going on yearly
                                                                            at Long Beach, Redang Island since 2013. This
                                                                            year it will be from 12 – 14 October 2018. It’s
                                                                            a rain or shine festival on the beach so leave
                                                                            your raincoats at home. Expect to discover new
                                                                            music, make new friends, have new experiences
                                                                            and a new appreciation for mother nature while
                                                                            having a great time!</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="item">
                                                    <div style="width:100%">
                                                        <div class="container-fluid">
                                                            <div class="col-md-12">
                                                                <div class="row">
                                                                    <div class="col-md-10 col-md-offset-1 faq-Model-Style-Header">
                                                                        <h4 style="margin-left: 14px;">HOW DO I GET THERE?</h4>
                                                                    </div>
                                                                    <div class="col-md-10 col-md-offset-1 PStyle1">
                                                                        <p class="PStyle">
                                                                            To get to the festival you need to get to Shahbandar Jetty, at the city center of Kuala Terengganu (just opposite the Tourist
                                                                            Information Center). You will be ferried to the
                                                                            island (return ferry tickets are included in
                                                                            the package).
                                                                            </br> To get to Kuala Terengganu:
                                                                            </br>
                                                                            <h5>1) Flight</h5>
                                                                            <p>
                                                                             (it’s best to arrive at Kuala Terengganu Airport before 8.30am as the Ferry leaves at 9am)
                                                                            </br> Suggested timing : first flight out – around
                                                                            7am
                                                                            </br> From Subang Airport : you can fly Malindo or
                                                                            Firefly
                                                                            </br> From KLIA 2 : you can fly Air Asia or MAS
                                                                            </br> * From Kuala Terengganu Airport to Shahbandar
                                                                            Jetty by taxi will take 20 minutes, and the
                                                                            </br> fare is RM30 single trip per cab.
                                                                            </br>
                                                                            </p>
                                                                            <h5>2) Bus</h5>
                                                                              <p>
                                                                            (arrive at KT coach station terminal — 6 to 7 hour journey)
                                                                            </br> Suggested timing : Bus that leaves KL the night
                                                                            before – Overnight bus.
                                                                            </br> From TBS, Terminal Bersepadu Selatan, in KL
                                                                            </br> From Pudu Raya Bus Terminal or Pekeliling Bus
                                                                            Station, in KL
                                                                            </br>
                                                                               </p>
                                                                            <h5>3) Car pool & park </h5>
                                                                            <p>
                                                                            Suggested timing : (it’s best to arrive at Shahbandar Jetty by 8.30am as the Ferry leaves at 9am)
                                                                            </br> If you are driving, do carpool — driving alone
                                                                            tends to be boring. There are a few parking facilities
                                                                            at and near Shahbandar Jetty,
                                                                            </br> but the facilities are limited. The car parking
                                                                            fees range between MYR7.00 to MYR10.00 per night.
                                                                            </br>
                                                                            </p>
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="item">
                                                    <div style="width:100%">
                                                        <div class="container-fluid">
                                                            <div class="col-md-12">
                                                                <div class="row">
                                                                    <div class="col-md-10 col-md-offset-1 faq-Model-Style-Header">
                                                                        <h4 style="margin-left: 14px;">FERRY SCHEDULE</h4>
                                                                    </div>
                                                                    <div class="col-md-10 col-md-offset-1">
                                                                        <p class="PStyle">
                                                                            9am to Redang Island. 11am from Redang Island.
                                                                            <br> It’s about an hour and a half each way. Ferry
                                                                            departs from Syahbandar Jetty in Kuala Terengganu
                                                                            and drops you off in front of the resorts.
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="item">
                                                    <div style="width:100%">
                                                        <div class="container-fluid">
                                                            <div class="col-md-12">
                                                                <div class="row">
                                                                    <div class="col-md-10 col-md-offset-1 faq-Model-Style-Header">
                                                                        <h4 style="margin-left: 14px;">ARE FOOD & DRINKS AVAILABLE?</h4>
                                                                    </div>
                                                                    <div class="col-md-10 col-md-offset-1">
                                                                        <p class="PStyle">
                                                                            Your Package includes breakfast, which will be served at your designated resort. After breakfast, food & beverage can be
                                                                            purchased at the festival site throughout the
                                                                            day. Corkage is applicable for own beverages
                                                                            at festival grounds.
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="item">
                                                    <div style="width:100%">
                                                        <div class="container-fluid">
                                                            <div class="col-md-12">
                                                                <div class="row">
                                                                    <div class="col-md-10 col-md-offset-1 faq-Model-Style-Header">
                                                                        <h4 style="margin-left: 14px;">CAN I PURCHASE THE PACKAGE ON THE ISLAND?</h4>
                                                                    </div>
                                                                    <div class="col-md-10 col-md-offset-1">
                                                                        <p class="PStyle">
                                                                            NO. All packages are pre-sold.
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="item">
                                                    <div style="width:100%">
                                                        <div class="container-fluid">
                                                            <div class="col-md-12">
                                                                <div class="row">
                                                                    <div class="col-md-10 col-md-offset-1 faq-Model-Style-Header">
                                                                        <h4 style="margin-left: 14px;">HOW OLD DO I HAVE TO BE TO ENTER THE FESTIVAL?</h4>
                                                                    </div>
                                                                    <div class="col-md-10 col-md-offset-1">
                                                                        <p class="PStyle">
                                                                            You must strictly be aged 18 years old or above. 21 years and above to purchase alcohol.
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="item">
                                                    <div style="width:100%">
                                                        <div class="container-fluid">
                                                            <div class="col-md-12">
                                                                <div class="row">
                                                                    <div class="col-md-10 col-md-offset-1 faq-Model-Style-Header">
                                                                        <h4 style="margin-left: 14px;">DO’S</h4>
                                                                    </div>

                                                                    <div class="col-md-10 col-md-offset-1">
                                                                        <p class="PStyle">

                                                                            – Bring your sunglasses and sunscreen
                                                                            <br> – Have enough cash as there are no ATM machines
                                                                            available on Redang Island.
                                                                            <br> – Credit card terminals are available.
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="item">
                                                    <div style="width:100%">
                                                        <div class="container-fluid">
                                                            <div class="col-md-12">
                                                                <div class="row">
                                                                    <div class="col-md-10 col-md-offset-1 faq-Model-Style-Header">
                                                                        <h4 style="margin-left: 14px;">DON’TS</h4>
                                                                    </div>
                                                                    <div class="col-md-10 col-md-offset-1">
                                                                        <p class="PStyle">

                                                                            – No illegal Drugs or Weapons
                                                                            <br> – No collecting, damaging or taking home any
                                                                            form of plants, marine life / corals. You will
                                                                            be Fined!
                                                                            <br> – No outside Food & Beverages at festival grounds.
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row faq-navigation">
                                    <div class="col-md-12 faq-navigation-col">
                                        <a class="left carousel-control" href="#faqCarousel" data-slide="prev" onclick="onPrev(this)">
                                            <span class="glyphicon glyphicon-chevron-left"></span>
                                            <span class="sr-only sr-prev"></span>
                                        </a>
                                        <a class="right carousel-control" href="#faqCarousel" data-slide="next" onclick="onNext(this)">
                                            <span class="glyphicon glyphicon-chevron-right"></span>
                                            <span class="sr-only sr-next">HOW DO I GET THERE?</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="video" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div role="document" style="margin-top:23px; ">
            <div class="container">
                <div class="row">
                    <div class="modal-content">
                        <div class="modal-header" style="border-bottom:0px;">
                            <div>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                            </div>
                            </button>
                            <div>
                                <h4 style="text-align: center;font-weight: normal;"> VIDEO </h4>
                            </div>
                        </div>
                        <div class="modal-body">
                            {{-- <iframe width="100%" height="480" src="https://www.youtube.com/embed/Zo-Q8-Hfr0A" frameborder="0" allow="autoplay; encrypted-media"
                                allowfullscreen></iframe> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="gallery" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div role="document" style="margin-top:23px; ">
            <div class="container">
                <div class="row">
                    <div class="modal-content">
                        <div class="modal-header" style="border-bottom: 0px;" >
                            <div>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                            </div>
                            </button>
                            <div>
                                <h4 style="text-align: center;font-weight: normal;"> Gallery </h4>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="demo">
                                <ul class="lightSlider">
                                    <li data-thumb="images/header/g.jpg" >
                                        <img src="images/header/g.jpg" class="img-responsive responsive img-fluid " />
                                    </li>
                                    <li data-thumb="images/header/g2.jpg">
                                        <img src="images/header/g2.jpg" class="img-responsive responsive img-fluid "/>
                                    </li>
                                    <li data-thumb="images/header/g3.jpg">
                                        <img src="images/header/g3.jpg" class="img-responsive  responsive img-fluid"/>
                                    </li>
                                    <li data-thumb="images/header/g4.jpg">
                                        <img src="images/header/g4.jpg" class="img-responsive  responsive img-fluid"/>
                                    </li>
                                    <li data-thumb="images/header/g5.jpg">
                                        <img src="images/header/g5.jpg" class="img-responsive  responsive img-fluid"/>
                                    </li>
                                    {{-- <li data-thumb="images/header/g6.jpg">
                                        <img src="images/header/g6.jpg" class="img-responsive responsive img-fluid"/>
                                    </li> --}}
                                    <li data-thumb="images/header/g7.jpg">
                                        <img src="images/header/g7.jpg" class="img-responsive  responsive img-fluid"/>
                                    </li>
                                    <li data-thumb="images/header/g8.jpg">
                                        <img src="images/header/g8.jpg" class="img-responsive  responsive img-fluid"/>
                                    </li>
                                    <li data-thumb="images/header/g9.jpg">
                                        <img src="images/header/g9.jpg" class="img-responsive  responsive img-fluid"/>
                                    </li>
                                    <li data-thumb="images/header/g10.jpg">
                                        <img src="images/header/g10.jpg" class="img-responsive  responsive img-fluid" />
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer"  style="border-top: 0px;">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="Activities" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div role="document" style="margin-top:23px; ">
        <div class="container">
            <div class="row">
                <div class="modal-content">
                    <div class="modal-header" style="border-bottom: 0px;">
                        <div>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                        </div>
                        </button>
                        <div>
                            <h4 style="text-align: center;font-weight: normal;"> ACTIVITES </h4>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="container" style="padding: 40px; ">
                            <div class="row">
                                    <div class="col-sm-12 col-md-2 col-md-offset-1 child">
                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/a1.png" alt="">
                                    </div>
                                    <div class="col-sm-12 col-md-2 child">
                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/a2.png" alt="">
                                    </div>
                                    <div class="col-sm-12 col-md-2 child">
                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/a3.png" alt="">
                                    </div>
                                    <div class="col-sm-12 col-md-2 child">
                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/a4.png" alt="">
                                    </div>
                                    <div class="col-sm-12 col-md-2 child">
                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/a5.png" alt="">
                                    </div>
                                    <div class="col-sm-12 col-md-offset-1 col-md-2  child">
                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/a6.png" alt="">
                                    </div>
                                    <div class="col-sm-12 col-md-2 child">
                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/a7.png" alt="">
                                               
                                    </div>
                                    <div class="col-sm-12 col-md-2 child">
                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/a8.png" alt="">
                                               
                                    </div>
                                    <div class="col-sm-12 col-md-2 child">
                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/a9.png" alt="">
                                    </div>
                                    <div class="col-sm-12 col-md-2 child">
                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/a10.png" alt="">
                                    </div>
                                    <div class="col-md-1">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="Artist" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div role="document" style="margin-top:23px; ">
        <div class="container">
            <div class="row">
                <div class="modal-content">
                    <div class="modal-header"  style="border-bottom: 0px;"> 
                        <div>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                        </div>
                        </button>
                        <div>
                            <h4 style="text-align: center;font-weight: normal;"> ARTISTS </h4>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div id="ArtistisCarousel" class="carousel slide" data-ride="false" data-interval="false">

                            <!-- Wrapper for slides -->
                            <div class="carousel-inner">
                                <div class="item active">
                                    <div style="width:100%">
                                        <div class="container  check-pad" >
                                            <div class="row check-pad-row">
                                                  <div class=" col-sm-12 col-md-2 col-md-offset-1 child">
                                                     <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/a-kid.png" alt="">
                                                   </div>
                                                   <div class=" col-sm-12  col-md-2 child">
                                                      <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/2.png" alt="">
                                                   </div>
                                                   <div class="col-sm-12  col-md-2 child">
                                                         <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/4.png" alt="">
                                                   </div>
                                                   <div class="col-sm-12  col-md-2 child">
                                                        <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/5.png" alt="">
                                                   </div>
                                                    <div class="col-sm-12  col-md-2 child">
                                                       <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/6.png" alt="">
                                                   </div>
                                                   <div class="col-sm-12  col-md-2 col-md-offset-1 child">
                                                       <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/fazz.png" alt="">
                                                   </div>
                                                   <div class="col-sm-12  col-md-2 child">
                                                        <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/7.png" alt="">
                                                   </div>
                                                   <div class="col-sm-12  col-md-2 child">
                                                       <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/jenni-f.png" alt="">
                                                   </div>
                                                  <div class="col-sm-12  col-md-2 child">
                                                       <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/8.png" alt="">
                                                   </div>
                                                   <div class="col-sm-12  col-md-2 child">
                                                       <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/9.png" alt="">
                                                   </div>
                                                   <div class="col-sm-12  col-md-2 child col-md-offset-1">
                                                      <img class="img-responsive center-block artist-img " src="images/islandmusicfest2018v2/11.png" alt="">
                                                   </div>
                                                   <div class="col-sm-12  col-md-2 child ">
                                                      <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/12.png" alt="">
                                                   </div>
                                                   <div class="col-sm-12  col-md-2 child">
                                                       <img class="img-responsive cente r-block artist-img" src="images/islandmusicfest2018v2/13.png" alt="">
                                                   </div>
                                                   <div class="col-sm-12  col-md-2 child">
                                                       <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/14.png" alt="">
                                                   </div>
                                                   <div class="col-sm-12  col-md-2 child"> 
                                                      <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/15.png" alt="">
                                                   </div>
                                                </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="item">
                                    <div style="width:100%">

                                      <div class="container  check-pad" >
                                            <div class="row check-pad-row">
                                                    <div class="col-sm-12  col-md-2 child col-md-offset-1">
                                                        <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/16.png" alt="">
                                                    </div>
                                                    <div class="col-sm-12  col-md-2 child">
                                                        <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/17.png" alt="">
                                                    </div>
                                                    <div class="col-sm-12  col-md-2 child">
                                                        <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/18.png" alt="">
                                                    </div>
                                                    <div class="col-sm-12  col-md-2 child" >
                                                        <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/19.png" alt="">
                                                    </div>
                                                    <div class="col-sm-12  col-md-2 child">
                                                        <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/20.png" alt="">
                                                    </div>
                                                    <div class="col-sm-12  col-md-2 child col-md-offset-1">
                                                        <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/21.png" alt="">
                                                    </div>
                                                    <div class="col-sm-12  col-md-2 child">
                                                        <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/22.png" alt="">
                                                    </div>
                                                    <div class="col-sm-12  col-md-2 child">
                                                        <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/23.png" alt="">
                                                    </div>
                                                    <div class="col-sm-12  col-md-2 child">
                                                        <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/24.png" alt="">
                                                    </div>
                                                    <div class="col-sm-12  col-md-2 child">
                                                        <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/25.png" alt="">
                                                    </div>
                                                    <div class="col-sm-12  col-md-offset-4 col-md-2 child">
                                                        <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/wyman.png" alt="">
                                                    </div>
                                                    <div class="col-sm-12 col-md-2 child">
                                                        <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/26.png" alt="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    </div>
                                </div>

                            </div>

                            <!-- Left and right controls -->
                            <a class="left carousel-control-left carousel-control" href="#ArtistisCarousel" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="right  carousel-control-right carousel-control" href="#ArtistisCarousel" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>

                    </div>
                </div>

            </div>
            <div class="modal-footer"  style="border-top: 0px;">

            </div>
        </div>
    </div>
</div>
</div>
</div>
<div class="modal fade" id="Artistschedule" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div role="document" style="margin-top:23px; ">
        <div class="container">
            <div class="row">
                <div class="modal-content">
                    <div class="modal-header"  style="border-bottom: 0px;">
                        <div>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                        </div>
                        </button>
                        <div>
                            <h4 style="text-align: center;font-weight: normal;"> ARTISTS SCHEDULE </h4>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="container" style="padding-right: 54px; ">
                            <div class="row">
                                <div class="col-md-6  big-screen">
                                    <h6 style="color: rgba(29, 29, 29, 0.65); text-align: center;"> Day 1 (12 October 2018) </h6>
                                    <div class="row">
                                        <div class="col-md-6 manage-smart">
                                            <ul style="color:#FD9394;">

                                                <li>
                                                    <span class="color-list1">3:00 PM</span>
                                                <span class="time-span1">LSP</span>
                                                </li>
                                                <li>
                                                    <span class="color-list1">4:00 PM</span>
                                                    <span class="time-span1">Peanut Butter Jelly Jam</span>
                                                </li>
                                                <li>
                                                    <span class="color-list1">4:30 PM</span>
                                                    <span class="time-span1">Jumero</span>
                                                </li>
                                                <li>
                                                    <span class="color-list1">5:00 PM</span>
                                                    <span class="time-span1">Bihzhu</span>
                                                </li>
                                                <li>
                                                    <span class="color-list1">5:30 PM</span>
                                                    <span class="time-span1">Gabriel Lynch</span>
                                                </li>
                                                <li>
                                                    <span class="color-list1">6:00 PM</span>
                                                    <span class="time-span1">Sea Travel</span>
                                                </li>
                                                <li>
                                                    <span class="color-list1">6:30 PM</span>
                                                    <span class="time-span1">NJWA</span>
                                                </li>
                                                <li>
                                                    <span class="color-list1">7:00 PM</span>
                                                    <span class="time-span1">Christian Theseira</span>
                                                </li>
                                                <li>
                                                    <span class="color-list1">7:30 PM</span>
                                                    <span class="time-span1">Dani</span>
                                                </li>
                                                <li>
                                                    <span class="color-list1">8:05 PM </span>
                                                    <span class="time-span1">Fazz</span>
                                                </li>
                                                <li>
                                                    <span class="color-list1">8:30 PM </span>
                                                    <span class="time-span1">Kaya</span>
                                                </li>
                                                <li>
                                                    <span class="color-list1">9:00 PM </span>
                                                    <span class="time-span1">Mass Music</span>
                                                </li>
                                                <li>
                                                    <span class="color-list1">9:30 PM </span>
                                                    <span class="time-span1">Skeletor, Lil J, A-Kid</span>
                                                </li>
                                                <li>
                                                    <span class="color-list1">10:00 PM </span>
                                                    <span class="time-span1">Leng Sisters</span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-md-6 manage-smart manage-smart1">
                                            <div class="row">
                                                <div class="col-md-4 manage-smart-loop" style="margin-bottom: 25px;">
                                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/peanut-butter-jelly-jam.png" alt="">
                                                </div>
                                                <div class="col-md-4 manage-smart-loop" style="margin-bottom: 25px;">
                                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/jumero.png" alt="">
                                                </div>
                                                <div class="col-md-4 manage-smart-loop" style="margin-bottom: 25px;">
                                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/bihzhu.png" alt="">
                                                </div>
                                                <div class="col-md-4 manage-smart-loop" style="margin-bottom: 25px;">
                                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/gabriel-lynch.png" alt="">
                                                </div>
                                                <div class="col-md-4 manage-smart-loop" style="margin-bottom: 25px;">
                                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/seatravel.png" alt="">
                                                </div>
                                                <div class="col-md-4 manage-smart-loop" style="margin-bottom: 25px;">
                                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/njwa.png" alt="">
                                                </div>
                                                <div class="col-md-4 manage-smart-loop" style="margin-bottom: 25px;">
                                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/christian-theseira.png" alt="">
                                                </div>
                                                <div class="col-md-4 manage-smart-loop" style="margin-bottom: 25px;">
                                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/dani.png" alt="">
                                                </div>
                                                <div class="col-md-4 manage-smart-loop" style="margin-bottom: 25px;">
                                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/fazz.png" alt="">
                                                </div>
                                                <div class="col-md-4 manage-smart-loop" style="margin-bottom: 25px;">
                                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/kaya.png" alt="">
                                                </div>
                                                <div class="col-md-4 manage-smart-loop" style="margin-bottom: 25px;">
                                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/massmusic.png" alt="">
                                                </div>
                                                <div class="col-md-4 manage-smart-loop"  style="margin-bottom: 25px;">
                                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/skeletor.png" alt="">
                                                </div>
                                                <div class="col-md-4 manage-smart-loop" >
                                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/leng-sisters.png" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6 big-screen  ">
                                <h6 style="color: rgba(29, 29, 29, 0.65); text-align: center;"> Day 2 (13 October 2018)  </h6>
                                  
                                    <div class="row">
                                        <div class="col-md-6 manage-smart manage-smart1">
                                            <ul style="color:#FD9394;">
                                                <li>
                                                    <span class="color-list1">2:30 PM</span>
                                                    <span class="time-span1">Skeletor</span>
                                                </li>
                                                <li>
                                                    <span class="color-list1">4:00 PM</span>
                                                    <span class="time-span1">Peanut Butter Jelly Jam</span>
                                                </li>
                                                <li>
                                                    <span class="color-list1">4:30 PM</span>
                                                    <span class="time-span1">Pris Xavier</span>
                                                </li>
                                                <li>
                                                    <span class="color-list1">5:00 PM</span>
                                                    <span class="time-span1">Annatasha</span>
                                                </li>
                                                <li>
                                                    <span class="color-list1">5:30 PM</span>
                                                    <span class="time-span1">Sound of Jane</span>
                                                </li>
                                                <li>
                                                    <span class="color-list1">6:00 PM</span>
                                                    <span class="time-span1">Last Logic</span>
                                                </li>
                                                <li>
                                                    <span class="color-list1">6:30 PM</span>
                                                    <span class="time-span1">Rhythm Rebels</span>
                                                </li>
                                                <li>
                                                    <span class="color-list1">7:00 PM</span>
                                                    <span class="time-span1">Vital Signs</span>
                                                </li>
                                                <li>
                                                    <span class="color-list1">7:30 PM</span>
                                                    <span class="time-span1">PJ12</span>
                                                </li>
                                                <li>
                                                    <span class="color-list1"> 8:05 PM </span>
                                                    <span class="time-span1">Zupanova</span>
                                                </li>
                                                <li>
                                                    <span class="color-list1"> 8:30 PM </span>
                                                    <span class="time-span1">LSP, UCOP & Wymann</span>
                                                </li>
                                                <li>
                                                    <span class="color-list1">9:30 PM </span>
                                                    <span class="time-span1">Jenni F</span>
                                                </li>
                                                <li>
                                                    <span class="color-list1">10:30 PM </span>
                                                    <span class="time-span1">Oddicon</span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-md-6 manage-smart manage-smart1">
                                            <div class="row">
                                                <div class="col-md-4 manage-smart-loop" style="margin-bottom: 25px;">
                                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/skeletor.png" alt="">
                                                </div>
                                                <div class="col-md-4 manage-smart-loop" style="margin-bottom: 25px;">
                                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/peanut-butter-jelly-jam.png" alt="">
                                                </div>
                                                <div class="col-md-4 manage-smart-loop" style="margin-bottom: 25px;">
                                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/pris-xavier.png" alt="">
                                                </div>
                                                <div class="col-md-4 manage-smart-loop" style="margin-bottom: 25px;">
                                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/annatasha.png" alt="">
                                                </div>
                                                <div class="col-md-4 manage-smart-loop" style="margin-bottom: 25px;">
                                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/sounds-of-jane.png" alt="">
                                                </div>
                                                <div class="col-md-4 manage-smart-loop" style="margin-bottom: 25px;">
                                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/lastlogic.png" alt="">
                                                </div>
                                                <div class="col-md-4 manage-smart-loop" style="margin-bottom: 25px;">
                                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/rhythm-rebels.png" alt="">
                                                </div>
                                                <div class="col-md-4 manage-smart-loop" style="margin-bottom: 25px;">
                                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/vital-signals.png" alt="">
                                                </div>
                                                <div class="col-md-4 manage-smart-loop" style="margin-bottom: 25px;">
                                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/zupanova.png" alt="">
                                                </div>
                                                <div class="col-md-4 manage-smart-loop" style="margin-bottom: 25px;">
                                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/lspxucop.png" alt="">
                                                </div>
                                                <div class="col-md-4 manage-smart-loop" style="margin-bottom: 25px;">
                                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/wyman.png" alt="">
                                                </div>
                                                <div class="col-md-4 manage-smart-loop" style="margin-bottom: 25px;">
                                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/jenni-f.png" alt="">
                                                </div>
                                                <div class="col-md-4 manage-smart-loop" style="margin-bottom: 25px;">
                                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/oddicon.png" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                        <div id="myCarouselll" class="carousel slide small-screen  big-screen"   data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            <li data-target="#myCarouselll" data-slide-to="0" class="active"></li>
                            <li data-target="#myCarouselll" data-slide-to="1"></li>
                            <li data-target="#myCarouselll" data-slide-to="2"></li>
                        </ol>

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner">
                            <div class="item active">
                            <div class="col-md-6">
                                    <h6 style="color: rgba(29, 29, 29, 0.65); text-align: center;"> Day 1 (12 October 2018) </h6>
                                    <div class="row">
                                        <div class="col-md-6 manage-smart">
                                            <ul style="color:#FD9394;">

                                                <li>
                                                    <span class="color-list1">3:00 PM</span>
                                                <span class="time-span1">LSP</span>
                                                </li>
                                                <li>
                                                    <span class="color-list1">4:00 PM</span>
                                                    <span class="time-span1">Peanut Butter Jelly Jam</span>
                                                </li>
                                                <li>
                                                    <span class="color-list1">4:30 PM</span>
                                                    <span class="time-span1">Jumero</span>
                                                </li>
                                                <li>
                                                    <span class="color-list1">5:00 PM</span>
                                                    <span class="time-span1">Bihzhu</span>
                                                </li>
                                                <li>
                                                    <span class="color-list1">5:30 PM</span>
                                                    <span class="time-span1">Gabriel Lynch</span>
                                                </li>
                                                <li>
                                                    <span class="color-list1">6:00 PM</span>
                                                    <span class="time-span1">Sea Travel</span>
                                                </li>
                                                <li>
                                                    <span class="color-list1">6:30 PM</span>
                                                    <span class="time-span1">NJWA</span>
                                                </li>
                                                <li>
                                                    <span class="color-list1">7:00 PM</span>
                                                    <span class="time-span1">Christian Theseira</span>
                                                </li>
                                                <li>
                                                    <span class="color-list1">7:30 PM</span>
                                                    <span class="time-span1">Dani</span>
                                                </li>
                                                <li>
                                                    <span class="color-list1">8:05 PM </span>
                                                    <span class="time-span1">Fazz</span>
                                                </li>
                                                <li>
                                                    <span class="color-list1">8:30 PM </span>
                                                    <span class="time-span1">Kaya</span>
                                                </li>
                                                <li>
                                                    <span class="color-list1">9:00 PM </span>
                                                    <span class="time-span1">Mass Music</span>
                                                </li>
                                                <li>
                                                    <span class="color-list1">9:30 PM </span>
                                                    <span class="time-span1">Skeletor, Lil J, A-Kid</span>
                                                </li>
                                                <li>
                                                    <span class="color-list1">10:00 PM </span>
                                                    <span class="time-span1">Leng Sisters</span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-md-6 manage-smart manage-smart1">
                                            <div class="row">
                                                <div class="col-md-4 manage-smart-loop" style="margin-bottom: 25px;">
                                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/peanut-butter-jelly-jam.png" alt="">
                                                </div>
                                                <div class="col-md-4 manage-smart-loop" style="margin-bottom: 25px;">
                                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/jumero.png" alt="">
                                                </div>
                                                <div class="col-md-4 manage-smart-loop" style="margin-bottom: 25px;">
                                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/bihzhu.png" alt="">
                                                </div>
                                                <div class="col-md-4 manage-smart-loop" style="margin-bottom: 25px;">
                                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/gabriel-lynch.png" alt="">
                                                </div>
                                                <div class="col-md-4 manage-smart-loop" style="margin-bottom: 25px;">
                                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/seatravel.png" alt="">
                                                </div>
                                                <div class="col-md-4 manage-smart-loop" style="margin-bottom: 25px;">
                                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/njwa.png" alt="">
                                                </div>
                                                <div class="col-md-4 manage-smart-loop" style="margin-bottom: 25px;">
                                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/christian-theseira.png" alt="">
                                                </div>
                                                <div class="col-md-4 manage-smart-loop" style="margin-bottom: 25px;">
                                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/dani.png" alt="">
                                                </div>
                                                <div class="col-md-4 manage-smart-loop" style="margin-bottom: 25px;">
                                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/fazz.png" alt="">
                                                </div>
                                                <div class="col-md-4 manage-smart-loop" style="margin-bottom: 25px;">
                                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/kaya.png" alt="">
                                                </div>
                                                <div class="col-md-4 manage-smart-loop" style="margin-bottom: 25px;">
                                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/massmusic.png" alt="">
                                                </div>
                                                <div class="col-md-4 manage-smart-loop"  style="margin-bottom: 25px;">
                                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/skeletor.png" alt="">
                                                </div>
                                                <div class="col-md-4 manage-smart-loop" >
                                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/leng-sisters.png" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="item">
                                <div class="col-md-6">
                                    <h6 style="color: rgba(29, 29, 29, 0.65); text-align: center;"> Day 2 (13 October 2018)  </h6>
                                  
                                    <div class="row">
                                        <div class="col-md-6 manage-smart manage-smart1">
                                            <ul style="color:#FD9394;">
                                                <li>
                                                    <span class="color-list1">2:30 PM</span>
                                                    <span class="time-span1">Skeletor</span>
                                                </li>
                                                <li>
                                                    <span class="color-list1">4:00 PM</span>
                                                    <span class="time-span1">Peanut Butter Jelly Jam</span>
                                                </li>
                                                <li>
                                                    <span class="color-list1">4:30 PM</span>
                                                    <span class="time-span1">Pris Xavier</span>
                                                </li>
                                                <li>
                                                    <span class="color-list1">5:00 PM</span>
                                                    <span class="time-span1">Annatasha</span>
                                                </li>
                                                <li>
                                                    <span class="color-list1">5:30 PM</span>
                                                    <span class="time-span1">Sound of Jane</span>
                                                </li>
                                                <li>
                                                    <span class="color-list1">6:00 PM</span>
                                                    <span class="time-span1">Last Logic</span>
                                                </li>
                                                <li>
                                                    <span class="color-list1">6:30 PM</span>
                                                    <span class="time-span1">Rhythm Rebels</span>
                                                </li>
                                                <li>
                                                    <span class="color-list1">7:00 PM</span>
                                                    <span class="time-span1">Vital Signs</span>
                                                </li>
                                                <li>
                                                    <span class="color-list1">7:30 PM</span>
                                                    <span class="time-span1">PJ12</span>
                                                </li>
                                                <li>
                                                    <span class="color-list1"> 8:05 PM </span>
                                                    <span class="time-span1">Zupanova</span>
                                                </li>
                                                <li>
                                                    <span class="color-list1"> 8:30 PM </span>
                                                    <span class="time-span1">LSP, UCOP & Wymann</span>
                                                </li>
                                                <li>
                                                    <span class="color-list1">9:30 PM </span>
                                                    <span class="time-span1">Jenni F</span>
                                                </li>
                                                <li>
                                                    <span class="color-list1">10:30 PM </span>
                                                    <span class="time-span1">Oddicon</span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-md-6 manage-smart manage-smart1">
                                            <div class="row">
                                                <div class="col-md-4 manage-smart-loop" style="margin-bottom: 25px;">
                                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/skeletor.png" alt="">
                                                </div>
                                                <div class="col-md-4 manage-smart-loop" style="margin-bottom: 25px;">
                                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/peanut-butter-jelly-jam.png" alt="">
                                                </div>
                                                <div class="col-md-4 manage-smart-loop" style="margin-bottom: 25px;">
                                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/pris-xavier.png" alt="">
                                                </div>
                                                <div class="col-md-4 manage-smart-loop" style="margin-bottom: 25px;">
                                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/annatasha.png" alt="">
                                                </div>
                                                <div class="col-md-4 manage-smart-loop" style="margin-bottom: 25px;">
                                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/sounds-of-jane.png" alt="">
                                                </div>
                                                <div class="col-md-4 manage-smart-loop" style="margin-bottom: 25px;">
                                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/lastlogic.png" alt="">
                                                </div>
                                                <div class="col-md-4 manage-smart-loop" style="margin-bottom: 25px;">
                                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/rhythm-rebels.png" alt="">
                                                </div>
                                                <div class="col-md-4 manage-smart-loop" style="margin-bottom: 25px;">
                                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/vital-signals.png" alt="">
                                                </div>
                                                <div class="col-md-4 manage-smart-loop" style="margin-bottom: 25px;">
                                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/zupanova.png" alt="">
                                                </div>
                                                <div class="col-md-4 manage-smart-loop" style="margin-bottom: 25px;">
                                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/lspxucop.png" alt="">
                                                </div>
                                                <div class="col-md-4 manage-smart-loop" style="margin-bottom: 25px;">
                                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/wyman.png" alt="">
                                                </div>
                                                <div class="col-md-4 manage-smart-loop" style="margin-bottom: 25px;">
                                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/jenni-f.png" alt="">
                                                </div>
                                                <div class="col-md-4 manage-smart-loop" style="margin-bottom: 25px;">
                                                    <img class="img-responsive center-block artist-img" src="images/islandmusicfest2018v2/oddicon.png" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
    
                            </div>

                            
                        </div>

                        <!-- Left and right controls -->
                        <a class="left carousel-control" href="#myCarouselll" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#myCarouselll" data-slide="next" style="width:0%;">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                            <span class="sr-only">Next</span>
                        </a>
                        </div>
                            </div>
                        </div>
                        <div class="modal-footer" style="border-top: 0px;">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<section class="ticket-area">
    <div class="container-fluid">
        <div class="row">
            <div class="container">
                <div class="row">
                    <h3 class="ticket-pr"> YOUR TICKET PRICE INCLUDE THIS: </h3>
                    <div class="col-md-3 down">
                        <img class="img-responsive ti-img" src="images/ticket-p/Ferry.png" alt="">
                        <h4 id="text-rol" class="ti-img-rol1"> Transportation:</h4>
                        <h6 id="text-rol21" class="ti-img-rol22"> Free transportation </br> from mainland</h6>
                    </div>
                    <div class="col-md-3 down">
                        <img class="img-responsive ti-img" src="images/ticket-p/Accomodation.png" alt="">
                       
                        <h4 id="text-rol1" class="ti-img-rol2"> Accomodation</h4>
                    </div>
                    <div class="col-md-3 down">
                    <img class="img-responsive ti-img" src="images/ticket-p/Festival.png" alt="">
                   
                        <h4 id="text-rol2" class="ti-img-rol3"> Festival:</h4>
                        <h6 id="text-rol22" class="ti-img-rol22"> Island Music Festival</h6>
                    </div>
                    <div class="col-md-3 down">
                    <img class="img-responsive ti-img" src="images/ticket-p/Breakfast.png" alt="">
                        <h4 id="text-rol3" class="ti-img-rol4"> Breakfast</h4>
                        <h6 id="text-rol23" class="ti-img-rol22"> Breakfast include</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="ticket-prices-section" id="gettickets">
    <div class="container-fluid big-screen">
        <div class="row">
            <h3 class="ticket-prices"> TICKET PRICES </h3>
            <div class="col-md-3">
                <img class="img-responsive" src="images/ticket-list/Earlybird.png" alt="">
            </div>
            <div class="col-md-3">
                <img class="img-responsive" src="images/ticket-list/Tier1.png" alt="">
            </div>
            <div class="col-md-3">
                <img class="img-responsive" src="images/ticket-list/Tier2.png" alt="">
            </div>
            <div class="col-md-3">
                <img class="img-responsive" src="images/ticket-list/Tier3.png" alt="">
            </div>
            <div class="GetTicket" style="margin: 0px auto; margin-bottom: 30px;">
            {{-- <a target="_blank" href="https://redtix-tickets.airasia.com/en-AU/shows/island music festival 2018 (12 to 14 october 2018)/events"> --}}
                <input type="submit" value="Buy Ticket" name="subscribe" id="mc-embedded-subscribe" class="button btn btn-style-ticket btn-block disabled">
            {{-- </a> --}}
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container  small-screen">
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators hide-filed">
                <li data-target="#myCarousel" data-slide-to="0" class="bold1 active"></li>
                <li data-target="#myCarousel" data-slide-to="1" class="bold1"></li>
                <li data-target="#myCarousel" data-slide-to="2" class="bold1"></li>
                <li data-target="#myCarousel" data-slide-to="3" class="bold1"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <div class="item active  ">
                    <img class="img-responsive ti-img2" src="images/ticket-list/Earlybird.png" alt="">
                </div>

                <div class="item">
                    <img class="img-responsive ti-img2" src="images/ticket-list/Tier1.png" alt="">
                </div>
                <div class="item">
                    <img class="img-responsive ti-img2" src="images/ticket-list/Tier2.png" alt="">
                </div>

                <div class="item">
                    <img class="img-responsive ti-img2" src="images/ticket-list/Tier3.png" alt="">
                </div>

            </div>
        </div>
        <div class="GetTicket" style="margin: 0px auto;  ">
             {{-- <a target="_blank" href="https://redtix-tickets.airasia.com/en-AU/shows/island music festival 2018 (12 to 14 october 2018)/events"> --}}
                <input type="submit" value="Buy Ticket" name="subscribe" id="mc-embedded-subscribe" class="button btn  btn-style-ticket btn-block disabled">
            {{-- </a> --}}
        </div>
    </div>
</section>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar section-white">
            <section class="pageCategory-section last line-up">
                <div class="container">
                    <div class="row">
                        
                    </div>
                </div>
            </section>
           

            <section class="pageCategory-section last hide-div">
                <div class="container-fluid tixPrice">
                    <div class="row">
                        <div class="col-sm-10 bottom-el">
                        <div class="clearfix">&nbsp;</div>
                            <div class="note text-left">
                            <span class="importantNote">1. Package price for Single Occupancy entitles 1 Pax only.</span>
                            <span class="importantNote">2. Package price for Twin Sharing entitles 2 Pax only.</span>
                            <span class="importantNote">3. Ticket price is inclusive of all Island Music Festival Activities and Entertainment.</span>
                            <span class="importantNote">4. Tickets price is inclusive of 3 Days 2 Nights stay at selected partner hotels.</span>
                            <span class="importantNote">5. Organizer will assign the partner hotels based on first come first serve basis.</span>
                            <span class="importantNote">6. Ticket price is inclusive of return ferry transfer (Syahbandar Jetty –> Redang Island -> Syahbandar Jetty)</span>
                                <h2>Important Notes</h2>
                                <ol style="padding-left: 15px">
                                    <li>Prices shown excludes RM4.00 AirAsiaRedTix fee.</li>
                                    <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li>
                                    <li>RM5.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                                    <li>Online ticket selling will close 2 days prior to event day, subject to availability.</li>
                                </ol>
                                <h2>For enquiry only:</h2>
                                <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.3/css/lightslider.min.css">
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.3/js/lightslider.min.js"></script>
    <script type="text/javascript">
        var myFunctionWasCalled = false;

        function loadSlider() {
            destroySlider();
            setTimeout(function () {
                window.lsSlider = $('.lightSlider').lightSlider({
                    gallery: true,
                    item: 1,
                    loop: true,
                    slideMargin: 0,
                    thumbItem: 9
                });
            }, 200);

        }

        function destroySlider() {
            if (window.lsSlider) {
                window.lsSlider.destroy();
                window.lsSlider = void 0;
            }
        }

        function onPrev(anchor) {
            setTimeout(() => {
                //debugger;
                if (!isFirstChild()) {
                    $('.sr-prev').text(prevSlideText());
                    $('.sr-next').text(nextSlideText());
                } else {
                    $('.sr-prev').text('');
                }
            }, 800);
        }

        function onNext(anchor) {
            setTimeout(() => {
                //debugger;
                if (!isLastChild()) {
                    $('.sr-prev').text(prevSlideText());
                    $('.sr-next').text(nextSlideText());
                } else {
                    $('.sr-next').text('');
                }
            }, 800);
        }

        function isFirstChild() {
            let prevSibling = $('#faqCarousel .item.active').prev();
            return !prevSibling || prevSibling.length === 0;
        }

        function isLastChild() {
            let nextSibling = $('#faqCarousel .item.active').next();
            return !nextSibling || nextSibling.length === 0;
        }

        function prevSlideText() {
            let prevSibling = $('#faqCarousel .item.active').prev();
            return prevSibling.find('.faq-Model-Style-Header').text().trim();
        }

        function nextSlideText() {
            let nextSibling = $('#faqCarousel .item.active').next();
            return nextSibling.find('.faq-Model-Style-Header').text().trim();
        }

        function currentSlideText() {
            return $('#faqCarousel .item.active').find('.faq-Model-Style-Header').text().trim();
        }

        $('#myCarousel').on('slid.bs.carousel', function (e) {
            let changeBg = $(e.relatedTarget).find('a.btn-primary').hasClass('change-bg');
            if (changeBg) {
                $('body').removeClass('white-bg').addClass('dark-bg');
                $('#text-rol').removeClass('ti-img-rol1').addClass('dark-bgg-tick');
                $('#text-rol1').removeClass('ti-img-rol2').addClass('dark-bgg-tick');
                $('#text-rol2').removeClass('ti-img-rol3').addClass('dark-bgg-tick');
                $('#text-rol3').removeClass('ti-img-rol4').addClass('dark-bgg-tick');
                $('#text-rol21').removeClass('ti-img-rol22').addClass('dark-bgg-tick');
                $('#text-rol22').removeClass('ti-img-rol22').addClass('dark-bgg-tick');
                $('#text-rol23').removeClass('ti-img-rol22').addClass('dark-bgg-tick');
            } else {
                $('body').removeClass('dark-bg').addClass('white-bg');
                $('#text-rol').removeClass('dark-bgg-tick').addClass('ti-img-rol1 ');
                $('#text-rol1').removeClass('dark-bgg-tick').addClass('ti-img-rol2  ');
                $('#text-rol2').removeClass('dark-bgg-tick').addClass('ti-img-rol3 ');
                $('#text-rol3').removeClass('dark-bgg-tick').addClass('ti-img-rol4');
                $('#text-rol21').removeClass('dark-bgg-tick').addClass('ti-img-rol22 ');
                $('#text-rol22').removeClass('dark-bgg-tick').addClass('ti-img-rol22 ');
                $('#text-rol23').removeClass('dark-bgg-tick').addClass('ti-img-rol22 ');
            }
        })

        // Modal Announcement
        $(document).ready(function(){
        $("#announcementModal").modal('show');
        });	

    </script> 
    @endsection
    
    @section('modal')
        @include('layouts.partials.modals._seatplan')
        @include('layouts.partials.modals._getTixCustom')

      <div id="announcementModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
                    <h1><i class="fa fa-bullhorn text-warning" aria-hidden="true"></i></h1>
                    <h4>Announcement</h4>
                    <div class="clearfix">&nbsp;</div>
                    <div class="well">
                        {{-- <p><b>TICKET SALES TEMPORARILY ON HOLD</b></p>
                        <p>We wish to inform that the sale of Island Music Festival’s tickets are on hold for now. New updates will be announced as soon as possible.</p>
                        <p>For any enquiries, kindly drop us an email at support@airasiaredtix.com</p> --}}
                        <p><h5>Message from Organiser</h5><p>
                        <p>With a heavy heart, Island Music Festival from 12-14 October 2018 has sadly been called off. 😔 </p>
                        <p>We used all best efforts to resolve matters. Even though we had complied with requirements the sudden extremism could not be overcome. We would like to thank those in the state government who were trying to assist us. Unfortunately we cannot align with the changed guidelines. To us
                        everyone should be free to stand equally side by side and enjoy responsibly regardless of sex or religion.</p>
                        <p>We're offering our guests to come to the island this weekend for a beach holiday.</p>
                        <p>We will convert your package to a full board 3d2n package with the resort directly.</p>
                        <p>It'll still include your ferry, food and snorkeling trip.<br />
                        If you choose to cancel do let us know by this Thursday 11th October 2018 and we will process your refund which we anticipate by the end of October.</p>
                        <p>It’s not the end, it’s a new beginning.<br />
                        See you soon beachbums.<br />
                        Thank you.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection