@extends('master')
@section('title')
    Privacy
@endsection

@section('header')
    @include('layouts.partials._header')
@endsection

@section('content')

    <!-- Banner Section -->
    <section class="pageBanner">
      <div class="jumbotron" style="background-image: url('images/bigmainbanner.jpg')">
        <div class="bigBanner-overlay"></div>
        <div class="container banner-body">
          <div class="col-sm-offset-1 col-sm-10 col-xs-12">
            <div class="bannerText text-center">
              <h1>Privacy</h1>
            </div>
          </div>
        </div>
      </div>
    </section><!-- /Banner Section -->

    <!-- Content Section -->
    <section class="pageContent">
      <div class="container">
        
        <div class="row">

          <!-- Main Body -->
          <div class="mainBodyContent col-sm-9">

            <section class="pageCategory-section last">
                <h1 class="mainSecTitle">AirAsia Privacy Policy</h1>
                <p>AirAsiaRedTix.com is concerned about your right to privacy. Therefore, AirAsiaRedTix.com pledges to be responsible when gathering your personal information and to protect your privacy in every possible way. Although, this Privacy Policy is not a contract and does not create any legal rights, however, it does serve as an expression of AirAsiaRedTix.com’s commitment to protecting private personal information.</p>
                <p>It must be borne in mind that AirAsiaRedTix.com reserves the right to change, amend and/or vary this Privacy Policy at any time and will notify users of the same by updating the policy here and by including a "NEWLY UPDATED" label with the "PRIVACY POLICY" link on the web sites governed by this policy for a period of 30 days.</p>
                <p>This Privacy notice aims to answer your questions about the sources and kinds of information AirAsiaRedTix.com collects, how it is used, when it may be shared with others, and how we safeguard its confidentiality and security. We also provide you with ways to correct the data you provide us and the option to limit our sharing of this data with AirAsiaRedTix.com’s authorized third parties.</p>
                <p>This Privacy notice is applicable to all users of AirAsiaRedTix.com’s website @ www.AirAsiaRedTix.com regardless of the intention and without limitation to Travel Reservations, Corporate Sales, Marketing Research, and Customer Relations/Service.</p>
                <p>This Privacy Policy was established within the context of our AirAsiaRedTix.com Website and the potential for gathering personal information within electronic media; however, this Policy is not limited to the AirAsiaRedTix.com Website. Our Privacy Policy applies whenever you and the Company interact and there is a possibility that during such interaction, we may acquire certain information about you.</p>
                <p>It must be borne in mind that AirAsiaRedTix.com reserves the right to change, amend and/or vary this Privacy Policy at any time and will notify users of the same by updating the policy here and by including a "NEWLY UPDATED" label with the "PRIVACY POLICY" link on the web sites governed by this policy for a period of 30 days.</p>
                <hr>
                <h1 class="mainSecTitle">Information Collection </h1>
                <p>To ensure that we successfully process your booking and to effectively manage our business, we collect and maintain personal information about you. We collect customer information from many sources for the purposes of understanding current or new demands, meeting these demands as well as to provide and personalize our services. The means of information collection, among others, are as following:</p>
                <ul>
                    <li>From you; when you voluntarily provide by signing up on AirAsiaRedTix.com</li>
                    <li>From your browser; when you visit our website and your browser interacts with us;</li>
                    <li>Become a registered user of the AirAsiaRedTix.com Website</li>
                    <li>Purchase tickets or admission to an event or venue</li>
                    <li>Sign-up or subscribe for products or services that we may periodically make available</li>
                    <li>Participate in a special promotion</li>
                </ul>
                <p>We do not collect personal information (such as names, addresses, phone numbers, email addresses or credit card numbers) about you except when you specifically provide your information on a voluntary basis. Your personal information may be collected as you:</p>
                <p>In addition, we may be required to share your personal information with certain entertainment organizations and venues (an “Event Host”) as part of our contractual relationship with them. Of course, if you wish to purchase tickets to an event and remain anonymous, you can purchase your tickets in person, directly through the Event Host's box office.</p>
                <p>Whenever required by law, we will disclose your information to the government bodies or authorities or third parties pursuant to a subpoena or other legal process accordingly. Further, we may also use or disclose your information as permitted by law to protect the rights or property of AirAsiaRedTix.com, our customers, our website, or its users. We may also disclose your information, whether in part or in full, to our contracted or authorized companies such as our data processors.</p>
                <p>AirAsiaRedTix.com does not sell or rent any personal information you provide on our web site to any other parties. AirAsiaRedTix.com may share anonymous and/or aggregated information about all our users with third parties.</p>
                <hr>
                <h1 class="mainSecTitle">Using your information:</h1>
                <p>We may utilise your information in the following manner:</p>
                <ul>
                    <li>If you are purchasing tickets or admission to an event or venue, the Company uses your personal information to complete the purchase transaction and to communicate with you about your ticket order, ticket delivery or other inquiry.</li>
                    <li>In addition, whenever you purchase tickets or admission to an event or venue, we may be required to share your personal information with a third party credit card processor to facilitate completion of your transaction. The credit card processor may share the same information with your credit card issuer for the issuer's use in connection with gathering data related to use of their cards, rewards programs or other purposes. You are urged to read and become familiar with the privacy policies governing the use of your credit cards.</li>
                    <li>Alternate: For security reasons, your credit card number is not saved. You must type the credit card number each time you make a purchase.</li>
                    <li>Also, when you purchase a ticket or admission to an event or venue, we may be contractually required to pass your personal information to the Event Host so that they may communicate with you about the event you are going to attend. Under those circumstances, we cannot offer you the option to opt-out from having your information passed to the Event Host.</li>
                    <li>Event Hosts are not subject to the provisions of this Privacy Policy, which means that they may use your personal information for purposes other than contacting you about the event you are going to attend. When you purchase tickets to an event, you should read and become familiar with the privacy policies of the Event Host. AirAsiaRedTix.com has no control over the use of your personal information by any Event Host and you agree that we are not liable for any use of your personal information by an Event Host. If you have special preferences concerning use of your personal information by an Event Host, you must communicate those preferences directly to them.</li>
                    <li>When you visit our Website, we may also gather certain information about the use of your computer that we aggregate with similar information collected from other visitor's computers, which we use for statistical and other purposes.</li>
                    <li>Finally, we may occasionally contact you, to notify you of information that might affect your event experience, or to notify you of event cancellations, or errors in your order. Additionally, we may contact you regarding offerings of future events products or services that may be of interest to you. If you are not interested in receiving email notices or advertisement from us, you can unsubscribe at any time.</li>
                </ul>
                <hr>
                <h1 class="mainSecTitle">Tracking Information</h1>
                <p>We use IP addresses to analyse trends, administer the site, track user's movement, and gather broad demographic information for aggregate use. IP addresses are not linked to personally identifiable information.</p>
                <p>We also utilise cookies to track the effectiveness of online advertising. This information is treated confidentially and will not be shared with anyone outside of AirAsiaRedTix.com. A cookie is a piece of data stored on the user's hard drive containing information about the user. Usage of a cookie is in no way linked to any personally identifiable information while on our site.</p>
                <p>We use tracking software to monitor customer traffic patterns and site usage to help us develop the design and layout of the site to better meet the needs of visitors to AirAsiaRedTix.com. This software does not enable us to capture any personally identifying information.</p>
                <hr>
                <h1 class="mainSecTitle">Your consent</h1>
                <p>In using the AirAsiaRedTix.com web site, you consent to the collection and use of this information by AirAsiaRedTix.com in the ways described above (which may change from time to time) unless and until you inform us to the contrary by following the instructions contained in our website.</p>
                <hr>
                <h1 class="mainSecTitle">Children Under 13 Years Old</h1>
                <p>AirAsiaRedTix.com does not collect or maintain information directly from those under 13 years old. If you are under 13 years old, please do not provide your name and address or other contact details or personal identifiable information of any kind whatsoever.</p>
                <p>Information of children under the age of 13 would only be collected to facilitate online bookings of flight or room reservation. Such information should only and would normally be provided by the parent or guardian of someone under 13 years old. In addition, only limited information of the child such as name, age, date of birth, traveling document, would be collected by AirAsiaRedTix.com.</p>
                <p>If you are a Parent or Guardian of someone under 13 years old who has provided us information without your knowledge and consent, you may request that we remove this information by contacting us at online feedback form.</p>
            </section>

          </div><!-- /Main Body -->

          <!-- Sidebar -->
          @include('layouts.partials._innersidebar')
          <!-- /Sidebar -->

        </div><!-- /row -->

      </div>
    </section><!-- /Content Section -->

@endsection