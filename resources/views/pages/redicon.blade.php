@extends('master')
@section('title')
	Redicon
@endsection

@section('header')
	@include('layouts.partials._header')
@endsection

@section('content')
	<link type="text/css" rel="stylesheet" href="/css/custom/redicon.css">
	<!-- Banner Section -->
   	<section class="innerPageBanner" style="width: 100%;">
	  	<div class="bigBanner-overlay"></div>
  		<a href="#eventThumb"><div class="jumbotron eventBanner img-responsive hidden-xs" style="background-image: url('images/redicon/web-banner.jpg')"></div></a>
   		<a href="#eventThumb">
		   	<div class="widewrapper main hidden-lg hidden-md hidden-sm">
				<img src="{{asset('images/redicon/mobile.png')}}" width="100%" class="img-responsive">
		  	</div>
  		</a>
	</section>

	<section class="pageContent">
	  <!-- Main Body -->
		<div class="mainBodyContent no-btm-mar">

			<section class="pageCategory-section last" id="eventThumb">
			  	<div class="container">
				  	<div class="row">
						<div class="col-sm-offset-1 col-sm-10 leftBar">
							<div class="text-center">
								<h1 class="mainSecTitle">HERE ARE YOUR AWESOME EVENTS:</h1>	
							</div>							
						</div>
					</div>
			  	</div>
			</section>
			
			<section class="pageCategory-section  last">
				<div class="container">
	                <div class="row">
	                    <div class="eventList-Home">

	                        <!--Ticket List-->
                            <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="">
                                <div class="eventCard">
                                    <div class="top">
                                        <a href="Fantasy_Rainforest">
                                            <div class="thumb" style="background-image: url('images/rainforest/rainforest-thumb.jpg')"></div>
                                            <div class="dateEvent concert">Daily</div>
                                            <div class="viewEvent"><span>View Event</span></div>
                                        </a>
                                    </div>
                                    <div class="content">
                                        <div class="category">Live Performance</div>
                                        <div class="titleEvent"><a href="Fantasy_Rainforest"><h2>Fantasy Rainforest</h2></a></div>
                                        <div class="location">Hall A, Putrajaya International Convention Centre</div>
                                    </div>
                                    <div class="bottom">
                                        <div class="col-sm-12 text-left bottomText">
	                                		<p>
		                                		Sorry, this event do not have AllStars exclusive discounts. But this shouldn’t stop you from having a blast at other events.
		                                	</p>
	                                	</div>
                                    </div>
                                </div>
                            </div>

                             <!--Ticket List-->
                            <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="">
                                <div class="eventCard">
                                    <div class="top">
                                        <a href="mystic_india_the_world_tour_live_in_kuala_lumpur">
                                            <div class="thumb" style="background-image: url('images/mysticindiakl/thumbnail2.jpg')"></div>
                                            <div class="dateEvent concert"> Postponed</div>
                                            <div class="viewEvent"><span>View Event</span></div>
                                        </a>
                                    </div>
                                    <div class="content">
                                        <div class="category">Music</div>
                                        <div class="titleEvent"><a href="mystic_india_the_world_tour_live_in_kuala_lumpur"><h2>Mystic India: The World Tour - Live In Kuala Lumpur 2018</h2></a></div>
                                        <div class="location">Kuala Lumpur, Malaysia</div>
                                    </div>
                                    <div class="bottom">
                                        <div class="col-sm-12 text-left bottomText">
                                            <p>
                                                Sorry, this event do not have AllStars exclusive discounts. But this shouldn’t stop you from having a blast at other events.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--Ticket List-->
                            <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="April 25 2018 00:00:00 GMT+0800">
                                <div class="eventCard">
                                    <div class="top">
                                        <a href="comedycourt20thanniversaryshow">
                                            <div class="thumb" style="background-image: url('images/comedycourt18/thumbnail1.jpg')"></div>
                                            <div class="dateEvent concert">Tue, 24 April '18</div>
                                            <div class="viewEvent"><span>View Event</span></div>
                                        </a>
                                    </div>
                                    <div class="content">
                                        <div class="category">Comedy</div>
                                        <div class="titleEvent"><a href="comedycourt20thanniversaryshow"><h2>Comedy Court - 20th Anniversary Show</h2></a></div>
                                        <div class="location">Galaxy Hall, HGH Convention Center</div>
                                    </div>
                                    <div class="bottom">
                                        <div class="col-sm-12 text-left bottomText">
                                            <p>
                                                Sorry, this event do not have AllStars exclusive discounts. But this shouldn’t stop you from having a blast at other events.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                           
                            <div id="hideMore" class="collapse">

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="May 6 2018 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="yutiangenting2018">
                                                <div class="thumb" style="background-image: url('images/yutian/thumbnail.jpg')"></div>
                                                <div class="dateEvent concert">Sat, 5 May '18</div>
                                                <div class="viewEvent"><span>View Event</span></div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">GENM</div>
                                            <div class="titleEvent"><a href="yutiangenting2018"><h2>Yu Tian 2018 Concert Live in Genting</h2></a></div>
                                            <div class="location">Arena Of Stars, Genting Highland</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="col-sm-12 text-left bottomText">
                                                <p>
                                                    Sorry, this event do not have AllStars exclusive discounts. But this shouldn’t stop you from having a blast at other events.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="May 6 2018 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="madras">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/milff2018/Madras-Thumbnail2.jpg')"></div>
                                                    <div class="dateEvent concert">Sat, 5 May '18</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">Music</div>
                                            <div class="titleEvent"><a href="madras"><h2>Madras: An 80's Musical</h2></a></div>
                                            <div class="location"> Hotel Istana Ballroom</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="col-sm-12 text-left bottomText">
                                                <p>
                                                    Sorry, this event do not have AllStars exclusive discounts. But this shouldn’t stop you from having a blast at other events.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="May 7 2018 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="unkonsciousbeachfestival2018">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/unkonsciousbeachfestival2018/thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert">  4 - 6 May '18​​​​​​</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">Festival</div>
                                            <div class="titleEvent"><a href="unkonsciousbeachfestival2018"><h2>UnKonscious Beach Festival 2018</h2></a></div>
                                            <div class="location"> Paradise Beach Club</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="col-sm-12 text-left bottomText">
                                                <p>
                                                    Sorry, this event do not have AllStars exclusive discounts. But this shouldn’t stop you from having a blast at other events.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="May 27 2018 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="adamchengworldtourmalaysia">
                                                <div class="thumb" style="background-image: url('images/adamcheng/thumbnail1.jpg')"></div>
                                                <div class="dateEvent concert">Sat, 26 May '18</div>
                                                <div class="viewEvent"><span>View Event</span></div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">GENM</div>
                                            <div class="titleEvent"><a href="adamchengworldtourmalaysia"><h2>Adam Cheng World Tour - Malaysia</h2></a></div>
                                            <div class="location">Arena Of Stars, Genting Highland</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="col-sm-12 text-left bottomText">
                                                <p>
                                                    Sorry, this event do not have AllStars exclusive discounts. But this shouldn’t stop you from having a blast at other events.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="June 25 2018 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="thesmurfsliveonstage">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/thesmurfsliveonstage/thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert"> 26 May – 24 June '18</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">Musical</div>
                                            <div class="titleEvent"><a href="thesmurfsliveonstage"><h2>The Smurfs Live on Stage</h2></a></div>
                                            <div class="location"> Genting International Showroom</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="col-sm-12 text-left bottomText">
                                                <p>
                                                    Sorry, this event do not have AllStars exclusive discounts. But this shouldn’t stop you from having a blast at other events.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="June 2 2018 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="icehousetowhitehousetour2018">
                                                <div class="thumb" style="background-image: url('images/iwhouse18/thumbnail.jpg')"></div>
                                                <div class="dateEvent concert"> Sat, 2 June '18</div>
                                                <div class="viewEvent"><span>View Event</span></div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">Comedy</div>
                                            <div class="titleEvent"><a href="icehousetowhitehousetour2018"><h2>RJ Balaji's Ice House To White House Tour Malaysia</h2></a></div>
                                            <div class="location">Plaza 51, Star Light Arena </div>
                                        </div>
                                        <div class="bottom">
                                            <div class="col-sm-12 text-left bottomText">
                                                <p>
                                                    Sorry, this event do not have AllStars exclusive discounts. But this shouldn’t stop you from having a blast at other events.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="June 10 2018 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="zhaochuanwanfanggenting">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/zcwf18/thumbnail1.jpg')"></div>
                                                    <div class="dateEvent concert"> Sat, 9 June '18</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">GENM</div>
                                            <div class="titleEvent"><a href="zhaochuanwanfanggenting"><h2>Best of Zhao Chuan & Wan Fang Live in Genting</h2></a></div>
                                            <div class="location"> Arena Of Stars, Genting Highland</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="col-sm-12 text-left bottomText">
                                                <p>
                                                    Sorry, this event do not have AllStars exclusive discounts. But this shouldn’t stop you from having a blast at other events.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="June 10 2018 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="tripletreat">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/milff2018/Triple-Trat-Hero-Thumbnail2.jpg')"></div>
                                                    <div class="dateEvent concert">Sat, 9 June '18</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">Music</div>
                                            <div class="titleEvent"><a href="tripletreat"><h2>Triple Treat - A Battle of the Bands</h2></a></div>
                                            <div class="location"> Hotel Istana Ballroom</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="col-sm-12 text-left bottomText">
                                                <p>
                                                    Sorry, this event do not have AllStars exclusive discounts. But this shouldn’t stop you from having a blast at other events.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="June 17 2018 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="ultrasingapore2018">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/herobanner/ultra-thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert"> 15 - 16 June '18</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">Festival</div>
                                            <div class="titleEvent"><a href="ultrasingapore2018"><h2>Ultra Singapore 2018</h2></a></div>
                                            <div class="location"> Ultra Park, 1 Bayfront Avenue, Singapore</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="col-sm-12 text-left bottomText">
                                                <p>
                                                    Sorry, this event do not have AllStars exclusive discounts. But this shouldn’t stop you from having a blast at other events.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="June 17 2018 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="happy70thfrancesliveingenting">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/francesyip2018/thumbnail1.jpg')"></div>
                                                    <div class="dateEvent concert"> Sat, 16 June '18</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">GENM</div>
                                            <div class="titleEvent"><a href="happy70thfrancesliveingenting"><h2>Happy 70th Frances Live in Genting</h2></a></div>
                                            <div class="location"> Arena Of Stars, Genting Highland</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="col-sm-12 text-left bottomText">
                                                <p>
                                                    Sorry, this event do not have AllStars exclusive discounts. But this shouldn’t stop you from having a blast at other events.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="June 24 2018 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="hauntfest2018">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/hauntfest2018/mobile.png')"></div>
                                                    <div class="dateEvent concert"> Sat, 23 June '18</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">Entertainment</div>
                                            <div class="titleEvent"><a href="hauntfest2018"><h2>#HAUNTFEST</h2></a></div>
                                            <div class="location"> Starxpo KWC Fashion Mall, Kuala Lumpur</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="col-sm-12 text-left bottomText">
                                                <p>
                                                    Sorry, this event do not have AllStars exclusive discounts. But this shouldn’t stop you from having a blast at other events.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="July 8 2018 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="retrorahman">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/milff2018/Retro-Rahman-Hero-Thumbnail2.jpg')"></div>
                                                    <div class="dateEvent concert">Sat, 7 July '18</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">Music</div>
                                            <div class="titleEvent"><a href="retrorahman"><h2>Rahman Retro - A Tribute</h2></a></div>
                                            <div class="location"> Hotel Istana Ballroom</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="col-sm-12 text-left bottomText">
                                                <p>
                                                    Sorry, this event do not have AllStars exclusive discounts. But this shouldn’t stop you from having a blast at other events.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="July 16 2018 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="rainforestworldmusicfestival2018">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/rainforest2018/thumbnail2.jpg')"></div>
                                                    <div class="dateEvent concert">13 - 15 July '18</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">Music</div>
                                            <div class="titleEvent"><a href="rainforestworldmusicfestival2018"><h2>Rainforest World Music Festival 2018</h2></a></div>
                                            <div class="location"> Sarawak Cultural Village</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="col-sm-12 text-left bottomText">
                                                <p>
                                                    Sorry, this event do not have AllStars exclusive discounts. But this shouldn’t stop you from having a blast at other events.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="July 14 2018 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="concertradjamalaikatcinta">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/radja2018/thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert">Sat, 14 July '18</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">Concert</div>
                                            <div class="titleEvent"><a href="concertradjamalaikatcinta"><h2>Concert Radja Malaikat Cinta</h2></a></div>
                                            <div class="location"> StarXpo Centre KWC Fashion Mall Kuala Lumpur</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="col-sm-12 text-left bottomText">
                                                <p>
                                                    Sorry, this event do not have AllStars exclusive discounts. But this shouldn’t stop you from having a blast at other events.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="August 5 2018 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="spbliveinkualalumpurconcert2018">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/spbliveinkl18/mobile-banner-spb.jpg')"></div>
                                                    <div class="dateEvent concert"> Sat, 4 August '18</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">Musical</div>
                                            <div class="titleEvent"><a href="spbliveinkualalumpurconcert2018"><h2>SPB Live in Kuala Lumpur Concert 2018</h2></a></div>
                                            <div class="location"> KLCC Plenary Hall</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="col-sm-12 text-left bottomText">
                                                <p>
                                                    Sorry, this event do not have AllStars exclusive discounts. But this shouldn’t stop you from having a blast at other events.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Sept 9 2018 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="milff2018">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/milff2018/Mainstage-thumbnail2.jpg')"></div>
                                                    <div class="dateEvent concert">Sat, 8 Sept '18</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">Music</div>
                                            <div class="titleEvent"><a href="milff2018"><h2>The Mainstage - #MILFF2018</h2></a></div>
                                            <div class="location"> Bukit Kiara Equestrian & Country</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="col-sm-12 text-left bottomText">
                                                <p>
                                                    Sorry, this event do not have AllStars exclusive discounts. But this shouldn’t stop you from having a blast at other events.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Sept 8 2018 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="milffconcert2018">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/milff2018/Mojo-mobile.jpg')"></div>
                                                    <div class="dateEvent concert"> 5th May, 9th June, 7th July & 8th Sep</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">Music</div>
                                            <div class="titleEvent"><a href="milffconcert2018"><h2>Mojo Concert Series 2018</h2></a></div>
                                            <div class="location"> Hotel Istana Ballroom & Bukit Kiara</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="col-sm-12 text-left bottomText">
                                                <p>
                                                    Sorry, this event do not have AllStars exclusive discounts. But this shouldn’t stop you from having a blast at other events.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Nov 5 2018 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="motogp2018">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/motogp2018/thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert"> 2 - 4 Nov '18</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>  
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">Motorsport</div>
                                            <div class="titleEvent"><a href="motogp2018"><h2>Shell Malaysia Motorcycle Grand Prix 2018</h2></a></div>
                                            <div class="location"> Sepang International Circuit</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="col-sm-12 text-left bottomText">
                                                <p>
                                                    Sorry, this event do not have AllStars exclusive discounts. But this shouldn’t stop you from having a blast at other events.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            
                            <div class="col-sm-12 pull-right col-xs-12"><span class="seeAllBtn pull-right"><a id="seeAll" data-toggle="collapse" data-target="#hideMore" style="cursor: pointer;">See More <i class="fa fa-angle-right" aria-hidden="true"></i></a></span></div>
	                    </div>
	                </div>
                </div>
             </section>

			<section class="pageCategory-section section-white last">
			  	<div class="container">
				  	<div class="row">
						<div class="col-sm-offset-1 col-sm-10 leftBar">
							<div class="text-center">
								<h1 class="mainSecTitle howTitle">HERE’S HOW TO USE YOUR PROMO CODE</h1>	
							</div>

							<div class="col-sm-12 stepCol">
	                        	<div class="col-sm-2 col-xs-12">
	                        		<div class="circle center-block">1</div>
	                        	</div>
	                        	<div class="col-sm-10">
	                        		<div class="col-sm-10 col-xs-12 stepDescription">
	                        			Logon to www.airasiaredtix.com and click on the event page you want.<br>
										Pro tip: Click <strong>“Get Tickets”</strong> to go straight to ticketing details
	                        		</div>
	                        		<div class="col-sm-5 hidden-xs">
	                        			<img class="img-responsive" src="images/redicon/step1.png">
	                        		</div>
	                        		<div class="col-sm-5 col-xs-12">
	                        			<img class="img-responsive" src="images/redicon/step1-1.png">	
	                        		</div>
	                        	</div>
	                        </div>

	                        <div class="col-sm-12 stepCol">
	                        	<div class="col-sm-2 col-xs-12">
	                        		<div class="circle center-block">2</div>
	                        	</div>
	                        	<div class="col-sm-10">
	                        		<div class="col-sm-10 col-xs-12 stepDescription">
	                        			Decide on which category you would like to choose. Once you have<br> made your mind up, click <strong>“BUY TICKETS”</strong>
	                        		</div>
	                        		<div class="col-sm-10 col-xs-12">
	                        			<img class="img-responsive" src="images/redicon/Step-2.png">
	                        		</div>
	                        	</div>
	                        </div>

	                        <div class="col-sm-12 stepCol">
	                        	<div class="col-sm-2 col-xs-12">
	                        		<div class="circle center-block">3</div>
	                        	</div>
	                        	<div class="col-sm-10">
	                        		<div class="col-sm-10 col-xs-12 stepDescription">
	                        			Next, decide on the amount of tickets you would like to purchase. Once done, click <strong>“Add to basket”</strong>
	                        		</div>
	                        		<div class="col-sm-10 col-xs-12">
	                        			<img class="img-responsive" src="images/redicon/Step-3.png">
	                        		</div>
	                        	</div>
	                        </div>

	                        <div class="col-sm-12 stepCol">
	                        	<div class="col-sm-2 col-xs-12">
	                        		<div class="circle center-block">4</div>
	                        	</div>
	                        	<div class="col-sm-10">
	                        		<div class="col-sm-10 col-xs-12 stepDescription">
	                        			This page will be a summary of your purchase. Please ensure all the details listed is correct before moving forward. Once everything is in order, click <strong>“Proceed to Checkout”</strong>
	                        		</div>
	                        		<div class="col-sm-10 col-xs-12">
	                        			<img class="img-responsive" src="images/redicon/Step-4.png">
	                        		</div>
	                        	</div>
	                        </div>

	                        <div class="col-sm-12 stepCol">
	                        	<div class="col-sm-2 col-xs-12">
	                        		<div class="circle center-block">5</div>
	                        	</div>
	                        	<div class="col-sm-10">
	                        		<div class="col-sm-10 col-xs-12 stepDescription">
	                        			On this page, fill all the information that is required. No login required. You can purchase as a guest
	                        		</div>
	                        		<div class="col-sm-10 col-xs-12">
	                        			<img class="img-responsive" src="images/redicon/Step-5.png">
	                        		</div>
	                        	</div>
	                        </div>

	                         <div class="col-sm-12 stepCol">
	                        	<div class="col-sm-2 col-xs-12">
	                        		<div class="circle center-block">6</div>
	                        	</div>
	                        	<div class="col-sm-10">
	                        		<div class="col-sm-10 col-xs-12 stepDescription">
		                        		<ul>
		                        			<li>Click the “Use Coupons/ Promo Codes & Voucher” check box</li>
											<li>Click “Add Coupon/ Promo Code”</li>
											<li>Under “Coupon/ Promo Code Name”, select your event.</li>
											<li>Key in the given promo code in “Coupon/ Promo Code ID” & click “Add to Basket”.</li>
		                        		</ul>
	                        		</div>
	                        		<div class="col-sm-10 col-xs-12">
	                        			<img class="img-responsive" src="images/redicon/Step-6.png">
	                        		</div>
	                        	</div>
	                        </div>

	                        <div class="col-sm-12 stepCol">
	                        	<div class="col-sm-2 col-xs-12">
	                        		<div class="circle center-block">7</div>
	                        	</div>
	                        	<div class="col-sm-10">
	                        		<div class="col-sm-10 col-xs-12 stepDescription">Your final amount will be shown here</div>
	                        		<div class="col-sm-10 col-xs-12">
	                        			<img class="img-responsive" src="images/redicon/Step-7.png">
	                        		</div>
	                        	</div>
	                        </div>

						</div>

					</div>
					<div class="col-sm-12 btnRed">
						<a class="btn btn-danger btn-lg getTix-btn btn-block center-block" href="/" role="button" style="width: 200px;">Get Tickets Now</a>
					</div>
			  	</div>
			</section>

		</div>

	</section>

@endsection

@section('customjs')

	<script type="text/javascript">
	//Initialize Swiper
	var swiper = new Swiper('.swiper-container', {
		pagination: '.swiper-pagination',        
		paginationClickable: true,
		slidesPerView: 'auto',
		spaceBetween: 10,
		nextButton: '.swiper-button-next',
		prevButton: '.swiper-button-prev',
		freeMode: true
	});

	// Enlarge Seat Plan Image
	$(function() {
		$('.seatPlanImg').on('click', function() {
		$('.enlargeImageModalSource').attr('src', $(this).attr('src'));
		$('#enlargeImageModal').modal('show');
		});
	});
	// Hide top Banner when page scroll
	var header = $('.eventBanner');
	var range = 450;

	$(window).on('scroll', function () {
		
		var scrollTop = $(this).scrollTop();
		var offset = header.offset().top;
		var height = header.outerHeight();
		offset = offset + height;
		var calc = 1 - (scrollTop - offset + range) / range;

		header.css({ 'opacity': calc });

		if ( calc > '1' ) {
		header.css({ 'opacity': 1 });
		} else if ( calc < '0' ) {
		header.css({ 'opacity': 0 });
		}
	});

	// Smooth scroll for acnhor links
	$('a[href*="#"]:not([href="#"])').click(function() {
		if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
		var target = $(this.hash);
		target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
		if (target.length) {
			$('html, body').animate({
			scrollTop: target.offset().top
			}, 1000);
			return false;
		}
		}
	});

	$('#seeAll').click(function() {
	    if($('#hideMore').is(':visible')){
	          jQuery(this).text('See More ').append('<i class="fa fa-angle-right" aria-hidden="true"></i>');
	    }else{
	          jQuery(this).text('See Less ').append('<i class="fa fa-angle-right" aria-hidden="true"></i>');
	    }
	    $('#hideMore').slideToggle('fast');
	    return false;
	});

	</script>

    {{-- Hide expired event --}}
    <script type="text/javascript">
        $(function() {
            $('div[id^=hideExpired]').each(function() {
                var date = new Date();
                var enddate = $(this).attr('datetime'); 
                if ( Date.parse(date) >= Date.parse(enddate)) {
                  $(this).remove();
                } 
            });
        });
    </script>

@endsection