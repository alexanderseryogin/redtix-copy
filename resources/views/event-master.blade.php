<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
    
  <meta property="og:site_name" content="AirAsiaRedTix" />
  <meta property="og:title" content="AirAsiaRedTix - The Passport to Entertainment" />
  <meta property="og:description" content="AirAsiaRedTix.com is the hottest, smartest new way to book tickets to an international line-up of concerts, sporting events, musicals, theatre performances and more. A subsidiary of Asia’s largest low-cost carrier, AirAsiaRedTix.com is The Passport to Entertainment." />
  <meta property="og:image" content="https://airasiaredtix.com/images/assets/fb-aart.jpg" />
  <meta property="og:type" content="website" />
  <meta property="og:url" content="https://airasiaredtix.com" />

  <title>RedTix - @yield('title')</title>
  <link rel="shortcut icon" href="images/redtix_logo.png">
  
  <!-- Bootstrap -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="/css/custom/registraion-page.css">

  <script type="text/javascript" src="/js/jquery.qrcode.min.js"></script>
  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
</head>
<body>

@yield('header')

@yield('content')

</body>
</html>