<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>RedTix - @yield('title')</title>
  <link rel="shortcut icon" href="{{ asset('images/redtix_logo.png') }}">
  <!-- Bootstrap -->
  <link rel="stylesheet" href="/css/app.min.css">
  <link rel="stylesheet" href="/css/appcustom.css">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <link href="https://cdn.rawgit.com/noelboss/featherlight/1.7.7/release/featherlight.min.css" type="text/css" rel="stylesheet" />
  <link href="https://cdn.rawgit.com/noelboss/featherlight/1.7.7/release/featherlight.gallery.min.css" type="text/css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans" />
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300" rel="stylesheet">

  @if (App::environment('production'))
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
              new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
              j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
              'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
      })(window,document,'script','dataLayer','GTM-W7WS5LM');</script>
    <!-- End Google Tag Manager -->

    <!-- Google Tag Manager Ultra -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
              new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
              j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
              'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-N687ZV8');</script>
    <!-- End Google Tag Manager Ultra -->

    <!-- Site tag (gtag.js) - Google Analytics Amobee -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-93534697-1"></script>
    <!-- End Site tag (gtag.js) - Google Analytics Amobee -->

    <!-- Facebook Pixel Code Amobee -->
    <script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window,document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '225142928059267');
    fbq('track', 'PageView');
    </script>
    <!-- End Facebook Pixel Code Amobee -->

    <!-- Turn Pixel for remarketing Amobee -->
    <!-- Do Not Remove - Turn Tracking Beacon Code - Do Not Remove -->
    <!-- Advertiser Name : MMSEA - Ultra Singapore -->
    <!-- Beacon Name : Ultra Singapore_Landing Page -->
    <!-- If Beacon is placed on a Transaction or Lead Generation based page, please populate the turn_client_track_id with your order/confirmation ID -->
    <script type="text/javascript"> turn_client_track_id = ""; </script>
    <script type="text/javascript" src="https://r.turn.com/server/beacon_call.js?b2=A4HfCQL1ROvcyUUpA0-g3DPaXbB2spC09hf-jD3jfsZZt69Qsf3PuM8FKxY2idp3bYsZtEioS4i9oyqGhO1uOg"> </script>
    <noscript> <img border="0" src="https://r.turn.com/r/beacon?b2=A4HfCQL1ROvcyUUpA0-g3DPaXbB2spC09hf-jD3jfsZZt69Qsf3PuM8FKxY2idp3bYsZtEioS4i9oyqGhO1uOg&cid="> </noscript>
    <!-- End Turn Tracking Beacon Code Do Not Remove -->
    <!-- End Turn Pixel for remarketing Amobee -->

    <!-- Singapore Tourism Board Global site tag (gtag.js) - DoubleClick -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=DC-6953330"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'DC-6953330');
    </script>

    <!-- Event snippet for VaynerMedia tracker - Adwords Conversion -->
    <script>
    function gtag_report_conversion(url) {
    var callback = function () {
        if (typeof(url) != 'undefined') {
        window.location = url;
        }
    };
    gtag('event', 'conversion', {
        'send_to': 'AW-801130855/jOecCOub9YMBEOeSgf4C',
        'value': 1.0,
        'currency': 'USD',
        'transaction_id': '',
        'event_callback': callback
    });
    return false;
    }
    </script>

    <!-- Start Heap Analytics -->
    <script type="text/javascript">
      window.heap=window.heap||[],heap.load=function(e,t){window.heap.appid=e,window.heap.config=t=t||{};var r=t.forceSSL||"https:"===document.location.protocol,a=document.createElement("script");a.type="text/javascript",a.async=!0,a.src=(r?"https:":"http:")+"//cdn.heapanalytics.com/js/heap-"+e+".js";var n=document.getElementsByTagName("script")[0];n.parentNode.insertBefore(a,n);for(var o=function(e){return function(){heap.push([e].concat(Array.prototype.slice.call(arguments,0)))}},p=["addEventProperties","addUserProperties","clearEventProperties","identify","removeEventProperty","setEventProperties","track","unsetEventProperty"],c=0;c<p.length;c++)heap[p[c]]=o(p[c])};
      heap.load("2662097889");
    </script>
    <!-- End Heap Analytics -->
@endif

</head>
<body>

@if (App::environment('production'))
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W7WS5LM" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-N687ZV8" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    
    <!-- Site tag (gtag.js) - Google Analytics Amobee -->
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      gtag('config', 'UA-93534697-1');
    </script>

    <!-- Facebook Pixel Code Amobee -->
    <noscript><img height="1" width="1" src="https://www.facebook.com/tr?id=225142928059267&ev=PageView&noscript=1"/></noscript>
    <!-- End Facebook Pixel Code -->

    <!-- Google Code for Remarketing Tag Amobee -->
    <script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 858078160;
    var google_custom_params = window.google_tag_params;
    var google_remarketing_only = true;
    /* ]]> */
    </script>
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
    </script>
    <noscript>
    <div style="display:inline;">
    <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/858078160/?guid=ON&amp;script=0"/>
    </div>
    </noscript>

    <!-- Google Analytics Cross Domain Tracking Amobee -->
    <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-93534697-1', 'auto', {allowLinker: true});


    // Loads the Linker plugin
    ga('require', 'linker');
    // Set domains for source and destination
    ga('linker:autoLink', ['airasiaredtix.com', 'redtix-tickets.airasia.com']);

    ga(function(tracker) {
      var linkerParam = tracker.get('linkerParam');
    });

    </script>
    <!-- End Google Analytics Cross Domain Tracking Amobee -->

    <!-- Singapore Tourism Board Global site tag - DoubleClick -->
    <script>
    var userAgentString = navigator.userAgent
      gtag('event', 'conversion', {
        'allow_custom_scripts': true,
        'u1': 'Ultra Singapore 2018',
        'u2': 'https://airasiaredtix.com/ultrasingapore2018',
        'u3': userAgentString,
        'u4': 'Passion Tribe',
        'u5': 'English',
        'u6': 'MICE cluster',
        'u7': 'Festival',
        'send_to': 'DC-6953330/im/frpag00s+standard'
      });
    </script>
    <noscript>
    <img src="https://ad.doubleclick.net/ddm/activity/src=6953330;type=im;cat=frpag00s;u1=Ultra%20Singapore%202018;u2=https://airasiaredtix.com/ultrasingapore2018;u3="+userAgentString+";u4=Passion%20Tribe;u5=English;u6=MICE%20cluster;u7=Festival;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;ord=1?" width="1" height="1" alt=""/>
    </noscript>
    <!-- End Singapore Tourism Board Global site tag - DoubleClick -->
      
@endif

@yield('header')

@yield('content')

@yield('modal')

@include('layouts.partials._newsletterModal')

@include('layouts.partials._footer')

@yield('customjs')

</body>
</html>