@extends('errorpage')
@section('title')
    404
@endsection

@section('header')
    @include('layouts.partials._header')
@endsection

@section('content')

    <!-- Content Section -->
    <section class="missingPage">
        <div class="container text-center">
            <img class="img-responsive center-block" src="{!! asset('images/error404.png') !!}" alt="">
            <h1><span class="text-danger">404</span> - Page Not Found</h1>
            <p>For Some Reason The Page You Requested Could Not Be Found On Our Server</p>
            <div class="clearfix">&nbsp;</div>
            <a class="btn btn-lg btn-black" href="/"><i class="fa fa-angle-left" aria-hidden="true"></i> Go Home</a>
        </div>
    </section><!-- /Content Section -->

@endsection