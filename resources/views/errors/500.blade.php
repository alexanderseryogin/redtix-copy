@extends('errorpage')
@section('title')
    500
@endsection

@section('header')
    @include('layouts.partials._header')
@endsection

@section('content')

    <!-- Content Section -->
    <section class="missingPage">
        <div class="container text-center">
            <img class="img-responsive center-block" src="images/error500.png" alt="">
            <h1><span class="text-danger">500</span> - Internal Server Error</h1>
            <p>Error, Something Went Wrong. Please Try Again Later Or</p>
            <div class="clearfix">&nbsp;</div>
            <a class="btn btn-lg btn-black" href="mailto:support@airasiaredtix.com"><i class="fa fa-envelope-o" aria-hidden="true"></i> Let Us Know</a>
        </div>
    </section><!-- /Content Section -->

@endsection