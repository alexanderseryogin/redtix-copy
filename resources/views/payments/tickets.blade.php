{{-- route(@extends('master') --}}
@extends('master-event-registration')
@section('title')
    Tickets
@endsection

@section('header')
<!-- Header & Navigation -->
<header class="mainHeader">
    <div class="container-fluid">
    <nav class="navbar navbar-expand-lg" role="navigation">
        <div class="navbar-header">
            <!-- TODO -->
            <!-- <button type="button" id="search" style="display: none;">
                <span><i class="fa fa-search pull-right"></i></span>
            </button> -->
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-01">
                <span class="sr-only">Toggle navigation</span>
            </button>
            <a class="navbar-brand" href="/"><img src="{!! asset('images/redtix_logo.png') !!}" alt="no nav img"></a>
        </div>
        <div class="collapse navbar-collapse" id="navbar-collapse-01">
            <ul class="nav navbar-nav pull-right">
                <li><a href="/about">About Us</a></li>
                <li><a href="/faq">FAQ</a></li>
                <li><a class="newsletter-nav" data-toggle="modal" data-target="#modalSubscribeNewsletter">Newsletter</a></li>
                <li><a href="mailto:support@airasiaredtix.com">Contact Us</a></li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </nav><!-- /navbar -->
    </div>
</header>
@endsection

@section('content')

                    <div class="embed-responsive embed-responsive-1by1" style="margin-top: 40px; margin-bottom: 40px;">
                        <iframe src="https://redtix.cognitix.id/aart/kamasiwashington/booking?type=widget" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                    </div>

@endsection

