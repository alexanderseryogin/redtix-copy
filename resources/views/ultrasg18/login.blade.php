@extends('ultrasg18.default')
@section('title','Login')

@section('content')
    <section class="login-page">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="login-box">
                    <div class="row">
                        <div class="col-md-5 col-sm-5 ">
                            <div class="login-box-img">
                            </div>
                        </div>
                        <div class="col-md-7 col-sm-7 ">
                            <div class="login-form ">
                                <form method="post" id="loginForm" action="{{ route('ultrasingapore.login') }}">
                                    {{csrf_field()}}
                                    <div class="form-group">
                                        <label class="custom-lable" for="email">Email</label>
                                        <input type="email" name='email' class="form-control custom-input" placeholder="name@youremail.com" required value="{{ old('email') }}" required autofocus>
                                    </div>
                                    <div class="form-group">
                                        <label class="custom-lable" for="pwd" >Transaction Number-TN (XXXXXX)</label>
                                        <input type="text" class="form-control custom-input" name="transaction_number" placeholder="123456" required value="{{ old('transaction_number') }}" required autofocus>
                                    </div>
                                    <div class="form-group login-error">
                                        @if($errors)
                                            {{--$errors->first()--}}
                                        @endif
                                    </div>
                                    <div class="sub-button">
                                        <button type="submit" class="btn-form">Submit</button>
                                        <p>Please provide the e-ticket number and your email used in your ticket purchase</p>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </section>

    @component('ultrasg18.modalcomponent', ['modalId' => 'loginFailed'])
     <p>We're unable to find your ticket purchase information. Please ensure the email and passport/nric information is the same used in your ticket purchase</p>
     <p>&nbsp;</p>
     <div class="text-center">
         <button id="close" class="btn-modal close-modal-logout" >DONE</button>
     </div>
    @endcomponent
@endsection

@section('js')
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.min.js"></script>
    <script>
        $.validator.addMethod("customemail", 
            function(value, element) {
                return /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i.test(value);
            }, 
            "Please enter a valid email address."
        );
        $("#loginForm").validate({
            errorClass: 'error-message',
            rules: {
                email: {
                    customemail: true
                }
            }
        });
        @if ($errors->first())
            $('#loginFailed').modal({backdrop: 'static', keyboard: false});
        @endif
    </script>
@endsection