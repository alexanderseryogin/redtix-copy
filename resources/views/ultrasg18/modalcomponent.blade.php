<div class="modal fade" id="{{ $modalId }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog modal-lg home-modal-dialog" role="document">
       <div class="modal-content">
           <div class="container-fluid">
               <div class="row">
                   <div class="col-sm-4 px-0">
                       <div class="model-img"></div>
                   </div>
                   <div class="col-sm-8">
                       <div class="thanku">
                           {{ $slot }}
                       </div>
                   </div>
               </div>
           </div>
       </div>
   </div>
</div>