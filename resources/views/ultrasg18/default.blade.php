<!DOCTYPE html>
<html>
<head>
    <title>@yield('title','Nazami')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://airasiaredtix.com/css/custom/ultrapassengerbooking2018bootstrap.min.css" >
    <link rel="stylesheet" type="text/css" href="https://airasiaredtix.com/css/custom/ultrapassengerbooking2018style.css"><!-- 
    <link rel="stylesheet" type="text/css" href="{{asset('css/app.css')}}"> -->

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://airasiaredtix.com/js/bootstrap.bundle.min.js"></script>
</head>
<body>

   @yield('content')

   @component('ultrasg18.modalcomponent', ['modalId' => 'thanks'])
     <p>Thank you for the details.</p>
     <p>We will send you your flight e-Tickets within 2 business days</p>
     <div class="text-center">
         <button class="btn-modal close-modal-logout">DONE</button>
     </div>
   @endcomponent

   <script>
       $(document).ready(function(){
           $('.close-modal-logout').on('click',function(){
               window.location = '{{route('ultrasingapore.logout')}}';
           });
       });
   </script>
   <script src="https://airasiaredtix.com/js/app.js"></script>

   @yield('js')

</body>
</html>