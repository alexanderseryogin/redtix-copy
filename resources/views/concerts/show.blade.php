@extends('master')
@section('title')
    {{ $concert[0]->title }}
@endsection

@section('header')
    @include('layouts.partials._header')
@endsection

@section('content')

    <!-- Banner Section -->
    <section class="innerPageBanner">
      <div class="bigBanner-overlay"></div>
      <div class="jumbotron eventBanner" style="background-image: url('{!! asset('images/gnr/gnr-banner.jpg') !!}')"></div>
    </section>

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
          <div class="row priceNbtn">
            <div class="col-sm-offset-1 col-sm-10">
              <div class="row">
                <div class="col-sm-9 leftBox">
                  <h6>{{ $concert[0]->title }}</h6>
                  Tickets from <span>RM623</span>
                </div>
                <div class="col-sm-3 text-center">
                  <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">View Price <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                </div>
              </div>
            </div>
          </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
      <div class="mainBodyContent no-btm-mar section-white">

        <!--<section class="eventDetails-Area">
          <div class="container">
            <div class="col-sm-offset-1 col-sm-10 holder">
              <div class="boxPanel">
                <div class="col-sm-4">
                  <h6><span class="fa-stack"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-usd fa-stack-1x fa-inverse"></i></span> RM200 - RM400</h6>
                </div>
                <div class="col-sm-4">
                  <h6><span class="fa-stack"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-calendar fa-stack-1x fa-inverse"></i></span> 7 - 8 Sept 2017</h6>
                </div>
                <div class="col-sm-4">
                  <h6><span class="fa-stack"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-map-marker fa-stack-1x fa-inverse"></i></span> National Stadium, KL</h6>
                  <p></p>
                </div>
              </div>
            </div>
          </div>
        </section>-->

        <section class="pageCategory-section last">
          
          <div class="container intro">
            <div class="row">
              <div class="col-sm-offset-1 col-sm-10 leftBar">
                <!--<h2>Title</h2>-->
                <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i> {{ $concert[0]->formatted_date }}</div>
                <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i> {{ $concert[0]->venue }}, {{ $concert[0]->venue_address }} <a target="_blank" href="https://goo.gl/maps/uQ4z6Zpeni42">View Map</a></div>
                <div><i class="fa fa-clock-o" aria-hidden="true"></i> {{ $concert[0]->formatted_time }}</div>
                <div class="clearfix">&nbsp;</div>
                <!-- Go to www.addthis.com/dashboard to customize your tools -->
                <div class="addthis_inline_share_toolbox"></div>
                <!-- /sharing -->
                <hr>
                <p>{!! $concert[0]->additional_information !!}</p>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>
        </section>

        <section class="pageCategory-section last section-grey">
          <div class="container">
            <div class="gallery">
              <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
              <!-- Swiper -->
              <div class="swiper-container">
                <div class="swiper-wrapper">
                  <div class="swiper-slide">
                    <iframe width="560" height="100%" src="https://www.youtube.com/embed/uImFjHf4aoE" frameborder="0" allowfullscreen></iframe>
                  </div>
                  <div class="swiper-slide">
                    <iframe width="560" height="100%" src="https://www.youtube.com/embed/bjJ7KCBykVU" frameborder="0" allowfullscreen></iframe>
                  </div>
                  <div class="swiper-slide">
                    <iframe width="560" height="100%" src="https://www.youtube.com/embed/x6gjFkdrXbo" frameborder="0" allowfullscreen></iframe>
                  </div>
                </div>
                <!-- Add Pagination -->
                <div class="swiper-pagination"></div>
                <!-- Add Arrows -->
                <div class="swiper-button-next swiper-button-white"></div>
                <div class="swiper-button-prev swiper-button-white"></div>
              </div><!-- /Swiper -->
            </div>
          </div>
        </section>

        <section class="pageCategory-section last"><a id="anchorPrice"></a>
          <div class="container tixPrice">
            <div class="row">
              <div class="col-sm-12 ">
                <div class="text-center">
                  <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                  <p>Select ticket(s)</p>
                </div>
                <div class="clearfix">&nbsp;</div>
                <div class="row">
                  <div class="col-sm-7">

                    <!--Ticket Type-->
                    <tickets :concert="{{ $concert[0]->id }}"></tickets>
                    <!--Order Summary-->


                  </div>
                  <div class="col-sm-5">
                    <!--Seat Plan-->
                    <div class="seatPlan-map">
                      <h1 class="subSecTitle"><strong>Seating Plan</strong></h1>
                      <img class="img-responsive seatPlanImg" src="{!! asset('images/gnr/GNRMap.jpg') !!}" alt="">
                      <small>Click image to enlarge</small>
                    </div>

                    <!--Flight Promo Note-->
                    <div class="flightpromo-note">
                      <h1 class="subSecTitle"><strong>Important Note</strong></h1>
                      <p>Before purchasing any tickets that come with the AirAsia promo code, please read the following:</p>
                      <ul>
                        <li>20% flat discount via AirAsia promo code valid only for all direct flights into Singapore</li>
                        <li>Flight booking period will be from <strong>13-24 Feb, 2017</strong></li>
                        <li>Travel period will be from <strong>13-28 Feb, 2017</strong></li>
                        <li>Flights are subject to availability on AirAsia.com</li>
                      </ul>
                      <a target="_blank" class="btn btn-black btn-lg getTix-btn" href="https://booking.airasia.com/" role="button">Check flights on AirAsia.com</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

      </div><!-- /Main Body -->
    </section>

@endsection

@section('customjs')

    <script>    
    // Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    $(function() {

        // Enlarge Seat Plan Image
        $('.seatPlanImg').on('click', function() {
          $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
          $('#enlargeImageModal').modal('show');
        });

        // Plus Minus Button for Ticket Quantity
        $(".button").on("click", function() {
        var $button = $(this);
        var oldValue = $button.parent().find("input").val();

        if ($button.text() == "+") {
          var newVal = parseFloat(oldValue) + 1;
        } else {
        // Don't allow decrementing below zero
          if (oldValue > 0) {
            var newVal = parseFloat(oldValue) - 1;
          } else {
            newVal = 0;
          }
        }

        $button.parent().find("input").val(newVal);
      });
      
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 450;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });
    </script>
    
@endsection

@section('modal')

    <!--Modal Seat Plan image-->
    <div class="modal fade" id="enlargeImageModal" tabindex="-1" role="dialog" aria-labelledby="enlargeImageModal" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
            <div class="clearfix">&nbsp;</div>
            <img src="" class="enlargeImageModalSource" style="width: 100%;">
            </div>
        </div>
        </div>
    </div>

    <!-- Modal Where to Get Tix Location-->
    <div id="modalGetTixLoc" class="modal fade" role="dialog">
        <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
                <h2>GET YOUR TICKETS FROM:</h2>
                <dl class="dl-horizontal">
                    <dt>Online:</dt>
                    <dd><a href="www.AirAsiaRedtix.com">www.AirAsiaRedtix.com</a></dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>Rock Corner outlets:</dt>
                    <dd>The Gardens, Mid Valley (TEL: 03 - 2201 4893)</dd>
                    <dd>Subang Parade (TEL: 03 - 5613 1139)</dd>
                    <dd>The Curve (TEL: 03 - 7733 1139)</dd>
                    <dd>Bangsar Village (TEL: 03-22021139)</dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>Victoria Music outlets:</dt>
                    <dd>Sg Wang(TEL: 03 - 2148 7208)</dd>
                    <dd>Amcorp (TEL: 03 - 7956 0592)</dd>
                    <dd>Tropicana (TEL: 03 - 7722 2955)</dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>Penang outlets:</dt>
                    <dd>Artist Gallery, Gurney Plaza (04-637 0191)</dd>
                    <dd>Artist Gallery, Queensbay Mall (04-228 5648)</dd>
                </dl>
            </div>
        </div>
        </div>
    </div>

@endsection