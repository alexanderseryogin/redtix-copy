@extends('master')

@section('title')
    Rugby World Cup 2019
@endsection

@section('header')
    @include('rugby2019.header')
@endsection

@section('top-section')
    <section class="package-header builder" style="background-image: url(/images/rugby2019/packages-detail/{{$packageInfo['name']}}.jpg);">
        <div class="package-header-mobile" style="background-image: url(/images/rugby2019/packages-detail/{{$packageInfo['name']}}-m.jpg);">
            <div class="d-flex flex-wrap justify-content-center align-items-center title">
                {{$packageInfo['title']}} 
            </div>
        </div>
        <div class="d-flex w-100 flex-wrap">
            <div class="agent-logo">
                <img src="/images/rugby2019/rwc-logo.png" alt="Rugby World Cup Japan 2019 - Official travel agent">
            </div>
            <div class="d-flex flex-column align-items-center info">
                <div class="event-date">
                    {{ Carbon\Carbon::parse($packageInfo['dt_start'])->format('d F')}} - {{ Carbon\Carbon::parse($packageInfo['dt_end'])->format('d F')}}  
                    {{ Carbon\Carbon::parse($packageInfo['dt_start'])->format('Y')}}
                </div>
                <div class="copyright">
                    <img src="/images/rugby2019/rwc-logo-small.png" alt="Rugby World Cup Japan 2019 - Official travel agent">
                    <span>TM © Rugby World Cup Limited 2015. All rights reserved.</span>                
                </div>
            </div>
        </div>
    </section> 
@endsection

@section('content')
    <package-builder :info="{{json_encode($packageInfo)}}"></package-builder>
@endsection