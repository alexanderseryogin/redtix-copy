<style>
    @import url('https://fonts.googleapis.com/css?family=Montserrat');

    body {
        padding: 10px 100px 0 100px;
        color: #6e6e6e;
        font-family: 'Montserrat', 'sans-serif';

    }

    h4, h5, h6 {
        font-weight: lighter;
        color: #777777;
        margin: 5px;
    }

    span {
        font-size: 18px;
    }

    .title-icon.down {
        width: 18px;
        height: 12px;
        background-image: url(/images/rugby2019/icons/airplane-down.png);
    }

    hr {
        margin-top: 30px;
    }

    .line {
        margin-top: 12px;
        font-weight: lighter;
        color: #777777;
    }

    .departure-day {
        font-size: 18px;
        font-weight: 600;
        margin-top: 15px;
        margin-bottom: 15px;
    }

    .after-departure-day {
        margin-top: 10px;
        margin-bottom: 10px;
        font-size: 12px;
    }

    .description-bottom {
        margin-top: 20px;
        font-size: 12px;
        line-height: 20px;
    }

</style>


<?php $i = 1 ?>
@foreach($packageDetailByPack['flight'] as $d)
    <!--    --><?php //echo '<pre style="font-size:10px">'; print_r($d); echo '</pre>' ?>
    <div style="font-family: 'Montserrat', 'sans-serif'; width: 600px">
        <div style="font-size: 24px; font-weight: 600;">Opening Ceremony | Package {{ $p = $packageInfo['name'] }}</div>
        <div style="font-size: 18px; margin: 40px 0 10px 0">Day {{ $i }} | Thursday 19th September 2019</div>
        @if ($i == 1)
            <div class="departure-day" style="font-size: 18px; margin: 20px 0 10px 0">Departure Day</div>
        @elseif($i == 2 || $i == 3)
            <div class="departure-day" style="font-size: 18px; margin: 20px 0 10px 0">Match Day {{ $i - 1 }}}</div>
        @elseif($i > 3)
            <div class="departure-day" style="font-size: 18px; margin: 20px 0 10px 0">Match Day {{ $i - 1 }}}</div>
        @endif
        <h4 class="after-departure-day">
            The journey begins. Time to check-in for your international flight to
            Japan.
        </h4>
        <div class="description-bottom">{{ $airport['description'] }}</div>
        <div style="margin-top: 20px">
            <h4 class="line" style="">{{ $d['city_origin']['airport'] }} to {{ $d['city_transit']['airport'] }}
                ({{ $d['city_transit']['abbr'] }})</h4>
        </div>
        @if($i == 1 || $i == 4)
            <div style="margin-top: 20px">
                <div class="line">
                    <img src="images/rugby2019/icons/airplane-up.png">&nbsp;&nbsp;
                    <span class="title-small" style="font-size:12px; font-weight: bold;">
                    Departs:
                </span>
                    {{--                <span style="font-size: 12px;">Thursday 19th September 2019 at 11:05 AM</span>--}}
                    <span style="font-size: 12px;">{{ date('H:i, l, F jS, Y', strtotime($d['date_origin_1'])) }}</span>
                </div>
                <div class="line" style="font-size: 12px;">Flight
                    Time: {{ gmdate("H:i", strtotime($d['date_origin_2']) - strtotime($d['date_origin_1'])) }} h
                </div>
                <div class="line">
                    <img src="images/rugby2019/icons/airplane-down.png"/>&nbsp;&nbsp;
                    <span class="title-small" style="font-size:12px; font-weight: bold;">
                    Arrives:
                </span>
                    {{--                <span style="font-size: 12px;">Thursday 19th September 2019 at 19:25 PM</span>--}}
                    <span style="font-size: 12px;">{{ date('H:i, l, F jS, Y', strtotime($d['date_origin_2'])) }}</span>
                </div>
            </div>
        @endif


        <div class="description-bottom" style="">
            Tokyo is the capital of Japan and is the world’s largest metropolitan center
            but behind the ‘concrete jungle’, busy streets and skyscrapers is a cultural city
            with enough activities to thrill its visitors.
        </div>
        <br>

        <div style="line-height: 20px">
            <div style="font-weight: bolder; margin-bottom: 20px;">Airport Transfer</div>
            <div style="margin-top: 5px; font-size: 12px;">Once you reach the hotel, you’ll be checked in for your
                three-night stay
            </div>

            <div style="margin-top: 5px; font-size: 12px;">Rest up - you have a busy Friday ahead of you!</div>
            <div style="margin-top: 5px; font-size: 12px;">Once you’ve made it past immigration and leave arrivals, you
                will need to
                arrangetransportation from Narita Airport to your hotel.
            </div>
        </div>


        <hr style="border: .6px solid lightgray"/>

        <?php $i++; if ($i > 4) return; ?>

    </div>
@endforeach


