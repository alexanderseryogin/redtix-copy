<section class="section-main">
    <h1 class="d-none">
        Rugby World Cup Japan 2019
    </h1>

    <div class="d-flex w-100 flex-wrap">
        <div class="d-flex column-1 align-items-end">
            <div class="agent-logo">
                <img src="/images/rugby2019/rwc-logo.png" alt="Rugby World Cup Japan 2019 - Official travel agent">
            </div>
        </div>
        <div class="d-flex column-2 justify-content-center">
            <div class="title-img">
                <img src="/images/rugby2019/main-title.png" alt="Rugby World Cup Japan 2019">
            </div>
        </div>
        <div class="d-flex column-3 justify-content-right align-items-end">
            <div class="copyright d-flex align-items-center">
                <img src="/images/rugby2019/rwc-logo-small.png" alt="Rugby World Cup Japan 2019 - Official travel agent">
                <span>TM © Rugby World Cup Limited 2015. All rights reserved.</span>
            </div>
        </div>
    </div>
</section>
