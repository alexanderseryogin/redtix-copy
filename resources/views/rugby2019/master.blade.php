@extends('master')
@section('title')
    Rugby World Cup 2019
@endsection

@section('header')
    @include('rugby2019.header')
@endsection

@section('top-section')
    @include('rugby2019.top-section')
@endsection

@section('content')
    <section class="section-info">
        <div class="container">
            <ul class="component-info">
                <li class="info-item info-badge">
                    Rugby World Cup 2019
                </li>
                <li class="info-item info-location">
                    Japan
                </li>
                <li class="info-item info-calendar">
                    20 September - 02 November 2019
                </li>
            </ul>
            <div class="d-flex">
                <a class="btn btn-primary" href="#" role="button">
                    Sports
                </a>
                <a class="btn btn-outline-primary" href="#" role="button">
                    Rugby
                </a>
            </div>
        </div>
    </section>
    <section class="section-nav-tabs">
        <div class="container">
            <div class="js-nav-tabs-wrapper nav-tabs-wrapper">
                <ul class="nav nav-tabs js-nav-sticky js-nav-tabs">
                    <li class="js-nav-item nav-item">
                        <a class="js-nav-link nav-link nav-link2" href="#tab-event-info-and-fixtures" data-toggle="tab" role="tab">
                            EVENT INFO & FIXTURES
                        </a>
                    </li>
                    <li class="js-nav-item nav-item">
                        <a class="js-nav-link nav-link nav-link2" href="#tab-packages" data-toggle="tab" role="tab">
                            TRAVEL PACKAGES
                        </a>
                    </li>
                    <li class="js-nav-item nav-item">
                        <a class="js-nav-link nav-link nav-link2" href="#tab-history" data-toggle="tab" role="tab">
                            HISTORY
                        </a>
                    </li>
                    <li class="js-nav-item nav-item">
                        <a class="js-nav-link nav-link nav-link2" href="#tab-venue" data-toggle="tab" role="tab">
                            VENUES
                        </a>
                    </li>
                    <li class="js-nav-item nav-item">
                        <a class="js-nav-link nav-link nav-link2" href="#tab-faq" data-toggle="tab" role="tab">
                            FAQ
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </section>
    <div class="tab-content">
        <div class="tab-pane" id="tab-event-info-and-fixtures" role="tabpanel">
            @include('rugby2019.tabs.event-info')
        </div>
        <div class="tab-pane" id="tab-packages" role="tabpanel">
            @include('rugby2019.tabs.packages')
        </div>
        <div class="tab-pane" id="tab-history" role="tabpanel">
            @include('rugby2019.tabs.history')
        </div>
        <div class="tab-pane" id="tab-venue" role="tabpanel">
            @include('rugby2019.tabs.venue')
        </div>
        <div class="tab-pane" id="tab-faq" role="tabpanel">
            @include('rugby2019.tabs.faq')
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
        <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar section-white" id="rwc19">
            <section class="pageCategory-section last" >

            </section>

            <section class="pageCategory-section last" id="section-black">

            </section>
        </div><!-- /Main Body -->
    </section><!-- /Content Section -->
@endsection

@section('customjs')
    @if( Request::segment(1) === 'rwc2019' && !Request::segment(2))
        <script src="{{ asset('rwc/app.js') }}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"></script>
    @endif
@endsection

@section('modal')
@endsection