<header class="js-nav-sticky-wrapper fixed-top">
    <nav class="navbar navbar-dark bg-dark navbar-expand-lg">
        <a class="navbar-brand" href="{{route('rwc2019.index')}}">
            <img src="/images/rugby2019/logo.png" alt="RedTix logo">
        </a>
        <button class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#navbar"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbar">
            <ul class="navbar-nav ml-auto mt-3 mt-lg-0">
                <li class="nav-item">
                    <a class="nav-link text-right nav-link-top" href="/about">
                        About us
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-right nav-link-top" href="/faq">
                        FAQ
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-right nav-link-top" data-toggle="modal" data-target="#modalSubscribeNewsletter">
                        Newsletter
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-right nav-link-top" href="mailto:support@airasiaredtix.com">
                        Contact us
                    </a>
                </li>
            </ul>
        </div>
    </nav>
</header>