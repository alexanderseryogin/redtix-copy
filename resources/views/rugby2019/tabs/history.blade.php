<section class="section-history-content">
    <div class="container history-content-wrap">
        <div class="d-flex flex-column flex-lg-row align-items-center">
            <div class="page-content-wrap">
                <h2 class="title">
                    History of rugby world cup
                </h2>
                <p>
                    The first Rugby World Cup competition organized by the International Rugby Board (IRB) was held in 1987 in
                    New Zealand and Australia and was a popular and financial success. It was staged four years after a failed
                    attempt to launch a global “rebel” (that is, outside the control of the IRB) professional championship.
                    Since 1987 Rugby World Cup has been hosted by Australia, New Zealand, United Kingdom, Ireland, France,
                    South Africa, and 2019 marks the first time the tournament is to be held in Asia, and also the first
                    time that the event will take place outside the traditional heartland of the sport. By 2015, Rugby World
                    Cup is the third largest international televised sporting event reaching over 200 countries and some 4
                    billion viewers.
                </p>
                <p>
                    Rugby World Cup 2019 runs from Friday 20 September to Saturday 2 November, with games
                    played across Japan. New Zealand has won the last two Rugby World Cups, but will they make it a hat-trick in 2019?
                </p>
            </div>
            <div class="image-wrap-right">
                <img src="{{asset('images/rugby2019/rwc-cup.png')}}" alt="RWC Cup">
                <div class="image-caption text-center">TM © Rugby World Cup Limited 1986 or<br>TM © RWCL 1986</div>
            </div>
        </div>
    </div>
    <div class="container container-lg">
        <div class="row row-lg-10 justify-content-center card-list">
            <div class="col-sm-8 col-md-6 col-lg-4">
                <div class="card">
                    <div class="card-header text-center" style="background: #452761">
                        <h3 class="h4 card-title text-uppercase">Rugby world cup 1987</h3>
                        <h4 class="card-text">Host(s) | Australia & New Zealand</h4>
                    </div>
                    <div class="card-body position-relative d-flex justify-content-center align-items-center">
                        <img class="card-img-top" src="{{asset('images/rugby2019/histories-bg/1.jpg')}}" alt="background">
                        <div class="text-center content-wrap">
                            <h4 class="card-title text-uppercase">New Zealand</h4>
                            <p class="card-text text-white">Winners</p>
                        </div>
                        <img src="{{asset('images/rugby2019/flags/md/new-zealand.png')}}" alt="flag" class="flag-img">
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item d-flex justify-content-center align-items-center">
                            <div class="title">Matches</div>
                            <div class="text">32</div>
                        </li>
                        <li class="list-group-item d-flex justify-content-center align-items-center">
                            <div class="title">Attendance</div>
                            <div class="text">604,500</div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-8 col-md-6 col-lg-4">
                <div class="card">
                    <div class="card-header text-center" style="background: #2D827B">
                        <h3 class="h4 card-title text-uppercase">Rugby world cup 1991</h3>
                        <h4 class="card-text">Host(s) | Europe</h4>
                    </div>
                    <div class="card-body position-relative d-flex justify-content-center align-items-center">
                        <img class="card-img-top" src="{{asset('images/rugby2019/histories-bg/2.jpg')}}" alt="background">
                        <div class="text-center content-wrap">
                            <h4 class="card-title text-uppercase">Australia</h4>
                            <p class="card-text text-white">Winners</p>
                        </div>
                        <img src="{{asset('images/rugby2019/flags/md/australia.png')}}" alt="flag" class="flag-img">
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item d-flex justify-content-center align-items-center">
                            <div class="title">Matches</div>
                            <div class="text">32</div>
                        </li>
                        <li class="list-group-item d-flex justify-content-center align-items-center">
                            <div class="title">Attendance</div>
                            <div class="text">1,007,760</div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-8 col-md-6 col-lg-4">
                <div class="card">
                    <div class="card-header text-center" style="background: #1D6096">
                        <h3 class="h4 card-title text-uppercase">Rugby world cup 1995</h3>
                        <h4 class="card-text">Host(s) | South Africa</h4>
                    </div>
                    <div class="card-body position-relative d-flex justify-content-center align-items-center">
                        <img class="card-img-top" src="{{asset('images/rugby2019/histories-bg/3.jpg')}}" alt="background">
                        <div class="text-center content-wrap">
                            <h4 class="card-title text-uppercase">South Africa</h4>
                            <p class="card-text text-white">Winners</p>
                        </div>
                        <img src="{{asset('images/rugby2019/flags/md/south-africa.png')}}" alt="flag" class="flag-img">
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item d-flex justify-content-center align-items-center">
                            <div class="title">Matches</div>
                            <div class="text">32</div>
                        </li>
                        <li class="list-group-item d-flex justify-content-center align-items-center">
                            <div class="title">Attendance</div>
                            <div class="text">1,100,000</div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-8 col-md-6 col-lg-4">
                <div class="card">
                    <div class="card-header text-center" style="background: #F5A72B">
                        <h3 class="h4 card-title text-uppercase">Rugby world cup 1999</h3>
                        <h4 class="card-text">Host(s) | Wales</h4>
                    </div>
                    <div class="card-body position-relative d-flex justify-content-center align-items-center">
                        <img class="card-img-top" src="{{asset('images/rugby2019/histories-bg/4.jpg')}}" alt="background">
                        <div class="text-center content-wrap">
                            <h4 class="card-title text-uppercase">Australia</h4>
                            <p class="card-text text-white">Winners</p>
                        </div>
                        <img src="{{asset('images/rugby2019/flags/md/australia.png')}}" alt="flag" class="flag-img">
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item d-flex justify-content-center align-items-center">
                            <div class="title">Matches</div>
                            <div class="text">41</div>
                        </li>
                        <li class="list-group-item d-flex justify-content-center align-items-center">
                            <div class="title">Attendance</div>
                            <div class="text">1,750,000</div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-8 col-md-6 col-lg-4">
                <div class="card">
                    <div class="card-header text-center" style="background: #A91B31">
                        <h3 class="h4 card-title text-uppercase">Rugby world cup 2003</h3>
                        <h4 class="card-text">Host(s) | Australia</h4>
                    </div>
                    <div class="card-body position-relative d-flex justify-content-center align-items-center">
                        <img class="card-img-top" src="{{asset('images/rugby2019/histories-bg/5.jpg')}}" alt="background">
                        <div class="text-center content-wrap">
                            <h4 class="card-title text-uppercase">England</h4>
                            <p class="card-text text-white">Winners</p>
                        </div>
                        <img src="{{asset('images/rugby2019/flags/md/england.png')}}" alt="flag" class="flag-img">
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item d-flex justify-content-center align-items-center">
                            <div class="title">Matches</div>
                            <div class="text">48</div>
                        </li>
                        <li class="list-group-item d-flex justify-content-center align-items-center">
                            <div class="title">Attendance</div>
                            <div class="text">1,837,547</div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-8 col-md-6 col-lg-4">
                <div class="card">
                    <div class="card-header text-center" style="background: #62C5E1">
                        <h3 class="h4 card-title text-uppercase">Rugby world cup 2007</h3>
                        <h4 class="card-text">Host(s) | France</h4>
                    </div>
                    <div class="card-body position-relative d-flex justify-content-center align-items-center">
                        <img class="card-img-top" src="{{asset('images/rugby2019/histories-bg/3.jpg')}}" alt="background">
                        <div class="text-center content-wrap">
                            <h4 class="card-title text-uppercase">South Africa</h4>
                            <p class="card-text text-white">Winners</p>
                        </div>
                        <img src="{{asset('images/rugby2019/flags/md/south-africa.png')}}" alt="flag" class="flag-img">
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item d-flex justify-content-center align-items-center">
                            <div class="title">Matches</div>
                            <div class="text">48</div>
                        </li>
                        <li class="list-group-item d-flex justify-content-center align-items-center">
                            <div class="title">Attendance</div>
                            <div class="text">2,263,223</div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-8 col-md-6 col-lg-4">
                <div class="card">
                    <div class="card-header text-center" style="background: #5EBCB3">
                        <h3 class="h4 card-title text-uppercase">Rugby world cup 2011</h3>
                        <h4 class="card-text">Host(s) | New Zealand</h4>
                    </div>
                    <div class="card-body position-relative d-flex justify-content-center align-items-center">
                        <img class="card-img-top" src="{{asset('images/rugby2019/histories-bg/1.jpg')}}" alt="background">
                        <div class="text-center content-wrap">
                            <h4 class="card-title text-uppercase">New Zealand</h4>
                            <p class="card-text text-white">Winners</p>
                        </div>
                        <img src="{{asset('images/rugby2019/flags/md/new-zealand.png')}}" alt="flag" class="flag-img">
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item d-flex justify-content-center align-items-center">
                            <div class="title">Matches</div>
                            <div class="text">48</div>
                        </li>
                        <li class="list-group-item d-flex justify-content-center align-items-center">
                            <div class="title">Attendance</div>
                            <div class="text">1,477,294</div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-8 col-md-6 col-lg-4">
                <div class="card">
                    <div class="card-header text-center" style="background: #E74870">
                        <h3 class="h4 card-title text-uppercase">Rugby world cup 2011</h3>
                        <h4 class="card-text">Host(s) | England</h4>
                    </div>
                    <div class="card-body position-relative d-flex justify-content-center align-items-center">
                        <img class="card-img-top" src="{{asset('images/rugby2019/histories-bg/1.jpg')}}" alt="background">
                        <div class="text-center content-wrap">
                            <h4 class="card-title text-uppercase">New Zealand</h4>
                            <p class="card-text text-white">Winners</p>
                        </div>
                        <img src="{{asset('images/rugby2019/flags/md/new-zealand.png')}}" alt="flag" class="flag-img">
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item d-flex justify-content-center align-items-center">
                            <div class="title">Matches</div>
                            <div class="text">48</div>
                        </li>
                        <li class="list-group-item d-flex justify-content-center align-items-center">
                            <div class="title">Attendance</div>
                            <div class="text">2,477,805</div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>