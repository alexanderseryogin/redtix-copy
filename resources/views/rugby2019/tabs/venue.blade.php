<section class="section-venue">
    <div class="container">
        <h2>
            RWC 2019 Venues
        </h2>
        <p>
            Twelve venues have been confirmed to host Rugby World Cup 2019.
        </p>
        <div class="map">
            <img class="d-none d-md-inline-block" src="/images/rugby2019/map-desktop.jpg" alt="Map">
            <img class="d-md-none" src="/images/rugby2019/map-mobile.jpg" alt="Map">
        </div>
    </div>
</section>