<div class="js-tab tab" data-tab="faq">
    <section class="section-faq">
        <div class="container">
            <div class="container-sm">
                <h2>
                    Rugby World Cup 2019(TM), Japan
                </h2>
                <p>
                    Here is an overview of the frequently asked questions about our travel packages for Rugby World Cup
                    2019 in Japan. If you have any queries that you don’t see below then please get in touch via
                    <a href="{{ env('LIVE_CHAT_LINK') }}">live chat</a>
                </p>
                <div class="accordion" id="faq-accordion">
                    <div class="component-collapse">
                        <h3 class="title " data-toggle="collapse" data-target="#faq-content-1">
                            Important notes
                        </h3>
                        <div class="text-wrapper collapse in" id="faq-content-1" data-parent="#faq-accordion">
                            <div class="text">
                                <ul><li>RM 8 RedTix fee per ticket.</li><li>Strictly no replacement for missing tickets and cancellation.</li></ul>
                            </div>
                        </div>
                    </div><div class="component-collapse">
                        <h3 class="title collapsed" data-toggle="collapse" data-target="#faq-content-2">
                            Why travel with RedTix?
                        </h3>
                        <div class="text-wrapper collapse " id="faq-content-2" data-parent="#faq-accordion">
                            <div class="text">
                                <ul><li>RedTix has been appointed as an Authorised Official Travel Agent for Rugby World Cup 2019, your match tickets and flights are 100% guaranteed. RedTix is an Authorised Sub-Agent for Rugby World Cup 2019. We have been putting together great sporting and festival packages in Asia for nearly a decade. Our packages are simple, seamless and affordable ensuring a value-for-money sporting experience.</li><li>Book with RedTix for the chance to absorb in the unique atmosphere Rugby World Cup 2019 will provide and the opportunity to explore a fascinating and beautiful country.</li><li>As an Authorised Sub-Agent for Rugby World Cup 2019, your match tickets are 100% guaranteed.</li></ul>
                            </div>
                        </div>
                    </div><div class="component-collapse">
                        <h3 class="title collapsed" data-toggle="collapse" data-target="#faq-content-3">
                            How do I book?
                        </h3>
                        <div class="text-wrapper collapse " id="faq-content-3" data-parent="#faq-accordion">
                            <div class="text">
                                <ul><li>On each package page, there is a GET TICKETS tab. Click this to enter the booking process for your package. Purchasing your package will secure your package place/s subject to receipt of monies.</li><li>If you have any questions then please feel free to get in touch via <a href="{{ env('LIVE_CHAT_LINK') }}">live chat</a>.</li></ul>
                            </div>
                        </div>
                    </div><div class="component-collapse">
                        <h3 class="title collapsed" data-toggle="collapse" data-target="#faq-content-4">
                            Is there free time on package so we can do our own things?
                        </h3>
                        <div class="text-wrapper collapse " id="faq-content-4" data-parent="#faq-accordion">
                            <div class="text">
                                <ul><li>We design our packages so that guests have plenty of free time. For Japan, we have not included any sightseeing tours.</li></ul>
                            </div>
                        </div>
                    </div><div class="component-collapse">
                        <h3 class="title collapsed" data-toggle="collapse" data-target="#faq-content-5">
                            What if I wish to organise my own flights to Japan?
                        </h3>
                        <div class="text-wrapper collapse " id="faq-content-5" data-parent="#faq-accordion">
                            <div class="text">
                                <ul><li>This is no problem, please send your request to <a href='mailto:support@redtix.com'>support@redtix.com</a> and we will try our best to facilitate to your needs.</li></ul>
                            </div>
                        </div>
                    </div><div class="component-collapse">
                        <h3 class="title collapsed" data-toggle="collapse" data-target="#faq-content-6">
                            Where can I note some special requests?
                        </h3>
                        <div class="text-wrapper collapse " id="faq-content-6" data-parent="#faq-accordion">
                            <div class="text">
                                <ul><li>This can be done by simply contacting us via <a href="{{ env('LIVE_CHAT_LINK') }}">live chat</a>.</li></ul>
                            </div>
                        </div>
                    </div><div class="component-collapse">
                        <h3 class="title collapsed" data-toggle="collapse" data-target="#faq-content-7">
                            What happens when I email an enquiry?
                        </h3>
                        <div class="text-wrapper collapse " id="faq-content-7" data-parent="#faq-accordion">
                            <div class="text">
                                <ul><li>We always endeavor to reply to all enquiries of any nature within 24 hours.</li></ul>
                            </div>
                        </div>
                    </div><div class="component-collapse">
                        <h3 class="title collapsed" data-toggle="collapse" data-target="#faq-content-8">
                            How can I pay for my package?
                        </h3>
                        <div class="text-wrapper collapse " id="faq-content-8" data-parent="#faq-accordion">
                            <div class="text">
                                <ul><li>We accept payment via debit/credit card. All payment methods and details are contained within your invoice.</li><li>Payment plans are also available subject to card issuer.</li></ul>
                            </div>
                        </div>
                    </div><div class="component-collapse">
                        <h3 class="title collapsed" data-toggle="collapse" data-target="#faq-content-9">
                            What happens after I submit my booking request?
                        </h3>
                        <div class="text-wrapper collapse " id="faq-content-9" data-parent="#faq-accordion">
                            <div class="text">
                                <ul><li>After you have chosen your preferred package, you will receive your booking number. If you have any questions once you receive your confirmation, you can email us on <a href='mailto:support@redtix.com'>support@redtix.com</a> with your booking number and query.</li></ul>
                            </div>
                        </div>
                    </div><div class="component-collapse">
                        <h3 class="title collapsed" data-toggle="collapse" data-target="#faq-content-10">
                            I have made a booking and now unable to travel, do I get a refund?
                        </h3>
                        <div class="text-wrapper collapse " id="faq-content-10" data-parent="#faq-accordion">
                            <div class="text">
                                <ul><li>Please refer to our Rugby World Cup 2019 <a href='#'>Terms & Conditions</a> if you need to cancel a booking.</li></ul>
                            </div>
                        </div>
                    </div><div class="component-collapse">
                        <h3 class="title collapsed" data-toggle="collapse" data-target="#faq-content-11">
                            RedTix doesn't have the match I want?
                        </h3>
                        <div class="text-wrapper collapse " id="faq-content-11" data-parent="#faq-accordion">
                            <div class="text">
                                <ul><li>If there is a match that you would like to attend and it’s not included within our packages, please feel free to contact us via <a href="{{ env('LIVE_CHAT_LINK') }}">live chat</a> and we will do our best to help accommodate your suggestions.</li></ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>