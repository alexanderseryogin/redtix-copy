<section class="section-packages">
    <div class="container">
        <p class="mb-5">
            Redtix has been appointed as an Authorised Sub-Agent for Rugby World Cup 2019™.<br>
            20 Nations will once again compete for rugby's biggest prize of the Webb Ellis Cup. Taking place in Japan,
            the land of the rising sun, this tournament provides the perfect opportunity to watch the best rugby teams
            face one another along with exploring a fascinating and beautiful country. Redtix personally customized
            packages ensure a unique and an one-of-a-kind experience.
        </p>
        <div class="package-details__title mb-2">PACKAGE INCLUDES:</div>
        <div class="packages">
            <packages :packages="{{ json_encode($packages) }}"></packages>
        </div>
        <div class="contact d-none d-md-block">
            <p>
                If these packages don't meet your needs, contact us so we can design one for you.
            </p>
            <p>
                Please “get in touch” via <a href="{{ env('LIVE_CHAT_LINK') }}">live chat</a>.
            </p>
        </div>
        <sponsor></sponsor>
    </div>

</section>