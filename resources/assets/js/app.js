/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('./index');



/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the body of the page. From here, you may begin adding components to
 * the application, or feel free to tweak this setup for your needs.
 */

window.Event = new Vue();

// Vue.component('ordersummary', require('./components/Ordersummary.vue'));
//
// Vue.component('example', require('./components/Example.vue'));
//
// Vue.component('tickets', require('./components/Tickets.vue'));
//
// Vue.component('ticket', require('./components/Ticket.vue'));
//
// Vue.component('coupon', require('./components/Coupon.vue'));

import VueSingleSelect from "vue-single-select";
Vue.component('vue-single-select', VueSingleSelect);

Vue.use(require('vue-moment'));

Vue.component('event-info-fixtures', require('./components/EventInfoFixtures/EventInfoFixtures.vue'));
Vue.component('date-ranger', require('./components/DateRanger.vue'));
Vue.component('matches', require('./components/EventInfoFixtures/parts/Matches'));
Vue.component('pools', require('./components/EventInfoFixtures/parts/Pools'));
Vue.component('videos', require('./components/EventInfoFixtures/parts/Videos'));
Vue.component('gallery', require('./components/EventInfoFixtures/parts/Gallery'));

Vue.component('package-builder', require('./components/PackageBuilder/PackageBuilder.vue'));

Vue.component('package-builder', require('./components/PackageBuilder/PackageBuilder.vue'));
Vue.component('package-builder-order-summary', require('./components/PackageBuilder/PackageBuilderOrderSummary.vue'));
Vue.component('package-builder-search-panel', require('./components/PackageBuilder/PackageBuilderSearchPanel.vue'));

Vue.component('category', require('./components/PackageBuilder/Category.vue'));
Vue.component('flight', require('./components/PackageBuilder/Flight.vue'));
Vue.component('flight-details', require('./components/PackageBuilder/FlightDetails.vue'));
Vue.component('hotel', require('./components/PackageBuilder/Hotel.vue'));

Vue.component('package-detail', require('./components/PackageDetail/PackageDetail.vue'));
Vue.component('package-detail-ticket', require('./components/PackageDetail/PackageDetailTicket.vue'));
Vue.component('package-detail-flight', require('./components/PackageDetail/PackageDetailFlight.vue'));
Vue.component('package-detail-hotel', require('./components/PackageDetail/PackageDetailHotel.vue'));

Vue.component('package', require('./components/PackagesPage/Package.vue'));
Vue.component('packages', require('./components/PackagesPage/Packages.vue'));

Vue.component('sponsor', require('./components/Sponsor.vue'));

Vue.filter('currency', function (value) {
    let val=value?value:0;
    return val.toLocaleString("en-EN",{minimumFractionDigits: 2, maximumFractionDigits: 2});
})
Vue.filter('currencyShort', function (value) {
    let val=value?value:0;
    return val.toLocaleString("en-EN",{minimumFractionDigits: 0, maximumFractionDigits: 0});
})
import { createStore } from './store'
const store = createStore()

const app = new Vue({
    el: '#app',
    store,
    data: {

    },
    methods: {

    },
    mounted() {
        
    }

});

export const EventBus = new Vue();
