import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export function createStore () {
    return new Vuex.Store({
        state: {
            packageInfo:{flight:{city_origin:1,city_destination:2} ,},
            rooms:1,
            travelers:1,
            data4post:{flight:'',hotel:0,tickets:[],packageName:'',travelers:0,rooms:0},
            imagesPath: '/images/rugby2019/',
        },
        mutations: {
            setPackageInfoValue(state,info) {
                state.packageInfo=info.packageInfo;
                state.rooms=info.rooms;
                state.travelers=info.travelers;
                state.data4post.packageName=info.packageInfo.name
                state.data4post.travelers=info.travelers
                state.data4post.rooms=info.rooms
            },
            addTicket(state,ticketCatAndId) {
                let cat=ticketCatAndId.type.toLowerCase()
                let ticketId=ticketCatAndId.id
                state.packageInfo.tickets.some(function (item) {
                    if(item.id==ticketId) {
                        let price=0
                        if('b'==cat)
                            price=item.cat_B
                        else if('c'==cat)
                            price=item.cat_C
                        else
                            price=item.cat_D
                        state.data4post.tickets.push({
                            'cat':cat,
                            'id':ticketId,
                            'price':price,
                        });
                        return true
                    }else return false;
                })
                // for(let i=0; i<state.packageInfo.tickets.length; i++) {
                //     if(state.packageInfo.tickets[i].id===ticketId) {
                //         state.data4post.tickets.push(state.packageInfo.tickets[i]);
                //         break
                //     }
                // }
            },
            delTicketByCategory(state,ticketCat) {
                let cat=ticketCat.toLowerCase()
                if(state.data4post.tickets) {
                    let arr = state.data4post.tickets.filter(function (val,index) {
                        // console.log('val.cat != cat',val.cat ,cat)
                        return val.cat != cat;
                    })
                    state.data4post.tickets=arr
                }
            },
            setTotalAmount(state,amount) {
                state.totalAmount=amount
            },
            selectedFlight(state,shortName) {
                state.data4post['flight']=state.packageInfo.flight.city_origin.abbr;
            },
            selectedHotel(state,name) {
                state.data4post['hotel']=state.packageInfo.hotel.hotel_name;
            },
        },
        getters: {
            packageInfo: state => {
                return state.packageInfo
            },
            totalAmount: state => {
                let amount=0
                let minAmount=0

                if(state.packageInfo.flight) {
                    minAmount+=state.packageInfo.flight.cost * state.travelers
                    if(state.data4post.flight) {
                        amount += state.packageInfo.flight.cost * state.travelers
                    }
                }
                if(state.packageInfo.hotel) {
                    minAmount+=state.packageInfo.hotel.hotel_cost * state.rooms
                    if(state.data4post.hotel ) {
                        amount += state.packageInfo.hotel.hotel_cost * state.rooms
                    }
                }
                if(state.data4post.tickets) {
                    for(let i=0;i<state.data4post.tickets.length;i++) {
                        amount+=state.data4post.tickets[i].price * state.travelers
                    }

                }
                if(state.packageInfo.tickets) {
                    state.packageInfo.tickets.forEach(function (item) {
                        if(item.cat_D) {
                            minAmount+=item.cat_D * state.travelers
                        }
                    })
                }
                if(state.packageInfo.margin) {
                    amount+=amount*state.packageInfo.margin/100
                    minAmount+=minAmount*state.packageInfo.margin/100
                }
                if(amount<minAmount) {
                    amount=minAmount
                }
                return amount
            },
            rooms: state => {
                return state.rooms
            },
            travelers: state => {
                return state.travelers
            },
            data4post: state => {
                return state.data4post
            },
            tickets: state => {
                if(!state.packageInfo)
                    return []
                return state.packageInfo.tickets
            },
            ticketsB: state => {
                let arr=[]
                if(!state.packageInfo || !state.packageInfo.tickets)
                    return arr
                state.packageInfo.tickets.forEach(function (item) {
                    if(item.cat_B) {
                        arr.push(item)
                    }
                })

                return arr
            },
            ticketsC: state => {
                let arr=[]
                if(!state.packageInfo || !state.packageInfo.tickets)
                    return arr
                state.packageInfo.tickets.forEach(function (item) {
                    if(item.cat_C) {
                        arr.push(item)
                    }
                })

                return arr
            },
            ticketsD: state => {
                let arr=[]
                if(!state.packageInfo || !state.packageInfo.tickets)
                    return arr
                state.packageInfo.tickets.forEach(function (item) {
                    if(item.cat_D) {
                        arr.push(item)
                    }
                })

                return arr
            },
            tickets4postUniqCount: state => {
                let ids=[]
                if(!state.data4post.tickets)
                    return 0
                state.data4post.tickets.forEach(function (item) {
                    if(ids.indexOf(item.id)===-1) {
                        ids.push(item.id)
                    }
                })
                return ids.length
            },
            tickets4postBcount: state => {
                let cnt=0
                if(!state.data4post.tickets)
                    return cnt
                state.data4post.tickets.forEach(function (item) {
                    if('b'===item.cat.toLowerCase()) {
                        cnt++
                    }
                })
                return cnt * state.travelers
            },
            tickets4postCcount: state => {
                let cnt=0
                if(!state.data4post.tickets)
                    return cnt
                state.data4post.tickets.forEach(function (item) {
                    if('c'===item.cat.toLowerCase()) {
                        cnt++
                    }
                })
                return cnt * state.travelers
            },
            tickets4postDcount: state => {
                let cnt=0
                if(!state.data4post.tickets)
                    return cnt
                state.data4post.tickets.forEach(function (item) {
                    if('d'===item.cat.toLowerCase()) {
                        cnt++
                    }
                })
                return cnt * state.travelers
            },
            imagesPath: state => {
                return state.imagesPath
            },
        },
        actions: {
            clearAll: (context, data) => {
                context.state.tickets= []
                context.state.totalAmount=0
                context.state.rooms=1
                context.state.travelers=1
                context.state.data4post={flight:'',hotel:0,tickets:[]}
            },
            setPackageInfo: (context, payload) => {
                context.commit('setPackageInfoValue',payload)
            },
            selectedFlight: (context, payload) => {
                context.commit('selectedFlight',payload)
            },
            selectedHotel: (context, payload) => {
                context.commit('selectedHotel',payload)
            },
            addTicket: (context, payload) => {
                context.commit('addTicket',payload)
            },
            delTicketByCategory: (context, payload) => {
                context.commit('delTicketByCategory',payload)
            },
        },
    })
}